package com.embibejio.coreapp.application

import android.annotation.SuppressLint
import android.content.Context
import com.embibejio.coreapp.BuildConfig
import com.segment.analytics.Analytics


class LibApp {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
        lateinit var activityContext: Context
        private var mInstance: LibApp? = null
        lateinit var analyticsRewrite: Analytics
        lateinit var unityAnalyticsRewrite: Analytics

        @Synchronized
        fun getAnalytics(): Analytics {
            return analyticsRewrite
        }

        @Synchronized
        fun getUnityAnalytics(): Analytics {
            return unityAnalyticsRewrite
        }

        @Synchronized
        fun Create(context_: Context) {

            context = context_
            analyticsRewrite = Analytics.Builder(
                context_,
                BuildConfig.SEGMENT_KEY
            ).tag("android")
                .flushQueueSize(1)
                .logLevel(Analytics.LogLevel.VERBOSE)
                .build()

            unityAnalyticsRewrite = Analytics.Builder(
                context_,
                BuildConfig.SEGMENT_KEY
            ).tag("unity")
                .flushQueueSize(1)
                .logLevel(Analytics.LogLevel.VERBOSE)
                .build()


            //Analytics.setSingletonInstance(analyticsRewrite)
        }

    }

}