package com.embibejio.coreapp.preference

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.embibejio.coreapp.application.LibApp
import com.embibejio.coreapp.model.LinkedProfile
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken


class PreferenceHelper {

    var preferences: SharedPreferences? =
        PreferenceManager.getDefaultSharedPreferences(LibApp.context)

    fun put(key: String, value: Any) {
        if (preferences == null)
            preferences = PreferenceManager.getDefaultSharedPreferences(LibApp.context)
        when (value) {
            is String -> preferences?.edit()?.putString(key, value)?.apply()
            is Int -> preferences?.edit()?.putInt(key, value)?.apply()
            is Boolean -> preferences?.edit()?.putBoolean(key, value)?.apply()
            is Float -> preferences?.edit()?.putFloat(key, value)?.apply()
            is Long -> preferences?.edit()?.putLong(key, value)?.apply()
            else -> throw UnsupportedOperationException("Not yet implemented")
        }
    }

    @Suppress("UNCHECKED_CAST")
    operator fun <T> get(key: String, defaultValue: T? = null): T {
        if (preferences == null)
            preferences = PreferenceManager.getDefaultSharedPreferences(LibApp.context)
        return when (defaultValue) {
            is String, null -> preferences?.getString(key, defaultValue as? String) as T
            is Int -> preferences?.getInt(key, defaultValue as? Int ?: -1) as T
            is Boolean -> preferences?.getBoolean(key, defaultValue as? Boolean ?: false) as T
            is Float -> preferences?.getFloat(key, defaultValue as? Float ?: -1f) as T
            is Long -> preferences?.getLong(key, defaultValue as? Long ?: -1) as T
            else -> throw UnsupportedOperationException("Not yet implemented")
        }
    }


    fun clear() {
        if (preferences == null) {
            preferences = PreferenceManager.getDefaultSharedPreferences(LibApp.context)
            preferences?.edit()?.clear()?.apply()
        } else {
            preferences?.edit()?.clear()?.apply()
        }
    }


    fun <T> putModel(`object`: T, key: String) {
        val jsonString = GsonBuilder().create().toJson(`object`)
        preferences!!.edit().putString(key, jsonString).apply()
    }

    fun <T> getModel(key: String): Any? {
        val value = preferences!!.getString(key, null)
        return GsonBuilder().create().fromJson(value, Any::class.java)
    }

    fun putObject(key: String?, `object`: Any?) {
        remove(key)
        val gson = Gson()
        val json = gson.toJson(`object`)
        preferences!!.edit()
            .putString(key, json).apply()
    }

    fun getObject(key: String?): Any? {
        val gson = Gson()
        return if (preferences!!.contains(key)) gson.fromJson<Any>(
            preferences?.getString(
                key,
                null
            ),
            Any::class.java
        ) else null
    }

    fun remove(key: String?) {
        if (preferences!!.contains(key)) preferences!!.edit()
            .remove(key).apply()
    }


    fun <T> putDataModelList(
        key: String?,
        listDatum: List<T?>?
    ) {
        remove(key)
        val gson = Gson()
        val json = gson.toJson(listDatum)
        preferences!!.edit()
            .putString(key, json).apply()
    }

    fun getDataModelList(key: String?): List<Any?>? {
        val gson = Gson()
        val collectionType =
            object : TypeToken<Collection<*>>() {}.type
        return if (preferences!!.contains(key)) gson.fromJson<List<Any>>(
            preferences!!.getString(
                key,
                null
            ),
            collectionType
        ) else null
    }


    fun getCurrentProfile(key: String?): LinkedProfile?? {
        val gson = Gson()
        val collectionType =
            object : TypeToken<LinkedProfile?>() {}.type
        return if (preferences!!.contains(key)) gson.fromJson<LinkedProfile>(
            preferences!!.getString(
                key,
                null
            ),
            collectionType
        ) else null
    }
}