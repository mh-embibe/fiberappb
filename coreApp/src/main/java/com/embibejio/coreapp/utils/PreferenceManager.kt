package com.embibejio.coreapp.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.embibejio.coreapp.constant.AppConstants

class PreferenceManager private constructor(context: Context) {
    private var sharedPreferences: SharedPreferences = context.getSharedPreferences(
        AppConstants.PREF_FILE,
        Context.MODE_PRIVATE
    )
    private var editor: SharedPreferences.Editor? = null
    private var bulkUpdate = false
    fun put(key: String?, `val`: String?) {
        doEdit()
        editor!!.putString(key, `val`)
        doCommit()
    }

    fun put(key: String?, `val`: Int) {
        doEdit()
        editor!!.putInt(key, `val`)
        doCommit()
    }

    fun put(key: String?, `val`: Boolean) {
        doEdit()
        editor!!.putBoolean(key, `val`)
        doCommit()
    }

    fun put(key: String?, `val`: Float) {
        doEdit()
        editor!!.putFloat(key, `val`)
        doCommit()
    }

    fun put(key: String?, `val`: Double) {
        doEdit()
        editor!!.putString(key, `val`.toString())
        doCommit()
    }

    fun put(key: String?, `val`: Long) {
        doEdit()
        editor!!.putLong(key, `val`)
        doCommit()
    }

    fun getString(key: String?, defaultValue: String?): String? {
        return sharedPreferences.getString(key, defaultValue)
    }

    fun getString(key: String?): String? {
        return sharedPreferences.getString(key, null)
    }

    fun getInt(key: String?): Int {
        return sharedPreferences.getInt(key, 0)
    }

    fun getInt(key: String?, defaultValue: Int): Int {
        return sharedPreferences.getInt(key, defaultValue)
    }

    fun getLong(key: String?): Long {
        return sharedPreferences.getLong(key, 0)
    }

    fun getLong(key: String?, defaultValue: Long): Long {
        return sharedPreferences.getLong(key, defaultValue)
    }

    fun getFloat(key: String?): Float {
        return sharedPreferences.getFloat(key, 0f)
    }

    fun getFloat(key: String?, defaultValue: Float): Float {
        return sharedPreferences.getFloat(key, defaultValue)
    }

    fun getDouble(key: String?): Double {
        return getDouble(key, 0.0)
    }

    private fun getDouble(key: String?, defaultValue: Double): Double {
        return try {
            java.lang.Double.valueOf(
                sharedPreferences.getString(
                    key,
                    defaultValue.toString()
                )!!
            )
        } catch (nfe: NumberFormatException) {
            defaultValue
        }
    }

    fun getBoolean(key: String?, defaultValue: Boolean): Boolean {
        return sharedPreferences.getBoolean(key, defaultValue)
    }

    fun getBoolean(key: String?): Boolean {
        return sharedPreferences.getBoolean(key, false)
    }

    fun remove(vararg keys: String?) {
        doEdit()
        for (key in keys) {
            editor!!.remove(key)
        }
        doCommit()
    }

    fun clear() {
        doEdit()
        editor!!.clear()
        doCommit()
    }

    @SuppressLint("CommitPrefEdits")
    fun edit() {
        bulkUpdate = true
        editor = sharedPreferences.edit()
    }

    fun commit() {
        bulkUpdate = false
        editor!!.commit()
        editor = null
    }

    @SuppressLint("CommitPrefEdits")
    private fun doEdit() {
        if (!bulkUpdate && editor == null) {
            editor = sharedPreferences.edit()
        }
    }

    private fun doCommit() {
        if (!bulkUpdate && editor != null) {
            editor!!.commit()
            editor = null
        }
    }

    companion object {
        private var instance: PreferenceManager? = null

        @JvmStatic
        fun getInstance(context: Context): PreferenceManager? {
            if (instance == null) {
                synchronized(PreferenceManager::class.java) {
                    if (instance == null) {
                        instance =
                            PreferenceManager(context)
                    }
                }
            }
            return instance
        }
    }

}