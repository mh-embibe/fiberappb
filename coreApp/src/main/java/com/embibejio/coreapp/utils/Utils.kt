package com.embibejio.coreapp.utils

import com.embibejio.coreapp.events.CooboEventParams
import com.embibejio.coreapp.events.UnitySegmentIO
import org.jetbrains.anko.doAsync
import org.json.JSONObject


/**
 * A collection of utility methods, all static.
 */
object Utils {

    private var mLastKeyDownTime: Long = 0

    /*Book Details Events*/
    @JvmStatic
    fun trackCooboEvent(
        obj: JSONObject,
        conceptId: String,
        muteState: Boolean,
        learnmapCode: String
    ) {
        doAsync {
            var cooboParams = getCooboParams(obj, conceptId, muteState, learnmapCode)
            UnitySegmentIO.getInstance().track(
                false,
                if (obj.has("log_type") && !obj.isNull("log_type")) obj.getString("log_type") else null,
                if (obj.has("event_name") && !obj.isNull("event_name")) obj.getString("event_name") else null,
                if (obj.has("event_code") && !obj.isNull("event_code")) obj.getString("event_code") else null,
                if (obj.has("nav_element") && !obj.isNull("nav_element")) obj.getString("nav_element") else null,
                if (obj.has("event_type") && !obj.isNull("event_type")) obj.getString("event_type") else null,
                if (obj.has("extra_params") && !obj.isNull("extra_params")) obj.getJSONObject("extra_params") else null,
                if (obj.has("session_id") && !obj.isNull("session_id")) obj.getString("session_id") else null,
                cooboParams
            )
        }
    }

    fun getCooboParams(
        obj: JSONObject,
        conceptId: String,
        muteState: Boolean,
        learnmapCode: String
    ): CooboEventParams {
        var cooboParams = CooboEventParams()
        if (conceptId != null)
            cooboParams.concept_id = conceptId
        if (muteState != null)
            cooboParams.mute_state = muteState
        if (learnmapCode != null)
            cooboParams.learning_map = learnmapCode
        if (obj.has("concept_name"))
            cooboParams.concept_name = obj.getString("concept_name")
        if (obj.has("coobo_id"))
            cooboParams.coobo_id = obj.getString("coobo_id")
        if (obj.has("coobo_name"))
            cooboParams.coobo_name = obj.getString("coobo_name")
        if (obj.has("slide_number"))
            cooboParams.slide_number = obj.getString("slide_number")
        if (obj.has("slide_name"))
            cooboParams.slide_name = obj.getString("slide_name")
        if (obj.has("slide_id"))
            cooboParams.slide_id = obj.getString("slide_id")
        if (obj.has("slide_type"))
            cooboParams.slide_type = obj.getString("slide_type")
        return cooboParams
    }


    @JvmStatic
    fun currentDurationForCoobo(current_slide: Int, total_slide: Int, length: Int): Long {
        try {
            if (current_slide <= total_slide && current_slide != 1) {
                var time = length / current_slide
                return ((current_slide - 1) * time) + 1.toLong()
            }
        } catch (e: Exception) {
        }
        return 0L
    }
}