package com.embibejio.coreapp.model

import com.google.gson.annotations.SerializedName

class LinkedProfile {


    @SerializedName("is_achieve")
    var isAchieve: Boolean = false

    @SerializedName("subcStatus")
    var subcStatus: String? = null

    @SerializedName("uid")
    var uid: String? = ""

    @SerializedName("first_name")
    var firstName: String? = null

    @SerializedName("last_name")
    var lastName: String? = null

    @SerializedName("user_type")
    var userType: String? = null

    @SerializedName("gender")
    var gender: String? = null

    @SerializedName("user_id")
    var userId: String = ""

    @SerializedName("school")
    var school: String? = null

    @SerializedName("grade")
    var grade: String? = null

    @SerializedName("city")
    var city: String? = null

    @SerializedName("email")
    var email: String? = ""

    @SerializedName("goals")
    var goals: List<String>? = null

    @SerializedName("_board")
    var _board: String? = null

    @SerializedName("board")
    var board: String? = null

    @SerializedName("mobile")
    var mobile: String? = ""

    @SerializedName("primary_exam_code")
    var primary_exam_code: String? = null

    @SerializedName("primary_goal_code")
    var primary_goal_code: String? = null

    @SerializedName("secondary_goal_code")
    var secondary_exam_goal_id: String? = null

    @SerializedName("secondary_exams")
    var secondary_exams: String? = null

    @SerializedName("profile_pic")
    var profilePic: String? = null

    @SerializedName("packs")
    var packs: List<Pack>? = null

    @SerializedName("embibe_token")
    var embibe_token: String? = null

    @SerializedName("city_id")
    var city_id: String? = null

    @SerializedName("state_id")
    var state_id: String? = null

    @SerializedName("latitude")
    var latitude: String? = null

    @SerializedName("longitude")
    var longitude: String? = null

    var goalSupported: Boolean = false

    var examSupported: Boolean = false
}