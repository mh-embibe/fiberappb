package com.embibejio.coreapp.constant

object AppConstants {

    const val NO_NETWORK: String = "Network not connected! check your connection status!"
    const val EMBIBETOKEN = "embibe-token"
    const val EVENT: String = "event"

    const val HEADER: String = "header"
    const val CHAPTER: String = "chapter"
    const val CATEGORY: String = "category"

    //    const val DB_NAME = BuildConfig.APPLICATION_ID + ".db"
    const val PREF_FILE = "com.embibe.pref"
    const val VIDEO_URL = "video_url"

    const val USER_ID = "user_id"
    const val CURRENT_PROFILE = "current_profile"
    const val CURRENT_PROFILE_USER_ID = "current_profile_user_id"
    const val SLIDE_TYPE_COOBO = "Coobo"
    const val SLIDE_TYPE_LEARN = "Coobo"
    const val SLIDE_TYPE_PRACTICE = "Practice"

    //shared_preference key
    const val IS_LOGIN = "isLogIn"
    const val USER_TYPE = "user_type"
    const val MOBILE = "mobile"

}