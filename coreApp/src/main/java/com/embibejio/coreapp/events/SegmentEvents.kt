package com.embibejio.coreapp.events

import java.util.*

/**
 * Created by sriram on 31/03/20.
 */
class SegmentEvents private constructor() {
    fun getEventCode(eventName: String?): String? {
        return eventCodeMap[eventName]
    }

    companion object {


        @JvmStatic
        var instance: SegmentEvents? = null
            get() {
                if (field == null) {
                    synchronized(SegmentIO::class.java) {
                        if (field == null) {
                            field = SegmentEvents()
                        }
                    }
                }
                return field
            }
            private set
        private lateinit var eventCodeMap: MutableMap<String, String>

        //API Failure
        const val LOG_TYPE_API_FAILURE = "api_failure"


        /*LogType */
        const val LOG_TYPE_TEST_FEEDBACK = "test feedback"
        const val LOG_TYPE_TEST_WINDOW = "test_window"
        const val LOG_TYPE_PRACTICE_WINDOW = "practice_window"
        const val LOG_TYPE_TEST_DASHBOARD = "test_dashboard"
        const val LOG_TYPE_ACCOUNT_WINDOW = "account_window"
        const val LOG_TYPE_MAIN_WINDOW = "main_window"
        const val LOG_TYPE_PROFILE_PAGE = "profile-page"
        const val LOG_TYPE_ADD_EXAMS = "add exams"
        const val LOG_TYPE_USER = "user"
        const val LOG_TYPE_ONBOARDING = "onboarding"
        const val LOG_TYPE_SIGN_IN = "sign-in"
        const val LOG_TYPE_TERMS_CONDITION = "terms and condition"
        const val LOG_TYPE_TEST = "test"
        const val LOG_TYPE_TEST_INSTRUCTION = "test instruction"
        const val LOG_TYPE_TEST_TAKING = "test taking"
        const val LOG_TYPE_CREATE_TEST = "create test"
        const val LOG_TYPE_TEST_MORE_INFO = "test more info"


        const val LOG_TYPE_SPLASH = "splash screen 1"
        const val LOG_TYPE_LOGIN = "login"
        const val LOG_TYPE_FORGOT_PASSWORD = "forgot password"
        const val LOG_TYPE_SWITCH_USER = "switch user"

        const val LOG_TYPE_USER_SWITCH_WINDOW = "user_switch_window"
        const val LOG_TYPE_PROFILE_WINDOW = "profile_window"
        const val LOG_TYPE_TEST_LIST_WINDOW = "test_list_window"
        const val LOG_TYPE_PRACTICE_LIST_WINDOW = "practice_list_window"
        const val LOG_TYPE_LEARN_WINDOW = "learn_window"
        const val LOG_TYPE_SEARCH_WINDOW = "search_window"
        const val LOG_TYPE_DETAIL_WINDOW = "detail_window"
        const val LOG_TYPE_PLAYER_WINDOW = "player_window"
        const val LOG_TYPE_APP_STUDY = "app-study"
        const val LOG_TYPE_HOME = "home"
        const val LOG_TYPE_RECOMMENDATION = "recommendation"
        const val LOG_TYPE_VIDEO_PLAYER = "video-player"
        const val LOG_TYPE_MORE_INFO = "more info"
        const val LOG_TYPE_SEARCH = "search"
        const val LOG_TYPE_CRASH = "crash"
        const val LOG_TYPE_ERROR = "error"
        const val LOG_TYPE_LEARN_SUMMARY = "learn summary"
        const val LOG_TYPE_PRACTICE_SUMMARY = "practice summary"
        const val LOG_TYPE_TEST_SUMMARY = "test summary"


        /*button type*/
        const val BUTTON_TYPE_MORE_INFO = "More info"
        const val BUTTON_TYPE_LEARN = "Learn"
        const val BUTTON_TYPE_MORE_PRACTICE = "Practice"
        const val BUTTON_TYPE_MORE_TEST = "Test"

        // Splash Window Events
        const val EVENT_APP_LAUNCH = "app launch"
        const val EVENT_SPLASH_LOAD_START = "splash screen load start"
        const val EVENT_SPLASH_LOAD_END = "splash screen load end"
        const val EVENT_FIRST_LAUNCH_SW = "first launch SW"

        const val EVENT_QUESTION_NUMBER = "question-number"


        /*login*/
        const val EVENT_LOGIN_LOAD_START = "login screen load start"
        const val EVENT_LOGIN_LOAD_END = "login screen load end"
        const val EVENT_LOGIN_SIGNIN_BTN_FOCUS = "sign-in focus"
        const val EVENT_LOGIN_SIGNIN_BTN_CLICK = "sign-in click"
        const val EVENT_LOGIN_SIGNUP_BTN_FOCUS = "sign-up focus"
        const val EVENT_LOGIN_SIGNUP_BTN_CLICK = "sign-up click"


        /*user selection*/
        const val EVENT_VERIFY_OTP_CLICK = "verify otp click"
        const val EVENT_RE_SEND_OTP_CLICK = "re-send otp click"
        const val EVENT_VIEW_USERS_AW = "view users AW"
        const val EVENT_SELECT_USER_AW = "select users AW"

        const val EVENT_SWITCH_SCREEN_LOAD_START = "switch user screen load start"
        const val EVENT_SWITCH_SCREEN_LOAD_END = "switch user screen load end"

        const val LOG_TYPE_CHOOSE_GOAL = "choose goal"

        const val EVENT_SCREEN_CHOOSE_AVATAR_LOAD_START = "choose your avatar screen load start"
        const val EVENT_SCREEN_CHOOSE_AVATAR_LOAD_END = "choose your avatar screen load end"
        const val EVENT_AVATAR_ICON_CLICKED = "avatar icon clicked"


        /*forgot password*/
        const val EVENT_FORGO_PASSWORD_MOBILE_SCREEN_LOAD_START = "mobile screen load start"
        const val EVENT_FORGO_PASSWORD_MOBILE_SCREEN_LOAD_END = "mobile screen load end"
        const val EVENT_FORGO_PASSWORD_SUBMIT_CLICK = "submit click"
        const val EVENT_FORGO_PASSWORD_BACK_TO_SIGN_IN_CLICK = "back to sign-in click"
        const val EVENT_FORGO_PASSWORD_OTP_SCREEN_LOAD_START = "OTP screen load start"
        const val EVENT_FORGO_PASSWORD_OTP_SCREEN_LOAD_END = "OTP screen load end"
        const val EVENT_FORGO_PASSWORD_VERIFY_OTP_CLICK = "forgot password verify otp click"
        const val EVENT_FORGO_PASSWORD_RESEND_OTP_CLICK = "forgot password resend otp click"
        const val EVENT_FORGO_PASSWORD_RE_ENTER_OTP_CLICK = "re-enter otp click"
        const val EVENT_FORGO_PASSWORD_SCREEN_LOAD_START = "screen load start"
        const val EVENT_SWITCH_USER_PROFILE_CLICK = "switch user profile click"
        const val EVENT_SWITCH_USER_ADD_USER_CLICK = "switch add user click"


        // Account Window
        const val EVENT_LOGIN_AW = "login AW"
        const val EVENT_LOGIN_SCRN_LOAD_START_AW = "login screen load start AW"
        const val EVENT_CLICK_LOGIN_AW = "click login AW"
        const val EVENT_LOGIN_SUCCESS_AW = "login success AW"
        const val EVENT_LOGIN_FAILED_AW = "login failed AW"
        const val EVENT_SINGNUP_AW = "signup AW"
        const val EVENT_SINGNUP_FAILED_AW = "signup failed AW"
        const val EVENT_LOCALE_CHANGED_AW = "locale changed AW"


        /*content*/
        const val EVENT_CONTENT_SELECTION = "event content selection LW"
        const val EVENT_SUBJECT_SELECTION = "event subject selection LW"
        const val EVENT_SEARCH_EXECUTED = "event search executed SW"
        const val EVENT_CONTENT_BOOKMARKED = "event bookmarked DW"
        const val EVENT_CONTENT_LIKED = "event liked DW"

        /*player */
        const val EVENT_CONTENT_PLAY = "play VP"
        const val EVENT_CONTENT_PAUSE = "pause VP"
        const val EVENT_CONTENT_LOADING_START = "load start VP"
        const val EVENT_CONTENT_LOADING_END = "load end VP"
        const val EVENT_CONTENT_PLAYBACK_ERROR = "player error VP"
        const val EVENT_CONTENT_PLAY_PAUSE_CLICK = "play or pause video click VP"
        const val EVENT_CONTENT_BUFFER_START = "buffer start VP"
        const val EVENT_CONTENT_BUFFER_END = "buffer end VP"
        const val EVENT_CONTENT_SEEK = "seek VP"
        const val EVENT_CONTENT_SETTING_CLICK = "setting VP"
        const val EVENT_CONTENT_VOLUME_CLICK = "volume VP"
        const val EVENT_CONTENT_BOOKMARK_CLICK = "bookmark click VP"
        const val EVENT_CONTENT_BOOKMARK_FOCUS = "bookmark focus VP"
        const val EVENT_CONTENT_LIKE_CLICK = "like click VP"
        const val EVENT_CONTENT_LIKE_FOCUS = "like focus VP"
        const val EVENT_CONTENT_EXIT = "exit VP"

        /* event_names */

        //home
        const val EVENT_SCREEN_LOAD_START = "home screen load start"
        const val EVENT_SCREEN_LOAD_END = "home screen load end"
        const val EVENT_HEROBANNER_MAIN_BUTTON_FOCUS = "herobanner main button focus"
        const val EVENT_HEROBANNER_MAIN_BUTTON_CLICK = "herobanner main button click"
        const val EVENT_HEROBANNER_MOREINFO_BUTTON_FOCUS = "herobanner moreinfo button focus"
        const val EVENT_HEROBANNER_MOREINFO_BUTTON_CLICK = "herobanner moreinfo button click"
        const val EVENT_HEROBANNER_SLIDE_CHANGE = "herobanner slide change"
        const val EVENT_HOME_TILE_CLICK = "home tile click"
        const val EVENT_HOME_TILE_FOCUS = "home tile focus"
        const val EVENT_NEXT_VIDEO_TILE_FOCUS = "play next video tile focus"
        const val EVENT_NEXT_VIDEO_TILE_CLICK = "play next video tile click"
        const val EVENT_RECOMMENDED_VIDEO_TILE_FOCUS = "recommended video tile focus"
        const val EVENT_RECOMMENDED_VIDEO_TILE_CLICK = "recommended video tile click"
        const val EVENT_REPLAY_BUTTON_FOCUS = "replay button focus"
        const val EVENT_REPLAY_BUTTON_CLICK = "replay button click"
        const val EVENT_REPLAY_ERROR_BUTTON_FOCUS = "replay error button focus"
        const val EVENT_REPLAY_ERROR_BUTTON_CLICK = "replay error button click"
        const val EVENT_PLAY_NEXT_VIDEO_TIME_COMPLETES = "play next video timer completes"
        const val EVENT_PLAY_NEXT_VIDEO_TIME_CANCELED = "play next video timer canceled"
        const val EVENT_ADVERSTIMENT_BANNER_CLICK = "adverstiment banner click"
        const val EVENT_ADVERSTIMENT_BANNER_FOCUS = "adverstiment banner focus"
        const val EVENT_NAVIAGTION_BAR_VIEW_OPEN = "navigation bar view open"
        const val EVENT_NAVIAGTION_BAR_VIEW_CLOSE = "naviagtion bar view close"
        const val EVENT_NAVIGATION_BAR_ITEM_CLICK = "navigation bar item click"
        const val EVENT_NAVIGATION_BAR_ITEM_FOCUS = "navigation bar item focus"


        // Main Window
        const val EVENT_MENU_SELECTION = "event menu selection MW"

        const val EVENT_GOAL_CHANGED_MW = "goal changed MW"
        const val EVENT_EXAM_CHANGED_MW = "exam changed MW"
        const val EVENT_CLICK_LEARN_MW = "click learn MW"
        const val EVENT_CLICK_NOTIFICATIONS_MW = "click_notifications MW"
        const val EVENT_LOCALE_CHANGED_MW = "locale changed MW"
        const val EVENT_CLICK_SYNC_MW = "click sync MW"

        // Profile Activity
        const val EVENT_LOGOUT_PW = "logout PW"

        //Test Feedback Window
        const val EVENT_CLICK_QUESTION_TF = "click question TF"
        const val EVENT_CLICK_HOW_DID_YOU_SCORE_TF = "click how_did_you_score TF"
        const val EVENT_CLICK_HOW_DID_YOU_SPENT_YOUR_TIME_TF =
            "click how_did_you_spend_you_time TF"
        const val EVENT_CLICK_HOW_EFFECTIVE_WERE_YOUR_ATTEMPTS_TF =
            "click how_effective_were_your_attempts TF"
        const val EVENT_CLICK_FIRST_LOOK_TF = "click first_look_graph TF"
        const val EVENT_CLICK_TEST_TAKING_STRAATEGY_TF =
            "click what_was_your_test_taking_strategy TF"
        const val EVENT_CLICK_QUESTION_WISE_ANALYSIS_TF = "click questionwise_analysis TF"
        const val EVENT_CLICK_CHAPTERWISE_ANALYSIS_TF = "click chapterwise_analysis TF"
        const val EVENT_CLICK_CHAPTER_I_GOT_RIGHT_TF = "click chapters i got right TF"
        const val EVENT_CLICK_CHAPTERS_I_GOT_WRONG_TF = "click chapters i got wrong TF"
        const val EVENT_CLICK_CHAPTER_I_DID_NOT_ATTEMPT_TF =
            "click chapters i did not attempt TF"

        // Test List Winodw
        const val EVENT_START_TEST_TLW = "start test TLW"
        const val EVENT_CLICK_TEST_FEEDBACK_TLW = "click feedback test TLW"
        const val EVENT_BEGIN_TEST_TLW = "test begin TLW"
        const val EVENT_VIEW_INSTRUCTIONS_TLW = "view instruction TLW"
        const val EVENT_BACK_FROM_INSTRUCTIONS_TLW = "back from instructions TLW"
        const val EVENT_CLICK_DOWNLOAD_TLW = "click download TLW"
        const val EVENT_DOWNLOAD_COMPLETE_TLW = "download complete TLW"
        const val EVENT_CANCEL_DOWNLOAD_TLW = "download cancel TLW"

        // Practice List Window
        const val EVENT_CLICK_DOWNLOAD_MORE_PLW = "click download more PLW"
        const val EVENT_CLICK_DOWNLOAD_PLW = "click download PLW"
        const val EVENT_CLICK_PRACTICE_FEEDBACK_PLW = "click feedback practice PLW"
        const val EVENT_START_PRACTICE_PLW = "start practice PLW"
        const val EVENT_CANCEL_DOWNLOAD_PLW = "download cancel PLW"
        const val EVENT_DOWNLOAD_COMPLETE_PLW = "download comaplete PLW"

        //    Test Window
        const val EVENT_VIEW_INSTRUCTIONS_TW = "view instruction TW"
        const val EVENT_BACK_FROM_INSTRUCTIONS_TW = "back from instructions TW"
        const val EVENT_ANSWER_CHOICE_TW = "answer choice TW"
        const val EVENT_CLICK_QUESTION_TW = "click question TW"
        const val EVENT_SAVE_ANSWER_TW = "save answer TW"
        const val EVENT_REVIEW_LATER_TW = "review later TW"
        const val EVENT_CLEAR_SELECTION_TW = "clear selection TW"
        const val EVENT_SELECT_SUBJECT_TW = "select subject TW"
        const val EVENT_VIEW_ALL_QUESTIONS_TW = "view all questions TW"
        const val EVENT_BACK_FROM_ALL_QUESTIONS_TW = "back from all questions TW"
        const val EVENT_FINISH_TEST_WINDOW_TW = "finish test window TW"
        const val EVENT_FINISH_TEST_POPUP_TW = "finish test popup TW"
        const val EVENT_CANCEL_FINISH_TEST_TW = "close finish test popup TW"

        // Practice Window
        const val EVENT_CLICK_HINT_PW = "click hint PW"
        const val EVENT_CLICK_CHECK_PW = "click check PW"
        const val EVENT_CLICK_SOLUTIONS_PW = "click solutions PW"
        const val EVENT_CLICK_NEXT_PW = "click next PW"
        const val EVENT_FINISH_PRACTICE_WINDOW_PW = "finish practice window PW"
        const val EVENT_FINISH_PRACITCE_POPUP_PW = "finish practice popup PW"
        const val EVENT_CANCEL_FINISH_PRACTICE_PW = "close finish practice popup PW"
        const val EVENT_ANSWER_CHOICE_PW = "answer choice PW"
        const val EVENT_CLICK_QUESTION_PW = "click question PW"

        // Learn window
        const val EVENT_SEARCH_BAR = "search bar"
        const val EVENT_SEARCH_BAR_RESULT = "search bar result"

        // Study Window
        const val EVENT_PAGE_LOAD = "page load"
        const val EVENT_BROWSE_CONTENT = "browse content"
        const val EVENT_START_VIDEO = "start video"
        const val EVENT_PLAY_PAUSE_VIDEO = "play/pause video"
        const val EVENT_VIEW_CONTENT_DOCUMENT = "view content document"
        const val EVENT_BACK_FROM_CONTENT_DOCUMENT = "back from content document"
        const val EVENT_BACK_FROM_VIDEO = "back from video"
        const val EVENT_BROWSE_BACK = "browse back"

        //    Payment Events
        const val EVENT_PAYMENT_SUCCESSFULL_AW = "payment successfull AW"
        const val EVENT_PAYMENT_UNSUCCESSFULL_AW = "payment unSuccessfull AW"
        const val NAV_ELEMENT_POPUP = "popup"
        const val NAV_ELEMENT_HEADER = "header"
        const val NAV_ELEMENT_FOOTER = "footer"
        const val NAV_ELEMENT_SIDEBAR = "sidebar"
        const val NAV_ELEMENT_LOGIN_WINDOW = "login_window"
        const val NAV_ELEMENT_MENU = "menu"
        const val NAV_ELEMENT_TEST_WINDOW = "test_window"
        const val NAV_ELEMENT_PRACTICE_WINDOW = "practice_window"
        const val NAV_ELEMENT_BODY_WINDOW = "body"
        const val NAV_ELEMENT_BACK_BUTTON = "back_button"
        const val NAV_ELEMENT_SPLASH_WINDOW = "splash_window"
        const val NAV_ELEMENT_MAIN_WINDOW = "main_window"
        const val NAV_ELEMENT_PROFILE_WINDOW = "profile_window"
        const val NAV_ELEMENT_ACCOUNT_WINDOW = "account_window"
        const val NAV_ELEMENT_DROPDOWN = "search bar dropdown"
        const val NAV_ELEMENT_SIDE_TILE = "side tile"


        /*event types*/
        const val EVENT_TYPE_CLICK = "click"
        const val EVENT_TYPE_FOCUS = "focus"
        const val EVENT_TYPE_LOAD = "load"
        const val EVENT_TYPE_SCROLL = "scroll"
        const val EVENT_TYPE_SLIDE_CHANGE = "slide-change"
        const val EVENT_TYPE_AUTO_TIMER = "auto-timer"
        const val EVENT_TYPE_ERROR = "error"
        const val EVENT_TYPE_BUFFER = "buffer"
        const val EVENT_TYPE_EXIT = "exit"
        const val EVENT_TYPE_SEEK = "seek"
        const val EVENT_TYPE_BODY = "body"
        const val NAV_ELEMENT_OPTIONS = "options"


        /*Nav elements*/
        const val NAV_ELEMENT_HEROBANNER = "heronbanner"
        const val NAV_ELEMENT_TILE_CAROUSEL = "tile_carousel"
        const val NAV_ELEMENT_ADVERSTIMENT_BANNER = "adverstiment banner"
        const val NAV_ELEMENT_NAVIGATION_BAR = "navigation bar"
        const val NAV_ELEMENT_BODY = "body"
        const val NAV_ELEMENT_BOX = "box"
        const val NAV_ELEMENT_TILE = "tile"
        const val NAV_ELEMENT_KEYBOARD = "keyboard"
        const val NAV_ELEMENT_QUICK_LINK = "quick link"
        const val NAV_ELEMENT_BUTTON = "button"
        const val NAV_ELEMENT_ICON = "icon"
        const val NAV_ELEMENT_NEXT_VIDEO_TILE = "next_video_tile"
        const val NAV_ELEMENT_RECOMMENDED_VIDEO_TILE = "recommended_video_tile"
        const val NAV_ELEMENT_REPLAY_BUTTON = "replay_button"
        const val NAV_ELEMENT_REPLAY_ERROR_BUTTON = "replay_error_button"
        const val NAV_ELEMENT_SUBJECT_TILE = "subject tile"
        const val NAV_ELEMENT_WIDGET = "widget"
        const val NAV_ELEMENT_BUCKET = "bucket"


        /* more info*/
        const val EVENT_MORE_INFO_SCREEN_LOAD_START = "moreinfo screen load start"
        const val EVENT_MORE_INFO_SCREEN_LOAD_END = "moreinfo screen load end"
        const val EVENT_MORE_INFO_MAIN_BUTTON_FOCUS = "moreinfo main button focus"
        const val EVENT_MORE_INFO_MAIN_BUTTON_CLICK = "moreinfo main button click"
        const val EVENT_MORE_INFO_BOOKMARK_FOCUS = "moreinfo bookmark focus"
        const val EVENT_MORE_INFO_BOOKMARK_CLICK = "moreinfo bookmark click"
        const val EVENT_MORE_INFO_LIKE_FOCUS = "moreinfo like focus"
        const val EVENT_MORE_INFO_LIKE_CLICK = "moreinfo like click"
        const val EVENT_MORE_INFO_MENU_ITEM_TILE_FOCUS = "more info menu item tile focus"
        const val EVENT_MORE_INFO_MENU_ITEM_TILE_CLICK = "more info menu item tile click"
        const val EVENT_MORE_INFO_MENU_RESULT_LOAD_START = "more info menu result load start"
        const val EVENT_MORE_INFO_MENU_RESULT_LOAD_END = "more info menu result load end"
        const val NAV_ELEMENT_BANNER = "banner"
        const val EVENT_TYPE_LOAD_END = "load end"
        const val EVENT_TYPE_LOAD_START = "load start"

        /* search*/
        const val EVENT_SEARCH_SCREEN_LOAD_START = "search screen load start"
        const val EVENT_SEARCH_SCREEN_LOAD_END = "search screen load end"
        const val EVENT_SEARCH_KEYBOARD_KEY_FOCUS = "keyboard keyfocus"
        const val EVENT_SEARCH_KEYBOARD_KEY_PRESS = "keyboard keypress"
        const val EVENT_SEARCH_RESULT_LOAD_START = "search result load start"
        const val EVENT_SEARCH_RESULT_LOAD_END = "search result load end"
        const val EVENT_SEARCH_TILE_FOCUS = "search tile focus"
        const val EVENT_SEARCH_TILE_CLICK = "search tile click"
        const val EVENT_SEARCH_QUICK_LINK_FOCUS = "quick link focus"
        const val EVENT_SEARCH_QUICK_LINK_CLICK = "quick link click"
        const val EVENT_SEARCH_BACK_BUTTON_CLICK = "search back button click"

        /* grade-board Screen  */
        const val EVENT_GRADE_BOARD_SCREEN_LOAD_START = "add-user screen start"
        const val EVENT_GRADE_BOARD_SCREEN_LOAD_END = "add-user screen end"
        const val EVENT_GRADE_BOARD_SCREEN_BOARD_CLICK = "select board click"
        const val EVENT_GRADE_BOARD_SCREEN_GRADE_CLICK = "select grade click"
        const val EVENT_GRADE_BOARD_SCREEN_CHOOSE_AVATAR_CLICK = "choose avatar click"
        const val EVENT_GRADE_BOARD_SCREEN_ADD_EXAMS_CLICK = "add exams click"
        const val EVENT_GRADE_BOARD_SCREEN_SAVE_CLICK = "save click"
        const val EVENT_GRADE_BOARD_SCREEN_CANCEL_CLICK = "cancel click"
        const val EVENT_GRADE_BOARD_SCREEN_CANCEL_FOCUS = "cancel focus"

        /*add - goals screen*/
        const val EVENT_ADD_GOALS_SCREEN_LOAD_START = "goal screen load start"
        const val EVENT_ADD_GOALS_SCREEN_LOAD_END = "goal screen load end"
        const val EVENT_ADD_GOALS_SCREEN_OPTIONS_CLICK = "select options click"
        const val EVENT_ADD_GOALS_SCREEN_OPTIONS_FOCUS = "select options focus"
        const val EVENT_ADD_GOALS_SCREEN_DONE_CLICK = "done cta"

        /*parent-profile screen*/
        const val EVENT_PARENT_PROFILE_SCREEN_LOAD_START = "setting start screen"
        const val EVENT_PARENT_PROFILE_SCREEN_LOAD_END = "setting screen"
        const val EVENT_PARENT_PROFILE_SCREEN_ICON_FOCUS = "profile select click"
        const val EVENT_PARENT_PROFILE_SCREEN_ADD_CLICK = "add profile click"
        const val EVENT_PARENT_PROFILE_SCREEN_LOGOUT_CLICK = "logout click"
        const val EVENT_PARENT_PROFILE_SCREEN_LOGOUT_FOCUS = "logout focus"
        const val EVENT_PARENT_PROFILE_SCREEN_EDIT_CLICK = "EDIT CLICK"

        /*add-user screen*/
        const val EVENT_ADD_USER_SCREEN_LOAD_START = "adduser screen load start"
        const val EVENT_ADD_USER_SCREEN_LOAD_END = "adduser screen load end"
        const val EVENT_ADD_USER_SCREEN_ENTRY_BOX_CLICK = "entry box click"
        const val EVENT_ADD_USER_SCREEN_ENTRY_BOX_FOCUS = "entry box focus"
        const val EVENT_ADD_USER_SCREEN_ADD_CLICK = "add"

        const val EVENT_PROFILE_NAME_CLICK = "profile name click"
        const val EVENT_PROFILE_ROLE_CLICK = "role click"
        const val EVENT_PROFILE_ROLE_FOCUS = "role focus"
        const val EVENT_PROFILE_EMAIL_CLICK = "email click"
        const val EVENT_PROFILE_CONTINUE_CLICK = "1continue click"
        const val EVENT_PROFILE_CONTINUE_FOCUS = "1continue focus"
        const val EVENT_PROFILE_CANCEL_CLICK = "1cancel click"
        const val EVENT_PROFILE_CANCEL_FOCUS = "1cancel focus"

        const val EVENT_PROFILE_PRIMARY_SEC_SCREEN_LOAD_START =
            "select primary/secondary goal screen load start"
        const val EVENT_PROFILE_PRIMARY_SEC_SCREEN_LOAD_END =
            "select primary/secondary goal screen load end"
        const val EVENT_PROFILE_PRIMARY_GOAL_VALUE_FOCUS = "select primary goal value focus"
        const val EVENT_PROFILE_PRIMARY_GOAL_VALUE_CLICK = "select primary goal value click"
        const val EVENT_PROFILE_EXAM_PRIMARY_GOAL_VALUE_FOCUS =
            "select exam primary goal value focus"
        const val EVENT_PROFILE_EXAM_PRIMARY_GOAL_VALUE_CLICK =
            "select exam primary goal value click"
        const val EVENT_PROFILE_EXAM_PRIMARY_GOAL_DONE_FOCUS = "exam primary goal done focus "
        const val EVENT_PROFILE_EXAM_PRIMARY_GOAL_DONE_CLICK = "exam primary goal done click"
        const val EVENT_PROFILE_SECONDARY_GOAL_VALUE_FOCUS = "select secondary goal value focus"
        const val EVENT_PROFILE_SECONDARY_GOAL_VALUE_CLICK = "select secondary goal value click"
        const val EVENT_PROFILE_SECONDARY_GOAL_CONTINUE_FOCUS = "secondary goal continue focus"
        const val EVENT_PROFILE_SECONDARY_GOAL_CONTINUE_CLICK = "secondary goal continue click"
        const val EVENT_PROFILE_SECONDARY_GOAL_SKIP_FOCUS = "secondary goal skip focus"
        const val EVENT_PROFILE_SECONDARY_GOAL_SKIP_CLICK = "secondary goal skip click"
        const val EVENT_PROFILE_SECONDARY_GOAL_BACK_FOCUS = "secondary goal back focus"
        const val EVENT_PROFILE_SECONDARY_GOAL_BACK_CLICK = "secondary goal back click"
        const val EVENT_PROFILE_EXAMS_SECONDARY_GOAL_VALUE_FOCUS =
            "select exams secondary goal value focus"
        const val EVENT_PROFILE_EXAMS_SECONDARY_GOAL_VALUE_CLICK =
            "select exams secondary goal value click"
        const val EVENT_PROFILE_EXAMS_SECONDARY_GOAL_DONE_FOCUS = "exams secondary goal done focus"
        const val EVENT_PROFILE_EXAMS_SECONDARY_GOAL_DONE_CLICK = "exams secondary goal done click"
        const val EVENT_PROFILE_EXAMS_SECONDARY_GOAL_BACK_FOCUS = "exams secondary goal back focus"
        const val EVENT_PROFILE_EXAMS_SECONDARY_GOAL_BACK_CLICK = "exams secondary goal back click"
        const val EVENT_PROFILE_GOAL_CHANGE_TOGGLE_TILE_FOCUS = "goal change toggle tile focus"
        const val EVENT_PROFILE_GOAL_CHANGE_TOGGLE_TILE_CLICK = "goal change toggle tile click"
        const val EVENT_PROFILE_GOAL_CHANGE_TOGGLE_SCREEN_LOAD_START =
            "goal change toggle screen load start"
        const val EVENT_PROFILE_GOAL_CHANGE_TOGGLE_SCREEN_LOAD_END =
            "goal change toggle screen load end"
        const val EVENT_PROFILE_GOAL_CHANGE_TOGGLE_CONTENT_FOCUS =
            "goal change toggle content focus"
        const val EVENT_PROFILE_GOAL_CHANGE_TOGGLE_CONTENT_CLICK =
            "goal change toggle content click"
        const val EVENT_PROFILE_GOAL_CHANGE_TOGGLE_DONE_FOCUS = "goal change toggle done focus"
        const val EVENT_PROFILE_GOAL_CHANGE_TOGGLE_DONE_CLICK = "goal change toggle done click"


        /*Onboarding screen*/
        const val EVENT_ONBOARDING_SCREEN_LOAD_START = "onboard screen load start"
        const val EVENT_ONBOARDING_SCREEN_LOAD_END = "onboard screen load end"
        const val EVENT_ONBOARDING_SCREEN_RIGHT_BUTTON = "onboard right button"
        const val EVENT_ONBOARDING_SCREEN_LEFT_BUTTON = "onboard left button"

        /*Crash screen*/
        const val EVENT_ERROR_CRASH = "crash screen"

        /*Error screen*/
        const val EVENT_ERROR_LOAD_START = "error load start"
        const val EVENT_ERROR_LOAD_END = "error load end"
        const val EVENT_ERROR_CTA_CLICK = "error cta click"

        /*New Signup screen*/
        const val EVENT_SIGNIN_SCREEN_LOAD_START = "sign-in screen load start "
        const val EVENT_SIGNIN_SCREEN_LOAD_END = "sign-in screen load end "
        const val EVENT_SIGNIN_GET_OPT_CLICK = "sign-in get otp click "
        const val EVENT_SIGNIN_LOGIN_WITH_JIO_CLICK = "sign-in login with jio click "
        const val EVENT_SIGNIN_TERMS_CONDITION_CLICK = "sign-in terms and conditions click "
        const val EVENT_SIGNIN_CLEAR_OTP_CLICK = "clear otp button click "

        /*Exam Goal screen*/
        const val EVENT_SIGNIN_GOAL_SELECTION_DONE_CLICK = "sign-in goal selection done click "

        /*Term and condition screen*/
        const val EVENT_TERM_CONDITION_SCREEN_LOAD_START = "terms and condition screen load start"
        const val EVENT_TERM_CONDITION_SCREEN_LOAD_END = "terms and condition screen load start"
        const val EVENT_TERM_CONDITION_SCREEN_BACK_BTN_PRESS =
            "terms and condition back button press"
        const val EVENT_TERM_CONDITION_SCREEN_SCROLL = "terms and condition scroll"

        /*test landing */
        const val EVENT_TEST_SCREEN_LOAD_START = "screen load start"
        const val EVENT_TEST_SCREEN_LOAD_END = "screen load end"
        const val EVENT_TEST_SCREEN_HEROBANNER_MAIN_BUTTON_FOCUS = "herobanner main button focus"
        const val EVENT_TEST_SCREEN_HEROBANNER_MAIN_BUTTON_CLICK = "herobanner main button click"
        const val EVENT_TEST_SCREEN_TEST_TILE_FOCUS = "test tile focus"
        const val EVENT_TEST_SCREEN_TEST_TILE_CLICK = "test tile click"
        const val EVENT_TEST_SCREEN_ADVERSTIMENT_BANNER_CTA_FOCUS = "adverstiment banner cta focus"
        const val EVENT_TEST_SCREEN_ADVERSTIMENT_BANNER_CTA_CLICK = "adverstiment banner cta click"
        const val EVENT_TEST_SCREEN_CREATE_CUSTOM_TEST_FOCUS = "create_customtest focus"
        const val EVENT_TEST_SCREEN_CREATE_CUSTOM_TEST_CLICK = "create_customtest click"

        /*    test instruction */
        const val EVENT_TEST_INSTRUCTION_SCREEN_LOAD_START = "screen load start"
        const val EVENT_TEST_INSTRUCTION_SCREEN_LOAD_END = "screen load end"
        const val EVENT_TEST_INSTRUCTION_SCREEN_SWAP_CTA_FOCUS = "screen swap cta focus"
        const val EVENT_TEST_INSTRUCTION_SCREEN_SWAP_CTA_CLICK = "screen swap cta click"
        const val EVENT_TEST_INSTRUCTION_SCREEN_BOX_FOCUS = "instruction box focus"
        const val EVENT_TEST_INSTRUCTION_SCREEN_BOX_CHECK = "instruction box check"
        const val EVENT_TEST_INSTRUCTION_SCREEN_START_NOW_FOCUS = "start now focus"
        const val EVENT_TEST_INSTRUCTION_SCREEN_START_NOW_CLICK = "start now click"

        /*test taking*/
        const val EVENT_TEST_TAKING_SCREEN_LOAD_START = "screen load start"
        const val EVENT_TEST_TAKING_SCREEN_LOAD_END = "screen load end"
        const val EVENT_TEST_TAKING_SCREEN_OPTIONS_FOCUS = "option focus"
        const val EVENT_TEST_TAKING_SCREEN_OPTIONS_CLICK = "option click"
        const val EVENT_TEST_TAKING_SCREEN_DROPDOWN_FOCUS = "dropdown focus"
        const val EVENT_TEST_TAKING_SCREEN_DROPDOWN_CLICK = "dropdown click"
        const val EVENT_TEST_TAKING_SCREEN_DROPDOWN_VALUE_FOCUS = "dropdown value focus"
        const val EVENT_TEST_TAKING_SCREEN_DROPDOWN_VALUE_CLICK = "dropdown value click"
        const val EVENT_TEST_TAKING_SCREEN_SAVE_NEXT_FOCUS = "save and next cta focus"
        const val EVENT_TEST_TAKING_SCREEN_SAVE_NEXT_CLICK = "save and next cta click"
        const val EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_FOCUS = "submit test cta focus"
        const val EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_CLICK = "submit test cta click"
        const val EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_LOAD = "submit test popup load"
        const val EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_TEST_FOCUS =
            "submit test popup submit test focus"
        const val EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_TEST_CLICK =
            "submit test popup submit test click"
        const val EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_CONTINUE_FOCUS =
            "submit test popup continue test focus"
        const val EVENT_TEST_TAKING_SUBMIT_TEST_POPUP_CONTINUE_CLICK =
            "submit test popup continue test click"
        const val EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_EXIT = "submit test popup exit"
        const val EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_FOCUS = "view paper cta focus"
        const val EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_CLICK = "view paper cta click"
        const val EVENT_TEST_TAKING_SCREEN_INSTRUCTIONS_FOCUS = "instruction cta focus"
        const val EVENT_TEST_TAKING_SCREEN_INSTRUCTIONS_CLICK = "instruction cta click"
        const val EVENT_TEST_TAKING_SCREEN_MARKED_REVIEW_FOCUS = "marked for review cta focus"
        const val EVENT_TEST_TAKING_SCREEN_MARKED_REVIEW_CLICK = "marked for review cta click"
        const val EVENT_TEST_TAKING_SCREEN_PREVIOUS_FOCUS = "previous cta focus"
        const val EVENT_TEST_TAKING_SCREEN_PREVIOUS_CLICK = "previous cta click"
        const val EVENT_TEST_TAKING_SCREEN_SECTION_FOCUS = "section cta focus"
        const val EVENT_TEST_TAKING_SCREEN_SECTION_CLICK = "section cta click"
        const val EVENT_TEST_TAKING_SCREEN_ATTEMPT_OVERVIEW_QUESTION_NO_FOCUS =
            "attempt overview question number focus"
        const val EVENT_TEST_TAKING_SCREEN_ATTEMPT_OVERVIEW_QUESTION_NO_CLICK =
            "attempt overview question number click"
        const val EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_FILTER_ATTEMPT_CLICK =
            "view paper filter my attempt types box click"
        const val EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_SECTION_FOCUS = "view paper section cta focus"
        const val EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_SECTION_CLICK = "view paper section cta click"
        const val EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_BACK_FOCUS = "view paper back cta focus"
        const val EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_BACK_CLICK = "view paper back cta click"
        const val EVENT_TEST_TAKING_SCREEN_INSTRUCTION_BACK_FOCUS = "instructions back cta focus"
        const val EVENT_TEST_TAKING_SCREEN_INSTRUCTION_BACK_CLICK = "instructions back cta click"

        /*create test*/
        const val EVENT_CREATE_TEST_SCREEN_LOAD_START = "screen load start"
        const val EVENT_CREATE_TEST_SCREEN_LOAD_END = "screen load end"
        const val EVENT_CREATE_TEST_SCREEN_SELECT_SUBJECTS_FOCUS = "select subjects focus"
        const val EVENT_CREATE_TEST_SCREEN_SELECT_SUBJECT_CLICK = "select subjects click"
        const val EVENT_CREATE_TEST_SCREEN_LEARNT_ON_EMBIBE_FOCUS = "learnt on embibe cta focus"
        const val EVENT_CREATE_TEST_SCREEN_LEARNT_ON_EMBIBE_CLICK = "learnt on embibe cta click"
        const val EVENT_CREATE_TEST_SCREEN_NEXT_FOCUS = "next cta focus"
        const val EVENT_CREATE_TEST_SCREEN_NEXT_CLICK = "next cta click"
        const val EVENT_CREATE_TEST_SCREEN_CHAPTERS_LOAD_START = "select chapters screen load start"
        const val EVENT_CREATE_TEST_SCREEN_CHAPTERS_LOAD_END = "select chapters screen load end"
        const val EVENT_CREATE_TEST_SCREEN_CHAPTERS_SUBJECT_FOCUS = "select chapters subject focus"
        const val EVENT_CREATE_TEST_SCREEN_CHAPTERS_SUBJECT_CLICK = "select chapters subject click"
        const val EVENT_CREATE_TEST_SCREEN_CHAPTERS_LEARNT_FOCUS =
            "chapters you have learnt on embibe focus"
        const val EVENT_CREATE_TEST_SCREEN_CHAPTERS_LEARNT_CLICK =
            "chapters you have learnt on embibe click"
        const val EVENT_CREATE_TEST_SCREEN_CHAPTERS_NEXT_FOCUS = "select chapters next focus"
        const val EVENT_CREATE_TEST_SCREEN_CHAPTERS_NEXT_CLICK = "select chapters next click"
        const val EVENT_CREATE_TEST_SCREEN_CHAPTERS_BACK_FOCUS = "select chapters back focus"
        const val EVENT_CREATE_TEST_SCREEN_CHAPTERS_BACK_CLICK = "select chapters back click"
        const val EVENT_CREATE_TEST_SCREEN_SETTING_SCREEN_LOAD_START =
            "select your test setting screen load start"
        const val EVENT_CREATE_TEST_SCREEN_SETTING_SCREEN_LOAD_END =
            "select your test setting screen load end"
        const val EVENT_CREATE_TEST_SCREEN_TEST_SETTING_DIFFICULTY_LEVEL_FOCUS =
            "select your test setting difficulty level focus"
        const val EVENT_CREATE_TEST_SCREEN_TEST_SETTING_DIFFICULTY_LEVEL_CLICK =
            "select your test setting difficulty level click"
        const val EVENT_CREATE_TEST_SCREEN_TEST_SETTING_DURATION_FOCUS =
            "select your test setting duration focus"
        const val EVENT_CREATE_TEST_SCREEN_TEST_SETTING_DURATION_CLICK =
            "select your test setting duration click"
        const val EVENT_CREATE_TEST_SCREEN_SETTING_CORRECT_ANS_MARKS_FOCUS =
            "select your test setting correct answer marks focus"
        const val EVENT_CREATE_TEST_SCREEN_SETTING_CORRECT_ANS_MARKS_CLICK =
            "select your test setting correct answer marks click"
        const val EVENT_CREATE_TEST_SCREEN_SETTING_INCORRECT_ANS_MARKS_FOCUS =
            "select your test setting incorrect answer marks focus"
        const val EVENT_CREATE_TEST_SCREEN_SETTING_INCORRECT_ANS_MARKS_CLICK =
            "select your test setting incorrect answer marks click"
        const val EVENT_CREATE_TEST_SCREEN_TEST_SETTING_CREATE_TEST_FOCUS =
            "select your test setting create test focus"
        const val EVENT_CREATE_TEST_SCREEN_TEST_SETTING_CREATE_TEST_CLICK =
            "select your test setting create test click"
        const val EVENT_CREATE_TEST_SCREEN_TEST_SETTING_BACK_FOCUS =
            "select your test setting back focus"
        const val EVENT_CREATE_TEST_SCREEN_TEST_SETTING_BACK_CLICK =
            "select your test setting back click"
        const val EVENT_CREATE_TEST_SCREEN_GENERATING_CUSTOM_TEST_LOAD_START =
            "generating your custom test screen load start"
        const val EVENT_CREATE_TEST_SCREEN_GENERATING_CUSTOM_TEST_LOAD_END =
            "generating your custom test screen load end"

        /*test more info*/
        const val EVENT_TEST_MORE_INFO_SCREEN_LOAD_START = "screen load start"
        const val EVENT_TEST_MORE_INFO_SCREEN_LOAD_END = "screen load end"
        const val EVENT_TEST_MORE_INFO_SCREEN_TEST_CARD_FOCUS = "test card focus"
        const val EVENT_TEST_MORE_INFO_SCREEN_START_TEST_CLICK = "start a test cta click"
        const val EVENT_TEST_MORE_INFO_SCREEN_VIEW_FEEDBACK_FOCUS = "view feedback cta focus"
        const val EVENT_TEST_MORE_INFO_SCREEN_VIEW_FEEDBACK_CLICK = "view feedback cta click"
        const val EVENT_TEST_MORE_INFO_SCREEN_BOOKMARK_FOCUS = "bookmark focus"
        const val EVENT_TEST_MORE_INFO_SCREEN_BOOKMARK_CLICK = "bookmark click"

        /*Test Feedback */
        const val EVENT_TEST_FEEDBACK_SCREEN_LOAD_START = "screen load start"
        const val EVENT_TEST_FEEDBACK_SCREEN_LOAD_END = "screen load end"
        const val EVENT_TEST_FEEDBACK_SCREEN_CLICK_TO_WATCH_FOCUS = "click to watch focus"
        const val EVENT_TEST_FEEDBACK_SCREEN_CLICK_TO_WATCH_CLCIK = "click to watch click"
        const val EVENT_TEST_FEEDBACK_SCREEN_SUBJECT_WISE_ANALYSIS_DROPDOWN_CLCIK =
            "subject wise analysis dropdown click"
        const val EVENT_TEST_FEEDBACK_SCREEN_SUBJECT_WISE_ANALYSIS_DROPDOWN_FOCUS =
            "subject wise analysis dropdown focus"
        const val EVENT_TEST_FEEDBACK_SCREEN_SUBJECT_WISE_ANALYSIS_DROPDOWN_VALUE_CLICK =
            "subject wise analysis dropdown value click"
        const val EVENT_TEST_FEEDBACK_SCREEN_SUBJECT_WISE_ANALYSIS_DROPDOWN_VALUE_FOCUS =
            "subject wise analysis dropdown value focus"
        const val EVENT_TEST_FEEDBACK_SCREEN_ATTEMPT_FOCUS = "attempt focus"
        const val EVENT_TEST_FEEDBACK_SCREEN_ATTEMPT_CLICK = "attempt click"
        const val EVENT_TEST_FEEDBACK_SCREEN_ACHIEVE_HIGH_SCORE_WIDGET_FOCUS =
            "achieve high score widget focus"
        const val EVENT_TEST_FEEDBACK_SCREEN_CURRENT_POTENTIAL_WIDGET_CLICK =
            "current potential widget click"

        const val EVENT_TEST_FEEDBACK_SCREEN_REVISION_LIST_VALUE =
            "get your revision list value focus"
        const val EVENT_TEST_FEEDBACK_SCREEN_REVISION_LIST_VALUE_CLICK =
            "get your revision list value click"


        const val EVENT_TEST_FEEDBACK_SCREEN_CHAPTER_WISE_ANALYSIS_LOAD_START = "Chapter wise analysis screen load start"
        const val EVENT_TEST_FEEDBACK_SCREEN_CHAPTER_WISE_ANALYSIS_LOAD_END = "Chapter wise analysis screen load end"
        const val EVENT_TEST_FEEDBACK_SCREEN_CHAPTER_WISE_ANALYSIS_TILE_FOCUS= "Chapter wise analysis screen focus"
        const val EVENT_TEST_FEEDBACK_SCREEN_CHAPTER_WISE_ANALYSIS_TILE_CLICK= "Chapter wise analysis screen click"

        const val EVENT_TEST_FEEDBACK_SCREEN_IMPROVE_TEST_TAKING_STRATEGY_VALUE_FOCUS= "improve test taking strategy value focus"
        const val EVENT_TEST_FEEDBACK_SCREEN_IMPROVE_TEST_TAKING_STRATEGY_VALUE_CLICK= "improve test taking strategy value click"

        const val EVENT_TEST_FEEDBACK_SCREEN_IMPROVE_TEST_TAKING_STRATEGY_VIDEO_FOCUS= "improve test taking strategy video focus"
        const val EVENT_TEST_FEEDBACK_SCREEN_IMPROVE_TEST_TAKING_STRATEGY_VIDEO_CLICK= "improve test taking strategy value click"
        const val EVENT_TEST_FEEDBACK_SCREEN_QUESTION_FOCUS= "question"
        const val EVENT_TEST_FEEDBACK_SCREEN_QUESTION_CLICK= "question"

        const val EVENT_TEST_FEEDBACK_SCREEN_QUESTION_SCREEN_LOAD_START= "Question sceen load"
        const val EVENT_TEST_FEEDBACK_SCREEN_QUESTION_SCREEN_LOAD_END= "Question sceen load"

        const val EVENT_TEST_FEEDBACK_SCREEN_QUESTION_WISE_ANAYLSIS_GRAPH_FOCUS= "questionwise analysis graph"
        const val EVENT_TEST_FEEDBACK_SCREEN_QUESTION_TOGGLE_FOCUS= "question toggle"
        const val EVENT_TEST_FEEDBACK_SCREEN_QUESTION_TOGGLE_CLICK= "question toggle"
        const val EVENT_TEST_FEEDBACK_SCREEN_BACK_TO_MARKS_OVERALL_PERFORMANCE_FOCUS= "back to Marks & Overall Performance"
        const val EVENT_TEST_FEEDBACK_SCREEN_BACK_TO_MARKS_OVERALL_PERFORMANCE_CLICK= "back to Marks & Overall Performance"

        //*****Learn Summary********
        const val EVENT_LEARN_SUMMARY_SCREEN_LOAD_START = "learnsummary screen load start"
        const val EVENT_LEARN_SUMMARY_SCREEN_LOAD_END = "learnsummary screen load end"
        const val EVENT_LEARN_SUMMARY_SCREEN_MAIN_BUTTON_FOCUS = "learnsummary main button focus"
        const val EVENT_LEARN_SUMMARY_SCREEN_MAIN_BUTTON_CLICK = "learnsummary main button click"
        const val EVENT_LEARN_SUMMARY_SCREEN_BOOKMARK_BUTTON_FOCUS =
            "learnsummary bookmark button focus"
        const val EVENT_LEARN_SUMMARY_SCREEN_BOOKMARK_BUTTON_CLICK =
            "learnsummary bookmark button click"
        const val EVENT_LEARN_SUMMARY_SCREEN_LIKE_BUTTON_FOCUS = "learnsummary like button focus"
        const val EVENT_LEARN_SUMMARY_SCREEN_LIKE_BUTTON_CLICK = "learnsummary like button click"
        const val EVENT_LEARN_SUMMARY_SCREEN_AVAILABLE_OPTION_FOCUS =
            "learnsummary available option focus"
        const val EVENT_LEARN_SUMMARY_SCREEN_AVAILABLE_OPTION_CLICK =
            "learnsummary available option click"
        const val EVENT_LEARN_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_START =
            "learnsummary available option content load start"
        const val EVENT_LEARN_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_END =
            "learnsummary available option content load end"
        const val EVENT_LEARN_SUMMARY_SCREEN_KNOWLEDGE_GRAPH_FOCUS =
            "learnsummary knowledge graph focus"
        const val EVENT_LEARN_SUMMARY_SCREEN_KNOWLEDGE_GRAPH_CLICK =
            "learnsummary knowledge graph click"
        const val EVENT_LEARN_SUMMARY_SCREEN_CHECK_PROGRESS_BACK_BUTTON_FOCUS =
            "learnsummary check progress back button focus"
        const val EVENT_LEARN_SUMMARY_SCREEN_CHECK_PROGRESS_BACK_BUTTON_CLICK =
            "learnsummary check progress back button click"
        const val EVENT_LEARN_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_FOCUS =
            "learnsummary sincerity score widget focus"
        const val EVENT_LEARN_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_CLICK =
            "learnsummary sincerity score widget click"
        const val EVENT_LEARN_SUMMARY_SCREEN_ATTEMPT_BUCKET_FOCUS =
            "learnsummary attempt bucket focus"
        const val EVENT_LEARN_SUMMARY_SCREEN_ATTEMPT_BUCKET_CLICK =
            "learnsummary attempt bucket click"
        const val EVENT_LEARN_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_FOCUS =
            "learnsummary attempt bucket description focus"
        const val EVENT_LEARN_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_CLICK =
            "learnsummary attempt bucket description click"
        const val EVENT_LEARN_SUMMARY_SCREEN_TILE_FOCUS = "learnsummary tile focus"
        const val EVENT_LEARN_SUMMARY_SCREEN_TILE_CLICK = "learnsummary tile click"
        const val EVENT_LEARN_SUMMARY_SCREEN_KEYBOARD_KEYFOCUS_FOCUS =
            "learnsummary keyboard keyfocus focus"
        const val EVENT_LEARN_SUMMARY_SCREEN_KEYBOARD_KEYPRESS_CLICK =
            "learnsummary keyboard keypress click"
        const val EVENT_LEARN_SUMMARY_SCREEN_VOICE_SEARCH_CLICK = "learnsummary voice search click"
        const val EVENT_LEARN_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_START =
            "learnsummary search result load start"
        const val EVENT_LEARN_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_END =
            "learnsummary search result load end"

        //******Practice Summary******
        const val EVENT_PRACTICE_SUMMARY_SCREEN_LOAD_START = "practiceSummary screen load start"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_LOAD_END = "practiceSummary screen load end"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_MAIN_BUTTON_FOCUS =
            "practiceSummary main button focus"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_MAIN_BUTTON_CLICK =
            "practiceSummary main button click"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_BOOKMARK_BUTTON_FOCUS =
            "practiceSummary bookmark button focus"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_BOOKMARK_BUTTON_CLICK =
            "practiceSummary bookmark button click"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_LIKE_BUTTON_FOCUS =
            "practiceSummary like button focus"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_LIKE_BUTTON_CLICK =
            "practiceSummary like button click"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_AVAILABLE_OPTION_FOCUS =
            "practiceSummary available option focus"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_AVAILABLE_OPTION_CLICK =
            "practiceSummary available option click"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_START =
            "practiceSummary available option content load start"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_END =
            "practiceSummary available option content load end"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_KNOWLEDGE_GRAPH_FOCUS =
            "practiceSummary knowledge graph focus"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_KNOWLEDGE_GRAPH_CLICK =
            "practiceSummary knowledge graph click"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_CHECK_PROGRESS_BACK_BUTTON_FOCUS =
            "practiceSummary check progress back button focus"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_CHECK_PROGRESS_BACK_BUTTON_CLICK =
            "practiceSummary check progress back button click"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_FOCUS =
            "practiceSummary sincerity score widget focus"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_CLICK =
            "practiceSummary sincerity score widget click"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_ATTEMPT_BUCKET_FOCUS =
            "practiceSummary attempt bucket focus"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_ATTEMPT_BUCKET_CLICK =
            "practiceSummary attempt bucket click"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_FOCUS =
            "practiceSummary attempt bucket description focus"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_CLICK =
            "practiceSummary attempt bucket description click"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_KEYBOARD_KEYFOCUS_FOCUS =
            "practiceSummary keyboard keyfocus focus"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_KEYBOARD_KEYPRESS_CLICK =
            "practiceSummary keyboard keypress click"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_VOICE_SEARCH_CLICK =
            "practiceSummary voice search click"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_START =
            "practiceSummary search result load start"
        const val EVENT_PRACTICE_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_END =
            "practiceSummary search result load end"

        //*****Test Summary**********
        const val EVENT_TEST_SUMMARY_SCREEN_LOAD_START = "testSummary screen load start"
        const val EVENT_TEST_SUMMARY_SCREEN_LOAD_END = "testSummary screen load end"
        const val EVENT_TEST_SUMMARY_SCREEN_MAIN_BUTTON_FOCUS = "testSummary main button focus"
        const val EVENT_TEST_SUMMARY_SCREEN_MAIN_BUTTON_CLICK = "testSummary main button click"
        const val EVENT_TEST_SUMMARY_SCREEN_BOOKMARK_BUTTON_FOCUS =
            "testSummary bookmark button focus"
        const val EVENT_TEST_SUMMARY_SCREEN_BOOKMARK_BUTTON_CLICK =
            "testSummary bookmark button click"
        const val EVENT_TEST_SUMMARY_SCREEN_LIKE_BUTTON_FOCUS = "testSummary like button focus"
        const val EVENT_TEST_SUMMARY_SCREEN_LIKE_BUTTON_CLICK = "testSummary like button click"
        const val EVENT_TEST_SUMMARY_SCREEN_AVAILABLE_OPTION_FOCUS =
            "testSummary available option focus"
        const val EVENT_TEST_SUMMARY_SCREEN_AVAILABLE_OPTION_CLICK =
            "testSummary available option click"
        const val EVENT_TEST_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_START =
            "testSummary available option content load start"
        const val EVENT_TEST_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_END =
            "testSummary available option content load end"
        const val EVENT_TEST_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_FOCUS =
            "testSummary sincerity score widget focus"
        const val EVENT_TEST_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_CLICK =
            "testSummary sincerity score widget click"
        const val EVENT_TEST_SUMMARY_SCREEN_ATTEMPT_BUCKET_FOCUS =
            "testSummary attempt bucket focus"
        const val EVENT_TEST_SUMMARY_SCREEN_ATTEMPT_BUCKET_CLICK =
            "testSummary attempt bucket click"
        const val EVENT_TEST_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_FOCUS =
            "testSummary attempt bucket description focus"
        const val EVENT_TEST_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_CLICK =
            "testSummary attempt bucket description click"
        const val EVENT_TEST_SUMMARY_SCREEN_KEYBOARD_KEYFOCUS_FOCUS =
            "testSummary keyboard keyfocus focus"
        const val EVENT_TEST_SUMMARY_SCREEN_KEYBOARD_KEYPRESS_CLICK =
            "testSummary keyboard keypress click"
        const val EVENT_TEST_SUMMARY_SCREEN_VOICE_SEARCH_CLICK = "testSummary voice search click"
        const val EVENT_TEST_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_START =
            "testSummary search result load start"
        const val EVENT_TEST_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_END =
            "testSummary search result load end"
    }

    init {
        eventCodeMap = HashMap()
        eventCodeMap[EVENT_APP_LAUNCH] = "SW-1"
        eventCodeMap[EVENT_CLICK_CHAPTER_I_DID_NOT_ATTEMPT_TF] = "TF-4"
        eventCodeMap[EVENT_CLICK_CHAPTER_I_DID_NOT_ATTEMPT_TF] = "TF-4"
        eventCodeMap[EVENT_CLICK_CHAPTER_I_GOT_RIGHT_TF] = "TF-5"
        eventCodeMap[EVENT_CLICK_CHAPTERS_I_GOT_WRONG_TF] = "TF-6"
        eventCodeMap[EVENT_CLICK_CHAPTERWISE_ANALYSIS_TF] = "TF-7"
        eventCodeMap[EVENT_CLICK_HOW_DID_YOU_SCORE_TF] = "TF-11"
        eventCodeMap[EVENT_CLICK_HOW_DID_YOU_SPENT_YOUR_TIME_TF] = "TF-12"
        eventCodeMap[EVENT_CLICK_HOW_EFFECTIVE_WERE_YOUR_ATTEMPTS_TF] = "TF-13"
        eventCodeMap[EVENT_CLICK_QUESTION_WISE_ANALYSIS_TF] = "TF-20"
        eventCodeMap[EVENT_CLICK_TEST_TAKING_STRAATEGY_TF] = "TF-31"
        eventCodeMap[EVENT_BEGIN_TEST_TLW] = "TLW-1"
        eventCodeMap[EVENT_SAVE_ANSWER_TW] = "TW-3"
        eventCodeMap[EVENT_REVIEW_LATER_TW] = "TW-4"
        eventCodeMap[EVENT_CLEAR_SELECTION_TW] = "TW-5"
        eventCodeMap[EVENT_SELECT_SUBJECT_TW] = "TW-6"
        eventCodeMap[EVENT_ANSWER_CHOICE_TW] = "TW-7"
        eventCodeMap[EVENT_VIEW_INSTRUCTIONS_TW] = "TW-8"
        eventCodeMap[EVENT_BACK_FROM_INSTRUCTIONS_TW] = "TW-9"
        eventCodeMap[EVENT_VIEW_ALL_QUESTIONS_TW] = "TW-10"
        eventCodeMap[EVENT_BACK_FROM_ALL_QUESTIONS_TW] = "TW-11"
        eventCodeMap[EVENT_FINISH_TEST_WINDOW_TW] = "TW-12"
        eventCodeMap[EVENT_FINISH_TEST_POPUP_TW] = "TW-13"
        eventCodeMap[EVENT_CANCEL_FINISH_TEST_TW] = "TW-14"
        //new codes
        eventCodeMap[EVENT_CLICK_QUESTION_TF] = "TF-23"
        eventCodeMap[EVENT_CLICK_FIRST_LOOK_TF] = "TF-40"
        eventCodeMap[EVENT_CLICK_TEST_FEEDBACK_TLW] = "TLW-10"
        eventCodeMap[EVENT_START_TEST_TLW] = "TLW-11"
        eventCodeMap[EVENT_BACK_FROM_INSTRUCTIONS_TLW] = "TLW-9"
        eventCodeMap[EVENT_VIEW_INSTRUCTIONS_TLW] = "TLW-8"
        eventCodeMap[EVENT_CLICK_DOWNLOAD_PLW] = "PLW-1"
        eventCodeMap[EVENT_CLICK_PRACTICE_FEEDBACK_PLW] = "PLW-3"
        eventCodeMap[EVENT_START_PRACTICE_PLW] = "PLW-4"
        eventCodeMap[EVENT_CLICK_DOWNLOAD_MORE_PLW] = "PLW-5"
        eventCodeMap[EVENT_DOWNLOAD_COMPLETE_PLW] = "PLW-2"
        eventCodeMap[EVENT_ANSWER_CHOICE_PW] = "PW-7"
        eventCodeMap[EVENT_CLICK_QUESTION_PW] = "PW-23"
        eventCodeMap[EVENT_CLICK_QUESTION_TW] = "TW-23"
        eventCodeMap[EVENT_CLICK_HINT_PW] = "PW-10"
        eventCodeMap[EVENT_CLICK_CHECK_PW] = "PW-11"
        eventCodeMap[EVENT_CLICK_SOLUTIONS_PW] = "PW-12"
        eventCodeMap[EVENT_CLICK_NEXT_PW] = "PW-13"
        eventCodeMap[EVENT_FINISH_PRACTICE_WINDOW_PW] = "PW-14"
        eventCodeMap[EVENT_FINISH_PRACITCE_POPUP_PW] = "PW-15"
        eventCodeMap[EVENT_CANCEL_FINISH_PRACTICE_PW] = "PW-16"
        eventCodeMap[EVENT_LOGIN_AW] = "AW-1"
        eventCodeMap[EVENT_LOGIN_SCRN_LOAD_START_AW] = "AW-1"
        eventCodeMap[EVENT_CLICK_LOGIN_AW] = "AW-2"
        eventCodeMap[EVENT_LOGIN_SUCCESS_AW] = "AW-3"
        eventCodeMap[EVENT_LOGIN_FAILED_AW] = "AW-4"
        eventCodeMap[EVENT_SINGNUP_AW] = "AW-5"
        eventCodeMap[EVENT_SINGNUP_FAILED_AW] = "AW-6"
        eventCodeMap[EVENT_VIEW_USERS_AW] = "AW-7"
        eventCodeMap[EVENT_SELECT_USER_AW] = "AW-8"
        /*learn */
        eventCodeMap[EVENT_CONTENT_SELECTION] = "LW-1"
        eventCodeMap[EVENT_SUBJECT_SELECTION] = "LW-2"
        eventCodeMap[EVENT_LOCALE_CHANGED_MW] = "MW-1"
        eventCodeMap[EVENT_GOAL_CHANGED_MW] = "MW-2"
        eventCodeMap[EVENT_EXAM_CHANGED_MW] = "MW-3"
        eventCodeMap[EVENT_CLICK_LEARN_MW] = "MW-4"
        eventCodeMap[EVENT_CLICK_NOTIFICATIONS_MW] = "MW-5"
        eventCodeMap[EVENT_CLICK_SYNC_MW] = "MW-6"
        eventCodeMap[EVENT_MENU_SELECTION] = "MW-7"
        eventCodeMap[EVENT_FIRST_LAUNCH_SW] = "SW-1"
        eventCodeMap[EVENT_CLICK_DOWNLOAD_TLW] = "TLW-12"
        eventCodeMap[EVENT_CANCEL_DOWNLOAD_TLW] = "TLW-14"
        eventCodeMap[EVENT_LOGOUT_PW] = "PRW-1"
        eventCodeMap[EVENT_CANCEL_DOWNLOAD_PLW] = "PLW-6"
        eventCodeMap[EVENT_DOWNLOAD_COMPLETE_TLW] = "TLW-13"
        eventCodeMap[EVENT_PAYMENT_SUCCESSFULL_AW] = "AW-6"
        eventCodeMap[EVENT_PAYMENT_UNSUCCESSFULL_AW] = "AW-7"
        eventCodeMap[EVENT_SEARCH_EXECUTED] = "SW-1"
        eventCodeMap[EVENT_SEARCH_BAR] = "r-lrnp-4"
        eventCodeMap[EVENT_SEARCH_BAR_RESULT] = "r-lrnp-5"
        eventCodeMap[EVENT_PAGE_LOAD] = "r-app-study-1"
        eventCodeMap[EVENT_BROWSE_CONTENT] = "r-app-study-2"
        eventCodeMap[EVENT_VIEW_CONTENT_DOCUMENT] = "r-app-study-3"
        eventCodeMap[EVENT_START_VIDEO] = "r-app-study-4"
        eventCodeMap[EVENT_PLAY_PAUSE_VIDEO] = "r-app-study-5"
        eventCodeMap[EVENT_BACK_FROM_CONTENT_DOCUMENT] = "r-app-study-6"
        eventCodeMap[EVENT_BACK_FROM_VIDEO] = "r-app-study-7"
        eventCodeMap[EVENT_BROWSE_BACK] = "r-app-study-8"
        eventCodeMap[EVENT_CONTENT_BOOKMARKED] = "DW-1"
        eventCodeMap[EVENT_CONTENT_LIKED] = "DW-2"
        eventCodeMap[EVENT_CONTENT_PLAY] = "PW-1"
        eventCodeMap[EVENT_CONTENT_PAUSE] = "PW-2"

        /*splash*/
        eventCodeMap[EVENT_SPLASH_LOAD_START] = "r-jf-splash-1"
        eventCodeMap[EVENT_SPLASH_LOAD_END] = "r-jf-splash-2"

        /*login*/
        eventCodeMap[EVENT_LOGIN_LOAD_START] = "r-jf-login-1"
        eventCodeMap[EVENT_LOGIN_LOAD_END] = "r-jf-login-2"
        eventCodeMap[EVENT_VERIFY_OTP_CLICK] = "r-jf-login-3"
        eventCodeMap[EVENT_RE_SEND_OTP_CLICK] = "r-jf-login-4"
        eventCodeMap[EVENT_LOGIN_SIGNIN_BTN_FOCUS] = "r-jf-login-5"
        eventCodeMap[EVENT_LOGIN_SIGNIN_BTN_CLICK] = "r-jf-login-6"
        eventCodeMap[EVENT_LOGIN_SIGNUP_BTN_FOCUS] = "r-jf-login-7"
        eventCodeMap[EVENT_LOGIN_SIGNUP_BTN_CLICK] = "r-jf-login-8"
        eventCodeMap[EVENT_SCREEN_CHOOSE_AVATAR_LOAD_START] = "r-jf-login-9"
        eventCodeMap[EVENT_SCREEN_CHOOSE_AVATAR_LOAD_END] = "r-jf-login-10"
        eventCodeMap[EVENT_AVATAR_ICON_CLICKED] = "r-jf-login-11"
        eventCodeMap[EVENT_FORGO_PASSWORD_MOBILE_SCREEN_LOAD_START] = "r-jf-login-12"
        eventCodeMap[EVENT_FORGO_PASSWORD_MOBILE_SCREEN_LOAD_END] = "r-jf-login-13"
        eventCodeMap[EVENT_FORGO_PASSWORD_SUBMIT_CLICK] = "r-jf-login-14"
        eventCodeMap[EVENT_FORGO_PASSWORD_BACK_TO_SIGN_IN_CLICK] = "r-jf-login-15"
        eventCodeMap[EVENT_FORGO_PASSWORD_OTP_SCREEN_LOAD_START] = "r-jf-login-16"
        eventCodeMap[EVENT_FORGO_PASSWORD_OTP_SCREEN_LOAD_END] = "r-jf-login-17"
        eventCodeMap[EVENT_FORGO_PASSWORD_VERIFY_OTP_CLICK] = "r-jf-login-18"
        eventCodeMap[EVENT_FORGO_PASSWORD_RESEND_OTP_CLICK] = "r-jf-login-19"
        eventCodeMap[EVENT_FORGO_PASSWORD_RE_ENTER_OTP_CLICK] = "r-jf-login-20"
        eventCodeMap[EVENT_SWITCH_SCREEN_LOAD_START] = "r-jf-login-23"
        eventCodeMap[EVENT_SWITCH_SCREEN_LOAD_END] = "r-jf-login-24"
        eventCodeMap[EVENT_SWITCH_USER_PROFILE_CLICK] = "r-jf-login-25"
        eventCodeMap[EVENT_SWITCH_USER_ADD_USER_CLICK] = "r-jf-login-26"

        /*new signup event code*/
        eventCodeMap[EVENT_SIGNIN_SCREEN_LOAD_START] = "r-jf-login-28"
        eventCodeMap[EVENT_SIGNIN_SCREEN_LOAD_END] = "r-jf-login-29"
        eventCodeMap[EVENT_SIGNIN_GET_OPT_CLICK] = "r-jf-login-30"
        eventCodeMap[EVENT_SIGNIN_LOGIN_WITH_JIO_CLICK] = "r-jf-login-31"
        eventCodeMap[EVENT_SIGNIN_TERMS_CONDITION_CLICK] = "r-jf-login-32"
        eventCodeMap[EVENT_SIGNIN_CLEAR_OTP_CLICK] = "r-jf-login-34"

        /* goal selection event code*/
        eventCodeMap[EVENT_SIGNIN_GOAL_SELECTION_DONE_CLICK] = "r-jf-login-33"

        /*home event codes*/
        eventCodeMap[EVENT_SCREEN_LOAD_START] = "r-jf-hs-1"
        eventCodeMap[EVENT_SCREEN_LOAD_END] = "r-jf-hs-2"
        eventCodeMap[EVENT_HEROBANNER_MAIN_BUTTON_FOCUS] = "r-jf-hs-3"
        eventCodeMap[EVENT_HEROBANNER_MAIN_BUTTON_CLICK] = "r-jf-hs-4"
        eventCodeMap[EVENT_HEROBANNER_MOREINFO_BUTTON_FOCUS] = "r-jf-hs-5"
        eventCodeMap[EVENT_HEROBANNER_MOREINFO_BUTTON_CLICK] = "r-jf-hs-6"
        eventCodeMap[EVENT_HEROBANNER_SLIDE_CHANGE] = "r-jf-hs-7"
        eventCodeMap[EVENT_HOME_TILE_CLICK] = "r-jf-hs-8"
        eventCodeMap[EVENT_HOME_TILE_FOCUS] = "r-jf-hs-9"
        eventCodeMap[EVENT_ADVERSTIMENT_BANNER_CLICK] = "r-jf-hs-10"
        eventCodeMap[EVENT_ADVERSTIMENT_BANNER_FOCUS] = "r-jf-hs-11"
        eventCodeMap[EVENT_NAVIAGTION_BAR_VIEW_OPEN] = "r-jf-hs-12"
        eventCodeMap[EVENT_NAVIAGTION_BAR_VIEW_CLOSE] = "r-jf-hs-13"
        eventCodeMap[EVENT_NAVIGATION_BAR_ITEM_CLICK] = "r-jf-hs-14"
        eventCodeMap[EVENT_NAVIGATION_BAR_ITEM_FOCUS] = "r-jf-hs-15"


        /*player event codes*/
        eventCodeMap[EVENT_CONTENT_LOADING_START] = "r-jf-vp-1"
        eventCodeMap[EVENT_CONTENT_LOADING_END] = "r-jf-vp-2"
        eventCodeMap[EVENT_CONTENT_PLAYBACK_ERROR] = "r-jf-vp-3"
        eventCodeMap[EVENT_CONTENT_PLAY_PAUSE_CLICK] = "r-jf-vp-4"
        eventCodeMap[EVENT_CONTENT_BUFFER_START] = "r-jf-vp-4"
        eventCodeMap[EVENT_CONTENT_BUFFER_END] = "r-jf-vp-5"
        eventCodeMap[EVENT_CONTENT_SEEK] = "r-jf-vp-6"
        eventCodeMap[EVENT_CONTENT_SETTING_CLICK] = "r-jf-vp-9"
        eventCodeMap[EVENT_CONTENT_VOLUME_CLICK] = "r-jf-vp-10"
        eventCodeMap[EVENT_CONTENT_BOOKMARK_CLICK] = "r-jf-vp-11"
        eventCodeMap[EVENT_CONTENT_BOOKMARK_FOCUS] = "r-jf-vp-12"
        eventCodeMap[EVENT_CONTENT_LIKE_CLICK] = "r-jf-vp-13"
        eventCodeMap[EVENT_CONTENT_LIKE_FOCUS] = "r-jf-vp-14"
        eventCodeMap[EVENT_CONTENT_EXIT] = "r-jf-vp-16"

        /*more info codes*/
        eventCodeMap[EVENT_MORE_INFO_SCREEN_LOAD_START] = "r-jf-info-1"
        eventCodeMap[EVENT_MORE_INFO_SCREEN_LOAD_END] = "r-jf-info-2"
        eventCodeMap[EVENT_MORE_INFO_MAIN_BUTTON_FOCUS] = "r-jf-info-3"
        eventCodeMap[EVENT_MORE_INFO_MAIN_BUTTON_CLICK] = "r-jf-info-4"
        eventCodeMap[EVENT_MORE_INFO_BOOKMARK_FOCUS] = "r-jf-info-5"
        eventCodeMap[EVENT_MORE_INFO_BOOKMARK_CLICK] = "r-jf-info-6"
        eventCodeMap[EVENT_MORE_INFO_LIKE_FOCUS] = "r-jf-info-7"
        eventCodeMap[EVENT_MORE_INFO_LIKE_CLICK] = "r-jf-info-8"
        eventCodeMap[EVENT_MORE_INFO_MENU_ITEM_TILE_FOCUS] = "r-jf-info-9"
        eventCodeMap[EVENT_MORE_INFO_MENU_ITEM_TILE_CLICK] = "r-jf-info-10"
        eventCodeMap[EVENT_MORE_INFO_MENU_RESULT_LOAD_START] = "r-jf-info-13"
        eventCodeMap[EVENT_MORE_INFO_MENU_RESULT_LOAD_END] = "r-jf-info-14"

        /* SEARCH */
        eventCodeMap[EVENT_SEARCH_SCREEN_LOAD_START] = "r-jf-search-1"
        eventCodeMap[EVENT_SEARCH_SCREEN_LOAD_END] = "r-jf-search-2"
        eventCodeMap[EVENT_SEARCH_KEYBOARD_KEY_FOCUS] = "r-jf-search-3"
        eventCodeMap[EVENT_SEARCH_KEYBOARD_KEY_PRESS] = "r-jf-search-4"
        eventCodeMap[EVENT_SEARCH_RESULT_LOAD_START] = "r-jf-search-5"
        eventCodeMap[EVENT_SEARCH_RESULT_LOAD_END] = "r-jf-search-6"
        eventCodeMap[EVENT_SEARCH_TILE_FOCUS] = "r-jf-search-8"
        eventCodeMap[EVENT_SEARCH_TILE_CLICK] = "r-jf-search-7"
        eventCodeMap[EVENT_SEARCH_QUICK_LINK_FOCUS] = "r-jf-search-10"
        eventCodeMap[EVENT_SEARCH_QUICK_LINK_CLICK] = "r-jf-search-9"
        eventCodeMap[EVENT_SEARCH_BACK_BUTTON_CLICK] = "r-jf-search-12"

        /* grade-board Screen  */
        eventCodeMap[EVENT_GRADE_BOARD_SCREEN_LOAD_START] = "r-jf-pf-1"
        eventCodeMap[EVENT_GRADE_BOARD_SCREEN_LOAD_END] = "r-jf-pf-2"
        eventCodeMap[EVENT_GRADE_BOARD_SCREEN_BOARD_CLICK] = "r-jf-pf-3"
        eventCodeMap[EVENT_GRADE_BOARD_SCREEN_GRADE_CLICK] = "r-jf-pf-5"
        eventCodeMap[EVENT_GRADE_BOARD_SCREEN_CHOOSE_AVATAR_CLICK] = "r-jf-pf-9"
        eventCodeMap[EVENT_GRADE_BOARD_SCREEN_ADD_EXAMS_CLICK] = "r-jf-pf-11"
        eventCodeMap[EVENT_GRADE_BOARD_SCREEN_SAVE_CLICK] = "r-jf-pf-13"
        eventCodeMap[EVENT_GRADE_BOARD_SCREEN_CANCEL_CLICK] = "r-jf-pf-15"
        eventCodeMap[EVENT_GRADE_BOARD_SCREEN_CANCEL_FOCUS] = "r-jf-pf-16"

        /*add - goals screen*/
        eventCodeMap[EVENT_ADD_GOALS_SCREEN_LOAD_START] = "r-jf-pf-17"
        eventCodeMap[EVENT_ADD_GOALS_SCREEN_LOAD_END] = "r-jf-pf-18"
        eventCodeMap[EVENT_ADD_GOALS_SCREEN_OPTIONS_CLICK] = "r-jf-pf-19"
        eventCodeMap[EVENT_ADD_GOALS_SCREEN_OPTIONS_FOCUS] = "r-jf-pf-20"
        eventCodeMap[EVENT_ADD_GOALS_SCREEN_DONE_CLICK] = "r-jf-pf-21"

        /*parent-profile screen*/
        eventCodeMap[EVENT_PARENT_PROFILE_SCREEN_LOAD_START] = "r-jf-pf-31"
        eventCodeMap[EVENT_PARENT_PROFILE_SCREEN_LOAD_END] = "r-jf-pf-32"
        eventCodeMap[EVENT_PARENT_PROFILE_SCREEN_ICON_FOCUS] = "r-jf-pf-33"
        eventCodeMap[EVENT_PARENT_PROFILE_SCREEN_ADD_CLICK] = "r-jf-pf-35"
        eventCodeMap[EVENT_PARENT_PROFILE_SCREEN_LOGOUT_CLICK] = "r-jf-pf-36"
        eventCodeMap[EVENT_PARENT_PROFILE_SCREEN_LOGOUT_FOCUS] = "r-jf-pf-37"
        eventCodeMap[EVENT_PARENT_PROFILE_SCREEN_EDIT_CLICK] = "r-jf-pf-43"

        /*add-user screen*/
        eventCodeMap[EVENT_ADD_USER_SCREEN_LOAD_START] = "r-jf-pf-38"
        eventCodeMap[EVENT_ADD_USER_SCREEN_LOAD_END] = "r-jf-pf-39"
        eventCodeMap[EVENT_ADD_USER_SCREEN_ENTRY_BOX_CLICK] = "r-jf-pf-40"
        eventCodeMap[EVENT_ADD_USER_SCREEN_ENTRY_BOX_FOCUS] = "r-jf-pf-41"
        eventCodeMap[EVENT_ADD_USER_SCREEN_ADD_CLICK] = "r-jf-pf-42"

        eventCodeMap[EVENT_PROFILE_NAME_CLICK] = "r-jf-pf-48"
        eventCodeMap[EVENT_PROFILE_ROLE_CLICK] = "r-jf-pf-49"
        eventCodeMap[EVENT_PROFILE_ROLE_FOCUS] = "r-jf-pf-50"
        eventCodeMap[EVENT_PROFILE_EMAIL_CLICK] = "r-jf-pf-51"
        eventCodeMap[EVENT_PROFILE_CONTINUE_CLICK] = "r-jf-pf-52"
        eventCodeMap[EVENT_PROFILE_CONTINUE_FOCUS] = "r-jf-pf-53"
        eventCodeMap[EVENT_PROFILE_CANCEL_CLICK] = "r-jf-pf-54"
        eventCodeMap[EVENT_PROFILE_CANCEL_FOCUS] = "r-jf-pf-55"

        eventCodeMap[EVENT_PROFILE_PRIMARY_SEC_SCREEN_LOAD_START] = "r-jf-pf-56"
        eventCodeMap[EVENT_PROFILE_PRIMARY_SEC_SCREEN_LOAD_END] = "r-jf-pf-57"
        eventCodeMap[EVENT_PROFILE_PRIMARY_GOAL_VALUE_FOCUS] = "r-jf-pf-58"
        eventCodeMap[EVENT_PROFILE_PRIMARY_GOAL_VALUE_CLICK] = "r-jf-pf-59"
        eventCodeMap[EVENT_PROFILE_EXAM_PRIMARY_GOAL_VALUE_FOCUS] = "r-jf-pf-60"
        eventCodeMap[EVENT_PROFILE_EXAM_PRIMARY_GOAL_VALUE_CLICK] = "r-jf-pf-61"
        eventCodeMap[EVENT_PROFILE_EXAM_PRIMARY_GOAL_DONE_FOCUS] = "r-jf-pf-62"
        eventCodeMap[EVENT_PROFILE_EXAM_PRIMARY_GOAL_DONE_CLICK] = "r-jf-pf-63"
        eventCodeMap[EVENT_PROFILE_SECONDARY_GOAL_VALUE_FOCUS] = "r-jf-pf-64"
        eventCodeMap[EVENT_PROFILE_SECONDARY_GOAL_VALUE_CLICK] = "r-jf-pf-65"
        eventCodeMap[EVENT_PROFILE_SECONDARY_GOAL_CONTINUE_FOCUS] = "r-jf-pf-66"
        eventCodeMap[EVENT_PROFILE_SECONDARY_GOAL_CONTINUE_CLICK] = "r-jf-pf-67"
        eventCodeMap[EVENT_PROFILE_SECONDARY_GOAL_SKIP_FOCUS] = "r-jf-pf-68"
        eventCodeMap[EVENT_PROFILE_SECONDARY_GOAL_SKIP_CLICK] = "r-jf-pf-69"
        eventCodeMap[EVENT_PROFILE_SECONDARY_GOAL_BACK_FOCUS] = "r-jf-pf-70"
        eventCodeMap[EVENT_PROFILE_SECONDARY_GOAL_BACK_CLICK] = "r-jf-pf-71"
        eventCodeMap[EVENT_PROFILE_EXAMS_SECONDARY_GOAL_VALUE_FOCUS] = "r-jf-pf-72"
        eventCodeMap[EVENT_PROFILE_EXAMS_SECONDARY_GOAL_VALUE_CLICK] = "r-jf-pf-73"
        eventCodeMap[EVENT_PROFILE_EXAMS_SECONDARY_GOAL_DONE_FOCUS] = "r-jf-pf-74"
        eventCodeMap[EVENT_PROFILE_EXAMS_SECONDARY_GOAL_DONE_CLICK] = "r-jf-pf-75"
        eventCodeMap[EVENT_PROFILE_EXAMS_SECONDARY_GOAL_BACK_FOCUS] = "r-jf-pf-76"
        eventCodeMap[EVENT_PROFILE_EXAMS_SECONDARY_GOAL_BACK_CLICK] = "r-jf-pf-77"
        eventCodeMap[EVENT_PROFILE_GOAL_CHANGE_TOGGLE_TILE_FOCUS] = "r-jf-pf-78"
        eventCodeMap[EVENT_PROFILE_GOAL_CHANGE_TOGGLE_TILE_CLICK] = "r-jf-pf-79"
        eventCodeMap[EVENT_PROFILE_GOAL_CHANGE_TOGGLE_SCREEN_LOAD_START] = "r-jf-pf-80"
        eventCodeMap[EVENT_PROFILE_GOAL_CHANGE_TOGGLE_SCREEN_LOAD_END] = "r-jf-pf-81"
        eventCodeMap[EVENT_PROFILE_GOAL_CHANGE_TOGGLE_CONTENT_FOCUS] = "r-jf-pf-82"
        eventCodeMap[EVENT_PROFILE_GOAL_CHANGE_TOGGLE_CONTENT_CLICK] = "r-jf-pf-83"
        eventCodeMap[EVENT_PROFILE_GOAL_CHANGE_TOGGLE_DONE_FOCUS] = "r-jf-pf-84"
        eventCodeMap[EVENT_PROFILE_GOAL_CHANGE_TOGGLE_DONE_CLICK] = "r-jf-pf-85"


        /*onboarding screen*/
        eventCodeMap[EVENT_ONBOARDING_SCREEN_LOAD_START] = "r-jf-ob-1"
        eventCodeMap[EVENT_ONBOARDING_SCREEN_LOAD_END] = "r-jf-ob-2"
        eventCodeMap[EVENT_ONBOARDING_SCREEN_RIGHT_BUTTON] = "r-jf-ob-3"
        eventCodeMap[EVENT_ONBOARDING_SCREEN_LEFT_BUTTON] = "r-jf-ob-4"

        /*Crash screen*/
        eventCodeMap[EVENT_ERROR_CRASH] = "r-jf-cr-1"

        /*Error screen*/
        eventCodeMap[EVENT_ERROR_LOAD_START] = "r-jf-error-1"
        eventCodeMap[EVENT_ERROR_LOAD_END] = "r-jf-error-2"
        eventCodeMap[EVENT_ERROR_CTA_CLICK] = "r-jf-error-3"

        /* Term and Condition events*/
        eventCodeMap[EVENT_TERM_CONDITION_SCREEN_LOAD_START] = "r-jf-tc_01"
        eventCodeMap[EVENT_TERM_CONDITION_SCREEN_LOAD_END] = "r-jf-tc_02"
        eventCodeMap[EVENT_TERM_CONDITION_SCREEN_BACK_BTN_PRESS] = "r-jf-tc_03"
        eventCodeMap[EVENT_TERM_CONDITION_SCREEN_SCROLL] = "r-jf-tc_04"

        /*test*/
        eventCodeMap[EVENT_TEST_SCREEN_LOAD_START] = "r-jf-tl-1"
        eventCodeMap[EVENT_TEST_SCREEN_LOAD_END] = "r-jf-tl-2"
        eventCodeMap[EVENT_TEST_SCREEN_HEROBANNER_MAIN_BUTTON_FOCUS] = "r-jf-tl-3"
        eventCodeMap[EVENT_TEST_SCREEN_HEROBANNER_MAIN_BUTTON_CLICK] = "r-jf-tl-4"
        eventCodeMap[EVENT_TEST_SCREEN_TEST_TILE_FOCUS] = "r-jf-tl-5"
        eventCodeMap[EVENT_TEST_SCREEN_TEST_TILE_CLICK] = "r-jf-tl-6"
        eventCodeMap[EVENT_TEST_SCREEN_ADVERSTIMENT_BANNER_CTA_FOCUS] = "r-jf-tl-7"
        eventCodeMap[EVENT_TEST_SCREEN_ADVERSTIMENT_BANNER_CTA_CLICK] = "r-jf-tl-8"
        eventCodeMap[EVENT_TEST_SCREEN_CREATE_CUSTOM_TEST_FOCUS] = "r-jf-tl-9"
        eventCodeMap[EVENT_TEST_SCREEN_CREATE_CUSTOM_TEST_CLICK] = "r-jf-tl-10"
        eventCodeMap[EVENT_TEST_INSTRUCTION_SCREEN_LOAD_START] = "r-jf-ti-1"
        eventCodeMap[EVENT_TEST_INSTRUCTION_SCREEN_LOAD_END] = "r-jf-ti-2"
        eventCodeMap[EVENT_TEST_INSTRUCTION_SCREEN_SWAP_CTA_FOCUS] = "r-jf-ti-3"
        eventCodeMap[EVENT_TEST_INSTRUCTION_SCREEN_SWAP_CTA_CLICK] = "r-jf-ti-4"
        eventCodeMap[EVENT_TEST_INSTRUCTION_SCREEN_BOX_FOCUS] = "r-jf-ti-5"
        eventCodeMap[EVENT_TEST_INSTRUCTION_SCREEN_BOX_CHECK] = "r-jf-ti-6"
        eventCodeMap[EVENT_TEST_INSTRUCTION_SCREEN_START_NOW_FOCUS] = "r-jf-ti-7"
        eventCodeMap[EVENT_TEST_INSTRUCTION_SCREEN_START_NOW_CLICK] = "r-jf-ti-8"

        eventCodeMap[EVENT_TEST_TAKING_SCREEN_LOAD_START] = "r-jf-tt-1"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_LOAD_END] = "r-jf-tt-2"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_OPTIONS_FOCUS] = "r-jf-tt-3"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_OPTIONS_CLICK] = "r-jf-tt-4"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_DROPDOWN_FOCUS] = "r-jf-tt-5"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_DROPDOWN_CLICK] = "r-jf-tt-6"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_DROPDOWN_VALUE_FOCUS] = "r-jf-tt-7"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_DROPDOWN_VALUE_CLICK] = "r-jf-tt-8"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_SAVE_NEXT_FOCUS] = "r-jf-tt-9"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_SAVE_NEXT_CLICK] = "r-jf-tt-10"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_FOCUS] = "r-jf-tt-11"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_CLICK] = "r-jf-tt-12"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_LOAD] = "r-jf-tt-13"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_TEST_FOCUS] = "r-jf-tt-14"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_TEST_CLICK] = "r-jf-tt-15"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_CONTINUE_FOCUS] = "r-jf-tt-16"
        eventCodeMap[EVENT_TEST_TAKING_SUBMIT_TEST_POPUP_CONTINUE_CLICK] = "r-jf-tt-17"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_EXIT] = "r-jf-tt-18"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_FOCUS] = "r-jf-tt-19"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_CLICK] = "r-jf-tt-20"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_INSTRUCTIONS_FOCUS] = "r-jf-tt-21"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_INSTRUCTIONS_CLICK] = "r-jf-tt-22"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_MARKED_REVIEW_FOCUS] = "r-jf-tt-23"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_MARKED_REVIEW_CLICK] = "r-jf-tt-24"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_PREVIOUS_FOCUS] = "r-jf-tt-25"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_PREVIOUS_CLICK] = "r-jf-tt-26"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_SECTION_FOCUS] = "r-jf-tt-27"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_SECTION_CLICK] = "r-jf-tt-28"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_ATTEMPT_OVERVIEW_QUESTION_NO_FOCUS] = "r-jf-tt-29"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_ATTEMPT_OVERVIEW_QUESTION_NO_CLICK] = "r-jf-tt-30"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_FILTER_ATTEMPT_CLICK] = "r-jf-tt-31"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_SECTION_FOCUS] = "r-jf-tt-32"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_SECTION_CLICK] = "r-jf-tt-33"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_BACK_FOCUS] = "r-jf-tt-34"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_BACK_CLICK] = "r-jf-tt-35"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_INSTRUCTION_BACK_FOCUS] = "r-jf-tt-36"
        eventCodeMap[EVENT_TEST_TAKING_SCREEN_INSTRUCTION_BACK_CLICK] = "r-jf-tt-37"

        eventCodeMap[EVENT_CREATE_TEST_SCREEN_LOAD_START] = "r-jf-ct-1"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_LOAD_END] = "r-jf-ct-2"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_SELECT_SUBJECTS_FOCUS] = "r-jf-ct-3"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_SELECT_SUBJECT_CLICK] = "r-jf-ct-4"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_LEARNT_ON_EMBIBE_FOCUS] = "r-jf-ct-5"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_LEARNT_ON_EMBIBE_CLICK] = "r-jf-ct-6"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_NEXT_FOCUS] = "r-jf-ct-7"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_NEXT_CLICK] = "r-jf-ct-8"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_CHAPTERS_LOAD_START] = "r-jf-ct-9"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_CHAPTERS_LOAD_END] = "r-jf-ct-10"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_CHAPTERS_SUBJECT_FOCUS] = "r-jf-ct-11"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_CHAPTERS_SUBJECT_CLICK] = "r-jf-ct-12"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_CHAPTERS_LEARNT_FOCUS] = "r-jf-ct-13"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_CHAPTERS_LEARNT_CLICK] = "r-jf-ct-14"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_CHAPTERS_NEXT_FOCUS] = "r-jf-ct-15"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_CHAPTERS_NEXT_CLICK] = "r-jf-ct-16"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_CHAPTERS_BACK_FOCUS] = "r-jf-ct-17"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_CHAPTERS_BACK_CLICK] = "r-jf-ct-18"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_SETTING_SCREEN_LOAD_START] = "r-jf-ct-19"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_SETTING_SCREEN_LOAD_END] = "r-jf-ct-20"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_TEST_SETTING_DIFFICULTY_LEVEL_FOCUS] = "r-jf-ct-21"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_TEST_SETTING_DIFFICULTY_LEVEL_CLICK] = "r-jf-ct-22"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_TEST_SETTING_DURATION_FOCUS] = "r-jf-ct-23"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_TEST_SETTING_DURATION_CLICK] = "r-jf-ct-24"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_SETTING_CORRECT_ANS_MARKS_FOCUS] = "r-jf-ct-25"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_SETTING_CORRECT_ANS_MARKS_CLICK] = "r-jf-ct-26"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_SETTING_INCORRECT_ANS_MARKS_FOCUS] = "r-jf-ct-27"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_SETTING_INCORRECT_ANS_MARKS_CLICK] = "r-jf-ct-28"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_TEST_SETTING_CREATE_TEST_FOCUS] = "r-jf-ct-29"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_TEST_SETTING_CREATE_TEST_CLICK] = "r-jf-ct-30"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_TEST_SETTING_BACK_FOCUS] = "r-jf-ct-31"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_TEST_SETTING_BACK_CLICK] = "r-jf-ct-32"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_GENERATING_CUSTOM_TEST_LOAD_START] = "r-jf-ct-33"
        eventCodeMap[EVENT_CREATE_TEST_SCREEN_GENERATING_CUSTOM_TEST_LOAD_END] = "r-jf-ct-34"

        eventCodeMap[EVENT_TEST_MORE_INFO_SCREEN_LOAD_START] = "r-jf-tmi-1"
        eventCodeMap[EVENT_TEST_MORE_INFO_SCREEN_LOAD_END] = "r-jf-tmi-2"
        eventCodeMap[EVENT_TEST_MORE_INFO_SCREEN_TEST_CARD_FOCUS] = "r-jf-tmi-3"
        eventCodeMap[EVENT_TEST_MORE_INFO_SCREEN_START_TEST_CLICK] = "r-jf-tmi-4"
        eventCodeMap[EVENT_TEST_MORE_INFO_SCREEN_VIEW_FEEDBACK_FOCUS] = "r-jf-tmi-5"
        eventCodeMap[EVENT_TEST_MORE_INFO_SCREEN_VIEW_FEEDBACK_CLICK] = "r-jf-tmi-6"
        eventCodeMap[EVENT_TEST_MORE_INFO_SCREEN_BOOKMARK_FOCUS] = "r-jf-tmi-7"
        eventCodeMap[EVENT_TEST_MORE_INFO_SCREEN_BOOKMARK_CLICK] = "r-jf-tmi-8"

        //API failer
        eventCodeMap[LOG_TYPE_API_FAILURE] = "r-jf-api-1"

        //Test Feedback
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_LOAD_START] = "r-jf-tf-1"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_LOAD_END] = "r-jf-tf-2"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_CLICK_TO_WATCH_FOCUS] = "r-jf-tf-3"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_CLICK_TO_WATCH_CLCIK] = "r-jf-tf-4"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_SUBJECT_WISE_ANALYSIS_DROPDOWN_FOCUS] = "r-jf-tf-5"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_SUBJECT_WISE_ANALYSIS_DROPDOWN_CLCIK] = "r-jf-tf-6"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_SUBJECT_WISE_ANALYSIS_DROPDOWN_VALUE_FOCUS] =
            "r-jf-tf-8"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_SUBJECT_WISE_ANALYSIS_DROPDOWN_VALUE_CLICK] =
            "r-jf-tf-9"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_ATTEMPT_FOCUS] = "r-jf-tf-10"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_ATTEMPT_CLICK] = "r-jf-tf-11"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_ACHIEVE_HIGH_SCORE_WIDGET_FOCUS] = "r-jf-tf-12"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_CURRENT_POTENTIAL_WIDGET_CLICK] = "r-jf-tf-13"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_REVISION_LIST_VALUE] =
            "r-jf-tf-14"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_REVISION_LIST_VALUE_CLICK] =
            "r-jf-tf-15"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_CHAPTER_WISE_ANALYSIS_LOAD_START] = "r-jf-tf-16"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_CHAPTER_WISE_ANALYSIS_LOAD_END] = "r-jf-tf-17"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_CHAPTER_WISE_ANALYSIS_TILE_FOCUS] = "r-jf-tf-18"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_CHAPTER_WISE_ANALYSIS_TILE_CLICK] = "r-jf-tf-19"

        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_IMPROVE_TEST_TAKING_STRATEGY_VALUE_FOCUS] = "r-jf-tf-19"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_IMPROVE_TEST_TAKING_STRATEGY_VALUE_CLICK] = "r-jf-tf-20"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_IMPROVE_TEST_TAKING_STRATEGY_VIDEO_FOCUS] = "r-jf-tf-21"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_IMPROVE_TEST_TAKING_STRATEGY_VIDEO_CLICK] = "r-jf-tf-22"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_QUESTION_FOCUS] = "r-jf-tf-23"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_QUESTION_CLICK] = "r-jf-tf-24"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_QUESTION_SCREEN_LOAD_START] = "r-jf-tf-25"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_QUESTION_SCREEN_LOAD_END] = "r-jf-tf-26"

        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_QUESTION_WISE_ANAYLSIS_GRAPH_FOCUS] = "r-jf-tf-27"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_QUESTION_TOGGLE_FOCUS] = "r-jf-tf-28"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_QUESTION_TOGGLE_CLICK] = "r-jf-tf-29"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_BACK_TO_MARKS_OVERALL_PERFORMANCE_FOCUS] = "r-jf-tf-30"

        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_BACK_TO_MARKS_OVERALL_PERFORMANCE_FOCUS] = "r-jf-tf-31"
        eventCodeMap[EVENT_TEST_FEEDBACK_SCREEN_BACK_TO_MARKS_OVERALL_PERFORMANCE_CLICK] = "r-jf-tf-32"

        //***Learn Summary
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_LOAD_START] = "r-jf-ls-1"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_LOAD_END] = "r-jf-ls-2"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_MAIN_BUTTON_FOCUS] = "r-jf-ls-3"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_MAIN_BUTTON_CLICK] = "r-jf-ls-4"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_BOOKMARK_BUTTON_FOCUS] = "r-jf-ls-5"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_BOOKMARK_BUTTON_CLICK] = "r-jf-ls-6"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_LIKE_BUTTON_FOCUS] = "r-jf-ls-7"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_LIKE_BUTTON_CLICK] = "r-jf-ls-8"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_AVAILABLE_OPTION_FOCUS] = "r-jf-ls-9"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_AVAILABLE_OPTION_CLICK] = "r-jf-ls-10"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_START] = "r-jf-ls-11"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_END] = "r-jf-ls-12"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_KNOWLEDGE_GRAPH_FOCUS] = "r-jf-ls-13"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_KNOWLEDGE_GRAPH_CLICK] = "r-jf-ls-14"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_CHECK_PROGRESS_BACK_BUTTON_FOCUS] = "r-jf-ls-17"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_CHECK_PROGRESS_BACK_BUTTON_CLICK] = "r-jf-ls-18"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_FOCUS] = "r-jf-ls-19"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_CLICK] = "r-jf-ls-20"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_ATTEMPT_BUCKET_FOCUS] = "r-jf-ls-23"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_ATTEMPT_BUCKET_CLICK] = "r-jf-ls-24"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_FOCUS] = "r-jf-ls-25"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_CLICK] = "r-jf-ls-26"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_TILE_FOCUS] = "r-jf-ls-27"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_TILE_CLICK] = "r-jf-ls-28"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_KEYBOARD_KEYFOCUS_FOCUS] = "r-jf-ls-29"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_KEYBOARD_KEYPRESS_CLICK] = "r-jf-ls-30"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_VOICE_SEARCH_CLICK] = "r-jf-ls-31"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_START] = "r-jf-ls-32"
        eventCodeMap[EVENT_LEARN_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_END] = "r-jf-ls-33"

        //*****Practice Summary*****
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_LOAD_START] = "r-jf-ps-1"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_LOAD_END] = "r-jf-ps-2"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_MAIN_BUTTON_FOCUS] = "r-jf-ps-3"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_MAIN_BUTTON_CLICK] = "r-jf-ps-4"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_BOOKMARK_BUTTON_FOCUS] = "r-jf-ps-5"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_BOOKMARK_BUTTON_CLICK] = "r-jf-ps-6"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_LIKE_BUTTON_FOCUS] = "r-jf-ps-7"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_LIKE_BUTTON_CLICK] = "r-jf-ps-8"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_AVAILABLE_OPTION_FOCUS] = "r-jf-ps-9"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_AVAILABLE_OPTION_CLICK] = "r-jf-ps-10"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_START] =
            "r-jf-ps-11"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_END] = "r-jf-ps-12"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_KNOWLEDGE_GRAPH_FOCUS] = "r-jf-ps-13"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_KNOWLEDGE_GRAPH_CLICK] = "r-jf-ps-14"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_CHECK_PROGRESS_BACK_BUTTON_FOCUS] = "r-jf-ps-17"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_CHECK_PROGRESS_BACK_BUTTON_CLICK] = "r-jf-ps-18"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_FOCUS] = "r-jf-ps-19"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_CLICK] = "r-jf-ps-20"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_ATTEMPT_BUCKET_FOCUS] = "r-jf-ps-23"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_ATTEMPT_BUCKET_CLICK] = "r-jf-ps-24"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_FOCUS] = "r-jf-ps-25"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_CLICK] = "r-jf-ps-26"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_KEYBOARD_KEYFOCUS_FOCUS] = "r-jf-ps-29"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_KEYBOARD_KEYPRESS_CLICK] = "r-jf-ps-30"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_VOICE_SEARCH_CLICK] = "r-jf-ps-31"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_START] = "r-jf-ps-32"
        eventCodeMap[EVENT_PRACTICE_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_END] = "r-jf-ps-33"

        //****Test Summary****
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_LOAD_START] = "r-jf-ts-1"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_LOAD_END] = "r-jf-ts-2"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_MAIN_BUTTON_FOCUS] = "r-jf-ts-3"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_MAIN_BUTTON_CLICK] = "r-jf-ts-4"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_BOOKMARK_BUTTON_FOCUS] = "r-jf-ts-5"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_BOOKMARK_BUTTON_CLICK] = "r-jf-ts-6"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_LIKE_BUTTON_FOCUS] = "r-jf-ts-7"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_LIKE_BUTTON_CLICK] = "r-jf-ts-8"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_AVAILABLE_OPTION_FOCUS] = "r-jf-ts-9"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_AVAILABLE_OPTION_CLICK] = "r-jf-ts-10"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_START] = "r-jf-ts-11"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_END] = "r-jf-ts-12"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_FOCUS] = "r-jf-ts-13"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_CLICK] = "r-jf-ts-14"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_ATTEMPT_BUCKET_FOCUS] = "r-jf-ts-17"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_ATTEMPT_BUCKET_CLICK] = "r-jf-ts-18"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_FOCUS] = "r-jf-ts-19"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_CLICK] = "r-jf-ts-20"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_KEYBOARD_KEYFOCUS_FOCUS] = "r-jf-ts-23"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_KEYBOARD_KEYPRESS_CLICK] = "r-jf-ts-24"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_VOICE_SEARCH_CLICK] = "r-jf-ts-25"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_START] = "r-jf-ts-26"
        eventCodeMap[EVENT_TEST_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_END] = "r-jf-ts-27"
    }
}