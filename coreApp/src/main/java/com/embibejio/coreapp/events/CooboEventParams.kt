package com.embibejio.coreapp.events

open class CooboEventParams {


    var concept_id: String? = null
    var concept_name: String? = null
    var coobo_id: String? = null
    var coobo_name: String? = null
    var learning_map: String? = null
    var slide_number: String? = null
    var slide_name: String? = null
    var slide_id: String? = null
    var slide_type: String? = null
    var mute_state: Boolean? = null
}

