package com.embibejio.coreapp.events

open class VideoEventParams {
    var video_url: String? = null
    var Player_session: String? = null
    var video_id: String? = null
    var video_title: String? = null
    var video_type: String? = null
    var video_total_length: String? = null
}

