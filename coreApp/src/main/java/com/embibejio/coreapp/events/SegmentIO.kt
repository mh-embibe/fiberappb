package com.embibejio.coreapp.events

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.Settings
import android.util.Log
import com.embibejio.coreapp.BuildConfig
import com.embibejio.coreapp.application.LibApp
import com.embibejio.coreapp.constant.AppConstants
import com.embibejio.coreapp.model.LinkedProfile
import com.embibejio.coreapp.preference.PreferenceHelper
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.segment.analytics.Properties
import org.json.JSONObject
import org.json.simple.parser.JSONParser
import org.json.simple.parser.ParseException
import java.util.*

/**
 * Created by Sriram on 31/03/20.
 */
class SegmentIO(context: Context) {
    private val source: String = "com.embibe.jioembibe"
    private val environment: String
    private var userId: Long? = null
    private var parentId: Long? = null
    private var childId: Long? = null
    private var userType: String? = null
    private var userMobile: String? = null
    private var goal: String? = null
    private var grade: String? = null
    private var board: String? = null
    private var email: String? = null
    private var identityEnabled = false
    private var cooboEventsHM: HashMap<String, Properties>? = null
    private val pref: PreferenceHelper = PreferenceHelper()
    private val sessionId: String
    private var videoSessionId: String? = null

    @SuppressLint("HardwareIds")
    private val secureId: String = Settings.Secure.getString(
        context.contentResolver,
        Settings.Secure.ANDROID_ID
    )
    private var currentUserProfile: LinkedProfile? = null

    fun track(logType: String, eventName: String) {
        track(false, logType, eventName, null, null, null)
    }

    fun track(
        logType: String,
        eventName: String,
        navElement: String?
    ) {
        track(false, logType, eventName, navElement, null, null)
    }

    fun track(
        logType: String,
        eventName: String,
        navElement: String?,
        eventType: String?
    ) {
        track(false, logType, eventName, navElement, eventType, null)
    }

    fun track(
        logType: String,
        eventName: String,
        navElement: String?,
        extraParams: EventParams?
    ) {
        track(false, logType, eventName, navElement, extraParams)
    }

    fun track(
        logType: String,
        eventName: String,
        extraParams: EventParams?
    ) {
        track(false, logType, eventName, null, extraParams)
    }

    fun track(
        logType: String,
        eventName: String,
        extraParams: EventParams?,
        videoParam: VideoEventParams?
    ) {
        //track(false, logType, eventName, null, extraParams);
        track(
            false,
            logType,
            eventName,
            SegmentEvents.instance!!.getEventCode(eventName),
            null,
            null,
            extraParams,
            null,
            null,
            null,
            videoParam
        )
    }

    fun track(
        rewrite: Boolean,
        logType: String,
        eventName: String,
        navElement: String?,
        extraParams: EventParams?
    ) {
        track(rewrite, logType, eventName, navElement, null, extraParams)
    }

    fun track(
        logType: String,
        eventName: String,
        navElement: String?,
        eventType: String?,
        extraParams: EventParams?
    ) {
        track(false, logType, eventName, navElement, eventType, extraParams)
    }

    @JvmOverloads
    fun track(
        rewrite: Boolean,
        logType: String,
        eventName: String,
        navElement: String? = null,
        eventType: String? = null,
        extraParams: EventParams? = null
    ) {
        track(
            rewrite,
            logType,
            eventName,
            SegmentEvents.instance!!.getEventCode(eventName),
            navElement,
            eventType,
            extraParams,
            null,
            null,
            null,
            null
        )
    }

    fun track(
        rewrite: Boolean,
        logType: String,
        eventName: String,
        eventCode: String?,
        navElement: String?,
        eventType: String?,
        extraParamsJson: JSONObject?
    ) {
        track(
            rewrite,
            logType,
            eventName,
            eventCode,
            navElement,
            eventType,
            null,
            extraParamsJson,
            null,
            null,
            null
        )
    }

    fun track(
        rewrite: Boolean,
        logType: String?,
        eventName: String?,
        eventCode: String?,
        navElement: String?,
        eventType: String?,
        extraParamsJson: JSONObject?,
        session_id: String?
    ) {
        track(
            rewrite,
            logType!!,
            eventName!!,
            eventCode,
            navElement,
            eventType,
            null,
            extraParamsJson,
            session_id,
            null,
            null
        )
    }

    fun track(
        rewrite: Boolean, logType: String?, eventName: String?, eventCode: String?,
        navElement: String?, eventType: String?, extraParamsJson: JSONObject?, session_id: String?,
        cooboParams: CooboEventParams?
    ) {
        track(
            rewrite, logType!!, eventName!!, eventCode, navElement, eventType, null,
            extraParamsJson, session_id, cooboParams, null
        )
    }

    fun track(
        rewrite: Boolean,
        logType: String,
        eventName: String,
        eventCode: String?,
        navElement: String?,
        eventType: String?,
        extraParams: EventParams?,
        extraParamsJson: JSONObject?,
        session_id: String?,
        cooboParams: CooboEventParams?,
        videoParams: VideoEventParams?
    ) {
        try {
            val properties = properties
            properties.putValue("log_type", logType)
            properties.putValue("event_name", eventName)
            properties.putValue("event_code", eventCode)
            val parser = JSONParser()
            if (cooboParams != null) {
                val gson = Gson().toJson(cooboParams)
                val json =
                    parser.parse(gson) as org.json.simple.JSONObject
                properties.putValue("common_extra_params", json)
            }
            if (videoParams != null) {
                videoParams.Player_session = videoSessionId
                val gson = Gson().toJson(videoParams)
                val json =
                    parser.parse(gson) as org.json.simple.JSONObject
                properties.putValue("common_extra_params", json)
            }
            if (session_id != null) properties.putValue("session_id", session_id)
            if (eventType != null) {
                properties.putValue("event_type", eventType)
            }
            if (navElement != null) {
                properties.putValue("nav_element", navElement)
            }
            if (extraParams != null) {
                val gson = Gson().toJson(extraParams)
                val json =
                    parser.parse(gson) as org.json.simple.JSONObject
                properties.putValue("extra_params", json)
                properties.putValue("ep_format", "str")
            } else if (extraParamsJson != null) {
                val json =
                    parser.parse(extraParamsJson.toString()) as org.json.simple.JSONObject
                properties.putValue("extra_params", json)
            }
            Log.d(
                TAG_CLASS_NAME,
                "Event: $logType $eventName $eventType, Properties: $properties"
            )
            try {
                segmentAnalytics
                val event = "$logType $eventName $eventType"
                /* if (cooboParams != null) {
                     cooboEventsHM!!.put(event, properties)
                 } else {*/
                LibApp.getAnalytics().track(event, properties)
                /*  for (key in cooboEventsHM!!.keys!!) {
                      Analytics.with(LibApp.context).track(key, cooboEventsHM!!.get(key))
                      cooboEventsHM!!.remove(key)
                  }
              }*/
            } catch (e: Exception) {
            }
        } catch (e: JsonParseException) {
            e.printStackTrace()
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }

    fun setVideoSessionId() {
        videoSessionId = generateSessionId()
    }

    private fun generateSessionId(): String {
        return UUID.randomUUID().toString()
    }

    private val segmentAnalytics: Unit
        get() {
            if (userId != null && userId != 0L && !identityEnabled) {
                identityEnabled = true
                LibApp.getAnalytics().identify(userId.toString())
            }
        }

    val properties: Properties
        get() {
            userDetails
            getCurrentUserProfile()
            val properties = Properties()
            properties.putValue("source", source)
            properties.putValue("environment", environment)
            properties.putValue("is_app", true)
            properties.putValue("userId", userId)
            properties.putValue("user_type", userType)
            properties.putValue("phone_number", userMobile)
            properties.putValue("parent_id", parentId)
            properties.putValue("child_id", childId)
            properties.putValue("grade", grade)
            properties.putValue("goal", goal)
            properties.putValue("board", board)
            properties.putValue("email", email)
            properties.putValue("state", "")
            properties.putValue("city", "")
            properties.putValue("school name", "")
            properties.putValue("is_paid", "")
            properties.putValue("session_id", sessionId)
            properties.putValue("time_stamp", System.currentTimeMillis())
            properties.putValue("device_id", secureId)
            properties.putValue("device", Build.MANUFACTURER + " " + Build.MODEL)
            properties.putValue("OS", Build.VERSION.RELEASE)
            return properties
        }

    private val userDetails: Unit
        get() {
            if (pref[AppConstants.IS_LOGIN, false]) {
                val userId =
                    pref.get<String>(AppConstants.USER_ID, null)
                userType =
                    pref.get<String>(AppConstants.USER_TYPE, null)
                if (userMobile == null) {
                    userMobile = pref.get<String>(
                        AppConstants.MOBILE,
                        null
                    )
                }
                if (this.userId == null) {
                    this.userId = userId.toLong()
                }
                if (userType != null && userType == "parent") {
                    parentId = userId.toLong()
                }
                val childId = pref[AppConstants.CURRENT_PROFILE_USER_ID, 0]
                if (childId != 0) {
                    this.childId = childId.toString().toLong()
                }
            }
        }

    private fun getCurrentUserProfile() {
        currentUserProfile =
            pref.getCurrentProfile(AppConstants.CURRENT_PROFILE)
        if (currentUserProfile != null) {
            if (currentUserProfile!!.grade != null) grade = currentUserProfile!!.grade
            if (currentUserProfile!!.board != null) board = currentUserProfile!!.board
            if (currentUserProfile!!._board != null) board = currentUserProfile!!._board
            email = currentUserProfile!!.email
            userType = currentUserProfile!!.userType
            if (currentUserProfile!!.goals != null && currentUserProfile!!.goals!!.isNotEmpty()) goal =
                currentUserProfile!!.goals!![0]
        }
    }


    companion object {
        private val TAG_CLASS_NAME = SegmentIO::class.java.name
        private lateinit var instance: SegmentIO
        fun getInstance(): SegmentIO {
            if (!::instance.isInitialized)
                instance = SegmentIO(LibApp.context)
            return instance
        }
    }

    init {
        sessionId = generateSessionId()
        environment = BuildConfig.ENVIRONMENT
        userDetails
    }
}