package com.embibejio.coreapp.events

import android.util.ArraySet
import org.json.simple.JSONObject

open class EventParams {


    var video_id: Nothing? = null
    var video_name: String? =null
    var improve_strategy_section_name: String? = null
    var improve_strategy_list_position: String? = null
    var improve_strategy_list_name: String? = null
    var tile_position: String? = null
    var target_symbol: String? = null
    var widget_position: String? = null
    var exams: ArraySet<String>? = null
    var secondary_goal_position: String? = null
    var secondary_goal: String? = null
    var exam_position: String? = null
    var primary_goal_position: String? = null
    var primary_goal: String? = null
    var test_count: String? = null
    var incorrect_ans_marks: String? = null
    var correct_ans_marks: String? = null
    var difficulty_level: String? = null
    var chapter_count: String? = null
    var subject_rank: String? = null
    var is_select: Boolean? = null
    var filter_by_attempt_types: String? = null
    var question_count: String? = null
    var section_name: String? = null
    var question_type: String? = null
    var is_response: Boolean? = null
    var response_answer: String? = null
    var response: String? = null
    var current_option_id: String? = null
    var last_option_id: String? = null
    var test_session: String? = null
    var test_im_tree: String? = null
    var test_name: String? = null
    var test_id: String? = null
    var content_subject: String? = null
    var keyboardType: String? = null
    var email_id: String? = null
    var isPaid: Boolean? = null
    var competive_exams: String? = null
    var goal: String? = null
    var avatar_name: String? = null
    var grade: String? = null
    var board: String? = null
    var answer_selected: Map<String, Boolean>? = null
    var correct_answer_choice: List<String>? = null
    var question_code: String? = null
    var question_number: Int? = null
    var xpath: String? = null
    var session_id: String? = null
    var subject_name: String? = null
    var payment_id: String? = null
    var error: String? = null
    var exam: String? = null

    // Learn Page
    var query: String? = null
    var content_type: String? = null
    var content_id: String? = null
    var content_name: String? = null
    var content_position: String? = null
    var target_content_type: String? = null
    var target_content_id: String? = null
    var target_content_name: String? = null

    // Splash Screen
    var device_model: String? = null
    var device_name: String? = null

    // Login Screen
    var user_id: String? = null
    var user_type: String? = null
    var mobile: String? = null
    var locale: String? = null

    //Study Screen
    var className: String? = null
    var subject: String? = null
    var unit: String? = null
    var chapter: String? = null
    var topic: String? = null

    var username: String? = null
    var password: String? = null
    var status: String? = null
    var message: String? = null
    var parent_id: String? = null
    var childs: List<String> = emptyList()
    var selected_user: String? = null
    var selected_menu: String? = null
    var subject_code: String? = null
    var category: String? = null
    var duration: String? = null
    var current_duration: Int? = null
    var play_mode: String? = null
    var quality: String? = null
    var network_speed: String? = null
    var seek_duration: Int? = null
    var embiums_available: String? = null
    var embiums_earned: String? = null
    var error_code: String? = null
    var error_message: String? = null
    var visibility: String? = null
    var avaliablity: String? = null
    var user_query: String? = null
    var query_length: String? = null
    var search_results: String? = null
    var bookmarked: Boolean? = null
    var liked: Boolean? = null
    var button_type: String? = null
    var slide_type: String? = null
    var slide_title: String? = null
    var slide_number: String? = null
    var tile_type: String? = null
    var tile_section: String? = null
    var tile_properties: JSONObject? = null
    var adverstiment_banner_rank: String? = null
    var banner_type: String? = null
    var banner_tiltle: String? = null
    var banner_content_type: String? = null
    var banner_target_type: String? = null
    var item_name: String? = null
    var page: String? = null
    var quick_link_name: String? = null
    var scroll_depth_vertical: String? = null
    var scroll_depth_horizontal: String? = null
    var section_position: Int? = null
    var current_item_position: Int? = null

    // player events
    var bit_rate: String? = null
    var compression: String? = null
    var start_position: Long? = null
    var player_error: String? = null
    var current_position: Long? = null
    var video_player_mode: String? = null
    var audio_bitrate: String? = null
    var video_bitrate: String? = null
    var content_play_state: String? = null
    var initial_resolution: String? = null
    var change_resolution: String? = null
    var current_network_state: String? = null
    var seek_position: Long? = null
    var start: Long? = null
    var end: Long? = null
    var seek_mode: String? = null
    var menu_name: String? = null

    var volume_change: String? = null
    var current_play_state: String? = null
    var current_volume: Int? = null
    var bookmark_state: Boolean? = null
    var like_state: Boolean? = null

    //more info events
    var concept: String? = null
    var rating: String? = null
    var embiums: String? = null
    var special_flag: String? = null
    var more_info_page_title: String? = null
    var no_of_result: Int? = null

    //onboarding
    var onboarding_screen_position: Int? = null

    //Crash
    var cause: String? = null

    //Error
    var cta_name: String? = null

    //add user
    var profile_name: String? = null
    var role: String? = null
    var email: String? = null

    var apiURL: String? = null
    var bodyParams: String? = null
    var statusCode: Int? = null
    var learnpath_name: String? = null
    var learnpath_format_name: String? = null
    var format_reference: String? = null

    //
    var attemptType: String? = null
    var button_name: String? = null
    var screenName: String? = null

    //
    var option_name: String? = null
    var option_type: String? = null
    var option_position: Int? = null
    var widget_name: String? = null
    var sincerity_score_type: String? = null
    var bucket_type: String? = null
    var bucket_position: Int? = null
    var attempt_quality: String? = null
    var source: String? = null
}

