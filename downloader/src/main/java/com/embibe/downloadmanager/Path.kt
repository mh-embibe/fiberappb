package com.embibe.downloadmanager

class Path(
    var parentPath: String,
    var currentFileName: String,
    var extension: String
)