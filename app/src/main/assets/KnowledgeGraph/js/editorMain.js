var scene,
    camera, fieldOfView, aspectRatio, nearPlane, farPlane,
    renderer,container;
//SCREEN VARIABLES
var HEIGHT, WIDTH;
const camMinY = 40;
const camMaxY = 500;
const camYStep = 20;
const camPanStep = 20;
var nodes = [];
var curve = null;
var gui;
var defaultDistanceBetNodes = 2.5;
var mockNodes = [];
var unCompletedMaterialColor = 0x00E2BE;
var catMullCurve;
var numMockNodes;
//INIT THREE JS, SCREEN AND MOUSE EVENTS
function createScene() {

    HEIGHT = window.innerHeight;
    WIDTH = window.innerWidth;
  
    scene = new THREE.Scene();
    aspectRatio = WIDTH / HEIGHT;
    fieldOfView = 75;
    nearPlane = 1;
    farPlane = 10000;
    camera = new THREE.PerspectiveCamera(
      fieldOfView,
      aspectRatio,
      nearPlane,
      farPlane
      );
    scene.fog = new THREE.Fog(0xf7d9aa, 10,950);
    
    renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
    renderer.setSize(WIDTH, HEIGHT);
    renderer.shadowMap.enabled = false;
    container = document.getElementById('world');
    container.appendChild(renderer.domElement);
    
    window.addEventListener('resize', handleWindowResize, false);
    window.addEventListener( 'keyup', onDocumentKeyUp, false );
    window.addEventListener("wheel", wheelScrollHandler);

    scene.background = new THREE.Color( 0x787878 );
    camera.position.set(0,camMinY,0);
    camera.rotation.set(-1.57,0,0);
    console.log(camera.position.y);
  }
  function wheelScrollHandler(event) {
    console.log(event);
    var camY = camera.position.y;
    if(event.deltaY < 0){
        camY = camY - camYStep;
    }
    else if(event.deltaY > 0){
        camY = camY + camYStep;
    }
    if(camY < camMinY){
        camY = camMinY;
    }else if (camY > camMaxY){
        camY = camMaxY;
    }
    camera.position.set(0,camY,0);
  }
  // LIGHTS
  function handleWindowResize() {
    HEIGHT = window.innerHeight;
    WIDTH = window.innerWidth;
    renderer.setSize(WIDTH, HEIGHT);
    camera.aspect = WIDTH / HEIGHT;
    camera.updateProjectionMatrix();
}
// HANDLE SCREEN EVENTS
function onDocumentKeyUp( event ) {
    var keyCode = event.which;
    //A
    if ( keyCode == 87 )
    {
        panForward();                    
    }
    //D
    else if ( keyCode == 83 )
    {
        panBack();        
    }
    //I
    if ( keyCode == 68 )
    {//D
        panRight();        
        //handleOnViewProgress();
    }
    //O
    else if ( keyCode == 65 )
    {//A
        panLeft();        
        //handleOnBack();
    }
}
var ambientLight, directionalLight;

function createLights() {
    //Lights
    ambientLight = new THREE.AmbientLight(0xFFFFFF,0.5);
    scene.add(ambientLight);

    directionalLight = new THREE.DirectionalLight(0xFFFFFF,1);
    directionalLight.position.set(-45,45,-45);
    scene.add(directionalLight);
}

function createGrid(){
    var size = 150;
    var divisions = 10;
    var gridHelper = new THREE.GridHelper( size, divisions,0xff0000,0xffffff );
    scene.add( gridHelper );

    var xArrow = new THREE.ArrowHelper(
        // first argument is the direction
        new THREE.Vector3(2, 2, 0).normalize(),
        // second argument is the orgin
        new THREE.Vector3(0, 0, 0),
        // length
        10,
        // color
        0x00ff00);
    scene.add(xArrow);
    var zArrow = new THREE.ArrowHelper(
        // first argument is the direction
        new THREE.Vector3(0, 0, 1).normalize(),
        // second argument is the orgin
        new THREE.Vector3(0, 0, 0),
        // length
        10,
        // color
        0xff0000);
    scene.add(zArrow);
}

function updateMockObjects(){
    if(!catMullCurve){
        return;
    }
    clearMockNodesIfPresent();
    if(numMockNodes == 0){
        return;
    }
    var geometry = new THREE.CylinderGeometry( 0.32,1,0.25,6,1,false,0,6.3);
    unCompletedMaterial = new THREE.MeshPhongMaterial( {color: unCompletedMaterialColor,flatShading:true} );
    
    var goalNodeFactory = new GoalNodeFactory();
    goalNodeFactory.init(scene,geometry,unCompletedMaterial);
    mockNodes = goalNodeFactory.create(numMockNodes);

    var gap = 5;
    var dir = new THREE.Vector3( 0, 0, -1 );
    const sampleMultiplier = 10;
    
    let splinePointsArray = catMullCurve.getPoints(numMockNodes*sampleMultiplier);
    const clonedPoints = splinePointsArray.slice();
    mockNodes.forEach(currMockNode => {
        let id = currMockNode.getId();
        let normT = id/mockNodes.length;
        if(id == 0){
            normT = 0;
        }
        if(id == (mockNodes.length-1)){
            normT = 1;
        }
        //console.log("NormT"+ normT);
        var dstPosition = new THREE.Vector3();
        if(normT == 0){
            dstPosition = clonedPoints[0];
        }
        else if(normT == 1){
            dstPosition = clonedPoints[sampleMultiplier*mockNodes.length];
        }
        else{
            dstPosition =  clonedPoints[Math.floor(normT*mockNodes.length*sampleMultiplier-1)];
        }
        currMockNode.setPosition(dstPosition);
    });   
}

function clearMockNodesIfPresent(){
    for(i = 0 ; i < mockNodes.length; i++){
        scene.remove(mockNodes[i].m_goalNodeMesh);
        console.log("Removed Node: "+i);
    }
    mockNodes = [];
}

function loop(){
    renderer.render(scene, camera);
    requestAnimationFrame(loop);
}

function createDragControl(){
    var controls = new THREE.DragControls(nodes,camera,renderer.domElement);
    controls.addEventListener( 'dragstart', function ( event ) {
    } );
    
    controls.addEventListener( 'dragend', function ( event ) {
    } );

    controls.addEventListener('drag', (event) => {
        event.object.position.y = 0;
        updateCurve();
      });
}
function panForward(){
    var camPos = camera.position.clone();
    camPos.z = camPos.z - camPanStep;
    camera.position.set(camPos.x,camPos.y,camPos.z);
}               
function panBack(){
    var camPos = camera.position.clone();
    camPos.z = camPos.z + camPanStep;
    camera.position.set(camPos.x,camPos.y,camPos.z);
}
function panRight(){
    var camPos = camera.position.clone();
    camPos.x = camPos.x + camPanStep;
    camera.position.set(camPos.x,camPos.y,camPos.z);
}
function panLeft(){
    var camPos = camera.position.clone();
    camPos.x = camPos.x - camPanStep;
    camera.position.set(camPos.x,camPos.y,camPos.z);
}

function updateCurve(){
    //Create a closed wavey loop
    if(curve){
        scene.remove(curve);
    }
    var points = [];
    for(i = 0 ; i < nodes.length; i++){
        points.push(nodes[i].position);
    }
    if(points.length <= 1){
        return;
    }
    catMullCurve = new THREE.CatmullRomCurve3( points );

    var points = catMullCurve.getPoints( points.length*10 );
    var geometry = new THREE.BufferGeometry().setFromPoints( points );

    var material = new THREE.LineBasicMaterial( { color : 0xffffff } );

    // Create the final object to add to the scene
    curve = new THREE.Line( geometry, material );
    scene.add(curve);
}

function getNumNodes(){
    return nodes.length;
}

var props = {hideBars:false,
            depthZ_Fraction:0.015,
            barColor: '#FFFFFF',
            barDirection:'Vertical'};

props.addNode = 
        function() {
            console.log("ADD Node CLICKED!");
            createNode();
       };
props.deleteLastNode = 
        function() {
            console.log("Delete Node CLICKED!");
            deleteLastNode();
        };
props.deleteAllNodes = 
        function() {
            console.log("Delete Node CLICKED!");
            var r = confirm("Are you sure you want to delete all Nodes?");
            if (r == true) {
                deleteAllNodes();
            txt = "You pressed OK!";
            } else {
                console.log("Didn't Delete Nodes");
            }
        };
props.numNodes = 
        function() {
            alert("Total Number of nodes present is: " + nodes.length);
        };
props.saveToJson =
        function(){
            console.log("Save To Json Called");
            saveNodesToJson();
        };
setNumNodes = { numMockNodes : 0 };
props.createMockNodes =
        function(){
            console.log("Create Mock Nodes Called");
            numMockNodes = setNumNodes.numMockNodes;
            updateMockObjects();
        };
props.deleteMockNodes =
        function(){
            console.log("Delete Mock Nodes Called");
            numMockNodes = 0;
            updateMockObjects();
        };
props.loadFile = 
            function() { 
                deleteAllNodes();
                var mydata = JSON.parse(data);
                for(i = 0;i < mydata.length;i++){
                    createNode(mydata[i]);
                }
                console.log(mydata);

            };
function updateGui(){
    if(gui){
        gui.destroy();
    }
    gui = new dat.GUI();
    gui.add(props,'addNode').name('Add Node');
    gui.add(props,'deleteLastNode').name('Delete Last Node');
    gui.add(props,'numNodes').name('No Nodes: ' + getNumNodes());
    gui.add(props,'deleteAllNodes').name("Delete All Nodes");
    gui.add(props,'saveToJson').name("Save As Json");
    gui.add(props, 'loadFile').name('Load Json');

    gui.add(setNumNodes, 'numMockNodes').min(0).max(120).step(1).listen().onFinishChange(function(){
        numMockNodes = setNumNodes.numMockNodes;
    }).name("No Of Mocks");
    gui.add(props,'createMockNodes').name("Create Mocks");
    gui.add(props,'deleteMockNodes').name("Delete Mocks");
}

function createNode(position){
    var geometry = new THREE.SphereGeometry( 1, 32, 32 );
    var material = new THREE.MeshBasicMaterial( {color: Math.random() * 0xffff00,transparent:true,opacity:0.75} );
    var sphere = new THREE.Mesh( geometry, material );
    scene.add( sphere );
    
    if(position){
        sphere.position.set(position.x,position.y,position.z);
    }
    else{
        sphere.position.set(0,0,-(nodes.length*defaultDistanceBetNodes));
    }
    
    nodes.push(sphere);

    updateCurve();
    updateGui();
}

function deleteLastNode(){
    if(nodes.length == 0){
        return;
    }
    scene.remove(nodes[nodes.length-1]);
    nodes.pop();

    updateCurve();
    updateGui();
}

function deleteAllNodes(){
    if(nodes.length == 0){
        return;
    }
    console.log("deleteAllNodes: ");
    for(var i = nodes.length-1 ; i >= 0; i--){
        console.log("INDEX: "+i);
        scene.remove(nodes[i]);
        nodes.splice(i,1);
        console.log("LENGTH AFTER DELETE: "+nodes.length);
    }
    updateCurve();
    updateGui();   
}

function saveNodesToJson(){
    var positions = [];
    for(i = 0 ; i < nodes.length; i++){
        positions.push(nodes[i].position);
    }
    download(JSON.stringify(positions),"nodes.json");
}

function loadFile(fileNames){
    console.log(fileNames[0].File);
    var jsonData = JSON.parse(fileNames[0]);
    console.log(jsonData);
}

function download(data, filename) {
    // data is the string type, that contains the contents of the file.
    // filename is the default file name, some browsers allow the user to change this during the save dialog.

    // Note that we use octet/stream as the mimetype
    // this is to prevent some browsers from displaying the 
    // contents in another browser tab instead of downloading the file
    var blob = new Blob([data], {type:'octet/stream'});

    //IE 10+
    if (window.navigator.msSaveBlob) {
        window.navigator.msSaveBlob(blob, filename);
    }
    else {
        //Everything else
        var url = window.URL.createObjectURL(blob);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.href = url;
        a.download = filename;

        setTimeout(() => {
            //setTimeout hack is required for older versions of Safari

            a.click();

            //Cleanup
            window.URL.revokeObjectURL(url);
            document.body.removeChild(a);
        }, 1);
    }
}
function main(event){
    createScene();
    createLights();
    createGrid();
    createDragControl();
    updateGui();
    loop();
}

window.addEventListener('load', main, false);
