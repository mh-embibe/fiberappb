class GoalNode {
    constructor(scene,geometry,material,id) {
    this.m_goalNodeMesh = new THREE.Mesh( geometry, material );
    scene.add(this.m_goalNodeMesh);
    this.m_id = id;
    }
    setPosition(destPos){
        this.m_goalNodeMesh.position.x = destPos.x;
        this.m_goalNodeMesh.position.y = destPos.y;
        this.m_goalNodeMesh.position.z = destPos.z;
    }
    getId(){
        return this.m_id;
    }
    getPosition(){
        ////console.log("position of: "+this.m_id+" is: "+ result.z);
        return this.m_goalNodeMesh.position;
    }
    getZ(){
        ////console.log("position of: "+this.m_id+" is: "+ result.z);
        return this.m_goalNodeMesh.position.z;
    }
}