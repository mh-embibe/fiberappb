//https://www.base64-image.de/ is used to convert png to base64
class GoalNode {
    constructor(scene,geometry,material,id) {
    this.m_goalNodeMesh = new THREE.Mesh( geometry, material );
    scene.add(this.m_goalNodeMesh);
    this.m_id = id;
    }
    setPosition(destPos){
        this.m_goalNodeMesh.position.x = destPos.x;
        this.m_goalNodeMesh.position.y = destPos.y;
        this.m_goalNodeMesh.position.z = destPos.z;
    }
    getId(){
        return this.m_id;
    }
    getPosition(){
        ////console.log("position of: "+this.m_id+" is: "+ result.z);
        return this.m_goalNodeMesh.position;
    }
    getZ(){
        ////console.log("position of: "+this.m_id+" is: "+ result.z);
        return this.m_goalNodeMesh.position.z;
    }
}
////////////////////////////////////////////////////////////////////////////////GoalNode
////////////////////////////////////////////////////////////////////////////////GoanNodeFactory
class GoalNodeFactory {
    constructor() {
    }
    init(scene,geometry,material){
    this.m_scene = scene;
    this.m_geometry = geometry;
    this.m_material = material;
    this.m_count = 0;
    }
    create(numGoalNodes){
        var goalNodes = [];
        for(var currGoalNodeIdx = 0;currGoalNodeIdx < numGoalNodes;currGoalNodeIdx++){
        goalNodes[currGoalNodeIdx] = new GoalNode(this.m_scene,this.m_geometry,this.m_material.clone(),this.m_count);
        this.m_count = this.m_count + 1;
        }
        return goalNodes;
    }
}
////////////////////////////////////////////////////////////////////////////////GoanNodeFactory
////////////////////////////////////////////////////////////////////////////////StraightGoalNodePlacer
class StraightGoalNodePlacer {
    constructor() {
    }
    place(dir,gap,goalNodes){
        goalNodes.forEach(currGoalNode => {
            let id = currGoalNode.getId();
            let s = gap*id;
            let destPos = new THREE.Vector3( );

            destPos.x = dir.x * s;
            destPos.y = dir.y * s;
            destPos.z = dir.z * s;
            currGoalNode.setPosition(destPos);
        });
    }
    place3dText(dir,gap,text3Ds,textOffset){
    text3Ds.forEach(currtext3D => {
        let id = currtext3D.getId();
        let s = gap*id;
        let destPos = new THREE.Vector3( );

        destPos.x = dir.x * s;
        destPos.y = dir.y * s;
        destPos.z = dir.z * s;
        currtext3D.setPosition({x:destPos.x+textOffset.x,y:destPos.y+textOffset.y,z:destPos.z+textOffset.z});
        //console.log(destPos);
    });
}
}
////////////////////////////////////////////////////////////////////////////////StraightGoalNodePlacer
////////////////////////////////////////////////////////////////////////////////DampingSinGoalNodePlacer
class DampingSinGoalNodePlacer {
    constructor() {
    }
    place(dir,gap,goalNodes){
        //const points = [];
        var points = [];
        const samplings = goalNodes.length;
        const distance = (samplings/100)*85;
        const scale = 5;
        const λ = 0.1;//0.1
        const ω = 1.85;//2
        const φ = 0;
        const A = samplings/2;
        const sampleMultiplier = 10;

        for (let i = 0; i < samplings; i++) {

            const t = (i / samplings) * distance;
            const s = A * Math.exp(-λ * t) * Math.sin(ω * t + φ);
            //points.push(new THREE.Vector3(t, s, 0));
            points.push(new THREE.Vector3(s*scale, 0, -t*scale));
        }
        const spline = new THREE.CatmullRomCurve3(points);

        // render spline curve
        let splinePointsArray = spline.getPoints(samplings*sampleMultiplier);

        const geometry = new THREE.BufferGeometry().setFromPoints(splinePointsArray);
        const material = new THREE.LineBasicMaterial();
        //material.uniforms.opacity = 0.05;

        splineLine = new THREE.Line(geometry, material);
        scene.add(splineLine);

        const clonedPoints = splinePointsArray.slice();
        goalNodes.forEach(currGoalNode => {
            let id = currGoalNode.getId();
            let normT = id/samplings;
            if(id == 0){
                normT = 0;
            }
            if(id == (samplings-1)){
                normT = 1;
            }
            //console.log("NormT"+ normT);
            var dstPosition = new THREE.Vector3();
            if(normT == 0){
                dstPosition = clonedPoints[0];
            }
            else if(normT == 1){
               dstPosition = clonedPoints[sampleMultiplier*samplings];
            }
            else{
               dstPosition =  clonedPoints[Math.floor(normT*samplings*sampleMultiplier-1)];
            }
            currGoalNode.setPosition(dstPosition);
        });   
    }
}
////////////////////////////////////////////////////////////////////////////////DampingSinGoalNodePlacer
////////////////////////////////////////////////////////////////////////////////JsonGoalNodePlacer
class JsonGoalNodePlacer {
    constructor() {
    }
    getRangeId(numNodes){
        //hardcoded for now
        if(numNodes >= 0 && numNodes <= 5){
            return 0;
        }
        else if(numNodes >= 6 && numNodes <= 10){
            return 1;
        }
        else if(numNodes >= 11 && numNodes <= 15){
            return 2;
        }
        else if(numNodes >= 16 && numNodes <= 20){
            return 3;
        }
        else if(numNodes >= 21 && numNodes <= 30){
            return 4;
        }
        else if(numNodes >= 31 && numNodes <= 40){
            return 5;
        }
        else if(numNodes >= 41 && numNodes <= 60){
            return 6;
        }
        else if(numNodes >= 61 && numNodes <= 80){
            return 7;
        }
        else if(numNodes >= 81){
            return 8;
        }
    }
    place(dir,gap,goalNodes){
        var points = [];
        const samplings = goalNodes.length;
        const sampleMultiplier = 10;
        var jsonCurvedata;
        if(goalNodes.length >= 0){
            var random_boolean = Math.random() >= 0.5;
            rangeId = this.getRangeId(goalNodes.length);//goalNodes.length
            var variantId = 0;
            if(random_boolean){
                variantId = 1;
            }
            jsonCurvedata = JSON.parse(eval('curvesData'+rangeId+variantId));
        }
        //console.log(jsonCurvedata);
        var points=[];
        for (let i = 0; i < jsonCurvedata.length; i++) {

            points.push(new THREE.Vector3(jsonCurvedata[i].x, jsonCurvedata[i].y, jsonCurvedata[i].z));
        }
        //console.log(points);
        let splinePointsArray;
        if(goalNodes.length > 1){
            const spline = new THREE.CatmullRomCurve3(points);

            // render spline curve
            splinePointsArray = spline.getSpacedPoints(samplings*sampleMultiplier);

            curveGeometry = new THREE.BufferGeometry().setFromPoints(splinePointsArray);
            curveMaterial = new THREE.LineBasicMaterial({
                                                        color: 0x7C78A5,
                                                        linewidth: 0.5,
                                                        linecap: 'round', //ignored by WebGLRenderer
                                                        linejoin:  'round' //ignored by WebGLRenderer
                                                     });

            splineLine = new THREE.Line(curveGeometry, curveMaterial);
            scene.add(splineLine);
        }
        else{
            goalNodes[0].setPosition(new THREE.Vector3(0, 0, 0));
            return;
        }
        

        const clonedPoints = splinePointsArray.slice();
        goalNodes.forEach(currGoalNode => {
            let id = currGoalNode.getId();
            let normT = id/samplings;
            if(id == 0){
                normT = 0;
            }
            if(id == (samplings-1)){
                normT = 1;
            }
            //console.log("NormT"+ normT);
            var dstPosition = new THREE.Vector3();
            if(normT == 0){
                dstPosition = clonedPoints[0];
            }
            else if(normT == 1){
               dstPosition = clonedPoints[sampleMultiplier*samplings];
            }
            else{
               dstPosition =  clonedPoints[Math.floor(normT*samplings*sampleMultiplier-1)];
            }
            currGoalNode.setPosition(dstPosition);
        });   
    }
}
////////////////////////////////////////////////////////////////////////////////JsonGoalNodePlacer
////////////////////////////////////////////////////////////////////////////////Texture Animator

function TextureAnimator(texture, tilesHoriz, tilesVert, numTiles, tileDispDuration) 
{	
	// note: texture passed by reference, will be updated by the update function.
		
	this.tilesHorizontal = tilesHoriz;
	this.tilesVertical = tilesVert;
	// how many images does this spritesheet contain?
	//  usually equals tilesHoriz * tilesVert, but not necessarily,
	//  if there at blank tiles at the bottom of the spritesheet. 
	this.numberOfTiles = numTiles;
	texture.wrapS = texture.wrapT = THREE.RepeatWrapping; 
	texture.repeat.set( 1 / this.tilesHorizontal, 1 / this.tilesVertical );

	// how long should each image be displayed?
	this.tileDisplayDuration = tileDispDuration;

	// how long has the current image been displayed?
	this.currentDisplayTime = 0;

	// which image is currently being displayed?
	this.currentTile = 0;
		
	this.update = function( milliSec )
	{
		this.currentDisplayTime += milliSec;
		while (this.currentDisplayTime > this.tileDisplayDuration)
		{
			this.currentDisplayTime -= this.tileDisplayDuration;
			this.currentTile++;
			if (this.currentTile == this.numberOfTiles)
				this.currentTile = 0;
			var currentColumn = this.currentTile % this.tilesHorizontal;
			texture.offset.x = currentColumn / this.tilesHorizontal;
			var currentRow = Math.floor( this.currentTile / this.tilesHorizontal );
			texture.offset.y = currentRow / this.tilesVertical;
		}
	};
}		
////////////////////////////////////////////////////////////////////////////////Texture Animator
//handling Canvas Resize -- End --
var rangeId = 0;//used for scaling sprites depending upon cam height from floor.
var currNode = 0;
var prevNode = 0;
var newNodes = [];
var dataFromApi = null;
var isFirstTime = true;
const pixelRatio = window.devicePixelRatio;//2
//new THREE.Vector3(-0.071,20.724,15);
const deltaPosition = new THREE.Vector3(10,25.724,15);
var camTopPosition = new THREE.Vector3(0,100,-50);
const camTopRotation = new THREE.Vector3(-1.57,0,0);
const camCloseViewRotation = new THREE.Vector3(-0.6,0,0);
const camCloseViewStartPosition = deltaPosition;

var completedMaterialColor = 0x00E2BE;
var unCompletedMaterialColor = 0x008BFF;

var timeToAnimate = 1200;
var progressDetailsID = "progressDetails";
var maskID = "mask";
var lessonNameID = "lessonName";
var isHardID = "isHard";
var isImportantID = "isImportant";
var isLengthyID = "isLengthy";
var videoProgressID = "videoProgress";
var questionProgressID = "questionProgress";
var conceptMasteryID = "conceptMastery";
var perfectAttemptID = "perfectAttempt";
//var checkProgressBtnID = "checkProgressBtn";
//var backBtnID = "backBtn";

var spritesObjects = [];
var glowObject;
var glowObjectMaterial;
var glowObjectGeometry;
var diamondObjects = [];
var animatedTextures = [];

var sunriseImage;
var redRippleImage;
var greenRippleImage;
var diamondImage;
var sunriseTexture;
var redRippleTexture;
var greenRippleTexture;
var diamondTexture;
var glowImage;
var glowTexture;

var splineLine;
//const progressUiOffset = { x:1,y:16};
const progressUiOffset = { x:4.5,y:16};
const fromStartToFinishCamtargetPercentage = 35;
const yStep = 1;

const States = {
    InitTopView: 1,
    Animating:2,
    CheckProgressView: 3
 };
 Object.freeze(States);
var appState = States.InitTopView;
var clock = new THREE.Clock();

setProgressDetailUIVisibility(false);
setMaskUIVisibility(false);
setDiamondObjectsVisibility(true);
setSpriteObjectsVisibility(true);
// THREEJS RELATED VARIABLES
var scene,
    camera, fieldOfView, aspectRatio, nearPlane, farPlane,
    renderer, container,logicCamera;
//SCREEN VARIABLES
var HEIGHT, WIDTH;
var curveGeometry;
var curveMaterial;
var cylinderGeometry;
var isTopToBottomPosAnimDone = false;
var isTopToBottomRotAnimDone = false;
function changeCurrentNode(dstNodeId){
    //console.log("YYY " + dstNodeId);
    var tempNodeId = dstNodeId;
    if(tempNodeId >= dataFromApi.data.length-1){
        tempNodeId = dataFromApi.data.length-1;
    }
    if(tempNodeId < 0){
        tempNodeId = 0;
    }
    prevNode = currNode;
    currNode = tempNodeId;
    changeMaterial(prevNode,currNode);
}

function changeMaterial(prvNode,curNode){
    //console.log("CHANGE MATERIAL CALLED");
    for(var i = 0; i < spritesObjects.length;i++){
        if(spritesObjects[i].id == prvNode){
            spritesObjects[i].object.visible = true;
        }
        if(spritesObjects[i].id == curNode){
            spritesObjects[i].object.visible = false;
        }
    }
    glowObject.position.set(newNodes[curNode].m_goalNodeMesh.position.x,newNodes[curNode].m_goalNodeMesh.position.y-0.13,newNodes[curNode].m_goalNodeMesh.position.z);
    return;
    var isCurrNodeCompleted = getIsGoalCompleted(curNode);
    var isPrevNodeCompleted = getIsGoalCompleted(prvNode);

    /////////////Remove this to notice color mixing
    if(isPrevNodeCompleted){
        newNodes[prvNode].m_goalNodeMesh.material.color.set(completedMaterialColor);
    }else{
        newNodes[prvNode].m_goalNodeMesh.material.color.set(unCompletedMaterialColor);
    }
    newNodes[curNode].m_goalNodeMesh.material.color.set(unCompletedMaterialColor);//Emssion doesnt work well with uncomplete color
    /////////////Remove this to notice color mixing
    newNodes[curNode].m_goalNodeMesh.material.emissive.set( 0xf23990 );// 0x00E2BE
    newNodes[prvNode].m_goalNodeMesh.material.emissiveIntensity = 0;
    newNodes[curNode].m_goalNodeMesh.material.emissiveIntensity = 0.75;
}

function resetGoalHighlights(){
    newNodes[currNode].m_goalNodeMesh.material.emissiveIntensity = 0;
}

var moveForward = function(movetime = timeToAnimate){
    if(dataFromApi == null){
        return;
    }
    if(appState !== States.CheckProgressView){
        return;
    }
    stopAllTweens();
    setProgressDetailUIVisibility(false);
    setMaskUIVisibility(false);
    changeCurrentNode(currNode + 1);
    /*currNode = currNode + 1;
    if(currNode >= dataFromApi.data.length-1){
        currNode = dataFromApi.data.length-1;
    }*/
    var position = camera.position;
    var target = new THREE.Vector3();
    target = getNodeCameraPosition(currNode);
    /*target.x = camera.position.x;
    target.y = camera.position.y;
    target.z = deltaPosition.z + (newNodes[currNode].getZ());*/
    tween = new TWEEN.Tween(position) // Create a new tween that modifies 'coords'.
    .to({x: target.x, y: target.y,z:target.z}, movetime) // Move to (300, 200) in 1 second.
    .easing(TWEEN.Easing.Quadratic.In) // Use an easing function to make the animation smooth.
    .onUpdate(() => {
        camera.position.x = position.x;
        camera.position.y = position.y;
        camera.position.z = position.z;
    })
    .start() // Start the tween immediately.
    .onComplete(function() {
        //console.log('moveForward Done!');
        goalTransitionCompleted();
      });
    //console.log("moveForward",newNodes[currNode].getZ());
    //console.log(currNode);
}

var moveBackward = function(movetime = timeToAnimate){
    if(dataFromApi == null){
        return;
    }
    if(appState !== States.CheckProgressView){
        return;
    }
    stopAllTweens();
    setProgressDetailUIVisibility(false);
    setMaskUIVisibility(false);
    changeCurrentNode(currNode - 1);
    /*currNode = currNode - 1;
    if(currNode < 0){
        currNode = 0;
    }*/
    var position = camera.position;
    var target = new THREE.Vector3();
    target = getNodeCameraPosition(currNode);
    /*target.x = camera.position.x;
    target.y = camera.position.y;
    target.z = deltaPosition.z + (newNodes[currNode].getZ())
    */
    tween = new TWEEN.Tween(position) // Create a new tween that modifies 'coords'.
    .to({x: target.x, y: target.y,z:target.z}, movetime) // Move to (300, 200) in 1 second.
    .easing(TWEEN.Easing.Quadratic.Out) // Use an easing function to make the animation smooth.
    .onUpdate(() => {
        camera.position.x = position.x;
        camera.position.y = position.y;
        camera.position.z = position.z;
    })
    .start() // Start the tween immediately.
    .onComplete(function() {
        //console.log('moveForward Done!');
        goalTransitionCompleted();
      });
}

function stopAllTweens(){
    var allTweens = TWEEN.getAll();
    for ( var i = allTweens.length - 1; i >= 0 ; i-- ) { // Start from the last index to 0, because tween.stop() changes allTweens.lenght! (?)
        var tween = allTweens[i];
        tween.stop();
    }
}

function getNodeCameraPosition(nodeId){
    //nodeId = 0;
    var target = new THREE.Vector3();
    /*target.x = deltaPosition.x;
    target.y = deltaPosition.y;
    target.z = deltaPosition.z + (newNodes[nodeId].getZ());*/
    target = newNodes[nodeId].getPosition().clone().add(deltaPosition);
    return target;
}

function objectToCameraPosition(object){
    camera.updateMatrix();
    camera.updateMatrixWorld();

    let pos = new THREE.Vector3();
    pos = pos.setFromMatrixPosition(object.matrixWorld);
    pos.project(camera);

    let widthHalf = WIDTH / 2;
    let heightHalf = HEIGHT / 2;
    //console.log("HalfWidth" + widthHalf);
    //console.log("HalfHeight" + heightHalf);
    pos.x = (pos.x * widthHalf) + widthHalf;
    pos.y = - (pos.y * heightHalf) + heightHalf;
    pos.z = 0;
    return pos;
}

function setProgressDetailUIVisibility(isVisible){
    if(isVisible){
        document.getElementById(progressDetailsID).style.display = "block"; 
    }else{
        document.getElementById(progressDetailsID).style.display = "none";
    }
}
function setMaskUIVisibility(isVisible){
    if(isVisible){
        document.getElementById(maskID).style.display = "block"; 
    }else{
        document.getElementById(maskID).style.display = "none"; 
    }
}
/*
function setBackUIVisibility(isVisible){
    if(isVisible){
        document.getElementById(backBtnID).style.display = "block"; 
    }else{
        document.getElementById(backBtnID).style.display = "none"; 
    }
}

function setCheckProgressUIVisibility(isVisible){
    if(isVisible){
        document.getElementById(checkProgressBtnID).style.display = "block"; 
    }else{
        document.getElementById(checkProgressBtnID).style.display = "none"; 
    }
}
*/
function setProgressDetailsForCurrGoal(){
    var redBtn = "btn1";
    var greenBtn = "btn1 green";
    var lessonNameElement = document.getElementById(lessonNameID);
    var isHardElement = document.getElementById(isHardID);
    var isImportantElement = document.getElementById(isImportantID);
    var isLengthyElement = document.getElementById(isLengthyID);
    var videoProgressElement = document.getElementById(videoProgressID);
    var questionProgressElement = document.getElementById(questionProgressID);
    var conceptMasteryElement = document.getElementById(conceptMasteryID);
    var perfectAttemptElement = document.getElementById(perfectAttemptID);

    lessonNameElement.innerHTML = dataFromApi.data[currNode].display_name;
    var difficultyLevel = dataFromApi.data[currNode].score.difficulty_level;
    var importanceScore = dataFromApi.data[currNode].score.importance;
    var length = dataFromApi.data[currNode].score.length;
    if(difficultyLevel == null){
        difficultyLevel = 0.0;
    }
    if(importanceScore == null){
        importanceScore = 0.0;
    }
    if(length == null){
        length = 0.0;
    }
    if(difficultyLevel > 8.0){
        isHardElement.innerHTML = "Difficult";
        isHardElement.className = redBtn;     
    }else if(difficultyLevel < 2.0){
        isHardElement.innerHTML = "Easy";
        isHardElement.className = greenBtn;         
    }
    if(importanceScore > 5.0){
        isImportantElement.innerHTML = "Important";    
        isImportantElement.className = redBtn;     
    }else{
        isImportantElement.innerHTML = "Not as Important";    
        isImportantElement.className = greenBtn;     
    }
    if(length > 5.0){
        isLengthyElement.innerHTML = "Lengthy"; 
        isLengthyElement.className = redBtn;   
    }else{
        isLengthyElement.innerHTML = "Short";  
        isLengthyElement.className = greenBtn;     
    }

    var totalVideos=dataFromApi.data[currNode].concept_body.progress.video_coverage.total_videos;
    if(totalVideos == null){
        totalVideos = 0;
    }
    var videosFinished=dataFromApi.data[currNode].concept_body.progress.video_coverage.watched_videos;
    if(videosFinished == null){
        videosFinished = 0;
    }
    var VideoProgressText =  videosFinished + " Video(s) Watched";
    videoProgressElement.innerHTML = VideoProgressText;

    var totalQuestions=dataFromApi.data[currNode].concept_body.progress.question_coverage.total_questions;
    var questionsFinished=dataFromApi.data[currNode].concept_body.progress.question_coverage.attempted_questions;
    if(totalQuestions == null){
        totalQuestions = 0;
    }
    if(questionsFinished == null){
        questionsFinished = 0;
    }
    var QuestionProgressText = questionsFinished+"/"+totalQuestions + " Questions";
    questionProgressElement.innerHTML = QuestionProgressText;
    
    var numConceptMastery=dataFromApi.data[currNode].concept_body.achievement.concept_mastery;
    if(numConceptMastery == null){
        numConceptMastery = 0;
    }
    var ConceptMasteryText = "<img class='perfectClass' src='./img/overall_accuracy.png' />" + numConceptMastery +"% Concept Mastery";
    conceptMasteryElement.innerHTML = ConceptMasteryText;

    var numPerfectAttempt=(dataFromApi.data[currNode].concept_body.achievement.perfect_attempts);
    if(numPerfectAttempt == null){
        numPerfectAttempt = 0;
    }
    var numPerfectAttemptText = "<img class='perfectClass' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHwAAAB8CAYAAACrHtS+AAAABmJLR0QA/wD/AP+gvaeTAAAJ2UlEQVR42u2de1BTVxrALyJ2tSoC1jJURVx3dced1nZltzqtfcy0FVG3nZ1UwFq1QnRW94FWE3ysWhe3Heujde2K1rbbVXcbV0sSCA+BPCAUAYUk8pIqVWsFGRSrkJD7OHsuJTSwAfK4N+Ym32/mG4b/MufHud93vvPlQhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAICj2EWjkXwn0+3QCncI/zwokZOxnZj87GHQBbTKK2R2M1HjxkBBjI9GWt4DIjgaTTlCWjCaWiNHlz59AOUKUvYtgyMVEft0iIudr/DMKjA6Cfg2aUCxGtVg4Kk5BXbuHI5PQhC8nqnULiRzUEw14p0eCWQeoV6BxWPQFVrYtMl9BOryIjFBkpxHtV7DkLjvhCO9042tEQQQYtqNSjEKx4Ap72bZ4fzTSCuRRTi8mztbZy/4xcqviiawwMP2D7FElq5HOkWw2ziagS3hBO3xd+ArCqHUsuzdKRYR6dEDLLk1FI7HUooFk2+LQRN8u4DYT37OPcvMQwtkoFBGywDyy1YjQCCwzayjZbGjfQrfTg9C3vimcoX9LFBidkG2LvDhC9VBAyZaJULBejGTOyLbF8adQri8KX0WYtC7ItuX0L58n1MMDR3YKOumK7J4g/xaCjL71KL93HQvsdF14d5zCj/dgv5aNCBRUvBodcUN2d8jnI73vHNMY+jWisNpN2d0RT+R8uoPYMcxvZetXo0Puyu49po3xjZZrMlGr9UT2j6E66JfCcQftPU9ls1GYhBrxgt9/kLK3ER3X8e68z41wtjmTu9+/WqYpaBcXsm2REY1UvIh8mGlfN42ue+NZsmxBIqmZu96qmbWrS/94elcpGzPfNZfP2Gs5P3VFZXZUnKJkyotKzfQ5WeonZyj18x7Jrp0fknPPA/Hb/UP2arSeS9ls6N5Cd/Ax7ZqngqXhTEvSi2TpnLct2p8dtNROOWKmcaChInxFXsu4RDlyFGEJ8huRCxWlM2YrNc9EqkzxQTmkC9I3Cl32H7iWbYsTs1GeO5JTJzGXXxBbNVM/Mjc5I7d/RB/uuDKQ7AHiTlS8Uv/UjGx9XIjq7hDCGXxkWytU2SuxGIYv4Tiod0NQlZM7ufnlN62an/7d3OCOZPuYuOemzkXh9mFhd3/sVGX5IDufwfWBWFgFWgpahoXQPMrujuxFqBwLpR1J3jkcdS2fS5WzuTf6iJn0VLQtHpUaSjwQ3ufRzz72B8j7FL5lSxTKzp7HNkn4lm2LfeNQoU3yO8MQnTKLNvxGYtHGZFhucyW5T/5emXuTC+H2j/yYZ5XqV0bk3ul3rWrFEevzwrGEYm/JZiN/GWpImU2fj5VaSmIyzK18SO7N3xmdTRzL7iN++tNKzYJg+0uYXJXPN1e8sLuZPAmqPfo5k/+OntJJjdTdObIuE5+ie/P3+y1aHoXbHvXXfx2TXd6zy9t9focfPsHkZe5kanELlZMcXpCKWv6zl6k4eIpR7yyhvpIaqFYsGdnH64Wk1hvCJ2ziJn87E1Hx8rKXRqou+bxwqYn6b7cIA9WxtYI2vZdH6T76gtH88x+M/kw6Y1DiPwbVFqYxdyO6litBV9nfT+9mqmV7mIpjx5jiD89Q6t1qquQv5XRVmoFq7i/XUaw9R5Z5Qzg+f3/nLeE9u/2kzwuXVHfNxBLMzojiKiRG6ju+ZUdndHzjTdk47o59Qz5NEJU6liDCwXhT+sxPzc285u99LTovyqbDlioWCuosnmYi070p/KVM6zk+hXN1/nYyNgjwPhQFpRmpL7wlfLnOymvhFr4y/1svyf5MsL301FI0UmqkK70hPPU8aeBNeEbHVS/J1hNxAp9321KLorGQZi9I74w5yl0L1T4m7fVG/s78ZvSyMxP84opUYiLnYiEWvqX/6qT5Eh/CI6WmYp6F3wtLlD/uV0MQEgP1Jt/CF+dYi3nJ36vyr/MomwlNVLzul2NOEgO5h0/h4q+ses6FH+68xmtzJVGe5rdTqzsQGoY7cQq+hG+qtjZxnr/33dLxKFuGjzNBhD+zqR6NwZW7kSfpzPRPLO2c5u+0izwJV1RGLVKOIgIBaRWaguXc4kP6c6e7LnCcv/k4kt2IECkfIwIJSTX5DBbUxbXwpCLuGjD4/puPZkvn2CWKWCIQwZ24lVwL/9N5utKH8zcTliRPIAIZqYk8wPHNGZvDGU7y92YTtwMPCfLtRKAjkqFgLCqLS+mzjluucpO/z17lUPZpv6/InT6uNaKxuHK/yJXwuKyuUg7y9w0Od/eFR5flPQym7dhqQDH4jN7KhfBkPanzOH/vv8VJOzU0UX4zPCFzEhh2VLkbyXlcVO4bqsh6z/N3jZYD2ebwpYqnweyg0um1HOxyctonlk7P8ndBEwcV+VIw6lTlTh3yVPpcmeWiB/nb42HF0ITMdDDpbBGnRsPx8arAE+GiAvfz+KT9bR6evzO/JHb46Zsd+CK1BoXjM/old4Wvq3B/dDlyc63GA+E14UtVY8GgOxctBjQd7/Q7bjZg3J5iHb+q4IqbsltDk7KmgjnPOnEvs0WYW6PLn1nckd7MFlxuyLbiG7DnwBgXPXcD/WdvjS5PPtBa7F6RJk8BU9xetBz2xuiyO/k7LClzDxjiGHElCsFzcUUuji4bXRUekVz4tYtTK7n4QiAYDPGxy+tQBBbZyN/ocqdL+Rt30urCRLJQMMNn5W5Cv8Ai252VHuvC6PLkD9pcyN+KNsF82U/wlbuBnI9lUs4IfzXX+dHlyK11aqcr8iT5C2DCq0UcvckZ4WvOkU6PLkekFDU6+b3tNWDgwfTcj3I3utzZ4lT+TlJ8ACv/oHruNWgElqrhYnR58oE2Z74OnE88HyDvOfdVNlSi8VjqZU9HlyO31RcNcfyqx21T+Ec1voDkovUJLPaeJ6PLOH83DCL89pglZ34OK+1bnbhXsVzandHl6COdrbjqpgeQTeFO2gJYYV/c6UZ6qzujy4Plbzy1sg5W1lfBrxnBgo+7OrocuWXA/H0MFtXXK/cm9BMsuKy/8Pgs64CjyxHionoHsnW4Rz4CVlQAbKzBL20wUtfshafoyWLH82vm/8/fCfIro0WqR2AlhdSUqbY+iUXfH2p0efKHt/vn7+/DlmT9ElZQiNIN1O/sXhDocHQ5cntDoaBfiAf0P66ROwcbXY4Qa2rthK+HFfODyj3NRP3b0ehyT/6mBP9CPKAvP7wgkCrvP7rcm78T5CWCfyEe0Jct1eixt6upPlelUdu683fFmETleFghfzyjV6JRMR9b1k05aj6BH+f/Gv/HsgTY2QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADA8j9NYrfmq5ddZwAAAABJRU5ErkJggg==' />" + numPerfectAttempt +"% Perfect Attempts";
    perfectAttemptElement.innerHTML = numPerfectAttemptText;

    if(numConceptMastery == 0){
        perfectAttemptElement.innerHTML = "";
    }
}

//INIT THREE JS, SCREEN AND MOUSE EVENTS
function createScene() {

    HEIGHT = window.innerHeight;
    WIDTH = window.innerWidth;

    scene = new THREE.Scene();
    aspectRatio = WIDTH / HEIGHT;
    fieldOfView = 75;
    nearPlane = 1;
    farPlane = 1000;
    camera = new THREE.PerspectiveCamera(
      fieldOfView,
      aspectRatio,
      nearPlane,
      farPlane
      );
    logicCamera = new THREE.PerspectiveCamera(
        fieldOfView,
        aspectRatio,
        nearPlane,
        farPlane
    );
    //scene.fog = new THREE.Fog(0xf7d9aa, 10,950);
    //camera.position.x = 0;
    //camera.position.z = 200;
    //camera.position.y = 100;
   // camera.position.set(camTopPosition);
    //camera.rotation.set(camTopRotation);
  
    renderer = new THREE.WebGLRenderer({ alpha: true, antialias: false, autoSize: true });
    renderer.setSize(WIDTH, HEIGHT);
    renderer.shadowMap.enabled = false;
    renderer.setPixelRatio(pixelRatio);
    container = document.getElementById('world');
    container.appendChild(renderer.domElement);
  
    /*var checkProgressBtnElement = document.getElementById(checkProgressBtnID);
    checkProgressBtnElement.onclick = handleOnViewProgress;
    var backBtnElement = document.getElementById(backBtnID);
    backBtnElement.onclick = handleOnBack;
    */
    window.addEventListener('resize', handleWindowResize, false);
    window.addEventListener( 'keyup', onDocumentKeyUp, false );
    window.moveForward = moveForward;
    window.moveBackward = moveBackward;
    window.handleOnViewProgress = handleOnViewProgress;
    window.handleOnBack = handleOnBack;
    window.setDataFromApi = setDataFromApi;
    window.clearObjects = clearObjects;
    window.setPixelRatio = setPixelRatio;
  }
  // LIGHTS

var ambientLight, directionalLight;

function createLights() {

  
    //Lights
    ambientLight = new THREE.AmbientLight(0xFFFFFF,0.5);
    scene.add(ambientLight);

    directionalLight = new THREE.DirectionalLight(0xFFFFFF,1);
    directionalLight.position.set(-45,45,-45);
    scene.add(directionalLight);

    return;
    hemisphereLight = new THREE.HemisphereLight(0xaaaaaa,0x000000, .9);

    ambientLight = new THREE.AmbientLight(0xdc8874, .5);

    shadowLight = new THREE.DirectionalLight(0xffffff, .9);
    shadowLight.position.set(150, 350, 350);
    shadowLight.castShadow = true;
    shadowLight.shadow.camera.left = -400;
    shadowLight.shadow.camera.right = 400;
    shadowLight.shadow.camera.top = 400;
    shadowLight.shadow.camera.bottom = -400;
    shadowLight.shadow.camera.near = 1;
    shadowLight.shadow.camera.far = 1000;
    shadowLight.shadow.mapSize.width = 2048;
    shadowLight.shadow.mapSize.height = 2048;

    scene.add(hemisphereLight);
    scene.add(shadowLight);
    scene.add(ambientLight);
}
  
// HANDLE SCREEN EVENTS
function onDocumentKeyUp( event ) {
    var keyCode = event.which;
    //A
    if ( keyCode == 87 )
    {
        moveForward();                    
    }
    //D
    else if ( keyCode == 83 )
    {
        moveBackward();        
    }
    //I
    if ( keyCode == 73 )
    {
        handleOnViewProgress();
    }
    //O
    else if ( keyCode == 79 )
    {
        handleOnBack();
    }
    else if( keyCode == 82){//R
        clearObjects();
        getDataFromApi();
    }
}
function handleWindowResize() {
    HEIGHT = window.innerHeight;
    WIDTH = window.innerWidth;
    renderer.setSize(WIDTH, HEIGHT);
    renderer.setPixelRatio(pixelRatio);
    camera.aspect = WIDTH / HEIGHT;
    camera.updateProjectionMatrix();
}

function getFirstIncompleteNodeID(){
    for (i = 0; i < dataFromApi.data.length; i++) {
        if(!getIsGoalCompleted(i)){
            //console.log("FIRST INCOMPLETE: "+i);
            return i;
        }
      }
      return dataFromApi.data.length - 1;
}

function getIsGoalCompleted(goalID){
    var numquestions = dataFromApi.data[goalID].concept_body.progress.question_coverage.total_questions;
    if(numquestions == null){
        numquestions = 0;
    }
    var completedquestions = dataFromApi.data[goalID].concept_body.progress.question_coverage.attempted_questions;
    if(completedquestions == null){
        completedquestions = 0;
    }
    var questionCompletedPercentage = completedquestions/numquestions;
    if(completedquestions == 0 && numquestions == 0){
        questionCompletedPercentage = 0;//no question so completed
    }
    var numVideos = dataFromApi.data[goalID].concept_body.progress.video_coverage.total_videos;
    if(numVideos == null){
        numVideos = 0;
    }
    var completedVideos = dataFromApi.data[goalID].concept_body.progress.video_coverage.watched_videos;
    if(completedVideos == null){
        completedVideos = 0;
    }
    var videoCompletedPercentage = completedVideos/numVideos;
    if(completedVideos == 0 && numVideos == 0){
        videoCompletedPercentage = 0;//no videos so completed
    }
    //console.log("ObjectID: " + goalID + " video Coverage : " + videoCompletedPercentage+ " QuestionCoverage : " + questionCompletedPercentage);
    //console.log((videoCompletedPercentage >= 0.25 || questionCompletedPercentage >= 0.25));
    return (videoCompletedPercentage >= 0.25 || questionCompletedPercentage >= 0.25);
}

function handleOnViewProgress(){
    if(dataFromApi == null){
        return;
    }
    if(appState !== States.InitTopView){
        return;
    }
    isTopToBottomPosAnimDone = false;
    isTopToBottomRotAnimDone = false;
    appState = States.Animating;
    setDiamondObjectsVisibility(false);
    setSpriteObjectsVisibility(true);
    changeCurrentNode(getFirstIncompleteNodeID());
    //console.log("FIRST INCOMPLETE NODE: "+currNode);
    //console.log("handleViewProgress");
    stopAllTweens();
    //Position
    var position = camera.position.clone();
    var destCampos = getNodeCameraPosition(currNode);
    var target = new THREE.Vector3(destCampos.x,destCampos.y,destCampos.z);
    //console.log("ggggggg" + target.x + " " + target.y + " " + target.z);
    var tweenPos = new TWEEN.Tween(position).to(target, 1000);
    tweenPos.onUpdate(function(){
        camera.position.x = position.x;
        camera.position.y = position.y;
        camera.position.z = position.z;
    });
    tweenPos.easing(TWEEN.Easing.Quadratic.In);
    tweenPos.start();
    tweenPos.onComplete(function(){
            isTopToBottomPosAnimDone = true;
            if(isTopToBottomPosAnimDone && isTopToBottomRotAnimDone){
                appState = States.CheckProgressView;
                goalTransitionCompleted();
            }
        });
    //Rotation
    var currRotation = camera.rotation.clone();
    var targetRotation = camCloseViewRotation.clone();
    var tweenRot = new TWEEN.Tween(currRotation).to(targetRotation, 1000);
    tweenRot.onUpdate(function(){
        camera.rotation.x = currRotation.x;
        camera.rotation.y = currRotation.y;
        camera.rotation.z = currRotation.z;
    });
    tweenRot.easing(TWEEN.Easing.Quadratic.In);
    tweenRot.start();
    tweenRot.onComplete(function(){
        isTopToBottomRotAnimDone = true;
        if(isTopToBottomPosAnimDone && isTopToBottomRotAnimDone){
            appState = States.CheckProgressView;
                        goalTransitionCompleted();
                    }
    });
    //curveMaterial.uniform.linewidth = 1;
}

function handleOnBack(){
    if(dataFromApi == null){
        return;
    }
    if(appState !== States.CheckProgressView){
        return;
    }
    appState = States.Animating;
    setDiamondObjectsVisibility(true);
    setSpriteObjectsVisibility(true);
    //console.log("handleBack");
    setProgressDetailUIVisibility(false);
    setMaskUIVisibility(false);
    resetGoalHighlights();
    glowObject.position.set(0,1000,0);
    stopAllTweens();
    //Position
    var position = camera.position;
    //var target = new THREE.Vector3(0,100,-50);
    var target = camTopPosition.clone();
    
    var tween = new TWEEN.Tween(position).to(target, 2000);
    tween.onUpdate(function(){
        camera.position.x = position.x;
        camera.position.y = position.y;
        camera.position.z = position.z;
    });
    tween.easing(TWEEN.Easing.Quadratic.In);
    tween.start();
    //Rotation
    var currRotation = camera.rotation;
    var targetRotation =  new THREE.Vector3(-1.57,0,0);
    tween = new TWEEN.Tween(currRotation).to(targetRotation, 2000);
    tween.onUpdate(function(){
        camera.rotation.x = currRotation.x;
        camera.rotation.y = currRotation.y;
        camera.rotation.z = currRotation.z;
    });
    tween.easing(TWEEN.Easing.Quadratic.In);
    tween.start();
    tween.onComplete(function(){
        appState = States.InitTopView;
        //setProgressDetailUIVisibility(false);
        //setBackUIVisibility(false);
        //setCheckProgressUIVisibility(true);
    });
}

var completedMaterial;
var unCompletedMaterial;
function setCompletedMaterialToNodes(){
    for (i = 0; i < dataFromApi.data.length; i++) {
        if(getIsGoalCompleted(i)){
            newNodes[i].m_goalNodeMesh.material = completedMaterial.clone();
        }
      }
    //completedMaterial
}

function createObjects(){
    camera.position.set(camTopPosition);
    camera.rotation.set(camTopRotation.x,camTopRotation.y,camTopRotation.z);
    //var geometry = new THREE.CircleGeometry( 1,32,0,6.3);
    //cylinderGeometry = new THREE.CylinderGeometry( 0.32,1,0.25,6,1,false,0,6.3);
    cylinderGeometry = new THREE.CylinderGeometry( 1,1,0.2,8,1,false,0,6.3);
    completedMaterial = new THREE.MeshPhongMaterial( {color: completedMaterialColor ,flatShading:true} );
    unCompletedMaterial = new THREE.MeshPhongMaterial( {color: unCompletedMaterialColor,flatShading:true} );
    //var cone = new THREE.Mesh( geometry, material );
    //scene.add( cone );
    var goalNodeFactory = new GoalNodeFactory();
    goalNodeFactory.init(scene,cylinderGeometry,unCompletedMaterial);
    //newNodes = goalNodeFactory.create(dataFromApi.goals.length);
    newNodes = goalNodeFactory.create(dataFromApi.data.length);

    setCompletedMaterialToNodes();

    var gap = 5;
    var dir = new THREE.Vector3( 0, 0, -1 );
    //var currNode = 0;
    //var placer = new StraightGoalNodePlacer();
    //var placer = new DampingSinGoalNodePlacer();
    var placer = new JsonGoalNodePlacer();
    placer.place(dir,gap,newNodes);
    //placer.place(dir,gap,newNodes.slice(0,14));
    var topMostNode = getIdOfTopMostNode();
    //var newCamTopPosition = getCameraPosCoveringPoints(newNodes[0].m_goalNodeMesh.position.clone(),newNodes[newNodes.length-1].m_goalNodeMesh.position.clone());
    var newCamTopPosition = getCameraPosCoveringPoints(newNodes[0].m_goalNodeMesh.position.clone(),newNodes[topMostNode].m_goalNodeMesh.position.clone());
    //calculating cam z pos
    var camzPos = ((newNodes[newNodes.length-1].m_goalNodeMesh.position.z)/100)*fromStartToFinishCamtargetPercentage;
    
    newCamTopPosition.z = camzPos;
    //console.log("FFFF: "+camzPos);
    //calculating cam z pos
    if(newCamTopPosition.y == 0){//worst case
        newCamTopPosition = camTopPosition;
    }else{
        camTopPosition = newCamTopPosition;
    }
    //console.log(camera.position);
    //console.log(camera.rotation);
    //console.log("Calculated Camera Y: "+ newCamTopPosition.y);
    //var position = camTopPosition;
    //var target = camTopPosition;
    var position = newCamTopPosition;
    var target = newCamTopPosition;
    if(newNodes.length == 1){
        target.z = 0;
    }
    var tween = new TWEEN.Tween(position).to(target, 2000);
    tween.onUpdate(function(){
        camera.position.x = position.x;
        camera.position.y = position.y;
        camera.position.z = position.z;
    });
    tween.easing(TWEEN.Easing.Quadratic.In);
    tween.start();

    setProgressDetailUIVisibility(false);
    setMaskUIVisibility(false);
    //setBackUIVisibility(false);
    //setCheckProgressUIVisibility(false);

    appState = States.InitTopView;

    createLines();
    createTextures();
    createSprites();
    createGlowObject();
}

function createSprites(){
    //Diamond 19, 1, 19, 75
    /*var redRippleScale = {x:7.60, y:7.60, z:1};
    var greenRippleScale = {x:7.60, y:7.60, z:1};
    var sunRiseScaleScale = {x:7.60, y:7.60, z:1};*/
    var redRippleScale = {x:5.70, y:5.70, z:1};
    var greenRippleScale = {x:5.70, y:5.70, z:1};
    var sunRiseScaleScale = {x:9.50, y:9.50, z:1};
    var DiamondScale = {x:2.25, y:2.25, z:1};//4.25
    var scaleMultiplier = 1;
    if(rangeId == 8){
        scaleMultiplier = 1.25;//0.85
    }
    if(rangeId == 7){
        scaleMultiplier = 1.20;
    }
    if(rangeId == 6){
        scaleMultiplier = 1.15;
    }
    if(rangeId == 5){
        scaleMultiplier = 1.10;
    }
    if(rangeId == 4){
        scaleMultiplier = 1.05;
    }
    if(rangeId == 3){
        scaleMultiplier = 0.85;
    }
    if(rangeId == 2){
        scaleMultiplier = 0.85;
    }
    if(rangeId == 1){
        scaleMultiplier = 0.85;
    }
    if(rangeId == 0){
        scaleMultiplier = 0.85;
    }
    scaleMultiplier = 0.75;
    redRippleScale = {x:redRippleScale.x*scaleMultiplier, y:redRippleScale.y*scaleMultiplier,z:1};
    greenRippleScale = {x:greenRippleScale.x*scaleMultiplier, y:greenRippleScale.y*scaleMultiplier,z:1};
    sunRiseScaleScale = {x:sunRiseScaleScale.x*scaleMultiplier, y:sunRiseScaleScale.y*scaleMultiplier,z:1};
    DiamondScale = {x:DiamondScale.x*scaleMultiplier, y:DiamondScale.y*scaleMultiplier,z:1};
    
    for (i = 0; i < dataFromApi.data.length; i++) {
        var difficultyLevel = dataFromApi.data[i].score.difficulty_level;
        var importance = dataFromApi.data[i].score.importance;
        var perfectAttempts = dataFromApi.data[i].concept_body.achievement.perfect_attempts;
        if(difficultyLevel == null){
            difficultyLevel = 0.0;
        }
        if(importance == null){
            importance = 0.0;
        }
        if(perfectAttempts == null){
            perfectAttempts = 0.0;
        }
        if(difficultyLevel > 8.0){
            createSpriteAndPlace(i,redRippleTexture,redRippleScale,0.26,newNodes[i].m_goalNodeMesh.position.clone(),0,-0.12,-0.15 );
        }else if(difficultyLevel < 2.0){
            //createSpriteAndPlace(i,greenRippleTexture,greenRippleScale,0.26,newNodes[i].m_goalNodeMesh.position.clone(),-0.13,-0.13,0 );
        }
        if(importance > 5.0){
            createSpriteAndPlace(i,sunriseTexture,sunRiseScaleScale,0.23,newNodes[i].m_goalNodeMesh.position.clone(),-0.13,-0.13,0 );
        }
        if(perfectAttempts > 0){
            //console.log("Diamond created");
            createSpriteAndPlace(i,diamondTexture,DiamondScale,0.0,newNodes[i].m_goalNodeMesh.position.clone(),-0.13,-0.13,-2.75,true );
        }
      }
    setDiamondObjectsVisibility(true);
    setSpriteObjectsVisibility(true);
}
function createSpriteAndPlace(id,textureMap,scale,zRotation,position,xOffset,yOffset,zOffset,isAddToDiamons = false){
    var runnerMaterial = new THREE.MeshBasicMaterial( { map: textureMap, side:THREE.DoubleSide ,transparent:true,alphaTest:0.05,depthWrite:false} );
	var runnerGeometry = new THREE.PlaneGeometry(scale.x,scale.y,scale.z,1);
	var spriteObject = new THREE.Mesh(runnerGeometry, runnerMaterial);
    spriteObject.position.set(position.x+xOffset,position.y+yOffset,position.z + zOffset);
    spriteObject.rotation.set(-1.57,0,zRotation);
    scene.add(spriteObject);

    spritesObjects.push({id:id,object:spriteObject});
    if(isAddToDiamons){
        diamondObjects.push(spriteObject);
    }
}

function createTextures(){
    /////////////////////////////sunrise
    sunriseImage = new Image();
    sunriseImage.src = SUNRISEGIF64;
    sunriseTexture = new THREE.Texture();
    sunriseTexture.image = sunriseImage;
    sunriseImage.onload = function() {
        //sunriseTexture.anisotropy = renderer.capabilities.getMaxAnisotropy();
        sunriseTexture.needsUpdate = true;
        //sunriseTexture.magFilter = THREE.NearestFilter;
        //sunriseTexture.minFilter = THREE.LinearMipMapLinearFilter;
        //sunriseTexture.generateMipmaps = false;
    };
    var sunriseAnimatedTexture = new TextureAnimator( sunriseTexture, 12, 1, 12, 55  ); // texture, #horiz, #vert, #total, duration.
    animatedTextures.push(sunriseAnimatedTexture);
    /////////////////////////////sunrise
    /////////////////////////////redRipple
    redRippleImage = new Image();
    redRippleImage.src = REDRIPPLEGIF64;
    redRippleTexture = new THREE.Texture();
    redRippleTexture.image = redRippleImage;
    redRippleImage.onload = function() {
        //redRippleTexture.anisotropy = renderer.capabilities.getMaxAnisotropy();
        redRippleTexture.needsUpdate = true;
        //redRippleTexture.magFilter = THREE.NearestFilter;
        //redRippleTexture.minFilter = THREE.LinearMipMapLinearFilter;
        //redRippleTexture.generateMipmaps = false;
    };
    var redRippleAnimatedTexture = new TextureAnimator( redRippleTexture, 6, 1, 6, 55 ); // texture, #horiz, #vert, #total, duration.
    animatedTextures.push(redRippleAnimatedTexture);
    /////////////////////////////redRipple
    /////////////////////////////greenRipple
    greenRippleImage = new Image();
    greenRippleImage.src = GREENRIPPLEGIF64;
    greenRippleTexture = new THREE.Texture();
    greenRippleTexture.image = greenRippleImage;
    greenRippleImage.onload = function() {
        greenRippleTexture.anisotropy = renderer.capabilities.getMaxAnisotropy();
        greenRippleTexture.needsUpdate = true;
        //greenRippleTexture.magFilter = THREE.NearestFilter;
        //greenRippleTexture.minFilter = THREE.LinearMipMapLinearFilter;
        //greenRippleTexture.generateMipmaps = false;
    };
    var greenRippleAnimatedTexture = new TextureAnimator( greenRippleTexture,3, 3, 9, 85 ); // texture, #horiz, #vert, #total, duration.
    //var greenRippleAnimatedTexture = new TextureAnimator( greenRippleTexture,3, 3, 9, 85 ); // texture, #horiz, #vert, #total, duration.
    animatedTextures.push(greenRippleAnimatedTexture);
    /////////////////////////////greenRipple
    /////////////////////////////diamond
    diamondImage = new Image();
    diamondImage.src = DIAMONDGIF64;
    diamondTexture = new THREE.Texture();
    diamondTexture.image = diamondImage;
    diamondImage.onload = function() {
        //diamondTexture.anisotropy = renderer.capabilities.getMaxAnisotropy();
        diamondTexture.needsUpdate = true;
        //diamondTexture.magFilter = THREE.NearestFilter;
        //diamondTexture.minFilter = THREE.LinearMipMapLinearFilter;
        //diamondTexture.generateMipmaps = false;
    };
    var diamondAnimatedTexture = new TextureAnimator( diamondTexture,  19, 1, 19, 75 ); // texture, #horiz, #vert, #total, duration.
    animatedTextures.push(diamondAnimatedTexture);
    /////////////////////////////diamond
}

function createGlowObject(){
    //console.log(position);
    glowImage = new Image();
    glowImage.src = GLOW64;
    glowTexture = new THREE.Texture();
    glowTexture.image = glowImage;
    glowImage.onload = function() {
        glowTexture.needsUpdate = true;
    };

    //var animatedTexture = new TextureAnimator( texture, gifNumHorizontal, gifNumVertical, gifTotal, gifDuration ); // texture, #horiz, #vert, #total, duration.
    //var animatedTexture = new TextureAnimator( texture, 19, 1, 19, 75 ); // texture, #horiz, #vert, #total, duration.
    //,depthTest:true,depthWrite:true
    glowObjectMaterial = new THREE.MeshBasicMaterial( { map: glowTexture, side:THREE.DoubleSide ,transparent:true} );
	glowObjectGeometry = new THREE.PlaneGeometry(6,6,1,1);
	glowObject = new THREE.Mesh(glowObjectGeometry, glowObjectMaterial);
    glowObject.position.set(0,1000,0);
    glowObject.rotation.set(-1.57,0,0);
    scene.add(glowObject);
}

function createGui(){
   
}

function setPixelRatio(value){
    pixelRatio = value;
    renderer.setPixelRatio(pixelRatio);
}

function getDataFromApi(){
    dataFromApi = mockData;
        //console.log(dataFromApi);
    createObjects();
    return;
    // 1. Create a new XMLHttpRequest object
    let xhr = new XMLHttpRequest();

    // 2. Configure it: GET-request for the URL /article/.../load
    xhr.open('GET', 'https://api.npoint.io/817354b09ec659fc3e82');

    // 3. Send the request over the network
    xhr.send();

    // 4. This will be called after the response is received
    xhr.onload = function() {
    if (xhr.status != 200) { // analyze HTTP status of the response
        alert(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
    } else { // show the result
        //alert(`Done, got ${xhr.response.length} bytes`); // response is the server
        dataFromApi = JSON.parse(xhr.response);
        //dataFromApi.goals = dataFromApi.goals.slice(0,1);
        //dataFromApi = JSON.parse(mockData);
        //console.log(dataFromApi);
        createObjects();
    }
    };

    xhr.onprogress = function(event) {
    if (event.lengthComputable) {
        //alert(`Received ${event.loaded} of ${event.total} bytes`);
    } else {
        //alert(`Received ${event.loaded} bytes`); // no Content-Length
    }

    };

    xhr.onerror = function() {
    alert("Request failed");
    };
}

function setDataFromApi(data){
    //console.log("KKK-Got Data From Android! :" +  JSON.stringify(data));
    clearObjects();
    //dataFromApi = JSON.parse(data);
    //dataFromApi = JSON.parse(JSON.stringify(data));
    dataFromApi = data;
    createObjects();
}

function isPointInsideFrustrum(objPos){
    //console.log("HHH"+objPos.x + " " + objPos.y + " " + objPos.z);
    var frustum = new THREE.Frustum();
    frustum.setFromProjectionMatrix( new THREE.Matrix4().multiplyMatrices( logicCamera.projectionMatrix, logicCamera.matrixWorldInverse ) );
    //frustum.setFromProjectionMatrix( logicCamera.projectionMatrix);
    if(frustum.containsPoint( objPos )){
        return true;
    }
    return false;
}

function getIdOfTopMostNode(){
    var lastZ = 0;
    var lastZID = 0;
    for (i = 0; i < newNodes.length; i++) {
        if(newNodes[i].m_goalNodeMesh.position.z < lastZ){
            lastZ = newNodes[i].m_goalNodeMesh.position.z;
            lastZID = i;
        }
    }
    return lastZID;
}

function getCameraPosCoveringPoints(objPos1,objPos2){
    //console.log("HHH"+objPos1.x + " " + objPos1.y + " " + objPos1.z);
    //console.log("HHH"+objPos2.x + " " + objPos2.y + " " + objPos2.z);

    logicCamera.position.set(0,0,0);
    logicCamera.lookAt( 0, 0, 0 );
    logicCamera.updateMatrix(); 
    logicCamera.updateMatrixWorld(); 
    logicCamera.matrixWorldInverse.getInverse( logicCamera.matrixWorld );

    for (let i = 0; i < 100; i++) {
        var obj1Status = isPointInsideFrustrum(objPos1);
        var obj2Status = isPointInsideFrustrum(objPos2);

        //console.log("IsInsideCamera "+ logicCamera.position.y + " " + i + " : " + obj1Status + " " + obj2Status);

        if(obj1Status  == true && obj2Status == true){
            return logicCamera.position.clone(); 
        }else{
            logicCamera.position.set(0,i*yStep,0);
            logicCamera.lookAt( 0, 0, 0 );
            logicCamera.updateMatrix(); 
            logicCamera.updateMatrixWorld(); 
            logicCamera.matrixWorldInverse.getInverse( logicCamera.matrixWorld );
        }
      }
    return {x:0,y:0,z:0};
}

var line;

function createLines(){
    return;
    var material = new THREE.LineBasicMaterial({
        color: 0xffffff
    });
    
    var points = [];

    for (i = 0; i < newNodes.length; i++) {
        points.push(newNodes[i].m_goalNodeMesh.position); 
    }
    //console.log("POINTS: "+points);
    /*points.push( new THREE.Vector3( - 10, 0, 0 ) );
    points.push( new THREE.Vector3( 0, 10, 0 ) );
    points.push( new THREE.Vector3( 10, 0, 0 ) );*/
    
    var geometry = new THREE.BufferGeometry().setFromPoints( points );
    
    line = new THREE.Line( geometry, material );
    scene.add( line );
}

function clearObjects(){
    //console.log("KKK-Objects Cleared" );
    for (i = 0; i < newNodes.length; i++) {
        scene.remove(newNodes[i].m_goalNodeMesh); 
    }
    scene.remove(splineLine);
    animatedTextures.length=0;
    for (i = 0; i < spritesObjects.length; i++) {
        scene.remove(spritesObjects[i].object); 
        spritesObjects[i].object.geometry.dispose();
        spritesObjects[i].object.material.dispose();
    }
    if(redRippleTexture){
        redRippleTexture.dispose();
    }
    if(greenRippleTexture){
        greenRippleTexture.dispose();
    }
    if(sunriseTexture){
        sunriseTexture.dispose();
    }
    if(diamondTexture){
        diamondTexture.dispose();
    }
    if(redRippleImage){
        redRippleImage = null;
    }
    if(greenRippleImage){
        greenRippleImage = null;
    }
    if(sunriseImage){
        sunriseImage = null;
    }
    if(diamondImage){
        diamondImage = null;
    }
    spritesObjects.length = 0;
    diamondObjects.length = 0;
    logicCamera.position.set(0,0,0);

    if(curveGeometry){
        curveGeometry.dispose();
    }
    if(curveMaterial){
        curveMaterial.dispose();
    }
    if(unCompletedMaterial){
        unCompletedMaterial.dispose();
    }
    if(completedMaterial){
        completedMaterial.dispose();
    }
    if(cylinderGeometry){
        cylinderGeometry.dispose();
    }
    if(glowObject){
        scene.remove(glowObject);
    }
    if(glowObjectMaterial){
        glowObjectMaterial.dispose();
    }
    if(glowObjectGeometry){
        glowObjectGeometry.dispose();
    }
    if(glowTexture){
        glowTexture.dispose();
    }
    if(glowImage){
        glowImage = null;
    }
    renderer.renderLists.dispose();
    stopAllTweens();
}

function setDiamondObjectsVisibility(isVisible){
    for (i = 0; i < diamondObjects.length; i++) {
        diamondObjects[i].visible = isVisible; 
    }
}

function setSpriteObjectsVisibility(isVisible){
    for (i = 0; i < spritesObjects.length; i++) {
        spritesObjects[i].object.visible = isVisible; 
    }
}

function goalTransitionCompleted(){
    setMaskUIVisibility(true);
    setProgressDetailUIVisibility(true);
    setProgressDetailsForCurrGoal();
    var pos = objectToCameraPosition(newNodes[currNode].m_goalNodeMesh);
    var element  = document.getElementById(progressDetailsID);
    element.style.left = pos.x + progressUiOffset.x + "px";
    element.style.top = pos.y + progressUiOffset.y - (element.clientHeight+50) + "px";
    //Fix for first time jump in ui offset
    if(isFirstTime){
        isFirstTime = false;
        moveForward(1);
        moveBackward(1);
    }
}

function loop(){
    var delta = clock.getDelta();
    if(TWEEN !== undefined){
    TWEEN.update();
    }
    if(appState !== States.InitTopView){
        animatedTextures.map(animTex =>{
            animTex.update(1000 * delta);
        });
    }
    /*if(diamondObjects !== null && diamondObjects.length > 0){
        for(let i = 0;i < diamondObjects.length;i++){
            //diamondObjects[i].lookAt(camera.position)
        }
    }*/


    renderer.render(scene, camera);
    requestAnimationFrame(loop);
}

function main(event){
    disableLogs();
    createScene();
    createLights();
    loadLevel();
    loop();
}

function disableLogs(){
    // Place this at the start of your code
    const log = console.log;
    console.log = () => {};
    const warn = console.warn;
    console.warn = () => {};
    const error = console.error;
    console.error = () => {};
}

function loadLevel(){
    clearObjects();
    //setDataFromApi();
    //getDataFromApi();
}

// HANDLE MOUSE EVENTS

var mousePos = { x: 0, y: 0 };

function handleMouseMove(event) {
  /*var tx = -1 + (event.clientX / WIDTH)*2;
  var ty = 1 - (event.clientY / HEIGHT)*2;
  mousePos = {x:tx, y:ty};*/
}

window.addEventListener('load', main, false);
