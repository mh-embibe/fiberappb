class GoalNodeFactory {
    constructor() {
    }
    init(scene,geometry,material){
    this.m_scene = scene;
    this.m_geometry = geometry;
    this.m_material = material;
    this.m_count = 0;
    }
    create(numGoalNodes){
        var goalNodes = [];
        for(var currGoalNodeIdx = 0;currGoalNodeIdx < numGoalNodes;currGoalNodeIdx++){
        goalNodes[currGoalNodeIdx] = new GoalNode(this.m_scene,this.m_geometry,this.m_material.clone(),this.m_count);
        this.m_count = this.m_count + 1;
        }
        return goalNodes;
    }
}