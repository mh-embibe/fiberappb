package com.embibe.embibetvapp.converter

import com.embibe.embibetvapp.model.test.Schedule
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.objectbox.converter.PropertyConverter

class ScheduleConverter :
    PropertyConverter<Schedule?, String?> {
    override fun convertToEntityProperty(jsonString: String?): Schedule? {
        return if (jsonString == null) {
            null
        } else Gson().fromJson(jsonString, object : TypeToken<Schedule?>() {}.type)
    }

    override fun convertToDatabaseValue(creatureList: Schedule?): String? {
        return if (creatureList == null) {
            null
        } else Gson().toJson(creatureList)
    }
}