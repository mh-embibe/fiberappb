package com.embibe.embibetvapp.converter

import com.embibe.embibetvapp.model.learning.OwnerInfo
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.objectbox.converter.PropertyConverter

class OwnerInfoConverter :
    PropertyConverter<OwnerInfo?, String?> {
    override fun convertToEntityProperty(jsonString: String?): OwnerInfo? {
        return if (jsonString == null) {
            null
        } else Gson().fromJson(jsonString, object : TypeToken<OwnerInfo?>() {}.type)
    }

    override fun convertToDatabaseValue(creatureList: OwnerInfo?): String? {
        return if (creatureList == null) {
            null
        } else Gson().toJson(creatureList)
    }
}