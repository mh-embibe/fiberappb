package com.embibe.embibetvapp.converter

import com.embibe.embibetvapp.model.BannerData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.objectbox.converter.PropertyConverter

class BannerDataConverter :
    PropertyConverter<ArrayList<BannerData>?, String?> {
    override fun convertToEntityProperty(jsonString: String?): ArrayList<BannerData>? {
        return if (jsonString == null) {
            null
        } else Gson().fromJson(jsonString, object : TypeToken<ArrayList<BannerData>?>() {}.type)
    }

    override fun convertToDatabaseValue(creatureList: ArrayList<BannerData>?): String? {
        return if (creatureList == null) {
            null
        } else Gson().toJson(creatureList)
    }
}