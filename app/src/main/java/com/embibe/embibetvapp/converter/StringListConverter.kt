package com.embibe.embibetvapp.converter

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.objectbox.converter.PropertyConverter

class StringListConverter :
    PropertyConverter<List<String>?, String?> {
    override fun convertToEntityProperty(jsonString: String?): List<String>? {
        return if (jsonString == null) {
            null
        } else Gson().fromJson(jsonString, object : TypeToken<List<String>?>() {}.type)
    }

    override fun convertToDatabaseValue(creatureList: List<String>?): String? {
        return if (creatureList == null) {
            null
        } else Gson().toJson(creatureList)
    }
}