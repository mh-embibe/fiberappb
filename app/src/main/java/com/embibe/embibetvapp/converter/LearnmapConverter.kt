package com.embibe.embibetvapp.converter

import com.embibe.embibetvapp.newmodel.LearningMap
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.objectbox.converter.PropertyConverter

class LearnmapConverter :
    PropertyConverter<LearningMap?, String?> {
    override fun convertToEntityProperty(jsonString: String?): LearningMap? {
        return if (jsonString == null) {
            null
        } else Gson().fromJson(jsonString, object : TypeToken<LearningMap?>() {}.type)
    }

    override fun convertToDatabaseValue(creatureList: LearningMap?): String? {
        return if (creatureList == null) {
            null
        } else Gson().toJson(creatureList)
    }
}