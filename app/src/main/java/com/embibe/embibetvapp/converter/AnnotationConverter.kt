package com.embibe.embibetvapp.converter

import com.embibe.embibetvapp.newmodel.Annotation
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.objectbox.converter.PropertyConverter

class AnnotationConverter :
    PropertyConverter<ArrayList<Annotation>?, String?> {
    override fun convertToEntityProperty(jsonString: String?): ArrayList<Annotation>? {
        return if (jsonString == null) {
            null
        } else Gson().fromJson(jsonString, object : TypeToken<ArrayList<Annotation>?>() {}.type)
    }

    override fun convertToDatabaseValue(creatureList: ArrayList<Annotation>?): String? {
        return if (creatureList == null) {
            null
        } else Gson().toJson(creatureList)
    }
}