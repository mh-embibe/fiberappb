package com.embibe.embibetvapp.converter

import com.embibe.embibetvapp.newmodel.Content
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.objectbox.converter.PropertyConverter

class ContentConverter :
    PropertyConverter<List<Content>?, String?> {
    override fun convertToEntityProperty(jsonString: String?): List<Content>? {
        return if (jsonString == null) {
            null
        } else Gson().fromJson(jsonString, object : TypeToken<List<Content>?>() {}.type)
    }

    override fun convertToDatabaseValue(creatureList: List<Content>?): String? {
        return if (creatureList == null) {
            null
        } else Gson().toJson(creatureList)
    }
}