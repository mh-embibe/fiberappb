package com.embibe.embibetvapp.base

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.volley.NoConnectionError
import com.android.volley.VolleyError
import com.embibe.app.utils.Resource
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.application.App.Companion.context
import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.model.LastWatchedContentRes
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.UserData.getExamCode
import com.embibe.embibetvapp.model.UserData.getGoal
import com.embibe.embibetvapp.model.UserData.getGoalCode
import com.embibe.embibetvapp.model.UserData.getGrade
import com.embibe.embibetvapp.model.connection.ConnectionModel
import com.embibe.embibetvapp.model.content.ContentStatusRequest
import com.embibe.embibetvapp.model.content.ContentStatusResponse
import com.embibe.embibetvapp.model.learnSummary.SincerietyRes
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkRequest
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkResponse
import com.embibe.embibetvapp.model.likeAndBookmark.MergedDetailModel
import com.embibe.embibetvapp.model.practise.conceptSequence.ConceptSeqRes
import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.embibe.embibetvapp.model.response.ErrorResponse
import com.embibe.embibetvapp.model.response.Errors
import com.embibe.embibetvapp.model.response.attemptquality.AttemptQualityResponse
import com.embibe.embibetvapp.model.testkg.TestKgRequest
import com.embibe.embibetvapp.network.repo.DataRepo
import com.embibe.embibetvapp.network.retrofit.RetrofitClient
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.activity.Loader
import com.embibe.embibetvapp.ui.progressBar.LoadingManager.Companion.activity
import com.embibe.embibetvapp.ui.progressBar.LoadingManager.Companion.removeDPadListener
import com.embibe.embibetvapp.utils.ConnectionManager
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Status
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.getChildId
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibe.embibetvapp.utils.exceptions.CustomException
import com.embibejio.coreapp.preference.PreferenceHelper
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody
import okio.Buffer
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException
import java.util.*
import kotlin.collections.ArrayList


abstract class BaseViewModel : ViewModel() {

    var isTokenRequired: Boolean = false
    val apiDetails = RetrofitClient.apiDetails!!
    val practiceSummary = RetrofitClient.practiceSummary!!
    var pref = PreferenceHelper()
    private var dataRepo = DataRepo()
    open var errorCode = 0
    private val connectionLiveData: MutableLiveData<ConnectionModel> = MutableLiveData()
    private val TAG = this.javaClass.simpleName
    private var jObjError: JSONObject = JSONObject()
    private var errorMsg: String = ""
    private var TAG_API_ERROR: String = "ERROR_HANDLING"
    val NO_CONTENT_CODE = 204


    /*fetch concept Sequence api data*/
    fun loadConceptsSequenceCoverage(content: Content, callBack: APICallBacks<ConceptSeqRes>) {
        safeApi(
            apiDetails.getConceptsSequenceCoverage(
                content.learning_map.format_id,
                content.learnpath_name,
                content.bundle_id
            ), callBack
        )
    }

    /*get KG graph */
    fun getTestKGGraph(testCode: String, callBack: APICallBacks<List<LinkedTreeMap<Any, Any>>>) {
        safeApi(apiDetails.getTestKGGraph(TestKgRequest(testCode)), callBack)
    }

    /*get current Content status of the content*/
    fun getContentStatus(
        contentId: String,
        contentType: String,
        callBack: APICallBacks<ContentStatusResponse>
    ) {
        safeApi(
            apiDetails.getContentStatus(
                contentId,
                getContentTypeString(contentType),
                getChildId()
            ), callBack
        )
    }

    /*get current Like status of the content*/
    fun getLikeStatus(contentId: String, callBack: APICallBacks<LikeBookmarkResponse>) {
        safeApi(apiDetails.getLikeStatus(getChildId(), "0"), callBack)
    }

    /*get current Bookmark status of the content*/
    fun getBookmarkStatus(contentId: String, callBack: APICallBacks<LikeBookmarkResponse>) {
        safeApi(apiDetails.getBookmarkStatus(getChildId(), contentId), callBack)
    }


    /*get attempt quality*/
    fun getUserAttemptQualityData(
        format_reference: String,
        learnpath_format: String,
        learnpath_name: String,
        callBack: APICallBacks<AttemptQualityResponse>
    ) {
        safeApi(
            apiDetails.getAttemptQualityData(
                format_reference,
                learnpath_format,
                learnpath_name
            ), callBack
        )
    }

    /*get Sinceriety Score*/
    fun getSincerietyScore(
        format_reference: String,
        learnpath_format: String,
        learnpath_name: String,
        type:String,
        callBack: APICallBacks<SincerietyRes>
    ) {
        safeApi(
            apiDetails.getSincerietyScore(
                format_reference,
                learnpath_format,
                learnpath_name,
                type
            ), callBack
        )
    }

    /*like api*/
    fun like(
        contentId: String,
        contentType: String,
        liked: Boolean,
        callBack: APICallBacks<LikeBookmarkResponse>
    ) {
        val req = LikeBookmarkRequest()
        req.contentId = contentId
        req.videoId = contentId
        req.contentType = getContentTypeString(contentType)
        req.liked = liked
        req.childId = getChildId()
        req.contentCategoryType = null
        req.timestamp = convertMilliSecondsToSeconds(System.currentTimeMillis())
        safeApi(apiDetails.likeApi(getChildId(), req), callBack)
    }

    /*Bookmark api*/
    fun bookmark(
        contentId: String,
        contentType: String,
        bookmarked: Boolean,
        callBack: APICallBacks<CommonApiResponse>
    ) {
        val req = LikeBookmarkRequest()
        req.videoId = contentId
        req.contentId = contentId
        req.contentType = getContentTypeString(contentType)
        req.bookmarked = bookmarked
        req.childId = getChildId()
        req.timestamp = convertMilliSecondsToSeconds(System.currentTimeMillis())
        safeApi(apiDetails.bookmarkApi(getChildId(), req), callBack)
    }

    fun getLastWatchedContent(
        source: String,
        bookId: String,
        callBack: APICallBacks<LastWatchedContentRes>
    ) {
        val map = LinkedHashMap<String, String>()
        map["childId"] = getChildId()
        map[AppConstants.SOURCE] = source
        map["goal"] = getGoal()
        map["grade"] = getGrade()
        map["bookId"] = bookId
        safeApi(apiDetails.getLastWatchedContent(map), callBack)
    }

    private fun getContentTypeString(contentType: String): String {
        return when (contentType.toLowerCase(Locale.getDefault())) {
            AppConstants.VIDEO -> AppConstants.STATUS_CONTENT_TYPE_VIDEO
            AppConstants.BOOK -> AppConstants.STATUS_CONTENT_TYPE_BOOK
            AppConstants.COOBO -> AppConstants.STATUS_CONTENT_TYPE_COOBO
            AppConstants.PRACTICE, AppConstants.CHAPTER -> AppConstants.STATUS_CONTENT_TYPE_PRACTISE_CHAPTERS
            AppConstants.LEARN_CHAPTER, AppConstants.LEARN -> AppConstants.STATUS_CONTENT_TYPE_LEARN_CHAPTERS
            AppConstants.SUBJECT -> AppConstants.STATUS_CONTENT_TYPE_SUBJECT
            AppConstants.TEST, AppConstants.CHAPTER_TEST -> AppConstants.STATUS_CONTENT_TYPE_TEST
            else -> contentType
        }
    }

    /*update content status api*/
    fun updatedContentStatus(
        contentId: String,
        contentType: String,
        watched: Int,
        status: String,
        topicLearnPathName: String,
        learnPathName: String,
        learnPathFormatName: String,
        format_id: String,
        concept_id: String,
        length: String
    ) {
        val req = ContentStatusRequest()
        req.childId = getChildId()
        req.content_id = contentId
        req.content_type = getContentTypeString(contentType)
        req.watched_duration = watched
        req.topic_learnpath_name = topicLearnPathName
        req.learnpath_name = learnPathName
        req.learnpath_format_name = learnPathFormatName
        req.format_reference = format_id
        req.concept_id = concept_id
        req.length = length
        req.content_status = status.toUpperCase()
        safeApi(
            apiDetails.updateContentStatus(getChildId(), req),
            object : APICallBacks<CommonApiResponse> {
                override fun onSuccess(model: CommonApiResponse?) {
                    if (model?.success!!) {
                        Utils.isContentStatusUpdatedForQuickLinks = true
                        when (getContentTypeString(contentType)) {
                            AppConstants.STATUS_CONTENT_TYPE_VIDEO -> {
                                Utils.isStatusUpdatedForVideosAndCoobos = true

                            }
                            AppConstants.PRACTICE,
                            AppConstants.STATUS_CONTENT_TYPE_LEARN_CHAPTERS, AppConstants.STATUS_CONTENT_TYPE_PRACTISE_CHAPTERS -> {
                                Utils.isContentStatusUpdatedForPractice = true
                            }
                            AppConstants.TEST -> {
                                Utils.isContentStatusUpdatedForTest = true
                            }
                            AppConstants.STATUS_CONTENT_TYPE_COOBO -> {
                                val preferencesWriter: SharedPreferences =
                                    App.context.getSharedPreferences(
                                        "app_embibe",
                                        Context.MODE_MULTI_PROCESS
                                    )
                                doAsync {
                                    preferencesWriter.edit()
                                        .putBoolean(AppConstants.ISCOOBO_UPDATED, true).commit()
                                    preferencesWriter.edit()
                                        .putBoolean(
                                            AppConstants.ISCOOBO_UPDATED_FOR_QUICKLINKS,
                                            true
                                        ).commit()
                                }
                            }
                        }

                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {

                }

            })
    }

    @SuppressLint("CheckResult")
    fun mergeContentStatusLikeBookmark(
        contentId: String,
        callBack: APICallBacks<MergedDetailModel>
    ) {
        val zipWith = Single.zip(
            apiDetails.getLikeStatus(getChildId(), contentId),
            apiDetails.getBookmarkStatus(getChildId(), contentId),
            BiFunction<
                    Response<LikeBookmarkResponse>,
                    Response<LikeBookmarkResponse>,
                    MergedDetailModel> { mLikeStatus, mBookmarkStatus ->
                // here we get both the results at a time.
                val result = parseLikBookmark(mLikeStatus, mBookmarkStatus)
                return@BiFunction result
            })
        zipWith
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callBack.onSuccess(it)
            }, {
                callBack.onFailed(0, getError(it), "")
            })
    }

    private fun parseLikBookmark(
        mLikeStatus: Response<LikeBookmarkResponse>,
        mBookmarkStatus: Response<LikeBookmarkResponse>
    ): MergedDetailModel {
        val mergedDetailModel = MergedDetailModel()
        mergedDetailModel.like = if (mLikeStatus.isSuccessful) {
            covertRes(mLikeStatus)
        } else null
        mergedDetailModel.bookmark = if (mBookmarkStatus.isSuccessful) {
            covertRes(mBookmarkStatus)
        } else null
        return mergedDetailModel
    }

    @SuppressLint("CheckResult")
    fun <T> makeApiCallLiveData(call: Single<Response<T>>, data: MutableLiveData<Resource<T>>) {
        hitSafeApiCall(call, data, null)
    }

    @SuppressLint("CheckResult")
    fun <T> safeApi(call: Single<Response<T>>, apiCallBack: APICallBacks<T>?) {
        hitSafeApiCall(call, null, apiCallBack!!)
    }

    /*hit api for common */
    @SuppressLint("CheckResult")
    fun <T> hitSafeApiCall(
        call: Single<Response<T>>,
        data: MutableLiveData<Resource<T>>?,
        apiCallBack: APICallBacks<T>?
    ) {
        if (ConnectionManager.instance.hasNetworkAvailable()) {
            call.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    handleResponseSafeApi(it, data, apiCallBack)
                }, {

                    handleError(it, data, apiCallBack)
                })
        } else {
            noNetwork(data, apiCallBack)
        }
    }


    inline fun <reified T> covertToData(it: Resource<T>): T {
        return it.data as T
    }

    private fun <T> covertRes(it: Response<T>): T {
        return it.body() as T
    }

    /*handle the res of api at single place*/
    private fun <T> handleResponseSafeApi(
        response: Response<T>,
        liveData: MutableLiveData<Resource<T>>?,
        apiCallBack: APICallBacks<T>?
    ) {
        if (response.isSuccessful && response.code() != 204) {
            /* code 200..299 excluding 204*/
            if (isTokenRequired) {
                try {
                    val token = response.headers().values(AppConstants.EMBIBETOKEN)[0]
                    PreferenceHelper().put(AppConstants.EMBIBETOKEN, token)
                    PreferenceHelper().put(AppConstants.IS_LOGIN, true)
                    isTokenRequired = false
                } catch (e: Exception) {
                    makeLog("header token not found ", e.localizedMessage ?: "")
                }
            }
            //makeLog("result", toJson(response.body()))
            apiCallBack?.onSuccess(covertRes(response))
            liveData?.postValue(Resource(Status.SUCCESS, response.body(), response.message()))
        } else {
            val request = response.raw().request
            with(request) {
                SegmentUtils.trackApiFailure(
                    url.toString(),
                    body?.let { bodyToString(it) },
                    response.code()
                )
            }
            handleStatusCode(response, liveData, apiCallBack)
        }
    }


    private fun hideProgress() {
        Handler().postDelayed({
            if (activity != null) {
                removeDPadListener(Loader.progressView!!)
                activity!!.hideActivity()
            }
        }, 1000)
    }

    /* status code validation for each response*/
    private fun <T> handleStatusCode(
        response: Response<T>,
        resultData: MutableLiveData<Resource<T>>?, apiCallBack: APICallBacks<T>?
    ) {
        val model = getErrorBody(response)
        model.code = response.code()
        val resCodeValue = getCodeName(response.code())
        makeLog("$TAG_API_ERROR code : ${response.code()}", " $resCodeValue  $errorMsg")

        apiCallBack?.onFailed(response.code(), resCodeValue, errorMsg)
        resultData?.postValue(Resource(Status.ERROR, model, "$resCodeValue $errorMsg"))
    }

    /* status code validation for each response*/
    private fun <T> handleStatusCodeWithProgress(
        response: Response<T>,
        resultData: MutableLiveData<Resource<T>>?, apiCallBack: APIProgressCallBacks<T>?
    ) {
        val model = getErrorBody(response)
        model.code = response.code()
        val resCodeValue = getCodeName(response.code())
        makeLog("$TAG_API_ERROR code : ${response.code()}", " $resCodeValue  $errorMsg")

        apiCallBack?.onFailed(response.code(), resCodeValue, errorMsg)
        resultData?.postValue(Resource(Status.ERROR, model, "$resCodeValue $errorMsg"))
    }

    private fun <T> getErrorBody(response: Response<T>): ErrorResponse {
        var model = ErrorResponse()
        errorMsg = ""
        try {
            jObjError = JSONObject()
            jObjError = JSONObject(response.errorBody()!!.string())
            if (jObjError.has("_error")) {
                jObjError = jObjError.getJSONObject("_error")
                errorMsg = jObjError.getString("message")
            } else if (jObjError.has("message")) {
                errorMsg = jObjError.getString("message")
            } else if (jObjError.has("errors")) {
                val errors = jObjError.getJSONObject("errors")
                val errorModel: Errors = fromJson(errors.toString())
                if (!errorModel.otp.isNullOrEmpty()) {
                    errorMsg = errorModel.otp[0]
                } else if (!errorModel.login.isNullOrEmpty()) {
                    errorMsg = errorModel.login[0]
                }


            }
            model = fromJson(jObjError.toString())
        } catch (e: java.lang.Exception) {
            errorMsg = e.toString()
        }
        return model
    }

    /* api errors handled here*/
    fun <T> handleError(
        it: Throwable,
        liveDataObj: MutableLiveData<Resource<T>>?,
        callBack: APICallBacks<T>?
    ) {
        try {
            callBack?.onFailed(500, getError(it), "")
            val error = getError(it)
            val model = ErrorResponse()
            model.code = 500
            model.error = error
            /*callback to liveData*/
            liveDataObj?.value = Resource(Status.ERROR, model, error)
        } catch (e: Exception) {
            makeLog("Exception", e.localizedMessage ?: "Error")
        }
    }

    /* api errors handled here*/
    fun <T> handleErrorWithProgress(
        it: Throwable,
        liveDataObj: MutableLiveData<Resource<T>>?,
        callBack: APIProgressCallBacks<T>?
    ) {
        try {
            callBack?.onFailed(500, getError(it), "")
            val error = getError(it)
            val model = ErrorResponse()
            model.code = 500
            model.error = error
            /*callback to liveData*/
            liveDataObj?.value = Resource(Status.ERROR, model, error)
        } catch (e: Exception) {
            makeLog("Exception", e.localizedMessage ?: "Error")
        }
    }

    private fun getCodeName(code: Int): String {
        return when (code) {
            204 -> "No Content Success"
            401 -> "Un authenticated"
            400 -> "Bad Request"
            403 -> "Forbidden"
            404 -> "Not found"
            500 -> "Internal server error"
            501 -> "Not implemented"
            502 -> "Bad gateway"
            503 -> "Service unavailable"
            else -> "Unknown"
        }
    }

    /* api errors message obtained here*/
    fun getError(it: Throwable): String {
        val error = "UnknownException"
        try {
            when (it) {
                is IllegalStateException -> {
                    return it.localizedMessage ?: error
                }
                is UnknownHostException -> {
                    return it.localizedMessage ?: error
                }
                is HttpException -> {
                    return it.localizedMessage ?: error
                }
                is NoConnectionError -> {
                    return it.localizedMessage ?: error
                }
                is CustomException -> {
                    return it.localizedMessage ?: error
                }
                else -> {
                    return error
                }
            }
        } catch (e: Exception) {
            return e.localizedMessage ?: error
        }
    }

    private fun showProgress() {
        if (!DataManager.isLoading) {
            DataManager.isLoading = true
            val intent = Intent(context, Loader::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }

    /*update UI to Connection lost! on API call*/
    private fun <T> noNetwork(
        data: MutableLiveData<Resource<T>>?,
        apiCallBack: APICallBacks<T>?
    ) {
        makeLog("no internet ", AppConstants.NO_NETWORK)
        ConnectionModel.instance.isConnected = false
        ConnectionModel.instance.msg = AppConstants.NO_NETWORK
        connectionLiveData.value = ConnectionModel.instance
        apiCallBack?.onFailed(
            ApiConstants.API_CODE_NO_NETWORK,
            AppConstants.NO_NETWORK,
            AppConstants.NO_NETWORK
        )
        val model = ErrorResponse()
        model.code = ApiConstants.API_CODE_NO_NETWORK
        model.error = AppConstants.NO_NETWORK
        data?.postValue(Resource(Status.ERROR, model, AppConstants.NO_NETWORK))
    }

    /*update UI to Connection lost! on API call*/
    private fun <T> noNetworkWithProgress(
        data: MutableLiveData<Resource<T>>?,
        apiCallBack: APIProgressCallBacks<T>?
    ) {
        makeLog("no internet ", AppConstants.NO_NETWORK)
        ConnectionModel.instance.isConnected = false
        ConnectionModel.instance.msg = AppConstants.NO_NETWORK
        connectionLiveData.value = ConnectionModel.instance
        apiCallBack?.onFailed(
            ApiConstants.API_CODE_NO_NETWORK,
            AppConstants.NO_NETWORK,
            AppConstants.NO_NETWORK
        )
        val model = ErrorResponse()
        model.code = ApiConstants.API_CODE_NO_NETWORK
        model.error = AppConstants.NO_NETWORK
        data?.postValue(Resource(Status.ERROR, model, AppConstants.NO_NETWORK))
    }

    interface ErrorCallBacks {
        fun onRetry(msg: String)
        fun onDismiss()
    }

    interface APICallBacks<T> {
        fun onSuccess(model: T?)
        fun onFailed(code: Int, error: String, msg: String)
    }

    interface APIProgressCallBacks<T> {
        fun onSuccess(model: T?)
        fun onFailed(code: Int, error: String, msg: String)
        fun onProgress(progress: Long, total: Long)

    }

    interface APIMergeCallBack {
        fun onSuccessCallBack(it: Any)
        fun onErrorCallBack(it: Throwable)
    }

    interface APIMergeCallBacks<T> {
        fun onSuccess(model: T?)
        fun onFailed(code: Int, error: String, msg: String)
    }

    interface APICallBackVolley {
        fun <T> onSuccessCallBack(it: T)
        fun onErrorCallBack(error: VolleyError)
    }

    interface DataCallback<T> {
        fun onSuccess(model: T?)
    }

    interface BacKPressListener {
        fun onCustomBackPressed()
    }


    private fun toast(msg: String) =
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

    /*prints a log on warning tag*/
    fun makeLog(tag: String, msg: String) = Log.w(tag, msg)

    inline fun <reified T> fromJson(json: String): T {
        return Gson().fromJson(json, object : TypeToken<T>() {}.type)
    }

    fun toJson(res: Any?) = Gson().toJson(res)!!


    /* save sections in local database */
    @JvmOverloads
    fun saveResultToDb(
        inputResults: List<ResultsEntity>,
        page: String,
        subject: String? = null,
        callback: DataCallback<String>? = null
    ) {
        makeLog("app", "SaveToDB initiated: $page")
        //dataRepo.clearBox()
        try {
            doAsync {
                synchronized(inputResults)
                {
                    for (resultItem in inputResults) {
                        val childId = UserData.getChildId().toLong()
                        resultItem.subject = subject ?: "all"
                        resultItem.child_id = childId
                        resultItem.page = page
                        resultItem.grade = getGrade()
                        resultItem.goal = getGoalCode()
                        resultItem.exam = getExamCode()
                        dataRepo.saveSection(resultItem)
                    }
                }
                uiThread {
                    PreferenceHelper().put(page, System.currentTimeMillis())
                    callback?.onSuccess("")
                }

            }
        } catch (e: Exception) {
            makeLog("app", "Database : Exception ${e.printStackTrace()}")
            PreferenceHelper().put(page, System.currentTimeMillis())
        }
    }

    @JvmOverloads
    fun fetchSectionByPageName(pageName: String, subject: String? = null): List<ResultsEntity> {
        return dataRepo.getAllSectionByPage(
            pageName, UserData.getChildId().toLong(), subject ?: "all"
        )
    }

    @JvmOverloads
    fun fetchBannerSectionByPageName(pageName: String, subject: String? = null): List<BannerData>? {
        val section = dataRepo.getBannerSectionByPage(
            pageName,
            UserData.getChildId().toLong(),
            subject
        )
        if (section != null) {
            if (section.content != null && section.content!!.isNotEmpty())
                return section.content!![0].banner_data
        }
        return null
    }

    @JvmOverloads
    fun getSectionById(id: Long, page: String? = null, subject: String? = null): ResultsEntity? {
        return dataRepo.getSection(id, page, subject)
    }

    private fun getContentIdFromResult(resultItem: Results): List<String>? {
        val contentIdList = ArrayList<String>()
        for (item in resultItem.content) {
            contentIdList.add(item.id)
        }
        return contentIdList
    }

    /*Bookmark api*/
    fun likeBookmarkTest(
        contentId: String,
        contentType: String,
        activityType: String,
        status: Boolean,
        callBack: APICallBacks<CommonApiResponse>
    ) {
        val req = LikeBookmarkRequest()
        req.contentId = contentId
        req.contentType = getContentTypeString(contentType)
        req.activityType = activityType
        req.status = status
        req.childId = getChildId()
        req.timestamp = convertMilliSecondsToSeconds(System.currentTimeMillis())

        safeApi(apiDetails.bookmarkTestApi(req), callBack)
    }

    private fun convertMilliSecondsToSeconds(milliseconds: Long) = (milliseconds / 1000)

    private fun bodyToString(request: RequestBody): String {
        return try {
            val copy: RequestBody = request
            val buffer = Buffer()
            copy.writeTo(buffer)
            buffer.readUtf8()
        } catch (e: IOException) {
            "did not work"
        }
    }
}