package com.embibe.embibetvapp.base

import android.content.*
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.leanback.app.RowsSupportFragment
import androidx.leanback.widget.ArrayObjectAdapter
import androidx.leanback.widget.Presenter
import androidx.leanback.widget.Row
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.network.repo.DataRepo
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.activity.*
import com.embibe.embibetvapp.ui.progressBar.LoadingManager
import com.embibe.embibetvapp.utils.CardListRow
import com.embibe.embibetvapp.utils.ConnectionManager
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.insertBundle

open class BaseRowsSupportFragment : RowsSupportFragment(), LoadingManager,
    ConnectionManager.ConnectivityReceiverListener {

    private lateinit var bookDetails: List<ResultsEntity>
    private var layoutId = R.layout.lb_row_header
    var paginationSize: Int = 20
    private lateinit var networkChangeReceiver: BroadcastReceiver
    private var isDisconnected = false
    lateinit var dataRepo: DataRepo

    val preferences: SharedPreferences =
        App.context.getSharedPreferences(
            "app_embibe",
            Context.MODE_MULTI_PROCESS
        )

    fun showToast(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }

    fun makeLog(msg: String) {
        Log.w("app", msg)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("usedLayoutHeader", "lb_row_header$layoutId")
        Log.d("userId", "" + R.dimen.lb_browse_padding_top)
        Log.d("userId", "" + R.dimen.lb_browse_rows_margin_top)
        dataRepo = DataRepo()
    }

    override fun onResume() {
        super.onResume()
        registerConnectionListener()
    }

    private fun registerConnectionListener() {
        ConnectionManager.connectivityReceiverListener = this
        networkChangeReceiver = ConnectionManager.NetworkReceiver()
        val intentFilter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        App.context.registerReceiver(networkChangeReceiver, intentFilter)
    }

    fun calculateOffset(section: ResultsEntity): Int {
        return section.content?.size ?: section.size
    }


    fun setBookDetails(filter: List<ResultsEntity>): List<ResultsEntity> {
        bookDetails = filter
        return bookDetails
    }

    override fun onPause() {
        super.onPause()
        try {
            App.context.unregisterReceiver(networkChangeReceiver)
        } catch (e: Exception) {
            /*ignore the exception*/
        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        //makeLog("NetworkConnectionChanged $isConnected")
        when (isConnected) {
            true -> {
                if (isDisconnected) {
                    showToast("Internet connected")
                    isDisconnected = false
                    /* if (Utils.dialogFragment.isVisible) {
                         Utils.dialogFragment.dismiss()
                     }*/
                }
            }
            false -> {
                isDisconnected = true
                showToast("Internet connection lost! please check your WiFi")
                //showNoNetwork()
            }
        }


    }

    fun getContentItems(video: ResultsEntity): List<Content> {
        return video.content!!
    }

    fun onClickItem(
        item: Content,
        row: Row,
        mRowsAdapter: ArrayObjectAdapter,
        itemViewHolder: Presenter.ViewHolder
    ) {
        val indexOfItem = ((row as CardListRow).adapter as ArrayObjectAdapter).indexOf(item)
        val indexOfRow = mRowsAdapter.indexOf(row)
        SegmentUtils.trackEventHomeTileClick(item, row.headerItem.name, indexOfRow, indexOfItem)
        makeLog("type ${item.type}")
        when (item.type.toLowerCase()) {
            AppConstants.SUBJECT -> {
                if (item.subject != "All Subjects") {
                    val intent = Intent(context, SubjectInfoActivity::class.java)
                    insertBundle(item, intent)
                    startActivity(intent)
                }
            }
            AppConstants.VIDEO, AppConstants.CONTINUE_LEARNING -> {
                gotoDetails(item)
            }
            AppConstants.COOBO -> {
                gotoDetails(item)
            }
            AppConstants.BOOK -> {
                gotoBookDetails(item, AppConstants.NAV_TYPE_BOOK)
            }
            AppConstants.LEARN_CHAPTER -> {
                //Utils.trackEventSubjectSelected(item)
//                gotoBookDetails(item, AppConstants.NAV_TYPE_SYLLABUS)
                gotoLearnSummary(item, AppConstants.NAV_TYPE_SYLLABUS)

            }
            AppConstants.STORY -> {

            }
            AppConstants.PRACTICE, AppConstants.CHAPTER -> {
                if (item.id != "") {
                    Utils.loadPracticeSummaryInfo(item, context)
                    val intent = Intent(context, PracticeSummaryActivity::class.java)
                    insertBundle(item, intent)
                    startActivity(intent)
                }
            }
            AppConstants.LEARN -> {
                val intent = Intent(context, SyllabusSelectionActivity::class.java)
                insertBundle(item, intent)
                startActivity(intent)
            }
            AppConstants.TEST -> {
//                val intent = Intent(context, TestDetailActivity::class.java)
//                insertBundle(item, intent)
//                startActivity(intent)
                gotoTestSummary(item, AppConstants.NAV_NAME_TEST)
            }
            AppConstants.CREATE_TEST -> {
                val intent = Intent(context, CreateNewTestActivity::class.java)
                insertBundle(item, intent)
                startActivity(intent)
            }
            AppConstants.PAJ -> {
                val intent = Intent(context, TestFeedbackActivity::class.java)
                insertBundle(item, intent)
                startActivity(intent)
            }
            else -> {
                gotoDetails(item)
            }
        }
    }

    private fun gotoBookDetails(item: Content, type: String) {
        val intent = Intent(context, BookDetailActivity::class.java)
        insertBundle(item, intent)
        intent.putExtra(AppConstants.NAV_NAME_TYPE, type.toString())
        startActivity(intent)
    }

    private fun gotoLearnSummary(item: Content, type: String) {
        Utils.loadLearnSummaryAsync(item, context)
        val intent = Intent(context, LearnSummaryActivity::class.java)
        insertBundle(item, intent)
        startActivity(intent)
    }

    private fun gotoTestSummary(item: Content, type: String) {
        Utils.loadTestSummaryAsync(item, context)
        val intent = Intent(context, TestSummaryActivity::class.java)
        insertBundle(item, intent)
        startActivity(intent)
    }

    private fun gotoDetails(item: Content) {
        Utils.loadConceptsAsync(item, context)
        val intent = Intent(context, DetailActivity::class.java)
        insertBundle(item, intent)
        startActivity(intent)
    }


    fun getContent(arguments: Bundle?): Content {
        return arguments?.getParcelable<Content>(AppConstants.CONTENT) as Content
    }

    infix fun <T> Collection<T>.deepEqualToIgnoreOrder(other: Collection<T>): Boolean {
        // check collections aren't same
        if (this !== other) {
            // fast check of sizes
            if (this.size != other.size) return false
            val areNotEqual = this.asSequence()
                // check other contains next element from this
                .map { it in other }
                // searching for first negative answer
                .contains(false)
            if (areNotEqual) return false
        }
        // collections are same or they are contains same elements
        return true
    }


}