package com.embibe.embibetvapp.base

import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.ui.progressBar.LoadingManager
import com.embibejio.coreapp.events.EventParams
import com.embibejio.coreapp.events.SegmentEvents
import com.embibejio.coreapp.events.SegmentIO
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import org.jetbrains.anko.doAsync
import java.util.*

abstract class BaseAppFragment : Fragment(), LoadingManager {

    private var bannerSlidePosition: Int = 0
    var bannerData: BannerData? = null

    var timerBookmark = Timer()
    var timerLike = Timer()
    val interval: Long = 1000
    val retryAttempt: Int = 2

    fun showToast(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }

    fun toast(msg: String) {
        //Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }

    fun makeLog(msg: String) {
        Log.w("app", msg)
    }


    fun makeGridCenter(recyclerView: RecyclerView) {
        val layoutManager = FlexboxLayoutManager(context)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.CENTER
        layoutManager.alignItems = AlignItems.CENTER
        recyclerView.layoutManager = layoutManager

    }

    /* MainBtnClick*/
    fun trackEventHomeMainBtnClick(data: BannerData?, slideType: String) {
        val eventParams = EventParams()
        eventParams.button_type = SegmentEvents.BUTTON_TYPE_LEARN
        eventParams.slide_title = data?.title
        eventParams.slide_type = slideType
        eventParams.slide_number = bannerSlidePosition.toString()
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_HEROBANNER_MAIN_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_HEROBANNER,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    /* banner change*/
    fun trackEventHomeBannerChange(data: BannerData, position: Int, slideType: String) {
        bannerData = data
        bannerSlidePosition = position
        val eventParams = EventParams()
        //eventParams.content_type = ""
        eventParams.slide_number = position.toString()
        eventParams.button_type = SegmentEvents.BUTTON_TYPE_LEARN
        eventParams.slide_title = data.title
        eventParams.slide_type = slideType

        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_HEROBANNER_SLIDE_CHANGE,
                SegmentEvents.NAV_ELEMENT_HEROBANNER,
                SegmentEvents.EVENT_TYPE_SLIDE_CHANGE,
                eventParams
            )
        }
    }

    /*MoreInfoBtnFocus */
    fun trackEventHomeMoreInfoBtnFocus(data: BannerData?) {

        val eventParams = EventParams()
        eventParams.button_type = SegmentEvents.BUTTON_TYPE_MORE_INFO
        eventParams.slide_title = data?.title
        eventParams.slide_type = ""
        eventParams.slide_number = bannerSlidePosition.toString()
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_HEROBANNER_MOREINFO_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_HEROBANNER,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    /* MoreInfoBtnClick*/
    fun trackEventHomeMoreInfoBtnClick(data: BannerData?, slideType: String) {
        val eventParams = EventParams()
        eventParams.button_type = SegmentEvents.BUTTON_TYPE_MORE_INFO
        eventParams.slide_title = data?.title
        eventParams.slide_type = slideType
        eventParams.slide_number = bannerSlidePosition.toString()
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_HEROBANNER_MOREINFO_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_HEROBANNER,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    /* MainBtnFocus */
    fun trackEventHomeMainBtnFocus(data: BannerData?, slideType: String) {
        val eventParams = EventParams()
        eventParams.button_type = SegmentEvents.BUTTON_TYPE_LEARN
        eventParams.slide_title = data?.title
        eventParams.slide_type = slideType
        eventParams.slide_number = bannerSlidePosition.toString()
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_HEROBANNER_MAIN_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_HEROBANNER,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun showErrorToast(error: String, msg: String) {
        val message = "$msg (Error : $error)"
        Toast.makeText(App.context, message, Toast.LENGTH_SHORT).show()
    }
}