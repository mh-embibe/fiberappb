package com.embibe.embibetvapp.base

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.media.AudioManager
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.network.repo.DataRepo
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.activity.PracticeActivity
import com.embibe.embibetvapp.ui.activity.SearchActivity
import com.embibe.embibetvapp.ui.interfaces.ConversionCallback
import com.embibe.embibetvapp.ui.interfaces.VoiceListener
import com.embibe.embibetvapp.ui.progressBar.LoadingManager
import com.embibe.embibetvapp.utils.ConnectionManager
import com.embibe.embibetvapp.utils.ExceptionHandler
import com.embibe.embibetvapp.utils.SpeechToTextConverter
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.showToast
import com.embibejio.coreapp.application.LibApp
import java.util.*

open class BaseFragmentActivity : FragmentActivity(),
    ConnectionManager.ConnectivityReceiverListener, LoadingManager {

    private lateinit var networkChangeReceiver: BroadcastReceiver
    var isDisconnected = false

    var timerBookmark = Timer()
    var timerLike = Timer()
    val interval: Long = 1000
    val retryAttempt: Int = 2
    lateinit var dataRepo: DataRepo
    lateinit var voiceListener: VoiceListener
    private var speechToTextConverter: SpeechToTextConverter? = null
    val classTag = BaseFragmentActivity::class.java.simpleName

    private var speechTimer: CountDownTimer? = null
    private val speechTimerDuration = 10000L // 10 secs
    private var elapsedSpeechTimer = 0L
    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.setDefaultUncaughtExceptionHandler(ExceptionHandler(this))
        super.onCreate(savedInstanceState)
        App.activityContext = this
        dataRepo = DataRepo()
        pauseMusic()
        initializeSpeechToText()
    }

    private fun initializeSpeechToText() {
        speechToTextConverter = SpeechToTextConverter(object : ConversionCallback {
            override fun onReady() {
                Log.i(classTag, "On Ready")
                //startMicSearchAnimation()
                Toast.makeText(baseContext, "Speak Now!", Toast.LENGTH_SHORT).show()
            }

            override fun onSuccess(result: ArrayList<String>?) {
                Log.i(classTag, "On Success")
                if (result!!.isNotEmpty()) {
                    val searchQuery = result[0].toLowerCase(Locale.ENGLISH)
                    if (::voiceListener.isInitialized)
                        voiceListener.onVoiceSuccess(searchQuery)
                }
                speechTimer?.cancel()
                // finishVoiceSearch()
            }

            override fun onCompletion() {
                Log.i(classTag, "On Completion")
            }

            override fun onErrorOccurred(errorMessage: String) {
                Log.i(classTag, "On Error $errorMessage")
                speechTimer?.cancel()
                voiceListener.onVoiceFailed(errorMessage)
                Toast.makeText(baseContext, "Please Speak Again!", Toast.LENGTH_SHORT).show()
                //finishVoiceSearch()
            }

        })

        speechToTextConverter?.initialize(this)
    }

    open fun startRecognition() {
        if (Utils.hasPermission(this, Manifest.permission.RECORD_AUDIO)) {
            speechToTextConverter?.startListening()
            setSpeechRecognitionTimer()
        } else {
            Toast.makeText(
                this,
                resources.getString(R.string.audio_permission_not),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun setSpeechRecognitionTimer() {
        elapsedSpeechTimer = 0L
        speechTimer = object : CountDownTimer(speechTimerDuration, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                elapsedSpeechTimer = (speechTimerDuration - millisUntilFinished) / 1000
                Log.i(PracticeActivity.TAG, "$elapsedSpeechTimer")
            }

            override fun onFinish() {
                speechTimer?.cancel()
            }
        }
        speechTimer?.start()
    }

    fun getContent(intent: Intent): Content {
        return intent.getParcelableExtra(com.embibe.embibetvapp.constant.AppConstants.CONTENT) as Content
    }

    fun getContent(arguments: Bundle?): Content {
        return arguments?.getParcelable<Content>(com.embibe.embibetvapp.constant.AppConstants.CONTENT) as Content
    }


    override fun onResume() {
        super.onResume()
        pauseMusic()
        speechToTextConverter?.setRecognitionListener()
        registerConnectionListener()
    }

    override fun onStop() {
        speechTimer?.cancel()
        speechToTextConverter?.releaseListener(true)
        super.onStop()
    }


    private fun registerConnectionListener() {
        ConnectionManager.connectivityReceiverListener = this
        networkChangeReceiver = ConnectionManager.NetworkReceiver()
        val intentFilter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        App.context.registerReceiver(networkChangeReceiver, intentFilter)
    }

    override fun onPause() {
        speechToTextConverter?.releaseListener(false)
        super.onPause()
        try {
            App.context.unregisterReceiver(networkChangeReceiver)
        } catch (e: Exception) {
            /*ignore the exception*/
        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        //makeLog("NetworkConnectionChanged $isConnected")
        when (isConnected) {
            true -> {
                if (isDisconnected) {
                    showToast(this, "Internet connected")
                    isDisconnected = false
                    /* if (Utils.dialogFragment.isVisible) {
                         Utils.dialogFragment.dismiss()
                     }*/
                }
            }
            false -> {
                isDisconnected = true
                showToast(this, "Internet connection lost! please check your WiFi")
                //showNoNetwork()
            }
        }
    }

    private fun showNoNetwork() {
        Utils.showError(
            App.activityContext,
            ApiConstants.API_CODE_NO_NETWORK,
            object : BaseViewModel.ErrorCallBacks {
                override fun onRetry(msg: String) {
                    if (ConnectionManager.instance.hasNetworkAvailable()) {
                        if (Utils.dialogFragment.isVisible) {
                            Utils.dialogFragment.dismiss()
                        }
                    } else {
                        showNoNetwork()
                    }
                }

                override fun onDismiss() {
                }
            })
    }

    fun makeLog(msg: String) {
        Log.w("app-log", msg)
    }

    fun makeLogI(msg: String) {
        Log.i("app-log", msg)
    }

    private fun pauseMusic() {
        val mAudioManager =
            this.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        if (mAudioManager.isMusicActive) {
            val i = Intent("com.android.music.musicservicecommand")
            i.putExtra("command", "pause")
            this.sendBroadcast(i)
        }
    }

    fun closeKeyBoard(view: View?) {
        if (view != null) {
            val imm =
                LibApp.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun showErrorToast(error: String, msg: String) {
        val message = "$msg (Error : $error)"
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun permissionsCheck() {

        val permissionsList = ArrayList<String>()
        if (!isPermissionGranted(permissionsList, Manifest.permission.RECORD_AUDIO))
            if (permissionsList.size > 0) {
                requestPermissions(
                    permissionsList.toTypedArray(),
                    SearchActivity.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS
                )
                return
            }
    }


    private fun isPermissionGranted(
        permissionsList: MutableList<String>,
        permission: String
    ): Boolean {

        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission)

            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            SearchActivity.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS -> {
                val perms = HashMap<String, Int>()
                perms[Manifest.permission.RECORD_AUDIO] = PackageManager.PERMISSION_GRANTED

                for (i in permissions.indices)
                    perms[permissions[i]] = grantResults[i]

                if (perms[Manifest.permission.RECORD_AUDIO] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(SearchActivity.TAG, "Record Audio permission granted")
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}