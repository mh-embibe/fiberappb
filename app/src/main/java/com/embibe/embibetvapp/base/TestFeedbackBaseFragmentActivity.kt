package com.embibe.embibetvapp.base

import com.embibe.embibetvapp.ui.interfaces.CommonAndroidAPI

open class TestFeedbackBaseFragmentActivity : BaseFragmentActivity(),
    CommonAndroidAPI.AndroidAPIOnCallListener {
    override val question: String?
        get() = TODO("Not yet implemented")

    override fun getQuestion(questionCode: String?): String? {
        TODO("Not yet implemented")
    }

    override val concept: String?
        get() = TODO("Not yet implemented")
    override val hint: String?
        get() = TODO("Not yet implemented")

    override fun setAnswer(): String? {
        TODO("Not yet implemented")
    }

    override fun getSections(): String? {
        TODO("Not yet implemented")
    }

    override fun getAnswer(): String? {
        TODO("Not yet implemented")
    }

    override fun getQuestionSet(sectionId: String?): String? {
        TODO("Not yet implemented")
    }

    override fun getData(): String? {
        TODO("Not yet implemented")
    }

    override fun getData(data: String) {
        TODO("Not yet implemented")
    }

    override fun getEvent(data: String) {
        TODO("Not yet implemented")
    }

    override fun sendToFeedback(testCode: String) {
        TODO("Not yet implemented")
    }

    override fun showLoader() {
        TODO("Not yet implemented")
    }

    override fun closeLoader() {
        TODO("Not yet implemented")
    }

    override fun hideKeyboard() {
        TODO("Not yet implemented")
    }

    override fun focusUp() {
    }

    override fun selectAnswer(value: String?) {
        TODO("Not yet implemented")
    }

    override fun deselectAnswer(value: String?) {
        TODO("Not yet implemented")
    }

    override fun inputAnswer(value: String?) {
        TODO("Not yet implemented")
    }

    override fun close() {
        TODO("Not yet implemented")
    }

    override val questionNumber: Int
        get() = TODO("Not yet implemented")
    override val sectionName: String?
        get() = TODO("Not yet implemented")
    override val chapterJson: String?
        get() = TODO("Not yet implemented")
    override val chapterName: String?
        get() = TODO("Not yet implemented")
    override val singQuestionJson: String?
        get() = TODO("Not yet implemented")
    override val questionListJson: String?
        get() = TODO("Not yet implemented")

    override fun showConcept(): Boolean? {
        return true
    }

    override fun focusDown() {
    }

    override fun focusRight() {
    }

    override fun focusLeft() {
    }

    override fun initVoiceInput(value: String) {

    }

    override fun getSelectedQuestion(qListJson: String) {
        TODO("Not yet implemented")
    }

    override fun setQuestion(singQuestionJson: String) {
        TODO("Not yet implemented")
    }

    override fun getSelectedVideo(videoData: String) {

    }

    override fun movePAJNextActivity(data: String) {

    }


}