package com.embibe.embibetvapp.newmodel

import com.embibe.embibetvapp.converter.StringListConverter
import io.objectbox.annotation.Convert
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import java.io.Serializable

@Entity
class Section : Serializable {
    @Id
    var objId: Long = 0
    var type: String = ""
        get() = getDefaultString(field)
    var section_id: Long = 0
        get() = getDefaultLong(field)
    var section_name: String = ""
        get() = getDefaultString(field)
    var available: Int = 0
        get() = getDefaultInt(field)
    var start: Int = 0
        get() = getDefaultInt(field)
    var end_count: Int = 0
        get() = getDefaultInt(field)
    var child_id: Long = 0
        get() = getDefaultLong(field)
    var parent_id: Long = 0
        get() = getDefaultLong(field)
    var page: String = ""
        get() = getDefaultString(field)
    var subject: String = ""
        get() = getDefaultString(field)

    @Convert(
        converter = StringListConverter::class,
        dbType = String::class
    )
    var contents: List<String>? = null


    private fun getDefaultString(field: String): String {
        return field
    }

    private fun getDefaultInt(field: Int): Int {
        return field
    }

    private fun getDefaultDouble(field: Double): Double {
        return field
    }

    private fun getDefaultLong(field: Long): Long {
        return field
    }
}