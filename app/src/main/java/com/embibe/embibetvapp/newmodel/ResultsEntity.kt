package com.embibe.embibetvapp.newmodel

import android.os.Parcel
import android.os.Parcelable
import com.embibe.embibetvapp.converter.ContentConverter
import com.google.gson.annotations.SerializedName
import io.objectbox.annotation.Convert
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
class ResultsEntity() : Parcelable {
    @Id
    var objId: Long = 0
    var type: String = ""
    var child_id: Long = 0
    var parent_id: Long = 0
    var page: String = ""
    var grade: String = ""
    var goal: String = ""
    var exam: String = ""
    var subject: String = ""
    var section_name: String = ""

    @SerializedName("section_id")
    var sectionId: Long = 0


    var offset: Int = 0

    var size: Int = 0

    var content_section_type: String = ""

    val total: Int = 0

    @Convert(
        converter = ContentConverter::class,
        dbType = String::class
    )
    @SerializedName("content")
    var content: ArrayList<
            Content>? = null

    //lateinit var contentList: ToMany<Content>

    @SerializedName("available")
    var available: String? = null

    @SerializedName("start")
    var start: String? = null

    @SerializedName("end_count")
    var endCount: String? = null

    constructor(parcel: Parcel) : this() {
        objId = parcel.readLong()
        type = parcel.readString().toString()
        child_id = parcel.readLong()
        parent_id = parcel.readLong()
        page = parcel.readString().toString()
        grade = parcel.readString().toString()
        goal = parcel.readString().toString()
        exam = parcel.readString().toString()
        subject = parcel.readString().toString()
        section_name = parcel.readString().toString()
        sectionId = parcel.readLong()
        offset = parcel.readInt()
        size = parcel.readInt()
        content_section_type = parcel.readString().toString()
        available = parcel.readString()
        start = parcel.readString()
        endCount = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(objId)
        parcel.writeString(type)
        parcel.writeLong(child_id)
        parcel.writeLong(parent_id)
        parcel.writeString(page)
        parcel.writeString(grade)
        parcel.writeString(goal)
        parcel.writeString(exam)
        parcel.writeString(subject)
        parcel.writeString(section_name)
        parcel.writeLong(sectionId)
        parcel.writeInt(offset)
        parcel.writeInt(size)
        parcel.writeString(content_section_type)
        parcel.writeString(available)
        parcel.writeString(start)
        parcel.writeString(endCount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ResultsEntity> {
        override fun createFromParcel(parcel: Parcel): ResultsEntity {
            return ResultsEntity(parcel)
        }

        override fun newArray(size: Int): Array<ResultsEntity?> {
            return arrayOfNulls(size)
        }
    }


}