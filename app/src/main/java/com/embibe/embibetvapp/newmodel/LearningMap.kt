package com.embibe.embibetvapp.newmodel

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


class LearningMap() : Parcelable {

    var concepts: List<String>? = arrayListOf()

    @SerializedName("concept_id")
    var conceptId: String = ""
        get() = if (field.isNullOrEmpty()) "" else field

    @SerializedName("lpcode")
    var lpcode: String = ""
        get() = getDefaultString(field)

    @SerializedName("lm_name")
    var lm_name: String = ""
        get() = getDefaultString(field)

    @SerializedName("format_id")
    var format_id: String = ""
        get() = getDefaultString(field)

    @SerializedName("lmCode")
    var lmCode: String = ""
        get() = getDefaultString(field)

    @SerializedName("exam")
    var exam: String = ""
        get() = getDefaultString(field)

    @SerializedName("goal")
    var goal: String = ""
        get() = getDefaultString(field)

    @SerializedName("unit")
    var unit: String = ""
        get() = getDefaultString(field)

    @SerializedName("grade")
    var grade: String = ""
        get() = getDefaultString(field)

    @SerializedName("topic")
    var topic: String = ""
        get() = getDefaultString(field)

    @SerializedName("source")
    var source: String = ""
        get() = getDefaultString(field)

    @SerializedName("subject")
    var subject: String = ""
        get() = getDefaultString(field)

    @SerializedName("subject_code")
    var subjectCode: String = ""
        get() = getDefaultString(field)

    @SerializedName("exam_code")
    var examCode: String = ""
        get() = getDefaultString(field)

    @SerializedName("chapter")
    var chapter: String = ""
        get() = getDefaultString(field)

    @SerializedName("author")
    var author: String = ""
        get() = getDefaultString(field)

    @SerializedName("board")
    var board: String = ""
        get() = getDefaultString(field)

    @SerializedName("topic_learnpath_name")
    var topicLearnPathName: String = ""
        get() = getDefaultString(field)

    @SerializedName("mode")
    var mode: String = ""
        get() = getDefaultString(field)

    @SerializedName("tags")
    var tags: List<String>? = arrayListOf()


    constructor(parcel: Parcel) : this() {
        concepts = parcel.createStringArrayList()
        conceptId = parcel.readString() ?: ""
        lpcode = parcel.readString() ?: ""
        lmCode = parcel.readString() ?: ""
        exam = parcel.readString() ?: ""
        goal = parcel.readString() ?: ""
        unit = parcel.readString() ?: ""
        grade = parcel.readString() ?: ""
        topic = parcel.readString() ?: ""
        source = parcel.readString() ?: ""
        subject = parcel.readString() ?: ""
        subjectCode = parcel.readString() ?: ""
        examCode = parcel.readString() ?: ""
        chapter = parcel.readString() ?: ""
        author = parcel.readString() ?: ""
        board = parcel.readString() ?: ""
        format_id = parcel.readString() ?: ""
        lm_name = parcel.readString() ?: ""
        topicLearnPathName = parcel.readString() ?: ""
        mode = parcel.readString() ?: ""
        tags = parcel.createStringArrayList()

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeStringList(concepts)
        parcel.writeString(conceptId)
        parcel.writeString(lpcode)
        parcel.writeString(lmCode)
        parcel.writeString(exam)
        parcel.writeString(goal)
        parcel.writeString(unit)
        parcel.writeString(grade)
        parcel.writeString(topic)
        parcel.writeString(source)
        parcel.writeString(subject)
        parcel.writeString(subjectCode)
        parcel.writeString(examCode)
        parcel.writeString(chapter)
        parcel.writeString(author)
        parcel.writeString(board)
        parcel.writeString(format_id)
        parcel.writeString(lm_name)
        parcel.writeString(topicLearnPathName)
        parcel.writeString(mode)
        parcel.writeStringList(tags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LearningMap> {
        override fun createFromParcel(parcel: Parcel): LearningMap {
            return LearningMap(parcel)
        }

        override fun newArray(size: Int): Array<LearningMap?> {
            return arrayOfNulls(size)
        }
    }


    private fun getDefaultString(field: String): String {
        return field
    }

    private fun getDefaultInt(field: Int): Int {
        return field
    }

    private fun getDefaultDouble(field: Double): Double {
        return field
    }
}