package com.embibe.embibetvapp.newmodel


data class NewVideos(
    var results: ArrayList<Results>
) {
    object get {
        fun from(datum: ArrayList<Results>): NewVideos {
            val results = mutableListOf<Results>()
            datum.forEach {
                results += Results.get.from(it)
            }
            return NewVideos(results as ArrayList<Results>)
        }
    }
}