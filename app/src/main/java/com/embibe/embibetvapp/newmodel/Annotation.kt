package com.embibe.embibetvapp.newmodel

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Annotation() : Parcelable {

    @SerializedName("id")
    var id: String? = null

    @SerializedName("bundle_id")
    var bundleId: String? = null

    @SerializedName("element_type")
    var elementType: String? = null

    @SerializedName("element_id")
    var elementId: String? = null
        get() = field ?: ""

    @SerializedName("annotation_point")
    var annotationPoint: String? = null
        get() = field ?: ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        bundleId = parcel.readString()
        elementType = parcel.readString()
        elementId = parcel.readString()
        annotationPoint = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(bundleId)
        parcel.writeString(elementType)
        parcel.writeString(elementId)
        parcel.writeString(annotationPoint ?: "")
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Annotation> {
        override fun createFromParcel(parcel: Parcel): Annotation {
            return Annotation(parcel)
        }

        override fun newArray(size: Int): Array<Annotation?> {
            return arrayOfNulls(size)
        }
    }


}