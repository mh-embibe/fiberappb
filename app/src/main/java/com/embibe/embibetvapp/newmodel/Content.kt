package com.embibe.embibetvapp.newmodel

import android.os.Parcel
import android.os.Parcelable
import com.embibe.embibetvapp.converter.*
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.model.learning.OwnerInfo
import com.google.gson.annotations.SerializedName
import io.objectbox.annotation.Convert
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
class Content() : Parcelable {
    @Id
    var objId: Long = 0
    var type: String = ""
        get() = getDefaultString(field)

    @Convert(
        converter = LearnmapConverter::class,
        dbType = String::class
    )
    var learning_map: LearningMap = LearningMap()

    var child_id: Long = 0
        get() = getDefaultLong(field)

    var sub_type: String = ""
        get() = getDefaultString(field)

    var id: String = ""
        get() = getDefaultString(field)

    var category: String = ""
        get() = getDefaultString(field)

    @SerializedName("content_id")
    var content_id: String = ""
        get() = getDefaultString(field)

    @SerializedName("category_url")
    var category_thumb: String = ""
        get() = getDefaultString(field)

    var description: String = ""
        get() = getDefaultString(field)

    var views: Int = 0
        get() = getDefaultInt(field)

    var url: String = ""
        get() = getDefaultString(field)

    var title: String = ""
        get() = getDefaultString(field)

    var currency: Int = 0
        get() = getDefaultInt(field)

    var length: Int = 0
        get() = getDefaultInt(field)

    var bg_thumb: String = ""
        get() = getDefaultString(field)

    var likes: Int = 0
        get() = getDefaultInt(field)

    var watched_duration: Int = 0
    var thumb: String = ""
        get() = getDefaultString(field)

    @Convert(
        converter = StringListConverter::class,
        dbType = String::class
    )
    var authors: ArrayList<String> = arrayListOf()

    var section_name: String = ""
        get() = getDefaultString(field)

    var teaser_url: String = ""
        get() = getDefaultString(field)

    var concept_count: Int = 0
        get() = getDefaultInt(field)

    var rating: String = ""
        get() = getDefaultString(field)

    @Convert(
        converter = OwnerInfoConverter::class,
        dbType = String::class
    )
    var owner_info: OwnerInfo = OwnerInfo()

    var learnmap_id: String = ""
        get() = getDefaultString(field)

    var learning_map_code: String = ""
        get() = getDefaultString(field)

    var subject: String = ""
        get() = getDefaultString(field)


    @SerializedName("data")
    @Convert(
        converter = BannerDataConverter::class,
        dbType = String::class
    )
    var banner_data: ArrayList<BannerData> = ArrayList()


    var isBookmarked: Boolean = false

    var isLiked: Boolean = false

    @Convert(
        converter = AnnotationConverter::class,
        dbType = String::class
    )
    var annotation_attributes: ArrayList<Annotation> = arrayListOf()

    @SerializedName("is_password_protected")
    var isPasswordProtectedFlag: Boolean = false

    @SerializedName("passwordProtected")
    var passwordProtected: Boolean = false

    var sequence: Double = 0.0
        get() = getDefaultDouble(field)

    var bundle_id: String = ""
        get() = getDefaultString(field)

    var xpath: String = ""
        get() = getDefaultString(field)

    var total_marks: Int = 0
        get() = getDefaultInt(field)

    var duration: Int = 0
        get() = getDefaultInt(field)

    var questions: String = ""
        get() = getDefaultString(field)

    var preview_url: String = ""
        get() = getDefaultString(field)


    var chapterSize: String = ""
        get() = getDefaultString(field)
    var practice_tile_image: String = ""
        get() = getDefaultString(field)
    var question_book_tag: String = ""
        get() = getDefaultString(field)

    var test_quality_score: String = ""
        get() = getDefaultString(field)

    var startedAtInMilliSeconds: String = ""
        get() = getDefaultString(field)
    var testDurationInSeconds: String = ""
        get() = getDefaultString(field)

    var learnpath_name: String = ""
        get() = getDefaultString(field)

    var learnpath_format_name: String = ""
        get() = getDefaultString(field)

    @SerializedName("total_steps")
    var total_steps: String = ""
        get() = getDefaultString(field)

    @SerializedName("covered_steps")
    var covered_steps: Int = 0
        get() = getDefaultInt(field)

    @SerializedName("overall_progress")
    var overall_progress: Int = 0
        get() = getDefaultInt(field)

    @SerializedName("behaviours")
    var behaviours: Int = 0
        get() = getDefaultInt(field)

    @SerializedName("effort_rating")
    var effort_rating: Int = 0
        get() = getDefaultInt(field)

    @SerializedName("actual_grade")
    var actual_grade: String = ""
        get() = getDefaultString(field)

    @SerializedName("potential_grade")
    var potential_grade: String = ""
        get() = getDefaultString(field)

    @SerializedName("previous_test_xpath")
    var previous_test_xpath: String = ""
        get() = getDefaultString(field)

    @SerializedName("previous_test_bunlde_id")
    var previous_test_bunlde_id: String = ""
        get() = getDefaultString(field)

    @SerializedName("completion_status")
    var completion_status: String = ""
        get() = getDefaultString(field)

    @SerializedName("status")
    var status: String = ""
        get() = getDefaultString(field)

    @SerializedName("videos")
    var videos: Int = 0
        get() = getDefaultInt(field)

    @SerializedName("accuracy")
    var accuracy: String = ""
        get() = getDefaultString(field)

    @SerializedName("attempted")
    var attempted: String = ""
        get() = getDefaultString(field)

    @SerializedName("correct_attempts")
    var correct_attempts: String = ""
        get() = getDefaultString(field)

    @SerializedName("new_topic_code")
    var new_topic_code: String = ""
        get() = getDefaultString(field)

    @SerializedName("topic_code")
    var topic_code: String = ""
        get() = getDefaultString(field)

    @SerializedName("topic_name")
    var topic_name: String = ""
        get() = getDefaultString(field)

    @SerializedName("total_questions")
    var total_questions: String = ""
        get() = getDefaultString(field)

    @SerializedName("topic_mastery")
    var topic_mastery: String = ""
        get() = getDefaultString(field)



    @SerializedName("test_status")
    var test_status: String = ""

    @SerializedName("format_id")
    var format_id: String = ""

    private fun getDefaultString(field: String): String {
        return field
    }

    private fun getDefaultInt(field: Int): Int {
        return field
    }

    private fun getDefaultDouble(field: Double): Double {
        return field
    }

    private fun getDefaultLong(field: Long): Long {
        return field
    }

    private constructor(parcel: Parcel) : this() {
        objId = parcel.readLong()
        type = parcel.readString() ?: getDefaultString(type)
        learning_map = parcel.readParcelable(LearningMap::class.java.classLoader)!!
        child_id = parcel.readLong()
        sub_type = parcel.readString() ?: getDefaultString(sub_type)
        id = parcel.readString() ?: getDefaultString(id)
        category = parcel.readString() ?: getDefaultString(category)
        content_id = parcel.readString() ?: getDefaultString(content_id)
        category_thumb = parcel.readString() ?: getDefaultString(category_thumb)
        description = parcel.readString() ?: getDefaultString(description)
        views = parcel.readInt()
        url = parcel.readString() ?: getDefaultString(url)
        title = parcel.readString() ?: getDefaultString(title)
        currency = parcel.readInt()
        length = parcel.readInt()
        bg_thumb = parcel.readString() ?: getDefaultString(bg_thumb)
        likes = parcel.readInt()
        watched_duration = parcel.readInt()
        thumb = parcel.readString() ?: getDefaultString(thumb)
        authors = parcel.createStringArrayList() as ArrayList<String>
        section_name = parcel.readString() ?: getDefaultString(section_name)
        teaser_url = parcel.readString() ?: getDefaultString(teaser_url)
        concept_count = parcel.readInt()
        rating = parcel.readString() ?: getDefaultString(rating)
        owner_info = parcel.readParcelable(OwnerInfo::class.java.classLoader)!!
        learnmap_id = parcel.readString() ?: getDefaultString(learnmap_id)
        learning_map_code = parcel.readString() ?: getDefaultString(learning_map_code)
        subject = parcel.readString() ?: ""
        isBookmarked = parcel.readInt() == 1
        isLiked = parcel.readInt() == 1
        annotation_attributes = parcel.createTypedArrayList(Annotation.CREATOR)!!
        isPasswordProtectedFlag = parcel.readInt() == 1
        passwordProtected = parcel.readInt() == 1
        sequence = parcel.readDouble()
        bundle_id = parcel.readString() ?: getDefaultString(bundle_id)
        xpath = parcel.readString() ?: getDefaultString(xpath)
        total_marks = parcel.readInt()
        duration = parcel.readInt()
        questions = parcel.readString() ?: getDefaultString(questions)
        preview_url = parcel.readString() ?: getDefaultString(preview_url)
        chapterSize = parcel.readString() ?: getDefaultString(preview_url)
        practice_tile_image = parcel.readString() ?: ""
        question_book_tag = parcel.readString() ?: getDefaultString(question_book_tag)
        test_quality_score = parcel.readString() ?: getDefaultString(test_quality_score)
        startedAtInMilliSeconds = parcel.readString() ?: getDefaultString(startedAtInMilliSeconds)
        testDurationInSeconds = parcel.readString() ?: getDefaultString(testDurationInSeconds)
        learnpath_name = parcel.readString() ?: getDefaultString(learnpath_name)
        learnpath_format_name = parcel.readString() ?: getDefaultString(learnpath_format_name)
        total_steps = parcel.readString() ?: getDefaultString(total_steps)
        covered_steps = parcel.readInt()
        overall_progress = parcel.readInt()
        behaviours = parcel.readInt()
        videos = parcel.readInt()
        effort_rating = parcel.readInt()
        actual_grade = parcel.readString() ?: getDefaultString(actual_grade)
        potential_grade = parcel.readString() ?: getDefaultString(potential_grade)
        previous_test_xpath = parcel.readString() ?: getDefaultString(previous_test_xpath)
        previous_test_bunlde_id = parcel.readString() ?: getDefaultString(previous_test_bunlde_id)
        completion_status = parcel.readString() ?: getDefaultString(completion_status)
        status = parcel.readString() ?: getDefaultString(status)
        attempted = parcel.readString() ?: getDefaultString(attempted)
        correct_attempts = parcel.readString() ?: getDefaultString(correct_attempts)
        new_topic_code = parcel.readString() ?: getDefaultString(new_topic_code)
        topic_code = parcel.readString() ?: getDefaultString(topic_code)
        topic_name = parcel.readString() ?: getDefaultString(topic_name)
        total_questions = parcel.readString() ?: getDefaultString(total_questions)
        topic_mastery = parcel.readString() ?: getDefaultString(topic_mastery)
        format_id = parcel.readString() ?: getDefaultString(format_id)
        test_status = parcel.readString() ?: getDefaultString(test_status)

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

        parcel.writeLong(objId)
        parcel.writeString(type)
        parcel.writeParcelable(learning_map, flags)
        parcel.writeLong(child_id)
        parcel.writeString(sub_type)
        parcel.writeString(id)
        parcel.writeString(category)
        parcel.writeString(content_id)
        parcel.writeString(category_thumb)
        parcel.writeString(description)
        parcel.writeInt(views)
        parcel.writeString(url)
        parcel.writeString(title)
        parcel.writeInt(currency)
        parcel.writeInt(length)
        parcel.writeString(bg_thumb)
        parcel.writeInt(likes)
        parcel.writeInt(watched_duration)
        parcel.writeString(thumb)
        parcel.writeStringList(authors)
        parcel.writeString(section_name)
        parcel.writeString(teaser_url)
        parcel.writeInt(concept_count)
        parcel.writeString(rating)
        parcel.writeParcelable(owner_info, flags)
        parcel.writeString(learnmap_id)
        parcel.writeString(learning_map_code)
        parcel.writeString(subject)
        parcel.writeInt(if (isBookmarked) 1 else 0)
        parcel.writeInt(if (isLiked) 1 else 0)
        parcel.writeTypedList(annotation_attributes)
        parcel.writeInt(if (isPasswordProtectedFlag) 1 else 0)
        parcel.writeInt(if (passwordProtected) 1 else 0)
        parcel.writeDouble(sequence)
        parcel.writeString(bundle_id)
        parcel.writeString(xpath)
        parcel.writeInt(total_marks)
        parcel.writeInt(duration)
        parcel.writeString(questions)
        parcel.writeString(preview_url)
        parcel.writeString(chapterSize)
        parcel.writeString(practice_tile_image)
        parcel.writeString(question_book_tag)
        parcel.writeString(test_quality_score)
        parcel.writeString(startedAtInMilliSeconds)
        parcel.writeString(testDurationInSeconds)
        parcel.writeString(learnpath_name)
        parcel.writeString(learnpath_format_name)
        parcel.writeString(total_steps)
        parcel.writeInt(covered_steps)
        parcel.writeInt(overall_progress)
        parcel.writeInt(behaviours)
        parcel.writeInt(effort_rating)
        parcel.writeInt(videos)
        parcel.writeString(actual_grade)
        parcel.writeString(potential_grade)
        parcel.writeString(previous_test_xpath)
        parcel.writeString(previous_test_bunlde_id)
        parcel.writeString(completion_status)
        parcel.writeString(status)
        parcel.writeString(attempted)
        parcel.writeString(correct_attempts)
        parcel.writeString(new_topic_code)
        parcel.writeString(topic_code)
        parcel.writeString(topic_name)
        parcel.writeString(total_questions)
        parcel.writeString(topic_mastery)
        parcel.writeString(format_id)
        parcel.writeString(test_status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Content> {
        override fun createFromParcel(parcel: Parcel): Content {
            return Content(parcel)
        }

        override fun newArray(size: Int): Array<Content?> {
            return arrayOfNulls(size)
        }
    }


}