package com.embibe.embibetvapp.newmodel

import android.os.Parcel
import android.os.Parcelable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.google.gson.annotations.SerializedName

data class Results(

    val content: List<
            Content>,
    var section_name: String,
    var type: String,
    @SerializedName("section_id")
    var sectionId: String? = null,

    @SerializedName("available")
    var available: String? = null,

    @SerializedName("start")
    var start: String? = null,

    @SerializedName("end_count")
    var endCount: String? = null
) : Parcelable {


    constructor(parcel: Parcel) : this(
        parcel.createTypedArrayList(Content)!!,
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    object get {
        fun from(datum: Results) = Results(
            datum.content,
            datum.section_name,
            ""
        )
    }

    /*companion object {

    }*/

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(content)
        parcel.writeString(section_name)
        parcel.writeString(type)
        parcel.writeString(sectionId)
        parcel.writeString(available)
        parcel.writeString(start)
        parcel.writeString(endCount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Results> {
        @BindingAdapter("image")
        @JvmStatic
        fun loadImage(view: ImageView, imageUrl: String?) {

            val requestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            Glide.with(view.context).setDefaultRequestOptions(requestOptions)
                .load(imageUrl)
                .placeholder(R.drawable.video_placeholder)
                .transition(DrawableTransitionOptions.withCrossFade())
                .transform(RoundedCorners(14))
                .into(view)
        }

        override fun createFromParcel(parcel: Parcel): Results {
            return Results(parcel)
        }

        override fun newArray(size: Int): Array<Results?> {
            return arrayOfNulls(size)
        }
    }
}