package com.embibe.embibetvapp.utils

import android.graphics.Typeface
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import com.embibe.embibetvapp.application.App.Companion.context


class FontHelper {
    enum class FontType(private val type: String) {
        GILROY_BOLD("fonts/gilroy_bold.ttf"),
        GILROY_SEMI_BOLD("fonts/gilroy_semibold.ttf"),
        GILROY_MEDIUM("fonts/gilroy_medium.ttf"),
        GILROY_REGULAR("fonts/gilroy_regular.ttf"),
        GILROY_LIGHT("fonts/gilroy_light.ttf");

        companion object {
            fun whichFont(fontType: FontType?): String? {
                if (fontType != null) {
                    for (typeEnum in values()) {
                        if (fontType == typeEnum) return typeEnum.type
                    }
                }
                return null
            }
        }

    }


    fun setFontFace(fontType: FontType?, vararg views: View) {
        val type = Typeface.createFromAsset(
            context.assets,
            FontType.whichFont(fontType)
        )
        for (view in views) {
            if (view is TextView) {
                view.typeface = type
            }
            if (view is Button) {
                view.typeface = type
            }
            if (view is EditText) {
                view.typeface = type
            }
            if (view is RadioButton) {
                view.typeface = type
            }
            if (view is SwitchCompat) {
                view.typeface = type
            }
        }
    }
}