package com.embibe.embibetvapp.utils.data

import android.util.Log
import com.embibe.embibetvapp.BuildConfig
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App.Companion.context
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.model.BannerDataList
import com.embibe.embibetvapp.model.LastWatchedContent
import com.embibe.embibetvapp.model.achieve.achieveFeedback.*
import com.embibe.embibetvapp.model.achieve.futureSuccess.FutureSuccessRes
import com.embibe.embibetvapp.model.achieve.readiness.Readiness
import com.embibe.embibetvapp.model.adduser.Exam
import com.embibe.embibetvapp.model.adduser.Goal
import com.embibe.embibetvapp.model.chapter.chapterLearningObjects.ChapterLearningObject
import com.embibe.embibetvapp.model.createtest.configuration.TestConfigurationRes
import com.embibe.embibetvapp.model.jiostb.JioUser
import com.embibe.embibetvapp.model.likeAndBookmark.BookmarkedQuestion
import com.embibe.embibetvapp.model.potential.Potential
import com.embibe.embibetvapp.model.test.TestQuestionResponse
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.utils.Utils
import com.embibejio.coreapp.model.LinkedProfile
import com.embibejio.coreapp.preference.PreferenceHelper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class DataManager private constructor() {

    private object HOLDER {
        val INSTANCE = DataManager()
    }

    companion object {
        val instance: DataManager by lazy(HOLDER::INSTANCE)
        var isLoading: Boolean = false
    }

    init {
        Utils.isLikeBookmarkUpdatedForQuickLinks = true
        Utils.isContentStatusUpdatedForQuickLinks = true
    }

    private lateinit var achieveAttemptsList: String
    private lateinit var achieveSkills: String
    var isDiagnosticTest: Boolean = false
    lateinit var testCode: String
    lateinit var content: Content
    private lateinit var testQuestions: String
    private lateinit var currentQuestion: String
    private lateinit var testAttempts: String
    private lateinit var testSkills: String
    private lateinit var testAchieve: String
    private lateinit var archieveStep: Step
    private lateinit var testPAJRes: AchieveFeedbackRes
    private lateinit var archieveStepArray: List<Step>
    private var testPAJ: String? = null
    private lateinit var testPAJActivity: Activity
    private lateinit var testPAJStep: Step
    private var feedBackSteps: Int = 0
    private lateinit var testQuestionSummary: String


    private lateinit var feedbackAchieveRes: AchieveFeedbackRes
    private var feedbackStepPos: Int = 0
    private var FAPos: Int = 0
    private var FASPos: Int = 0
    private var feedbackLV1StepPos: Int = 0
    private var feedbackLV2StepPos: Int = 0
    private lateinit var createOwnTestSection: ResultsEntity
    private lateinit var cooboResult: ResultsEntity
    private var futureSuccessData: List<FutureSuccessRes>? = null
    private var goalsExams: List<Goal> = arrayListOf()
    private lateinit var jioSTBUser: JioUser
    private var herobannerData: BannerDataList = BannerDataList()
    private var herobannerDataPratice: BannerDataList = BannerDataList()
    private var herobannerDataTest: BannerDataList = BannerDataList()
    private var homeResults = ArrayList<ResultsEntity>()
    private var homeResultsPractice = ArrayList<ResultsEntity>()
    private var homeResultsTest = ArrayList<ResultsEntity>()
    private var readinessData: Readiness? = null
    var isHomeDataRemoved: Boolean = false
    var isHomePracticeDataRemoved: Boolean = false
    var listRelatedContents = ArrayList<Results>()
    var listRecommendationLearningForPractice = ArrayList<Results>()
    var listBookAvailable = ArrayList<Content>()
    var listBookmarkedQuestions = ArrayList<BookmarkedQuestion>()

    var testConfigurationsList: TestConfigurationRes? = null
    var listMoreContents = ArrayList<Results>()
    var topicsForChapterContents = ArrayList<Content>()
    var videosForChapterContents = ArrayList<ChapterLearningObject>()
    var testsForChapterContents = ArrayList<Content>()
    var practicesForChapterContents = ArrayList<Content>()
    var prerequisitesForChapterContents = ArrayList<Content>()
    var relatedTopicsContents = ArrayList<Content>()
    var videosForTopicContents = ArrayList<ResultsEntity>()
    var testsForTopicContents = ArrayList<Content>()
    var practicesForTopicContents = ArrayList<Content>()
    var prerequisitesForTopicContents = ArrayList<Content>()
    var lastWatchedContentData: LastWatchedContent? = null
    private var recommendedLearningForTest = ArrayList<Results>()
    private var recommendedPracticeForTest = ArrayList<Content>()
    private var moreTests = ArrayList<Content>()
    var listPracticeTopics = ArrayList<Content>()
    lateinit var annotation: ArrayList<com.embibe.embibetvapp.newmodel.Annotation>
    private var videosSeeksHashMap = HashMap<String, Long>()
    var nextVideoIds = arrayListOf<String>()
    var mTestQuestionResponse: TestQuestionResponse? = null

    var mRankComparisons: String = ""
    var mBehaviours: String = ""
    var mTopicSummary: String = ""
    var paj_id: String = ""

    fun setLastSeek(videoId: String, lastSeekPos: Long) {
        videosSeeksHashMap[videoId] = lastSeekPos
    }

    fun getLastSeek(videoId: String): Long? {
        return videosSeeksHashMap[videoId]
    }

    fun setRelatedConceptDetails(id: String, relatedConceptData: ArrayList<Results>) {
        listRelatedContents = relatedConceptData
        if (relatedConceptData.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.RELATED_CONCEPTS + "_" + id, time)
        }
    }

    fun getRelatedConcepts(): ArrayList<Results> {
        return listRelatedContents
    }

    fun setMoreTopics(id: String, moreTopicsData: ArrayList<Results>) {
        listMoreContents = moreTopicsData
        if (moreTopicsData.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.MORE_TOPICS + "_" + id, time)
        }
    }

    fun setTopicsForChapter(id: String, topics: ArrayList<Content>) {
        topicsForChapterContents = topics
        if (topics.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.TOPICS_IN_THIS_CHAPTER + "_" + id, time)
        }
    }

    fun setVideosForChapter(id: String, videos: ArrayList<ChapterLearningObject>) {
        videosForChapterContents = videos
        if (videos.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.ALL_VIDEOS_FOR_THIS_CHAPTER + "_" + id, time)
        }
    }

    fun setTestsForChapter(id: String, tests: ArrayList<Content>) {
        testsForChapterContents = tests
        if (tests.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.TEST_ON_THIS_CHAPTER + "_" + id, time)
        }
    }

    fun setPracticesForChapter(id: String, practices: ArrayList<Content>) {
        practicesForChapterContents = practices
        if (practices.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.PRACTICE_ON_THIS_CHAPTER + "_" + id, time)
        }
    }

    fun setPrerequisitesForChapter(id: String, prerequisites: ArrayList<Content>) {
        prerequisitesForChapterContents = prerequisites
        if (prerequisites.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.PRE_REQUISITE_TOPICS + "_" + id, time)
        }
    }

    fun setRelatedTopics(id: String, topics: ArrayList<Content>) {
        relatedTopicsContents = topics
        if (topics.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.RELATED_TOPICS + "_" + id, time)
        }
    }

    fun setVideosForTopic(id: String, videos: ArrayList<ResultsEntity>) {
        videosForTopicContents = videos
        if (videos.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.ALL_VIDEOS_FOR_THIS_TOPIC + "_" + id, time)
        }
    }

    fun setTestsForTopic(id: String, tests: ArrayList<Content>) {
        testsForTopicContents = tests
        if (tests.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.TEST_ON_THIS_TOPIC + "_" + id, time)
        }
    }

    fun setPracticesForTopic(id: String, practices: ArrayList<Content>) {
        practicesForTopicContents = practices
        if (practices.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.PRACTICE_ON_THIS_TOPIC + "_" + id, time)
        }
    }

    fun setPrerequisitesForTopic(id: String, prerequisites: ArrayList<Content>) {
        prerequisitesForTopicContents = prerequisites
        if (prerequisites.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.PRE_REQUISITE_TOPICS + "_" + id, time)
        }
    }

    fun setLastWatchedContent(id: String, lastWatchedData: LastWatchedContent?) {
        lastWatchedContentData = lastWatchedData
        if (lastWatchedData?.content?.isNotEmpty()!!) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.LAST_WATCHED_CONTENT + "_" + id, time)
        }
    }

    fun setAchieveAttemptsList(list: String) {
        achieveAttemptsList = list
    }

    fun setAchieveSkills(list: String) {
        achieveSkills = list
    }

    fun setRankComparisons(models: String) {
        mRankComparisons = models
    }

    fun setTopicSummary(models: String) {
        mTopicSummary = models
    }

    fun setBehaviours(model: String) {
        mBehaviours = model
    }

    fun getMoreTopics(): ArrayList<Results> {
        return listMoreContents
    }

    fun setPracticeTopics(id: String, topics: ArrayList<Content>) {
        listPracticeTopics = topics
        if (topics.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.PRACTICE_TOPICS + "_" + id, time)
        }

    }

    fun getPracticeTopics(): ArrayList<Content> {
        return listPracticeTopics
    }

    fun setRecommendationLearningForPractice(id: String, relatedConceptData: List<Results>) {
        listRecommendationLearningForPractice = relatedConceptData as ArrayList<Results>
        if (relatedConceptData.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(
                AppConstants.RECOMMENDATION_LEARNING_FOR_PRACTICE + "_" + id,
                time
            )
        }
    }

    fun getRecommendationLearningForPractice(): ArrayList<Results> {
        return listRecommendationLearningForPractice
    }

    fun setBookAvailableForPractise(id: String, relatedConceptData: List<Content>) {
        listBookAvailable = relatedConceptData as ArrayList<Content>
        if (relatedConceptData.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.BOOK_AVAILABLE_FOR_PRACTICE + "_" + id, time)
        }
    }

    fun setBookmarkedQuestionsForPractise(
        id: String,
        bookmarkedQuestions: List<BookmarkedQuestion>
    ) {
        listBookmarkedQuestions = bookmarkedQuestions as ArrayList<BookmarkedQuestion>
        if (bookmarkedQuestions.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.BOOKMARKED_QUESTIONS_FOR_PRACTICE + "_" + id, time)
        }
    }

    fun getBookAvailableForPractise(): ArrayList<Content> {
        return listBookAvailable
    }

    fun getPracticesForChapter(): ArrayList<Content> {
        return practicesForChapterContents
    }

    fun getTestsForChapter(): ArrayList<Content> {
        return testsForChapterContents
    }

    fun getVideosForChapter(): ArrayList<ChapterLearningObject> {
        return videosForChapterContents
    }

    fun getPrerequisitesForChapter(): ArrayList<Content> {
        return prerequisitesForChapterContents
    }

    fun getTopicsForChapter(): ArrayList<Content> {
        return topicsForChapterContents
    }

    fun getPracticesForTopic(): ArrayList<Content> {
        return practicesForTopicContents
    }

    fun getTestsForTopic(): ArrayList<Content> {
        return testsForTopicContents
    }

    fun getVideosForTopic(): ArrayList<ResultsEntity> {
        return videosForTopicContents
    }

    fun getPrerequisitesForTopic(): ArrayList<Content> {
        return prerequisitesForTopicContents
    }

    fun getRelatedTopics(): ArrayList<Content> {
        return relatedTopicsContents
    }

    fun getLastWatchedContent(): LastWatchedContent? {
        return lastWatchedContentData
    }

    fun setTestConfigurations(list: TestConfigurationRes?) {
        testConfigurationsList = list!!
    }

    fun getRecommendedLearningForTest(): ArrayList<Results> {
        return recommendedLearningForTest
    }

    fun setRecommendedLearningForTest(
        id: String,
        recommendedLearning: ArrayList<Results>
    ) {
        recommendedLearningForTest = recommendedLearning
        if (recommendedLearning.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.RECOMMENDED_LEARNING + "_" + id, time)
        }
    }

    fun getRecommendedPracticeForTest(): ArrayList<Content> {
        return recommendedPracticeForTest
    }

    fun setRecommendedPracticeForTest(id: String, recommendedPractice: ArrayList<Content>) {
        recommendedPracticeForTest = recommendedPractice
        if (recommendedPractice.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.RECOMMENDED_PRACTICE + "_" + id, time)
        }
    }

    fun getMoreTests(): ArrayList<Content> {
        return moreTests
    }

    fun setMoreTests(id: String, tests: ArrayList<Content>) {
        moreTests = tests
        if (tests.isNotEmpty()) {
            val time = System.currentTimeMillis()
            PreferenceHelper().put(AppConstants.MORE_TESTS + "_" + id, time)
        }
    }

//    fun setTestConfigurations(list: ArrayList<TestConfigModel>) {
//        testConfigurationsList = list
//    }

    fun getTestConfigurations(): TestConfigurationRes {
        return testConfigurationsList!!
    }


    fun getHome(): ArrayList<ResultsEntity> {
        return homeResults
    }

    fun setHeroBannerDataPractice(contents: List<Content>) {
        herobannerDataPratice = BannerDataList()
        herobannerDataPratice.data = contents[0].banner_data
    }

    fun getHeroBannerData(): BannerDataList {
        return herobannerData
    }

    fun getHeroBannerDataPractice(): BannerDataList {
        return herobannerDataPratice
    }

    fun setHeroBannerData(contents: List<Content>) {
        herobannerData = BannerDataList()
        herobannerData.data = contents[0].banner_data
    }

    fun setHeroBannerDataTest(contents: List<Content>) {
        herobannerDataTest = BannerDataList()
        herobannerDataTest.data = contents[0].banner_data
    }

    fun getHeroBannerDataTest(): BannerDataList {
        return herobannerDataTest
    }

    fun setHomePractice(
        homeData: ArrayList<ResultsEntity>,
        callback: BaseViewModel.DataCallback<ArrayList<ResultsEntity>>?
    ) {
        if (!homeData.isNullOrEmpty()) {
            homeResultsPractice = homeData
            isHomePracticeDataRemoved = false
            for (result in homeData) {
                if (result.section_name == "" && result.sectionId.equals("100") || result.sectionId == 100L) {
                    if (!result.content.isNullOrEmpty()) {
                        setHeroBannerDataPractice(result.content!!)
                    }
                    homeResultsPractice.remove(result)
                    break
                }
            }
            callback?.onSuccess(homeResultsPractice)
        } else {
            isHomePracticeDataRemoved = true
        }
    }

    fun getHomePractice(): ArrayList<ResultsEntity> {
        return homeResultsPractice
    }

    fun getHomeTest(): ArrayList<ResultsEntity> {
        return homeResultsTest
    }

    fun setHomeTest(
        homeData: ArrayList<ResultsEntity>,
        callback: BaseViewModel.DataCallback<ArrayList<ResultsEntity>>?
    ) {
        homeResultsTest = homeData
        if (!homeResultsTest.isNullOrEmpty()) {
            for (result in homeData) {
                if (result.section_name == "" && result.sectionId.equals("100") || result.sectionId == 100L) {
                    if (!result.content.isNullOrEmpty()) {
                        setHeroBannerDataTest(result.content!!)
                    }
                    homeResultsTest.remove(result)
                    break
                }
            }
            callback?.onSuccess(homeResultsTest)
        }
    }


    fun setHome(
        homeData: ArrayList<ResultsEntity>,
        callback: BaseViewModel.DataCallback<ArrayList<ResultsEntity>>?
    ) {
        Log.w("app", "set Home size = ${homeResults.size}")
        if (!homeData.isNullOrEmpty()) {
            isHomeDataRemoved = false
            homeResults = homeData
            for (result in homeData) {
                if (result.section_name.equals("") && result.sectionId == 100L) {
                    if (!result.content.isNullOrEmpty()) {
                        setHeroBannerData(result.content!!)
                    }
                    homeResults.remove(result)
                    break
                }
            }
            callback?.onSuccess(homeResults)
        } else {
            isHomeDataRemoved = true
        }

    }

    fun insertCoobo(
        homeData: ArrayList<ResultsEntity>,
        callback: BaseViewModel.DataCallback<ArrayList<ResultsEntity>>?
    ) {
        if (BuildConfig.IS_EXTERNALDATA_NEEDED) {
            var positionInteractiveCoobos: Int = 0
            doAsync {
                for (index in homeData.indices) {
                    if (homeData[index].section_name == "Interactive Coobos") {
                        positionInteractiveCoobos = index + 1
                        break
                    }
                }
                if (positionInteractiveCoobos != 0) {
                    val coobo_json =
                        Utils.inputStreamToString(context.resources.openRawResource(R.raw.coobo))
                    cooboResult = Gson().fromJson(coobo_json, ResultsEntity::class.java)
                    cooboResult.section_name = "Not working Coobos"
                    cooboResult.type = AppConstants.COOBO
                    homeData.add(positionInteractiveCoobos, cooboResult)
                }
                callback?.onSuccess(homeData)
            }

        } else {
            callback?.onSuccess(homeData)
        }
    }

    fun insertCreateOwnTestSection(
        testList: ArrayList<ResultsEntity>,
        callback: BaseViewModel.DataCallback<ArrayList<ResultsEntity>>?
    ) {
        var createTestSectionIndex: Int = 2
        doAsync {
            for (index in testList.indices) {
                if (testList[index].sectionId == 4L) {
                    createTestSectionIndex = index + 1
                    break
                }
            }
            if (createTestSectionIndex != 0) {
                val json =
                    Utils.inputStreamToString(context.resources.openRawResource(R.raw.create_your_own_test_section))
                createOwnTestSection = Gson().fromJson(json, ResultsEntity::class.java)
                createOwnTestSection.type = AppConstants.CREATE_TEST
                testList.add(createTestSectionIndex, createOwnTestSection)
            }
            uiThread {
                callback?.onSuccess(testList)
            }

        }
    }

    /* fun setCurrentProfile(profile: LinkedProfile) {
     PreferenceHelper().put(
         AppConstants.CURRENT_PROFILE,
         Gson().toJson(profile).toString()
     )
     this.profile = profile
 }
*/
    fun getCurrentProfile(): LinkedProfile? {
        return PreferenceHelper().getCurrentProfile(AppConstants.CURRENT_PROFILE)
    }

    fun getUserId(): String {
        return PreferenceHelper()[AppConstants.USER_ID, ""]
    }

    fun getChildId(): String {
        var profile = getCurrentProfile()
        return if (profile != null) {
            return profile.userId
        } else ""
    }

    fun setJioSTBUser(model: JioUser) {
        jioSTBUser = model
        PreferenceHelper().put(
            AppConstants.SUBSCRIBER_ID,
            jioSTBUser.sessionAttributes?.user?.subscriberId ?: ""
        )
    }

    fun setReadiness(model: Readiness) {
        readinessData = model
    }

    fun getReadinessData() = readinessData


    fun setFutureSuccessData(model: List<FutureSuccessRes>?) {
        futureSuccessData = model
    }

    fun getFutureSuccessData() = futureSuccessData


    fun getExamCodeFromContent(content: Content?): String {
        try {
            if (content == null) {
                return "ex4"
            }
            val parts = content.learning_map.lpcode.split("--")
            val lmCode = parts[parts.size - 1]
            val lmLevel = Utils.getLevelUsingCode(lmCode)
            val examCode = Utils.getExamLevelCode(parts)
            return examCode
        } catch (e: Exception) {
            e.printStackTrace()
            return "ex4"
        }
    }


    fun saveChildEmbibeToken(embibeToken: String?) {
        if (embibeToken != null) {
            PreferenceHelper().put(AppConstants.EMBIBETOKEN, embibeToken)
        }
    }


    fun setGoalsExamsList(goalsExams: List<Goal>) {
        this.goalsExams = goalsExams
        PreferenceHelper().put(
            AppConstants.GOAL_AND_EXAMS,
            Gson().toJson(goalsExams).toString()
        )
    }

    fun getGoalsExamsList(): List<Goal> {
        return try {
            val json = PreferenceHelper()[AppConstants.GOAL_AND_EXAMS, ""]
            val model: List<Goal> =
                Gson().fromJson(json, object : TypeToken<List<Goal>>() {}.type)
            if (model.isNotEmpty()) {
                model.sortedByDescending { it.default }
            } else arrayListOf()
        } catch (e: Exception) {
            arrayListOf()
        }
    }


    fun getPrimaryGoals(): List<Goal> {
        return getGoalsExamsList()
    }

    fun getPrimaryExamGoals(goalCode: String): ArrayList<Exam> {
        val goalsList = getGoalsExamsList()
        val exams = goalsList.find { it.code == goalCode }
        return exams?.exams ?: arrayListOf()
    }

    fun getPrimaryGoalsExcept(goalCode: String): List<Goal> {
        val goalsList = getGoalsExamsList()
        val copy = goalsList.toCollection(mutableListOf())
        val item = copy.find { it.code == goalCode }
        copy.remove(item)
        return copy
    }

    fun getGoalNameByCode(goalCode: String): String {
        val goalsList = getGoalsExamsList()
        val goal = goalsList.find { it.code == goalCode }
        return goal?.name ?: ""
    }

    fun getGoalSlugNameByCode(goalCode: String): String {
        val goalsList = getGoalsExamsList()
        val goal = goalsList.find { it.code == goalCode }
        return goal?.slug ?: ""
    }

    fun getGoalByGoalCode(goalCode: String): Goal? {
        val goalsList = getGoalsExamsList()
        val goal = goalsList.find { it.code == goalCode }
        return goal
    }

    fun getExamByGoalCodeExamCode(goalCode: String?, examCode: String?): Exam? {
        val goalsList = getGoalsExamsList()
        val goal = goalsList.find { it.code == goalCode }
        val exams = goal?.exams ?: arrayListOf()
        return exams.find { it.code == examCode }
    }

    fun getExamNameByCode(goalCode: String, examCode: String?): String {
        val goalsList = getGoalsExamsList()
        val goal = goalsList.find { it.code == goalCode }
        val exams = goal?.exams ?: arrayListOf()
        val exam = exams.find { it.code == examCode }
        return exam?.name ?: ""
    }

    fun getExamSlugNameByCode(goalCode: String, examCode: String): String {
        val goalsList = getGoalsExamsList()
        val goal = goalsList.find { it.code == goalCode }
        val exams = goal?.exams ?: arrayListOf()
        val exam = exams.find { it.code == examCode }
        return exam?.slug ?: ""
    }

    fun getTestSkills() = testSkills
    fun getTestAttempts() = testAttempts
    fun getTestQuestion() = testQuestions
    fun getCurrentQuestion() = currentQuestion
    fun getTestAchieve() = testAchieve
    fun getTestQuestionSummary() = testQuestionSummary

    fun getBehaviours() = mBehaviours
    fun getAchieveTopicSummaryList() = mTopicSummary
    fun getAchieveRankComparisons() = mRankComparisons
    fun getAchieveAttemptsList() = achieveAttemptsList
    fun getAchieveSkills() = achieveSkills
    fun getTestPAJ() = testPAJ

    fun setTestSkills(testSkill: String) {
        testSkills = testSkill
    }

    fun setCurrentQuestion(testQue: String) {
        currentQuestion = testQue
    }

    fun setTestAttempts(testAttempt: String) {
        testAttempts = testAttempt
    }

    fun setTestQuestion(testQn: String) {
        testQuestions = testQn
    }

    fun setTestAchieve(achieve: String) {
        testAchieve = achieve
    }

    fun setTestQuestionSummary(summary: String) {
        testQuestionSummary = summary
    }


    fun setTestPAJ(currentTestPAJ: String) {
        testPAJ = currentTestPAJ
    }


    //
    fun setKGPotentialQuestionList(questionPotential: List<Potential>) {
        //this.goalsExams = questionPotential
        PreferenceHelper().put(
            AppConstants.KG_QUESTION_POTENTIAL,
            Gson().toJson(questionPotential).toString()
        )
    }

    fun getKGPotentialQuestionList(): List<Potential> {
        return try {
            val json = PreferenceHelper()[AppConstants.KG_QUESTION_POTENTIAL, ""]
            val model: List<Potential> =
                Gson().fromJson(json, object : TypeToken<List<Potential>>() {}.type)
            if (model.isNotEmpty()) {
                model.sortedByDescending { it.equals(true) }
            } else arrayListOf()
        } catch (e: Exception) {
            arrayListOf()
        }
    }

    fun setFeedbackAchieveRes(feedbackRes: AchieveFeedbackRes) {
        feedbackAchieveRes = feedbackRes
    }

    fun getFeedbackAchieveRes(): AchieveFeedbackRes {
        return feedbackAchieveRes
    }

    fun setFeedbackStepPos(pos: Int) {
        feedbackStepPos = pos
    }

    fun setFeedbackLV1StepPos(pos: Int) {
        feedbackLV1StepPos = pos
    }

    fun setFeedbackLV2StepPos(pos: Int) {
        feedbackLV2StepPos = pos
    }

    fun getFeedbackStepPos(): Int {
        return feedbackStepPos
    }

    fun getFeedbackLV1StepPos(): Int {
        return feedbackLV1StepPos
    }

    fun getFeedbackLV2StepPos(): Int {
        return feedbackLV2StepPos
    }

    fun clearAllPos() {
        feedbackStepPos = 0
        feedbackLV1StepPos = 0
        feedbackLV2StepPos = 0
    }

    fun loadFeedbackPosition() {

        if (::feedbackAchieveRes.isInitialized) {
            for (stepData in feedbackAchieveRes.steps!!) {
                for (activity in stepData.activities!!) {
                    for (metaItem in activity.meta!!) {
                        if (metaItem.type == "learn") {

                        } else if (metaItem.type == "practice") {
                            for (data in metaItem.meta_data.objects) {

                            }
                        }
                    }
                }
            }
        }
    }

    /*fun getNextFeedbackData(): Data? {

        if (::feedbackAchieveRes.isInitialized) {
            var stepData = feedbackAchieveRes.steps!!.get(getFeedbackStepPos())
            var activity = stepData.activities!!.get(getFeedbackLV1StepPos())
            var metaItem = activity.meta!!.get(getFeedbackLV2StepPos())
            if (metaItem.type == "learn") {

            } else if (metaItem.type == "practice") {
                for (data in metaItem.meta_data.objects) {

                }
            }
        }
        return null
    }*/

    fun setFAPos(pos: Int) {
        FAPos = pos
    }

    fun setFASPos(pos: Int) {
        FASPos = pos
    }

    fun setActivity(activity: Activity) {
        testPAJActivity = activity
    }

    fun setSteposPos(pos: Int) {
        feedBackSteps = pos
    }

    fun setFeedbackStep(step: Step) {
        this.archieveStep = step
    }

    fun setFeedbackStepArray(steps: List<Step>) {
        this.archieveStepArray = steps
    }

    fun setPAJ_Id(paj_id: String) {
        this.paj_id = paj_id
    }

    fun setStep(step: Step) {
        this.testPAJStep = step
    }
    fun setPAJRes(PAJRes: AchieveFeedbackRes) {
        this.testPAJRes= PAJRes
    }

    fun movePosition(): Int {
        var activity = testPAJActivity
        if (activity.meta != null) {
            var metaData = activity.meta!![FAPos].meta_data
            var type = activity.meta!![FAPos].type
            if (type == "practice") {
                if (activity.meta!!.size > FAPos + 1)
                    FAPos = FAPos + 1
                else
                    return -1
            } else {
                if (metaData.objects != null) {
                    if (metaData.objects.size == FASPos + 1) {
                        if (activity.meta!!.size > FAPos + 1)
                            FAPos = FAPos + 1
                        else
                            return -1
                    }
                    if (metaData.objects.size > FASPos + 1)
                        FASPos = FASPos + 1
                }
            }
            return 0
        }
        return -1
    }

    fun loadActivityFromStep(): Data? {
        return null
       // var step = testPAJRes?.steps[feedbackStepPos]?.activities
    }

    fun getNextFeedbackData(): Data? {
        var activity = testPAJActivity
        if (activity.meta != null) {
            var metaData = activity.meta!![FAPos].meta_data
            var type = activity.meta!![FAPos].type
            if (type == "practice" || type=="question") {
                if (metaData.objects != null) {
                    var data = metaData.objects[0]
                    data.progress = archieveStep.progress!!
                    data.practiceMeta =
                        getPracticeData(metaData, activity.meta!![FAPos].index)
                    return data
                }
            } else {
                var item = metaData.objects[FASPos]
                item.progress = archieveStep.progress!!
                return item
            }
        }
        return null
    }


    fun findActivityPosition(activity: Activity) {
        FAPos = 0
        if (activity.meta != null) {
            var pos = 0
            for (metaData in activity.meta) {
                if (metaData.meta_data.progress_percent != 100) {
                    FAPos = pos
                    if (metaData.type == "practice") {
                        FASPos = 0
                    } else {
                        var sub_pos = 0
                        if (metaData.meta_data.objects != null) {
                            for (data in metaData.meta_data.objects) {
                                if (!data.completed) {
                                    FASPos = sub_pos
                                    return
                                }
                                sub_pos = sub_pos + 1
                            }
                        }

                    }
                    return
                }
                pos = pos + 1
            }

        }
    }

    private fun getPracticeData(data: MetaData, stepPathIndex: Int): PracticeData? {
        var practiceData = PracticeData()
        practiceData.call_id = data.call_id
        practiceData.title = data.title
        practiceData.paj_step_path = data.paj_step_path
        practiceData.paj_step_index = feedbackStepPos
        practiceData.paj_step_path_index = stepPathIndex
        practiceData.paj_id = paj_id
        practiceData.progress_percent = archieveStep.progress!!
        practiceData.questionCode = getQuestionCodes(data.objects)
        return practiceData
    }

    private fun getQuestionCodes(listData: List<Data>): String {
        var questionCode = ArrayList<String>()
        for (data in listData) {
            questionCode.add(data.id.toString())
        }
        return questionCode.joinToString().replace(" ", "")
    }

}

