package com.embibe.videoplayer.player.audioPlayer

import android.content.Context
import android.media.MediaPlayer

class AudioPlayer {

    var mMediaPlayer: MediaPlayer = MediaPlayer()
    lateinit var audioFinishCallBack: AudioFinishCallBack

    fun stopAudio() {
        try {
            if (mMediaPlayer.isPlaying) { //If music is playing already
                mMediaPlayer.stop() //Stop playing the music
            }
            mMediaPlayer.reset()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun playAudio(mContext: Context, fileName: String, callback: AudioFinishCallBack) {
        try {
            stopAudio()
            val afd = mContext.assets.openFd(fileName)
            mMediaPlayer.setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)
            afd.close()
            //mMediaPlayer = MediaPlayer.create(mContext, mContext.resources.getIdentifier(fileName, "raw", mContext.packageName))
            mMediaPlayer.setOnCompletionListener {
                stopAudio()
                callback.onCompleted("")
            }
            mMediaPlayer.setOnPreparedListener { }
            mMediaPlayer.prepare()
            mMediaPlayer.start()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }


}

interface AudioFinishCallBack {
    fun onCompleted(msg: String)
}

