package com.embibe.embibetvapp.utils

/**
 * Created by Arpit Johri on 07-04-2020.
 */
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.util.Log
import com.embibe.embibetvapp.ui.interfaces.ConversionCallback


class SpeechToTextConverter(private val conversionCallback: ConversionCallback) :
    SpeechConverter {

    private val TAG = SpeechToTextConverter::class.java.name
    private var speechRecognizer: SpeechRecognizer? = null
    private var intent: Intent? = null
    private var listener: CustomRecognitionListener? = null

    override fun initialize(appContext: Activity?) {

        intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent?.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        intent?.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "en-US")
        intent?.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, true)

        //Add listeners
        listener = CustomRecognitionListener()
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(appContext)
        speechRecognizer?.setRecognitionListener(listener)
    }

    fun startListening() {
        speechRecognizer?.startListening(intent)
    }

    fun setRecognitionListener() {
        speechRecognizer?.setRecognitionListener(listener)
    }

    fun stopListening() {
        speechRecognizer?.stopListening()
    }

    internal inner class CustomRecognitionListener : RecognitionListener {

        override fun onReadyForSpeech(params: Bundle) {
            Log.i(TAG, "onReadyForSpeech")
            conversionCallback.onReady()
        }

        override fun onBeginningOfSpeech() {
            Log.i(TAG, "onBeginningOfSpeech")
        }

        override fun onRmsChanged(rmsdB: Float) {
//            Log.i(TAG, "onRmsChanged")
        }

        override fun onBufferReceived(buffer: ByteArray) {
            Log.i(TAG, "onBufferReceived")
        }

        override fun onEndOfSpeech() {
            Log.i(TAG, "onEndofSpeech")
        }

        override fun onError(error: Int) {
            Log.e(TAG, "error $error")
            conversionCallback.onErrorOccurred(getErrorText(error))
        }

        override fun onResults(results: Bundle) {
            val matchesFound = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
            Log.i(TAG, "onResults = $matchesFound")
            conversionCallback.onSuccess(matchesFound)
        }

        override fun onPartialResults(partialResults: Bundle) {
            Log.d(TAG, "onPartialResults")
        }

        override fun onEvent(eventType: Int, params: Bundle) {
            Log.d(TAG, "onEvent $eventType")
        }
    }

    override fun getErrorText(errorCode: Int): String {
        return when (errorCode) {
            SpeechRecognizer.ERROR_AUDIO -> "Audio recording error"
            SpeechRecognizer.ERROR_CLIENT -> "Client side error"
            SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS -> "Insufficient permissions"
            SpeechRecognizer.ERROR_NETWORK -> "Network error"
            SpeechRecognizer.ERROR_NETWORK_TIMEOUT -> "Network timeout"
            SpeechRecognizer.ERROR_NO_MATCH -> "No match"
            SpeechRecognizer.ERROR_RECOGNIZER_BUSY -> "RecognitionService busy"
            SpeechRecognizer.ERROR_SERVER -> "error from server"
            SpeechRecognizer.ERROR_SPEECH_TIMEOUT -> "No speech input"
            else -> "Didn't understand, please try again."
        }
    }

    fun releaseListener(isDestroy: Boolean) {
        speechRecognizer?.setRecognitionListener(null)
        if (isDestroy)
            speechRecognizer?.destroy()
    }
} 