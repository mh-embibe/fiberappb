package com.embibe.embibetvapp.utils

import android.app.Activity

/**
 * Created by Arpit Johri on 07-04-2020.
 */

interface SpeechConverter {
    fun initialize(appContext: Activity?)

    fun getErrorText(errorCode: Int): String
}
