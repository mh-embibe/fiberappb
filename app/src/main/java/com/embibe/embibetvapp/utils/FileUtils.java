package com.embibe.embibetvapp.utils;

import android.content.res.AssetManager;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;

public final class FileUtils {

    private static final String TAG_CLASS_NAME = FileUtils.class.getName();

    private FileUtils() {
        //default constructor
    }

    public static boolean deleteFile(File file) {
        if (file.exists()) {
            return (file.delete());
        }
        return false;
    }

    public static boolean deleteDirectory(File file) {
        if (file.exists()) {
            Log.d(TAG_CLASS_NAME, "Deleting existing assets");
            File[] files = file.listFiles();
            if (files == null) {
                return true;
            }
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (file.delete());
    }

    public static boolean copyAssetFolder(AssetManager assetManager, String fromAssetPath, String toPath) throws IOException {
        String[] files = assetManager.list(fromAssetPath);
        if (files.length == 0) {
            return false;
        }
        new File(toPath).mkdirs();
        boolean res = true;
        for (String file : files)
            if (file.contains(".")) {
                res &= copyAsset(assetManager, fromAssetPath + "/" + file, toPath + "/" + file);
            } else {
                res &= copyAssetFolder(assetManager, fromAssetPath + "/" + file, toPath + "/" + file);
            }
        return res;
    }

    public static boolean copyAsset(AssetManager assetManager, String fromAssetPath, String toPath) throws IOException {
        InputStream fis = null;
        OutputStream fos = null;
        try {
            fis = assetManager.open(fromAssetPath);
            new File(toPath).createNewFile();
            fos = new FileOutputStream(toPath);
            copyFile(fis, fos);
            return true;
        } finally {
            try {
                if (fis != null)
                    fis.close();
                if (fos != null) {
                    fos.flush();
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public static String getStringSizeLengthFile(long size) {
        DecimalFormat df = new DecimalFormat("0");
        float sizeKb = 1024.0f;
        float sizeMb = sizeKb * sizeKb;
        float sizeGb = sizeMb * sizeKb;
        float sizeTerra = sizeGb * sizeKb;
        if (size < sizeMb)
            return df.format(size / sizeKb) + " KB";
        else if (size < sizeGb)
            return df.format(size / sizeMb) + " MB";
        else if (size < sizeTerra)
            return df.format(size / sizeGb) + " GB";

        return "";
    }
}
