package com.embibe.embibetvapp.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.embibe.embibetvapp.constant.AppConstants;
import com.embibejio.coreapp.preference.PreferenceHelper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by Sriram on 05/03/21.
 */

public class CopyAssetsService extends JobIntentService {

    public static final int JOB_ID = 1005;
    private static final String TAG_CLASS_NAME = CopyAssetsService.class.getName();
    private static boolean assetsCopied;
    public PreferenceHelper pref = new PreferenceHelper();

    public CopyAssetsService() {
        super();
    }

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, CopyAssetsService.class, JOB_ID, intent);
    }

    public static boolean unzip(String path, InputStream zipInputStream) {
        ZipInputStream zis;
        try {
            String filename;
            zis = new ZipInputStream(new BufferedInputStream(zipInputStream));
            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;

            while ((ze = zis.getNextEntry()) != null) {
                filename = ze.getName();
                if (ze.isDirectory()) {
                    File fmd = new File(path + filename);
                    fmd.mkdirs();
                    continue;
                }
                File dir = new File(path);
                if (!dir.exists()) {
                    dir.mkdir();
                }
                File file = new File(path + filename);
                if (!file.exists()) {
                    file.createNewFile();
                }

                FileOutputStream fout = new FileOutputStream(file);
                while ((count = zis.read(buffer)) != -1) {
                    fout.write(buffer, 0, count);
                }
                fout.close();
                zis.closeEntry();
            }
            zis.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG_CLASS_NAME, "Destroyed");
        Intent assetsCopyIntent;
        if (assetsCopied) {
            assetsCopyIntent = new Intent(AppConstants.ACTION_ASSETS_COPY_COMPLETED);
        } else {
            assetsCopyIntent = new Intent(AppConstants.ACTION_ASSETS_COPY_FAILED);
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(assetsCopyIntent);
        super.onDestroy();
    }

    @Override
    protected void onHandleWork(@Nullable Intent intent) {
        copyAssets();
    }

    private void copyAssets() {
        int assetsVersion = pref.get(AppConstants.PREF_KEY_ASSETS_VERSION, 0);

        if (assetsVersion < AppConstants.ASSETS_VERSION) {
            Long startTime = System.currentTimeMillis();
            Log.i(TAG_CLASS_NAME, "Copying Assets starts");
            Log.i(TAG_CLASS_NAME, "start time: " + startTime);

            try {
                String assetsDestination = getApplicationContext().getFilesDir().getPath() + "/assets/";
                File assetFolder = new File(assetsDestination);
                FileUtils.deleteDirectory(assetFolder);
                assetFolder.mkdirs();
                //getMathJax(assetsDestination);
                getZip(assetsDestination, AppConstants.PRACTICE);
                getZip(assetsDestination, AppConstants.BOOK);
                getZip(assetsDestination, AppConstants.SYLLABUS);
                getZip(assetsDestination, AppConstants.KnowledgeGraph);
                /*FileUtils.copyAssetFolder(getApplicationContext().getAssets(), "css", assetsDestination + "css");
                Log.d(TAG_CLASS_NAME, "CSS COPIED");
                FileUtils.copyAssetFolder(getApplicationContext().getAssets(), "js", assetsDestination + "js");
                Log.d(TAG_CLASS_NAME, "JS COPIED");
                FileUtils.copyAssetFolder(getApplicationContext().getAssets(), "templates", assetsDestination + "templates");
                Log.d(TAG_CLASS_NAME, "TEMPLATES COPIED");*/
                pref.put(AppConstants.PREF_KEY_ASSETS_VERSION, AppConstants.ASSETS_VERSION);

                Log.i(TAG_CLASS_NAME, "Assets Copied Successfully");
                Long endTime = System.currentTimeMillis();
                Log.i(TAG_CLASS_NAME, "end time: " + endTime);
                Log.i(TAG_CLASS_NAME, "Time Taken: " + TimeUnit.MILLISECONDS.toSeconds(endTime - startTime) + " sec");
                assetsCopied = true;
            } catch (Exception e) {
                Log.e(TAG_CLASS_NAME, "Error while copying assets." + e.getMessage());
                assetsCopied = false;
                e.printStackTrace();
            }
        } else {
            assetsCopied = true;
        }
        Log.i(TAG_CLASS_NAME, "Assets already copied");
    }

    private void getMathJax(String destinationPath) {
        String zipFileName = AppConstants.MATH_JAX + AppConstants.ZIP_FORMAT;
        try {
            InputStream is = getResources().getAssets().open(zipFileName);
            unzip(destinationPath, is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*private void getPracticeZip(String destinationPath) {
        String zipFileName = "practice" + AppConstants.ZIP_FORMAT;
        try {
            InputStream is = getResources().getAssets().open(zipFileName);
            unzip(destinationPath, is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    private void getZip(String destinationPath, String folder_name) {
        String zipFileName = folder_name + AppConstants.ZIP_FORMAT;
        try {
            InputStream is = getResources().getAssets().open(zipFileName);
            unzip(destinationPath, is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
