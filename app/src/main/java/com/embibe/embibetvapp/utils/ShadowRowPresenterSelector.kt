/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.embibe.embibetvapp.utils

import androidx.leanback.widget.FocusHighlight.ZOOM_FACTOR_LARGE
import androidx.leanback.widget.ListRowPresenter
import androidx.leanback.widget.Presenter
import androidx.leanback.widget.PresenterSelector


class ShadowRowPresenterSelector : PresenterSelector() {

    val customListRowPresenter = object : ListRowPresenter(ZOOM_FACTOR_LARGE, false) {
        override fun isUsingDefaultListSelectEffect() = false
        override fun isUsingDefaultShadow(): Boolean {
            return false
        }

    }.apply {
        shadowEnabled = false
    }


    private val mShadowEnabledRowPresenter = customListRowPresenter
    private val mShadowDisabledRowPresenter = customListRowPresenter

    init {
        mShadowEnabledRowPresenter.setNumRows(1)
        mShadowDisabledRowPresenter.shadowEnabled = true
    }

    override fun getPresenter(item: Any): Presenter {

        if (item !is CardListRow) return mShadowDisabledRowPresenter
        return mShadowDisabledRowPresenter
    }

    override fun getPresenters(): Array<Presenter> {
        return arrayOf(mShadowDisabledRowPresenter, mShadowEnabledRowPresenter)
    }

    fun getListRowPresenter(): ListRowPresenter = customListRowPresenter

}