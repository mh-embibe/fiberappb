package com.embibe.embibetvapp.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Process
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.ui.activity.SplashActivity
import com.embibe.embibetvapp.ui.activity.UserSwitchActivity
import com.embibejio.coreapp.events.SegmentEvents
import com.embibejio.coreapp.preference.PreferenceHelper
import java.io.PrintWriter
import java.io.StringWriter
import kotlin.system.exitProcess

class ExceptionHandler(private val context: Activity) :
    Thread.UncaughtExceptionHandler {
    lateinit var intent: Intent
    var pref = PreferenceHelper()

    @SuppressLint("ApplySharedPref")
    override fun uncaughtException(thread: Thread, exception: Throwable) {
        val stackTrace = StringWriter()
        exception.printStackTrace(PrintWriter(stackTrace))
        /*    val preferences = PreferenceManager.getDefaultSharedPreferences(LibApp.context)
            //using commit to write pref immediately in case of crash
            //preferences.edit().putBoolean(AppConstants.IS_APP_CRASHED, true).commit()
            */
        intent = if (pref[AppConstants.IS_LOGIN, false]) {
            Intent(context, UserSwitchActivity::class.java)
        } else
            Intent(context, SplashActivity::class.java)
        Crashlytics.logException(exception)
        Log.d("LOG_TYPE_CRASH", stackTrace.toString())
        SegmentUtils.trackEventCrash(SegmentEvents.LOG_TYPE_CRASH, stackTrace.toString())
        context.startActivity(intent)
        context.finish()
        Process.killProcess(Process.myPid())
        exitProcess(10)
    }

}