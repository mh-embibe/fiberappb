package com.embibe.embibetvapp.utils

import android.content.Context
import android.util.TypedValue
import android.view.View
import androidx.constraintlayout.widget.Group

/**
 * Shows the system software keyboard.
 *
 * @param requestFocus whether to request the focus on the specified [View].
 */
internal fun View.showKeyboard(requestFocus: Boolean = true) {
    if (requestFocus) {
        requestFocus()
    }

    this.context.showKeyboard(this)
}


/**
 * Hides the system software keyboard.
 *
 * @param clearFocus whether to remove the focus from the input field
 */
internal fun View.hideKeyboard(clearFocus: Boolean = true) {
    if (clearFocus) {
        clearFocus()
    }

    this.context.hideKeyboard(this)
}

fun Group.setAllOnClickListener(listener: View.OnClickListener?) {
    referencedIds.forEach { id ->
        rootView.findViewById<View>(id).setOnClickListener(listener)
    }
}


// Extension method to convert pixels to dp
fun Int.toDp(context: Context): Int = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), context.resources.displayMetrics
).toInt()

