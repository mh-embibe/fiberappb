package com.embibe.app.utils

import com.embibe.embibetvapp.utils.Status

data class Resource<T>(val status: Status, val data: Any?, val message: String?) {
    constructor(status: Status) : this(status, null, null)
    constructor(status: Status, data: Any) : this(status, data, null)
    constructor(status: Status, message: String?) : this(status, null, message)

    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }
    }
}