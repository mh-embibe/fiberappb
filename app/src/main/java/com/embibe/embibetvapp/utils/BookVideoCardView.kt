package com.embibe.embibetvapp.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.leanback.widget.BaseCardView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants.NOT_ANIMATE
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory


@SuppressLint("ViewConstructor")
class BookVideoCardView(context: Context, styleResId: Int) :
    BaseCardView(ContextThemeWrapper(context, styleResId), null, 0) {

    //lateinit var ivVideo: SimpleDraweeView
    lateinit var iv_thumbnail: ImageView
    lateinit var playerView: PlayerView
    lateinit var cardView: CardView
    lateinit var player: Player
    private lateinit var simpleExoPlayer: SimpleExoPlayer
    private lateinit var mediaDataSourceFactory: DataSource.Factory

    init {
        buildImageCardView()
    }

    companion object;

    private fun buildImageCardView() {
        isFocusable = true
        isFocusableInTouchMode = true
        val context = context
        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.card_book, this)
        //ivVideo = findViewById(R.id.ivVideo)
        playerView = findViewById<View>(R.id.playerView) as PlayerView
        iv_thumbnail = findViewById(R.id.iv_thumbnail)
        cardView = findViewById(R.id.cardView)
        //initializePlayer()
    }

    fun hideVideoView() {
        tag = NOT_ANIMATE
        iv_thumbnail.visibility = View.VISIBLE
        playerView.visibility = View.INVISIBLE
        cardView.visibility = View.INVISIBLE
        releasePlayer()
    }


    fun showVideoView(previewURl: String) {
        //setGif()
        hideImageView()
        initializePlayer(previewURl)
        setWebP(previewURl)
    }

    fun hideImageView() {
        //iv_thumbnail.visibility = View.GONE
        playerView.visibility = View.VISIBLE
        cardView.visibility = View.VISIBLE
    }

    private fun setWebP(previewURl: String) {
        simpleExoPlayer.playWhenReady = true
    }


    private fun initializePlayer(previewURl: String) {

        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(context)
        mediaDataSourceFactory = DefaultDataSourceFactory(context, "mediaPlayerSample")
        val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory)
            .createMediaSource(Uri.parse(previewURl))
        simpleExoPlayer.prepare(mediaSource, false, false)
        simpleExoPlayer.playWhenReady = false
        playerView.setShutterBackgroundColor(Color.TRANSPARENT)
        playerView.player = simpleExoPlayer
        playerView.requestFocus()
        simpleExoPlayer.addListener(object : Player.DefaultEventListener() {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    Player.STATE_IDLE -> {
                    }
                    Player.STATE_BUFFERING -> {
                    }
                    Player.STATE_READY -> {
                        // showVideoView()
                    }
                    Player.STATE_ENDED -> {
                        hideVideoView()
                    }
                }
            }
        })
    }

    private fun releasePlayer() {
        if (::simpleExoPlayer.isInitialized)
            simpleExoPlayer.release()
    }


}