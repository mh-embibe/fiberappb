import com.google.gson.annotations.SerializedName


data class VimeoVideo(
    @SerializedName("quality") val quality: String,
    @SerializedName("type") val type: String,
    @SerializedName("width") val width: Int,
    @SerializedName("height") val height: Int,
    @SerializedName("link") val link: String,
    @SerializedName("created_time") val created_time: String,
    @SerializedName("md5") val md5: String,
    @SerializedName("public_name") val public_name: String,
    @SerializedName("size_short") val size_short: String
)