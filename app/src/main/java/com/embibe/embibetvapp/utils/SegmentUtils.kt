/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.embibe.embibetvapp.utils

import android.util.ArraySet
import com.embibe.embibetvapp.constant.AppConstants.PLAYER_FULLSCREEN
import com.embibe.embibetvapp.constant.AppConstants.PLAYER_PAUSE
import com.embibe.embibetvapp.constant.AppConstants.PLAYER_PLAY
import com.embibe.embibetvapp.newmodel.Content
import com.embibejio.coreapp.events.EventParams
import com.embibejio.coreapp.events.SegmentEvents
import com.embibejio.coreapp.events.SegmentIO
import com.embibejio.coreapp.events.VideoEventParams
import com.embibejio.coreapp.model.LinkedProfile
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import org.json.simple.parser.JSONParser
import java.util.regex.Pattern


/**
 * A collection of utility methods, all static.
 */
object SegmentUtils {

    fun trackTestScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST,
                SegmentEvents.EVENT_TEST_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackTestScreenLoadEnd(
        id: String,
        title: String,
        subject: String,
        embiumCoins: String
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.content_id = id
            eventParams.content_name = title
            eventParams.content_subject = subject
            eventParams.embiums_earned = embiumCoins
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST,
                SegmentEvents.EVENT_TEST_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD,
                eventParams
            )
        }
    }

    fun trackTestScreenHeroBannerMainBtnFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST,
                SegmentEvents.EVENT_TEST_SCREEN_HEROBANNER_MAIN_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_HEROBANNER,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestScreenHeroBannerMainBtnCLick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST,
                SegmentEvents.EVENT_TEST_SCREEN_HEROBANNER_MAIN_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_HEROBANNER,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestScreenTileFocus(
        indexOfRow: Int,
        indexOfItem: Int,
        contentId: String,
        subject: String,
        sectionName: String
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.scroll_depth_vertical = ""
            eventParams.scroll_depth_horizontal = ""
            eventParams.tile_section = ""
            eventParams.content_id = ""
            eventParams.subject = ""
            eventParams.content_name = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST,
                SegmentEvents.EVENT_TEST_SCREEN_TEST_TILE_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE_CAROUSEL,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackTestScreenTileClick(
        indexOf: Int,
        indexOf1: Int,
        contentId: String,
        subject: String,
        sectionName: String, content: Content
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.scroll_depth_vertical = ""
            eventParams.scroll_depth_horizontal = ""
            eventParams.tile_section = ""
            eventParams.content_id = ""
            eventParams.subject = ""
            eventParams.content_name = ""
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST,
                SegmentEvents.EVENT_TEST_SCREEN_TEST_TILE_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE_CAROUSEL,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackTestScreenAdBannerFocus(item: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.adverstiment_banner_rank = ""
            eventParams.banner_type = item.category
            eventParams.banner_target_type = item.category
            eventParams.banner_tiltle = item.title
            eventParams.banner_content_type = item.sub_type
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST,
                SegmentEvents.EVENT_TEST_SCREEN_ADVERSTIMENT_BANNER_CTA_FOCUS,
                SegmentEvents.NAV_ELEMENT_ADVERSTIMENT_BANNER,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackTestScreenAdBannerClick(item: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.adverstiment_banner_rank = ""
            eventParams.banner_type = item.category
            eventParams.banner_target_type = item.category
            eventParams.banner_tiltle = item.title
            eventParams.banner_content_type = item.sub_type
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST,
                SegmentEvents.EVENT_TEST_SCREEN_ADVERSTIMENT_BANNER_CTA_CLICK,
                SegmentEvents.NAV_ELEMENT_ADVERSTIMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackTestScreenCreateCustomTestFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST,
                SegmentEvents.EVENT_TEST_SCREEN_CREATE_CUSTOM_TEST_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestScreenCreateCustomTestCLick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST,
                SegmentEvents.EVENT_TEST_SCREEN_CREATE_CUSTOM_TEST_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestInstructionScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_INSTRUCTION,
                SegmentEvents.EVENT_TEST_INSTRUCTION_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackTestInstructionScreenLoadEnd() {
        doAsync {
            val eventParams = EventParams()
            eventParams.test_id = ""
            eventParams.test_name = ""
            eventParams.test_im_tree = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_INSTRUCTION,
                SegmentEvents.EVENT_TEST_INSTRUCTION_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD,
                eventParams
            )
        }
    }

    fun trackTestInstructionScreenSwapFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_INSTRUCTION,
                SegmentEvents.EVENT_TEST_INSTRUCTION_SCREEN_SWAP_CTA_FOCUS,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestInstructionScreenSwapClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.user_type = ""
            eventParams.email_id = ""
            eventParams.grade = ""
            eventParams.goal = ""
            eventParams.board = ""
            eventParams.isPaid = false
            eventParams.session_id = ""
            eventParams.user_id = ""
            eventParams.test_name = ""
            eventParams.test_id = ""
            eventParams.test_im_tree = ""
            eventParams.test_session = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_INSTRUCTION,
                SegmentEvents.EVENT_TEST_INSTRUCTION_SCREEN_SWAP_CTA_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackTestInstructionScreenBoxFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_INSTRUCTION,
                SegmentEvents.EVENT_TEST_INSTRUCTION_SCREEN_BOX_FOCUS,
                SegmentEvents.NAV_ELEMENT_BOX,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestInstructionScreenBoxClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_INSTRUCTION,
                SegmentEvents.EVENT_TEST_INSTRUCTION_SCREEN_BOX_CHECK,
                SegmentEvents.NAV_ELEMENT_BOX,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestInstructionScreenStartNowFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_INSTRUCTION,
                SegmentEvents.EVENT_TEST_INSTRUCTION_SCREEN_START_NOW_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestInstructionScreenStartNowClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_INSTRUCTION,
                SegmentEvents.EVENT_TEST_INSTRUCTION_SCREEN_START_NOW_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestTakingScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackTestTakingScreenLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackTestTakingScreenOptionFocus() {
        doAsync {
            val eventParams = EventParams()
            eventParams.last_option_id = ""
            eventParams.current_option_id = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_OPTIONS_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackTestTakingScreenOptionClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.last_option_id = ""
            eventParams.current_option_id = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_OPTIONS_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackTestTakingScreenDropdownFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_DROPDOWN_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestTakingScreenDropdownClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_DROPDOWN_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestTakingScreenDropdownValueFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_DROPDOWN_VALUE_FOCUS,
                SegmentEvents.NAV_ELEMENT_OPTIONS,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestTakingScreenDropdownValueClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.response = ""
            eventParams.response_answer = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_DROPDOWN_VALUE_CLICK,
                SegmentEvents.NAV_ELEMENT_OPTIONS,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackTestTakingScreenSaveAndNextFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_SAVE_NEXT_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestTakingScreenSaveAndNextClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.is_response = false
            eventParams.question_type = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_SAVE_NEXT_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackTestTakingScreenSubmitTestFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestTakingScreenSubmitTestCLick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestTakingScreenSubmitTestPopup() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_LOAD,
                null,
                SegmentEvents.NAV_ELEMENT_POPUP
            )
        }
    }

    fun trackTestTakingScreenSubmitTestPopupFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_TEST_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestTakingScreenSubmitTestPopupClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_TEST_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestTakingScreenContinueTestPopupFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_CONTINUE_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestTakingScreenContinueTestPopupClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SUBMIT_TEST_POPUP_CONTINUE_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestTakingScreenSubmitTestPopupExit() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_SUBMIT_TEST_POPUP_EXIT,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestTakingScreenViewPaperFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestTakingScreenViewPaperClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestTakingScreenInstructionFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_INSTRUCTIONS_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestTakingScreenInstructionClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_INSTRUCTIONS_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestTakingScreenMarkedReviewFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_MARKED_REVIEW_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestTakingScreenMarkedReviewClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.is_response = false
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_MARKED_REVIEW_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackTestTakingScreenPreviousFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_PREVIOUS_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestTakingScreenPreviousCLick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.is_response = false
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_PREVIOUS_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackTestTakingScreenSectionFocus() {
        doAsync {
            val eventParams = EventParams()
            eventParams.section_position = 0
            eventParams.section_name = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_SECTION_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackTestTakingScreenSectionClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.section_position = 0
            eventParams.section_name = ""
            eventParams.question_count = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_SECTION_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackTestTakingScreenAttemptQuestionFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_ATTEMPT_OVERVIEW_QUESTION_NO_FOCUS,
                SegmentEvents.EVENT_QUESTION_NUMBER,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }


    fun trackTestTakingScreenAttemptQuestionClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_ATTEMPT_OVERVIEW_QUESTION_NO_CLICK,
                SegmentEvents.EVENT_QUESTION_NUMBER,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestTakingScreenViewPaperFilterAttemptlick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.filter_by_attempt_types = ""
            eventParams.is_select = false
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_FILTER_ATTEMPT_CLICK,
                SegmentEvents.NAV_ELEMENT_BOX,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackTestTakingScreenViewPaperSectionFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_SECTION_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestTakingScreenViewPaperSectionCLick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_SECTION_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestTakingScreenViewPaperBackFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_BACK_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestTakingScreenViewPaperBackCLick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_VIEW_PAPER_BACK_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestTakingScreenInstructionBackFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_TAKING,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_INSTRUCTION_BACK_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestTakingScreenInstructionBackCLick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_TEST_TAKING_SCREEN_INSTRUCTION_BACK_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    /*create test*/

    fun trackCreateTestScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackCreateTestScreenLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackCreateTestScreenSubjectSelectFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_SELECT_SUBJECTS_FOCUS,
                SegmentEvents.NAV_ELEMENT_SUBJECT_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackCreateTestScreenSubjectSelectClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.subject = ""
            eventParams.subject_rank = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_SELECT_SUBJECT_CLICK,
                SegmentEvents.NAV_ELEMENT_SUBJECT_TILE,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenLearntEmbibeFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_LEARNT_ON_EMBIBE_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackCreateTestScreenLearntEmbibeClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_LEARNT_ON_EMBIBE_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackCreateTestScreenNextFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_NEXT_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackCreateTestScreenNextClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.tile_section = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_NEXT_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackCreateTestChaptersScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_CHAPTERS_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackCreateTestChaptersScreenLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_CHAPTERS_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackCreateTestScreenChaptersFocus() {
        doAsync {
            val eventParams = EventParams()
            eventParams.subject = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_CHAPTERS_SUBJECT_FOCUS,
                SegmentEvents.NAV_ELEMENT_SIDE_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenChaptersClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.subject = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_CHAPTERS_SUBJECT_CLICK,
                SegmentEvents.NAV_ELEMENT_SIDE_TILE,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenChaptersLearntFocus() {
        doAsync {
            val eventParams = EventParams()
            eventParams.subject = ""
            eventParams.chapter = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_CHAPTERS_LEARNT_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenChaptersLearntClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.subject = ""
            eventParams.chapter = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_CHAPTERS_LEARNT_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenChaptersNextFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_CHAPTERS_NEXT_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackCreateTestScreenChaptersNextClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.chapter_count = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_CHAPTERS_NEXT_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenChaptersBackFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_CHAPTERS_BACK_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackCreateTestScreenChaptersBackClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.chapter_count = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_CHAPTERS_BACK_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenSettingLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_SETTING_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackCreateTestScreenSettingLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_SETTING_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackCreateTestScreenSettingDifficultyLevelFocus() {
        doAsync {
            val eventParams = EventParams()
            eventParams.difficulty_level = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_TEST_SETTING_DIFFICULTY_LEVEL_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenSettingDifficultyLevelCLick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.difficulty_level = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_TEST_SETTING_DIFFICULTY_LEVEL_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenSettingDurationFocus() {
        doAsync {
            val eventParams = EventParams()
            eventParams.duration = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_TEST_SETTING_DURATION_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenSettingDurationCLick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.duration = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_TEST_SETTING_DURATION_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenSettingCorrectAnsMarksFocus() {
        doAsync {
            val eventParams = EventParams()
            eventParams.correct_ans_marks = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_SETTING_CORRECT_ANS_MARKS_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenSettingCorrectAnsMarksClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.correct_ans_marks = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_SETTING_CORRECT_ANS_MARKS_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenSettingIncorrectAnsMarksFocus() {
        doAsync {
            val eventParams = EventParams()
            eventParams.incorrect_ans_marks = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_SETTING_INCORRECT_ANS_MARKS_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenSettingIncorrectAnsMarksClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.incorrect_ans_marks = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_SETTING_INCORRECT_ANS_MARKS_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackCreateTestScreenSettingCreateTestFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_TEST_SETTING_CREATE_TEST_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackCreateTestScreenSettingCreateTestClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_TEST_SETTING_CREATE_TEST_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackCreateTestScreenSettingBackFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_TEST_SETTING_BACK_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackCreateTestScreenSettingBackClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_TEST_SETTING_BACK_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackCreateTestScreenCustomTestLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_GENERATING_CUSTOM_TEST_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackCreateTestScreenCustomTestLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CREATE_TEST,
                SegmentEvents.EVENT_CREATE_TEST_SCREEN_GENERATING_CUSTOM_TEST_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackTestMoreInfoScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_MORE_INFO,
                SegmentEvents.EVENT_TEST_MORE_INFO_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackTestMoreInfoScreenLoadEnd() {
        doAsync {
            val eventParams = EventParams()
            eventParams.embiums = ""
            eventParams.special_flag = ""
            eventParams.test_count = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_MORE_INFO,
                SegmentEvents.EVENT_TEST_MORE_INFO_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD,
                eventParams
            )
        }
    }

    fun trackTestMoreInfoScreenTestCardFocus() {
        doAsync {
            val eventParams = EventParams()
            eventParams.scroll_depth_vertical = ""
            eventParams.test_count = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_MORE_INFO,
                SegmentEvents.EVENT_TEST_MORE_INFO_SCREEN_TEST_CARD_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackTestMoreInfoScreenStartTestCardClick() {
        doAsync {
            val eventParams = EventParams()
            eventParams.scroll_depth_vertical = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_MORE_INFO,
                SegmentEvents.EVENT_TEST_MORE_INFO_SCREEN_START_TEST_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackTestMoreInfoScreenFeedBackFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_MORE_INFO,
                SegmentEvents.EVENT_TEST_MORE_INFO_SCREEN_VIEW_FEEDBACK_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestMoreInfoScreenFeedBackClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_MORE_INFO,
                SegmentEvents.EVENT_TEST_MORE_INFO_SCREEN_VIEW_FEEDBACK_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestMoreInfoScreenBookMarkFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_MORE_INFO,
                SegmentEvents.EVENT_TEST_MORE_INFO_SCREEN_BOOKMARK_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestMoreInfoScreenBookMarkClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_MORE_INFO,
                SegmentEvents.EVENT_TEST_MORE_INFO_SCREEN_BOOKMARK_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }


    //*******************Terms and condition event*************************//
    fun trackTermsAndConditionLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TERMS_CONDITION,
                SegmentEvents.EVENT_TERM_CONDITION_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackTermsAndConditionLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TERMS_CONDITION,
                SegmentEvents.EVENT_TERM_CONDITION_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackTermsAndConditionBackPress() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TERMS_CONDITION,
                SegmentEvents.EVENT_TERM_CONDITION_SCREEN_BACK_BTN_PRESS,
                null,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTermsAndConditionScroll() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TERMS_CONDITION,
                SegmentEvents.EVENT_TERM_CONDITION_SCREEN_SCROLL,
                null,
                SegmentEvents.EVENT_TYPE_SCROLL
            )
        }
    }


    //********************* New Signup page events**************************//
    fun trackSignInLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SIGN_IN,
                SegmentEvents.EVENT_SIGNIN_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackSignInLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SIGN_IN,
                SegmentEvents.EVENT_SIGNIN_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackSigninGetOptClick(email: String, mobile: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.email_id = email
            eventParams.mobile = mobile
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SIGN_IN,
                SegmentEvents.EVENT_SIGNIN_GET_OPT_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun tracLoginWithJioClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SIGN_IN,
                SegmentEvents.EVENT_SIGNIN_LOGIN_WITH_JIO_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun tracTermsConditionsClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SIGN_IN,
                SegmentEvents.EVENT_SIGNIN_TERMS_CONDITION_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun tracClearOtpClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SIGN_IN,
                SegmentEvents.EVENT_SIGNIN_CLEAR_OTP_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackSigninGoalSelectionDoneClick(goal: String, exam: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.goal = goal
            eventParams.exam = exam
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SIGN_IN,
                SegmentEvents.EVENT_SIGNIN_GOAL_SELECTION_DONE_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    //**********************Onboarding page events*******************************//
    fun trackOnboardingLoadStart(pos: Int) {
        doAsync {
            val eventParams = EventParams()
            eventParams.onboarding_screen_position = pos
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_ONBOARDING,
                SegmentEvents.EVENT_ONBOARDING_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD,
                eventParams
            )
        }
    }

    fun trackOnboardingLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_ONBOARDING,
                SegmentEvents.EVENT_ONBOARDING_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackOnboardingRightButton(position: Int) {
        doAsync {
            val eventParams = EventParams()
            eventParams.onboarding_screen_position = position
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_ONBOARDING,
                SegmentEvents.EVENT_ONBOARDING_SCREEN_RIGHT_BUTTON,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackOnboardingLeftButton(position: Int) {
        doAsync {
            val eventParams = EventParams()
            eventParams.onboarding_screen_position = position
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_ONBOARDING,
                SegmentEvents.EVENT_ONBOARDING_SCREEN_LEFT_BUTTON,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    //**********************Splash page events*******************************//
    fun trackAppLaunchEvents() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SPLASH,
                SegmentEvents.EVENT_APP_LAUNCH
            )
        }
    }

    fun trackSplashLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SPLASH,
                SegmentEvents.EVENT_SPLASH_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackSplashLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SPLASH,
                SegmentEvents.EVENT_SPLASH_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    //*********************login page events**********************************//

    fun trackLoginLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_LOGIN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackLoginLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SPLASH,
                SegmentEvents.EVENT_LOGIN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    /*Verify Btn Click*/
    fun trackLoginVerifyOtpBtnClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_VERIFY_OTP_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }


    /*Resend OTP Btn Click*/
    fun trackResendOtpBtnClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_RE_SEND_OTP_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    /*Sign IN Focus*/
    fun trackSignInFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_LOGIN_SIGNIN_BTN_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    /*Sign IN Click*/
    fun trackSignInClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_LOGIN_SIGNIN_BTN_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    /*Sign UP Focus*/
    fun trackSignUpFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_LOGIN_SIGNUP_BTN_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }


    /*Sign UP Click*/
    fun trackSignUpClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_LOGIN_SIGNUP_BTN_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    /*switch user load start */
    fun trackSwitchUserScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_SWITCH_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    /*switch user load end*/
    fun trackSwitchUserScreenLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SPLASH,
                SegmentEvents.EVENT_SWITCH_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    /*avatar profiles load start */
    fun trackSelectAvatarScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_SCREEN_CHOOSE_AVATAR_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    /*avatar profiles load end*/
    fun trackSelectAvatarScreenLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SPLASH,
                SegmentEvents.EVENT_SCREEN_CHOOSE_AVATAR_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    /*avatar icon Click*/
    fun trackAvatarIconClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_AVATAR_ICON_CLICKED,
                SegmentEvents.NAV_ELEMENT_ICON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    /*avatar profiles load start */
    fun trackForgotPasswordLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_FORGOT_PASSWORD,
                SegmentEvents.EVENT_FORGO_PASSWORD_MOBILE_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    /*forgot password load end*/
    fun trackForgotPasswordLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_FORGOT_PASSWORD,
                SegmentEvents.EVENT_FORGO_PASSWORD_MOBILE_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }


    /*ForgotPassword Submit Click*/
    fun trackForgotPasswordSubmitClick(mobleNumber: String) {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_FORGOT_PASSWORD,
                SegmentEvents.EVENT_FORGO_PASSWORD_SUBMIT_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    /*ForgotPassword BackToSignIn Click*/
    fun trackForgotPasswordBackToSignInClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_FORGOT_PASSWORD,
                SegmentEvents.EVENT_FORGO_PASSWORD_BACK_TO_SIGN_IN_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }


    /*OTP screen load start */
    fun trackOTPLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_FORGOT_PASSWORD,
                SegmentEvents.EVENT_FORGO_PASSWORD_OTP_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    /*forgot password load end*/
    fun trackOTPLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_FORGOT_PASSWORD,
                SegmentEvents.EVENT_FORGO_PASSWORD_OTP_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    /*Verify OTP click*/
    fun trackVerifyOTPClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_FORGOT_PASSWORD,
                SegmentEvents.EVENT_FORGO_PASSWORD_VERIFY_OTP_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    /*Resend OTP click*/
    fun trackResendOTPClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_FORGOT_PASSWORD,
                SegmentEvents.EVENT_FORGO_PASSWORD_RESEND_OTP_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    /*ReEnter OTP click*/
    fun trackReEnterOTPClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_FORGOT_PASSWORD,
                SegmentEvents.EVENT_FORGO_PASSWORD_RE_ENTER_OTP_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    /*SWitchUser screen load start */
    fun trackSWitchUserLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SWITCH_USER,
                SegmentEvents.EVENT_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    /*SWitchUser load end*/
    /* fun trackSWitchUserLoadEnd() {
         doAsync {
             SegmentIO.getInstance(App.context).track(
                 SegmentEvents.LOG_TYPE_USER_SWITCH_WINDOW,
                 SegmentEvents.EVENT_SCREEN_LOAD_END,
                 null,
                 SegmentEvents.EVENT_TYPE_LOAD
             )
         }
     }*/

    /*Profile click*/
    fun trackProfileClick(userId: String?, position: Int) {
        doAsync {
            val eventParams = EventParams()
            eventParams.user_id = userId.toString()
            eventParams.current_item_position = position
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SWITCH_USER,
                SegmentEvents.EVENT_SWITCH_USER_PROFILE_CLICK,
                SegmentEvents.NAV_ELEMENT_BODY,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    /*ReEnter OTP click*/
    fun trackAddUserClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_FORGOT_PASSWORD,
                SegmentEvents.EVENT_SWITCH_USER_ADD_USER_CLICK,
                SegmentEvents.NAV_ELEMENT_BODY,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun loginEvents() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_LOGIN_AW
            )
        }
    }

    fun loginBTNClicked() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_CLICK_LOGIN_AW
            )
        }
    }

    fun loginSuccessEvents(
        mobile: String,
        password: String,
        userid: String,
        userType: String,
        status: String
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.mobile = mobile
            eventParams.password = password
            eventParams.user_id = userid
            eventParams.user_type = userType
            eventParams.status = status
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_LOGIN_SUCCESS_AW, eventParams
            )
        }

    }

    fun loginFailureEvents(mobile: String, password: String, status: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.mobile = mobile
            eventParams.password = password
            eventParams.status = status
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LOGIN,
                SegmentEvents.EVENT_LOGIN_FAILED_AW, eventParams
            )
        }
    }

    /*Book Details Events*/
    fun trackBookDetailsEvent(obj: JSONObject) {
        doAsync {
            SegmentIO.getInstance().track(
                false,
                if (obj.has("log_type") && !obj.isNull("log_type")) obj.getString("log_type") else null,
                if (obj.has("event_name") && !obj.isNull("event_name")) obj.getString("event_name") else null,
                if (obj.has("event_code") && !obj.isNull("event_code")) obj.getString("event_code") else null,
                if (obj.has("nav_element") && !obj.isNull("nav_element")) obj.getString("nav_element") else null,
                if (obj.has("event_type") && !obj.isNull("event_type")) obj.getString("event_type") else null,
                if (obj.has("extra_params") && !obj.isNull("extra_params")) obj.getJSONObject("extra_params") else null,
                if (obj.has("session_id") && !obj.isNull("session_id")) obj.getString("session_id") else null
            )
        }
    }

    /*Book Details Events*/
    fun trackPracticeEvent(obj: JSONObject) {
        doAsync {
            SegmentIO.getInstance().track(
                false,
                if (obj.has("log_type") && !obj.isNull("log_type")) obj.getString("log_type") else null,
                if (obj.has("event_name") && !obj.isNull("event_name")) obj.getString("event_name") else null,
                if (obj.has("event_code") && !obj.isNull("event_code")) obj.getString("event_code") else null,
                if (obj.has("nav_element") && !obj.isNull("nav_element")) obj.getString("nav_element") else null,
                if (obj.has("event_type") && !obj.isNull("event_type")) obj.getString("event_type") else null,
                if (obj.has("extra_params") && !obj.isNull("extra_params")) obj.getJSONObject("extra_params") else null,
                if (obj.has("session_id") && !obj.isNull("session_id")) obj.getString("session_id") else null
            )
        }
    }

    //*************************User Switch page events*********************************//

    fun trackViewUsersEvents(dataList: List<LinkedProfile?>) {
        doAsync {
            val eventParams = EventParams()
            val childs = arrayListOf<String>()
            for (child in dataList) {
                childs.add(child?.userId!!)
            }
            eventParams.childs = childs
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_USER_SWITCH_WINDOW,
                SegmentEvents.EVENT_VIEW_USERS_AW, eventParams
            )
        }

    }

    fun trackSelectUserEvents(userId: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.selected_user = userId.toString()
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_USER_SWITCH_WINDOW,
                SegmentEvents.EVENT_SELECT_USER_AW, eventParams
            )
        }

    }

    //*****************Learn rows events**********************//
    fun trackEventContentSelected(item: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.content_id = item.id
            eventParams.format_reference = item.learning_map.format_id
            eventParams.learnpath_name = item.learnpath_name
            eventParams.learnpath_format_name = item.learnpath_format_name
            eventParams.category = item.type/*here type as Category like Coobo / Video */
            eventParams.embiums_available = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_WINDOW,
                SegmentEvents.EVENT_CONTENT_SELECTION, eventParams
            )
        }

    }

    fun trackEventSubjectSelected(item: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.subject_code = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_WINDOW,
                SegmentEvents.EVENT_SUBJECT_SELECTION, eventParams
            )
        }
    }

    /* tile click*/
    fun trackEventHomeTileClick(
        content: Content,
        sectionName: String,
        sectionPosition: Int,
        currentItemPosition: Int
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.tile_type = content.type
            eventParams.tile_section = sectionName
            eventParams.tile_properties =
                JSONParser().parse(Gson().toJson(content)) as org.json.simple.JSONObject
            eventParams.scroll_depth_vertical = sectionPosition.toString()
            eventParams.scroll_depth_horizontal = currentItemPosition.toString()
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_HOME_TILE_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE_CAROUSEL,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    /* recommendation tile click*/
    fun trackEventRecommendationTileClick(content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.tile_type = content.type
            eventParams.tile_section = content.section_name
            eventParams.tile_properties =
                JSONParser().parse(Gson().toJson(content)) as org.json.simple.JSONObject
            eventParams.scroll_depth_vertical = ""
            eventParams.scroll_depth_horizontal = ""
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_RECOMMENDATION,
                SegmentEvents.EVENT_RECOMMENDED_VIDEO_TILE_CLICK,
                SegmentEvents.NAV_ELEMENT_RECOMMENDED_VIDEO_TILE,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    /* Ad Banner click*/
    fun trackEventHomeBannerClick(content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.banner_tiltle = content.title
            //eventParams.adverstiment_banner_rank = ""
            eventParams.banner_type = content.category
            eventParams.banner_target_type = content.category
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_ADVERSTIMENT_BANNER_CLICK,
                SegmentEvents.NAV_ELEMENT_ADVERSTIMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    /* tile focus*/
    fun trackEventHomeTileFocus(
        content: Content,
        sectionName: String,
        sectionPos: Int,
        currentItemPosition: Int
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.tile_type = content.type
            eventParams.tile_section = sectionName
            eventParams.tile_properties =
                JSONParser().parse(Gson().toJson(content)) as org.json.simple.JSONObject
            eventParams.scroll_depth_vertical = sectionPos.toString()
            eventParams.scroll_depth_horizontal = currentItemPosition.toString()
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_HOME_TILE_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE_CAROUSEL,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    /* recommendation tile click*/
    fun trackEventRecommendationTileFocus(content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.tile_type = content.type
            eventParams.tile_section = content.section_name
            eventParams.tile_properties =
                JSONParser().parse(Gson().toJson(content)) as org.json.simple.JSONObject
            eventParams.scroll_depth_vertical = ""
            eventParams.scroll_depth_horizontal = ""
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_RECOMMENDATION,
                SegmentEvents.EVENT_RECOMMENDED_VIDEO_TILE_FOCUS,
                SegmentEvents.NAV_ELEMENT_RECOMMENDED_VIDEO_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    /* recommendation next video tile focus*/

    fun trackEventRecommendationNextVideoTileFocus(content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.tile_type = content.type
            eventParams.tile_section = content.section_name
            eventParams.tile_properties =
                JSONParser().parse(Gson().toJson(content)) as org.json.simple.JSONObject
            eventParams.scroll_depth_vertical = ""
            eventParams.scroll_depth_horizontal = ""
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_RECOMMENDATION,
                SegmentEvents.EVENT_NEXT_VIDEO_TILE_FOCUS,
                SegmentEvents.NAV_ELEMENT_NEXT_VIDEO_TILE,
                eventParams
            )
        }
    }

    /* recommendation next video tile click*/

    fun trackEventRecommendationNextVideoTileClick(content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.tile_type = content.type
            eventParams.tile_section = content.section_name
            eventParams.tile_properties =
                JSONParser().parse(Gson().toJson(content)) as org.json.simple.JSONObject
            eventParams.scroll_depth_vertical = ""
            eventParams.scroll_depth_horizontal = ""
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_RECOMMENDATION,
                SegmentEvents.EVENT_NEXT_VIDEO_TILE_CLICK,
                SegmentEvents.NAV_ELEMENT_NEXT_VIDEO_TILE,
                eventParams
            )
        }
    }


    /* Ad Banner focus*/
    fun trackEventHomeBannerFocus(content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.banner_tiltle = content.title
            //eventParams.adverstiment_banner_rank = ""
            eventParams.banner_type = content.category
            eventParams.banner_target_type = content.category
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_ADVERSTIMENT_BANNER_FOCUS,
                SegmentEvents.NAV_ELEMENT_ADVERSTIMENT_BANNER,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    //**************************Search detail page event******************//

    fun trackEventSearchLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SEARCH,
                SegmentEvents.EVENT_SEARCH_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackEventSearchLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SEARCH,
                SegmentEvents.EVENT_SEARCH_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackEventSearchKeyBoardKeyFocus() {
        doAsync {
            val eventParams = EventParams()
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SEARCH,
                SegmentEvents.EVENT_SEARCH_KEYBOARD_KEY_FOCUS,
                SegmentEvents.NAV_ELEMENT_KEYBOARD,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackEventSearchKeyBoardKeyPress(query: String, queryLength: String, keyboardType: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.user_query = query
            eventParams.query_length = queryLength
            eventParams.keyboardType = keyboardType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SEARCH,
                SegmentEvents.EVENT_SEARCH_KEYBOARD_KEY_PRESS,
                SegmentEvents.NAV_ELEMENT_KEYBOARD,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackEventSearchResultLoadStart(query: String, queryLength: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.user_query = query
            eventParams.query_length = queryLength
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SEARCH,
                SegmentEvents.EVENT_SEARCH_RESULT_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParams
            )
        }
    }

    fun trackEventSearchResultLoadEnd(count: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.search_results = count
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SEARCH,
                SegmentEvents.EVENT_SEARCH_RESULT_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParams
            )
        }
    }

    fun trackEventSearchTileCaroselFocus(
        vertical: String, horizontal: String,
        tileProperties: org.json.simple.JSONObject, tileSection: String,
        tileType: String
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.scroll_depth_horizontal = horizontal
            eventParams.scroll_depth_vertical = vertical
            eventParams.tile_properties = tileProperties
            eventParams.tile_section = tileSection
            eventParams.tile_type = tileType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SEARCH,
                SegmentEvents.EVENT_SEARCH_TILE_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE_CAROUSEL,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParams
            )
        }
    }

    fun trackEventSearchTileCaroselClick(
        vertical: String, horizontal: String,
        tileProperties: org.json.simple.JSONObject, tileSection: String,
        tileType: String
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.scroll_depth_horizontal = horizontal
            eventParams.scroll_depth_vertical = vertical
            eventParams.tile_properties = tileProperties
            eventParams.tile_section = tileSection
            eventParams.tile_type = tileType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SEARCH,
                SegmentEvents.EVENT_SEARCH_TILE_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE_CAROUSEL,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackEventSearchQuickLinkFocus(vertical: String, qName: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.scroll_depth_vertical = vertical
            eventParams.quick_link_name = qName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SEARCH,
                SegmentEvents.EVENT_SEARCH_QUICK_LINK_FOCUS,
                SegmentEvents.NAV_ELEMENT_QUICK_LINK,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParams
            )
        }
    }

    fun trackEventSearchQuickLinkClick(vertical: String, qName: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.scroll_depth_vertical = vertical
            eventParams.quick_link_name = qName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SEARCH,
                SegmentEvents.EVENT_SEARCH_QUICK_LINK_CLICK,
                SegmentEvents.NAV_ELEMENT_QUICK_LINK,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackEventSearch(query: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.user_query = query
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SEARCH_WINDOW,
                SegmentEvents.EVENT_SEARCH_EXECUTED, eventParams
            )
        }
    }

    fun trackEventSearchBackBtnClick(query: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.user_query = query
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_SEARCH_WINDOW,
                SegmentEvents.EVENT_SEARCH_BACK_BUTTON_CLICK, eventParams
            )
        }
    }

    //******************************detail activity page event**************//

    fun trackEventScreenLoadStart(isLiked: Boolean, isBookmarked: Boolean, content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.subject = content.subject
            eventParams.concept = content.title
            eventParams.content_type = content.type
            eventParams.duration = Utils.setDuration(content)
            eventParams.rating = content.rating.toString()
            eventParams.embiums = Utils.setEmbium(content)
            eventParams.special_flag = ""
            eventParams.bookmark_state = isBookmarked
            eventParams.like_state = isLiked
            eventParams.content_id = content.content_id
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_MORE_INFO,
                SegmentEvents.EVENT_MORE_INFO_SCREEN_LOAD_START,
                SegmentEvents.EVENT_TYPE_LOAD, eventParams
            )
        }

    }

    fun trackEventScreenLoadEnd(isLiked: Boolean, isBookmarked: Boolean, content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.subject = content.subject
            eventParams.concept = content.title
            eventParams.content_type = content.type
            eventParams.duration = Utils.setDuration(content)
            eventParams.rating = content.rating.toString()
            eventParams.embiums = Utils.setEmbium(content)
            eventParams.special_flag = ""
            eventParams.bookmark_state = isBookmarked
            eventParams.like_state = isLiked
            eventParams.content_id = content.content_id
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_MORE_INFO,
                SegmentEvents.EVENT_MORE_INFO_SCREEN_LOAD_END,
                SegmentEvents.EVENT_TYPE_LOAD, eventParams
            )
        }

    }

    fun trackEventMoreInfoMainButtonFocus(content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.subject = content.subject
            eventParams.concept = content.title
            eventParams.content_type = content.type
            eventParams.duration = Utils.setDuration(content)
            eventParams.rating = content.rating.toString()
            eventParams.embiums = Utils.setEmbium(content)
            eventParams.special_flag = ""
            eventParams.target_content_type = "Learn"
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_MORE_INFO,
                SegmentEvents.EVENT_MORE_INFO_MAIN_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParams
            )
        }

    }

    fun trackEventMoreInfoMainButtonClick(content: Content, menuName: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.subject = content.subject
            eventParams.concept = content.title
            eventParams.content_type = content.type
            eventParams.duration = Utils.setDuration(content)
            eventParams.rating = content.rating.toString()
            eventParams.embiums = Utils.setEmbium(content)
            eventParams.special_flag = ""
            eventParams.target_content_type = "Learn"
            eventParams.menu_name = menuName
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_MORE_INFO,
                SegmentEvents.EVENT_MORE_INFO_MAIN_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }

    }

    fun trackEventMoreInfoMainMenuResultLoadStart(
        tiltle: String,
        menuName: String,
        resultCount: Int
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.more_info_page_title = tiltle
            eventParams.menu_name = menuName
            eventParams.no_of_result = resultCount
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_MORE_INFO,
                SegmentEvents.EVENT_MORE_INFO_MENU_RESULT_LOAD_START,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }

    }

    fun trackEventMoreInfoMainMenuResultLoadEnd(
        tiltle: String,
        menuName: String,
        resultCount: Int
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.more_info_page_title = tiltle
            eventParams.menu_name = menuName
            eventParams.no_of_result = resultCount
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_MORE_INFO,
                SegmentEvents.EVENT_MORE_INFO_MENU_RESULT_LOAD_END,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }

    }

    fun trackEventMoreInfoBookmarkFocus(isBookmarked: Boolean, content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.bookmark_state = isBookmarked
            eventParams.subject = content.subject
            eventParams.concept = content.title
            eventParams.content_type = content.type
            eventParams.duration = Utils.setDuration(content)
            eventParams.rating = content.rating.toString()
            eventParams.embiums = Utils.setEmbium(content)
            eventParams.special_flag = ""
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_MORE_INFO,
                SegmentEvents.EVENT_MORE_INFO_BOOKMARK_FOCUS,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParams
            )
        }
    }

    fun trackEventMoreInfoBookmarkClick(isBookmarked: Boolean, content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.bookmark_state = isBookmarked
            eventParams.subject = content.subject
            eventParams.concept = content.title
            eventParams.content_type = content.type
            eventParams.duration = Utils.setDuration(content)
            eventParams.rating = content.rating.toString()
            eventParams.embiums = Utils.setEmbium(content)
            eventParams.special_flag = ""
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_MORE_INFO,
                SegmentEvents.EVENT_MORE_INFO_BOOKMARK_CLICK,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackEventMoreInfoLikeFocus(isLiked: Boolean, content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.like_state = isLiked
            eventParams.subject = content.subject
            eventParams.concept = content.title
            eventParams.content_type = content.type
            eventParams.duration = Utils.setDuration(content)
            eventParams.rating = content.rating.toString()
            eventParams.embiums = Utils.setEmbium(content)
            eventParams.special_flag = ""
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_MORE_INFO,
                SegmentEvents.EVENT_MORE_INFO_LIKE_FOCUS,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParams
            )
        }
    }

    fun trackEventMoreInfoLikeClick(isLiked: Boolean, content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.like_state = isLiked
            eventParams.subject = content.subject
            eventParams.concept = content.title
            eventParams.content_type = content.type
            eventParams.duration = Utils.setDuration(content)
            eventParams.rating = content.rating.toString()
            eventParams.embiums = Utils.setEmbium(content)
            eventParams.special_flag = ""
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_MORE_INFO,
                SegmentEvents.EVENT_MORE_INFO_LIKE_CLICK,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }


    fun trackEventMoreInfoMenuItemTileFocus(content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.subject = content.subject
            eventParams.concept = content.title
            eventParams.content_type = content.type
            eventParams.duration = Utils.setDuration(content)
            eventParams.rating = content.rating.toString()
            eventParams.embiums = Utils.setEmbium(content)
            eventParams.special_flag = ""
            eventParams.menu_name = " "
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_MORE_INFO,
                SegmentEvents.EVENT_MORE_INFO_MENU_ITEM_TILE_FOCUS,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParams
            )
        }
    }

    fun trackEventMoreInfoMenuItemTileClick(content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.subject = content.subject
            eventParams.concept = content.title
            eventParams.content_type = content.type
            eventParams.duration = Utils.setDuration(content)
            eventParams.rating = content.rating.toString()
            eventParams.embiums = Utils.setEmbium(content)
            eventParams.special_flag = ""
            eventParams.menu_name = " "
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_MORE_INFO,
                SegmentEvents.EVENT_MORE_INFO_MENU_ITEM_TILE_CLICK,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackEventLike(isLiked: Boolean, content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.liked = isLiked
            eventParams.content_id = content.id
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            eventParams.category = content.type
            eventParams.current_duration = content.watched_duration
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_DETAIL_WINDOW,
                SegmentEvents.EVENT_CONTENT_LIKED, eventParams
            )
        }
    }

    //*****************video player detail controller events*****************//
    fun trackEventPlay(content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.content_id = content.id
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            eventParams.category = content.type
            eventParams.duration = ""
            eventParams.current_duration = content.watched_duration
            eventParams.seek_duration = 0
            eventParams.network_speed = ConnectionManager.instance.getNetworkSpeed().toString()
            eventParams.quality = ""
            eventParams.play_mode = ""
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PLAYER_WINDOW,
                SegmentEvents.EVENT_CONTENT_PLAY, eventParams
            )
        }
    }

    fun trackEventPause(content: Content) {
        doAsync {
            val eventParams = EventParams()
            eventParams.content_id = content.id
            eventParams.format_reference = content.learning_map.format_id
            eventParams.learnpath_name = content.learnpath_name
            eventParams.learnpath_format_name = content.learnpath_format_name
            eventParams.category = content.type
            eventParams.duration = ""
            eventParams.current_duration = content.watched_duration
            eventParams.seek_duration = 0
            eventParams.network_speed = ConnectionManager.instance.getNetworkSpeed().toString()
            eventParams.quality = ""
            eventParams.play_mode = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PLAYER_WINDOW,
                SegmentEvents.EVENT_CONTENT_PAUSE, eventParams
            )
        }
    }

    //*********************main activity events****************//
    /* home screen load start*/
    fun trackEventHomeStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    /* recommendation screen load start*/
    fun trackEventRecommendationStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_RECOMMENDATION,
                SegmentEvents.EVENT_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    /* home screen load end*/
    fun trackEventHomeEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_SCREEN_LOAD_END, null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    /* recommendation screen load end*/
    fun trackEventRecommendationEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_RECOMMENDATION,
                SegmentEvents.EVENT_SCREEN_LOAD_END, null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    /*replay video click on recommendation screen*/
    fun trackEventReplayVideoClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_RECOMMENDATION,
                SegmentEvents.EVENT_REPLAY_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_REPLAY_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    /*replay video focus on recommendation screen*/
    fun trackEventReplayVideoFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_RECOMMENDATION,
                SegmentEvents.EVENT_REPLAY_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_REPLAY_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    /*error replay video click on recommendation screen*/
    fun trackEventErrorReplayVideoClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_RECOMMENDATION,
                SegmentEvents.EVENT_REPLAY_ERROR_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_REPLAY_ERROR_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    /*error replay video focus on recommendation screen*/
    fun trackEventErrorReplayVideoFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_RECOMMENDATION,
                SegmentEvents.EVENT_REPLAY_ERROR_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_REPLAY_ERROR_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    /*play next video timer completed on recommendation screen*/
    fun trackEventPlayNextVideoTimerCompleted() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_RECOMMENDATION,
                SegmentEvents.EVENT_PLAY_NEXT_VIDEO_TIME_COMPLETES,
                "",
                SegmentEvents.EVENT_TYPE_AUTO_TIMER
            )
        }
    }

    /*play next video timer canceled on recommendation screen*/
    fun trackEventPlayNextVideoTimerCanceled() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_RECOMMENDATION,
                SegmentEvents.EVENT_PLAY_NEXT_VIDEO_TIME_CANCELED,
                "",
                SegmentEvents.EVENT_TYPE_AUTO_TIMER
            )
        }
    }

    /* Navigation bar item Click*/

    //********************nav menu frag events******************//
    /* Navigation bar View open*/
    fun trackEventHomeNavigationBarViewOpen(lastSelectedMenu: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.page = lastSelectedMenu
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_NAVIAGTION_BAR_VIEW_OPEN,
                SegmentEvents.NAV_ELEMENT_NAVIGATION_BAR,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    //********************nav menu frag events******************//
    /* Navigation bar View*/
    fun trackEventHomeNavigationBarView(lastSelectedMenu: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.page = lastSelectedMenu
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.NAV_ELEMENT_NAVIGATION_BAR,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    /* Navigation bar item Focus*/
    fun trackEventHomeNavigationBarFocus(itemName: String, lastSelectedMenu: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.item_name = itemName
            eventParams.page = lastSelectedMenu + "Page"
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_NAVIGATION_BAR_ITEM_FOCUS,
                SegmentEvents.NAV_ELEMENT_BODY,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }

    }

    /* Navigation bar item Click*/
    fun trackEventHomeNavigationBarClick(itemName: String) {

        doAsync {
            val eventParams = EventParams()
            eventParams.item_name = itemName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_HOME,
                SegmentEvents.EVENT_NAVIGATION_BAR_ITEM_CLICK,
                SegmentEvents.NAV_ELEMENT_BODY,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    //*****************video player detail controller events*****************//
    fun trackEventVideoLoadingStart(
        start_position: Long,
        bit_rate: String,
        videoUrl: String,
        videoTitle: String,
        videoType: String,
        videoId: String,
        totalDuration: String? = null,
        format_id: String? = null,
        learnPathName: String? = null,
        learnPathFormatName: String? = null

    ) {
        val eventParams = EventParams()
        eventParams.start_position = start_position
        eventParams.bit_rate = bit_rate
        eventParams.format_reference = format_id
        eventParams.learnpath_name = learnPathName
        eventParams.learnpath_format_name = learnPathFormatName
        val videoParams = VideoEventParams()
        videoParams.video_url = videoUrl
        videoParams.video_title = videoTitle
        videoParams.video_type = videoType
        videoParams.video_id = videoId
        if (totalDuration != null) videoParams.video_total_length = totalDuration
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_LOADING_START, eventParams, videoParams
            )
        }
    }

    fun trackEventVideoLoadingEnd(
        bit_rate: String,
        videoUrl: String,
        videoTitle: String,
        videoType: String,
        videoId: String,
        totalDuration: String? = null,
        format_id: String? = null,
        learnPathName: String? = null,
        learnPathFormatName: String? = null
    ) {
        val eventParams = EventParams()
        eventParams.bit_rate = bit_rate
        eventParams.format_reference = format_id
        eventParams.learnpath_name = learnPathName
        eventParams.learnpath_format_name = learnPathFormatName
        val videoParams = VideoEventParams()
        videoParams.video_url = videoUrl
        videoParams.video_title = videoTitle
        videoParams.video_type = videoType
        videoParams.video_id = videoId
        if (totalDuration != null) videoParams.video_total_length = totalDuration
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_LOADING_END, eventParams, videoParams
            )
        }
    }

    fun trackEventVideoPlaybackError(current_position: Long, player_error: String) {
        val eventParams = EventParams()
        eventParams.current_position = current_position
        eventParams.player_error = player_error
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_PLAYBACK_ERROR, eventParams
            )
        }
    }

    fun trackEventVideoPlayPauseClick(
        current_position: Long,
        video_player_mode: String,
        current_play_state: String,
        audio_bitrate: String,
        video_bitrate: String,
        videoUrl: String,
        videoTitle: String,
        videoType: String,
        videoId: String,
        totalDuration: String? = null,
        format_id: String? = null,
        learnPathName: String? = null,
        learnPathFormatName: String? = null
    ) {
        val eventParams = EventParams()
        eventParams.current_position = current_position
        eventParams.video_player_mode = video_player_mode
        eventParams.current_play_state = current_play_state
        eventParams.audio_bitrate = audio_bitrate
        eventParams.video_bitrate = video_bitrate
        eventParams.format_reference = format_id
        eventParams.learnpath_name = learnPathName
        eventParams.learnpath_format_name = learnPathFormatName
        val videoParams = VideoEventParams()
        videoParams.video_url = videoUrl
        videoParams.video_title = videoTitle
        videoParams.video_type = videoType
        videoParams.video_id = videoId
        if (totalDuration != null) videoParams.video_total_length = totalDuration
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_PLAY_PAUSE_CLICK, eventParams, videoParams
            )
        }
    }

    fun trackEventVideoBufferStart(
        current_position: Long,
        initial_resolution: String,
        change_resolution: String,
        current_network_state: String,
        videoUrl: String,
        videoTitle: String,
        videoType: String,
        videoId: String,
        totalDuration: String? = null,
        format_id: String? = null,
        learnPathName: String? = null,
        learnPathFormatName: String? = null
    ) {
        val eventParams = EventParams()
        eventParams.initial_resolution = initial_resolution
        eventParams.change_resolution = change_resolution
        eventParams.current_network_state = current_network_state
        eventParams.current_position = current_position
        eventParams.format_reference = format_id
        eventParams.learnpath_name = learnPathName
        eventParams.learnpath_format_name = learnPathFormatName
        val videoParams = VideoEventParams()
        videoParams.video_url = videoUrl
        videoParams.video_title = videoTitle
        videoParams.video_type = videoType
        videoParams.video_id = videoId
        if (totalDuration != null) videoParams.video_total_length = totalDuration
        doAsync {
            trackEventVideoPlayPauseClick(
                current_position,
                PLAYER_FULLSCREEN,
                PLAYER_PAUSE,
                "",
                "",
                videoUrl,
                videoTitle,
                videoType,
                videoId,
                totalDuration,
                format_id, learnPathName, learnPathFormatName
            )
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_BUFFER_START, eventParams, videoParams
            )

        }
    }

    fun trackEventVideoBufferEnd(
        current_position: Long,
        change_resolution: String,
        current_network_state: String,
        videoUrl: String,
        videoTitle: String,
        videoType: String,
        videoId: String,
        totalDuration: String? = null,
        format_id: String? = null,
        learnPathName: String? = null,
        learnPathFormatName: String? = null
    ) {
        val eventParams = EventParams()
        eventParams.change_resolution = change_resolution
        eventParams.current_network_state = current_network_state
        eventParams.current_position = current_position
        eventParams.format_reference = format_id
        eventParams.learnpath_name = learnPathName
        eventParams.learnpath_format_name = learnPathFormatName
        val videoParams = VideoEventParams()
        videoParams.video_url = videoUrl
        videoParams.video_title = videoTitle
        videoParams.video_type = videoType
        videoParams.video_id = videoId
        if (totalDuration != null) videoParams.video_total_length = totalDuration
        doAsync {
            trackEventVideoPlayPauseClick(
                current_position,
                PLAYER_FULLSCREEN,
                PLAYER_PLAY,
                "",
                "",
                videoUrl,
                videoTitle,
                videoType,
                videoId,
                totalDuration, format_id, learnPathName, learnPathFormatName
            )
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_BUFFER_END, eventParams, videoParams
            )

        }
    }

    fun trackEventVideoSeek(
        seek_position: Long,
        start: Long,
        end: Long,
        seek_mode: String,
        videoUrl: String,
        videoTitle: String,
        videoType: String,
        videoId: String,
        totalDuration: String? = null,
        format_id: String? = null,
        learnPathName: String? = null,
        learnPathFormatName: String? = null
    ) {
        val eventParams = EventParams()
        eventParams.seek_position = seek_position
        eventParams.start = start
        eventParams.end = end
        eventParams.seek_mode = seek_mode
        eventParams.format_reference = format_id
        eventParams.learnpath_name = learnPathName
        eventParams.learnpath_format_name = learnPathFormatName
        val videoParams = VideoEventParams()
        videoParams.video_url = videoUrl
        videoParams.video_title = videoTitle
        videoParams.video_type = videoType
        videoParams.video_id = videoId
        if (totalDuration != null) videoParams.video_total_length = totalDuration
        doAsync {
            trackEventVideoPlayPauseClick(
                start,
                PLAYER_FULLSCREEN,
                PLAYER_PAUSE,
                "",
                "",
                videoUrl,
                videoTitle,
                videoType,
                videoId,
                totalDuration
            )
            trackEventVideoPlayPauseClick(
                end,
                PLAYER_FULLSCREEN,
                PLAYER_PLAY,
                "",
                "",
                videoUrl,
                videoTitle,
                videoType,
                videoId,
                totalDuration
            )
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_SEEK, eventParams, videoParams
            )

        }
    }

    fun trackEventVideoSetting(content: Content) {
        val eventParams = EventParams()

        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_SETTING_CLICK, eventParams
            )
        }
    }

    fun trackEventVideoVolume(
        current_position: Long,
        current_volume: Int,
        current_play_state: String,
        volume_change: String,
        videoUrl: String,
        videoTitle: String,
        videoType: String,
        videoId: String,
        totalDuration: String? = null,
        format_id: String? = null,
        learnPathName: String? = null,
        learnPathFormatName: String? = null
    ) {
        val eventParams = EventParams()
        eventParams.current_play_state = current_play_state
        eventParams.current_position = current_position
        eventParams.volume_change = volume_change
        eventParams.current_volume = current_volume
        eventParams.format_reference = format_id
        eventParams.learnpath_name = learnPathName
        eventParams.learnpath_format_name = learnPathFormatName
        val videoParams = VideoEventParams()
        videoParams.video_url = videoUrl
        videoParams.video_title = videoTitle
        videoParams.video_type = videoType
        videoParams.video_id = videoId
        if (totalDuration != null) videoParams.video_total_length = totalDuration

        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_VOLUME_CLICK, eventParams, videoParams
            )
        }
    }

    fun trackEventVideoBookmark(
        current_position: Long,
        current_play_state: String,
        bookmark_state: Boolean,
        videoUrl: String,
        videoTitle: String,
        videoType: String,
        videoId: String,
        totalDuration: String? = null,
        format_id: String? = null,
        learnPathName: String? = null,
        learnPathFormatName: String? = null
    ) {
        val eventParams = EventParams()
        eventParams.current_play_state = current_play_state
        eventParams.current_position = current_position
        eventParams.bookmark_state = bookmark_state
        eventParams.format_reference = format_id
        eventParams.learnpath_name = learnPathName
        eventParams.learnpath_format_name = learnPathFormatName
        val videoParams = VideoEventParams()
        videoParams.video_url = videoUrl
        videoParams.video_title = videoTitle
        videoParams.video_type = videoType
        videoParams.video_id = videoId
        if (totalDuration != null) videoParams.video_total_length = totalDuration
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_BOOKMARK_CLICK, eventParams, videoParams
            )
        }
    }

    fun trackEventVideoBookmarkFocus(
        current_position: Long,
        current_play_state: String,
        bookmark_state: Boolean,
        videoUrl: String,
        videoTitle: String,
        videoType: String,
        videoId: String,
        totalDuration: String? = null
    ) {
        val eventParams = EventParams()
        eventParams.current_play_state = current_play_state
        eventParams.current_position = current_position
        eventParams.bookmark_state = bookmark_state
        val videoParams = VideoEventParams()
        videoParams.video_url = videoUrl
        videoParams.video_title = videoTitle
        videoParams.video_type = videoType
        videoParams.video_id = videoId
        if (totalDuration != null) videoParams.video_total_length = totalDuration
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_BOOKMARK_FOCUS, eventParams, videoParams
            )
        }
    }

    fun trackEventVideoLike(
        current_position: Long,
        current_play_state: String,
        like_state: Boolean,
        videoUrl: String,
        videoTitle: String,
        videoType: String,
        videoId: String,
        totalDuration: String? = null
    ) {
        val eventParams = EventParams()
        eventParams.current_play_state = current_play_state
        eventParams.current_position = current_position
        eventParams.like_state = like_state
        val videoParams = VideoEventParams()
        videoParams.video_url = videoUrl
        videoParams.video_title = videoTitle
        videoParams.video_type = videoType
        videoParams.video_id = videoId
        if (totalDuration != null) videoParams.video_total_length = totalDuration
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_LIKE_CLICK, eventParams, videoParams
            )
        }
    }

    fun trackEventVideoLikeFocus(
        current_position: Long,
        current_play_state: String,
        like_state: Boolean,
        videoUrl: String,
        videoTitle: String,
        videoType: String,
        videoId: String,
        totalDuration: String? = null
    ) {
        val eventParams = EventParams()
        eventParams.current_play_state = current_play_state
        eventParams.current_position = current_position
        eventParams.like_state = like_state
        val videoParams = VideoEventParams()
        videoParams.video_url = videoUrl
        videoParams.video_title = videoTitle
        videoParams.video_type = videoType
        videoParams.video_id = videoId
        if (totalDuration != null) videoParams.video_total_length = totalDuration
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_LIKE_FOCUS, eventParams, videoParams
            )
        }
    }

    fun trackEventVideoExit(
        current_position: Long,
        current_play_state: String,
        current_network_state: String
    ) {
        val eventParams = EventParams()
        eventParams.current_play_state = current_play_state
        eventParams.current_position = current_position
        eventParams.current_network_state = current_network_state
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_VIDEO_PLAYER,
                SegmentEvents.EVENT_CONTENT_EXIT, eventParams
            )
        }
    }

    fun extractYoutubeVideoId(ytUrl: String): String {
        val pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*"
        val compiledPattern = Pattern.compile(pattern)
        val matcher = compiledPattern.matcher(ytUrl)
        return if (matcher.find()) {
            matcher.group()
        } else {
            "error"
        }
    }

    //**************More Info events************************//

    fun trackEventMoreInfoStart(
        subject: String,
        concept: String,
        content_type: String,
        duration: String,
        rating: String,
        embiums: Int,
        special_flag: String,
        user_bookmark_state: Boolean,
        user_like_state: Boolean
    ) {
    }

    fun trackEventMoreInfoEnd() {}

    fun getExamLevelCode(parts: List<String>): String {
        for (part in parts) {
            if (getLevelUsingCode(part) == "exam")
                return part
        }
        return ""
    }

    fun getLevelUsingCode(lmCode: String): String {
        return when (lmCode.substring(0, 2)) {
            "ch" -> "chapter"
            "tp" -> "topic"
            "un" -> "unit"
            "ex" -> "exam"
            else -> "chapter"
        }
    }

    /* grade- board Screen*/
    fun trackGradeBoardScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_GRADE_BOARD_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD_START
            )
        }
    }

    fun trackGradeBoardScreenLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_GRADE_BOARD_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD_END
            )
        }
    }

    fun trackGradeBoardScreenBoardClick(board: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.board = board
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_GRADE_BOARD_SCREEN_BOARD_CLICK,
                SegmentEvents.NAV_ELEMENT_OPTIONS,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackGradeBoardScreenGradeClick(grade: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.grade = grade
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_GRADE_BOARD_SCREEN_GRADE_CLICK,
                SegmentEvents.NAV_ELEMENT_OPTIONS,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackGradeBoardScreenChooseAvatar() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_GRADE_BOARD_SCREEN_CHOOSE_AVATAR_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackGradeBoardScreenAddExams() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_GRADE_BOARD_SCREEN_ADD_EXAMS_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackGradeBoardScreenSaveClick(
        board: String,
        grade: String,
        goal: String,
        avatarName: String
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.board = board
            eventParams.grade = grade
            eventParams.avatar_name = avatarName
            eventParams.goal = goal
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_GRADE_BOARD_SCREEN_SAVE_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackGradeBoardScreenCancelClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_GRADE_BOARD_SCREEN_CANCEL_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackGradeBoardScreenCancelFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_GRADE_BOARD_SCREEN_CANCEL_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }
    /*add - goals screen*/

    fun trackAddGoalsScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_ADD_EXAMS,
                SegmentEvents.EVENT_ADD_GOALS_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackAddGoalsScreenLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_ADD_EXAMS,
                SegmentEvents.EVENT_ADD_GOALS_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackAddGoalsScreenOptionsClick(goal: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.goal = goal
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_ADD_EXAMS,
                SegmentEvents.EVENT_ADD_GOALS_SCREEN_OPTIONS_CLICK,
                SegmentEvents.NAV_ELEMENT_OPTIONS,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackAddGoalsScreenOptionsFocus(goal: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.goal = goal
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CHOOSE_GOAL,
                SegmentEvents.EVENT_ADD_GOALS_SCREEN_OPTIONS_FOCUS,
                SegmentEvents.NAV_ELEMENT_OPTIONS,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParams
            )
        }
    }

    fun trackAddGoalsScreenDoneClick(board: String, className: String, competiveExams: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.board = board
            eventParams.className = className
            eventParams.competive_exams = competiveExams
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CHOOSE_GOAL,
                SegmentEvents.EVENT_ADD_GOALS_SCREEN_DONE_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    /*parent-profile screen*/

    fun trackParentProfileScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PARENT_PROFILE_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD_START
            )
        }
    }

    fun trackParentProfileScreenLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PARENT_PROFILE_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD_END
            )
        }
    }

    fun trackParentProfileScreenIconFocus(position: String, embiums: String, status: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.content_position = position
            eventParams.embiums = embiums
            eventParams.status = status
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PARENT_PROFILE_SCREEN_ICON_FOCUS,
                SegmentEvents.NAV_ELEMENT_ICON,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParams
            )
        }
    }

    fun trackParentProfileScreenAddIconClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PARENT_PROFILE_SCREEN_ADD_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackParentProfileScreenLogoutClick(status: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.status = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PARENT_PROFILE_SCREEN_LOGOUT_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackParentProfileScreenLogoutFocus() {
        doAsync {
            val eventParams = EventParams()
            eventParams.status = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PARENT_PROFILE_SCREEN_LOGOUT_FOCUS,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParams
            )
        }
    }

    fun trackParentProfileScreenEditClick() {
        doAsync {

            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PARENT_PROFILE_SCREEN_EDIT_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }


    /*add-user screen*/

    fun trackAddUserScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_USER,
                SegmentEvents.EVENT_ADD_USER_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD_START
            )
        }
    }

    fun trackAddUserScreenLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_USER,
                SegmentEvents.EVENT_ADD_USER_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD_END
            )
        }
    }

    fun trackAddUserScreenBoxClick(value: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.item_name = value
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_USER,
                SegmentEvents.EVENT_ADD_USER_SCREEN_ENTRY_BOX_CLICK,
                SegmentEvents.NAV_ELEMENT_BODY,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackAddUserScreenBoxFocus(value: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.item_name = value
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_USER,
                SegmentEvents.EVENT_ADD_USER_SCREEN_ENTRY_BOX_FOCUS,
                SegmentEvents.NAV_ELEMENT_BODY,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParams
            )
        }
    }

    fun trackAddUserScreenAddClick(
        username: String, user_type: String, email: String, mobile: String, isPaid: Boolean
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.username = username
            eventParams.user_type = user_type
            eventParams.email_id = email
            eventParams.mobile = mobile
            eventParams.isPaid = isPaid
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_USER,
                SegmentEvents.EVENT_ADD_USER_SCREEN_ADD_CLICK,
                SegmentEvents.NAV_ELEMENT_BODY,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParams
            )
        }
    }

    //**add user
    fun trackAddUserProfileNameClick(value: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.profile_name = value
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_NAME_CLICK,
                SegmentEvents.NAV_ELEMENT_BOX,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackAddUserEmailClick(value: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.email = value
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_EMAIL_CLICK,
                SegmentEvents.NAV_ELEMENT_BOX,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackAddUserRoleClick(value: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.role = value
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_ROLE_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackAddUserRoleFocus(value: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.role = value
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_ROLE_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParams
            )
        }
    }

    fun trackAddUserContinueClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_CONTINUE_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackAddUserContinueFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_CONTINUE_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackAddUserCancelClick() {
        doAsync {

            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_CANCEL_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackAddUserCancelFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_CANCEL_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    /*primary secondary goal profile page*/

    fun trackProfilePrimarySecLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_PRIMARY_SEC_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackProfilePrimarySecLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_PRIMARY_SEC_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackProfilePrimaryGoalValueFocus(name: String, adapterPosition: Int) {
        doAsync {
            val eventParams = EventParams()
            eventParams.primary_goal = name
            eventParams.primary_goal_position = adapterPosition.toString()
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_PRIMARY_GOAL_VALUE_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackProfilePrimaryGoalValueClick(name: String, adapterPosition: Int) {
        doAsync {
            val eventParams = EventParams()
            eventParams.primary_goal = name
            eventParams.primary_goal_position = adapterPosition.toString()
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_PRIMARY_GOAL_VALUE_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackProfileExamPrimaryGoalValueFocus(name: String, adapterPosition: Int) {
        doAsync {
            val eventParams = EventParams()
            eventParams.exam = name
            eventParams.exam_position = adapterPosition.toString()
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_EXAM_PRIMARY_GOAL_VALUE_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackProfileExamPrimaryGoalValueCLick(name: String, adapterPosition: Int, error: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.exam = name
            eventParams.exam_position = adapterPosition.toString()
            eventParams.error = error
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_EXAM_PRIMARY_GOAL_VALUE_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackProfileExamPrimaryGoalDoneFocus(
        primaryGoalExamCode: String,
        primaryGoal: String
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.exam = primaryGoalExamCode
            eventParams.primary_goal = primaryGoal
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_EXAM_PRIMARY_GOAL_DONE_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackProfileExamPrimaryGoalDoneCLick(
        primaryGoalExamCode: String,
        primaryGoal: String
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.exam = primaryGoalExamCode
            eventParams.primary_goal = primaryGoal
            eventParams.error = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_EXAM_PRIMARY_GOAL_DONE_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackProfileSecondaryGoalValueFocus(
        name: String,
        goal: Int
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.secondary_goal = name
            eventParams.secondary_goal_position = goal.toString()
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_SECONDARY_GOAL_VALUE_FOCUS,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackProfileSecondaryGoalValueClick(
        name: String,
        goal: Int
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.secondary_goal = name
            eventParams.secondary_goal_position = goal.toString()
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_SECONDARY_GOAL_VALUE_CLICK,
                SegmentEvents.NAV_ELEMENT_TILE,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackProfileSecondaryGoalContinueFocus(secondaryGoal: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.secondary_goal = secondaryGoal
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_SECONDARY_GOAL_CONTINUE_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackProfileSecondaryGoalContinueClick(secondaryGoal: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.secondary_goal = secondaryGoal
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_SECONDARY_GOAL_CONTINUE_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackProfileSecondaryGoalSkipFocus(secondaryGoal: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.secondary_goal = secondaryGoal
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_SECONDARY_GOAL_SKIP_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackProfileSecondaryGoalSkipClick(secondaryGoal: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.secondary_goal = secondaryGoal
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_SECONDARY_GOAL_SKIP_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackProfileSecondaryGoalBackFocus(secondaryGoal: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.secondary_goal = secondaryGoal
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_SECONDARY_GOAL_BACK_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackProfileSecondaryGoalBackClick(secondaryGoal: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.secondary_goal = secondaryGoal
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_SECONDARY_GOAL_BACK_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackProfileExamsSecondaryGoalValueFocus(name: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.exam = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_EXAMS_SECONDARY_GOAL_VALUE_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS,
                eventParams
            )
        }
    }

    fun trackProfileExamsSecondaryGoalValueClick(name: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.exam = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_EXAMS_SECONDARY_GOAL_VALUE_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackProfileExamsSecondaryGoalDoneFocus(secondaryGoalExams: ArraySet<String>) {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_EXAMS_SECONDARY_GOAL_DONE_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackProfileExamsSecondaryGoalDoneClick(
        secondaryGoalExams: ArraySet<String>,
        secondaryGoal: String
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.exams = secondaryGoalExams
            eventParams.secondary_goal = secondaryGoal
            eventParams.error = ""
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_EXAMS_SECONDARY_GOAL_DONE_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParams
            )
        }
    }

    fun trackProfileExamsSecondaryGoalBackFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_EXAMS_SECONDARY_GOAL_BACK_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackProfileExamsSecondaryGoalBackClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PROFILE_PAGE,
                SegmentEvents.EVENT_PROFILE_EXAMS_SECONDARY_GOAL_BACK_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    //Error---> Crash Event
    fun trackEventCrash(error_msg: String, cause: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.cause = cause
            eventParams.error_message = error_msg
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_CRASH,
                SegmentEvents.EVENT_ERROR_CRASH,
                null,
                null, eventParams
            )
        }
    }

    fun trackErrorLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_ERROR,
                SegmentEvents.EVENT_ERROR_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackErrorLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_ERROR,
                SegmentEvents.EVENT_ERROR_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackErrorCTAClick(ctaName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.cta_name = ctaName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_ERROR,
                SegmentEvents.EVENT_ERROR_CTA_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK,
                eventParam
            )
        }
    }


    //API
    fun trackApiFailure(apiUrl: String, bodyParams: String?, statusCode: Int) {
        doAsync {
            val eventParams = EventParams()
            eventParams.apiURL = apiUrl
            eventParams.statusCode = statusCode
            if (bodyParams != null)
                eventParams.bodyParams = bodyParams
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_ERROR, SegmentEvents.LOG_TYPE_API_FAILURE, null,
                null, eventParams
            )
        }
    }

    //Test Feedback
    fun trackTestFeedbackScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackTestFeedbackScreenLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackTestFeedbackScreenClickToWatchFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_CLICK_TO_WATCH_FOCUS,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestFeedbackScreenClickToWatchClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_CLICK_TO_WATCH_CLCIK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestFeedbackScreenSubjectWiseAnalyseDropdownFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_SUBJECT_WISE_ANALYSIS_DROPDOWN_FOCUS,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestFeedbackScreenSubjectWiseAnalyseDropdownClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_SUBJECT_WISE_ANALYSIS_DROPDOWN_CLCIK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestFeedbackScreenSubjectWiseAnalyseDropdownValueFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_SUBJECT_WISE_ANALYSIS_DROPDOWN_VALUE_FOCUS,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestFeedbackScreenSubjectWiseAnalyseDropdownValueClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_SUBJECT_WISE_ANALYSIS_DROPDOWN_VALUE_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestFeedbackScreenAttemptFocus(attemptType: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.attemptType = attemptType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_ATTEMPT_FOCUS,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackTestFeedbackScreenAttemptClick(attemptType: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.attemptType = attemptType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_ATTEMPT_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackTestFeedbackScreenAchieveHighScoreWidgetFocus(
        title: String,
        adapterPosition: Int,
        isAdded: Boolean
    ) {
        val eventParam = EventParams()
        eventParam.widget_name = title
        eventParam.widget_position = adapterPosition.toString()
        if (isAdded) {
            eventParam.target_symbol = "Add"
        } else {
            eventParam.target_symbol = "Minus"
        }

        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_ACHIEVE_HIGH_SCORE_WIDGET_FOCUS,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackTestFeedbackScreenCurrentAndPotentialWidgetClick(
        title: String,
        adapterPosition: Int,
        isAdded: Boolean
    ) {
        val eventParam = EventParams()
        eventParam.widget_name = title
        eventParam.widget_position = adapterPosition.toString()
        if (isAdded) {
            eventParam.target_symbol = "Add"
        } else {
            eventParam.target_symbol = "Minus"
        }
        doAsync {

            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_CURRENT_POTENTIAL_WIDGET_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackTestFeedbackScreenRevisionListFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_REVISION_LIST_VALUE,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestFeedbackScreenRevisionListClick() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_REVISION_LIST_VALUE_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackTestFeedbackScreenChapterWiseAnalysisLoadStart() {
        doAsync {
            val eventParam = EventParams()
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_CHAPTER_WISE_ANALYSIS_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParam
            )
        }
    }

    fun trackTestFeedbackScreenChapterWiseAnalysisLoadEnd() {
        doAsync {
            val eventParam = EventParams()
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_CHAPTER_WISE_ANALYSIS_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParam
            )
        }
    }

    fun trackTestFeedbackScreenChapterWiseAnalysisFocus(screenName: String, position: Int) {
        doAsync {
            val eventParam = EventParams()
            eventParam.content_name = screenName
            eventParam.tile_position = position.toString()
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_CHAPTER_WISE_ANALYSIS_TILE_FOCUS,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackTestFeedbackScreenChapterWiseAnalysisClick(screenName: String, position: Int) {
        doAsync {
            val eventParam = EventParams()
            eventParam.content_name = screenName
            eventParam.tile_position = position.toString()
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_CHAPTER_WISE_ANALYSIS_TILE_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackTestFeedbackScreenImproveTestTakingStrategyValueFocus(
        screenName: String,
        position: Int
    ) {
        doAsync {
            val eventParam = EventParams()
            eventParam.improve_strategy_list_name = screenName
            eventParam.improve_strategy_list_position = position.toString()
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_IMPROVE_TEST_TAKING_STRATEGY_VALUE_FOCUS,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackTestFeedbackScreenImproveTestTakingStrategyValueClick(
        screenName: String,
        position: Int
    ) {
        doAsync {
            val eventParam = EventParams()
            eventParam.improve_strategy_list_name = screenName
            eventParam.improve_strategy_list_position = position.toString()
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_IMPROVE_TEST_TAKING_STRATEGY_VALUE_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackTestFeedbackScreenImproveTestTakingStrategyVideoFocus(
        screenName: String,
        position: String?,
        nothing: String?
    ) {
        doAsync {
            val eventParam = EventParams()
            eventParam.improve_strategy_section_name = screenName
            eventParam.video_name = position.toString()
            eventParam.video_id = null
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_IMPROVE_TEST_TAKING_STRATEGY_VALUE_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackTestFeedbackScreenImproveTestTakingStrategyVideoClick(
        screenName: String,
        position: String?,
        nothing: String?
    ) {
        doAsync {
            val eventParam = EventParams()
            eventParam.improve_strategy_section_name = screenName
            eventParam.video_name = position.toString()
            eventParam.video_id = null
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_FEEDBACK,
                SegmentEvents.EVENT_TEST_FEEDBACK_SCREEN_IMPROVE_TEST_TAKING_STRATEGY_VALUE_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }


    //Learn Summary Screen
    fun trackLearnSummaryScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackLearnSummaryScreenLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackLearnSummaryScreenMainButtonFocus(btnName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.button_name = btnName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_MAIN_BUTTON_FOCUS,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenMainButtonClick(btnName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.button_name = btnName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_MAIN_BUTTON_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenBookmarkButtonFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_BOOKMARK_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackLearnSummaryScreenBookmarkButtonClick(isBookmarked: Boolean) {
        doAsync {
            val eventParam = EventParams()
            eventParam.bookmark_state = isBookmarked
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_BOOKMARK_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenLikekButtonFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_LIKE_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackLearnSummaryScreenLikeButtonClick(isLiked: Boolean) {
        doAsync {
            val eventParam = EventParams()
            eventParam.like_state = isLiked
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_LIKE_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenAvailableOptionFocus(
        optionName: String,
        optionPosition: Int,
        optionType: String
    ) {
        doAsync {
            val eventParam = EventParams()
            eventParam.option_name = optionName
            eventParam.option_type = optionType
            eventParam.option_position = optionPosition
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_AVAILABLE_OPTION_FOCUS,
                SegmentEvents.NAV_ELEMENT_OPTIONS,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenAvailableOptionClick(
        optionName: String,
        optionPosition: Int,
        optionType: String
    ) {
        doAsync {
            val eventParam = EventParams()
            eventParam.option_name = optionName
            eventParam.option_type = optionType
            eventParam.option_position = optionPosition
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_AVAILABLE_OPTION_CLICK,
                SegmentEvents.NAV_ELEMENT_OPTIONS,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenAvailableOptionContentLoadStart(optionName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.option_name = optionName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenAvailableOptionContentLoadEnd(optionName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.option_name = optionName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenKnowledgeGraphFocus(widgetName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.widget_name = widgetName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_KNOWLEDGE_GRAPH_FOCUS,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenKnowledgeGraphClick(widgetName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.widget_name = widgetName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_KNOWLEDGE_GRAPH_CLICK,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackEventKnowledgeGraphExpandedBackBtnFocus() {
        doAsync {

            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_CHECK_PROGRESS_BACK_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackEventKnowledgeGraphExpandedBackBtnClick() {
        doAsync {

            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_CHECK_PROGRESS_BACK_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackLearnSummaryScreenSincerityScoreFocus(sincerityScoreType: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.sincerity_score_type = sincerityScoreType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_FOCUS,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenSincerityScoreClick(sincerityScoreType: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.sincerity_score_type = sincerityScoreType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_CLICK,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenAttemptQualityFocus(bucketType: String, bucketPosition: Int) {
        doAsync {
            val eventParam = EventParams()
            eventParam.bucket_type = bucketType
            eventParam.bucket_position = bucketPosition
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_ATTEMPT_BUCKET_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUCKET,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenAttemptQualityClick(bucketType: String, bucketPosition: Int) {
        doAsync {
            val eventParam = EventParams()
            eventParam.bucket_type = bucketType
            eventParam.bucket_position = bucketPosition
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_ATTEMPT_BUCKET_CLICK,
                SegmentEvents.NAV_ELEMENT_BUCKET,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenAttemptBucketDescriptionFocus(attemptQuality: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.attempt_quality = attemptQuality
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_FOCUS,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenAttemptBucketDescriptionClick(attemptQuality: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.attempt_quality = attemptQuality
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_CLICK,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackLearnSummaryScreenKeyboardFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_KEYBOARD_KEYFOCUS_FOCUS,
                SegmentEvents.NAV_ELEMENT_KEYBOARD,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackLearnSummaryScreenKeyboardClick(
        query: String,
        queryLength: String,
        keyboardType: String
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.user_query = query
            eventParams.query_length = queryLength
            eventParams.keyboardType = keyboardType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_KEYBOARD_KEYPRESS_CLICK,
                SegmentEvents.NAV_ELEMENT_KEYBOARD,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackLearnSummaryScreenVoiceSearchClick(query: String, queryLength: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.user_query = query
            eventParams.query_length = queryLength
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_VOICE_SEARCH_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackLearnSummaryScreenSearchLoadStart(source: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.source = source
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParams
            )
        }
    }

    fun trackLearnSummaryScreenSearchLoadEnd(source: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.source = source
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_LEARN_SUMMARY,
                SegmentEvents.EVENT_LEARN_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParams
            )
        }
    }


    //***********Practice Summary*************

    fun trackPracticeSummaryScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackPracticeSummaryScreenLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackPracticeSummaryScreenMainButtonFocus(btnName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.button_name = btnName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_MAIN_BUTTON_FOCUS,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenMainButtonClick(btnName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.button_name = btnName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_MAIN_BUTTON_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenBookmarkButtonFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_BOOKMARK_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackPracticeSummaryScreenBookmarkButtonClick(isBookmarked: Boolean) {
        doAsync {
            val eventParam = EventParams()
            eventParam.bookmark_state = isBookmarked
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_BOOKMARK_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenLikekButtonFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_LIKE_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackPracticeSummaryScreenLikeButtonClick(isLiked: Boolean) {
        doAsync {
            val eventParam = EventParams()
            eventParam.like_state = isLiked
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_LIKE_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenAvailableOptionFocus(
        optionName: String,
        optionPosition: Int,
        optionType: String
    ) {
        doAsync {
            val eventParam = EventParams()
            eventParam.option_name = optionName
            eventParam.option_type = optionType
            eventParam.option_position = optionPosition
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_AVAILABLE_OPTION_FOCUS,
                SegmentEvents.NAV_ELEMENT_OPTIONS,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenAvailableOptionClick(
        optionName: String,
        optionPosition: Int,
        optionType: String
    ) {
        doAsync {
            val eventParam = EventParams()
            eventParam.option_name = optionName
            eventParam.option_type = optionType
            eventParam.option_position = optionPosition
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_AVAILABLE_OPTION_CLICK,
                SegmentEvents.NAV_ELEMENT_OPTIONS,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenAvailableOptionContentLoadStart(optionName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.option_name = optionName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenAvailableOptionContentLoadEnd(optionName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.option_name = optionName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenKnowledgeGraphFocus(widgetName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.widget_name = widgetName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_KNOWLEDGE_GRAPH_FOCUS,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenKnowledgeGraphClick(widgetName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.widget_name = widgetName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_KNOWLEDGE_GRAPH_CLICK,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackPracticeSummaryCheckProgreeBackBtnFocus() {
        doAsync {

            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_CHECK_PROGRESS_BACK_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackPracticeSummaryCheckProgressBackBtnClick() {
        doAsync {

            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_CHECK_PROGRESS_BACK_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_BUTTON,
                SegmentEvents.EVENT_TYPE_CLICK
            )
        }
    }

    fun trackPracticeSummaryScreenSincerityScoreFocus(sincerityScoreType: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.sincerity_score_type = sincerityScoreType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_FOCUS,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenSincerityScoreClick(sincerityScoreType: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.sincerity_score_type = sincerityScoreType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_CLICK,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenAttemptBucketFocus(bucketType: String, bucketPosition: Int) {
        doAsync {
            val eventParam = EventParams()
            eventParam.bucket_type = bucketType
            eventParam.bucket_position = bucketPosition
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_ATTEMPT_BUCKET_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUCKET,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenAttemptBucketClick(bucketType: String, bucketPosition: Int) {
        doAsync {
            val eventParam = EventParams()
            eventParam.bucket_type = bucketType
            eventParam.bucket_position = bucketPosition
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_ATTEMPT_BUCKET_CLICK,
                SegmentEvents.NAV_ELEMENT_BUCKET,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenAttemptBucketDescriptionFocus(attemptQuality: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.attempt_quality = attemptQuality
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_FOCUS,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenAttemptBucketDescriptionClick(attemptQuality: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.attempt_quality = attemptQuality
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_CLICK,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackPracticeSummaryScreenKeyboardFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_KEYBOARD_KEYFOCUS_FOCUS,
                SegmentEvents.NAV_ELEMENT_KEYBOARD,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackPracticeSummaryScreenKeyboardClick(
        query: String,
        queryLength: String,
        keyboardType: String
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.user_query = query
            eventParams.query_length = queryLength
            eventParams.keyboardType = keyboardType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_KEYBOARD_KEYPRESS_CLICK,
                SegmentEvents.NAV_ELEMENT_KEYBOARD,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackPracticeSummaryScreenVoiceSearchClick(query: String, queryLength: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.user_query = query
            eventParams.query_length = queryLength
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_VOICE_SEARCH_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackPracticeSummaryScreenSearchLoadStart(source: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.source = source
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParams
            )
        }
    }

    fun trackPracticeSummaryScreenSearchLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_PRACTICE_SUMMARY,
                SegmentEvents.EVENT_PRACTICE_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }


    //*****Test Summary*****
    fun trackTestSummaryScreenLoadStart() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackTestSummaryScreenLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }

    fun trackTestSummaryScreenMainButtonFocus(btnName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.button_name = btnName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_MAIN_BUTTON_FOCUS,
                null,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackTestSummaryScreenMainButtonClick(btnName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.button_name = btnName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_MAIN_BUTTON_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackTestSummaryScreenBookmarkButtonFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_BOOKMARK_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestSummaryScreenBookmarkButtonClick(isBookmarked: Boolean) {
        doAsync {
            val eventParam = EventParams()
            eventParam.bookmark_state = isBookmarked
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_BOOKMARK_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackTestSummaryScreenLikeButtonFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_LIKE_BUTTON_FOCUS,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestSummaryScreenLikeButtonClick(isLiked: Boolean) {
        doAsync {
            val eventParam = EventParams()
            eventParam.like_state = isLiked
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_LIKE_BUTTON_CLICK,
                SegmentEvents.NAV_ELEMENT_BANNER,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackTestSummaryScreenAvailableOptionFocus(
        optionName: String,
        optionPosition: Int,
        optionType: String
    ) {
        doAsync {
            val eventParam = EventParams()
            eventParam.option_name = optionName
            eventParam.option_type = optionType
            eventParam.option_position = optionPosition
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_AVAILABLE_OPTION_FOCUS,
                SegmentEvents.NAV_ELEMENT_OPTIONS,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackTestSummaryScreenAvailableOptionClick(
        optionName: String,
        optionPosition: Int,
        optionType: String
    ) {
        doAsync {
            val eventParam = EventParams()
            eventParam.option_name = optionName
            eventParam.option_type = optionType
            eventParam.option_position = optionPosition
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_AVAILABLE_OPTION_CLICK,
                SegmentEvents.NAV_ELEMENT_OPTIONS,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackTestSummaryScreenAvailableOptionContentLoadStart(optionName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.option_name = optionName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParam
            )
        }
    }

    fun trackTestSummaryScreenAvailableOptionContentLoadEnd(optionName: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.option_name = optionName
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_AVAILABLE_OPTION_CONTENT_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParam
            )
        }
    }

    fun trackTestSummaryScreenSincerityScoreFocus(sincerityScoreType: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.sincerity_score_type = sincerityScoreType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_FOCUS,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackTestSummaryScreenSincerityScoreClick(sincerityScoreType: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.sincerity_score_type = sincerityScoreType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_SINCERITY_SCORE_WIDGET_CLICK,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackTestSummaryScreenAttemptBucketFocus(bucketType: String, bucketPosition: Int) {
        doAsync {
            val eventParam = EventParams()
            eventParam.bucket_type = bucketType
            eventParam.bucket_position = bucketPosition
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_ATTEMPT_BUCKET_FOCUS,
                SegmentEvents.NAV_ELEMENT_BUCKET,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackTestSummaryScreenAttemptBucketClick(bucketType: String, bucketPosition: Int) {
        doAsync {
            val eventParam = EventParams()
            eventParam.bucket_type = bucketType
            eventParam.bucket_position = bucketPosition
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_ATTEMPT_BUCKET_CLICK,
                SegmentEvents.NAV_ELEMENT_BUCKET,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackTestSummaryScreenAttemptBucketDescriptionFocus(attemptQuality: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.attempt_quality = attemptQuality
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_FOCUS,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_FOCUS, eventParam
            )
        }
    }

    fun trackTestSummaryScreenAttemptBucketDescriptionClick(attemptQuality: String) {
        doAsync {
            val eventParam = EventParams()
            eventParam.attempt_quality = attemptQuality
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_ATTEMPT_BUCKET_DESCRIPTION_CLICK,
                SegmentEvents.NAV_ELEMENT_WIDGET,
                SegmentEvents.EVENT_TYPE_CLICK, eventParam
            )
        }
    }

    fun trackTestSummaryScreenKeyboardFocus() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_KEYBOARD_KEYFOCUS_FOCUS,
                SegmentEvents.NAV_ELEMENT_KEYBOARD,
                SegmentEvents.EVENT_TYPE_FOCUS
            )
        }
    }

    fun trackTestSummaryScreenKeyboardClick(
        query: String,
        queryLength: String,
        keyboardType: String
    ) {
        doAsync {
            val eventParams = EventParams()
            eventParams.user_query = query
            eventParams.query_length = queryLength
            eventParams.keyboardType = keyboardType
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_KEYBOARD_KEYPRESS_CLICK,
                SegmentEvents.NAV_ELEMENT_KEYBOARD,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackTestSummaryScreenVoiceSearchClick(query: String, queryLength: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.user_query = query
            eventParams.query_length = queryLength
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_VOICE_SEARCH_CLICK,
                null,
                SegmentEvents.EVENT_TYPE_CLICK, eventParams
            )
        }
    }

    fun trackTestSummaryScreenSearchLoadStart(source: String) {
        doAsync {
            val eventParams = EventParams()
            eventParams.source = source
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_START,
                null,
                SegmentEvents.EVENT_TYPE_LOAD, eventParams
            )
        }
    }

    fun trackTestSummaryScreenSearchLoadEnd() {
        doAsync {
            SegmentIO.getInstance().track(
                SegmentEvents.LOG_TYPE_TEST_SUMMARY,
                SegmentEvents.EVENT_TEST_SUMMARY_SCREEN_SEARCH_RESULT_LOAD_END,
                null,
                SegmentEvents.EVENT_TYPE_LOAD
            )
        }
    }
}