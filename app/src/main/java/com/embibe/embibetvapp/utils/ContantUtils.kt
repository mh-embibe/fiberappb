package com.embibe.embibetvapp.utils

class ContantUtils {

    companion object {

        val mImgIds = arrayOf(
            "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/avatar/ic_avatar1.png",
            "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/avatar/ic_avatar2.png",
            "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/avatar/ic_avatar3.png",
            "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/avatar/ic_avatar4.png",
            "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/avatar/ic_avatar5.png"
        )

        var testingdata = arrayOf(
            "Akash Kumar", "Sachin Raj", "Asif Ali"
        )

        val timeSpendData = arrayOf("1 hours", "2 hours", "3 hours", "More than 4 hours")
        val targetGradeData =
            arrayOf("Grade A1", "Grade A2", "Grade B1", "Grade B2", "Grade C1", "Grade C2")
        val targetPointData = arrayOf(
            "Grade point 10",
            "Grade point 9",
            "Grade point 8",
            "Grade point 7",
            "Grade point 6",
            "Grade point 5"
        )
        val targetMarksData = arrayOf(
            "Marks range 91-100%",
            "Marks range 81-90%",
            "Marks range 71-80%",
            "Marks range 61-70%",
            "Marks range 51-60%",
            "Marks range 41-50%"
        )

        val calenderData = arrayOf("Days", "Weeks", "Months", "Years")
        val classData = arrayOf("11", "12", "12+")
        val genderData = arrayOf("Male", "Female")
        val categoryData = arrayOf("GEN", "OBC", "ST", "SC", "EWS")
        val diffAbledData = arrayOf("Yes", "No")

        fun getImgResourceId(position: Int): String {
            val pos: Int = if (position < mImgIds.size) {
                position
            } else {
                position - mImgIds.size
            }
            return mImgIds[pos]
        }


    }

}