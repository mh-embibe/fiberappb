package com.embibe.embibetvapp.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}