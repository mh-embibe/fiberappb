package com.embibe.embibetvapp.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Animatable
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.leanback.widget.BaseCardView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants.ANIMATE
import com.embibe.embibetvapp.constant.AppConstants.NOT_ANIMATE
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo


@SuppressLint("ViewConstructor")
class GifCardView(context: Context, styleResId: Int) :
    BaseCardView(ContextThemeWrapper(context, styleResId), null, 0) {

    lateinit var tvSubjectName: TextView
    lateinit var ivVideo: SimpleDraweeView
    lateinit var iv_thumbnail: ImageView
    lateinit var ivEmbibeLogo: ImageView
    lateinit var ivCategory: ImageView
    lateinit var progress_bar: ProgressBar
    lateinit var cardView: CardView

    init {
        buildImageCardView()
    }

    companion object;

    private fun buildImageCardView() {
        isFocusable = true
        isFocusableInTouchMode = true
        val context = context
        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.card_animation_video, this)
        tvSubjectName = findViewById(R.id.tvSubjectName)
        ivVideo = findViewById(R.id.ivVideo)
        iv_thumbnail = findViewById(R.id.iv_thumbnail)
        ivEmbibeLogo = findViewById(R.id.ivEmbibeLogo)
        ivCategory = findViewById(R.id.ivCategory)
        progress_bar = findViewById(R.id.progress_bar)
        cardView = findViewById(R.id.cardView)
    }

    fun hideVideoView() {
        tag = NOT_ANIMATE
        iv_thumbnail.visibility = View.VISIBLE
        ivVideo.visibility = View.INVISIBLE
        cardView.visibility = View.INVISIBLE
    }


    fun showVideoView(previewURl: String) {
        //setGif()
        hideImageView()
        setWebP(previewURl)
    }

    fun hideImageView() {
        //iv_thumbnail.visibility = View.GONE
        ivVideo.visibility = View.VISIBLE
        cardView.visibility = View.VISIBLE
    }

    private fun setWebP(previewURl: String) {
        val controller = Fresco.newDraweeControllerBuilder()
        controller.autoPlayAnimations = true
        controller.setUri(previewURl)
        controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
            override fun onFinalImageSet(
                id: String?,
                imageInfo: ImageInfo?,
                animatable: Animatable?
            ) {
                val anim = animatable as AnimatedDrawable2
                anim.setAnimationListener(object : AnimationListener {
                    override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                    override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                        hideImageView()
                    }

                    override fun onAnimationFrame(
                        drawable: AnimatedDrawable2?,
                        frameNumber: Int
                    ) {
                        if (frameNumber == 0)
                            tag = ANIMATE

                    }

                    override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                        hideVideoView()
                    }

                    override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                })
            }
        }
        ivVideo.controller = controller.build()
    }

}