package com.embibe.embibetvapp.utils

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.util.Log
import com.embibe.embibetvapp.application.App.Companion.context

class ConnectionManager private constructor() {


    private object HOLDER {
        val INSTANCE = ConnectionManager()
    }

    companion object {
        val instance: ConnectionManager by lazy {
            HOLDER.INSTANCE
        }
        var connectivityReceiverListener: ConnectivityReceiverListener? = null
        val isConnected: Boolean
            get() {
                return hasNetworkAvailable()
            }

        private fun hasNetworkAvailable(): Boolean {
            val service = Context.CONNECTIVITY_SERVICE
            val manager = context.getSystemService(service) as ConnectivityManager?
            val network = manager?.activeNetwork
            return network != null
        }
    }

    class NetworkReceiver : BroadcastReceiver() {
        @SuppressLint("UnsafeProtectedBroadcastReceiver")
        override fun onReceive(context: Context?, intent: Intent?) {
            try {
                val notConnected = intent!!.getBooleanExtra(
                    ConnectivityManager
                        .EXTRA_NO_CONNECTIVITY, false
                )
                connectivityReceiverListener!!.onNetworkConnectionChanged(!notConnected)
            } catch (e: Exception) {

            }

        }

    }

    private fun connected() {
        Log.w("internet", " connected")
    }

    private fun disconnected() {
        Log.w("internet", " disconnected")
    }


    fun hasNetworkAvailable(): Boolean {
        val service = Context.CONNECTIVITY_SERVICE
        val manager = context.getSystemService(service) as ConnectivityManager?
        val network = manager?.activeNetwork
        return network != null
    }

    fun getNetworkSpeed(): Int? {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val nc =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        val downSpeed = nc.linkDownstreamBandwidthKbps
        val upSpeed = nc.linkUpstreamBandwidthKbps


        /* val wifiManager =
             context.getSystemService(Context.WIFI_SERVICE) as WifiManager
         val linkSpeed = wifiManager.connectionInfo.rssi
         val level = WifiManager.calculateSignalLevel(linkSpeed, 5)*/
        return downSpeed
    }

    interface ConnectivityReceiverListener {
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }


}