package com.embibe.embibetvapp.utils

import android.util.Log
import java.lang.Exception
import java.util.concurrent.TimeUnit

class DateTimeUtils {


    companion object {
        private const val TAG = "DateTimeUtils"
        const val ONE_SECOND_IN_MILLI:Long = 1000


        fun getFormattedTimerString(milliSec: Long): String {

            var result = ""
            try {
                result = String.format(
                    "%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(milliSec),
                    TimeUnit.MILLISECONDS.toMinutes(milliSec) - TimeUnit.HOURS.toMinutes(
                        TimeUnit.MILLISECONDS.toHours(milliSec)
                    ),
                    TimeUnit.MILLISECONDS.toSeconds(milliSec) - TimeUnit.MINUTES.toSeconds(
                        TimeUnit.MILLISECONDS.toMinutes(milliSec)
                    )
                )
            } catch (ex: Exception) {
                Log.e(TAG, ex.localizedMessage)
            }
            return result
        }

         fun timeConversion(totalSeconds: Int): String? {
            val MINUTES_IN_AN_HOUR = 60
            val SECONDS_IN_A_MINUTE = 60
            val seconds = totalSeconds % SECONDS_IN_A_MINUTE
            val totalMinutes = totalSeconds / SECONDS_IN_A_MINUTE
            val minutes = totalMinutes % MINUTES_IN_AN_HOUR
            val hours = totalMinutes / MINUTES_IN_AN_HOUR
            return "$hours : $minutes : $seconds"
        }
    }
}