package com.embibe.embibetvapp.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

    public static final String APP_ID = "app_id";

    public static final String S3_BUCKET_NAME = "s3_bucket_name";

    public static final String S3_IMAGE_BUCKET_NAME = "s3_image_bucket_name";

    public static final String FEATURE_GENERATE_PASSWORD = "feature_generate_password";

    public static final String INSTITUTE_CONTACT_NUMBER = "institute_contact_number";

    public static final String PACK_TYPE = "pack_type";

    public static final String PACK_TYPE_UPGRADED = "pack_type_upgraded";

    public static final String POWRED_BY_EMBIBE = "powered_by_embibe";

    public static final String PAYABLE_AMOUNT = "payable_amount";

    public static final String USER_PROFILE = "user_profile";

    public static final String TOOLBAR_SUBTITLE = "toolbar_subtitle";

    public static final String SHOW_CONCEPT = "show_concept";
    public static final String SHOW_CHAPTER_WISE_ANALYSIS = "show_chapterwise_analysis";
    public static final String SHOW_CHAPTER_WISE_ANALYSIS_CONCEPT = "show_chapterwise_analysis_concept";
    public static final String TEST_ENABLED = "test_enabled";
    public static final String PRACTICE_ENABLED = "practice_enabled";
    public static final String VIDEOS_ENABLED = "videos_enabled";
    public static final String STUDY_ENABLED = "study_enabled";
    public static final String LEARN_ENABLED = "learn_enabled";
    public static final String SIGNUP_ENABLED = "signup_enabled";
    public static final String CONTACT_INSTITUTE_ENABLED = "contact_institute_enabled";
    public static final String SHOW_QUESTION_PAPER = "show_question_paper";
    public static final String TEST_LANDSCAPE_MODE_ENABLED = "test_landscape_mode_enabled";
    public static final String GENERATE_PASSWORD = "generate_password";
    public static final String FORGOT_PASSWORD_OPTION = "forgot_password_option";

    public static final String JUMBLED_SEQUENCE = "jumbled_sequence";
    public static final String MDM_ENABLED = "enable_mdm";

    public static final String SD_CARD_BASE_DIR = "sd_card_base_dir";
    public static final String SD_CARD_BUCKET_NAME = "sd_card_bucket_name";
    public static final String SD_CARD_VIDEO_DIR = "sd_card_video_dir";
    public static final String WEBSITE = "website";
    private static final String CLASS_TAG = Config.class.getName();
    private static Config instance = null;
    private Properties properties;

    private Config(Context context) {
        Resources resources = context.getResources();
        AssetManager assetManager = resources.getAssets();

        try {
            InputStream inputStream = assetManager.open("app.properties");
            properties = new Properties();
            properties.load(inputStream);
            Log.i(CLASS_TAG, "Properties are loaded");
        } catch (IOException e) {
            Log.e(CLASS_TAG, "Failed to open app property file");
            e.printStackTrace();
        }
    }

    public static Config getInstance(Context context) {
        if (instance == null) {
            instance = new Config(context);
        }
        return instance;
    }

    public String getProperty(String propertyName) {
        if (properties != null)
            return properties.getProperty(propertyName);
        return "com.embibe.embibetvapp";
    }

    public String getProperty(String propertyName, String defaultValue) {
        return properties.getProperty(propertyName, defaultValue);
    }
}
