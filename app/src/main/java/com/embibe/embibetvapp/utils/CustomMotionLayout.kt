package com.embibe.embibetvapp.utils

import android.content.Context
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.motion.widget.MotionScene
import androidx.constraintlayout.motion.widget.TransitionBuilder
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.size

class CustomMotionLayout(context: Context) : MotionLayout(context) {

    private var list = ArrayList<View>()

    private var listener: RowListener? = null

    private val viewIds: List<Int> get() = list.map { it.id }

    private val weights: FloatArray

    private var viewTransition: MotionScene.Transition? = null

    init {
        weights = FloatArray(list.size)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        val scene = MotionScene(this)
        createViews(this)
        dpadListenerForViews()
        viewTransition = createTransition(scene)
        scene.addTransition(viewTransition)
        scene.setTransition(viewTransition)
        setScene(scene)
    }

    private fun createViews(layout: CustomMotionLayout) {
        val set = ConstraintSet()
        set.clone(layout)
        for (view in list) {
            val layoutParams = ViewGroup.LayoutParams(size, size)
            layout.addView(view, layoutParams)
            set.constrainHeight(
                view.id,
                size
            )
            set.connect(
                view.id,
                ConstraintSet.END,
                ConstraintSet.PARENT_ID,
                ConstraintSet.END
            )
        }

        set.createVerticalChain(
            ConstraintSet.END,
            ConstraintSet.TOP,
            ConstraintSet.END,
            ConstraintSet.BOTTOM,
            viewIds.toIntArray(),
            weights, ConstraintSet.CHAIN_SPREAD
        )
        set.applyTo(layout)
    }

    private fun createTransition(scene: MotionScene): MotionScene.Transition {
        val startSetId = View.generateViewId()
        val startSet = ConstraintSet()
        startSet.clone(this)
        val endSetId = View.generateViewId()
        val endSet = ConstraintSet()
        endSet.clone(this)
        val transitionId = View.generateViewId()
        return TransitionBuilder.buildTransition(
            scene,
            transitionId,
            startSetId, startSet,
            endSetId, endSet
        )
    }


    fun addViewsToMotionLayout(list: ArrayList<View>) {
        this.list = list
    }

    private fun dpadListenerForViews() {
        for (i in 0 until list.size)
            setDpadListener(list[i], i)
    }

    private fun setDpadListener(view: View, index: Int) {
        view.setOnKeyListener { v, keyCode, event ->
            when (event.action) {
                KeyEvent.KEYCODE_DPAD_UP -> {
                    listener?.currentMovedRow(index)
                }
                KeyEvent.KEYCODE_DPAD_DOWN -> {
                    listener?.currentMovedRow(index)
                }
            }
            return@setOnKeyListener false
        }
    }

    fun setRowListener(listener: RowListener) {
        this.listener = listener
    }

    interface RowListener {
        fun currentMovedRow(int: Int)
    }

    fun animateWidget() {
//        val startSet: ConstraintSet = getConstraintSet(viewTransition!!.startConstraintSetId)
//        val endSet: ConstraintSet = getConstraintSet(viewTransition!!.endConstraintSetId)
//        setTransition(viewTransition!!.startConstraintSetId, viewTransition!!.endConstraintSetId)
//        startSet.applyTo(this)
//        endSet.applyTo(this)
    }

}