package com.embibe.embibetvapp.utils

import java.util.regex.Pattern

object VideoUtils {
    // regular expressions
    private val YOUTUBE_PATTERN = Pattern.compile(
        ".*(?:youtu.be\\/|v\\/|u\\/\\w\\/|embed\\/|watch\\?v=)([^#\\&\\?]*).*",
        Pattern.CASE_INSENSITIVE
    )
    private val VIMEO_PATTERN = Pattern.compile(
        "[http|https]+:\\/\\/(?:www\\.|)vimeo\\.com\\/([a-zA-Z0-9_\\-]+)(&.+)?",
        Pattern.CASE_INSENSITIVE
    )

    // youtube thumbnail settings
    private const val YOUTUBE_THUMBNAIL_URL = "http://i.ytimg.com/vi/"
    private const val YOUTUBE_THUMBNAIL_SIZE = "/hqdefault.jpg"
    fun isYouTubeUrl(url: String?): Boolean {
        return YOUTUBE_PATTERN.matcher(url).matches()
    }

    fun isVimeoUrl(url: String?): Boolean {
        return VIMEO_PATTERN.matcher(url).matches()
    }

    fun getYouTubeThumbnailUrl(url: String): String? {
        val youtube = YOUTUBE_PATTERN.matcher(url)
        if (!youtube.find()) return null
        val group = youtube.group(1)
        return YOUTUBE_THUMBNAIL_URL + group + YOUTUBE_THUMBNAIL_SIZE
    }

    fun getVimeoVideoId(url: String): Long {
        val vimeo = VIMEO_PATTERN.matcher(url)
        return if (!vimeo.find()) -1 else java.lang.Long.valueOf(vimeo.group(1))
    }
}