/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.embibe.embibetvapp.utils

import VimeoVideo
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.ConfigurationCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProviders
import com.airbnb.lottie.LottieAnimationView
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.embibe.app.utils.Resource
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App.Companion.context
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.QWA_ATTEMPT_TYPE.*
import com.embibe.embibetvapp.model.LastWatchedContentRes
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.adduser.Exam
import com.embibe.embibetvapp.model.home.HomeModel
import com.embibe.embibetvapp.model.home.TestFeedBackModel
import com.embibe.embibetvapp.model.practise.details.PracticeTopicsRequest
import com.embibe.embibetvapp.model.response.ErrorResponse
import com.embibe.embibetvapp.model.test.TestSkillsRes
import com.embibe.embibetvapp.newmodel.Annotation
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.activity.YouTubePlayerActivity
import com.embibe.embibetvapp.ui.fragment.errorAlertScreen.CustomScreenDialog
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.PractiseViewModel
import com.embibe.embibetvapp.ui.viewmodel.TestViewModel
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.application.LibApp
import com.embibejio.coreapp.preference.PreferenceHelper
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jgabrielfreitas.core.BlurImageView
import okhttp3.internal.filterList
import org.jetbrains.anko.doAsync
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import java.lang.reflect.Type
import java.math.RoundingMode
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.collections.HashMap
import kotlin.math.floor


/**
 * A collection of utility methods, all static.
 */
object Utils {

    private const val TAG = "Utils"

    var isLikedForTest: Boolean = false
    var isTestGenerated: Boolean = false
    var isSubjectFilterFromPractice: Boolean = false
    var subjectFilterBy: String = ""
    var isDialogShowing: Boolean = false
    var isStatusUpdatedForVideosAndCoobos: Boolean = false
    var isContentStatusUpdatedForPractice: Boolean = false
    var isContentStatusUpdatedForTest: Boolean = false
    var isContentStatusUpdatedForQuickLinks: Boolean = false
    var isContentStatusUpdatedForQuickLinksInLearn: Boolean = false
    var isLikeBookmarkUpdatedForQuickLinks: Boolean = false
    var isLikeBookmarkUpdatedInLearn: Boolean = false
    var isBookmarkUpdatedForTEST: Boolean = false
    var isSwitchGoalDone: Boolean = false
    private var mLastKeyDownTime: Long = 0
    private lateinit var currentJSONObject: String

    @JvmStatic
    @JvmOverloads
    fun updateStatus(
        contentID: String,
        contentType: String,
        contentStatus: String,
        watchedDuration: Int? = 0
    ) {
        doAsync {
            var volleyRequest = Volley.newRequestQueue(LibApp.context)
            val childId = UserData.getChildId()
            val params = JSONObject()
            params.put("content_id", contentID)
            params.put("content_type", contentType)
            params.put("watched_duration", watchedDuration ?: 0)
            params.put("content_status", contentStatus.toUpperCase())
            params.put("child_id", childId)
            val jsonBigRequest = object : JsonObjectRequest(
                Method.POST,
                "https://preprodms.embibe.com/fiber_ms/content-status", params,
                Response.Listener { response: JSONObject ->
                    isStatusUpdatedForVideosAndCoobos = true
                    Log.d("Update Status", response.toString())
                    Log.w("Updating.. unity content-status success : res", response.toString())
                    //apiCallBackVolley!!.onSuccessCallBack(response)
                },
                Response.ErrorListener { error: VolleyError ->
                    Log.d("Update content-status", "error")

                }) {

                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers[com.embibejio.coreapp.constant.AppConstants.EMBIBETOKEN] =
                        PreferenceHelper().get(
                            com.embibejio.coreapp.constant.AppConstants.EMBIBETOKEN,
                            ""
                        )
                    return headers
                }
            }
            Log.w("Updating.. unity content-status", params.toString())
            volleyRequest.add(jsonBigRequest)
        }
    }

    internal fun getExams(): List<Exam> {
        val listType = object : TypeToken<List<Exam>>() {}.type
        return Gson().fromJson<List<Exam>>(PreferenceHelper()[AppConstants.GOALS, "[]"], listType)
    }

    fun inputStreamToString(inputStream: InputStream): String {
        return try {
            val bytes = ByteArray(inputStream.available())
            inputStream.read(bytes, 0, bytes.size)
            String(bytes)
        } catch (e: IOException) {
            return ""
        }
    }

    fun loadLottieAnimation(lv: LottieAnimationView, animJson: Int) {
        lv.setAnimation(animJson)
        lv.playAnimation()
    }

    fun getDuration(time: Int): String {
        val hrs = time / 60
        val min = time % 60
        return if (hrs > 0 && min > 0)
            hrs.toString() + "h " + min + " min"
        else if (hrs > 0)
            hrs.toString() + "h"
        else
            "$min min"
    }

    fun hasPermission(context: Context, permission: String): Boolean {
        return PackageManager.PERMISSION_GRANTED == context.packageManager.checkPermission(
            permission, context.packageName
        )
    }

    fun hideKeyboardFrom(context: Context?, view: View) {
        val imm: InputMethodManager =
            context!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showToast(context: Context, msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT)
            .show()
    }

    fun getBackgroundUrl(data: Content) = getSubjectBackground(data.subject)


    fun getVideoTypeUsingUrl(url: String): String {
        return if (url.contains("vimeo.com")) {
            AppConstants.PLAYER_TYPE_EXOPLAYER
        } else {
            AppConstants.PLAYER_TYPE_YOUTUBE
        }
    }

    fun getSubjectBackground(subject: String?): String {
        return when (subject) {
            "Biology" -> "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/bg/Biology_DP.png"
            "Chemistry" -> "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/bg/Chemistry_DP.png"
            "Maths", "Mathematics" -> "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/bg/Mathematics_DP.png"
            "Physics" -> "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/bg/Physics_DP.png"
            "Science" -> "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/bg/Science_DP.png"
            else -> "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/bg/Science_DP.png"
        }
    }

    fun getBackgroundBlurUrl(data: Content): String {
        return getSubjectBackgroundBlur(data.subject)
    }

    fun getSubjectBackgroundBlur(subject: String?): String {
        return when (subject) {
            "Biology" -> "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/backgroundblur/Biology_Blur_DP.webp"
            "Chemistry" -> "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/backgroundblur/Chemistry_Blur_DP.webp"
            "Maths", "Mathematics" -> "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/backgroundblur/Mathematics_Blur_DP.webp"
            "Physics" -> "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/backgroundblur/Physics_Blur_DP.webp"
            "Science" -> "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/backgroundblur/Science_Blur_DP.webp"
            else -> "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/backgroundblur/Science_Blur_DP.webp"
        }
    }

    fun getVideoURL(
        context: Context,
        videoID: String?, accessToken: String?,
        apiCallBackVolley: BaseViewModel.APICallBackVolley?
    ) {

        val volleyRequest = Volley.newRequestQueue(context)
        val jsonBigRequest = object : JsonObjectRequest(
            Method.GET,
            ApiConstants.VIMEO_BASE_URL + "$videoID", null,
            Response.Listener { response: JSONObject ->
                Log.d("sample", response.toString())
                apiCallBackVolley!!.onSuccessCallBack(response)
            },
            Response.ErrorListener { error: VolleyError ->
                apiCallBackVolley!!.onErrorCallBack(error)
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] =
                    ApiConstants.VIMEO_BEARER + if (accessToken.isNullOrEmpty()) ApiConstants.VIMEO_AUTHORIZATION_TOKEN else accessToken
                headers["Accept"] = ApiConstants.VIMEO_HEADER
                return headers
            }
        }
        volleyRequest.add(jsonBigRequest)
    }

    fun isKeyPressedTooFast(interval: Int): Boolean {
        val current = System.currentTimeMillis()
        var res = false
        if (current - mLastKeyDownTime < interval) {
            res = true
        } else {
            mLastKeyDownTime = current
        }
        return res
    }

    fun isEmailValid(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email.trim()).matches()
    }

    /**
     *  if return 1 then input type is mobile
     *  if return 2 then input type is email
     *  if return 0 then input type is invalid
     * **/
    fun getInputType(input: String): Int {
        val emailPattern = "[a-zA-Z0-9._-]+@+"
        val emailPattern1 = "[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+"
        val emailPattern2 = "[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.+[a-zA-Z0-9._-]+"
        val mobilePattern = "[0-9]+"
        return when {
            input.matches(mobilePattern.toRegex()) -> {
                1
            }
            input.matches(emailPattern.toRegex()) -> {
                2
            }
            input.matches(emailPattern1.toRegex()) -> {
                2
            }
            input.matches(emailPattern2.toRegex()) -> {
                2
            }
            else -> 0
        }
    }

    fun getSpannable(
        ctx: Context,
        strToChange: String,
        color: Int,
        start: Int,
        end: Int
    ): SpannableString {
        var spannableString = SpannableString(strToChange)
        spannableString.setSpan(
            ForegroundColorSpan(ResourcesCompat.getColor(ctx.resources, color, null)),
            start,
            end,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return spannableString
    }

    fun getSpannable(strToChange: String, size: Float, color: Int, start: Int, end: Int)
            : SpannableString {
        var spannableString = SpannableString(strToChange)
        spannableString.setSpan(RelativeSizeSpan(size), start, end, 0)
        spannableString.setSpan(
            ForegroundColorSpan(context.resources.getColor(color)),
            start, end, 0
        )
        return spannableString
    }

    fun setColorToProgressBar(progressBar: ProgressBar, view: View, bgColor: Int, fgColor: Int) {
        var progressBarDrawable = progressBar.progressDrawable as LayerDrawable
        val backgroundDrawable: Drawable = progressBarDrawable.getDrawable(0)
        val progressDrawable: Drawable = progressBarDrawable.getDrawable(1)

        backgroundDrawable.setColorFilter(
            ContextCompat.getColor(
                view.context,
                bgColor
            ), PorterDuff.Mode.SRC_IN
        )
        progressDrawable.setColorFilter(
            ContextCompat.getColor(
                view.context,
                fgColor
            ), PorterDuff.Mode.SRC_IN
        )
    }

    fun <T : ViewDataBinding> binder(layout: Int, parent: ViewGroup): T {
        return DataBindingUtil.inflate<T>(
            LayoutInflater.from(parent.context)!!,
            layout, parent, false
        )
    }

    /* get child id here*/
    fun getChildId() = UserData.getChildId()

    fun getAndroidId(): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun getCustomId(androidId: String, count: Int): String {
        return androidId.takeLast(count)
    }

    fun setDuration(content: Content): String {
        return if (content.type != "chapter")
            convertIntoMins(content.length)
        else
            convertIntoConcepts(content.concept_count)
    }

    fun setEmbium(content: Content): String {
        return content.currency.toString()
    }


    fun extractYoutubeVideoId(ytUrl: String): String {
        val pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*"
        val compiledPattern = Pattern.compile(pattern)
        val matcher = compiledPattern.matcher(ytUrl)
        return if (matcher.find()) {
            matcher.group()
        } else {
            "error"
        }
    }

    fun getExamLevelCode(parts: List<String>): String {
        for (part in parts) {
            if (getLevelUsingCode(part) == "exam")
                return part
        }
        return ""
    }

    fun getLevelUsingCode(lmCode: String): String {
        return when (lmCode.substring(0, 2)) {
            "ch" -> "chapter"
            "tp" -> "topic"
            "un" -> "unit"
            "ex" -> "exam"
            else -> "chapter"
        }
    }


    //********
    var dialogFragment = CustomScreenDialog()
    var practiceDialogFragment = CustomScreenDialog()

    /**
     *@param: dialogConstant must not be null or Empty
     *@param: fragmentManager must not be null
     * other params can be null
     * */
    @JvmOverloads
    fun showAlertDialog(
        dialogConstant: String,
        fragmentManager: FragmentManager,
        errorScreensBtnClickListener: Any? = null,
        callBacks: BaseViewModel.ErrorCallBacks? = null,
        errorMsg: String = "",
        concept_id: String? = null
    ) {
        showDialog(
            dialogConstant, fragmentManager, errorMsg,
            errorScreensBtnClickListener, callBacks, concept_id
        )
    }


    private fun showDialog(
        dialogConstant: String,
        fragmentManager: FragmentManager,
        errorMsg: String,
        errorScreensBtnClickListener: Any?,
        callBacks: BaseViewModel.ErrorCallBacks?,
        conceptCode: String?
    ) {
        if (dialogConstant == AppConstants.PRACTICE_DIALOG) {
            practiceDialogFragment.setMsg(errorMsg)
            practiceDialogFragment.setDialogConstant(dialogConstant)
            practiceDialogFragment.setListener(errorScreensBtnClickListener)
            practiceDialogFragment.setCallBacks(callBacks)
            practiceDialogFragment.setConceptCode(conceptCode)
            practiceDialogFragment.show(fragmentManager, dialogConstant)
        } else {
            isDialogShowing = true
            dialogFragment.setMsg(errorMsg)
            dialogFragment.setDialogConstant(dialogConstant)
            dialogFragment.setListener(errorScreensBtnClickListener)
            dialogFragment.setCallBacks(callBacks)
            dialogFragment.show(fragmentManager, dialogConstant)
        }
    }

    fun getIsShowing(): Boolean {
        return dialogFragment.isHidden
    }

    @JvmOverloads
    fun closeDialog(isPractice: String? = null) {
        if (isPractice == null) {
            dialogFragment.dismiss()
        } else {
            practiceDialogFragment.dismiss()
        }
    }

    fun dialogWebViewAction(searchQuery: String) {
        if (practiceDialogFragment != null)
            practiceDialogFragment.updateSearchQuery(searchQuery)
    }

    fun convertFirstCharCapital(str: String): String {
        return if (str.isNotEmpty())
            str[0].toUpperCase().toString() + str.subSequence(1, str.length)
        else
            str
    }

    fun removeUnderScoreAndCapitalize(headerText: String): String {

        val words = headerText.split("_").toMutableList()
        var output = ""

        for (word in words) {
            output += word.capitalize() + " "
        }

        output = output.trim()
        return output
    }

    fun getConvertedTime(duration: Long): String {
        return String.format("%02d:%02d", (duration % 3600) / 60, duration % 60)
    }

    fun convertIntoMins(length: Int?): String {
        var minuteString = context.getString(R.string.mintue)
        return if (length != null) {
            var mins = length / 60
            val seconds = length % 60
            if (mins > 2) {
                minuteString = context.getString(R.string.mintues)
            }
            return when {
                mins < 1 -> {
                    mins = 1
                    "$mins $minuteString"
                }
                else -> {
                    "$mins $minuteString $seconds " + context.getString(R.string.seconds)
                }
            }
        } else
            "0 $minuteString"
    }

    fun convertIntoMinsTest(length: Int?): String {
        var minuteString = context.getString(R.string.mintue)
        return if (length != null) {
            var mins = length / 60
            if (mins > 2) {
                minuteString = context.getString(R.string.mintues)
            }
            if (mins < 1)
                mins = 1
            "$mins $minuteString "
        } else
            "0 $minuteString"
    }

    fun convertIntoConcepts(conceptCount: Int?): String {
        return if (conceptCount != null) {
            if (conceptCount < 2) {
                "$conceptCount " + context.getString(R.string.concept)
            } else {
                "$conceptCount " + context.getString(R.string.concepts)
            }
        } else
            "0 " + context.getString(R.string.concept)
    }


    fun getColorCode(subject: String?): Int {
        return when (subject?.toLowerCase(Locale.ENGLISH)) {
            "maths", "mathematics" -> return R.color.sub_maths
            "chemistry" -> return R.color.sub_chemistry
            "biology" -> return R.color.sub_biology
            "physics" -> return R.color.sub_physics
            else -> R.color.sub_all
        }
    }

    fun <T> isApiFailed(resource: Resource<T>): Boolean {
        val errorModel = resource.data as ErrorResponse
        return getErrorType(errorModel.code) != null
    }

    fun isApiFailed(code: Int): Boolean {
        return getErrorType(code) != null
    }

    fun <T> showError(
        context: Context?,
        resource: Resource<T>,
        callBack: BaseViewModel.ErrorCallBacks, errorListener: Any? = null
    ) {
        val errorModel = resource.data as ErrorResponse
        if (!isDialogShowing) {
            showAlertDialog(
                getErrorType(errorModel.code)!!,
                getFragmentManger(context!!),
                errorListener, callBack,
                ""/* error msg ignored (its taken care by UI )*/
            )
        }
    }

    fun showError(
        context: Context?,
        code: Int,
        callBack: BaseViewModel.ErrorCallBacks,
        errorListener: Any? = null
    ) {
        if (!isDialogShowing) {
            showAlertDialog(
                getErrorType(code)!!,
                getFragmentManger(context!!),
                errorListener, callBack,
                ""/* error msg ignored (its taken care by UI )*/
            )
        }

    }


    private fun getFragmentManger(context: Context): FragmentManager {

        return when (context) {
            is FragmentActivity -> context.supportFragmentManager
            else -> {
                val themeWrapper: ContextThemeWrapper = context as ContextThemeWrapper
                val fm: FragmentManager =
                    (themeWrapper.baseContext as AppCompatActivity).supportFragmentManager
                fm
            }
        }

    }

    private fun getErrorType(code: Int): String? {
        return when (code) {
            400 -> AppConstants.SERVER_DOWN
            401 -> null
            403 -> null
            404 -> AppConstants.ERROR_404_DIALOG
            500 -> AppConstants.SERVER_DOWN
            ApiConstants.API_CODE_NO_NETWORK -> AppConstants.NO_CONNECTIVITY
            else -> null
        }
    }

    fun getEmail(subscriberId: String) = "$subscriberId@jio-embibe.com"

    fun parseHashMapFromAnnotation(annotationDetails: ArrayList<Annotation>): HashMap<Long, Annotation> {
        val annotationMap = HashMap<Long, Annotation>()
        for (item in annotationDetails) {
            annotationMap[item.annotationPoint!!.toLong()] = item
        }
        return annotationMap
    }

    fun insertBundle(content: Content, intent: Intent) {
        //intent.putExtra(AppConstants.CONTENT_ID, content.objId)
        intent.putExtra(AppConstants.CONTENT, content)
    }

    fun loadConceptsAsync(content: Content, context: Context?) {
        val homeViewModel =
            ViewModelProviders.of(context as FragmentActivity).get(HomeViewModel::class.java)
        homeViewModel.getConceptsAsync(
            content,
            object : BaseViewModel.APIMergeCallBacks<HomeModel> {
                override fun onSuccess(model: HomeModel?) {
                    if (model?.moreConcepts!!.isNotEmpty()) {
                        DataManager.instance.setMoreTopics(content.id, model.moreConcepts!!)
                    } else {
                        DataManager.instance.setMoreTopics(content.id, arrayListOf())
                    }
                    if (model.relatedConcepts!!.isNotEmpty()) {
                        DataManager.instance.setRelatedConceptDetails(
                            content.id,
                            model.relatedConcepts!!
                        )
                    } else {
                        DataManager.instance.setRelatedConceptDetails(content.id, arrayListOf())
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    /*ignored for Async call*/
                    DataManager.instance.setRelatedConceptDetails(content.id, arrayListOf())
                    DataManager.instance.setMoreTopics(content.id, arrayListOf())
                }
            })
    }


    fun loadLearnSummaryAsync(content: Content, context: Context?) {
        val homeViewModel =
            ViewModelProviders.of(context as FragmentActivity).get(HomeViewModel::class.java)
        homeViewModel.getLearnSummaryAsync(
            content, object : BaseViewModel.APIMergeCallBacks<HomeModel> {
                override fun onSuccess(model: HomeModel?) {
                    if (model?.topicsForChapter!!.isNotEmpty()) {
                        DataManager.instance.setTopicsForChapter(
                            content.id,
                            model.topicsForChapter!!
                        )
                    } else {
                        DataManager.instance.setTopicsForChapter(content.id, arrayListOf())
                    }
                    if (model.videosForChapterV1!!.isNotEmpty()) {
                        DataManager.instance.setVideosForChapter(
                            content.id,
                            model.videosForChapterV1!!
                        )
                    } else {
                        DataManager.instance.setVideosForChapter(content.id, arrayListOf())
                    }
                    if (model.testsForChapter!!.isNotEmpty()) {
                        DataManager.instance.setTestsForChapter(
                            content.id,
                            model.testsForChapter!!
                        )
                    } else {
                        DataManager.instance.setTestsForChapter(content.id, arrayListOf())
                    }
                    if (model.practicesForChapter!!.isNotEmpty()) {
                        DataManager.instance.setPracticesForChapter(
                            content.id,
                            model.practicesForChapter!!
                        )
                    } else {
                        DataManager.instance.setPracticesForChapter(content.id, arrayListOf())
                    }
                    if (model.prerequisitesForChapter!!.isNotEmpty()) {
                        DataManager.instance.setPrerequisitesForChapter(
                            content.id,
                            model.prerequisitesForChapter!!
                        )
                    } else {
                        DataManager.instance.setPrerequisitesForChapter(content.id, arrayListOf())
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    /*ignored for Async call*/
                    DataManager.instance.setTopicsForChapter(content.id, arrayListOf())
                    DataManager.instance.setTestsForChapter(content.id, arrayListOf())
                    DataManager.instance.setVideosForChapter(content.id, arrayListOf())
                    DataManager.instance.setPrerequisitesForChapter(content.id, arrayListOf())
                    DataManager.instance.setPracticesForChapter(content.id, arrayListOf())
                }
            })
    }

    fun loadTestSummaryAsync(content: Content, context: Context?) {
        val homeViewModel =
            ViewModelProviders.of(context as FragmentActivity).get(HomeViewModel::class.java)
        homeViewModel.getTestSummaryAsync(
            content, object : BaseViewModel.APIMergeCallBacks<HomeModel> {
                override fun onSuccess(model: HomeModel?) {
                    if (model?.recommendedLearningForTest!!.isNotEmpty()) {
                        DataManager.instance.setRecommendedLearningForTest(
                            content.id, model.recommendedLearningForTest!!
                        )
                    } else {
                        DataManager.instance.setRecommendedLearningForTest(
                            content.id, arrayListOf()
                        )
                    }

                    if (model.recommendedPracticeForTest!!.isNotEmpty()) {
                        DataManager.instance.setRecommendedPracticeForTest(
                            content.id, model.recommendedPracticeForTest!!
                        )
                    } else {
                        DataManager.instance.setRecommendedPracticeForTest(
                            content.id, arrayListOf()
                        )
                    }

                    if (model.moreTests!!.isNotEmpty()) {
                        DataManager.instance.setMoreTests(content.id, model.moreTests!!)
                    } else {
                        DataManager.instance.setMoreTests(content.id, arrayListOf())
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    /*ignored for Async call*/
                    DataManager.instance.setRecommendedLearningForTest(content.id, arrayListOf())
                    DataManager.instance.setRecommendedPracticeForTest(content.id, arrayListOf())
                    DataManager.instance.setMoreTests(content.id, arrayListOf())
                }
            })
    }

    fun loadTopicSummaryAsync(content: Content, context: Context?) {
        val homeViewModel =
            ViewModelProviders.of(context as FragmentActivity).get(HomeViewModel::class.java)
        homeViewModel.getTopicSummaryAsync(
            content,
            object : BaseViewModel.APIMergeCallBacks<HomeModel> {
                override fun onSuccess(model: HomeModel?) {
                    if (model?.relatedTopics!!.isNotEmpty()) {
                        DataManager.instance.setRelatedTopics(
                            content.id,
                            model.relatedTopics!!
                        )
                    } else {
                        DataManager.instance.setRelatedTopics(content.id, arrayListOf())
                    }
                    if (model.videosForTopic!!.isNotEmpty()) {
                        DataManager.instance.setVideosForTopic(
                            content.id,
                            model.videosForTopic!!
                        )
                    } else {
                        DataManager.instance.setVideosForTopic(content.id, arrayListOf())
                    }
                    if (model.testsForTopic!!.isNotEmpty()) {
                        DataManager.instance.setTestsForTopic(
                            content.id,
                            model.testsForTopic!!
                        )
                    } else {
                        DataManager.instance.setTestsForTopic(content.id, arrayListOf())
                    }
                    if (model.practicesForTopic!!.isNotEmpty()) {
                        DataManager.instance.setPracticesForTopic(
                            content.id,
                            model.practicesForTopic!!
                        )
                    } else {
                        DataManager.instance.setPracticesForTopic(content.id, arrayListOf())
                    }
                    if (model.prerequisitesForTopic!!.isNotEmpty()) {
                        DataManager.instance.setPrerequisitesForTopic(
                            content.id,
                            model.prerequisitesForTopic!!
                        )
                    } else {
                        DataManager.instance.setPrerequisitesForTopic(content.id, arrayListOf())
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    /*ignored for Async call*/
                    DataManager.instance.setRelatedTopics(content.id, arrayListOf())
                    DataManager.instance.setTestsForTopic(content.id, arrayListOf())
                    DataManager.instance.setVideosForTopic(content.id, arrayListOf())
                    DataManager.instance.setPrerequisitesForTopic(content.id, arrayListOf())
                    DataManager.instance.setPracticesForTopic(content.id, arrayListOf())
                }
            })
    }


    /**
     * Last Watched content
     */
    fun getLastWatchedContent(context: Context?, source: String, bookId: String) {
        val homeViewModel =
            ViewModelProviders.of(context as FragmentActivity).get(HomeViewModel::class.java)
        homeViewModel.getLastWatchedContent(
            source, bookId,
            object : BaseViewModel.APICallBacks<LastWatchedContentRes> {
                override fun onSuccess(model: LastWatchedContentRes?) {
                    DataManager.instance.setLastWatchedContent(bookId, model?.data)
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    DataManager.instance.setLastWatchedContent(bookId, null)
                }
            })
    }

    fun getTopicsForPracticeAsync(context: Context, data: Content) {


        DataManager.instance.setPracticeTopics(data.id, arrayListOf())
        val practiseViewModel =
            ViewModelProviders.of(context as FragmentActivity).get(PractiseViewModel::class.java)
        val req = PracticeTopicsRequest()
        req.learnmapId = data.learnmap_id
        req.subject = data.subject
        practiseViewModel.getPracticeTopics(
            req,
            object : BaseViewModel.APICallBacks<List<Content>> {
                override fun onSuccess(model: List<Content>?) {
                    if (model != null && model.isNotEmpty()) {
                        DataManager.instance.setPracticeTopics(data.id, model as ArrayList<Content>)
                    } else {
                        DataManager.instance.setPracticeTopics(data.id, arrayListOf())
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    /*ignored for Async call*/
                    Log.w("app-log", "api failed $msg, $error")
                    DataManager.instance.setPracticeTopics(data.id, arrayListOf())
                }
            })
    }


    fun loadPracticeSummaryInfo(content: Content, context: Context?) {
        val practiseViewModel =
            ViewModelProviders.of(context as FragmentActivity).get(PractiseViewModel::class.java)
        practiseViewModel.getPracticeSummaryAsync(
            content,
            object : BaseViewModel.APIMergeCallBacks<HomeModel> {
                override fun onSuccess(model: HomeModel?) {
                    if (model?.practiceTopicsForChapter!!.isNotEmpty()) {
                        DataManager.instance.setPracticeTopics(
                            content.id,
                            model.practiceTopicsForChapter!!
                        )
                    } else {
                        DataManager.instance.setPracticeTopics(content.id, arrayListOf())
                    }

                    if (model.recommendationLearning!!.isNotEmpty()) {
                        DataManager.instance.setRecommendationLearningForPractice(
                            content.id,
                            model.recommendationLearning!!
                        )
                    } else {
                        DataManager.instance.setRecommendationLearningForPractice(
                            content.id,
                            arrayListOf()
                        )
                    }
                    if (model.testsForChapter!!.isNotEmpty()) {
                        DataManager.instance.setTestsForChapter(
                            content.id,
                            model.testsForChapter!!
                        )
                    } else {
                        DataManager.instance.setTestsForChapter(content.id, arrayListOf())
                    }
                    if (model.booksAvailable!!.isNotEmpty()) {
                        DataManager.instance.setBookAvailableForPractise(
                            content.id,
                            model.booksAvailable!!
                        )
                    } else {
                        DataManager.instance.setBookAvailableForPractise(content.id, arrayListOf())
                    }

                    /*if (model.bookmarkedQuestions!!.isNotEmpty()) {
                        DataManager.instance.setBookmarkedQuestionsForPractise(
                            content.id,
                            model.bookmarkedQuestions!!
                        )
                    } else {
                        DataManager.instance.setBookmarkedQuestionsForPractise(content.id, arrayListOf())
                    }*/
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    /*ignored for Async call*/
                    DataManager.instance.setPracticeTopics(content.id, arrayListOf())
                    DataManager.instance.setRecommendationLearningForPractice(
                        content.id,
                        arrayListOf()
                    )
                    DataManager.instance.setTestsForChapter(content.id, arrayListOf())
                    DataManager.instance.setBookAvailableForPractise(content.id, arrayListOf())


                }
            })

    }


    fun getContent(intent: Intent): Content {
        return intent.getParcelableExtra(AppConstants.CONTENT)!!
        //return  intent.getSerializableExtra(AppConstants.CONTENT) as Content
    }

    fun getContent(arguments: Bundle?): Content {
        //return  intent.getParcelableExtra(AppConstants.CONTENT)!!
        return arguments?.getParcelable<Content>(AppConstants.CONTENT) as Content
    }

    fun getCurrentLocale(): String {
        //en- english / hi- hindi
        return ConfigurationCompat.getLocales(context.resources.configuration)[0].language
    }


    fun setAvatarImage(context: Context, ivAvatar: ImageView, avatarPosition: String?) {
        if (!avatarPosition.isNullOrEmpty()) {
            if (avatarPosition == "ic_avatar1" || avatarPosition == "ic_avatar2" ||
                avatarPosition == "ic_avatar3" || avatarPosition == "ic_avatar4" ||
                avatarPosition == "ic_avatar5"
            ) {
                Glide.with(context).load(ContantUtils.mImgIds[listOf(0, 1, 2, 3, 4).random()])
                    .into(ivAvatar)
            } else {
                if (avatarPosition.contains("svg", true)) {
                    GlideToVectorYou.init().with(context).load(Uri.parse(avatarPosition), ivAvatar)
                } else {
                    Glide.with(context).load(avatarPosition).into(ivAvatar)
                }
            }
        } else {
            Glide.with(context).load(ContantUtils.mImgIds[listOf(0, 1, 2, 3, 4).random()])
                .into(ivAvatar)
        }
    }

    fun setBackgroundImage(url: String, imageView: ImageView, blurImageView: BlurImageView) {
        Glide.with(context)
            .load(url)
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.banner_placeholder)
            .into(imageView)

        Glide.with(context)
            .load(url)
            .placeholder(R.drawable.blur_placeholder_detail_fragment)
            .transform(BlurTransformation(context))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(blurImageView)
        blurImageView.setBitmapScale(0.3f)
        blurImageView.setBlur(20)
    }

    fun setBlurBackgroundImage(imageView: ImageView?) {
        if (imageView != null) {
            imageView.visibility = View.GONE
        }
    }

    fun setTestBlurBackgroundImage(url: String, imageViewBlur: BlurImageView) {
        Glide.with(context)
            .load(url)
            .placeholder(R.drawable.blur_placeholder_detail_fragment)
            .transform(BlurTransformation(context))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(imageViewBlur)
        imageViewBlur.setBlur(24)
        imageViewBlur.alpha = 0.4f
    }

    fun getClassFromExam(exam: String?): String {
        var examString = ""
        exam?.forEach {
            if (Character.isDigit(it)) {
                examString += it.toString()
            }
        }

        return if (examString.isEmpty()) // this means there is no class in exam and exam contains jee, neet etc.
            exam.toString()
        else
            examString
    }

    fun getRound(value: Double): String {
        val rounded = floor(value + 0.5)
        return rounded.toInt().toString()
    }

    fun getCooboJSONObject() = currentJSONObject

    fun setCooboJSONObject(cooboJSON: String) {
        currentJSONObject = cooboJSON
    }

    fun loadJSONFromAsset(context: Context, fileName: String): String {
        var json: String? = null
        json = try {
            val `is` =
                Objects.requireNonNull<InputStream>(context.assets.open(fileName))
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer, StandardCharsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return ""
        }
        return json
    }

    fun getVimeoHD(jsonArray: JSONArray): String {
        val type: Type = object : TypeToken<List<VimeoVideo>>() {}.type
        val vimeoVideoList: List<VimeoVideo> = Gson().fromJson(jsonArray.toString(), type)
        val hdVideoList = vimeoVideoList.filterList { quality == "hd" }
        val sdVideoList = vimeoVideoList.filterList { quality == "sd" }
        return if (hdVideoList.isNullOrEmpty()) {
            sdVideoList.sortedByDescending { it.width }[0].link
        } else {
            hdVideoList.sortedByDescending { it.width }[0].link
        }
    }

    fun capitalize(capString: String): String? {
        val capBuffer = StringBuffer()
        val capMatcher: Matcher =
            Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString)
        while (capMatcher.find()) {
            capMatcher.appendReplacement(
                capBuffer,
                capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase()
            )
        }
        return capMatcher.appendTail(capBuffer).toString()
    }

    fun changeBannerTitleTextSize(tv: TextView) {
        if (tv.lineCount >= 2) {
            tv.textSize = 26f
        } else {
            tv.textSize = 32f
        }
    }

    fun getDrawableByAttemptType(attemptType: String): Int {
        return when (attemptType) {
            Perfect.name -> R.drawable.perfect_attempt_qwa_bg
            TooFastCorrect.name, OvertimeCorrect.name -> R.drawable.green_qwa_bg
            Incorrect.name, OvertimeIncorrect.name, Wasted.name -> R.drawable.red_qwa_bg
            else -> 0
        }
    }

    fun getIconByAttemptType(attemptType: String): Int {
        return when (attemptType) {
            Perfect.name -> R.drawable.ic_perfect_attempt
            OvertimeCorrect.name -> R.drawable.ic_overtime_correct
            TooFastCorrect.name -> R.drawable.ic_too_fast_correct
            Incorrect.name -> R.drawable.ic_incorrect
            OvertimeIncorrect.name -> R.drawable.ic_overtime_incorrect
            Wasted.name -> R.drawable.ic_wasted
            NonAttempt.name -> R.drawable.ic_unattempted
            else -> 0
        }
    }

    fun getIconByAttemptTypeNew(attemptType: String): Int {
        return when (attemptType) {
            Perfect.name -> R.drawable.ic_perfect_attempt_circled
            OvertimeCorrect.name -> R.drawable.ic_overtime_correct_circled
            TooFastCorrect.name -> R.drawable.ic_too_fast_correct_circled
            Incorrect.name -> R.drawable.ic_incorrect_circled
            OvertimeIncorrect.name -> R.drawable.ic_overtime_incorrect_circled
            Wasted.name -> R.drawable.ic_wasted_circled
            NonAttempt.name -> R.drawable.ic_unattempted_circled
            else -> 0
        }
    }

    fun getDrawableByTitle(title: String): Int {
        return when (title) {
            AppConstants.TYPE_CHAPTERS_YOU_GOT_RIGHT -> R.drawable.chapters_right
            AppConstants.TYPE_CHAPTERS_YOU_GOT_WRONG -> R.drawable.chapters_wrong
            AppConstants.TYPE_CHAPTERS_YOU_DID_NOT_ATTEMPT -> R.drawable.chapters_not_attempted
            AppConstants.TYPE_STRONG_CHAPTERS_YOU_GOT_WRONG -> R.drawable.strong_chapters_wrong
            AppConstants.TYPE_STRONG_CHAPTERS_YOU_DID_NOT_ATTEMPT -> R.drawable.strong_chapters_not_attempted
            else -> R.drawable.chapters_wrong
        }
    }

    fun getAttemptFromKey(key: String): String {
        val map = HashMap<String, String>()
        map[Perfect.name] = AppConstants.PERFECT_ATTEMPTS
        map[OvertimeCorrect.name] = AppConstants.OVERTIME_CORRECT_ATTEMPTS
        map[TooFastCorrect.name] = AppConstants.TOO_FAST_CORRECT_ATTEMPTS
        map[Incorrect.name] = AppConstants.INCORRECT_ATTEMPTS
        map[OvertimeIncorrect.name] = AppConstants.OVERTIME_INCORRECT_ATTEMPTS
        map[Wasted.name] = AppConstants.WASTED_ATTEMPTS
        map[NonAttempt.name] = AppConstants.UNATTEMPTED_ATTEMPTS
        return map[key]!!
    }

    fun getAttemptBadge(attemptType: String): Int {
        when (attemptType) {
            Perfect.name
            -> {
                return R.drawable.ic_perfect_attempt_circled
            }
            TooFastCorrect.name
            -> {
                return R.drawable.ic_too_fast_correct_circled
            }
            Wasted.name
            -> {
                return R.drawable.ic_wasted_circled
            }
            OvertimeCorrect.name
            -> {
                return R.drawable.ic_overtime_correct_circled
            }
            OvertimeIncorrect.name
            -> {
                return R.drawable.ic_overtime_incorrect_circled
            }
            Incorrect.name
            -> {
                return R.drawable.ic_incorrect_circled
            }
            else -> {
                return R.drawable.ic_unattempted_circled
            }
        }
    }

    inline fun <reified T> fromJson(json: String): T {
        return Gson().fromJson(json, object : TypeToken<T>() {}.type)
    }

    fun startYoutubeApp(context: Context, url: String/* = ""*/) {
        val intent = Intent(context, YouTubePlayerActivity::class.java)
        intent.putExtra(AppConstants.VIDEO_URL, url)
        context.startActivity(intent)
    }

    fun isYouTubeAppAvailable(context: Context): Boolean {
        val pm = context.packageManager
        val launchIntent = pm.getLaunchIntentForPackage(AppConstants.YOUTUBE_PACKAGE_NAME)
        return launchIntent != null
    }

    fun getRoundedAccuracy(accuracy: Double): Double {
        return accuracy.toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
    }

    /*private fun startVideoPlayer(
        videoUrl: String,
        videoId: String,
        videoConceptId: String,
        contentType: String,
        videoTitle: String,
        videoDescription: String,
        videoSubject: String,
        videoChapter: String,
        videoAuthors: String,
        videoType: String,
        videoTotalDuration: String,
        topicLearnPath: String
    ) {
        val intent = Intent(App.context, VideoPlayerActivity::class.java)
        intent.putExtra(AppConstants.VIDEO_URL, videoUrl)
        intent.putExtra(AppConstants.VIDEO_ID, videoId)
        intent.putExtra(AppConstants.VIDEO_CONCEPT_ID, videoConceptId)
        intent.putExtra(AppConstants.CONTENT_TYPE, contentType)
        intent.putExtra(AppConstants.VIDEO_TITLE, videoTitle)
        intent.putExtra(AppConstants.VIDEO_TOTAL_DURATION, videoTotalDuration)
        intent.putExtra(AppConstants.VIDEO_DESCRIPTION, videoDescription)
        intent.putExtra(AppConstants.VIDEO_SUBJECT, videoSubject)
        intent.putExtra(AppConstants.VIDEO_CHAPTER, videoChapter)
        intent.putExtra(AppConstants.VIDEO_AUTHORS, videoAuthors)
        intent.putExtra(AppConstants.VIDEO_SOURCE, videoType)
        intent.putExtra(AppConstants.TOPIC_LEARN_PATH, topicLearnPath)
        intent.putExtra(AppConstants.LEARNMAP_CODE, learningMap)
        intent.putExtra(AppConstants.IS_RECOMMENDATION_ENABLED, true)
        intent.putExtra(AppConstants.IS_EMBIBE_SYLLABUS, isEmbibeSyllabus)
        intent.putExtra(AppConstants.BOOK_ID, book_id)
        intent.putExtra(AppConstants.FORMAT_ID, format_id)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, learnPathName)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, learnPathFormatName)
        startActivity(intent)
        //activity?.finish()
    }*/

    fun readJSONFromAsset(jsonDataFileName: String, context: Context): String? {
        var json: String? = null
        try {
            val inputStream: InputStream = context.assets.open("$jsonDataFileName.json")
            json = inputStream.bufferedReader().use { it.readText() }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    //TODO: Need to remove later if not required
    fun getTruncatedString1(inputStr: String, totalAllowedCharCount: Int)
            : String {
        var updatedStr = ""
        try {
            if (inputStr.length > totalAllowedCharCount) {
                val sp: List<String> = inputStr.split(" ")
                if (sp.size > 3) {
                    updatedStr = inputStr.replace(sp[1], "...")
                    if (updatedStr.length > (totalAllowedCharCount + 2)) {
                        updatedStr = updatedStr.replace(sp[2], " ")
                    }
                    // TODO: need to check later
                    // special case
//                    if (updatedStr.length > (totalAllowedCharCount + 2))
//                        updatedStr = updatedStr.replace(sp[3], "")
                }
            } else {
                updatedStr = inputStr
            }
        } catch (ex: Exception) {
            updatedStr = inputStr
            Log.e(TAG, ex.message)
        }
        return updatedStr
    }

    fun getTruncatedString(inputStr: String, totalAllowedCharCount: Int): String? {
        val lastIndex: Int
        var updatedStr = ""
        var arrSplit = arrayOfNulls<String>(0)

        try {

        } catch (ex: Exception) {
            updatedStr = inputStr
        }
        if (inputStr.length > totalAllowedCharCount) {
            arrSplit = inputStr.split("\\s".toRegex()).toTypedArray()
            lastIndex =
                totalAllowedCharCount - arrSplit[arrSplit.size - 1]!!.length - 3 //minus 3 for dots
            if ((arrSplit[0].toString() + "..." + arrSplit[arrSplit.size - 1]).length > totalAllowedCharCount) {
                updatedStr = arrSplit[0].toString() + "..."
                //println(result)
                Log.d(TAG, updatedStr)
                return updatedStr
            }
            updatedStr = inputStr.substring(0, lastIndex)
            if (inputStr[lastIndex] != ' ') {
                updatedStr = updatedStr.substring(0, updatedStr.lastIndexOf(" "))
            }
        } else {
            //println(content)
            Log.d(TAG, updatedStr)
            return inputStr
        }
        //println(result)
        Log.d(TAG, updatedStr)
        updatedStr = updatedStr + "..." + arrSplit[arrSplit.size - 1]
        Log.d(TAG, "Final string::\n$updatedStr")
        Log.d(TAG, "Final String length::\n$updatedStr")
        return updatedStr
    }

    fun testFeedbackData(
        context: Context,
        testCode: String,
        testSkillsRes: TestSkillsRes,
        sentToFeedBack: (TestSkillsRes) -> Unit
    ) {
        /*val testViewModel =
            ViewModelProviders.of(context as FragmentActivity).get(TestViewModel::class.java)
        testViewModel.getTestFeedbackData(
            testCode,
            object : BaseViewModel.APIMergeCallBacks<TestFeedBackModel> {
                override fun onSuccess(model: TestFeedBackModel?) {
                    if (model?.testAttemptsRes != null)
                        DataManager.instance.setTestAttempts(Gson().toJson(model.testAttemptsRes))
                    if (model?.testQuestionResponse != null)
                        DataManager.instance.setTestQuestion(Gson().toJson(model.testQuestionResponse))
                    if (model?.testAchieveRes != null)
                        DataManager.instance.setTestAchieve(Gson().toJson(model.testAchieveRes))
                    if (model?.testQuestionSummaryRes != null)
                        DataManager.instance.setTestQuestionSummary(Gson().toJson(model.testQuestionSummaryRes))

                    sentToFeedBack.invoke(testSkillsRes)
                }

                override fun onFailed(code: Int, error: String, msg: String) {

                }
            })*/
    }
}