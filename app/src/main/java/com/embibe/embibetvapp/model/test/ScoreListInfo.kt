package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class ScoreListInfo() : Parcelable {
    @SerializedName("avgScore")
    var avgScore: Double = 0.0

    @SerializedName("cutOffScore")
    var cutOffScore: Int = 0

    @SerializedName("goodScore")
    var goodScore: Double = 0.0

    @SerializedName("grade")
    var grade: String? = null

    @SerializedName("maxScore")
    var maxScore: Double = 0.0

    @SerializedName("myScore")
    var myScore: Double = 0.0

    @SerializedName("sectionSummaryList")
    var sectionSummaryList: List<SectionSummary>? = null

    @SerializedName("tqs")
    var tqs: String? = null

    constructor(parcel: Parcel) : this() {
        avgScore = parcel.readDouble()
        cutOffScore = parcel.readInt()
        goodScore = parcel.readDouble()
        grade = parcel.readString()
        maxScore = parcel.readDouble()
        myScore = parcel.readDouble()
        tqs = parcel.readString()
        sectionSummaryList =
            parcel.readArrayList(SectionSummary::class.java.classLoader) as ArrayList<SectionSummary>
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(avgScore)
        parcel.writeInt(cutOffScore)
        parcel.writeDouble(goodScore)
        parcel.writeString(grade)
        parcel.writeDouble(maxScore)
        parcel.writeDouble(myScore)
        parcel.writeString(tqs)
        parcel.writeList(sectionSummaryList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ScoreListInfo> {
        override fun createFromParcel(parcel: Parcel): ScoreListInfo {
            return ScoreListInfo(parcel)
        }

        override fun newArray(size: Int): Array<ScoreListInfo?> {
            return arrayOfNulls(size)
        }
    }
}