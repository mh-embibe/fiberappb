package com.embibe.embibetvapp.model

data class MenuName(var name: Int, var nameString: String)
