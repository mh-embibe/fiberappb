package com.embibe.embibetvapp.model.createtest.examconfig.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubjectData(
    val entity_codes: List<EntityCode>,
    val name: String,
    val selected: Boolean,
    val sequence: Int
) : Parcelable
