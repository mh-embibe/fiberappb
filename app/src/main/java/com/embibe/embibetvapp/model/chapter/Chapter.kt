package com.embibe.embibetvapp.model.chapter

import com.google.gson.annotations.SerializedName


data class Chapter(

    @SerializedName("_id") var _id: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("lpcode") var lpcode: String? = null,
    @SerializedName("has_concepts") var has_concepts: Boolean? = null,
    @SerializedName("practice") var practice: List<String>? = null,
    @SerializedName("description") var description: String? = null,
    @SerializedName("learn_tile_image") var learn_tile_image: String? = null,
    @SerializedName("practice_tile_image") var practice_tile_image: String? = null,
    var selectedState: Boolean? = false

)