package com.embibe.videoplayer.model

import android.widget.ImageView

class HexagonalModel {

    var id = 0
    var x = 0f
    var y = 0f
    var type = 0
    var isHighlight = false
    var alpha = 1F
    lateinit var view: ImageView
    var moveToX = 0f
    var moveToY = 0f
}