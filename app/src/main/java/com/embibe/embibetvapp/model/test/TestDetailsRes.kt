package com.embibe.embibetvapp.model.test


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


class TestDetailsRes() : Parcelable {

    @SerializedName("max_score")
    var max_score: String? = ""

    @SerializedName("tqs")
    var tqs: Double = 0.0
        get() = getDefaultDouble(field)

    @SerializedName("top_score")
    var topScore: Double = 0.0
        get() = getDefaultDouble(field)

    @SerializedName("good_score")
    var good_score: String? = ""

    @SerializedName("my_score")
    var my_score: String? = ""

    @SerializedName("avg_score")
    var avgScore: Double = 0.0
        get() = getDefaultDouble(field)

    @SerializedName("least_score")
    var leastScore: Double = 0.0
        get() = getDefaultDouble(field)

    private fun getDefaultDouble(field: Double): Double {
        return field
    }

    private fun getDefaultInt(field: Int): Int {
        return field
    }

    @SerializedName("cutoff_score")
    var cutoff_score: String? = ""

    constructor(parcel: Parcel) : this() {
        tqs = parcel.readDouble()
        topScore = parcel.readDouble()
        avgScore = parcel.readDouble()
        leastScore = parcel.readDouble()
        max_score = parcel.readString()
        good_score = parcel.readString()
        my_score = parcel.readString()
        avgScore = parcel.readDouble()
        cutoff_score = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(tqs)
        parcel.writeValue(topScore)
        parcel.writeValue(avgScore)
        parcel.writeValue(leastScore)
        parcel.writeString(max_score)
        parcel.writeString(good_score)
        parcel.writeString(my_score)
        parcel.writeValue(avgScore)
        parcel.writeString(cutoff_score)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TestDetailsRes> {
        override fun createFromParcel(parcel: Parcel): TestDetailsRes {
            return TestDetailsRes(parcel)
        }

        override fun newArray(size: Int): Array<TestDetailsRes?> {
            return arrayOfNulls(size)
        }
    }

}
