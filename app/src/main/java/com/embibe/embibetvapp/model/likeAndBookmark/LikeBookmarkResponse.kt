package com.embibe.embibetvapp.model.likeAndBookmark

class LikeBookmarkResponse {

    var success: Boolean = false
    var data: List<LikeBookmarkDetailModel>? = null
}