package com.embibe.embibetvapp.model.details

import com.google.gson.annotations.SerializedName


class Links {
    @SerializedName("parent")
    private val parent: Parent? = null

    @SerializedName("self")
    private val self: Self? = null

    @SerializedName("next")
    private val next: Next? = null

    @SerializedName("last")
    private val last: Last? = null
}
