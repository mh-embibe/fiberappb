package com.embibe.embibetvapp.model.completionSummary


import com.google.gson.annotations.SerializedName


class ImprovementBehaviour {
    @SerializedName("thumb_url")
    val thumbUrl: String? = null

    @SerializedName("behaviours")
    val behaviours: List<Behaviour>? = null
}