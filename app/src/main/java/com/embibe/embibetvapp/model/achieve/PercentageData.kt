package com.embibe.embibetvapp.model.achieve


import com.google.gson.annotations.SerializedName


class PercentageData {
    @SerializedName("to_grade")
    val toGrade: String? = null

    @SerializedName("from_grade")
    val fromGrade: String? = null

    @SerializedName("from_goal")
    val fromGoal: String? = null

    @SerializedName("to_goal")
    val toGoal: String? = null

    @SerializedName("percentage_connection")
    val percentageConnection: Double? = null

    @SerializedName("from_exam")
    val fromExam: String? = null

    @SerializedName("to_exam")
    val toExam: String? = null
}