package com.embibe.embibetvapp.model.DEAnalysis

import com.google.gson.annotations.SerializedName


class DEOverAllAccuracy : DEBaseResponse() {

    @SerializedName("social_accuracy")
    var socialAccuracy: Double? = null
        get() = field ?: 0.0

    @SerializedName("user_accuracy")
    var userAccuracy: Double? = null
        get() = field ?: 0.0
}