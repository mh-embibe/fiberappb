package com.embibe.embibetvapp.model.achieve.educationQualification

import com.embibe.embibetvapp.model.achieve.AchieveListBase
import com.google.gson.annotations.SerializedName

class EducationQualification  {
    @SerializedName("id")
    val id: Int? = null

    @SerializedName("name")
    val name: String? = null
}