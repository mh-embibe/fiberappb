package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable

class TestSkillsRes() : Parcelable {
    var answered: Int = 0
    var correct_answer: Int = 0
    var questions: Int = 0
    var skillset: List<SkillSet>? = null
    var top_skill: String? = null

    constructor(parcel: Parcel) : this() {
        answered = parcel.readInt()
        correct_answer = parcel.readInt()
        questions = parcel.readInt()
        top_skill = parcel.readString()
        skillset = parcel.readArrayList(SkillSet::class.java.classLoader) as ArrayList<SkillSet>
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(answered)
        parcel.writeInt(correct_answer)
        parcel.writeInt(questions)
        parcel.writeString(top_skill)
        parcel.writeTypedList(skillset)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TestSkillsRes> {
        override fun createFromParcel(parcel: Parcel): TestSkillsRes {
            return TestSkillsRes(parcel)
        }

        override fun newArray(size: Int): Array<TestSkillsRes?> {
            return arrayOfNulls(size)
        }
    }
}