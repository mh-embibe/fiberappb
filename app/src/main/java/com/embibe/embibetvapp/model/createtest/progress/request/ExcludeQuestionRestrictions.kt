package com.embibe.embibetvapp.model.createtest.progress.request


import com.google.gson.annotations.SerializedName

data class ExcludeQuestionRestrictions(
    @SerializedName("atg_ids")
    var atgIds: List<Any>?,
    @SerializedName("is_default")
    var isDefault: Boolean?,
    @SerializedName("previous_year")
    var previousYear: Boolean?,
    @SerializedName("x_month_tests")
    var xMonthTests: Boolean?,
    @SerializedName("x_value")
    var xValue: Int?
)