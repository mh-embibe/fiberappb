package com.embibe.embibetvapp.model.home

import com.embibe.embibetvapp.model.achieve.RankComparison.RankComparison
import com.embibe.embibetvapp.model.chapter.chapterLearningObjects.ChapterLearningObject
import com.embibe.embibetvapp.model.completionSummary.Topic
import com.embibe.embibetvapp.model.coobo.CooboApiRes
import com.embibe.embibetvapp.model.home.BooksResponse.BookRes
import com.embibe.embibetvapp.model.home.videos.VideosApiRes
import com.embibe.embibetvapp.model.likeAndBookmark.BookmarkedQuestion
import com.embibe.embibetvapp.model.practise.chapters.Chapter
import com.embibe.embibetvapp.model.test.*
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.newmodel.ResultsEntity

class HomeModel {

    lateinit var bookList: List<BookRes>
    lateinit var videosList: List<VideosApiRes>
    lateinit var cooboApiRes: CooboApiRes

    var home: ArrayList<Results>? = null
    var homePractice: ArrayList<Results>? = null


    var relatedConcepts: ArrayList<Results>? = null
    var moreConcepts: ArrayList<Results>? = null
    var topicsForChapter: ArrayList<Content>? = null
    var videosForChapter: ArrayList<ResultsEntity>? = null
    var videosForChapterV1: ArrayList<ChapterLearningObject>? = null
    var testsForChapter: ArrayList<Content>? = null
    var booksAvailable: ArrayList<Content>? = null
    var bookmarkedQuestions: ArrayList<BookmarkedQuestion>? = null
    var practicesForChapter: ArrayList<Content>? = null
    var prerequisitesForChapter: ArrayList<Content>? = null
    var relatedTopics: ArrayList<Content>? = null
    var videosForTopic: ArrayList<ResultsEntity>? = null
    var testsForTopic: ArrayList<Content>? = null
    var practicesForTopic: ArrayList<Content>? = null
    var prerequisitesForTopic: ArrayList<Content>? = null
    var quickLinks: List<String>? = arrayListOf()
    var searchResults: List<Results>? = arrayListOf()
    var recommendationLearning: ArrayList<Results>? = null
    var practiceTopicsForChapter: ArrayList<Content>? = null
    var searchSummaryResults: List<Content>? = arrayListOf()

    var recommendedLearningForTest: ArrayList<Results>? = null
    var recommendedPracticeForTest: ArrayList<Content>? = null
    var moreTests: ArrayList<Content>? = null

    lateinit var chaptersPhysics: List<Chapter>
    lateinit var chaptersChemistry: List<Chapter>
    lateinit var chaptersScience: List<Chapter>
    lateinit var chaptersMathematics: List<Chapter>
    lateinit var chaptersBiology: List<Chapter>


    var mTestQuestionResponse: TestQuestionResponse? = null
    var mTestAttemptsList: List<TestAttemptsRes>? = null
    var mTestAttempts: TestAttemptsRes? = null
    var mTestQuestionSummaryRes: TestQuestionSummaryRes? = null
    var mRankComparison: List<RankComparison>? = null
    var mBehaviours: List<ImproveStrategy>? = null
    var mTestAchieveRes: TestAchieveRes? = null
    var mTopicSummary: TestQuestionSummaryRes? = null
    var mAchiveTopicummarylist: List<Topic>? = null
}