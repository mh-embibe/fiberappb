package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class SkillSet() : Parcelable {
    @SerializedName("skill")
    var skill: String = ""

    @SerializedName("accuracy")
    var accuracy: Double = 0.0

    constructor(parcel: Parcel) : this() {
        skill = parcel.readString()!!
        accuracy = parcel.readDouble()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(skill)
        parcel.writeDouble(accuracy)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SkillSet> {
        override fun createFromParcel(parcel: Parcel): SkillSet {
            return SkillSet(parcel)
        }

        override fun newArray(size: Int): Array<SkillSet?> {
            return arrayOfNulls(size)
        }
    }
}