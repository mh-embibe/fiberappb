package com.embibe.embibetvapp.model.createtest.generateowntest.request


import com.google.gson.annotations.SerializedName

data class SectionData(
    @SerializedName("entity_codes")
    var entityCodes: List<EntityCode>?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("selected")
    var selected: Boolean?,
    @SerializedName("sequence")
    var sequence: Int?
)