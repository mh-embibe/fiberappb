package com.embibe.embibetvapp.model.achieve.home

import com.google.gson.annotations.SerializedName


data class AchieveHome(

	@SerializedName("section_id") val section_id: Int,
	@SerializedName("section_name") val section_name: String,
	@SerializedName("content") val content: List<Content>
)