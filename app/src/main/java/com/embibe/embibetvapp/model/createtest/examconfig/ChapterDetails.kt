package com.embibe.embibetvapp.model.createtest.examconfig

import android.os.Parcelable
import com.embibe.embibetvapp.model.createtest.examconfig.response.ChapterData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChapterDetails(
    var selectedState: Boolean? = false,
    var chapterName: String? = null,
    var chapterCode: String? = null,
    var sectionCode: String? = null,
    var subjectName: String? = null,
    var chapter_data: ChapterData? = null

) : Parcelable