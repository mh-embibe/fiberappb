package com.embibe.embibetvapp.model.detailModel

data class Detail(
    val results: ArrayList<DetailItemModel>
)