package com.embibe.embibetvapp.model.home

class TestForChapterRequest {
    var board: String? = null
    var child_id: String? = null
    var exam: String? = null
    var goal: String? = null
    var grade: String? = null
    var subject: String? = null
    var unit: String? = null
    var chapter_name: String? = null
}