package com.embibe.embibetvapp.model.achieve

import android.widget.ImageView
import android.widget.TextView
import com.embibe.embibetvapp.ui.custom.LineView

data class RelatedConceptView(
    val connectingLine: LineView,
    val connectingPoint: Pair<ImageView, TextView>,
    val relatedConcepts: ArrayList<Pair<LineView, Pair<ImageView, TextView>>>
)