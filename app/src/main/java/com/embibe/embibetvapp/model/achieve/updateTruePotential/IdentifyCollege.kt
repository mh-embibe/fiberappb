package com.embibe.embibetvapp.model.achieve.updateTruePotential

import com.google.gson.annotations.SerializedName




class IdentifyCollege {
    @SerializedName("present_class")
    val presentClass: String? = null

    @SerializedName("select_category")
    val selectCategory: String? = null

    @SerializedName("gender")
    val gender: String? = null

    @SerializedName("differenlty_abled")
    val differenltyAbled: String? = null

    @SerializedName("preparation_type")
    val preparationType: String? = null

    @SerializedName("state_domicile")
    val stateDomicile: String? = null
}