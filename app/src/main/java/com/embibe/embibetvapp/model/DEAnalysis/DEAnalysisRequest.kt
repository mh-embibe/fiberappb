package com.embibe.embibetvapp.model.DEAnalysis


import com.google.gson.annotations.SerializedName


class DEAnalysisRequest {
    @SerializedName("exam_code")
    var examCode: String? = null

    @SerializedName("level")
    var level: String? = null

    @SerializedName("content_code")
    var contentCode: String? = null

    @SerializedName("user_id")
    var userId: String? = ""
}