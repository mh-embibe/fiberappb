package com.embibe.embibetvapp.model.test

data class TestQuestionResponse(
    val paper: Paper
)