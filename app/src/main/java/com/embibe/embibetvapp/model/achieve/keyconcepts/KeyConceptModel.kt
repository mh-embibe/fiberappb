package com.embibe.embibetvapp.model.achieve.keyconcepts

import com.embibe.embibetvapp.model.practise.conceptSequence.Score
import com.google.gson.annotations.SerializedName


class KeyConceptModel {
    @SerializedName("concept_name")
    val conceptName: String? = null

    @SerializedName("related_concepts")
    val relatedConcepts: List<RelatedKeyConcept>? = null

    @SerializedName("concept_id")
    val conceptId: String? = null

    @SerializedName("score")
    val score: Score? = null

}