package com.embibe.embibetvapp.model

import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.EMBIBETOKEN
import com.embibe.embibetvapp.model.auth.login.LoginResponse
import com.embibe.embibetvapp.model.updateProfile.UpdateProfileRes
import com.embibe.embibetvapp.utils.ContantUtils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.model.LinkedProfile
import com.embibejio.coreapp.preference.PreferenceHelper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONObject


object UserData {
    var preferenceHelper = PreferenceHelper()

    fun updateLinkedUser(loginResponse: LoginResponse) {
        val linkedProfiles = ArrayList<LinkedProfile>()
        if (!loginResponse.linkedProfiles.isNullOrEmpty())
            linkedProfiles.addAll(loginResponse.linkedProfiles)
        linkedProfiles.add(createLoginLinkedProfile(loginResponse))
        PreferenceHelper().putDataModelList(
            AppConstants.LINKED_PROFILE,
            setProfilePicIfNot(linkedProfiles)
        )
    }

    private fun setProfilePicIfNot(linkedProfiles: ArrayList<LinkedProfile>): ArrayList<LinkedProfile> {
        val linkedProf = ArrayList<LinkedProfile>()
        for (profile in linkedProfiles) {
            if (profile.profilePic.isNullOrEmpty()) {
                profile.profilePic = ContantUtils.mImgIds[listOf(0, 1, 2, 3, 4).random()]
            } else {
                when (profile.profilePic) {
                    "ic_avatar1" -> profile.profilePic = ContantUtils.mImgIds[0]
                    "ic_avatar2" -> profile.profilePic = ContantUtils.mImgIds[1]
                    "ic_avatar3" -> profile.profilePic = ContantUtils.mImgIds[2]
                    "ic_avatar4" -> profile.profilePic = ContantUtils.mImgIds[3]
                    "ic_avatar5" -> profile.profilePic = ContantUtils.mImgIds[4]
                }
            }
            linkedProf.add(profile)
        }
        return linkedProfiles
    }

    fun getLinkedUser(): List<LinkedProfile?> {
        return getLinkedProfileList(AppConstants.LINKED_PROFILE)!!
    }

    //dummy
    fun updateUserID(userId: String) = PreferenceHelper().put(AppConstants.USER_ID, userId)

    fun getUserID() = PreferenceHelper()[AppConstants.USER_ID, ""].toString()

    fun setCurrentProfile(profile: LinkedProfile) {
        updateUserID(profile.userId)
        PreferenceHelper().put(AppConstants.CURRENT_PROFILE, Gson().toJson(profile).toString())
    }

    fun getCurrentProfile() = PreferenceHelper().getCurrentProfile(AppConstants.CURRENT_PROFILE)

    fun getEaxmSlugName() = DataManager.instance.getExamSlugNameByCode(
        getGoalCode(),
        getExamCode()
    )

    fun getGoalSlugName() = DataManager.instance.getGoalSlugNameByCode(getGoalCode())

    fun getGoalCode() = getGoalCodeWithPreferred()

    fun getExamCode() = getExamCodeWithPreferred()

    fun getGoal() = DataManager.instance.getGoalNameByCode(getGoalCode())

    fun getGrade() = getGradeWithPreferred()

    fun getBoard() = getCurrentProfile()?.board ?: ""

    fun getName() = getCurrentProfile()?.firstName ?: ""

    fun getChildId() = if (getCurrentProfile() == null) "" else getCurrentProfile()!!.userId

    fun getSecondaryGoalCode() = getCurrentProfile()?.secondary_exam_goal_id ?: ""

    fun getSecondaryExamCode() = getCurrentProfile()?.secondary_exams ?: ""

    internal fun getAddchildCount(): Int {
        val dataList = getLinkedUser()
        var addchildCount = 0
        for (item in dataList) {
            if (item!!.userType.equals("child") || item.userType.equals(
                    "student",
                    true
                )
            ) addchildCount++
        }
        return addchildCount
    }

    internal fun getAddParentCount(): Int {
        val dataList = getLinkedUser()
        var addParentCount = 0
        for (item in dataList) {
            if (item!!.userType.equals("parent")) addParentCount++
        }
        return addParentCount
    }

    fun getLinkedProfileList(): List<LinkedProfile?>? {
        val list = getLinkedProfileList(AppConstants.LINKED_PROFILE)
        val childList = ArrayList<LinkedProfile>()
        val parentList = ArrayList<LinkedProfile>()
        if (list != null) {
            for (item in list) {
                if (item != null) {
                    if ((item.userType == "child" || item.userType.equals(
                            "student",
                            true
                        )) && childList.size < 4
                    )
                        childList.add(item)
                    else if (item.userType == "parent" && parentList.size < 2)
                        parentList.add(item)
                }
            }
            childList.addAll(parentList)
            return childList
        }
        return childList
    }

    private fun getLinkedProfileList(key: String?): List<LinkedProfile?>? {
        val collectionType = object : TypeToken<Collection<LinkedProfile?>?>() {}.type
        return if (PreferenceHelper().preferences!!.contains(key)) Gson().fromJson<List<LinkedProfile>>(
            PreferenceHelper().preferences!!.getString(key, null), collectionType
        ) else null
    }

    fun setCurrentLoginUserType(userType: String) {
        PreferenceHelper().put(AppConstants.USER_TYPE, userType)
    }

    private fun createLoginLinkedProfile(loginResponse: LoginResponse): LinkedProfile {
        val currentUserLinkedProfile = LinkedProfile()
        currentUserLinkedProfile.userId = loginResponse.userId!!
        currentUserLinkedProfile.userType = loginResponse.userType
        currentUserLinkedProfile.embibe_token = PreferenceHelper()[EMBIBETOKEN, ""]
        currentUserLinkedProfile.firstName = loginResponse.firstName
        currentUserLinkedProfile.lastName = loginResponse.lastName
        currentUserLinkedProfile.email = loginResponse.email
        currentUserLinkedProfile.mobile = loginResponse.mobile
        currentUserLinkedProfile.grade = loginResponse.grade
        currentUserLinkedProfile.profilePic = loginResponse.profilePic
        currentUserLinkedProfile.primary_exam_code = loginResponse.primary_exam_code
        currentUserLinkedProfile.primary_goal_code = loginResponse.primary_goal_code
        currentUserLinkedProfile.secondary_exam_goal_id = loginResponse.secondary_exam_goal_id
        currentUserLinkedProfile.secondary_exams = loginResponse.secondary_exams
        currentUserLinkedProfile.board = loginResponse.board
        currentUserLinkedProfile.uid = loginResponse.uid
        currentUserLinkedProfile.isAchieve = loginResponse.isAchieve
        if (loginResponse.embibe_token.isNullOrEmpty())
            currentUserLinkedProfile.embibe_token = loginResponse.embibe_token
        return currentUserLinkedProfile
    }


    fun getChildForGoalSet(dataList: List<LinkedProfile?>): List<LinkedProfile> {
        val childLinkedProfileList: MutableList<LinkedProfile> = ArrayList()
        for (item in dataList) {
            if ((item!!.userType.equals("child") || item.userType.equals(
                    "student",
                    true
                )) && childLinkedProfileList.size < 4
            ) childLinkedProfileList.add(
                item
            )
        }
        return childLinkedProfileList
    }

    private fun getGoalCodeWithPreferred(): String {
        val preferredGoalForUser = preferenceHelper[getChildId() + "goal", ""]
        if (preferredGoalForUser.isNotEmpty()) {
            return preferredGoalForUser
        } else {
            setPrefGoal(getCurrentProfile()?.primary_goal_code ?: "")
        }
        return getCurrentProfile()?.primary_goal_code ?: ""
    }

    private fun getExamCodeWithPreferred(): String {
        val preferredGoalForUser = preferenceHelper[getChildId() + "exam", ""]
        if (preferredGoalForUser != "") {
            return preferredGoalForUser
        } else {
            setPrefExam(getCurrentProfile()?.primary_exam_code ?: "")
        }
        return getCurrentProfile()?.primary_exam_code ?: ""
    }

    private fun getGradeWithPreferred(): String {
        val preferredGradeForUser = preferenceHelper[getChildId() + "grade", ""]
        if (preferredGradeForUser != "") {
            return preferredGradeForUser
        } else {
            preferenceHelper.put(
                getChildId() + "grade",
                DataManager.instance.getExamByGoalCodeExamCode(
                    getGoalCodeWithPreferred(),
                    getExamCodeWithPreferred()
                )?.grade!!
            )
        }
        return DataManager.instance.getExamByGoalCodeExamCode(
            getGoalCodeWithPreferred(),
            getExamCodeWithPreferred()
        )?.grade!!
    }


    fun setUserPrefExamWithGoal(goalCode: String, examCode: String) {
        val preferredExam = JSONObject()
        preferredExam.put(goalCode, examCode)
        setPrefGoal(goalCode)
        setPrefExam(examCode)
        setPrefGrade(goalCode, examCode)
    }

    private fun setPrefGrade(goalCode: String, examCode: String) {
        val preGrade =
            DataManager.instance.getExamByGoalCodeExamCode(goalCode, examCode)?.grade ?: "10"
        preferenceHelper.put(getChildId() + "grade", preGrade)
    }

    private fun setPrefExam(examCode: String) {
        preferenceHelper.put(getChildId() + "exam", examCode)
    }

    private fun setPrefGoal(goalCode: String) {
        preferenceHelper.put(getChildId() + "goal", goalCode)
    }

    fun getExamName() = DataManager.instance.getExamNameByCode(
        getGoalCodeWithPreferred(),
        getExamCodeWithPreferred()
    )

    fun getAchieveOfCurrentUser() = getCurrentProfile()!!.isAchieve

    fun setCurrentUserAsAchiever() {
        val currentProfile = getCurrentProfile()!!
        currentProfile.isAchieve = true
        setCurrentProfile(currentProfile)
        val linkedProfile = getLinkedProfileList()!!
        val dummyLinkedProfile = arrayListOf<LinkedProfile>()
        for (profile in linkedProfile) {
            val linkedProf: LinkedProfile = profile!!
            if (profile.userId == currentProfile.userId) {
                linkedProf.isAchieve = true
            }
            dummyLinkedProfile.add(linkedProf)
        }
        PreferenceHelper().putDataModelList(
            AppConstants.LINKED_PROFILE, dummyLinkedProfile
        )
    }

    fun updateUserData(model: UpdateProfileRes, userId: String) {
        val linkedProfile = getLinkedProfileList()!!
        val dummyLinkedProfile = arrayListOf<LinkedProfile>()
        for (profile in linkedProfile) {
            val linkedProf: LinkedProfile = profile!!
            if (profile.userId == userId) {
                linkedProf.firstName = model.resource?.personal?.personalInfo?.firstName
                linkedProf.profilePic = model.resource?.profilePic
                linkedProf.primary_goal_code = model.resource?.goal?.goalInfo?.primaryGoal
                linkedProf.primary_exam_code = model.resource?.goal?.goalInfo?.primaryExamCode
                linkedProf.secondary_exam_goal_id = model.resource?.goal?.goalInfo?.secondaryGoal
                linkedProf.secondary_exams =
                    model.resource?.goal?.goalInfo?.secondaryExamsPreparingFor
            }
            dummyLinkedProfile.add(linkedProf)
        }
        PreferenceHelper().putDataModelList(
            AppConstants.LINKED_PROFILE, dummyLinkedProfile
        )
    }
}