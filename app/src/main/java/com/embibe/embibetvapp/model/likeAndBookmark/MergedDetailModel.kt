package com.embibe.embibetvapp.model.likeAndBookmark

import com.embibe.embibetvapp.model.content.ContentStatusResponse

class MergedDetailModel {
    var contentStatus: ContentStatusResponse? = null
    var like: LikeBookmarkResponse? = null
    var bookmark: LikeBookmarkResponse? = null
}