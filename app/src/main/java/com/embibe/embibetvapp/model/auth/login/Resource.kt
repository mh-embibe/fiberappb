package com.embibe.embibetvapp.model.auth.login

import com.embibejio.coreapp.model.LinkedProfile
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class Resource {
    @SerializedName("email")
    @Expose
    val email: String? = null

    @SerializedName("user_type")
    @Expose
    val userType: String? = null

    @SerializedName("uid")
    @Expose
    val uid: String? = null

    @SerializedName("mobile")
    @Expose
    val mobile: String? = null

    @SerializedName("klass")
    @Expose
    val klass: Int = 0

    @SerializedName("first_name")
    @Expose
    val firstName: Any = ""

    @SerializedName("last_name")
    @Expose
    val lastName: Any = ""

    @SerializedName("city")
    @Expose
    val city: Any = ""

    @SerializedName("exam_year")
    @Expose
    val examYear: Any = ""

    @SerializedName("gender")
    @Expose
    val gender: String? = null

    @SerializedName("verified_mobile")
    @Expose
    val verifiedMobile: Any = ""

    @SerializedName("verified_email")
    @Expose
    val verifiedEmail: Any = ""

    @SerializedName("primary_organization_id")
    @Expose
    val primaryOrganizationId: Any = ""

    @SerializedName("unlocked_custom_pack_ids")
    @Expose
    val unlockedCustomPackIds: Any = ""

    @SerializedName("embium_count")
    @Expose
    val embiumCount: Any = ""

    @SerializedName("state_id")
    @Expose
    val stateId: Int = 0

    @SerializedName("city_id")
    @Expose
    val cityId: Int = 0

    @SerializedName("applied_rankup")
    @Expose
    val appliedRankup: Any = ""

    @SerializedName("_board")
    @Expose
    val board: String? = null

    @SerializedName("primary_goal_code")
    @Expose
    val primaryGoalCode: String? = null

    @SerializedName("latitude")
    @Expose
    val latitude: Any = ""

    @SerializedName("longitude")
    @Expose
    val longitude: Any = ""

    @SerializedName("guardian_type")
    @Expose
    val guardianType: Any = ""

    @SerializedName("user_id")
    @Expose
    val userId: Int = 0

    @SerializedName("selected_goal_code")
    @Expose
    val selectedGoalCode: Any = ""

    @SerializedName("exam_code")
    @Expose
    val examCode: Any = ""

    @SerializedName("profile_pic")
    @Expose
    val profilePic: String? = null

    @SerializedName("school")
    @Expose
    val school: Any = ""

    @SerializedName("college")
    @Expose
    val college: Any = ""

    @SerializedName("grade")
    @Expose
    val grade: Any = ""

    @SerializedName("linked_profiles")
    @Expose
    val linkedProfiles: List<LinkedProfile>? = null

    @SerializedName("subc_status")
    @Expose
    val subcStatus: String? = null
}