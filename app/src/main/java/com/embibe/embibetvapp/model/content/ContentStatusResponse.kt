package com.embibe.embibetvapp.model.content

import com.embibe.embibetvapp.model.response.CommonApiResponse

class ContentStatusResponse : CommonApiResponse() {

    var data: ContentStatusModel? = null

}