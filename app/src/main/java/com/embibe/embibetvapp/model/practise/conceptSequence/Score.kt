package com.embibe.embibetvapp.model.practise.conceptSequence


import com.google.gson.annotations.SerializedName


class Score {
    @SerializedName("importance")
    val importance: Double? = null

    @SerializedName("difficulty_level")
    val difficultyLevel: Double? = null

    @SerializedName("length")
    val length: Double? = null

    @SerializedName("fundamental_score")
    val fundamentalScore: Double? = null

    @SerializedName("complexity_score")
    val complexityScore: Double? = null

    @SerializedName("oswaal_importance")
    val oswaalImportance: Double? = null

}