package com.embibe.embibetvapp.model.achieve.completedJourney

import com.embibe.embibetvapp.model.completionSummary.CompletionSummary
import com.google.gson.annotations.SerializedName

class CompletedJourney {
    @SerializedName("tests_summary")
    val testsSummary : List<TestSummary>? = null

    @SerializedName("achieve_summary")
    val achieve_summary : CompletionSummary? = null
}