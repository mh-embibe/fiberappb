package com.embibe.embibetvapp.model.achieve.home


import com.google.gson.annotations.SerializedName

data class Subject(
    @SerializedName("progress")
    var progress: Int?,
    @SerializedName("subject")
    var subject: String?
){
    override fun toString(): String {
        return "Subject(progress=$progress, subject=$subject)"
    }
}