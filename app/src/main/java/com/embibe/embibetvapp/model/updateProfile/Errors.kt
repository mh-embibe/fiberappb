package com.embibe.embibetvapp.model.updateProfile

import com.google.gson.annotations.SerializedName


class Errors {
    @SerializedName("email")
    var email: String? = null
    var profile: String? = null
    var mobile: String? = null
}