package com.embibe.embibetvapp.model.auth.signup

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class UserExistResponse {
    @SerializedName("success")
    @Expose
    val success: Boolean = false

    @SerializedName("status")
    @Expose
    val status: Int = 0

    @SerializedName("user_exists")
    @Expose
    var userExists: Boolean = false
}