package com.embibe.embibetvapp.model.achieve.readiness

import com.google.gson.annotations.SerializedName


class Readiness {
    @SerializedName("goal")
    val goal: String? = null

    @SerializedName("exam")
    val exam: String? = null

    @SerializedName("user_type")
    val userType: String? = null

    @SerializedName("predicted_grade")
    val predictedGrade: PredictedGrade? = null

    @SerializedName("projected_grade")
    val projectedGrade: ProjectedGrade? = null

}