package com.embibe.embibetvapp.model.updateProfile

import com.google.gson.annotations.SerializedName


class AcademicInfo {

    @SerializedName("primary_org_course_id")
    var primaryOrgCourseId: Any? = null

    @SerializedName("primary_org_type")
    var primaryOrgType: Any? = null

    @SerializedName("primary_org_state_id")
    var primaryOrgStateId: Any? = null

    @SerializedName("primary_org_city_id")
    var primaryOrgCityId: Any? = null

    @SerializedName("primary_org_name_id")
    var primaryOrgNameId: Any? = null
}