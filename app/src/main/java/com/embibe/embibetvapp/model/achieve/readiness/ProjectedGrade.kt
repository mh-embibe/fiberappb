package com.embibe.embibetvapp.model.achieve.readiness

import com.google.gson.annotations.SerializedName

class ProjectedGrade {
    @SerializedName("score")
    val score: Int? = null

    @SerializedName("grade")
    val grade: String? = null

    @SerializedName("subject_scores")
    val subjectScores: List<SubjectScore>? = null


    @SerializedName("status_url")
    val statusUrl: String? = null
}