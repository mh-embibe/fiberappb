package com.embibe.embibetvapp.model.achieve.updateTruePotential

import com.google.gson.annotations.SerializedName




class DreamCollege {
    @SerializedName("category")
    val category: String? = null

    @SerializedName("name")
    val name: String? = null
}