package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class RevisionList() : Parcelable {

    @SerializedName("display_name")
    var display_name: String? = ""

    @SerializedName("chapter_list")
    var chapter_list: List<String>? = null

    @SerializedName("chapter_list_data")
    var chapter_list_data: List<ChapterListData>? = null

    @SerializedName("thumb_url")
    var thumb_url: String? = ""

    constructor(parcel: Parcel) : this() {
        display_name = parcel.readString()
        chapter_list = parcel.readArrayList(String::class.java.classLoader) as ArrayList<String>
        chapter_list_data =
            parcel.readArrayList(ChapterListData::class.java.classLoader) as ArrayList<ChapterListData>
        thumb_url = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

        parcel.writeString(display_name)
        parcel.writeList(chapter_list)
        parcel.writeString(thumb_url)
        parcel.writeTypedList(chapter_list_data)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RevisionList> {
        override fun createFromParcel(parcel: Parcel): RevisionList {
            return RevisionList(parcel)
        }

        override fun newArray(size: Int): Array<RevisionList?> {
            return arrayOfNulls(size)
        }
    }
}