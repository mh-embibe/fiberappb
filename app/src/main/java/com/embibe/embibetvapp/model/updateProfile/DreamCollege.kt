package com.embibe.embibetvapp.model.updateProfile

import com.google.gson.annotations.SerializedName


class DreamCollege {
    @SerializedName("dream_colleges")
    var dreamColleges: Any? = null

    @SerializedName("dream_branches")
    var dreamBranches: Any? = null
}