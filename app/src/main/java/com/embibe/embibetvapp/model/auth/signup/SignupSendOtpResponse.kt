package com.embibe.embibetvapp.model.auth.signup

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SignupSendOtpResponse {
    @SerializedName("result")
    @Expose
    val result: String? = null
}