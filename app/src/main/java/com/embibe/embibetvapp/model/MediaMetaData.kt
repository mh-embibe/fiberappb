package com.embibe.embibetvapp.model

import com.google.gson.annotations.SerializedName

data class MediaMetaData(
    @SerializedName("user_name") val email: String, @SerializedName("password") val password: String
)
