package com.embibe.embibetvapp.model.achieve.readiness

import com.google.gson.annotations.SerializedName

class SubjectScore {
    @SerializedName("subject")
    val subject: String? = null

    @SerializedName("score")
    val score: Double? = null

}