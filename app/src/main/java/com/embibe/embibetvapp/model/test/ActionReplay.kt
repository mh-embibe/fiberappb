package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class ActionReplay() : Parcelable {
    @SerializedName("count")
    var count: Int = 0

    @SerializedName("embium")
    var embium: Int = 0

    constructor(parcel: Parcel) : this() {
        count = parcel.readInt()
        embium = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(count)
        parcel.writeInt(embium)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ActionReplay> {
        override fun createFromParcel(parcel: Parcel): ActionReplay {
            return ActionReplay(parcel)
        }

        override fun newArray(size: Int): Array<ActionReplay?> {
            return arrayOfNulls(size)
        }
    }
}

