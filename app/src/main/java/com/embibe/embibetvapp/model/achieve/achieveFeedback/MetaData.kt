package com.embibe.embibetvapp.model.achieve.achieveFeedback

data class MetaData(
    val lm_level: String?,
    val lm_level_code: String?,
    val objects: List<Data>,
    val params: Param?,
    val reason:  String?,
    val mode: String?,
    val title: String?,
    val progress_percent: Int?,
    val paj_step_path: String?,
    val language: String?,
    val call_id: Int?
)
