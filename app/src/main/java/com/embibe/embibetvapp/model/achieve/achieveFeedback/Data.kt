package com.embibe.embibetvapp.model.achieve.achieveFeedback


import com.embibe.embibetvapp.newmodel.Content
import com.google.gson.annotations.SerializedName


class Data {

    @SerializedName("status")
    val status: String = "open"

    @SerializedName("id")
    val id: Int? = null

    @SerializedName("type")
    val type: String? = null

    @SerializedName("completed")
    val completed: Boolean = false

    @SerializedName("content_meta")
    val contentMeta: Content? = null

    var practiceMeta: PracticeData? = null

    var progress: Int = 0
}