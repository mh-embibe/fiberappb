package com.embibe.embibetvapp.model.adduser

import com.google.gson.annotations.SerializedName

data class Exam(
    @SerializedName("code") val code: String,
    @SerializedName("name") val name: String,
    @SerializedName("slug") val slug: String,
    @SerializedName("old_id") val old_id: Int,
    @SerializedName("xpath") val xpath: String,
    @SerializedName("default") val default: Boolean,
    @SerializedName("supported") val supported: Boolean,
    @SerializedName("grade") val grade: String


)