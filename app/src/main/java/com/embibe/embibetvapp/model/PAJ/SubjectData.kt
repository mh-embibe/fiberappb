package com.embibe.embibetvapp.model.PAJ

data class SubjectData(
    val grade: String,
    val subjects: List<Subject>
)