package com.embibe.embibetvapp.model

import com.google.gson.annotations.SerializedName

data class Resource(
    @SerializedName("ab_version")
    val ab_version: Int,
    @SerializedName("applied_rankup")
    val applied_rankup: String,
    @SerializedName("board")
    val board: String,
    @SerializedName("city_id")
    val city_id: String,
    @SerializedName("dob")
    val dob: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("embium_count")
    val embium_count: String,
    @SerializedName("exam_code")
    val exam_code: String,
    @SerializedName("exam_year")
    val exam_year: String,
    @SerializedName("first_name")
    val first_name: String,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("goal_code")
    val goal_code: String,
    @SerializedName("grade")
    val grade: String,
    @SerializedName("highest_product")
    val highest_product: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("is_guest")
    val is_guest: Boolean,
    @SerializedName("jump_pack_id")
    val jump_pack_id: String,
    @SerializedName("last_name")
    val last_name: String,
    @SerializedName("mandatory_reset_password")
    val mandatory_reset_password: Boolean,
    @SerializedName("mobile")
    val mobile: String,
    @SerializedName("primary_organization_id")
    val primary_organization_id: String,
    @SerializedName("profile_pic")
    val profile_pic: String,
    @SerializedName("provider")
    val provider: String,
    @SerializedName("rankup_pack_id")
    val rankup_pack_id: String,
    @SerializedName("school")
    val school: String,
    @SerializedName("selected_goal_code")
    val selected_goal_code: String,
    @SerializedName("state_id")
    val state_id: String,
    @SerializedName("uid")
    val uid: String,
    @SerializedName("unlocked_custom_pack_ids")
    val unlocked_custom_pack_ids: String,
    @SerializedName("verified_email")
    val verified_email: String,
    @SerializedName("verified_mobile")
    val verified_mobile: String
)