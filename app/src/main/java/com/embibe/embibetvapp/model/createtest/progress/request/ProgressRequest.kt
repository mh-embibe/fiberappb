package com.embibe.embibetvapp.model.createtest.progress.request


import com.google.gson.annotations.SerializedName

data class ProgressRequest(
    @SerializedName("atg_id")
    var atgId: Int?,
    @SerializedName("create_mocktest_data")
    var createMocktestData: Boolean?,
    @SerializedName("created_at")
    var createdAt: String?,
    @SerializedName("difficulty_level")
    var difficultyLevel: String?,
    @SerializedName("duration")
    var duration: Int?,
    @SerializedName("exam_code")
    var examCode: String?,
    @SerializedName("exclude_question_restrictions")
    var excludeQuestionRestrictions: ExcludeQuestionRestrictions?,
    @SerializedName("ideal_time_level_to_finish")
    var idealTimeLevelToFinish: String?,
    @SerializedName("language")
    var language: String?,
    @SerializedName("last_modified_at")
    var lastModifiedAt: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("question_count")
    var questionCount: Int?,
    @SerializedName("request_id")
    var requestId: String?,
    @SerializedName("resource_type")
    var resourceType: String?,
    @SerializedName("source")
    var source: String?
)