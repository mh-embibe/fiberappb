package com.embibe.embibetvapp.model.likeAndBookmark

import com.google.gson.annotations.SerializedName


class Marks {
    @SerializedName("total_marks")
    val totalMarks: Int? = null

    @SerializedName("user_marks")
    val userMarks: Int? = null
}