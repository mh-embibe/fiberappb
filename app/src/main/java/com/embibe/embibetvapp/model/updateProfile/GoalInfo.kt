package com.embibe.embibetvapp.model.updateProfile

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class GoalInfo {
    @SerializedName("primary_goal")
    @Expose
    val primaryGoal: String? = null

    @SerializedName("primary_exam_code")
    @Expose
    val primaryExamCode: String? = null

    @SerializedName("previous_primary_goal")
    @Expose
    private val previousPrimaryGoal: String? = null

    @SerializedName("exams_preparing_for")
    @Expose
    private val examsPreparingFor: Any? = null

    @SerializedName("secondary_goal_code")
    @Expose
    val secondaryGoal: String? = null

    @SerializedName("secondary_exams_preparing_for")
    @Expose
    val secondaryExamsPreparingFor: String? = null

    @SerializedName("_category")
    @Expose
    private val category: Any? = null

    @SerializedName("physical_disability")
    @Expose
    private val physicalDisability: Boolean? = null

    @SerializedName("exam_year")
    @Expose
    private val examYear: Any? = null
}