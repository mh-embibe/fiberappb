package com.embibe.embibetvapp.model.createtest.generateowntest.request


import com.google.gson.annotations.SerializedName

data class ChapterData(

    @SerializedName("name")
    var name: String?,
    @SerializedName("no_of_questions")
    var noOfQuestions: Int?,
    @SerializedName("previous_year_weight_percent")
    var previousYearWeightPercent: Int?,
    @SerializedName("subject_name")
    var subjectName: String?,
    @SerializedName("unit_name")
    var unitName: String?
)