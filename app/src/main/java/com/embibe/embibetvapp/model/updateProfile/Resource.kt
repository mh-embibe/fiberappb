package com.embibe.embibetvapp.model.updateProfile

import com.google.gson.annotations.SerializedName


class Resource {
    @SerializedName("personal")
    var personal: Personal? = null

    @SerializedName("academic")
    var academic: Academic? = null

    @SerializedName("goal")
    var goal: Goal? = null

    @SerializedName("profile_pic")
    var profilePic: String? = null

    @SerializedName("non_updatable_columns")
    var nonUpdatableColumns: List<String>? = null

    @SerializedName("gold_embiums")
    var goldEmbiums: Any? = null

    @SerializedName("silver_embiums")
    var silverEmbiums: Any? = null
}