package com.embibe.embibetvapp.model.home.BooksResponse

import com.google.gson.annotations.SerializedName


class Book {

    @SerializedName("name")
    var name: String = ""

    @SerializedName("category")
    var category: String = ""


    @SerializedName("front_page_image_url")
    var frontPageImageUrl: String = ""

    @SerializedName("thumbnail_url")
    var thumbnailUrl: String = ""

    @SerializedName("book_id")
    var bookId: String = ""

    @SerializedName("description")
    var description: String = ""

    @SerializedName("authors")
    var authors: List<String> = arrayListOf()

    @SerializedName("embiums")
    var embiums: Double? = null
        get() = if (field == null) 0.0 else field

    @SerializedName("duration")
    var duration: String = ""
        get() = if (field == null || field.isEmpty()) "0" else field


    @SerializedName("rating")
    var rating: Double? = null

    @SerializedName("subtitle")
    var subtitle: String = ""

    @SerializedName("subject")
    var subject: String = ""

    @SerializedName("bg_url")
    var bg_url: String? =
        "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/bg/Chemistry_DP.png"


}