package com.embibe.embibetvapp.model


import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.embibe.embibetvapp.newmodel.Annotation

class AnnotationResponse : CommonApiResponse() {
    var data: List<Annotation> = arrayListOf()


}