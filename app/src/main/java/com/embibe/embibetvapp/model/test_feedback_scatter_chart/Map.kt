package com.embibe.videoplayer.model

import com.google.gson.annotations.SerializedName

class Map {

    @SerializedName("coordinates_list")
    val coordinatesList: List<CoordinateModel>? = null

    @SerializedName("scene_types")
    val sceneTypes: List<SceneTypeModel>? = null

    @SerializedName("scene_details")
    val sceneDetails: List<SceneDetailsModel>? = null
}