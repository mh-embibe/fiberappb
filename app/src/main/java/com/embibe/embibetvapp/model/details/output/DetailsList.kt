package com.embibe.embibetvapp.model.details.output

class DetailsList {

    lateinit var id: String
    lateinit var name: String
    lateinit var list: List<String>
}