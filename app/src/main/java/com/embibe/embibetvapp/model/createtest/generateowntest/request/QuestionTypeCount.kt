package com.embibe.embibetvapp.model.createtest.generateowntest.request


import com.google.gson.annotations.SerializedName

data class QuestionTypeCount(
    @SerializedName("FillInTheBlanks")
    var fillInTheBlanks: Int?,
    @SerializedName("SingleChoice")
    var singleChoice: Int?,
    @SerializedName("SubjectiveAnswer")
    var subjectiveAnswer: Int?,
    @SerializedName("TrueFalse")
    var trueFalse: Int?
)