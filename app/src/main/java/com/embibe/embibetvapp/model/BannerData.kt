package com.embibe.embibetvapp.model

import android.os.Parcel
import android.os.Parcelable
import com.embibe.embibetvapp.converter.OwnerInfoConverter
import com.embibe.embibetvapp.model.learning.OwnerInfo
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.LearningMap
import com.google.gson.annotations.SerializedName
import io.objectbox.annotation.Convert

data class BannerData(
    @SerializedName("id")
    var id: String = "",
    @SerializedName("title")
    var title: String = "",
    @SerializedName("image_url")
    var imgUrl: String = "",
    @SerializedName("video_url")
    var videoUrl: String = "",
    @SerializedName("description")
    var description: String = "",
    @SerializedName("title_image_url")
    var title_image_url: String = "",
    @SerializedName("social")
    var social: Boolean = false,
    @SerializedName("subject")
    var subject: String = "",
    @SerializedName("duration")
    var duration: String = "",
    @SerializedName("embium_coins")
    var embiumCoins: String = "",
    @SerializedName("button_text")
    var buttonText: String = "",
    @SerializedName("move_focus")
    var moveFocus: Boolean = false,
    @SerializedName("type")
    var type: String = "",
    @SerializedName("teaser_url")
    var teaser_url: String = "",
    @SerializedName("test_list")
    var testList: List<Content>,
    @SerializedName("learning_map")
    var learning_map: LearningMap = LearningMap(),
    @SerializedName("learnmap_id")
    var learnmap_id: String = "",
    var learnpath_name: String = "",
    var learnpath_format_name: String = "",
    @Convert(converter = OwnerInfoConverter::class, dbType = String::class)
    var owner_info: OwnerInfo = OwnerInfo(),
    @SerializedName("preview_url")
    var previewUrl: String = "https://visualintelligence.blob.core.windows.net/video-summary/prod/2763566/summary.webp"
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readByte() != 0.toByte(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readByte() != 0.toByte(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readArrayList(String.javaClass.classLoader) as ArrayList<Content>,
        parcel.readParcelable(LearningMap::class.java.classLoader)!!,
        parcel.readString() ?: "",
        owner_info = parcel.readParcelable(OwnerInfo::class.java.classLoader)!!,
        previewUrl = parcel.readString() ?: ""
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(title)
        parcel.writeString(imgUrl)
        parcel.writeString(videoUrl)
        parcel.writeString(description)
        parcel.writeString(title_image_url)
        parcel.writeByte(if (social) 1 else 0)
        parcel.writeString(subject)
        parcel.writeString(duration)
        parcel.writeString(embiumCoins)
        parcel.writeString(buttonText)
        parcel.writeByte(if (moveFocus) 1 else 0)
        parcel.writeString(type)
        parcel.writeString(teaser_url)
        parcel.writeList(testList)
        parcel.writeParcelable(learning_map, flags)
        parcel.writeString(learnmap_id)
        /*parcel.writeParcelable(owner_info, flags)*/
        parcel.writeString(previewUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BannerData> {
        override fun createFromParcel(parcel: Parcel): BannerData {
            return BannerData(parcel)
        }

        override fun newArray(size: Int): Array<BannerData?> {
            return arrayOfNulls(size)
        }
    }
}