package com.embibe.embibetvapp.model.auth

import com.google.gson.annotations.SerializedName

data class SignUpRequest(
    @SerializedName("first_name") val first_name: String,
    @SerializedName("user_name") val user_name: String,
    @SerializedName("password") val password: String,
    @SerializedName("profile_pic") val profile_pic: String

)


