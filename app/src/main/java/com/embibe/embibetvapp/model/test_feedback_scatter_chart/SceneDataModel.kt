package com.embibe.videoplayer.model

import com.google.gson.annotations.SerializedName

class SceneDataModel {
    @SerializedName("coordinate_points_id")
    val coordinatePointId: Int = 0

    @SerializedName("type")
    val type: String = ""

    @SerializedName("text")
    val text: String = ""

    @SerializedName("sub_text")
    val subText: String = ""

    @SerializedName("texts")
    val texts: List<TextModel> = arrayListOf()

    @SerializedName("jar_details")
    val jarDetails: List<JarModel>? = null
}