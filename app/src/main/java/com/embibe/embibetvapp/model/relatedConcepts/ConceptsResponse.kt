package com.embibe.embibetvapp.model.relatedConcepts

import com.embibe.embibetvapp.newmodel.Results
import com.google.gson.annotations.SerializedName


class ConceptsResponse {

    @SerializedName("success")

    val success: Boolean = false

    @SerializedName("data")

    val data: ArrayList<Results>? = null
}