package com.embibe.embibetvapp.model.achieve.completedJourney

import com.google.gson.annotations.SerializedName




class Attempt {
    @SerializedName("attempt_type")
    val attemptType: String? = null

    @SerializedName("attempted_time")
    val attemptedTime: Int? = null

    @SerializedName("first_look_time")
    val firstLookTime: Int? = null

    @SerializedName("last_look_time")
    val lastLookTime: Int? = null

    @SerializedName("total_time_spent")
    val totalTimeSpent: Int? = null

    @SerializedName("difficulty")
    val difficulty: Int? = null

    @SerializedName("question_code")
    val questionCode: String? = null

    @SerializedName("question_number")
    val questionNumber: Int? = null
}