package com.embibe.embibetvapp.model.response

import com.google.gson.annotations.SerializedName

open class CommonApiResponse {

    @SerializedName("success")
    var success: Boolean = false
        get() {
            return if (field != null) field else false
        }

    @SerializedName("message")
    var message: String? = null

}