package com.embibe.embibetvapp.model.diagnostic

import com.google.gson.annotations.SerializedName

data class TestDiagnosticRes(
    @SerializedName("improvement_plan")
    val improvementPlan: List<ReadninessData>,
    @SerializedName("life_success")
    val lifeSuccess: List<LifeSucces>,
    @SerializedName("syllabus_readiness")
    val syllabusReadiness: List<ReadninessData>
)