package com.embibe.embibetvapp.model.coobo

import com.embibe.embibetvapp.model.details.Links
import com.google.gson.annotations.SerializedName


class Coobo {

    @SerializedName("_id")

    val id: String? = null

    @SerializedName("type")

    val type: String? = null

    @SerializedName("modifier")

    val modifier: String? = null

    @SerializedName("status")

    val status: String? = null

    @SerializedName("author_id")

    val authorId: Int = 0

    @SerializedName("content")

    val content: CooboContent? = null

    @SerializedName("created_by")

    val createdBy: Int = 0

    @SerializedName("updated_by")

    val updatedBy: Int = 0

    @SerializedName("content_schema_version")

    val contentSchemaVersion: Int = 0

    @SerializedName("version")

    val version: Int = 0

    @SerializedName("tenant_id")

    val tenantId: Int = 0

    @SerializedName("_updated")

    val updated: String? = null

    @SerializedName("_created")

    val created: String? = null

    @SerializedName("iid")

    val iid: Int = 0

    @SerializedName("uuid")

    val uuid: String? = null

    @SerializedName("created_at")

    val createdAt: String? = null

    @SerializedName("updated_at")

    val updatedAt: String? = null

    @SerializedName("_etag")

    val etag: String? = null

    @SerializedName("_links")

    val links: Links? = null

    @SerializedName("_latest_version")

    val latestVersion: Int = 0

    @SerializedName("user_name")

    val userName: String? = null

    @SerializedName("subjects")
    val subjects: List<String> = arrayListOf()
}