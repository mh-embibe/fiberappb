package com.embibe.embibetvapp.model.createtest.progress.response


import com.google.gson.annotations.SerializedName

data class ProgressResponse(
    @SerializedName("fiber_atg_data")
    var fiberAtgData: FiberAtgData?,
    @SerializedName("progress")
    var progress: Int?,
    @SerializedName("status")
    var status: Any?,
    @SerializedName("success")
    var success: Boolean?
)