package com.embibe.embibetvapp.model.content

import com.google.gson.annotations.SerializedName

class ContentStatusRequest {

    var content_id: String = ""
    var content_type: String = ""
    var watched_duration: Int = 0
    var content_status: String = ""
    var topic_learnpath_name: String = ""
    var learnpath_name: String = ""
    var learnpath_format_name: String = ""
    var format_reference: String = ""

    @SerializedName("child_id")
    var childId: String = ""

    @SerializedName("concept_id")
    var concept_id: String = ""

    @SerializedName("length")
    var length: String = ""

}