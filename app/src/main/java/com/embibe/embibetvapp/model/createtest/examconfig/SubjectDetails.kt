package com.embibe.embibetvapp.model.createtest.examconfig

import android.os.Parcelable
import com.embibe.embibetvapp.model.createtest.examconfig.response.EntityCode
import kotlinx.android.parcel.Parcelize


@Parcelize
data class SubjectDetails(
    var subjectCode: String? = null,
    var subjectName: String? = null,
    var subjectIcon: Int? = null,
    var bgSubject: Int? = null,
    var selectedPosition: Boolean? = false,
    var count: Int? = 0,
    var entity_codes: List<EntityCode>? = null,
    var sequence: Int = 0

) : Parcelable