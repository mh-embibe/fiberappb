package com.embibe.embibetvapp.model.achieve.home


import com.google.gson.annotations.SerializedName

data class GradeReadiness(
    @SerializedName("exam")
    var exam: String?,
    @SerializedName("potential")
    var potential: Potential?,
    @SerializedName("predicted")
    var predicted: Predicted?
) {
    override fun toString(): String {
        return "GradeReadiness(exam=$exam, potential=$potential, predicted=$predicted)"
    }
}