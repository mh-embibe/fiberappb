package com.embibe.embibetvapp.model.likeAndBookmark


import com.google.gson.annotations.SerializedName


class LikeBookmarkRequest {

    @SerializedName("video_id")
    var videoId: String = ""

    @SerializedName("content_id")
    var contentId: String = ""

    @SerializedName("content_type")
    var contentType: String = ""

    @SerializedName("activity_type")
    var activityType: String = ""


    @SerializedName("liked")
    var liked: Boolean = false

    @SerializedName("status")
    var status: Boolean = false

    @SerializedName("bookmarked")
    var bookmarked: Boolean = false

    @SerializedName("timestamp")
    var timestamp: Long = 0

    @SerializedName("child_id")
    var childId: String = ""

    @SerializedName("content_category_type")
    var contentCategoryType: String? = null
/*
    @SerializedName("parent_id")
    var parentId: Int = 0*/

    /*@SerializedName("source")
    var source: Source? = null*/
}
