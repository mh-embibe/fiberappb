package com.embibe.embibetvapp.model.adduser

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class AddUserResponse {
    @SerializedName("success")
    @Expose
    val success: Boolean = false

    @SerializedName("message")
    @Expose
    val message: String = ""

    @SerializedName("child_id")
    @Expose
    val childId: Int = 0

    @SerializedName("status")
    @Expose
    val status: Int = 0

    @SerializedName("errors")
    @Expose
    val errors: Errors = Errors()


    inner class Errors {
        @SerializedName("parent")
        @Expose
        val parent: List<String> = arrayListOf()

        @SerializedName("mobile")
        @Expose
        val mobile: List<String> = arrayListOf()

    }
}