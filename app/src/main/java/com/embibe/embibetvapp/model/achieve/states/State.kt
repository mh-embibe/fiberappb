package com.embibe.embibetvapp.model.achieve.states

import com.google.gson.annotations.SerializedName




class State {
    @SerializedName("id")
    val id: Int? = null

    @SerializedName("name")
    val name: String? = null
}