package com.embibe.embibetvapp.model

data class MenuIcon(var icon: Int, var iconRes: Int)
