package com.embibe.embibetvapp.model.completionSummary

import com.google.gson.annotations.SerializedName


class CompletionSummary {
    @SerializedName("past_grade")
    val pastGrade: String? = null

    @SerializedName("current_grade")
    val currentGrade: String? = null

    @SerializedName("improvement_behaviour")
    val improvementBehaviour: ImprovementBehaviour? = null

    @SerializedName("improvement_knowledge")
    val improvementKnowledge: ImprovementKnowledge? = null
}