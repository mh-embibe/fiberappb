package com.embibe.embibetvapp.model.likeAndBookmark

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class LikeBookmarkDetailModel {

    @SerializedName("id")
     val id: String? = null

    @SerializedName("content_id")
    val contentId: String? = null

    @SerializedName("content_type")
    val contentType: String? = null

    @SerializedName("content_category_type")
    val contentCategoryType: Any? = null

    @SerializedName("activity_type")
    val activityType: String? = null

    @SerializedName("child_id")
    val childId: Int? = null

    @SerializedName("parent_id")
    val parentId: Int? = null

    @SerializedName("status")
    val status: Boolean = false

    @SerializedName("timestamp")
    val timestamp: String? = null

    /*@SerializedName("source")
    var source: Source? = null*/
}

