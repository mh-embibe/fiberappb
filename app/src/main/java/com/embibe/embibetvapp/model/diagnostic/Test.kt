package com.embibe.embibetvapp.model.diagnostic

data class Test(
    val bundle_id: String,
    val duration: Int,
    val questions: Int,
    val thumb_url: String,
    val title: String,
    val type: String,
    val xpath: String
)