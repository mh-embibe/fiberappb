package com.embibe.embibetvapp.model.updateProfile

import com.google.gson.annotations.SerializedName


class OtherSourceOfLearning {
    @SerializedName("secondary_org_type")
    val secondaryOrgType: Any? = null

    @SerializedName("secondary_org_state_id")
    val secondaryOrgStateId: Any? = null

    @SerializedName("secondary_org_city_id")
    val secondaryOrgCityId: Any? = null

    @SerializedName("secondary_org_name")
    val secondaryOrgName: Any? = null
}