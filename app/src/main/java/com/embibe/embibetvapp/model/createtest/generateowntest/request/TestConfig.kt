package com.embibe.embibetvapp.model.createtest.generateowntest.request


import com.google.gson.annotations.SerializedName

data class TestConfig(
    @SerializedName("algorithm")
    var algorithm: String?,
    @SerializedName("avg_difficulty_level")
    var avgDifficultyLevel: Double?,
    @SerializedName("avg_ideal_time")
    var avgIdealTime: Int?,
    @SerializedName("base_entity")
    var baseEntity: String?,
    @SerializedName("difficulty_level")
    var difficultyLevel: String?,
    @SerializedName("ignore_previous_year_tests")
    var ignorePreviousYearTests: String?,
    @SerializedName("maximum_difficulty_level")
    var maximumDifficultyLevel: Int?,
    @SerializedName("maximum_ideal_time")
    var maximumIdealTime: Int?,
    @SerializedName("minimum_difficulty_level")
    var minimumDifficultyLevel: Int?,
    @SerializedName("minimum_ideal_time")
    var minimumIdealTime: Int?,
    @SerializedName("mocktest_bundle")
    var mocktestBundle: String?,
    @SerializedName("no_of_subjects")
    var noOfSubjects: Int?,
    @SerializedName("no_of_tests")
    var noOfTests: Int?,
    @SerializedName("std_difficulty_level")
    var stdDifficultyLevel: Double?,
    @SerializedName("std_ideal_time")
    var stdIdealTime: Int?,
    @SerializedName("subject_question_count")
    val subject_question_count: Map<String, Int>?
)