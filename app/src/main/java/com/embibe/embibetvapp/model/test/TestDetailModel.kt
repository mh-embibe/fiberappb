package com.embibe.embibetvapp.model.test

import java.io.Serializable

class TestDetail(
    var title: String,
    var totalDuration: String,
    var totalQuestions: String,
    var totalMarks: String,
    var myScore: String,
    var goodScore: String,
    var cutOffScore: String,
    var tqs: String
) : Serializable