package com.embibe.embibetvapp.model.coobo

import com.google.gson.annotations.SerializedName


class Link {
    @SerializedName("self")
    val self: Self? = null
}