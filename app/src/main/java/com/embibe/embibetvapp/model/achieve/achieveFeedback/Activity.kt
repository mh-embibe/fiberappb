package com.embibe.embibetvapp.model.achieve.achieveFeedback


import com.google.gson.annotations.SerializedName


class Activity {

    @SerializedName("title")
    val title: String? = null

    @SerializedName("id")
    val id: Int? = null

    @SerializedName("duration")
    val duration: String? = null

    @SerializedName("type")
    val type: String? = null

    @SerializedName("thumb_url")
    val thumbUrl: String? = null

    @SerializedName("status")
    val status: String? = null

    @SerializedName("progress")
    val progress: Int? = null

    @SerializedName("accuracy")
    val accuracy: String? = null

    @SerializedName("concept_mastery")
    val concept_mastery: String? = null

    @SerializedName("meta")
    val meta :ArrayList<MetaItem>?=null
}