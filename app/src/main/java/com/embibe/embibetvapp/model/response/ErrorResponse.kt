package com.embibe.embibetvapp.model.response

import com.google.gson.annotations.SerializedName


data class ErrorResponse(
    @SerializedName("timestamp")

    val timestamp: String? = null,

    @SerializedName("status")

    val status: Int = 0,

    var code: Int = 0,

    @SerializedName("error")

    var error: String? = null,

    @SerializedName("message")

    var message: String? = "no response from server",

    @SerializedName("path")

    val path: String? = null
)

class Errors {
    @SerializedName("login")
    val login: List<String>? = null

    @SerializedName("otp")
    val otp: List<String>? = null
}