package com.embibe.embibetvapp.model.diagnostic.test

import com.embibe.embibetvapp.model.diagnostic.LifeSucces
import com.embibe.embibetvapp.model.diagnostic.Test
import com.google.gson.annotations.SerializedName

class DiagnosticTest {
    @SerializedName("improvement_plan")
    val improvementPlan: LifeSuccessPlan? = null

    @SerializedName("life_success")
    val lifeSuccess: LifeSuccessPlan? = null

    @SerializedName("syllabus_readiness")
    var syllabusReadiness: LifeSuccessPlan? = null

}