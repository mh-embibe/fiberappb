package com.embibe.embibetvapp.model.practise.conceptSequence


import com.google.gson.annotations.SerializedName


class QuestionCoverage {
    @SerializedName("attempted_questions")
    val attemptedQuestions: Int? = null

    @SerializedName("total_questions")
    val totalQuestions: Int? = null
}