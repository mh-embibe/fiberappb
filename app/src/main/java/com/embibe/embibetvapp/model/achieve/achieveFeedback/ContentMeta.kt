package com.embibe.embibetvapp.model.achieve.achieveFeedback

import com.google.gson.annotations.SerializedName




class ContentMeta {
    @SerializedName("mode")
    val mode: String? = null

    @SerializedName("title")
    val title: String? = null

    @SerializedName("progress_percent")
    val progressPercent: Int? = null

    @SerializedName("paj_step_path")
    val pajStepPath: String? = null

    @SerializedName("language")
    val language: String? = null
}