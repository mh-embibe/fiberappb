package com.embibe.embibetvapp.model.DEAnalysis

import com.google.gson.annotations.SerializedName


open class DEBaseResponse {

    @SerializedName("content_code")
    private val contentCode: String? = null

    @SerializedName("exam_code")
    private val examCode: String? = null

    @SerializedName("level")
    private val level: String? = null

    @SerializedName("user_id")
    private val userId: Int? = null

}