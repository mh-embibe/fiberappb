package com.embibe.embibetvapp.model.achieve.achieveFeedback

 class PracticeData() {
     var questionCode: String?=""
     var title: String?=""
     var progress_percent: Int=0
     var paj_step_path: String?=""
     var paj_step_path_index: Int?=0
     var call_id: Int?=0
     var paj_id: String?=""
     var paj_step_index: Int=0
 }
