package com.embibe.videoplayer.model

open class AxisLabelModel {
    var label: String = ""
    var isHighlighted: Boolean = false
}