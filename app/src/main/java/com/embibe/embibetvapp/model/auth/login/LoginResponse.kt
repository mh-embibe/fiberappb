package com.embibe.embibetvapp.model.auth.login

import com.embibe.embibetvapp.model.auth.Errors
import com.embibejio.coreapp.model.LinkedProfile
import com.google.gson.annotations.SerializedName

class LoginResponse {

    @SerializedName("success")
    var success: Boolean = false

    @SerializedName("errors")
    val error: Errors? = null

    @SerializedName("is_achieve")
    val isAchieve: Boolean = false

    @SerializedName("user_id")
    val userId: String? = null

    @SerializedName("uid")
    val uid: String? = null

    @SerializedName("first_name")
    val firstName: String? = null

    @SerializedName("last_name")
    val lastName: String? = null

    @SerializedName("exam_year")
    val exam_year: String? = null

    @SerializedName("primary_organization_id")
    val primary_organization_id: String? = null

    @SerializedName("selected_goal_code")
    val selected_goal_code: String? = null

    @SerializedName("primary_exam_code")
    val primary_exam_code: String? = null

    @SerializedName("primary_goal_code")
    val primary_goal_code: String? = null

    @SerializedName("primary_goal_id")
    val primary_goal_id: String? = null

    @SerializedName("secondary_goal_code")
    val secondary_exam_goal_id: String? = null

    @SerializedName("secondary_exams")
    val secondary_exams: String? = null

    @SerializedName("gender")
    val gender: String? = null

    @SerializedName("user_type")
    val userType: String? = null

    @SerializedName("email")
    val email: String? = null

    @SerializedName("mobile")
    val mobile: String? = null

    @SerializedName("unlocked_custom_pack_ids")
    val unlocked_custom_pack_ids: String? = null

    @SerializedName("board")
    val board: String? = null

    @SerializedName("grade")
    val grade: String? = null

    @SerializedName("city_id")
    val city_id: String? = null

    @SerializedName("state_id")
    val state_id: String? = null

    @SerializedName("latitude")
    val latitude: String? = null

    @SerializedName("longitude")
    val longitude: String? = null

    @SerializedName("linked_profiles")
    val linkedProfiles: List<LinkedProfile>? = null

    @SerializedName("profile_pic")
    val profilePic: String? = null

    @SerializedName("subc_status")
    val subcStatus: String? = null

    @SerializedName("embibe_token")
    var embibe_token: String? = null
}