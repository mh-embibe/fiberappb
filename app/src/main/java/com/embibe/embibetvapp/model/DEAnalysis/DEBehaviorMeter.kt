package com.embibe.embibetvapp.model.DEAnalysis

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class DEBehaviorMeter : DEBaseResponse() {


    @SerializedName("social_behaviour")
    @Expose
    var socialBehaviour: Behaviour? = null

    @SerializedName("user_behaviour")
    @Expose
    var userBehaviour: Behaviour? = null


}