package com.embibe.embibetvapp.model.DEAnalysis


import com.google.gson.annotations.SerializedName


class DEConceptCoverage : DEBaseResponse() {

    @SerializedName("user_concept_coverage")
    var userConceptCoverage: Int? = null
        get() = field ?: 0

    @SerializedName("social_concept_coverage")
    var socialConceptCoverage: Int? = null
        get() = field ?: 0

}