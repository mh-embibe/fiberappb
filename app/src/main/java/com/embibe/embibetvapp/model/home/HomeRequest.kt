package com.embibe.embibetvapp.model.home

import com.google.gson.annotations.SerializedName

class HomeRequest {
    var child_id: String = ""
    var grade: String? = null
    var goal: String? = null
    var board: String? = null
    var exam: String? = null
    var subject: String? = null
    var exam_name: String? = null
    var onlyPractise: Boolean? = null
        get() = field ?: false

    @SerializedName("content_section_type")
    var contentSectionType: String? = null
    var offset: Int? = null
    var size: Int? = null
}