package com.embibe.embibetvapp.model.home.videos

import com.google.gson.annotations.SerializedName

class VideosApiRes {

    @SerializedName("_id")
    var id: String = ""

    @SerializedName("details")
    var videos: List<VideoModel>? = null


}