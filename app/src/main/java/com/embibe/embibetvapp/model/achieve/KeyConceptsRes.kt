package com.embibe.embibetvapp.model.achieve

import com.embibe.embibetvapp.model.achieve.keyconcepts.KeyConceptModel

class KeyConceptsRes : StagingBaseApiResponse() {
    var data: ArrayList<KeyConceptModel>? = null
}