package com.embibe.embibetvapp.model.test

data class TestDetailModel(var image: Int, var value: String, var type: String)