package com.embibe.videoplayer.model

import com.google.gson.annotations.SerializedName

class TextModel {
    @SerializedName("id")
    val id: Int = 0

    @SerializedName("coordinate_points_id")
    val coordinatePointId: Int = 0

    @SerializedName("type")
    val type: String = ""

    @SerializedName("text")
    val text: String = ""
}