package com.embibe.videoplayer.model

class CoordinateModel {
    val id: Int = 0
    val x_axis: Int = 0
    val y_axis: Int = 0
}