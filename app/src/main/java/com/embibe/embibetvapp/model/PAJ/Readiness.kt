package com.embibe.embibetvapp.model.PAJ

data class Readiness(
    val exam: String,
    val potential: SubjectData,
    val predicted: SubjectData
)