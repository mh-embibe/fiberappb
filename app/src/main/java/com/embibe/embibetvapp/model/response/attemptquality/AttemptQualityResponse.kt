package com.embibe.embibetvapp.model.response.attemptquality

import com.google.gson.annotations.SerializedName

data class AttemptQualityResponse(
    @SerializedName("success") val success: Boolean,
    @SerializedName("data") val data: Data

)

