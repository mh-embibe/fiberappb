package com.embibe.embibetvapp.model.test

class TestKnowledgeImprovement(
    var title: String,
    var previous: String,
    var current: String,
    var content_thumb: String = ""
)