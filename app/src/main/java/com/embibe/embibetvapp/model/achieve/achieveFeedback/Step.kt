package com.embibe.embibetvapp.model.achieve.achieveFeedback


import com.google.gson.annotations.SerializedName


class Step {

    @SerializedName("title")
    val title: String? = null

    @SerializedName("status")
    val status: String? = null

    @SerializedName("current_grade")
    val currentGrade: String? = null

    @SerializedName("predicted_grade")
    val predictedGrade: String? = null

    @SerializedName("questions")
    val questions: String? = null

    @SerializedName("videos")
    val videos: String? = null

    @SerializedName("behaviours")
    val behaviours: String? = null

    @SerializedName("duration")
    val duration: String? = null

    @SerializedName("effort_rating")
    val effortRating: Double? = null

    @SerializedName("thumb_url")
    val thumbUrl: String? = null

    @SerializedName("progress")
    val progress: Int? = null

    @SerializedName("activities")
    val activities: List<Activity>? = null




}