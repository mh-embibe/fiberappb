package com.embibe.embibetvapp.model.coobo

import com.google.gson.annotations.SerializedName


class CooboContent {

    @SerializedName("category")
    var category: String? = null

    @SerializedName("slides")

    val slides: List<String> = arrayListOf()


    @SerializedName("slide_count")

    val slideCount: Int = 0

    @SerializedName("coobo_uuid")

    val cooboUuid: String? = null

    @SerializedName("title")

    val title: String? = null

    @SerializedName("description")

    val description: String? = null

    @SerializedName("thumb")

    val thumb: String? = null

    @SerializedName("bg_thumb")

    val bg_thumb: String? = null

    @SerializedName("slide_details")

    val slideDetails: List<SlideDetail>? = null
}