package com.embibe.embibetvapp.model.home.BooksResponse

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class BookId {
    @SerializedName("grade")
    @Expose
    private val grade: List<String> = arrayListOf()

    @SerializedName("subject")
    @Expose
    val subject: List<String> = arrayListOf()
}