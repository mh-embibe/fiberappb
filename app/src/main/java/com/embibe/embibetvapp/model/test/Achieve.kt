package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Achieve() : Parcelable {

    @SerializedName("metric")
    var metric: String? = ""

    @SerializedName("current")
    var current: String? = ""

    @SerializedName("potential")
    var potential: String? = ""

    constructor(parcel: Parcel) : this() {
        metric = parcel.readString()
        current = parcel.readString()
        potential = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(metric)
        parcel.writeString(current)
        parcel.writeString(potential)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Achieve> {
        override fun createFromParcel(parcel: Parcel): Achieve {
            return Achieve(parcel)
        }

        override fun newArray(size: Int): Array<Achieve?> {
            return arrayOfNulls(size)
        }
    }
}

