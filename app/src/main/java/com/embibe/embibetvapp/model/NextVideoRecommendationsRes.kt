package com.embibe.embibetvapp.model

import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.google.gson.annotations.SerializedName

class NextVideoRecommendationsRes : CommonApiResponse() {
    @SerializedName("data")
    val data: NextVideoRecommendations? = null
}