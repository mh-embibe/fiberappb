package com.embibe.embibetvapp.model.search

import com.google.gson.annotations.SerializedName


class Data {
    @SerializedName("quicklinks")
    val quicklinks: List<String> = arrayListOf()
}