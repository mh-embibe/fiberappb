package com.embibe.embibetvapp.model.practise.conceptSequence


import com.google.gson.annotations.SerializedName


class Achievement {
    @SerializedName("concept_mastery")
    val conceptMastery: Any? = null

    @SerializedName("perfect_attempts")
    val perfectAttempts: Int? = null
}