package com.embibe.embibetvapp.model.adduser

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AddUserRequest(
    @SerializedName("user_type") val user_type: String,
    @SerializedName("first_name") val first_name: String,
    @SerializedName("email") val email: String,
    @SerializedName("password") val password: String,
    @SerializedName("flag") val flag: String,
    @SerializedName("primary_goal_code") val primary_goal_code: String,
    @SerializedName("primary_exam_code") val primary_exam_code: String,
    @SerializedName("current_grade") val current_grade: String,
    @SerializedName("_board") val board: String,
    @SerializedName("gender") val gender: String,
    @SerializedName("profile_pic_file_name") val profile_pic: String,
    @SerializedName("mobile") val mobile: String? = null
) : Serializable

