package com.embibe.embibetvapp.model.learnSummary

import com.google.gson.internal.LinkedTreeMap

data class SincerietyRes(
    val `data`: LinkedTreeMap<Any,Any>?,
    val success: Boolean
)