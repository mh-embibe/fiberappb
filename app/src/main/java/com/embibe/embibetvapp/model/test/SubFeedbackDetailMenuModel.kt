package com.embibe.embibetvapp.model.test

class SubFeedbackDetailMenuModel(
    var title: String,
    var description: String,
    var accuracy: String,
    var topic_mastery: String,
    var type: String
)