package com.embibe.embibetvapp.model.achieve.paj

import com.google.gson.annotations.SerializedName

class PAJCreationRes {
    @SerializedName("paj_id")
    val pajId :String? =null
}