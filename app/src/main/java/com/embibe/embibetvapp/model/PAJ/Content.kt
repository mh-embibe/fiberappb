package com.embibe.embibetvapp.model.PAJ

data class Content(
    val actual_grade: String?,
    val behaviours: Int?,
    val bundle_id: String?,
    val completion_status: String?,
    val covered_steps: Int?,
    val effort_rating: Int?,
    val id: Int?,
    val duration: Double?,
    val thumb: String?,
    val subject_code: String?,
    val thumb_url: String?,
    val overall_progress: Int?,
    val potential_grade: String?,
    val post_test_xpath: String?,
    val post_test_bundle_id: String?,
    val previous_test_bunlde_id: String?,
    val previous_test_xpath: String?,
    val questions: Int?,
    val status: String?,
    val subject: String?,
    val title: String?,
    val total_steps: Int?,
    val type: String?,
    val videos: Int?,
    val xpath: String?
)