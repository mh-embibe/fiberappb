package com.embibe.embibetvapp.model.createtest.configuration


import com.google.gson.annotations.SerializedName

data class TestConfigurationRes(
    @SerializedName("correct_marks")
    var correctMarks: List<Int>?,
    @SerializedName("duration")
    var duration: List<Int>?,
    @SerializedName("incorrect_marks")
    var incorrectMarks: List<Int>?,
    @SerializedName("difficulty_level")
    var difficultyLevel: List<String>?
)