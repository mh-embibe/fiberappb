package com.embibe.embibetvapp.model.updateProfile

import com.google.gson.annotations.SerializedName


class PersonalInfo {
    @SerializedName("first_name")
    var firstName: String? = null

    @SerializedName("last_name")
    var lastName: Any? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("mobile")
    var mobile: String? = null

    @SerializedName("password")
    var password: Any? = null

    @SerializedName("country")
    var country: String? = null

    @SerializedName("bio")
    var bio: Any? = null

    @SerializedName("verified_email")
    var verifiedEmail: Any? = null

    @SerializedName("verified_mobile")
    var verifiedMobile: Any? = null
}