package com.embibe.embibetvapp.model.learning

import android.os.Parcel
import android.os.Parcelable

class OwnerInfo() : Parcelable {
    var copy_logo: String? = ""
    var copyright: String? = ""
    var key: String? = ""
    var teaser_key: String? = ""

    constructor(parcel: Parcel) : this() {
        copy_logo = parcel.readString()
        copyright = parcel.readString()
        key = parcel.readString()
        teaser_key = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(copy_logo)
        parcel.writeString(copyright)
        parcel.writeString(key)
        parcel.writeString(teaser_key)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OwnerInfo> {
        override fun createFromParcel(parcel: Parcel): OwnerInfo {
            return OwnerInfo(parcel)
        }

        override fun newArray(size: Int): Array<OwnerInfo?> {
            return arrayOfNulls(size)
        }
    }
}