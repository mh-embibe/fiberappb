package com.embibe.embibetvapp.model.achieve.home

import com.google.gson.annotations.SerializedName


data class Subjects(

    @SerializedName("subject") val subject: String,
    @SerializedName("progress") val progress: Int
)