package com.embibe.embibetvapp.model.search

import com.embibe.embibetvapp.newmodel.Results
import com.google.gson.annotations.SerializedName


class SearchResponse {

    @SerializedName("success")
    val success: Boolean = false

    @SerializedName("data")
    val data: Data? = null

    @SerializedName("message")
    val message: Any = ""

    @SerializedName("results")
    val results: List<Results>? = null
}