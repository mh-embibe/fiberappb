package com.embibe.embibetvapp.model.createtest.generateowntest.request


import com.google.gson.annotations.SerializedName

data class EntityCode(
    @SerializedName("difficulty_level")
    var difficultyLevel: String?,
    @SerializedName("difficulty_level_std")
    var difficultyLevelStd: Int?,
    @SerializedName("difficulty_order")
    var difficultyOrder: String?,
    @SerializedName("entity_name")
    var entityName: String?,
    @SerializedName("entity_type")
    var entityType: String?,
    @SerializedName("ideal_time_level")
    var idealTimeLevel: String?,
    @SerializedName("ideal_time_level_std")
    var idealTimeLevelStd: Int?,
    @SerializedName("question_count")
    var questionCount: Int?,
    @SerializedName("question_type_count")
    var questionTypeCount: QuestionTypeCount?,
    @SerializedName("question_types")
    var questionTypes: Int?
)