package com.embibe.embibetvapp.model.practise.conceptSequence


import com.google.gson.annotations.SerializedName


class ConceptBody {
    @SerializedName("achievement")
    val achievement: Achievement? = null

    @SerializedName("progress")
    val progress: Progress? = null
}