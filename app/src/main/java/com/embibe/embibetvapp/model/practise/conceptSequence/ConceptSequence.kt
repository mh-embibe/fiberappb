package com.embibe.embibetvapp.model.practise.conceptSequence


import com.google.gson.annotations.SerializedName


class ConceptSequence {

    @SerializedName("code")
    val code: String? = null

    @SerializedName("sequence")
    val sequence: Int? = null

    @SerializedName("display_name")
    val displayName: String? = null

    @SerializedName("score")
    val score: Score? = null

    @SerializedName("concept_body")
    val conceptBody: ConceptBody? = null
}