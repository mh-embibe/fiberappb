package com.embibe.embibetvapp.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class ResetPasswordResponse {
    @SerializedName("success")
    @Expose
    val success: Boolean = false

    @SerializedName("errors")
    @Expose
    val errors: Errors = Errors()

    inner class Errors {
        @SerializedName("token")
        @Expose
        private val token: String = ""
    }
}