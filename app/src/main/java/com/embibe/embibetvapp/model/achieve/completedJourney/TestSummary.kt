package com.embibe.embibetvapp.model.achieve.completedJourney

import android.os.Parcel
import android.os.Parcelable
import com.embibe.embibetvapp.model.test.ScoreListInfo
import com.embibe.embibetvapp.model.test.TestSkillsRes
import com.google.gson.annotations.SerializedName

class TestSummary() : Parcelable {
    @SerializedName("score")
    var score: ScoreListInfo? = null

    @SerializedName("skills")
    var skills: TestSkillsRes? = null

    @SerializedName("attempts")
    var attemptsSection: List<AttemptSection>? = null

    @SerializedName("xpath")
    var xpath: String? = null

    @SerializedName("bundle_id")
    var bundle_id: String? = null

    constructor(parcel: Parcel) : this() {
        score = parcel.readParcelable(ScoreListInfo::class.java.classLoader)
        skills = parcel.readParcelable(TestSkillsRes::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(score, flags)
        parcel.writeParcelable(skills, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TestSummary> {
        override fun createFromParcel(parcel: Parcel): TestSummary {
            return TestSummary(parcel)
        }

        override fun newArray(size: Int): Array<TestSummary?> {
            return arrayOfNulls(size)
        }
    }
}