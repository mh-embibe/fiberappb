package com.embibe.embibetvapp.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class SendOtpResponse : Serializable {
    @SerializedName("success")
    @Expose
    val success: Boolean = false

    @SerializedName("reset_password_via_otp")
    @Expose
    val resetPasswordViaOtp: Boolean = false

    @SerializedName("errors")
    @Expose
    var errors: Errors = Errors()

    inner class Errors {
        @SerializedName("Mobile")
        @Expose
        val mobile: String = ""

        @SerializedName("Email")
        @Expose
        val email: String = ""

        @SerializedName("otp")
        var otp: String = ""
    }
}