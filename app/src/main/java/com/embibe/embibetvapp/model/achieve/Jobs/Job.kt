package com.embibe.embibetvapp.model.achieve.Jobs

import com.google.gson.annotations.SerializedName

class Job {
    @SerializedName("id")
    val id: Int? = null

    @SerializedName("name")
    val name: String? = null
}