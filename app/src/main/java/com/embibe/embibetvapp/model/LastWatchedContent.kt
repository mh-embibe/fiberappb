package com.embibe.embibetvapp.model

import com.embibe.embibetvapp.newmodel.Content
import com.google.gson.annotations.SerializedName

class LastWatchedContent {

    @SerializedName("content")
    var content: List<Content>? = null

    @SerializedName("buttonType")
    val buttonType: String? = null
}