package com.embibe.embibetvapp.model.response.attemptquality

import com.google.gson.annotations.SerializedName


data class Data(

    @SerializedName("non_attempt") val non_attempt: AttemptData?,
    @SerializedName("normal_incorrect") val normal_incorrect: AttemptData?,
    @SerializedName("overtime_correct") val overtime_correct: AttemptData?,
    @SerializedName("overtime_incorrect") val overtime_incorrect: AttemptData?,
    @SerializedName("perfect_attempt") val perfect_attempt: AttemptData?,
    @SerializedName("too_fast_correct") val too_fast_correct: AttemptData?,
    @SerializedName("wasted_attempt") val wasted_attempt: AttemptData?,
    @SerializedName("recall") val recall: AttemptData?,
    @SerializedName("analytical_thinking") val analytical_thinking: AttemptData?,
    @SerializedName("conceptual_thinking") val conceptual_thinking: AttemptData?
)