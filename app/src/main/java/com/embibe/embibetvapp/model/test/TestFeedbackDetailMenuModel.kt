package com.embibe.embibetvapp.model.test

class TestFeedbackDetailMenuModel(
    var title: String,
    var subItems: ArrayList<SubFeedbackDetailMenuModel>
)