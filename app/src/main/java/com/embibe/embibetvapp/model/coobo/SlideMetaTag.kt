package com.embibe.embibetvapp.model.coobo

import com.google.gson.annotations.SerializedName


class SlideMetaTag {
    @SerializedName("difficulty_level")

    val difficultyLevel: String? = null

    @SerializedName("primary_concept")

    val primaryConcept: String? = null

    @SerializedName("ideal_time")

    val idealTime: String? = null

    @SerializedName("secondary_concept")

    val secondaryConcept: List<Any>? = null
}