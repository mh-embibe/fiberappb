package com.embibe.embibetvapp.model.updateProfile

import com.google.gson.annotations.SerializedName


class ParentInfo {
    @SerializedName("parent_name")
    private val parentName: Any? = null

    @SerializedName("parent_email")
    private val parentEmail: Any? = null

    @SerializedName("parent_mobile")
    private val parentMobile: Any? = null

    @SerializedName("alternate_mobile")
    private val alternateMobile: Any? = null
}