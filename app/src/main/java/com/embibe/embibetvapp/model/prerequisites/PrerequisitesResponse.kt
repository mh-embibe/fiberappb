package com.embibe.embibetvapp.model.prerequisites

import com.embibe.embibetvapp.newmodel.Results
import com.google.gson.annotations.SerializedName


class PrerequisitesResponse {

    @SerializedName("success")

    val success: Boolean = false

    @SerializedName("data")

    val data: Results? = null
}