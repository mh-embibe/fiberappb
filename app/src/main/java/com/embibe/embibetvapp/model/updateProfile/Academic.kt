package com.embibe.embibetvapp.model.updateProfile

import com.google.gson.annotations.SerializedName

class Academic {
    @SerializedName("academic_info")
    var academicInfo: AcademicInfo? = null

    @SerializedName("other_source_of_learning")
    var otherSourceOfLearning: OtherSourceOfLearning? = null

    @SerializedName("language_preference")
    var languagePreference: LanguagePreference? = null
}