package com.embibe.embibetvapp.model.chapter

import com.google.gson.annotations.SerializedName


data class ChapterResponse(

    @SerializedName("Chapter") var chapter: List<Chapter>? = null
)