package com.embibe.embibetvapp.model.content

import com.google.gson.annotations.SerializedName

class ContentStatusModel {

    @SerializedName("watched")
    val watched: Long = 0L

    @SerializedName("content_id")
    val contentId: String = ""

    @SerializedName("content_type")
    val contentType: String = ""

    @SerializedName("content_status")
    val contentStatus: String = ""

    @SerializedName("is_watched")
    val isWatched: Boolean = false

    @SerializedName("watched_duration")
    val watchedDuration: Long = 0L
}