package com.embibe.embibetvapp.model.DEAnalysis

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class DEAnalysisModel : Parcelable {

    var title: String? = null
    var accuracyModel: DEOverAllAccuracy? = null
    var conceptCoverageModel: DEConceptCoverage? = null
    var behaviourMerterModel: DEBehaviorMeter? = null
}