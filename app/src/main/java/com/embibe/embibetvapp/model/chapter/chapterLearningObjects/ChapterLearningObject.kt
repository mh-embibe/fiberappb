package com.embibe.embibetvapp.model.chapter.chapterLearningObjects

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName




class ChapterLearningObject() : Parcelable {
    @SerializedName("sectionName")
    val sectionName: String? = null

    @SerializedName("isFlat")
    val isFlat: Boolean? = null

    @SerializedName("currency")
    var currency: Int = 0
        get() = getDefaultInt(field)

    @SerializedName("sections")
    val sections: List<Section>? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ChapterLearningObject> {
        override fun createFromParcel(parcel: Parcel): ChapterLearningObject {
            return ChapterLearningObject(parcel)
        }

        override fun newArray(size: Int): Array<ChapterLearningObject?> {
            return arrayOfNulls(size)
        }
    }

    private fun getDefaultInt(field: Int): Int {
        return field
    }
}