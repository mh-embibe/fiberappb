package com.embibe.embibetvapp.model.details

import com.google.gson.annotations.SerializedName


data class Content(

    @SerializedName("description") val description: String = "",

    @SerializedName("thumb_image")
    val thumbImage: String = "",

    @SerializedName("tile_image")
    val tileImage: String = "",

    @SerializedName("bg_image")
    val bgImage: String = ""
)
