package com.embibe.embibetvapp.model.completionSummary

import com.google.gson.annotations.SerializedName


class Topic {
    @SerializedName("title")
    val title: String? = null

    @SerializedName("coverage")
    val coverage: String? = null

    @SerializedName("thumb")
    val thumb: String? = null

    @SerializedName("name")
    val name: String? = null
}