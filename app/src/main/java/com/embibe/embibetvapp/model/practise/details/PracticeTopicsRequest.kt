package com.embibe.embibetvapp.model.practise.details


import com.google.gson.annotations.SerializedName


class PracticeTopicsRequest {

    @SerializedName("learnmap_id")
    var learnmapId: String? = null

    @SerializedName("subject")
    var subject: String? = null
}