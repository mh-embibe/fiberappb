package com.embibe.embibetvapp.model

data class HeaderBannerData(
    var title: String,
    var rating: Float,
    var imgUrl: String,
    var videoUrl: String
)