package com.embibe.embibetvapp.model.practise.conceptSequence


import com.google.gson.annotations.SerializedName


class Progress {
    @SerializedName("question_coverage")
    val questionCoverage: QuestionCoverage? = null

    @SerializedName("video_coverage")
    val videoCoverage: VideoCoverage? = null
}