package com.embibe.embibetvapp.model.test


import com.google.gson.annotations.SerializedName


class TestReadiness {
    @SerializedName("readiness")
    val readiness: Double? = null
        get() = field ?: 0.0

    @SerializedName("success")
    val success: Boolean? = null

    @SerializedName("test_code")
    val testCode: String? = null

    @SerializedName("user_id")
    val userId: String? = null

}