package com.embibe.embibetvapp.model.achieve

data class UpdateAchieveRequest(
    val is_achieve: Boolean
)