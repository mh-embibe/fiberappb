package com.embibe.embibetvapp.model

import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.google.gson.annotations.SerializedName

class LastWatchedContentRes : CommonApiResponse() {
    @SerializedName("data")
    val data: LastWatchedContent? = null
}