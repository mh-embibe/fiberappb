package com.embibe.embibetvapp.model.diagnostic

data class LifeSucces(
    val goal: String,
    val section: String,
    val tests: List<ReadninessData>
)