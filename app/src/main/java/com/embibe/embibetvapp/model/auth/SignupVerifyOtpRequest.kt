package com.embibe.embibetvapp.model.auth

import com.google.gson.annotations.SerializedName

data class SignupVerifyOtpRequest(
    @SerializedName("otp") val otp: String,
    @SerializedName("id") val id: String
)