package com.embibe.embibetvapp.model.test


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


class Schedule() : Parcelable {

    var copy_logo: String? = ""
    var copyright: String? = ""

    @SerializedName("starts_at")
    var startsAt: String = ""
        get() = getDefaultString(field)

    @SerializedName("locks_at")
    var locksAt: String = ""
        get() = getDefaultString(field)

    @SerializedName("stops_at")
    var stopsAt: String = ""
        get() = getDefaultString(field)

    @SerializedName("solutions_public_at")
    var solutionsPublicAt: String = ""
        get() = getDefaultString(field)

    @SerializedName("live_status")
    var liveStatus: String = ""
        get() = getDefaultString(field)

    @SerializedName("is_result_available")
    var isResultAvailable: Boolean = false

    constructor(parcel: Parcel) : this() {
        copy_logo = parcel.readString() ?: ""
        copyright = parcel.readString() ?: ""
        startsAt = parcel.readString() ?: ""
        locksAt = parcel.readString() ?: ""
        stopsAt = parcel.readString() ?: ""
        solutionsPublicAt = parcel.readString() ?: ""
        liveStatus = parcel.readString() ?: ""
        isResultAvailable = parcel.readInt() == 1
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(copy_logo)
        parcel.writeString(copyright)
        parcel.writeString(startsAt)
        parcel.writeString(locksAt)
        parcel.writeString(stopsAt)
        parcel.writeString(solutionsPublicAt)
        parcel.writeString(liveStatus)
        parcel.writeInt(if (isResultAvailable) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Schedule> {
        override fun createFromParcel(parcel: Parcel): Schedule {
            return Schedule(parcel)
        }

        override fun newArray(size: Int): Array<Schedule?> {
            return arrayOfNulls(size)
        }
    }

    private fun getDefaultString(field: String): String {
        return field
    }

    private fun getDefaultInt(field: Int): Int {
        return field
    }

    private fun getDefaultDouble(field: Double): Double {
        return field
    }

}