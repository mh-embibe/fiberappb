package com.embibe.embibetvapp.model.chapter.chapterLearningObjects

import android.os.Parcel
import android.os.Parcelable
import com.embibe.embibetvapp.newmodel.Content
import com.google.gson.annotations.SerializedName




class Section() : Parcelable {
    @SerializedName("subSectionName")
    val subSectionName: String? = null

    @SerializedName("content")
    val content: List<Content>? = null

    @SerializedName("currency")
    var currency: Int = 0
        get() = getDefaultInt(field)

    constructor(parcel: Parcel) : this() {
    }

    private fun getDefaultInt(field: Int): Int {
        return field
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Section> {
        override fun createFromParcel(parcel: Parcel): Section {
            return Section(parcel)
        }

        override fun newArray(size: Int): Array<Section?> {
            return arrayOfNulls(size)
        }
    }
}