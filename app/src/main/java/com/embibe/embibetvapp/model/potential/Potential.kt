package com.embibe.embibetvapp.model.potential

import com.google.gson.annotations.SerializedName
import java.util.*


class Potential {

    @SerializedName("section_index")
    val sectionIndex: Int? = null

    @SerializedName("section")
    val section: String? = null

    @SerializedName("display_name")
    val displayName: String? = null

    @SerializedName("display_sub_text")
    val displaySubText: String? = null

    @SerializedName("options")
    val options: List<Option>? = null

    @SerializedName("choice_info")
    val choiceInfo: HashMap<String, Choice>? = null
}
