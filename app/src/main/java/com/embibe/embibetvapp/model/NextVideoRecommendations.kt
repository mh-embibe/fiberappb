package com.embibe.embibetvapp.model

import com.embibe.embibetvapp.newmodel.Results
import com.google.gson.annotations.SerializedName

class NextVideoRecommendations {
    @SerializedName("recommendations")
    var recommendations: List<Results>? = null
        get() = field ?: arrayListOf()

    @SerializedName("autoplay")
    var nextcontent: List<Results>? = null
        get() = field ?: arrayListOf()

    @SerializedName("autoplay_next_concept_id")
    var autoplayNextConceptId: String? = null
        get() = field ?: ""

}