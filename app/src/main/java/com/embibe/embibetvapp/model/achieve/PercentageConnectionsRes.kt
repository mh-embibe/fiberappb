package com.embibe.embibetvapp.model.achieve

class PercentageConnectionsRes : StagingBaseApiResponse() {
    var data: ArrayList<PercentageData>? = null
    var meta_data: QueryParams? = null
}