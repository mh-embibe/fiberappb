package com.embibe.embibetvapp.model.achieve

import com.google.gson.annotations.SerializedName

open class StagingBaseApiResponse {
    @SerializedName("success")
    var success: String = ""


}