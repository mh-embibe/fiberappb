package com.embibe.embibetvapp.model.auth.otp


import com.google.gson.annotations.SerializedName


class AuthOtpRes {

    @SerializedName("success")
    private val success: Boolean? = null

    @SerializedName("user_id")
    private val userId: Int? = null

    @SerializedName("uid")
    private val uid: String? = null

    @SerializedName("first_name")
    private val firstName: String? = null

    @SerializedName("last_name")
    private val lastName: String? = null

    @SerializedName("exam_year")
    private val examYear: Any? = null

    @SerializedName("primary_organization_id")
    private val primaryOrganizationId: Any? = null

    @SerializedName("selected_goal_code")
    private val selectedGoalCode: Any? = null

    @SerializedName("primary_exam_code")
    private val primaryExamCode: Any? = null

    @SerializedName("primary_goal_code")
    private val primaryGoalCode: Any? = null

    @SerializedName("primary_goal_id")
    private val primaryGoalId: Int? = null

    @SerializedName("secondary_goal_code")
    private val secondaryExamGoalId: Any? = null

    @SerializedName("secondary_exams")
    private val secondaryExams: Any? = null

    @SerializedName("gender")
    private val gender: Any? = null

    @SerializedName("user_type")
    private val userType: String? = null

    @SerializedName("email")
    private val email: String? = null

    @SerializedName("mobile")
    private val mobile: String? = null

    @SerializedName("unlocked_custom_pack_ids")
    private val unlockedCustomPackIds: Any? = null

    @SerializedName("profile_pic")
    private val profilePic: Any? = null

    @SerializedName("subc_status")
    private val subcStatus: String? = null

    @SerializedName("board")
    private val board: Any? = null

    @SerializedName("grade")
    private val grade: Any? = null

    @SerializedName("city_id")
    private val cityId: Any? = null

    @SerializedName("state_id")
    private val stateId: Any? = null

    @SerializedName("latitude")
    private val latitude: Any? = null

    @SerializedName("longitude")
    private val longitude: Any? = null
}