package com.embibe.embibetvapp.model.potential

import com.google.gson.annotations.SerializedName
import java.util.*


class Option {
    @SerializedName("type")
    val type: String? = null

    @SerializedName("type_title")
    val typeTitle: String? = null

    @SerializedName("action_required")
    val actionRequired: String? = null

    @SerializedName("choices")
    val choices: List<String>? = null

    @SerializedName("choice_info")
    val choiceInfo: HashMap<String, Choice>? = null

    @SerializedName("input_field_default")
    val inputFieldDefault: String? = null

    @SerializedName("input_field_choices")
    val inputFieldChoices: List<String>? = null
}