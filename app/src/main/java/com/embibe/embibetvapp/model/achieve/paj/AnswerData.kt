package com.embibe.embibetvapp.model.achieve.paj

import com.embibe.embibetvapp.model.achieve.updateTruePotential.TestDate
import com.google.gson.annotations.SerializedName


class AnswerData {
    @SerializedName("test_date")
    var testDate: TestDate? = null

    @SerializedName("available_time_per_day")
    var availableTimePerDay: AvailableTimePerDay? = null
}