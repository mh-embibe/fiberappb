package com.embibe.embibetvapp.model.test

import com.google.gson.annotations.SerializedName

class TestDetailRequest {
    var board: String? = null

    @SerializedName("test_code")
    var test_code: String? = null
    var exam: String? = null
    var goal: String? = null
    var grade: String? = null
    var unit: String? = null
    var subject: String? = null

    @SerializedName("test-type")
    var test_type: String? = null

}