package com.embibe.embibetvapp.model.recommendations

import com.embibe.embibetvapp.model.response.CommonApiResponse

class RecommendationsResponse : CommonApiResponse() {
    val data: Recommendations? = null
}