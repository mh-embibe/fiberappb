package com.embibe.embibetvapp.model.auth.otp

class AuthOTP {
    var login: String? = null
    var otp: String? = null
    var password: String? = null
    var fiber_user: Boolean = true
    var flag: String? = null
    var user_type: String? = null
    var first_name: String? = null
    var profile_pic_file_name: String? = null
}