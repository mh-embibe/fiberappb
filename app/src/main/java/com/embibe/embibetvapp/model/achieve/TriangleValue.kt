package com.embibe.embibetvapp.model.achieve

import android.graphics.PointF


data class TriangleValue(val trianglePosition: PointF, val triangleAngle: Float)