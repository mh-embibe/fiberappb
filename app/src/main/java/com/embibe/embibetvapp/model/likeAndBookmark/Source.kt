package com.embibe.embibetvapp.model.likeAndBookmark

import com.google.gson.annotations.SerializedName

class Source {
    @SerializedName("learn_path_name")
    var learnPathName: String? = null

    @SerializedName("source_id")
    var sourceId: String? = null

    @SerializedName("source_type")
    var sourceType: String? = null

    @SerializedName("source_name")
    var sourceName: String? = null
}