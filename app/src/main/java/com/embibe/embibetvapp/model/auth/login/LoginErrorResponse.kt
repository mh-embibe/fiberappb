package com.embibe.embibetvapp.model.auth.login

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class LoginErrorResponse {
    @SerializedName("success")
    @Expose
    val success: Boolean? = null

    @SerializedName("errors")
    @Expose
    val errors: Errors? = null

    inner class Errors {
        @SerializedName("password")
        @Expose
        val password: List<String>? = null

        @SerializedName("mobile")
        @Expose
        val mobile: List<String>? = null
    }
}