package com.embibe.embibetvapp.model.test

data class LearningMap(
    val chapter_code: String,
    val chapter_name: String,
    val code: String,
    val exam_code: String,
    val exam_name: String,
    val goal_code: String,
    val goal_name: String,
    val goal_slug: String,
    val level: String,
    val namespace: String,
    val subject_code: String,
    val subject_name: String,
    val unit_code: String,
    val unit_name: String
)