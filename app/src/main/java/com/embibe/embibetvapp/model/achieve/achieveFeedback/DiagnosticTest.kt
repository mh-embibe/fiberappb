package com.embibe.embibetvapp.model.achieve.achieveFeedback

import com.google.gson.annotations.SerializedName


class DiagnosticTest {

    @SerializedName("xpath")
    val xpath: String? = null

    @SerializedName("bundle_id")
    val bundleId: String? = null

    @SerializedName("thumb_url")
    val thumbUrl: String? = null

    @SerializedName("status")
    val status: String? = null

}