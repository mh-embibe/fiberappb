package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class TestAchieveRes() : Parcelable {

    @SerializedName("achieve")
    var achieve: Achieve? = null

    @SerializedName("revisions")
    var revisions: List<RevisionList>? = null

    @SerializedName("improve_strategy")
    var improveStrategy: ImproveStrategy? = null


    constructor(parcel: Parcel) : this() {
        achieve = parcel.readParcelable(Achieve::class.java.classLoader)
        revisions =
            parcel.readArrayList(RevisionList::class.java.classLoader) as ArrayList<RevisionList>
        improveStrategy = parcel.readParcelable(ImproveStrategy::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(achieve, flags)
        parcel.writeTypedList(revisions)
        parcel.writeParcelable(improveStrategy, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TestAchieveRes> {
        override fun createFromParcel(parcel: Parcel): TestAchieveRes {
            return TestAchieveRes(parcel)
        }

        override fun newArray(size: Int): Array<TestAchieveRes?> {
            return arrayOfNulls(size)
        }
    }
}