package com.embibe.embibetvapp.model

data class SubjectReadiness(
    var percentage: Int?,
    var progressBar: Int?,
    var subjectName: String?,
    var image: Int?
)