package com.embibe.embibetvapp.model.completionSummary


import com.google.gson.annotations.SerializedName


class ImprovementKnowledge {
    @SerializedName("thumb_url")
    val thumbUrl: String? = null

    @SerializedName("topics")
    val topics: List<Topic>? = null
}