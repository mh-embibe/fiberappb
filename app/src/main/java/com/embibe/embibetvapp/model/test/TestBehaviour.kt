package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class TestBehaviour() : Parcelable {

    @SerializedName("behavior_name")
    var behavior_name: String? = ""

    @SerializedName("description")
    var description: String? = ""

    @SerializedName("content_thumb")
    var content_thumb: String? = ""

    @SerializedName("content_type")
    var content_type: String? = ""

    @SerializedName("content_url")
    var content_url: String? = ""

    constructor(parcel: Parcel) : this() {
        behavior_name = parcel.readString()
        description = parcel.readString()
        content_thumb = parcel.readString()
        content_type = parcel.readString()
        content_url = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(behavior_name)
        parcel.writeString(description)
        parcel.writeString(content_thumb)
        parcel.writeString(content_type)
        parcel.writeString(content_url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TestBehaviour> {
        override fun createFromParcel(parcel: Parcel): TestBehaviour {
            return TestBehaviour(parcel)
        }

        override fun newArray(size: Int): Array<TestBehaviour?> {
            return arrayOfNulls(size)
        }
    }
}

