package com.embibe.embibetvapp.model.test_feedback_scatter_chart

import android.widget.ImageView

class XYModel {

    var id = 0
    var x = 0f
    var y = 0f
    var type = 0
    var isHighlight = false
    var alpha = 1F
    lateinit var view: ImageView
    lateinit var viewBg: ImageView
    lateinit var questionCode: String
}