package com.embibe.embibetvapp.model.test


import com.google.gson.annotations.SerializedName


class TestDetailsRequest {

    @SerializedName("child_id")
    var childId: String? = null

    @SerializedName("test_code")
    var testCode: String? = null

}