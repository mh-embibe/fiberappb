package com.embibe.embibetvapp.model.achieve.successStories

import com.google.gson.annotations.SerializedName


class SuccessStory {
    @SerializedName("background_url")
    val backgroundUrl: String? = null

    @SerializedName("content")
    val content: String? = null
}