package com.embibe.embibetvapp.model.auth

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class SignupVerifyOtpResponse {
    @SerializedName("result")
    @Expose
    val result: String? = null
}