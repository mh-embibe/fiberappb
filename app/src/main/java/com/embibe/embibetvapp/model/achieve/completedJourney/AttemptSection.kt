package com.embibe.embibetvapp.model.achieve.completedJourney

import com.google.gson.annotations.SerializedName

class AttemptSection {
    @SerializedName("section_name")
    val sectionName : String? = null
    @SerializedName("section_code")
    val sectionCode : String? = null
    @SerializedName("attempts")
    val attempts : List<Attempt>? = null
}