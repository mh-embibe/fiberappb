package com.embibe.videoplayer.model

class JarModel {
    var id: Int = 0
    var image: String = ""
    var title: String = ""
    var isHighlighted: Boolean = false
}