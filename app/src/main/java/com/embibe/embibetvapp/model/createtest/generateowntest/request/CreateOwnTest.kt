package com.embibe.embibetvapp.model.createtest.generateowntest.request


import com.google.gson.annotations.SerializedName

data class CreateOwnTest(
    @SerializedName("chapter_data")
    val chapter_data: Map<String, ChapterData>,
    @SerializedName("create_mocktest_data")
    var createMocktestData: Any?,
    @SerializedName("created_at")
    var createdAt: String?,
    @SerializedName("difficulty_level")
    var difficultyLevel: String?,
    @SerializedName("duration")
    var duration: Int?,
    @SerializedName("exam_code")
    var examCode: String?,
    @SerializedName("ideal_time_level_to_finish")
    var idealTimeLevelToFinish: String?,
    @SerializedName("language")
    var language: String?,
    @SerializedName("last_modified_at")
    var lastModifiedAt: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("question_count")
    var questionCount: Int?,
    @SerializedName("resource_type")
    var resourceType: String?,
    @SerializedName("section_data")
    val section_data: HashMap<String, SectionData>?,
    @SerializedName("source")
    var source: String?,
    @SerializedName("test_config")
    var testConfig: TestConfig?,
    @SerializedName("marking_scheme")
    var marking_scheme: MarkingScheme?
)