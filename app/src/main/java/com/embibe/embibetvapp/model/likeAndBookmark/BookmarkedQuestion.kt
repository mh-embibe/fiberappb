package com.embibe.embibetvapp.model.likeAndBookmark

import com.google.gson.annotations.SerializedName


class BookmarkedQuestion {

    @SerializedName("question_id")
    val questionId: String? = null

    @SerializedName("question_sequence")
    val questionSequence: String? = null

    @SerializedName("attempt_type")
    val attemptType: String? = null

    @SerializedName("marks")
    val marks: Marks? = null

    @SerializedName("difficulty_level")
    val difficultyLevel: Int? = null

    @SerializedName("question_txt")
    val questionTxt: String? = null

    @SerializedName("subject")
    val subject: String? = null

    @SerializedName("bookmarked_source")
    val bookmarkedSource: String? = null

    @SerializedName("source_name")
    val sourceName: String? = null

    @SerializedName("source_id")
    val sourceId: String? = null

}