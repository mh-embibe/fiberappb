package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class TestAttemptsRes() : Parcelable {
    @SerializedName("actionReplay")
    var actionReplay: ActionReplay? = null

    @SerializedName("attempts")
    var attempts: List<Attempt>? = null

    @SerializedName("scoreListInfo")
    var scoreListInfo: ScoreListInfo? = null

    @SerializedName("startedAtInMilliSec")
    var startedAtInMilliSec: Long? = null

    constructor(parcel: Parcel) : this() {
        actionReplay = parcel.readParcelable(ActionReplay::class.java.classLoader)
        attempts = parcel.readArrayList(Attempt::class.java.classLoader) as ArrayList<Attempt>
        scoreListInfo = parcel.readParcelable(ScoreListInfo::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(actionReplay, flags)
        parcel.writeParcelable(scoreListInfo, flags)
        parcel.writeTypedList(attempts)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TestAttemptsRes> {
        override fun createFromParcel(parcel: Parcel): TestAttemptsRes {
            return TestAttemptsRes(parcel)
        }

        override fun newArray(size: Int): Array<TestAttemptsRes?> {
            return arrayOfNulls(size)
        }
    }
}