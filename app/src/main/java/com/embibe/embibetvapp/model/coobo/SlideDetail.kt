package com.embibe.embibetvapp.model.coobo

import com.google.gson.annotations.SerializedName


class SlideDetail {

    @SerializedName("slide_uuid")

    val slideUuid: String? = null

    @SerializedName("slide_meta_tags")

    val slideMetaTags: List<SlideMetaTag>? = null

    @SerializedName("questionsList")

    val questionsList: List<Any>? = null
}