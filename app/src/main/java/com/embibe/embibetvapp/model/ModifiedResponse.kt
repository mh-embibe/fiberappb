package com.embibe.embibetvapp.model

import com.embibe.embibetvapp.model.achieve.readiness.PredictedGrade
import com.embibe.embibetvapp.model.achieve.readiness.ProjectedGrade

data class ModifiedResponse(
    var type: String,
    var gradeText: String?,
    var gradeMessage: String,
    var discoverYourPotential: String,
    var likeDislikeImg: String?,
    var projectedGrade: ProjectedGrade?,
    var predictedGrade: PredictedGrade?
)