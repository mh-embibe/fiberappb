package com.embibe.embibetvapp.model

import com.google.gson.annotations.SerializedName

data class SendOtpRequest(
    @SerializedName("login") val login: String
)
