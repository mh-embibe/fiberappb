package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class SectionSummary() : Parcelable {
    @SerializedName("incorrectCount")
    var incorrectCount: Int = 0

    @SerializedName("marksScored")
    var marksScored: Double = 0.0

    @SerializedName("name")
    var name: String? = null

    @SerializedName("otcCount")
    var otcCount: Int = 0

    @SerializedName("otiCount")
    var otiCount: Int = 0

    @SerializedName("perfectCount")
    var perfectCount: Int = 0

    @SerializedName("tooFastCorrectCount")
    var tooFastCorrectCount: Int = 0

    @SerializedName("totalMarks")
    var totalMarks: Double = 0.0

    @SerializedName("wastedCount")
    var wastedCount: Int = 0

    constructor(parcel: Parcel) : this() {
        incorrectCount = parcel.readInt()
        marksScored = parcel.readDouble()
        name = parcel.readString()
        otcCount = parcel.readInt()
        otiCount = parcel.readInt()
        perfectCount = parcel.readInt()
        tooFastCorrectCount = parcel.readInt()
        totalMarks = parcel.readDouble()
        wastedCount = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(incorrectCount)
        parcel.writeDouble(marksScored)
        parcel.writeString(name)
        parcel.writeInt(otcCount)
        parcel.writeInt(otiCount)
        parcel.writeInt(perfectCount)
        parcel.writeInt(tooFastCorrectCount)
        parcel.writeDouble(totalMarks)
        parcel.writeInt(wastedCount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SectionSummary> {
        override fun createFromParcel(parcel: Parcel): SectionSummary {
            return SectionSummary(parcel)
        }

        override fun newArray(size: Int): Array<SectionSummary?> {
            return arrayOfNulls(size)
        }
    }
}