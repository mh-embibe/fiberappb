package com.embibe.embibetvapp.model.details

import com.google.gson.annotations.SerializedName


class Self {
    @SerializedName("title")
    private val title: String? = null

    @SerializedName("href")
    private val href: String? = null
}
