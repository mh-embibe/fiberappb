package com.embibe.embibetvapp.model.test

data class SectionData(
    val code: String,
    val name: String,
    val questions: List<Question>,
    val sequence: Double
)