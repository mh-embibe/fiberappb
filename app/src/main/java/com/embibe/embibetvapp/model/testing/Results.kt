package com.embibe.embibetvapp.model.testing

import com.google.gson.annotations.SerializedName

data class Results(
    @SerializedName("content") val content: List<Content>,
    @SerializedName("type") val type: String,
    @SerializedName("section_name") val section_name: String
)