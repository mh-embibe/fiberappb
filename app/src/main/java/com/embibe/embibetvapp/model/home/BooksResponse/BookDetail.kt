package com.embibe.embibetvapp.model.home.BooksResponse

import com.embibe.embibetvapp.newmodel.Content
import com.google.gson.annotations.SerializedName


class BookDetail {

    @SerializedName("name")
    var name: String = ""

    @SerializedName("category")
    var category: String = ""

    @SerializedName("lpcode")
    var lpcode: String = ""

    @SerializedName("front_page_image_url")
    var frontPageImageUrl: String = ""

    @SerializedName("thumbnail_url")
    var thumbnailUrl: String = ""

    @SerializedName("book_id")
    var bookId: String = ""

    @SerializedName("description")
    var description: String = ""

    @SerializedName("authors")
    var authors: String? = "Lakhmir Singh,Manjit Kaur"

    @SerializedName("embiums")
    var embiums: Double? = null

    @SerializedName("duration")
    var duration: String = ""

    @SerializedName("rating")
    var rating: Double? = null

    @SerializedName("subtitle")
    var subtitle: String = ""

    @SerializedName("subject")
    var subject: String = ""

    @SerializedName("nav_type")
    var navType: String = ""

    @SerializedName("user_accuracy")
    var user_accuracy: String? = "25%"

    @SerializedName("social_accuracy")
    var social_accuracy: String? = "58%"

    @SerializedName("user_behaviour")
    var user_behaviour: String? = "Not Started"

    @SerializedName("social_behaviour")
    var social_behaviour: String? = "Flukes"

    @SerializedName("user_growth")
    var user_growth: String? = "Not Started"

    @SerializedName("social_growth")
    var social_growth: String? = "60%"

    var response: Content? = null

    @SerializedName("bg_url")
    var bg_url: String? =
        "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/bg/Chemistry_DP.png"


}