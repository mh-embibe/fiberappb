package com.embibe.embibetvapp.model

import com.google.gson.annotations.SerializedName


data class TestDataRes(
    var section_name: String,
    @SerializedName("content")
    var data: ArrayList<TestModel>
)