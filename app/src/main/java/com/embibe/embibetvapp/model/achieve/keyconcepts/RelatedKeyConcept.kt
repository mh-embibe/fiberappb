package com.embibe.embibetvapp.model.achieve.keyconcepts

import com.google.gson.annotations.SerializedName


class RelatedKeyConcept {
    @SerializedName("concept_name")
    val conceptName: String? = null

    @SerializedName("concept_id")
    val conceptId: String? = null

}