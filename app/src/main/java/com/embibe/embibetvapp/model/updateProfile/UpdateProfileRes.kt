package com.embibe.embibetvapp.model.updateProfile

import com.google.gson.annotations.SerializedName

class UpdateProfileRes {
    @SerializedName("success")
    var success: Boolean = false

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("resource")
    val resource: Resource? = null

    @SerializedName("ip")
    private val ip: String? = null

    @SerializedName("errors")
    var errors: Errors? = null
}