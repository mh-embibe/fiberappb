package com.embibe.embibetvapp.model.test

import com.embibe.embibetvapp.newmodel.Content

data class Question(
    val answers: List<Answer>,
    val body: String,
    val category: String,
    val code: String,
    val difficulty_band: String,
    val difficulty_level: Double,
    val ideal_time: Double,
    val language: String,
    val learning_map: LearningMap,
    val marking_scheme: MarkingScheme,
    val question_index: Double,
    val version: Double,
    var attemptTypeBadge: String? = "",
    var correctAnswerCsv: String? = "",
    var answerSelectedOption: String? = "",
    var tags: List<String>,
    var explanation: String? = "",
    var marks_obtained: Double,
    var first_looked_at: Int,
    var attempted_at: Int,
    var test_started_at: Long,
    var practiced_before: Boolean? = false,
    var practice_badge: String?,
    var primary_concept: List<Content>?,
    var secondary_concept: List<Content>?
)