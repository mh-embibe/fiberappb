package com.embibe.embibetvapp.model.createtest.examconfig.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TestDetails(
    val subject_data: List<SubjectData>
) : Parcelable