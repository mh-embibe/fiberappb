package com.embibe.embibetvapp.model

import android.os.Parcel
import android.os.Parcelable
import com.embibe.embibetvapp.converter.*
import com.embibe.embibetvapp.model.learning.OwnerInfo
import com.embibe.embibetvapp.newmodel.LearningMap
import com.google.gson.annotations.SerializedName
import io.objectbox.annotation.Convert
import io.objectbox.annotation.Id

class TestModel() : Parcelable {
    @Id
    var objId: Long = 0

    var type: String = ""
        get() = getDefaultString(field)

    @Convert(
        converter = LearnmapConverter::class,
        dbType = String::class
    )
    var learning_map: LearningMap = LearningMap()

    var child_id: Long = 0
        get() = getDefaultLong(field)

    var sub_type: String = ""
        get() = getDefaultString(field)

    var id: String = ""
        get() = getDefaultString(field)

    var category: String = ""
        get() = getDefaultString(field)

    @SerializedName("content_id")
    var content_id: String = ""
        get() = getDefaultString(field)

    @SerializedName("category_url")
    var category_thumb: String = ""
        get() = getDefaultString(field)

    var description: String = ""
        get() = getDefaultString(field)

    var views: Int = 0
        get() = getDefaultInt(field)

    var url: String = ""
        get() = getDefaultString(field)

    var title: String = ""
        get() = getDefaultString(field)

    var currency: Int = 0
        get() = getDefaultInt(field)

    var length: Int = 0
        get() = getDefaultInt(field)

    var bg_thumb: String = ""
        get() = getDefaultString(field)

    var likes: Int = 0
        get() = getDefaultInt(field)

    var watched_duration: Int = 0
    var thumb: String = ""
        get() = getDefaultString(field)

    @Convert(
        converter = StringListConverter::class,
        dbType = String::class
    )
    var authors: ArrayList<String> = arrayListOf()

    var section_name: String = ""
        get() = getDefaultString(field)

    var teaser_url: String = ""
        get() = getDefaultString(field)

    var concept_count: Int = 0
        get() = getDefaultInt(field)

    /*var rating: Double = 0.0
        get() = getDefaultDouble(field)*/

    @Convert(
        converter = OwnerInfoConverter::class,
        dbType = String::class
    )
    var owner_info: OwnerInfo = OwnerInfo()

    var learnmap_id: String = ""
        get() = getDefaultString(field)

    var subject: String = ""
        get() = getDefaultString(field)


    @SerializedName("data")
    @Convert(
        converter = BannerDataConverter::class,
        dbType = String::class
    )
    var banner_data: ArrayList<BannerData> = ArrayList()


    var isBookmarked: Boolean = false

    var isLiked: Boolean = false

    //lateinit var section: ToOne<ResultsEntity>

    @Convert(
        converter = AnnotationConverter::class,
        dbType = String::class
    )
    var annotation_attributes: ArrayList<Annotation> = arrayListOf()

    private fun getDefaultString(field: String): String {
        return field
    }

    private fun getDefaultInt(field: Int): Int {
        return field
    }

    private fun getDefaultDouble(field: Double): Double {
        return field
    }

    private fun getDefaultLong(field: Long): Long {
        return field
    }

    private constructor(parcel: Parcel) : this() {
        subject = parcel.readString() ?: ""
        objId = parcel.readLong()
        type = parcel.readString() ?: ""
        learning_map = parcel.readParcelable(LearningMap::class.java.classLoader)!!
        child_id = parcel.readLong()
        sub_type = parcel.readString() ?: ""
        //id = parcel.readString()?:""
        category = parcel.readString() ?: ""
        content_id = parcel.readString() ?: ""
        category_thumb = parcel.readString() ?: ""
        description = parcel.readString() ?: ""
        views = parcel.readInt()
        url = parcel.readString() ?: ""
        title = parcel.readString() ?: ""
        currency = parcel.readInt()
        length = parcel.readInt()
        bg_thumb = parcel.readString() ?: ""
        likes = parcel.readInt()
        watched_duration = parcel.readInt()
        thumb = parcel.readString() ?: ""
        authors = parcel.readArrayList(String.javaClass.classLoader) as ArrayList<String>
        section_name = parcel.readString() ?: ""
        teaser_url = parcel.readString() ?: ""
        concept_count = parcel.readInt()
        //rating = parcel.readDouble()
        learnmap_id = parcel.readString() ?: ""

        //banner_data = parcel.createTypedArrayList(BannerData.CREATOR)!!
        isBookmarked = parcel.readInt() == 1
        isLiked = parcel.readInt() == 1


    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(subject)
        parcel.writeLong(objId)
        parcel.writeString(type)
        parcel.writeParcelable(learning_map, flags)
        parcel.writeLong(child_id)
        parcel.writeString(sub_type)
        // parcel.writeString(id)
        parcel.writeString(category)
        parcel.writeString(content_id)
        parcel.writeString(category_thumb)
        parcel.writeString(description)
        parcel.writeInt(views)
        parcel.writeString(url)
        parcel.writeString(title)
        parcel.writeInt(currency)
        parcel.writeInt(length)
        parcel.writeString(bg_thumb)
        parcel.writeInt(likes)
        parcel.writeInt(watched_duration)
        parcel.writeString(thumb)
        parcel.writeStringList(authors)
        parcel.writeString(section_name)
        parcel.writeString(teaser_url)
        parcel.writeInt(concept_count)
        //parcel.writeDouble(rating)
        parcel.writeParcelable(owner_info, flags)
        parcel.writeString(learnmap_id)

        // parcel.writeTypedList(banner_data)
        parcel.writeInt(if (isBookmarked) 1 else 0)
        parcel.writeInt(if (isLiked) 1 else 0)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TestModel> {
        override fun createFromParcel(parcel: Parcel): TestModel {
            return TestModel(parcel)
        }

        override fun newArray(size: Int): Array<TestModel?> {
            return arrayOfNulls(size)
        }
    }
}