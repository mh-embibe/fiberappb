package com.embibe.embibetvapp.model.connection

class ConnectionModel private constructor() {
    var isConnected = false
    var msg: String = ""

    private object HOLDER {
        val INSTANCE = ConnectionModel()
    }

    companion object {
        val instance: ConnectionModel by lazy {
            HOLDER.INSTANCE
        }
    }

}