package com.embibe.embibetvapp.model.practise.conceptSequence

class ConceptSeqRes {
    val success: Boolean = false
    val data: List<ConceptSequence> = arrayListOf()
}