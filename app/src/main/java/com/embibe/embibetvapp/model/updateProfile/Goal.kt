package com.embibe.embibetvapp.model.updateProfile

import com.google.gson.annotations.SerializedName


class Goal {
    @SerializedName("goal_info")
    var goalInfo: GoalInfo? = null

    @SerializedName("dream_college")
    var dreamCollege: DreamCollege? = null
}