package com.embibe.embibetvapp.model.achieve.home


import com.embibe.embibetvapp.newmodel.Content
import com.google.gson.annotations.SerializedName

data class AchieveHomeResponseItem(
    @SerializedName("content")
    var content: List<Content>?,
    @SerializedName("data")
    var `data`: Data?,
    @SerializedName("section")
    var section: String?,
    @SerializedName("section_id")
    var sectionId: Int?,
    @SerializedName("section_name")
    var sectionName: String?

) {
    override fun toString(): String {
        return "AchieveHomeResponseItem(content=$content, `data`=$`data`, section=$section, sectionId=$sectionId, sectionName=$sectionName)"
    }
}