package com.embibe.embibetvapp.model.detailModel

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions

data class DetailItemModel(
    var title: String,
    var type: String,
    var image: String,
    var description: String,
    var rating: Double,
    var time: String,
    var embiums: String

) {
    companion object {
        @BindingAdapter("image")
        @JvmStatic
        fun loadImage(view: ImageView, imageUrl: String?) {

            val requestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            Glide.with(view.context).setDefaultRequestOptions(requestOptions)
                .load(imageUrl)
                .transition(DrawableTransitionOptions.withCrossFade())
                .transform(RoundedCorners(14))
                .into(view)
        }
    }
}