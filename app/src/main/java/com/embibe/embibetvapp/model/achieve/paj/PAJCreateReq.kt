package com.embibe.embibetvapp.model.achieve.paj

import com.google.gson.annotations.SerializedName




class PAJCreateReq {
    @SerializedName("type")
    var type: String? = null

    @SerializedName("bundle_code")
    var bundleCode: String? = null

    @SerializedName("answer_data")
    var answerData: AnswerData? = null
}