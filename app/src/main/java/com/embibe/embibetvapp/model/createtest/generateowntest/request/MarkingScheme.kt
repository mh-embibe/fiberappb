package com.embibe.embibetvapp.model.createtest.generateowntest.request

import com.google.gson.annotations.SerializedName

data class MarkingScheme(
    @SerializedName("nmarks")
    var nmarks: String?,
    @SerializedName("pmarks")
    var pmarks: String?,
    @SerializedName("partial_marking")
    var partial_marking: Boolean?
)