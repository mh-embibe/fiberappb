package com.embibe.embibetvapp.model.test

import com.embibe.embibetvapp.newmodel.Content

data class TestQuestionSummaryResData(
    val practiced_before: Boolean = true,
    val primary_concept: List<Content>?,
    val question_code: String?,
    val practice_badge: String = "",
    val secondary_concept: List<Content>?
)