package com.embibe.embibetvapp.model.updateProfile


import com.google.gson.annotations.SerializedName


class Personal {
    @SerializedName("personal_info")
    var personalInfo: PersonalInfo? = null

    @SerializedName("parent_info")
    var parentInfo: ParentInfo? = null
}