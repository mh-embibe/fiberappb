package com.embibe.embibetvapp.model.details

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Parent {
    @SerializedName("title")
    @Expose
    private val title: String = ""

    @SerializedName("href")
    @Expose
    private val href: String = ""
}
