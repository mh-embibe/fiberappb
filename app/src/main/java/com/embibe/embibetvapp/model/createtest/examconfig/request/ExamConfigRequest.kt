package com.embibe.embibetvapp.model.createtest.examconfig.request


import com.google.gson.annotations.SerializedName


data class ExamConfigRequest(
    @SerializedName("exam_name")
    var examName: String? = null,
    @SerializedName("namespace")
    var namespace: String? = null,
    @SerializedName("version")
    var version: String? = null,
    @SerializedName("type")
    var type: String? = null,
    @SerializedName("goal_code")
    var goal_code: String? = null,
    @SerializedName("child_id")
    var child_id: String? = null,
    @SerializedName("learnt_embibe")
    var learnt_embibe: Boolean = false
)