package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class AttemptedAnswer() : Parcelable {

    @SerializedName("@class")
    var classType: String? = ""

    @SerializedName("answer")
    var answer: List<String>? = null

    constructor(parcel: Parcel) : this() {
        classType = parcel.readString()
        answer = parcel.readArrayList(String.javaClass.classLoader) as ArrayList<String>
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(classType)
        parcel.writeStringList(answer)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AttemptedAnswer> {
        override fun createFromParcel(parcel: Parcel): AttemptedAnswer {
            return AttemptedAnswer(parcel)
        }

        override fun newArray(size: Int): Array<AttemptedAnswer?> {
            return arrayOfNulls(size)
        }
    }

}
