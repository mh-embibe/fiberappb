package com.embibe.embibetvapp.model.jiostb

import com.google.gson.annotations.SerializedName


class User {
    @SerializedName("subscriberId")
    val subscriberId: String? = null

    @SerializedName("preferredLocale")
    private val preferredLocale: String? = null

    @SerializedName("ssoLevel")
    private val ssoLevel: String? = null

    @SerializedName("unique")
    private val unique: String? = null

}
