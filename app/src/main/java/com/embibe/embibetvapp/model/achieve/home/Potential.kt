package com.embibe.embibetvapp.model.achieve.home
import com.google.gson.annotations.SerializedName

data class Potential(

    @SerializedName("grade") val grade: String,
    @SerializedName("subjects") val subjects: List<Subjects>
)