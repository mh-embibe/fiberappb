package com.embibe.embibetvapp.model.test

class DiagnosticTestFeedbackModel(
    var stepNo: String,
    var stepDescription: String,
    var predictedGradeImprovement: String,
    var actualGradeImprovement: String,
    var status: String,
    var background: Int,
    var subItems: ArrayList<DiagnosticTestFeedbackSubModel>
)