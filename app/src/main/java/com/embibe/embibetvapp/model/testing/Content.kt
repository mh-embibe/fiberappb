package com.embibe.embibetvapp.model.testing

import com.google.gson.annotations.SerializedName


data class Content(
    @SerializedName("category") val category: String,
    @SerializedName("description") val description: String,
    @SerializedName("title") val title: String,
    @SerializedName("url") val url: String,
    @SerializedName("bg_thumb") val bg_thumb: String
)

