package com.embibe.embibetvapp.model.details

import com.google.gson.annotations.SerializedName


class Last {
    @SerializedName("title")
    private val title: String = ""

    @SerializedName("href")
    private val href: String = ""
}
