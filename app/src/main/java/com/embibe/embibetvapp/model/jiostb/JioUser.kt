package com.embibe.embibetvapp.model.jiostb


import com.google.gson.annotations.SerializedName


class JioUser {
    @SerializedName("ssoLevel")
    private val ssoLevel: String? = null

    @SerializedName("ssoToken")
    private val ssoToken: String? = null

    @SerializedName("sessionAttributes")
    val sessionAttributes: SessionAttributes? = null

    @SerializedName("lbCookie")
    private val lbCookie: String? = null
}