package com.embibe.embibetvapp.model.createtest.examconfig.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChapterData(
    var code: String = "",
    var name: String = "",
    var no_of_questions: Int = 0,
    var previous_year_weight_percent: Int = 0,
    var subject_name: String = "",
    var unit_name: String = ""
) : Parcelable