package com.embibe.embibetvapp.model.home

import com.embibe.embibetvapp.model.test.TestAchieveRes
import com.embibe.embibetvapp.model.test.TestAttemptsRes
import com.embibe.embibetvapp.model.test.TestQuestionResponse
import com.embibe.embibetvapp.model.test.TestQuestionSummaryRes

class TestFeedBackModel {

    lateinit var testQuestionResponse: TestQuestionResponse
    lateinit var testAchieveRes: TestAchieveRes
    lateinit var testAttemptsRes: TestAttemptsRes
    lateinit var testQuestionSummaryRes: TestQuestionSummaryRes
}