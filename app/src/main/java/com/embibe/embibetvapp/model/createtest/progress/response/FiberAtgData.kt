package com.embibe.embibetvapp.model.createtest.progress.response


import com.google.gson.annotations.SerializedName

data class FiberAtgData(
    @SerializedName("active_since")
    var activeSince: Any?,
    @SerializedName("active_till")
    var activeTill: String?,
    @SerializedName("air_type")
    var airType: String?,
    @SerializedName("approved_by")
    var approvedBy: Int?,
    @SerializedName("chapter_count")
    var chapterCount: Int?,
    @SerializedName("code")
    var code: String?,
    @SerializedName("complexity_coefficient")
    var complexityCoefficient: Any?,
    @SerializedName("config_fields")
    var configFields: Any?,
    @SerializedName("created_at")
    var createdAt: String?,
    @SerializedName("created_by")
    var createdBy: Int?,
    @SerializedName("cutoff")
    var cutoff: Any?,
    @SerializedName("description")
    var description: String?,
    @SerializedName("duration")
    var duration: Double?,
    @SerializedName("feedback_section_level")
    var feedbackSectionLevel: Int?,
    @SerializedName("goal_code")
    var goalCode: Any?,
    @SerializedName("id")
    var id: Int?,
    @SerializedName("is_live_test")
    var isLiveTest: Boolean?,
    @SerializedName("jumble_questions")
    var jumbleQuestions: Any?,
    @SerializedName("language")
    var language: String?,
    @SerializedName("learning_map_code")
    var learningMapCode: String?,
    @SerializedName("marking_scheme")
    var markingScheme: MarkingScheme?,
    @SerializedName("max_marks")
    var maxMarks: Double?,
    @SerializedName("mocktest_instruction_code")
    var mocktestInstructionCode: String?,
    @SerializedName("mocktest_section_code")
    var mocktestSectionCode: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("namespace")
    var namespace: String?,
    @SerializedName("old_xpath")
    var oldXpath: String?,
    @SerializedName("path")
    var path: String?,
    @SerializedName("quality_score")
    var qualityScore: Any?,
    @SerializedName("question_count")
    var questionCount: Int?,
    @SerializedName("sequence")
    var sequence: Double?,
    @SerializedName("show_air")
    var showAir: Boolean?,
    @SerializedName("signature")
    var signature: Any?,
    @SerializedName("slug")
    var slug: String?,
    @SerializedName("status")
    var status: String?,
    @SerializedName("subject_name")
    var subjectName: List<String>?,
    @SerializedName("target_scores")
    var targetScores: Any?,
    @SerializedName("test_meta")
    var testMeta: Any?,
    @SerializedName("test_type")
    var testType: String?,
    @SerializedName("ui_section_level")
    var uiSectionLevel: Int?,
    @SerializedName("unique_code")
    var uniqueCode: String?,
    @SerializedName("updated_at")
    var updatedAt: String?,
    @SerializedName("updated_by")
    var updatedBy: Int?,
    @SerializedName("version")
    var version: Int?
)