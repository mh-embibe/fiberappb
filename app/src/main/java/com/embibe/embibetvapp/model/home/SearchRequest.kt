package com.embibe.embibetvapp.model.home

class SearchRequest {
    var cache_on: String? = null
    var learn_path: String? = null
    var query: String? = null
    var section: String? = null
    var source: String? = null
    var user_id: String? = null
    var test_code: String? = null
    var xpath: String? = null
    var formatReferenceId: String? = null
    var grade: String? = null
    var size: Int? = null
}