package com.embibe.embibetvapp.model

import android.os.Parcel
import android.os.Parcelable

class Demo() : Parcelable {

    var test: String? = ""

    var temp: String? = ""
        get() = field ?: ""
    var other: String? = null

    constructor(parcel: Parcel) : this() {
        test = parcel.readString() ?: ""
        temp = parcel.readString() ?: ""
        other = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(test)
        parcel.writeString(temp)
        parcel.writeString(other)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Demo> {
        override fun createFromParcel(parcel: Parcel): Demo {
            return Demo(parcel)
        }

        override fun newArray(size: Int): Array<Demo?> {
            return arrayOfNulls(size)
        }
    }


}