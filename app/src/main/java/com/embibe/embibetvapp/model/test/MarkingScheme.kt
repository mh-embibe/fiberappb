package com.embibe.embibetvapp.model.test

data class MarkingScheme(
    val nmarks: Double,
    val partial_marking: Boolean,
    val pmarks: Double
)