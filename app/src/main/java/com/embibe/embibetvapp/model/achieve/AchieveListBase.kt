package com.embibe.embibetvapp.model.achieve

import com.embibe.embibetvapp.model.achieve.educationQualification.EducationQualification

open class AchieveListBase {
    val exam :String? = null
    val key :String? = null
    val data :List<EducationQualification>? = null
}