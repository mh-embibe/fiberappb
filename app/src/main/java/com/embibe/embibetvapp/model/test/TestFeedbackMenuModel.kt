package com.embibe.embibetvapp.model.test

class TestFeedbackMenuModel(
    var title: String,
    var subItems: ArrayList<SubFeedbackMenuModel>,
    var background: Int
)