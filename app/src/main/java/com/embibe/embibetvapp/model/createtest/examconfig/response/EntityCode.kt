package com.embibe.embibetvapp.model.createtest.examconfig.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EntityCode(
    val difficulty_level: String,
    val difficulty_level_std: Int,
    val difficulty_order: String,
    val entity_name: String,
    val entity_type: String,
    val ideal_time_level: String,
    val ideal_time_level_std: Int,
    val question_count: Int,
    val question_type_count: QuestionTypeCount,
    val question_types: Int

) : Parcelable