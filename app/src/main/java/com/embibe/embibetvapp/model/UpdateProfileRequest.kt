package com.embibe.embibetvapp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class UpdateProfileRequest : Serializable {

    @SerializedName("primary_goal")
    var primaryGoal: String? = null

    @SerializedName("secondary_goal")
    var secondaryGoal: String? = null

    @SerializedName("secondary_exams_preparing_for")
    var secondaryExamsPreparingFor: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("mobile")
    var mobile: String? = null

    @SerializedName("first_name")
    var firstName: String? = null

    @SerializedName("primary_exam_code")
    var primaryExamCode: String? = null

    @SerializedName("profile_pic_file_name")
    var profilePic: String? = null

}