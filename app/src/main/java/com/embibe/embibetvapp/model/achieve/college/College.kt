package com.embibe.embibetvapp.model.achieve.college

import com.google.gson.annotations.SerializedName

class College {
    @SerializedName("id")
    val id: Int? = null

    @SerializedName("name")
    val name: String? = null
}