package com.embibe.embibetvapp.model.coobo

import com.google.gson.annotations.SerializedName

class CooboApiRes {

    @SerializedName("_items")
    var _items: ArrayList<Coobo>? = null
}