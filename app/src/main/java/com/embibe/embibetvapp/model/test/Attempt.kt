package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Attempt() : Parcelable {
    @SerializedName("attemptTypeBadge")
    var attemptTypeBadge: String? = ""

    @SerializedName("questionCode")
    var questionCode: String? = ""

    @SerializedName("questionVersion")
    var questionVersion: Int? = 0

    @SerializedName("correctAnswerCsv")
    var correctAnswerCsv: String? = ""

    @SerializedName("tags")
    var tags: List<String>? = null

    @SerializedName("marks")
    var marks: Double? = 0.0

    @SerializedName("difficultyBand")
    var difficultyBand: String? = ""

    @SerializedName("explanation")
    var explanation: String? = ""

//    @SerializedName("attemptedAnswer")
//    var attemptedAnswer: AttemptedAnswer? = null

    @SerializedName("primaryConceptCode")
    var primaryConceptCode: String? = ""

    @SerializedName("t_first_look")
    var tFirstLook: Int? = 0

    @SerializedName("blendedDifficulty")
    var blendedDifficulty: Int? = 0

    @SerializedName("t_first_save")
    var tFirstSave: Int? = 0

    @SerializedName("t_last_save")
    var tLastSave: Int? = 0

    @SerializedName("t_last_look")
    var tLastLook: Int? = 0

    @SerializedName("timeSpent")
    var timeSpent: Double? = 0.0

    @SerializedName("sequence")
    var sequence: Int? = 0

    @SerializedName("answerSelectedOption")
    var answerSelectedOption: String? = ""

    constructor(parcel: Parcel) : this() {
        attemptTypeBadge = parcel.readString()
        questionCode = parcel.readString()
        questionVersion = parcel.readInt()
        correctAnswerCsv = parcel.readString()
        tags = parcel.readArrayList(String::class.java.classLoader) as ArrayList<String>
        marks = parcel.readDouble()
        difficultyBand = parcel.readString()
        explanation = parcel.readString()
//        attemptedAnswer = parcel.readParcelable(AttemptedAnswer::class.java.classLoader)
        primaryConceptCode = parcel.readString()
        tFirstLook = parcel.readInt()
        blendedDifficulty = parcel.readInt()
        tFirstSave = parcel.readInt()
        tLastSave = parcel.readInt()
        tLastLook = parcel.readInt()
        timeSpent = parcel.readDouble()
        sequence = parcel.readInt()
        answerSelectedOption = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

        parcel.writeString(attemptTypeBadge)
        parcel.writeString(questionCode)
        questionVersion?.let { parcel.writeInt(it) }
        parcel.writeString(correctAnswerCsv)
        parcel.writeList(tags)
        marks?.let { parcel.writeDouble(it) }
        parcel.writeString(difficultyBand)
        parcel.writeString(explanation)
//        parcel.writeParcelable(attemptedAnswer, flags)
        parcel.writeString(primaryConceptCode)
        tFirstLook?.let { parcel.writeInt(it) }
        blendedDifficulty?.let { parcel.writeInt(it) }
        tFirstSave?.let { parcel.writeInt(it) }
        tLastSave?.let { parcel.writeInt(it) }
        tLastLook?.let { parcel.writeInt(it) }
        timeSpent?.let { parcel.writeDouble(it) }
        sequence?.let { parcel.writeInt(it) }
        parcel.writeString(answerSelectedOption)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Attempt> {
        override fun createFromParcel(parcel: Parcel): Attempt {
            return Attempt(parcel)
        }

        override fun newArray(size: Int): Array<Attempt?> {
            return arrayOfNulls(size)
        }
    }
}