package com.embibe.embibetvapp.model.createtest.examconfig.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class QuestionTypeCount(


    val FillInTheBlanks: Int,
    val SingleChoice: Int,
    val SubjectiveAnswer: Int,
    val TrueFalse: Int

) : Parcelable