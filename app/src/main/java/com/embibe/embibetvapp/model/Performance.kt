package com.embibe.embibetvapp.model

data class Performance(
    var image: Int,
    var message: String
)