package com.embibe.embibetvapp.model.achieve.updateTruePotential

import com.google.gson.annotations.SerializedName




class UpdateTruePotentialRes {
    @SerializedName("available_time_per_day")
    val availableTimePerDay: String? = null

    @SerializedName("target_grade")
    val targetGrade: String? = null

    @SerializedName("dream_job")
    val dreamJob: String? = null

    @SerializedName("earn_per_month")
    val earnPerMonth: String? = null

    @SerializedName("identify_college")
    val identifyCollege: IdentifyCollege? = null
}