package com.embibe.embibetvapp.model

import com.google.gson.annotations.SerializedName

data class BannerDataList(
    @SerializedName("data")
    var data: ArrayList<BannerData> = ArrayList()
)