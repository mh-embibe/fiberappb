package com.embibe.embibetvapp.model.test

data class Paper(
    val sections: HashMap<String, SectionData>
)