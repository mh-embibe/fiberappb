package com.embibe.embibetvapp.model.goals

import com.embibe.embibetvapp.model.adduser.Goal

class GoalsExamsRes {
    val success: Boolean? = false
    val data: List<Goal>? = null
}