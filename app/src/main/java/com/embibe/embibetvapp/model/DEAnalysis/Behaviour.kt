package com.embibe.embibetvapp.model.DEAnalysis

import com.google.gson.annotations.SerializedName

class Behaviour {
    @SerializedName("behaviour")
    val behaviour: String? = null
        get() = field ?: ""

    @SerializedName("behaviour_%")
    val behaviourPercentage: Double? = null
        get() = field ?: 0.0

    @SerializedName("behaviour_command")
    var behaviourCommand: String? = null
        get() = field ?: ""
}
