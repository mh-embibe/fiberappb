package com.embibe.embibetvapp.model.achieve.futureSuccess

import com.embibe.embibetvapp.model.achieve.readiness.PredictedGrade
import com.embibe.embibetvapp.model.achieve.readiness.ProjectedGrade

import com.google.gson.annotations.SerializedName


data class FutureSuccessRes(
    @SerializedName("exam")
    var exam: String? = null,

    @SerializedName("is_primary")
    val isPrimary: Boolean? = null,

    @SerializedName("predicted_grade")
    val predictedGrade: PredictedGrade? = null,

    @SerializedName("projected_grade")
    val projectedGrade: ProjectedGrade? = null
)