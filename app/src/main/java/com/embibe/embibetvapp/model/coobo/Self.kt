package com.embibe.embibetvapp.model.coobo

import com.google.gson.annotations.SerializedName


class Self {

    @SerializedName("title")
    val title: String? = null

    @SerializedName("href")
    val href: String? = null
}