package com.embibe.embibetvapp.model.auth.login

import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("user_name") val user_name: String,
    @SerializedName("password") val password: String
)