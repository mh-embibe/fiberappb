package com.embibe.embibetvapp.model.achieve.home


import com.google.gson.annotations.SerializedName

data class HolisticReadiness(
    @SerializedName("potential")
    var potential: Potential?,
    @SerializedName("predicted")
    var predicted: Predicted?
){
    override fun toString(): String {
        return "HolisticReadiness(potential=$potential, predicted=$predicted)"
    }
}