package com.embibe.embibetvapp.model.achieve.home

import com.google.gson.annotations.SerializedName

class Content {

    @SerializedName("type")
    val type: String = ""

    @SerializedName("exam")
    val exam: String = ""

    @SerializedName("predicted")
    val predicted: Predicted? = null

    @SerializedName("potential")
    val potential: Potential? = null

    @SerializedName("title")
    val title: String = ""

    @SerializedName("total_steps")
    val total_steps: Int = 0

    @SerializedName("current_step")
    val current_step: Int = 0

    @SerializedName("covered_steps")
    val covered_steps: Int = 0

    @SerializedName("overall_progress")
    val overall_progress: Int = 0

    @SerializedName("questions")
    val questions: Int = 0

    @SerializedName("videos")
    val videos: Int = 0

    @SerializedName("behaviours")
    val behaviours: Int = 0

    @SerializedName("effort_rating")
    val effort_rating: Int = 0

    @SerializedName("actual_grade")
    val actual_grade: String = ""

    @SerializedName("potential_grade")
    val potential_grade: String = ""

    @SerializedName("xpath")
    val xpath: String = ""

    @SerializedName("post_test_xpath")
    val post_test_xpath: String = ""

    @SerializedName("post_test_bundle_id")
    val post_test_bundle_id: String = ""

    @SerializedName("bundle_id")
    val bundle_id: String = ""

    @SerializedName("subject")
    val subject: String = ""

    @SerializedName("completion_status")
    val completion_status: String = ""

    @SerializedName("status")
    val status: String = ""

    @SerializedName("id")
    val id: String = ""

}