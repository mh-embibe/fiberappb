package com.embibe.embibetvapp.model.auth.signup

import com.google.gson.annotations.SerializedName

data class SignupSendOtpRequest(
    @SerializedName("mobile") val mobile: String?,
    @SerializedName("email") val email: String?,
    @SerializedName("app_type") val app_type: String,
    @SerializedName("ttl") val ttl: String,
    @SerializedName("attempts") val attempts: String
)
