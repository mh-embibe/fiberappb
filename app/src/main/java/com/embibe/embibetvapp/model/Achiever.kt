package com.embibe.embibetvapp.model

data class Achiever(
    var image: String?,
    var title: String?,
    var isFlipped: Boolean
)