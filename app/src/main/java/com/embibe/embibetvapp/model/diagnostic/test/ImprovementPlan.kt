package com.embibe.embibetvapp.model.diagnostic.test

import com.embibe.embibetvapp.model.diagnostic.ReadninessData
import com.embibe.embibetvapp.model.diagnostic.Test

data class ImprovementPlan(
    val title: String,
    val text: String,
    val data: List<LifeSuccessPlan>
)