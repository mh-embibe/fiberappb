package com.embibe.embibetvapp.model.achieve.achieveFeedback

data class MetaItem(
    val index: Int,
    val meta_data: MetaData,
    val type: String
)