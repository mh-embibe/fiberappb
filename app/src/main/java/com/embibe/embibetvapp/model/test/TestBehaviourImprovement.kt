package com.embibe.embibetvapp.model.test

class TestBehaviourImprovement(
    var previousBehaviour: String,
    var currentBehaviour: String,
    var content_thumb: String = ""
)