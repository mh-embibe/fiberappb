package com.embibe.embibetvapp.model.recommendations

import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.google.gson.annotations.SerializedName

class Recommendations {
    @SerializedName("recommendations")
    val recommendations: List<Results> = arrayListOf()

    @SerializedName("next_video")
    val nextVideo: Content? = null

}