package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class ChapterListData() : Parcelable {

    @SerializedName("code")
    var code: String? = ""

    @SerializedName("format_reference")
    var format_reference: String? = ""

    @SerializedName("name")
    var name: String? = ""

    @SerializedName("bg_thumb")
    var bg_thumb: String? = ""

    @SerializedName("chapter_name")
    var chapter_name: String? = ""

    @SerializedName("lmformat")
    var lmformat: String? = ""

    constructor(parcel: Parcel) : this() {
        code = parcel.readString()
        format_reference = parcel.readString()
        name = parcel.readString()
        bg_thumb = parcel.readString()
        chapter_name = parcel.readString()
        lmformat = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(code)
        parcel.writeString(format_reference)
        parcel.writeString(name)
        parcel.writeString(bg_thumb)
        parcel.writeString(chapter_name)
        parcel.writeString(lmformat)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RevisionList> {
        override fun createFromParcel(parcel: Parcel): RevisionList {
            return RevisionList(parcel)
        }

        override fun newArray(size: Int): Array<RevisionList?> {
            return arrayOfNulls(size)
        }
    }
}