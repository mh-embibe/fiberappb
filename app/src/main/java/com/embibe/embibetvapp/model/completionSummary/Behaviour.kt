package com.embibe.embibetvapp.model.completionSummary

import com.google.gson.annotations.SerializedName


class Behaviour {
    @SerializedName("previous")
    val previous: String? = null

    @SerializedName("current")
    val current: String? = null

    @SerializedName("current_thumb")
    val currentThumb: String? = null
}