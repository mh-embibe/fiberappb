package com.embibe.embibetvapp.model.achieve.updateTruePotential

import com.google.gson.annotations.SerializedName




class IdentifyPotential {
    @SerializedName("select_category")
    val selectCategory: String? = null

    @SerializedName("gender")
    val gender: String? = null

    @SerializedName("differenlty_abled")
    val differenltyAbled: String? = null

    @SerializedName("preparation_type")
    val preparationType: String? = null

    @SerializedName("state_domicile")
    val stateDomicile: String? = null

    @SerializedName("education_qualification")
    val educationQualification: String? = null

    @SerializedName("years_of_experience")
    val yearsOfExperience: String? = null

}