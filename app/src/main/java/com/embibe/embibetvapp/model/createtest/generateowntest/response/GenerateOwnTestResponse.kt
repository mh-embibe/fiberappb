package com.embibe.embibetvapp.model.createtest.generateowntest.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GenerateOwnTestResponse(
    @SerializedName("atg_id")
    var atgId: Int?,
    @SerializedName("request_id")
    var requestId: String?,
    @SerializedName("status")
    var status: String?
) : Parcelable