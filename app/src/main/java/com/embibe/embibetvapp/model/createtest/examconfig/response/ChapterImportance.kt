package com.embibe.embibetvapp.model.createtest.examconfig.response

data class ChapterImportance(
    val chapter_data: List<ChapterData>
)