package com.embibe.embibetvapp.model.achieve.home


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("grade_readiness")
    var gradeReadiness: GradeReadiness?,
    @SerializedName("holistic_readiness")
    var holisticReadiness: HolisticReadiness?,
    @SerializedName("success_readiness")
    var successReadiness: SuccessReadiness?
){
    override fun toString(): String {
        return "Data(gradeReadiness=$gradeReadiness, holisticReadiness=$holisticReadiness, successReadiness=$successReadiness)"
    }
}