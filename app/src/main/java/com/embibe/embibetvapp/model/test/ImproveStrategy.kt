package com.embibe.embibetvapp.model.test

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class ImproveStrategy() : Parcelable {

    @SerializedName("positiveBehaviour")
    var positiveBehaviour: List<TestBehaviour>? = null

    @SerializedName("negativeBehaviour")
    var negativeBehaviour: List<TestBehaviour>? = null

    constructor(parcel: Parcel) : this() {
        positiveBehaviour =
            parcel.readArrayList(TestBehaviour::class.java.classLoader) as ArrayList<TestBehaviour>
        negativeBehaviour =
            parcel.readArrayList(TestBehaviour::class.java.classLoader) as ArrayList<TestBehaviour>
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(positiveBehaviour)
        parcel.writeTypedList(negativeBehaviour)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ImproveStrategy> {
        override fun createFromParcel(parcel: Parcel): ImproveStrategy {
            return ImproveStrategy(parcel)
        }

        override fun newArray(size: Int): Array<ImproveStrategy?> {
            return arrayOfNulls(size)
        }
    }
}

