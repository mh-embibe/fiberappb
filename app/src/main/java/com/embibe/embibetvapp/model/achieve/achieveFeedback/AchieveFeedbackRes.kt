package com.embibe.embibetvapp.model.achieve.achieveFeedback

import com.google.gson.annotations.SerializedName


class AchieveFeedbackRes {

    @SerializedName("paj_id")
    val pajId: String? = null

    @SerializedName("diagnostic_test")
    val diagnosticTest: DiagnosticTest? = null

    @SerializedName("steps")
    val steps: List<Step>? = null

    @SerializedName("current_grade")
    val currentGrade: String? = null

    @SerializedName("potential_grade")
    val potentialGrade: String? = null

    @SerializedName("current_college")
    val currentCollege: String? = null

    @SerializedName("potential_college")
    val potentialCollege: String? = null

    @SerializedName("current_salary")
    val currentSalary: String? = null

    @SerializedName("potential_salary")
    val potentialSalary: String? = null

    @SerializedName("bundle_code")
    val bundleCode: String? = null

    @SerializedName("questions")
    val questions: Int = 0

    @SerializedName("effort_rating")
    val effortRating: Int = 0

    @SerializedName("covered_steps")
    val coveredSteps: Int = 0

    @SerializedName("total_steps")
    val totalSteps: Int = 0

    @SerializedName("behaviours")
    val behaviours: Int = 0

    @SerializedName("videos")
    val videos: Int = 0
}