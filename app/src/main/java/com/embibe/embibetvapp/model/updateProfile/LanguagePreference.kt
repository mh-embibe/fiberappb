package com.embibe.embibetvapp.model.updateProfile

import com.google.gson.annotations.SerializedName


class LanguagePreference {
    @SerializedName("preferred_language")
    var preferredLanguage: Any? = null
}