package com.embibe.embibetvapp.model.diagnostic.test

import com.embibe.embibetvapp.model.diagnostic.LifeSucces

data class LifeSuccessPlan(
    val title: String,
    val text: String,
    val data: List<LifeSucces>
)