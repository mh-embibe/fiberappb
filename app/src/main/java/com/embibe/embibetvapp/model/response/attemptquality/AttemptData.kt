package com.embibe.embibetvapp.model.response.attemptquality

import com.google.gson.annotations.SerializedName


data class AttemptData(

    @SerializedName("text") val text: String,
    @SerializedName("gif") val gif: String,
    @SerializedName("coobo") val coobo: String,
    @SerializedName("value") val value: String
)