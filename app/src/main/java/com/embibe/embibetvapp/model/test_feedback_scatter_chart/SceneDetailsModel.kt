package com.embibe.videoplayer.model

import com.google.gson.annotations.SerializedName

class SceneDetailsModel {
    val seconds: String = ""

    @SerializedName("scene_data")
    val sceneDataModel: SceneDataModel? = null
}