package com.embibe.embibetvapp.model.practise.conceptSequence


import com.google.gson.annotations.SerializedName


class VideoCoverage {
    @SerializedName("total_videos")
    val totalVideos: Any? = null

    @SerializedName("watched_videos")
    val watchedVideos: Int? = null

}