package com.embibe.embibetvapp.model.home.BooksResponse

import com.google.gson.annotations.SerializedName


class BookRes {
    @SerializedName("_id")
    lateinit var id: BookId

    @SerializedName("details")
    lateinit var details: List<Book>
}