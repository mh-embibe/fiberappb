package com.embibe.embibetvapp.model.home.videos

import com.google.gson.annotations.SerializedName


class VideoModel {

    @SerializedName("video_id")
    lateinit var videoId: String

    @SerializedName("category")
    var category: String? = null

    @SerializedName("video_type")
    val videoType: String? = null

    @SerializedName("url")
    val url: String? = null

    @SerializedName("type")
    val type: String? = null

    @SerializedName("source")
    val source: String? = null

    @SerializedName("title")
    val title: String? = null

    @SerializedName("description")
    val description: String? = null

    @SerializedName("thumbnail")
    val thumbnail: String? = null

    @SerializedName("bg_thumbnail")
    val bgThumbnail: String? = null

    @SerializedName("duration")
    val duration: Int? = null

    @SerializedName("rating")
    val rating: Double? = null

    @SerializedName("concept_code")
    val conceptCode: String? = null

    @SerializedName("embiums")
    val embiums: Double? = null
        get() = field ?: 0.0

    @SerializedName("status")
    val status: String? = null

    @SerializedName("subjects")
    val subjects: List<String>? = null
}