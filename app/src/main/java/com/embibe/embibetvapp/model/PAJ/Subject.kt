package com.embibe.embibetvapp.model.PAJ

data class Subject(
    val progress: Int,
    val subject: String
)