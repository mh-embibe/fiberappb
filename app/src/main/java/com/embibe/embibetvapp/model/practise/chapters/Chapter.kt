package com.embibe.embibetvapp.model.practise.chapters

import com.embibe.embibetvapp.model.details.Content
import com.google.gson.annotations.SerializedName


class Chapter {

    @SerializedName("_id")

    val id: String? = null

    @SerializedName("chapter_name")

    val chapterName: String = ""

    @SerializedName("practice")

    val practice: List<String> = arrayListOf()

    @SerializedName("content")

    val content: Content = Content()
}