package com.embibe.embibetvapp.model.auth.login

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class Pack {
    @SerializedName("pack_name")
    @Expose
    val packName: String? = null

    @SerializedName("pack_status")
    @Expose
    val packStatus: String? = null
}