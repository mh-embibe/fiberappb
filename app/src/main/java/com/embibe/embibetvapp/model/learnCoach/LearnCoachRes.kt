package com.embibe.embibetvapp.model.learnCoach

import com.embibe.embibetvapp.newmodel.Content
import com.google.gson.annotations.SerializedName

class LearnCoachRes {
    @SerializedName("current_progress")
    val currentProgress: Int = 0

    @SerializedName("content")
    val content: Content = Content()

}