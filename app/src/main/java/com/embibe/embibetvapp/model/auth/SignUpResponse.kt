package com.embibe.embibetvapp.model.auth

import com.google.gson.annotations.SerializedName


class SignUpResponse {


    @SerializedName("success")
    var success: Boolean = false

    @SerializedName("resource")
    val resource: Resource? = null

    @SerializedName("errors")
    val error: Errors? = null
}

class Errors {
    @SerializedName("login")
    val login: List<String>? = null
}


class Resource {
    @SerializedName("id")
    private val id: Int? = null

    @SerializedName("email")
    private val email: String? = null

    @SerializedName("uid")
    private val uid: String? = null

    @SerializedName("mobile")
    private val mobile: String? = null

    @SerializedName("provider")
    private val provider: String? = null

    @SerializedName("city")
    private val city: String? = null

    @SerializedName("status")
    private val status: String? = null

    @SerializedName("gender")
    private val gender: String? = null

    @SerializedName("subcStatus")
    private val subcStatus: String? = null

    @SerializedName("school")
    private val school: String? = null

    @SerializedName("cohort")
    private val cohort: String? = null

    @SerializedName("user_id")
    private val userId: Any? = null

    @SerializedName("encrypted_password")
    private val encryptedPassword: Any? = null

    @SerializedName("first_name")
    private val firstName: String? = null

    @SerializedName("last_name")
    private val lastName: String? = null

    @SerializedName("is_guest")
    private val isGuest: Boolean? = null

    @SerializedName("exam_year")
    private val examYear: String? = null

    @SerializedName("verified_mobile")
    private val verifiedMobile: String? = null

    @SerializedName("verified_email")
    private val verifiedEmail: String? = null

    @SerializedName("primary_organization_id")
    private val primaryOrganizationId: Int? = null

    @SerializedName("unlocked_custom_pack_ids")
    private val unlockedCustomPackIds: Any? = null

    @SerializedName("embium_count")
    private val embiumCount: Any? = null

    @SerializedName("applied_rankup")
    private val appliedRankup: Boolean? = null

    @SerializedName("jump_pack_id")
    private val jumpPackId: Any? = null

    @SerializedName("rankup_pack_id")
    private val rankupPackId: Any? = null

    @SerializedName("ab_version")
    private val abVersion: Int? = null

    @SerializedName("profile_pic")
    private val profilePic: String? = null

    @SerializedName("goal_code")
    private val goalCode: String? = null

    @SerializedName("selected_goal_code")
    private val selectedGoalCode: String? = null

    @SerializedName("selected_goal_id")
    private val selectedGoalId: Int? = null

    @SerializedName("primary_goal_id")
    private val primaryGoalId: Any? = null

    @SerializedName("secondary_exam_goal_id")
    private val secondaryExamGoalId: Any? = null

    @SerializedName("secondary_exams_preparing_for")
    private val secondaryExamsPreparingFor: Any? = null

    @SerializedName("exam_code")
    private val examCode: Any? = null

    @SerializedName("exam_id")
    private val examId: Any? = null

    @SerializedName("mandatory_reset_password")
    private val mandatoryResetPassword: Boolean? = null

    @SerializedName("highest_product")
    private val highestProduct: Any? = null

    @SerializedName("user_type")
    private val userType: Any? = null

    @SerializedName("city_id")
    private val cityId: Any? = null

    @SerializedName("state_id")
    private val stateId: Any? = null

    @SerializedName("_board")
    private val board: String? = null

    @SerializedName("klass")
    private val klass: String? = null

    @SerializedName("latitude")
    private val latitude: Any? = null

    @SerializedName("longitude")
    private val longitude: Any? = null

    @SerializedName("guardian_type")
    private val guardianType: Any? = null

    @SerializedName("grade")
    private val grade: String? = null

    @SerializedName("primary_exam_code")
    private val primaryExamCode: String? = null

    @SerializedName("primary_goal_code")
    private val primaryGoalCode: String? = null

    @SerializedName("logout_redirect")
    private val logoutRedirect: Any? = null

    @SerializedName("dream_colleges")
    private val dreamColleges: Any? = null

    @SerializedName("stream_ids")
    private val streamIds: Any? = null

    @SerializedName("master_login")
    private val masterLogin: Any? = null

    @SerializedName("parent_name")
    private val parentName: Any? = null

    @SerializedName("parent_mobile")
    private val parentMobile: Any? = null

    @SerializedName("parent_email")
    private val parentEmail: Any? = null

    @SerializedName("pincode")
    private val pincode: Any? = null

    @SerializedName("institute")
    private val institute: Any? = null

    @SerializedName("physical_disability")
    private val physicalDisability: Boolean? = null

    @SerializedName("_category")
    private val category: Any? = null

    @SerializedName("is_fiber_child")
    private val isFiberChild: Boolean? = null
}


