package com.embibe.embibetvapp.model.PAJ

data class AchieveSection(
    val content: List<Content>,
    val data: Data,
    val section: String,
    val section_id: Int,
    val section_name: String
)