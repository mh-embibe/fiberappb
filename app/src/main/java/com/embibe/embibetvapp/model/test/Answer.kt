package com.embibe.embibetvapp.model.test

data class Answer(
    val body: String,
    val code: String,
    val position: Double
)