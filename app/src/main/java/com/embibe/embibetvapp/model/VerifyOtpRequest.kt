package com.embibe.embibetvapp.model

import com.google.gson.annotations.SerializedName

class VerifyOtpRequest(
    @SerializedName("otp") val otp: String,
    @SerializedName("login") val email_mobile: String
)