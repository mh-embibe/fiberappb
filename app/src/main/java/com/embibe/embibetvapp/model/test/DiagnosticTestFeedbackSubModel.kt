package com.embibe.embibetvapp.model.test

class DiagnosticTestFeedbackSubModel(
    var title: String,
    var time: String,
    var background: Int,
    var btnText: String
)