package com.embibe.embibetvapp.model.achieve

import android.graphics.PointF

data class LinePoint(val startPoint: PointF, val endPoint: PointF)