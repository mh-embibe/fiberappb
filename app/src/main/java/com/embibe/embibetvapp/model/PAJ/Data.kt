package com.embibe.embibetvapp.model.PAJ

data class Data(
    val grade_readiness: Readiness,
    val holistic_readiness: Readiness,
    val success_readiness: Readiness
)