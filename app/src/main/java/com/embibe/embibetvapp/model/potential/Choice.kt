package com.embibe.embibetvapp.model.potential

import com.google.gson.annotations.SerializedName


class Choice {

    @SerializedName("grade")
    val grade: String? = null

    @SerializedName("grade_point")
    val gradePoint: Int? = null

    @SerializedName("marks_max")
    val marksMax: Int? = null

    @SerializedName("mark_min")
    val markMin: Int? = null
}