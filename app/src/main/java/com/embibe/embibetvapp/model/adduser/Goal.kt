package com.embibe.embibetvapp.model.adduser

import com.google.gson.annotations.SerializedName

data class Goal(
    @SerializedName("code") val code: String,
    @SerializedName("name") val name: String,
    @SerializedName("slug") val slug: String,
    @SerializedName("old_id") val old_id: Int,
    @SerializedName("xpath") val xpath: String,
    @SerializedName("exams") val exams: ArrayList<Exam>,
    @SerializedName("default") val default: Boolean,
    @SerializedName("supported") val supported: Boolean
)