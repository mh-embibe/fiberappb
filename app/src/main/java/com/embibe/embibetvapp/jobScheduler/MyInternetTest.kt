package com.embibe.embibetvapp.jobScheduler

import android.app.job.JobInfo
import android.app.job.JobParameters
import android.app.job.JobScheduler
import android.app.job.JobService
import android.content.ComponentName
import android.content.Context
import android.util.Log
import com.embibe.embibetvapp.application.App


class MyInternetTest {
    companion object {
        fun scheduleJob() {
            Log.e("MyInternetTest", "myInternetTest")
            val scheduler =
                App.context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
            val job =
                JobInfo.Builder(1, ComponentName(App.context, NetworkSchedulerService::class.java))
                    .setMinimumLatency(1000)
                    .setPersisted(true)
                    .build()
            scheduler.schedule(job)
        }
    }

    class NetworkSchedulerService : JobService() {
        val TAG = "NetworkSchedulerService"

        override fun onStartJob(params: JobParameters): Boolean {
//            scheduleJob()

            val check = isInternetAvailable()

            connectivityReceiverListener?.onNetworkConnectionChanged(check)
            Log.e(TAG, "internet: $check")
            if (check) {
                jobFinished(params, false)
            }
            return true
        }

        override fun onStopJob(params: JobParameters): Boolean {
            return false
        }

        private fun isInternetAvailable(): Boolean {

            return try {
//            val ipAddr: InetAddress = InetAddress.getByName("www.google.com")
//            //You can replace it with your name
//            return !ipAddr.equals("")
                val command = "ping -c 1 google.com"
                if (Runtime.getRuntime().exec(command).waitFor() == 0) {
                }
                return Runtime.getRuntime().exec(command).waitFor() == 0

            } catch (e: Exception) {
                Log.e(TAG, " isInternetAvailable ${e.message}")
                false
            }
        }

        interface ConnectivityReceiverListener {
            fun onNetworkConnectionChanged(isConnected: Boolean)
        }

        companion object {
            var connectivityReceiverListener: ConnectivityReceiverListener? = null
        }
    }
}