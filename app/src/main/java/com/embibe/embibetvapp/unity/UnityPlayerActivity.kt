package com.embibe.embibetvapp.unity

import android.annotation.SuppressLint
import android.content.ComponentCallbacks2
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.media.AudioManager
import android.os.Bundle
import android.os.Process
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.Window
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.CONTENT
import com.embibe.embibetvapp.constant.AppConstants.CONTENT_TYPE
import com.embibe.embibetvapp.constant.AppConstants.COOBO
import com.embibe.embibetvapp.constant.AppConstants.COOBO_URL
import com.embibe.embibetvapp.constant.AppConstants.DETAILS_ID
import com.embibe.embibetvapp.constant.AppConstants.DURATION_LENGTH
import com.embibe.embibetvapp.constant.AppConstants.FORMAT_ID
import com.embibe.embibetvapp.constant.AppConstants.IS_RECOMMENDATION_ENABLED
import com.embibe.embibetvapp.constant.AppConstants.LEARNMAP_CODE
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_FORMAT_NAME
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_NAME
import com.embibe.embibetvapp.constant.AppConstants.SLIDE_COUNT
import com.embibe.embibetvapp.constant.AppConstants.TOPIC_LEARN_PATH
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CLOSE_DIALOG
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CONCEPT_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_ID
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.activity.RecommendationActivity
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.ExceptionHandler
import com.embibe.embibetvapp.utils.Utils.getContent
import com.embibe.embibetvapp.utils.Utils.showAlertDialog
import com.embibejio.coreapp.utils.Utils.trackCooboEvent
import com.unity3d.player.IUnityPlayerLifecycleEvents
import com.unity3d.player.UnityPlayer
import com.unity3d.player.UnityPlayer.UnitySendMessage
import org.json.JSONException
import org.json.JSONObject
import java.io.File

@SuppressLint("Registered")
class UnityPlayerActivity : FragmentActivity(), IUnityPlayerLifecycleEvents,
    ErrorScreensBtnClickListener {
    private lateinit var contentData: Content
    private lateinit var mUnityPlayer: UnityPlayer // don't change the name of this variable; referenced from native code: UnityPlayer? = null

    companion object {
        lateinit var coobooUrl: String
        private var conceptId: String? = ""
        private var learnPathName: String? = ""
        private var learnPathFormatName: String? = ""
        private var formatId: String? = ""
        private var unityInitiated: Boolean = false
        private var downloadCompleted: Boolean = false

        @SuppressLint("StaticFieldLeak")
        private lateinit var context: Context
        private lateinit var errorScreensBtnClickListener: ErrorScreensBtnClickListener
        private var id: String? = ""
        private var topicLearnPathName = ""
        private var learnMapCode = ""
        private var isRecommendation = false
        private var slideCount = 1
        private var durationLength = 0
        private lateinit var homeViewModel: HomeViewModel
        private lateinit var supportFM: FragmentManager
        var currentFolderUri = ""

        // Check for mute using isStreamMute(int)
        private val muteState: Boolean
            get() {
                try {
                    val audio =
                        App.activityContext.getSystemService(Context.AUDIO_SERVICE) as AudioManager
                    // Check for mute using isStreamMute(int)
                    if (audio.isStreamMute(AudioManager.STREAM_MUSIC)) {
                        return true
                    }
                } catch (e: Exception) {
                }
                return false
            }

        @JvmStatic
        fun moveToRecommendation() {
            if (isRecommendation) {
                val recommendationIntent = Intent(context, RecommendationActivity::class.java)
                with(recommendationIntent) {
                    putExtra(CONTENT_TYPE, COOBO)
                    putExtra(VIDEO_ID, id)
                    putExtra(VIDEO_CONCEPT_ID, conceptId)
                    putExtra(COOBO_URL, coobooUrl)
                    putExtra(DURATION_LENGTH, durationLength)
                    putExtra(SLIDE_COUNT, slideCount)
                    putExtra(LEARNMAP_CODE, learnMapCode)
                    putExtra(AppConstants.BOOK_ID, "")
                }
                context.startActivity(recommendationIntent)
            }
        }


        @JvmStatic
        fun PushUnityStatus(statusJson: String) {
            try {
                val obj = JSONObject(statusJson)
                val data = obj.getJSONObject(AppConstants.DATA)
                val status = data.getString(AppConstants.STATUS)
                var slideCount = 0
                if (obj.has(AppConstants.CURRENT_SLIDE))
                    slideCount = obj.getInt(AppConstants.CURRENT_SLIDE)

                homeViewModel.updatedContentStatus(
                    id!!,
                    AppConstants.COOBO,
                    slideCount,
                    status,
                    topicLearnPathName,
                    learnPathName ?: "",
                    learnPathFormatName ?: "",
                    formatId ?: "",
                    conceptId ?: "",
                    durationLength.toString()
                )
                Log.w("updatedContentStatus ", "slide_count :$slideCount")
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        @JvmStatic
        fun getlocale() = context.resources.configuration.locales[0].language

        @JvmStatic
        fun getDownloadStatus() = downloadCompleted

        @JvmStatic
        fun PushUnityEvents(eventJson: String) {
            Log.d("Android-PushUnityEvents", eventJson)
            try {
                trackCooboEvent(JSONObject(eventJson), conceptId!!, muteState, learnMapCode)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        @JvmStatic
        fun CallFromUnity(s: String?) {
        }

        @JvmStatic
        fun killScreen() {
        }

        @JvmStatic
        fun showQuitPopUp(uri: String) {
            currentFolderUri = uri
            showAlertDialog(VIDEO_CLOSE_DIALOG, supportFM, errorScreensBtnClickListener)
        }

        @JvmStatic
        fun getCooboJson() = run {
            unityInitiated = true
            // currentJSONObject.toString()
        }

        @JvmStatic
        fun getCurrentSlide(totalSlide: Int): Int {
            try {
                if (durationLength != 0 && totalSlide != 0 && slideCount != 0) {
                    val time = durationLength / totalSlide
                    val currentSlide = (slideCount - 1) / time + 1
                    if (currentSlide > 0) return if (currentSlide <= totalSlide) currentSlide else totalSlide
                }
            } catch (e: Exception) {
            }
            return 1
        }


        @JvmStatic
        fun getCooboUrl(): String? {
            return if (coobooUrl.contains("https://coobo.embibe.com/api/")) {
                coobooUrl.replace("api/view", "api/demo")
            } else "https://coobo.embibe.com/api/demo/$coobooUrl"
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("unity_splah_image", "" + R.drawable.unity_static_splash)
        Log.d("game_view_content_description", "" + R.string.game_view_content_description)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        Thread.setDefaultUncaughtExceptionHandler(ExceptionHandler(this))
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        context = this@UnityPlayerActivity
        supportFM = supportFragmentManager
        coobooUrl = intent.extras!!.getString(COOBO_URL)!!
        Log.d("coobooUrl", coobooUrl)
        mUnityPlayer = UnityPlayer(this, this)
        setContentView(mUnityPlayer)
        //FilterAssetJSONObject
        //downloadAssets()
        errorScreensBtnClickListener = this
        with(intent) {
            //putExtra("unity", updateUnityCommandLineArguments(getStringExtra("unity")!!))
            mUnityPlayer.requestFocus()
            if (hasExtra(VIDEO_CONCEPT_ID))
                conceptId = getStringExtra(VIDEO_CONCEPT_ID)
            if (hasExtra(DETAILS_ID))
                id = getStringExtra(DETAILS_ID)
            if (hasExtra(SLIDE_COUNT))
                slideCount = extras!!.getInt(SLIDE_COUNT)
            if (hasExtra(DURATION_LENGTH))
                durationLength = extras?.getInt(DURATION_LENGTH)!!
            if (hasExtra(IS_RECOMMENDATION_ENABLED))
                isRecommendation = getBooleanExtra(IS_RECOMMENDATION_ENABLED, false)
            if (hasExtra(TOPIC_LEARN_PATH))
                topicLearnPathName = getStringExtra(TOPIC_LEARN_PATH) ?: ""
            if (hasExtra(LEARNMAP_CODE))
                try {
                    learnMapCode = getStringExtra(LEARNMAP_CODE)!!
                } catch (e: Exception) {

                }
            if (hasExtra(CONTENT)) {
                contentData = getContent(this)
                formatId = contentData.learning_map.format_id
                learnPathName = contentData.learnpath_name
                learnPathFormatName = contentData.learnpath_format_name
            }
            if (hasExtra(LEARN_PATH_NAME))
                learnPathName = getStringExtra(LEARN_PATH_NAME) ?: ""
            if (hasExtra(LEARN_PATH_FORMAT_NAME))
                learnPathFormatName = getStringExtra(LEARN_PATH_FORMAT_NAME) ?: ""
            if (hasExtra(FORMAT_ID))
                formatId = getStringExtra(FORMAT_ID) ?: ""
        }
    }

    // Quit Unity
    override fun onDestroy() {
        mUnityPlayer.destroy()
        super.onDestroy()
    }

    // Pause Unity
    override fun onPause() {
        super.onPause()
        mUnityPlayer.pause()
    }

    // Resume Unity
    override fun onResume() {
        super.onResume()
        mUnityPlayer.resume()
    }

    // Low Memory Unity
    override fun onLowMemory() {
        super.onLowMemory()
        mUnityPlayer.lowMemory()
    }

    // Trim Memory Unity
    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        if (level == ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL) {
            mUnityPlayer.lowMemory()
        }
    }

    // This ensures the layout will be correct.
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mUnityPlayer.configurationChanged(newConfig)
    }

    // Notify Unity of the focus change.
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        mUnityPlayer.windowFocusChanged(hasFocus)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
        mUnityPlayer.newIntent(intent)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        UnitySendMessage(
            "EventSystem", "OnKeyUp",
            KeyEvent.keyCodeToString(event.keyCode)
        )
        return mUnityPlayer.injectEvent(event)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        Log.i("onKeyDown", "xxxxxxxxxxxxx Before sample " + event.keyCharacterMap)
        if (event.repeatCount < 2)
            UnitySendMessage(
                "EventSystem", "OnKeyDown",
                KeyEvent.keyCodeToString(event.keyCode)
            )
        UnitySendMessage(
            "EventSystem", "OnKey",
            KeyEvent.keyCodeToString(event.keyCode)
        )
        return mUnityPlayer.injectEvent(event)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return mUnityPlayer.injectEvent(event)
    }

    override fun onGenericMotionEvent(event: MotionEvent): Boolean {
        return mUnityPlayer.injectEvent(event)
    }

    @SuppressLint("RestrictedApi")
    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        return if (event.action == KeyEvent.ACTION_MULTIPLE) mUnityPlayer.injectEvent(
            event
        ) else super.dispatchKeyEvent(event)
    }

    override fun onUnityPlayerUnloaded() {
        moveTaskToBack(true)
    }

    override fun onUnityPlayerQuitted() {
        Process.killProcess(Process.myPid())
    }

    override fun screenUpdate(type: String) {
        if (type == AppConstants.CLOSE_VIDEO) {
            deleteSubFolders(currentFolderUri)
            finish()
        }
    }

    private fun deleteSubFolders(uri: String) {
        val currentFolder = File(uri)

        val files = currentFolder.listFiles() ?: return
        for (f in files) {
            if (f.isDirectory) {
                deleteSubFolders(f.toString())
            }
            f.delete()
        }
    }

}
