package com.embibe.embibetvapp.constant

object ApiConstants {

    const val VIMEO_BEARER = "bearer "
    const val VIMEO_AUTHORIZATION_TOKEN = "b736a13d36146a6feadaa125be4e6044"
    const val VIMEO_HEADER = "application/vnd.vimeo.*+json;version=3.4"
    const val VIMEO_BASE_URL = "https://api.vimeo.com/videos/"

    // Base URLS
    const val BASE_URL_PREPRODMS = "https://preprodms.embibe.com"
    // const val BASE_URL_PREPRODMS = "http://52.172.156.139:5000"


    const val BASE_URL_STAGING = "https://staging1ms.embibe.com"
    private const val PATH_FIBER_MS = "/fiber_ms"
    private const val PATH_USER_MS = "/user_ms"
    private const val PATH_ACHIEVE_MS = "/achieve_ms"
    private const val PATH_TEST_MS = "/testsubmission_ms_fiber"
    private const val PATH_CONTENT_MS = "/content_ms"
    private const val PATH_CONTENT_MS_FIBER = "/content_ms_fiber"
    private const val PATH_OTP_MS = "/otp_ms"
    private const val PATH_USER_AUTH = "/user_auth"
    private const val PATH_USER_PROFILE = "/user_profile"
    private const val PATH_VERSION = "/v1"
    private const val PATH_FIBER_MS_VERSION = "$PATH_FIBER_MS$PATH_VERSION"
    private const val PATH_SECTIONS = "/sections"

    /*ZLA jio api*/
    const val URL_JIO_STB_USER = "http://api-sit.jio.com/ftth/v2/users/me"

    //login
    const val URL_VERIFY_OTP = "$PATH_OTP_MS/v1/verify_otp"
    const val URL_USER_EXIST = "$PATH_FIBER_MS/user/profile/exists"

    const val URL_SEND_OTP_SIGNUP = "$PATH_OTP_MS/v1/send_otp"
    const val URL_AUTH_OTP_SIGN_IN = "$PATH_USER_AUTH/auth/sign_in"
    const val URL_LINKED_PROFILES = "$PATH_USER_AUTH/profile/connected_profiles"

    //adduser
//    const val URL_ADD_USER = "$PATH_USER_AUTH/2/{userid}/add"
    const val URL_ADD_USER = "$PATH_USER_AUTH/add"
    const val URL_UPDATE_PROFILE = "$PATH_USER_PROFILE/edit_profile"

    const val URL_GOALS_EXAMS = "$PATH_CONTENT_MS/v1/embibe/en/fiber-countries-goals-exams"


    /*home*/
    const val URL_HOME = "$PATH_FIBER_MS_VERSION/home"
    const val URL_HOME_PRACTICE = "$PATH_FIBER_MS_VERSION/home/practise"
    const val URL_HOME_SUBJECT = "$PATH_FIBER_MS_VERSION/home/{subject}"
    const val URL_HOME_QUICKLINKS = "$PATH_FIBER_MS_VERSION/userHome"
    const val URL_HOME_TEST = "$PATH_FIBER_MS/home/test"

    /*home pagination*/
    const val URL_HOME_SECTION = "$PATH_FIBER_MS_VERSION/home$PATH_SECTIONS"
    const val URL_HOME_PRACTICE_SECTION = "$PATH_FIBER_MS_VERSION/home/practise$PATH_SECTIONS"
    const val URL_HOME_SUBJECT_SECTION = "$PATH_FIBER_MS_VERSION/home/{subject}$PATH_SECTIONS"
    const val URL_HOME_QUICKLINKS_SECTION = "$PATH_FIBER_MS_VERSION/userHome$PATH_SECTIONS"

    const val URL_RELATED_CONCEPTS = "$PATH_FIBER_MS/concepts/connected/{conceptId}?"
    const val URL_MORE_CONCEPTS = "$PATH_FIBER_MS/concepts/more/{conceptId}?"
    const val URL_SUGGESTIONS = "$PATH_FIBER_MS/search/suggestions?"
    const val URL_SEARCH = "$PATH_FIBER_MS/search/results?"
    const val URL_PREREQUISITES = "$PATH_FIBER_MS/concepts/prerequisites/{conceptId}?"
    const val URL_NEXT_VIDEO = "$PATH_FIBER_MS/next/{contentId}?"
    const val URL_AUTO_PLAY_VIDEO = "$PATH_FIBER_MS/autoplay/{conceptId}?"
    const val URL_RECOMMENDED_LEARNING = "$PATH_FIBER_MS/chapters/learning-objects?"
    const val URL_LAST_WATCHED_CONTENT = "$PATH_FIBER_MS/api/v1/last-watched-content/book?"

    /*update content status like and Bookmark*/
    const val URL_UPDATE_CONTENT_STATUS = "$PATH_FIBER_MS/content-status?"
    const val URL_GET_CONTENT_STATUS = "$PATH_FIBER_MS/content-status/{content_id}/{content_type}?"
    const val URL_LIKE = "$PATH_FIBER_MS/like?"
    const val URL_GET_LIKE_STATUS = "$PATH_FIBER_MS/like?"
    const val URL_BOOKMARK = "$PATH_FIBER_MS/bookmark?"
    const val URL_GET_BOOKMARK_STATUS = "$PATH_FIBER_MS/bookmark?"
    const val URL_GET_BOOKMARKED_CONTENTS = "$PATH_FIBER_MS/bookmark/{content_type}?"
    const val URL_OVERALL_ACCURACY = "/de/jf_analyse/overall_accuracy"
    const val URL_CONCEPTS_COVERAGE = "/de/jf_analyse/concepts_coverage"
    const val URL_BEHAVIOUR_METER = "/de/jf_analyse/behaviour_meter"
    const val URL_BOOKMARK_TEST = "$PATH_FIBER_MS/event"
    const val URL_TEST_DETAILS = "$PATH_FIBER_MS/test/detail?"
    const val URL_TEST_ATTEMPTS = "$PATH_FIBER_MS/test/feedback/attempts?"
    const val URL_TEST_QUESTIONS = "$PATH_TEST_MS/v1/test/{test_code}/questions?"
    const val URL_TEST_SKILLS = "$PATH_FIBER_MS/test/feedback/skills?"
    const val URL_TEST_ACHIEVE = "$PATH_FIBER_MS/test/feedback/achieve?"
    const val URL_CHAPTER_PRACTICES = "$PATH_FIBER_MS/chapterPractices"
    const val URL_MORE_TOPICS = "$PATH_FIBER_MS/topics/more/{topic_name}?"
    const val URL_TEST_STATUS = "$PATH_TEST_MS/v1/test/{test_code}/session?"

    /* Learn Summary paths */
    const val URL_TESTS_FOR_CHAPTER = "$PATH_FIBER_MS/home/filter-test"
    const val URL_PRACTICES_FOR_CHAPTER = "$PATH_FIBER_MS/chapterPractices"
    const val URL_VIDEOS_FOR_CHAPTER = "$PATH_FIBER_MS/chapters/learning-objects?"
    const val URL_VIDEOS_FOR_CHAPTER_V1 = "$PATH_FIBER_MS/v1/chapters/learning-objects?"
    const val URL_PREREQUISITES_FOR_CHAPTER =
        "$PATH_FIBER_MS/chapter/prerequisites"
    const val URL_TOPICS_FOR_CHAPTER = "$PATH_FIBER_MS/chapterTopics"
    const val URL_SINCERITY_SCORE = "$PATH_FIBER_MS/sincerity_score"

    /* Test Summary paths */
    const val URL_RECOMMENDED_LEARNING_FOR_TEST =
        "$PATH_FIBER_MS/home/test-recommended-learn"
    const val URL_RECOMMENDED_PRACTICE_FOR_TEST =
        "$PATH_FIBER_MS/home/test-chapter-practice"
    const val URL_MORE_TESTS = "$PATH_FIBER_MS/home/filter-test"
    const val URL_TEST_KG_GRAPH = "$PATH_FIBER_MS/home/test-kg-graph"

    /* Book Summary paths */
    const val URL_BOOKS_AVAILABLE_FOR_PRACTICE = "$PATH_FIBER_MS/search/books?"

    /**Search Api's**/
    const val URL_SEARCH_RECOMMENDED_LEARNING =
        "$PATH_FIBER_MS/search/practice/recommended/learning"
    const val URL_SEARCH_TOPIC_FOR_PRACTICE = "$PATH_FIBER_MS/search/practice/chapter/topics"
    const val URL_SEARCH_ALL_VIDEOS = "$PATH_FIBER_MS/search/learn/chapter/all-videos"
    const val URL_SEARCH_CHAPTER_TOPICS = "$PATH_FIBER_MS/search/learn/chapter/all-topics"
    const val URL_SEARCH_CHAPTER_PREREQUISITE =
        "$PATH_FIBER_MS/search/learn/chapter/all-prerequisite"
    const val URL_SEARCH_CHAPTER_PRACTISE = "$PATH_FIBER_MS/search/learn/chapter/practice"
    const val URL_SEARCH_TESTS_ON_CHAPTER = "$PATH_FIBER_MS/home/learn-summary-test"
    const val URL_SEARCH_MORE_TESTS = "$PATH_FIBER_MS/home/more-search-test"
    const val URL_SEARCH_BOOKMARKED_QUESTIONS =
        "$PATH_FIBER_MS/search/practice/chapter/bookmarked/questions"


    /* Topic Summary paths */
    const val URL_TESTS_FOR_TOPIC = "$PATH_FIBER_MS/home/filter-test"
    const val URL_PRACTICES_FOR_TOPIC = "$PATH_FIBER_MS/topicPractice"
    const val URL_VIDEOS_FOR_TOPIC = "$PATH_FIBER_MS/topic/learning-objects?"
    const val URL_PREREQUISITES_FOR_TOPIC =
        "$PATH_FIBER_MS/topic/prerequisites"
    const val URL_RELATED_TOPICS = "$PATH_FIBER_MS/topic/related"


    const val URL_GET_ANNOTATIONS = "/fiber_ms/annotations?"


    /*test*/
    const val URL_EXAM_CONFIG = "$PATH_FIBER_MS/home/exam_config"
    const val URL_GENERATE_TEST = "$PATH_FIBER_MS/v1/atg/test"
    const val URL_TEST_PROGRESS = "$PATH_FIBER_MS/v1/atg/progress"
    const val URL_TEST_CONFIGURATION = "$PATH_FIBER_MS/exam/configurations"
    const val URL_TEST_READINESS = "$PATH_FIBER_MS/home/test-readiness-info"

    /* Attempt Quality paths */
    const val URL_ATTEMPT_QUALITY = "$PATH_FIBER_MS/attempt_quality?"


    const val URL_CONCEPT_SEQUENCE = "$PATH_FIBER_MS/concept_sequence/coverage?"

    /*Feedback */
    const val URL_FEEDBACK_QUESTION_SUMMARY = "$PATH_FIBER_MS/test/question/summary"
    const val URL_FEEDBACK_TOPIC_SUMMARY = "$PATH_FIBER_MS/test/feedback/topic-summary"

    /*Achieve*/
    const val URL_ACHIEVE_HOME = "$PATH_ACHIEVE_MS/home"
    const val URL_ACHIEVE_HOME_SUBJECT = "$PATH_ACHIEVE_MS/home/{subject}"
    const val URL_ACHIEVE_EDUCATION_QUALIFICATION =
        "$PATH_ACHIEVE_MS/meta/data/education_qualifications/default"
    const val URL_ACHIEVE_JOB_OPTIONS = "$PATH_ACHIEVE_MS/meta/data/job_options/default"
    const val URL_ACHIEVE_COLLEGES = "$PATH_ACHIEVE_MS/meta/data/colleges/default"
    const val URL_ACHIEVE_STATES = "$PATH_ACHIEVE_MS/meta/data/states/default"
    const val URL_ACHIEVE_QUESTIONNAIRE_TRUE_POTENTIAL_PREPG =
        "$PATH_ACHIEVE_MS/questionnaire/true_potential/prepg"
    const val URL_ACHIEVE_QUESTIONNAIRE_TRUE_POTENTIAL_PREUG =
        "$PATH_ACHIEVE_MS/questionnaire/true_potential/preug"
    const val URL_ACHIEVE_QUESTIONNAIRE_TRUE_POTENTIAL_K12 =
        "$PATH_ACHIEVE_MS/questionnaire/true_potential/k12"
    const val URL_ACHIEVE_CRUNCHING_VARIABLES = "$PATH_ACHIEVE_MS/meta/crunching_variables"

    const val URL_ACHIEVE_DIAGNOSTIC_TESTS = "$PATH_ACHIEVE_MS/diagnostic-tests?"
    const val URL_ACHIEVE_DIAGNOSTIC_MOCK_TESTS =
        "/content_ms_fiber/v2/mocktest-bundles/get-diagnostic-tests?"
    const val URL_ACHIEVE_UPDATE_USER = "$PATH_USER_PROFILE/edit_profile"
    const val URL_ACHIEVE_PAJ = "$PATH_ACHIEVE_MS/paj"
    const val URL_ACHIEVE_PERCENTAGE_CONNECTIONS = "$PATH_ACHIEVE_MS/kg/percentage-connections"
    const val URL_ACHIEVE_KEY_RELATIONS = "$PATH_ACHIEVE_MS/kg/key-relations"
    const val URL_ACHIEVE_READINESS = "$PATH_FIBER_MS/readiness?"
    const val URL_ACHIEVE_FUTURE_SUCCESS = "$PATH_FIBER_MS/futureSuccess?"
    const val URL_ACHIEVE_SUCCESS_STORIES = "$PATH_FIBER_MS/successStories?"
    const val URL_ACHIEVE_ATTEMPTS = "$PATH_FIBER_MS/achieve/feedback/attempts?"
    const val URL_ACHIEVE_SKILLS = "$PATH_FIBER_MS/achieve/feedback/skills?"
    const val URL_ACHIEVE_BEHAVIOURS = "$PATH_FIBER_MS/achieve/feedback/behaviours?"
    const val URL_ACHIEVE_FEEDBACK_TOPIC_SUMMARY = "$PATH_FIBER_MS/achieve/feedback/topic-summary?"
    const val URL_ACHIEVE_FEEDBACK_RANKS_COMPARISON = "$PATH_FIBER_MS/achieve/feedback/ranks-comparison?"
    const val URL_ACHIEVE_POST_PAJ_TEST_CODE = "/de/achieve/post_paj_test_code?PAJ_ID=ed1e6f2f-baeb-4096-8101-26b7642e9761&bundle_code=mb36599?"


    const val API_CODE_NO_NETWORK = 11


}