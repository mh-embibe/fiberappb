package com.embibe.embibetvapp.constant

import com.embibe.embibetvapp.BuildConfig


object AppConstants {

    const val YOUTUBE_PACKAGE_NAME = "com.jio.yt"
    const val VIDEO_FOR_CHAPTER: String = "Video"
    const val PRACTICE_TOPICS: String = "listPracticeTopics"
    const val ISCOOBO_UPDATED: String = "iscoobo_updated"
    const val ISCOOBO_UPDATED_FOR_QUICKLINKS: String = "iscoobo_updated_for_quicklinks"
    const val BOOK_ID: String = "book_id"
    const val CONTENT_IDS: String = "content_ids"
    const val SOURCE: String = "source"
    const val SOURCE_BOOK_TOC: String = "book-toc"
    const val SOURCE_EMBIBE_SYLLABUS: String = "embibe-syllabus"
    const val EMBIBE_SYLLABUS: String = "EMBIBE_SYLLABUS"
    const val GOAL_AND_EXAMS: String = "goal_and_exams"
    const val TEST_DETAILS_MODEL: String = "test_details_model"
    const val USER_DATA: String = "user_data"
    const val CURRENT_PROFILE_PIC: String = "current_profile_pic"
    var ISPROFILEUPDATED: Boolean = false
    var TEMP_EMBIBE_TOKEN: String = "temp_embibe_token"
    var ACTIVITY_TYPE_BOOKMARK: String = "Bookmark"
    var ACTIVITY_TYPE_LIKE: String = "Like"
    var ACTIVITY_TYPE_VIEW: String = "View"
    const val LEARN: String = "learn"
    const val PRACTICE: String = "practice"
    const val ACHIEVE: String = "achieve"
    const val TEST = "test"
    const val CHAPTER_TEST = "chapter test"
    const val TEST_XPATH = "test_xpath"
    const val TEST_CODE = "test_code"
    const val IS_DIAGNOSTIC = "is_diagnostic"
    const val IS_ACHIEVE = "is_achieve"
    const val IS_DIAGNOSTIC_TEST_COMPLETED = "is_diagnostic_test_completed"
    const val PREVIOUS_TEST_CODE = "previous_test_code"
    const val PROFILE = "profile"
    const val QUICK_LINKS: String = "quick_links"
    const val SUBJECT_LEARN: String = "subject_learn"
    const val SUBJECT_PRACTICE: String = "subject_practice"
    const val SUBJECT_TEST: String = "subject_test"
    const val SUBJECT_QUICK_LINKS: String = "subject_quick_links"

    const val ISHOME_INITIALIZED: String = "isHomeInitialized"
    const val PERCENTAGE: String = "%"
    const val JIO_STB_HEADER_KEY_X_MAC: String = "x-mac"
    const val JIO_STB_HEADER_KEY_APP_NAME: String = "app-name"
    const val JIO_STB_HEADER_KEY_X_API_KEY: String = "x-api-key"
    const val JIO_STB_HEADER_KEY_IP: String = "x-mme-ip"
    const val JIO_STB_HEADER_VALUE_X_MAC: String = "405862999990475"
    const val JIO_STB_HEADER_VALUE_APP_NAME: String = "RJIL_JioSaavn"
    const val JIO_STB_HEADER_VALUE_X_API_KEY: String = "l7xxd73dda47218a4482905b496d77dd2e14"
    const val JIO_STB_HEADER_VALUE_IP: String = "2405:201:fffb:0:1::42"

    const val SUBSCRIBER_ID: String = "subscriber_id"
    const val ISUSEREXIST: String = "isUserExist"

    const val ALERT_MSG_FOR_BOOKMARK: String = "Unable to update Bookmark"
    const val ALERT_MSG_FOR_LIKE: String = "Unable to update LIKE"

    const val PLAYER_TYPE_EXOPLAYER = "exoplayer"
    const val PLAYER_TYPE_YOUTUBE = "youtube"
    const val MORE_TOPICS: String = "more_topics"
    const val RELATED_CONCEPTS: String = "related_concepts"
    const val PRACTICE_ON_THIS_CHAPTER: String = "practice_on_this_chapter"
    const val PRACTICE_ON_THIS_TOPIC: String = "practice_on_this_topic"
    const val TEST_ON_THIS_CHAPTER: String = "test_on_this_chapter"
    const val TEST_ON_THIS_TOPIC: String = "test_on_this_topic"
    const val PRE_REQUISITE_TOPICS: String = "pre_requisite_topics"
    const val TOPICS_IN_THIS_CHAPTER: String = "topics_in_this_chapter"
    const val RELATED_TOPICS: String = "related_topics"
    const val ALL_VIDEOS_FOR_THIS_CHAPTER: String = "all_videos_for_this_chapter"
    const val ALL_VIDEOS_FOR_THIS_TOPIC: String = "all_videos_for_this_topic"
    const val LAST_WATCHED_CONTENT: String = "last_watched_content"
    const val NO_NETWORK: String = "Network not connected! check your connection status!"

    const val ABOUT_THE_TEST: String = "about_the_test"
    const val RECOMMENDED_LEARNING: String = "recommended_learning"
    const val RECOMMENDED_PRACTICE: String = "recommended_practice"
    const val MORE_TESTS: String = "more_tests"
    const val ACHIEVEATTEMPTS_LIST: String = "achieveattempts_list"
    const val TEST_QUESTIONS: String = "test_questions"
    const val TEST_QUESTION_SUMMARY: String = "test_question_summary"
    const val RANK_COMPARISONS: String = "rank_comparisons"
    const val TEST_BEHAVIOURS: String = "test_behaviours"

    const val EVENT: String = "event"
    const val CHAPTER: String = "chapter"
    const val DB_NAME = BuildConfig.APPLICATION_ID + ".db"

    //Error Screen Btns
    const val CANCEL = "cancel"
    const val LEAVE_APP = "leave_app"
    const val CLOSE_VIDEO = "close_video"
    const val ON_BACK_PRESS_DIALOG_FRAGMENT = "on_back_press_dialog_fragment"
    const val START = "start"
    const val RESUME = "resume"

    const val END_SESSION = "end_session"
    const val CONTINUE_PRACTICE = "continue_practice"
    const val CONTINUE_TEST = "continue_test"

    //Dialog Fragment Constants
    const val ERROR_404_DIALOG = "error_404"
    const val APP_CLOSE_DIALOG = "close_alert"
    const val ALERT_MSG_DIALOG = "alert_msg"
    const val CUSTOM_ALERT_DIALOG = "custom_alert"
    const val NO_CONNECTIVITY = "no_connectivity"
    const val SERVER_DOWN = "server_down"
    const val ERROR_SERVER_NOT_REACHABLE = "server_not_reachable_error"
    const val PRACTICE_SESSION_ALERT_DIALOG = "practice_session_alert"
    const val TEST_SESSION_ALERT_DIALOG = "test_session_alert"
    const val VIDEO_CLOSE_DIALOG = "close_video"
    const val NO_MORE_QUESTIONS_DIALOG = "no_more_questions"
    const val PRACTICE_DIALOG = "practice_full_screen_show"
    const val SELECT_DAY_MONTH_YEAR = "select_day_month_year"

    const val PREF_KEY_ASSETS_VERSION = "assets_version"
    const val ASSETS_VERSION = 7
    const val ZIP_FORMAT = ".zip"

    const val ACTION_ASSETS_COPY_COMPLETED = "assets-copy-completed"
    const val ACTION_ASSETS_COPY_FAILED = "assets-copy-failed"
    const val VIDEO_URL = "video_url"
    const val VIDEO_TITLE = "title_url"
    const val VIDEO_DESCRIPTION = "description_url"
    const val VIDEO_SUBJECT = "subject"
    const val VIDEO_CHAPTER = "chapter"
    const val VIDEO_AUTHORS = "authors"
    const val VIDEO_ID = "video_id"
    const val VIDEO_CONCEPT_ID = "video_concept_id"
    const val VIDEO_TOTAL_DURATION = "video_total_duration"
    const val CONTENT_TYPE = "content_type"
    const val SOURCE_REFERENCE = "source_reference"
    const val IS_RECOMMENDATION_ENABLED = "is_recommendation_enabled"
    const val IS_EMBIBE_SYLLABUS = "is_embibe_syllabus"
    const val IS_NEXVIDEO = "is_nexvideo"
    const val CONCEPT_ID = "concept_id"
    const val LENGTH = "length"

    const val DETAILS_ID = "id"
    const val _ID = "_id"
    const val LPCODE = "lpcode"
    const val NAME = "name"
    const val DETAILS_TYPE = "type"

    const val NAV_NAME_TEST = "Test"
    const val NAV_NAME_SEARCH = "Search"
    const val NAV_NAME_LEARN = "Learn"
    const val NAV_NAME_ACHIEVE = "Achieve"
    const val NAV_NAME_PRACTICE = "Practice"
    const val NAV_NAME_PROFILE = "Profile"

    const val NAV_NAME_TYPE = "nav_type"
    const val NAV_TYPE_BOOK = "book"
    const val NAV_TYPE_SYLLABUS = "syllabus"


    //CARD_TYPE
    const val SUBJECT: String = "subject" // subjectinfo activity
    const val CONTINUE_LEARNING: String = "continue_watching" // video
    const val COOBO: String = "coobo"
    const val BANNER: String = "banner"
    const val AD_BANNER: String = "ad_banner"
    const val BOOK: String = "book"
    const val SYLLABUS: String = "syllabus"
    const val YOUTUBE_VIDEO: String = "video-player"
    const val KnowledgeGraph: String = "KnowledgeGraph"
    const val VIDEO: String = "video"
    const val STORY: String = "story"
    const val REAL_LIFE_EXAMPLES: String = "real_life_examples"
    const val OUT_OF_SYLLABUS: String = "out_of_syllabus"
    const val DIY: String = "diy"
    const val PAJ: String = "paj"
    const val PAJ_STEP: String = "paj_step"

    const val EXPERIMENTS: String = "experiment"
    const val LEARNING_VIDEOS: String = "learning_videos"

    const val LEARN_CHAPTER: String = "learn_chapter"

    //shared_preference key
    const val EMBIBETOKEN = "embibe-token"
    const val IS_LOGIN = "isLogIn"
    const val CURRENT_PARENT = "current_parent"
    const val LINKED_PROFILE = "linked_profiles"
    const val USER_TYPE = "user_type"
    const val USER_ID = "user_id"
    const val CURRENT_PROFILE = "current_profile"

    const val HOST_PAGE = "host_page"
    const val CONTENT_STATUS_INPROGRESS = "INPROGRESS"
    const val CONTENT_STATUS_STARTED = "STARTED"
    const val CONTENT_STATUS_COMPLETED = "COMPLETED"
    const val CONTENT_STATUS_FINISHED = "FINISHED"

    const val STATUS_CONTENT_TYPE_COOBO = "Coobo"
    const val STATUS_CONTENT_TYPE_LEARN_CHAPTERS = "Learn_Chapters"
    const val STATUS_CONTENT_TYPE_TEST = "Test"
    const val STATUS_CONTENT_TYPE_BOOK = "Book"
    const val STATUS_CONTENT_TYPE_PRACTISE_CHAPTERS = "Practise_Chapters"
    const val STATUS_CONTENT_TYPE_VIDEO = "Video"
    const val STATUS_CONTENT_TYPE_SUBJECT = "Subject"

    enum class Subjects {
        Mathematics,
        Biology,
        Science,
        Physics,
        Chemistry
    }

    //Section Name
    const val SECTION_SUBJECTS = "Subjects"
    const val SECTION_CONTINUE_LEARNING = "Continue Watching"
    const val SECTION_INTERACTIVE_COOBOS = "Interactive Coobos"
    const val SECTION_GRADE_PRACTICE = "grade_practice"
    const val SECTION_GRADE_TEST = "grade_test"
    private const val SECTION_LEARN_PRACTICE_BOOK = "Learn & Practice Books"
    private const val SECTION_BOOK_HYPHEN = "$SECTION_LEARN_PRACTICE_BOOK - "
    val SECTION_LEARN_PRACTICE_BOOK_PHYSICS = "$SECTION_BOOK_HYPHEN${Subjects.Physics}"
    val SECTION_LEARN_PRACTICE_BOOK_CHEMISTRY = "$SECTION_BOOK_HYPHEN${Subjects.Chemistry}"
    val SECTION_LEARN_PRACTICE_BOOK_MATHS = "$SECTION_BOOK_HYPHEN${Subjects.Mathematics}"
    val SECTION_LEARN_PRACTICE_BOOK_BIOLOGY = "$SECTION_BOOK_HYPHEN${Subjects.Biology}"
    val SECTION_LEARN_PRACTICE_BOOK_SCIENCE = "$SECTION_BOOK_HYPHEN${Subjects.Science}"

    const val SECTION_IRL = "In Real Life (IRL)"
    const val SECTION_OOS = "Out of Syllabus (OOS)"
    const val SECTION_DIY = "Do It Yourself (DIY)"
    const val SECTION_BOW = "Best of Web"

    const val SECTION_PRACTICE_CHEMISTRY = "Practice Chemistry"
    const val SECTION_PRACTICE_BIOLOGY = "Practice Biology"
    const val SECTION_PRACTICE_PHYSICS = "Practice Physics"
    const val SECTION_PRACTICE_SCIENCE = "Practice Science"
    const val SECTION_PRACTICE_MATHS = "Practice Mathematics"
    const val SECTION_EXPERIMENT: String = "Experiment"
    const val SECTION__CUSTOM_EXPERIMENTS: String = "Experiments"

    const val VIDEO_TYPE_BEST_OF_WEB = "best_of_web"
    const val VIDEO_TYPE_DO_IT_YOURSELF = "do_it_yourself"
    const val VIDEO_TYPE_IN_REAL_LIFE = "in_real_life"
    const val VIDEO_TYPE_OUT_OF_SYLLABUS = "out_of_syllabus"
    const val VIDEO_TYPE_SPOOF = "spoof"
    const val VIDEO_TYPE_MULT_VIDEO = "mult_video"
    const val VIDEO_TYPE_PRACTICE_SYMBOL = "practice_symbol"

    const val COOBO_URL = "cooboUrl"
    const val MATH_JAX = "MathJax"
    const val DATA = "data"
    const val GOALS = "goals"

    const val SLIDE_COUNT = "slide_count"
    const val DURATION_LENGTH = "duration_length"
    const val CURRENT_SLIDE = "current_slide"
    const val TOTAL_SLIDE = "total_slide"

    //Category Url
    const val SECTION_OOS_CATEGORY_THUMB =
        "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/cardicons/colour_out_of_syllabus.png"
    const val SECTION_IRL_CATEGORY_THUMB =
        "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/cardicons/colour_in_real_life.png"
    const val SECTION_DIY_CATEGORY_THUMB =
        "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/cardicons/colour_do_it_yourself.png"
    const val SECTION_BOW_THUMB =
        "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/cardicons/colour_best_of_web.png"

    const val SECTION_PRACTICE_CATEGORY_THUMB =
        "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/cardicons/Practice-symbol.png"

    const val SECTION_INTERACTIVE_COOBOS_CATEGORY_THUMB =
        "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/cardicons/Coobo-interaction.png"

    //Source Datas
    const val VIDEO_SOURCE = "source"

    const val STATUS = "status"

    const val ERROR = "error"

    const val PRACTICE_ID = "id"

    // more/related concepts card types
    const val TYPE_HEADER = "header"
    const val TYPE_SUB_HEADER = "subheader"
    const val TYPE_CARD = "card"
    const val TYPE_VIDEO = "video"
    const val TYPE_COOBO = "coobo"
    const val TYPE_BOOK = "book"
    const val TYPE_PRACTICE = "practice"
    const val TYPE_CHAPTER = "chapter"
    const val TYPE_TEST = "test"
    const val TYPE_TOPIC = "topic"


    const val ANNOTATION_DETAILS = "annotationDetails"
    const val ANNOTATION_MODEL = "annotation_model"
    const val ERROR_TYPE = "error_type"
    const val ANALYSIS_DATA = "analysis_data"
    const val CONCEPT_COUNT = "concept_count"
    const val CONTENT = "content"
    const val BANNER_CONTENT = "banner_content"
    const val ID = "id"
    const val LEARNMAP_CODE = "learnmap_code"
    const val TITLE = "title"
    const val CONTENT_ID = "content_id"
    const val FORMAT_ID = "format_id"
    const val LM_NAME = "lm_name"

    //Achieve new ui
    const val TYPE_TROPHY = "trophy"
    const val TYPE_READINESS = "readiness"
    const val TYPE_LIKE_DISLIKE = "like_dislike"
    const val TYPE_DISCOVER = "discover"
    const val EXAM_CONFIG_DETAILS: String = "exam_config_details"


    // Add/Edit User
    const val USER_ACTION_TYPE = "actionType"
    const val PROFILE_NAME = "name"
    const val PROFILE_ROLE = "role"
    const val PROFILE_EMAIL = "email"
    const val PROFILE_MOBILE = "mobile"
    const val ACTION_TYPE_ADD = "add_user"
    const val ACTION_TYPE_EDIT = "edit_user"
    const val USER_TYPE_CHILD = "child"
    const val USER_TYPE_PARENT = "parent"
    const val PARENT = "Parent"
    const val STUDENT = "Student"

    const val PRIMARY_GOAL = "primary_goal"
    const val PRIMARY_GOAL_EXAM = "primary_goal_exam"

    const val TEMP_K_CLASS = "10"
    const val TEMP_BOARD = "CBSE"

    const val NOT_ANIMATE = "NOT_ANIMATE"
    const val ANIMATE = "ANIMATE"

    // Add/Edit User
    const val USER_TYPE_STUDENT = "student"

    //Practice Modes
    enum class PracticeModes(val typeName: String) {
        Normal("Normal"),
        BookPractice("BookPractice"),
        CFUPractice("CFUPractice"),
        AdhocPractice("AdhocPractice"),
        PAJPractice("PAJPractice")
    }

    const val PRACTICE_MODE = "mode"

    //Used in Book for referring practice Types
    const val CFU_PRACTICE = "cfu_practice"
    const val NORMAL_PRACTICE = "normal_practice"

    //Readiness
    const val TYPE_PROGRESS = 1
    const val TYPE_FUTURE_SUCCESS = 2

    //video player
    const val PLAYER_PAUSE = "pause"
    const val PLAYER_PLAY = "play"
    const val PLAYER_FULLSCREEN = "full screen"

    const val TAG_PRACTICE_DETAL = "practice_detail"
    const val TAG_PRACTICE_ANALYSE = "practice_analyse"

    //content available
    const val NO_CONTENT = "NoContent"
    const val CONTENT_AVAILABLE = "ContentAvailable"

    const val MULTI_TYPE_TEST: String = "multi_type_test"
    const val SINGLE_TYPE_TEST: String = "single_type_test"

    const val TOPIC_LEARN_PATH: String = "topic_learn_path"
    const val LEARN_PATH_NAME: String = "learnpath_name"
    const val LEARN_PATH_FORMAT_NAME: String = "learnpath_format_name"

    //Next Recommendation source
    const val SOURCE_NEXT_HOMEPAGE = "homepage"
    const val SOURCE_NEXT_EMBIBE_SYLLABUS = "embibe_syllabus"
    const val SOURCE_NEXT_BOOK_TOC = "book_toc"

    const val CREATE_TEST = "create_test"
    const val TEST_NAME: String = "test_name"
    const val NAMESPACE: String = "namespace"
    const val VERSION: String = "2.0"
    const val CUSTOM_TEST = "custom_test"
    const val CREATED_AT = "created_at"

    const val TEST_DIFFICULTY_LEVEL: String = "test_difficulty_level"
    const val TEST_DURATION: String = "test_duration"
    const val RECOMMENDATION_LEARNING_FOR_PRACTICE: String = "recommendation_learning"
    const val BOOK_AVAILABLE_FOR_PRACTICE: String = "book_available"
    const val BOOKMARKED_QUESTIONS_FOR_PRACTICE: String = "bookmarked_questions_for_practice"
    val PRACTICE_DETAIL_TYPE: String? = "practice_detail_type"
    val PRACTICE_DETAIL_BOOK_AVAILABLE: String = "Books Available"
    val PRACTICE_DETAIL_RECOMMENDED_LEARNING: String = "Recommended Learning"
    val PRACTICE_DETAIL_TOPICS_FOR_PRACTICE: String = "Topics for Practice"
    val PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER: String = "Tests in this Chapter"
    val PRACTICE_DETAIL_BOOKMARKED_QUESTIONS: String = "Bookmarked Questions"


    const val TEST_DETAIL: String = "test_detail"
    const val TEST_CORRECT_MARK: String = "test_correct_mark"
    const val TEST_INCORRECT_MARK: String = "test_incorrect_mark"

    //Attempt Quality
    const val CORRECT: String = "correct"
    const val PERFECT: String = "perfect"
    const val OVERTIME_CORRECT: String = "overtime correct"
    const val WASTED: String = "wasted"
    const val WRONG: String = "wrong"
    const val OVERTIME_WRONG: String = "overtime incorrect"
    const val RECALL: String = "recall"
    const val ANALYTICAL_THINKING: String = "analytical_thinking"
    const val CONCEPTUAL_THINKING: String = "conceptual_thinking"
    const val WASTED_IN_FOUR: String = "wasted_in_four"

    const val DROP_DOWN: String = "drop_down"
    const val PERFECT_ATTEMPTS: String = "Perfect Attempts"
    const val OVERTIME_CORRECT_ATTEMPTS: String = "Overtime Correct Attempts"
    const val TOO_FAST_CORRECT_ATTEMPTS: String = "Too  Fast Correct Attempts"
    const val INCORRECT_ATTEMPTS: String = "Incorrect Attempts"
    const val OVERTIME_INCORRECT_ATTEMPTS: String = "Overtime In Correct Attempts"
    const val WASTED_ATTEMPTS: String = "Wasted Attempts"
    const val UNATTEMPTED_ATTEMPTS: String = "Unattempted"

    const val TYPE_CHAPTERS_YOU_GOT_RIGHT: String = "Chapters You Got Right"
    const val TYPE_CHAPTERS_YOU_GOT_WRONG: String = "Chapters You Got Wrong"
    const val TYPE_CHAPTERS_YOU_DID_NOT_ATTEMPT: String = "Chapters You Did Not Attempt"
    const val TYPE_STRONG_CHAPTERS_YOU_GOT_WRONG: String = "Your Strong Chapters You Got Wrong"
    const val TYPE_STRONG_CHAPTERS_YOU_DID_NOT_ATTEMPT: String =
        "Your Strong Chapters You Did Not Attempt"


    const val ATTEMPT_TYPE_PERFECT = "Perfect"
    const val ATTEMPT_TYPE_OVERTIME_CORRECT = "OvertimeCorrect"
    const val ATTEMPT_TYPE_TOO_FAST_CORRECT = "TooFastCorrect"

    const val ATTEMPT_TYPE_WASTED = "Wasted"
    const val ATTEMPT_TYPE_OVERTIME_INCORRECT = "OvertimeIncorrect"
    const val ATTEMPT_TYPE_INCORRECT = "Incorrect"
    const val ATTEMPT_TYPE_NON_ATTEMPT = "NonAttempt"
    const val ALL_SUBJECTS = "All Subjects"
    const val ALL_ATTEMPTS = "All ATTEMPTS"

    enum class QWA_Modes {
        list,
        detail
    }

    enum class QWA_ATTEMPT_TYPE {
        Perfect,
        TooFastCorrect,
        Wasted,
        OvertimeCorrect,
        OvertimeIncorrect,
        Incorrect,
        SolveWithUsCorrect,
        SolveWithUsInCorrect,
        SolveWithUsRevealedAnswer,
        NonAttempt
    }

    const val DIAGNOSTIC_TEST_STATUS_OPEN = "OPEN"
    const val DIAGNOSTIC_TEST_STATUS_LOCKED = "LOCKED"

    const val DIAGNOSTIC_TEST_ACTIVITY_STATUS_LEARN = "learn"
    const val DIAGNOSTIC_TEST_ACTIVITY_STATUS_COACH = "coach"

    const val KG_QUESTION_POTENTIAL = "KG question potential"

    //PAJ Feedback
    const val PAJ_ID = "paj_id"
    const val PAJ_STEP_INDEX = "paj_step_index"
    const val PAJ_STEP_PATH = "paj_step_path"
    const val PAJ_STEP_PATH_INDEX = "paj_step_path_index"
    const val CALL_ID = "call_id"
    const val PROGRESS_PERCENT = "progress_percent"
    const val QUESTION_CODE = "questionCode"

    //bannerVideo
    const val LEARN_BANNER_TEASER_VIDEO_WATCHED = "learn_banner_video_watched"
}