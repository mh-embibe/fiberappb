package com.embibe.embibetvapp.application

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.embibe.embibetvapp.BuildConfig
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.jobScheduler.MyInternetTest
import com.embibe.embibetvapp.newmodel.MyObjectBox
import com.embibejio.coreapp.application.LibApp
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.stetho.Stetho
import io.fabric.sdk.android.Fabric
import io.objectbox.BoxStore
import io.objectbox.android.AndroidObjectBrowser

class App : Application() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
        lateinit var activityContext: Context
        private var mInstance: App? = null
        lateinit var boxStore: BoxStore

        // val WRITE_KEY_SEGMENT_IO_REWRITE_PROD="zcSPewKVEfHk8MH7Y9oHEj8RIZ1N9Ipm"
        private var crashlyticsKit: Crashlytics? = null

        fun setConnectivityListener(listener: MyInternetTest.NetworkSchedulerService.ConnectivityReceiverListener?) {
            MyInternetTest.NetworkSchedulerService.connectivityReceiverListener = listener
        }


        @Synchronized
        fun setUserDetails() {
            //set userdetails for craslytics
        }
    }

    override fun onCreate() {
        super.onCreate()
        LibApp.Create(this)
        context = this
        mInstance = this
        boxStore =
            MyObjectBox.builder().name(AppConstants.DB_NAME).androidContext(applicationContext)
                .build()

        if (BuildConfig.DEBUG) {
            Log.d("object browser", "started")
            AndroidObjectBrowser(boxStore).start(this)
        }
        Stetho.initializeWithDefaults(this)
        // Segment IO for rewrite
        // var isDebug = BuildConfig.DEBUG
        val isDebug = false
        crashlyticsKit = Crashlytics.Builder()
            .core(CrashlyticsCore.Builder().disabled(isDebug).build())
            .build()
        Fabric.with(this, crashlyticsKit)
        Fresco.initialize(context)
    }

    @Synchronized
    fun getInstance(): App? {
        return mInstance
    }

}