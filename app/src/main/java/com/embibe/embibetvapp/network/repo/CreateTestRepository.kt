package com.embibe.embibetvapp.network.repo

import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.createtest.configuration.TestConfigurationRes
import com.embibe.embibetvapp.model.createtest.examconfig.ChapterDetails
import com.embibe.embibetvapp.model.createtest.examconfig.SubjectDetails
import com.embibe.embibetvapp.model.createtest.examconfig.request.ExamConfigRequest
import com.embibe.embibetvapp.model.createtest.examconfig.response.ChapterImportance
import com.embibe.embibetvapp.model.createtest.examconfig.response.TestDetails
import com.embibe.embibetvapp.model.createtest.generateowntest.request.*
import com.embibe.embibetvapp.model.createtest.generateowntest.response.GenerateOwnTestResponse
import com.embibe.embibetvapp.model.createtest.progress.request.ExcludeQuestionRestrictions
import com.embibe.embibetvapp.model.createtest.progress.request.ProgressRequest
import com.embibe.embibetvapp.model.createtest.progress.response.ProgressResponse
import com.embibe.embibetvapp.network.retrofit.RetrofitClient
import io.reactivex.Single
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.set

class CreateTestRepository {

    fun generateTest(model: CreateOwnTest): Single<Response<GenerateOwnTestResponse>> {
        return RetrofitClient.preprodCreateTestApi!!.generateTest(model)
    }

    fun progress(data: ProgressRequest): Single<Response<ProgressResponse>> {
        return RetrofitClient.preprodCreateTestApi!!.progress(data)
    }

    fun getTestConfigurations(): Single<Response<TestConfigurationRes>> {
        return RetrofitClient.preprodCreateTestApi!!.getTestConfigurations()
    }

    fun getExamConfigDetails(examConfigRequest: ExamConfigRequest): Single<Response<TestDetails>> {
        return RetrofitClient.preprodCreateTestApi!!.getExamConfig(examConfigRequest)
    }

    fun getExamConfigDetailsForChapter(examConfigRequest: ExamConfigRequest): Single<Response<ChapterImportance>> {
        return RetrofitClient.preprodCreateTestApi!!.getExamConfigForChapter(examConfigRequest)
    }

    fun getSubjectDetailsFromResponse(response: TestDetails): MutableList<SubjectDetails> {
        var subjectSampleDetails = mutableListOf<SubjectDetails>()
        val testDetails = response.subject_data
        for (section in testDetails) {
            var subjectSample =
                SubjectDetails()
            subjectSample.subjectCode = section.name
            subjectSample.subjectName = section.name
            subjectSample.subjectIcon = getSubjectIcon(section.name)
            subjectSample.bgSubject = getSubjectBg(section.name)
            subjectSample.entity_codes = section.entity_codes
            subjectSample.sequence = section.sequence

            subjectSampleDetails.add(subjectSample)

        }
        return subjectSampleDetails

    }

    fun getChapterDetails(response: ChapterImportance): MutableList<ChapterDetails> {
        var chapterSampleDetails = mutableListOf<ChapterDetails>()
        val chapterDetails = response.chapter_data
        for (chapter in chapterDetails) {
            var chapterDetailsSize =
                chapterSampleDetails.filter { it.subjectName == chapter.subject_name }
            if (chapterDetailsSize.isEmpty()) {
                var chapterSample =
                    ChapterDetails()
                chapterSample.chapterCode = ""
                chapterSample.chapterName = "All Chapters"
                chapterSample.subjectName = chapter.subject_name
                chapterSampleDetails.add(chapterSample)
            }
            var chapterSample =
                ChapterDetails()
            chapterSample.chapterCode = chapter.name
            chapterSample.chapterName = chapter.name
            chapterSample.subjectName = chapter.subject_name
            chapterSample.chapter_data = chapter

            chapterSampleDetails.add(chapterSample)

        }

        return chapterSampleDetails
    }

    fun getChapterDetailsBasedOnSubjectCode(
        subjectCode: String,
        chapterDetails: MutableList<ChapterDetails>?
    ): Single<List<ChapterDetails>> {
        return Single.just(chapterDetails!!.filter { it.subjectName == subjectCode })
    }

    fun getSelectedChapterDetails(
        subjectCode: String,
        chapterDetails: MutableList<ChapterDetails>?
    ): Single<Int> {
        var count = chapterDetails!!.filter {
            it.subjectName == subjectCode && it.selectedState == true && it.chapter_data != null
        }.size
        return Single.just(count)
    }

    fun getSelectedChapterDetails(
        chapterDetails: MutableList<ChapterDetails>?
    ): Single<List<ChapterDetails>> {
        var count = chapterDetails!!.filter {
            it.selectedState == true && it.chapter_data != null
        }
        return Single.just(count)
    }

    fun updateSubjectDetails(
        subjectResponseDetail: SubjectDetails,
        count: Int
    ): SubjectDetails {

        var subjectSample =
            SubjectDetails()
        subjectSample.subjectCode = subjectResponseDetail.subjectName
        subjectSample.subjectName = subjectResponseDetail.subjectName
        subjectSample.subjectIcon = getSubjectIcon(subjectResponseDetail.subjectName)
        subjectSample.count = count
        subjectSample.selectedPosition = true
        subjectSample.entity_codes = subjectResponseDetail.entity_codes
        subjectSample.sequence = subjectResponseDetail.sequence
        return subjectSample
    }


    fun getSubjectIcon(name: String?): Int {
        var drawableIcon: Int? = 0
        when (name) {
            "chemistry" -> {
                drawableIcon = R.drawable.ic_test_chemistry
            }
            "physics" -> {
                drawableIcon = R.drawable.ic_test_physics
            }
            "biology" -> {
                drawableIcon = R.drawable.ic_test_biology
            }
            "mathematics" -> {
                drawableIcon = R.drawable.ic_test_maths
            }
            "maths" -> {
                drawableIcon = R.drawable.ic_test_maths
            }
            else -> {
                drawableIcon = R.drawable.ic_test_physics
            }

        }
        return drawableIcon
    }

    fun getSubjectBg(name: String?): Int {
        var drawableBg: Int? = 0
        when (name) {
            "chemistry" -> {
                drawableBg = R.drawable.ic_bg_subject_one
            }
            "physics" -> {
                drawableBg = R.drawable.ic_bg_subject_three
            }
            "biology" -> {
                drawableBg = R.drawable.ic_bg_subject_four
            }
            "mathematics" -> {
                drawableBg = R.drawable.ic_bg_subject_two
            }
            "maths" -> {
                drawableBg = R.drawable.ic_bg_subject_two
            }
            else -> {
                drawableBg = R.drawable.ic_bg_subject_three
            }

        }
        return drawableBg
    }


    fun generateTestRepositoryRequest(
        subjectDetails: ArrayList<SubjectDetails>?,
        chapterDetails: ArrayList<ChapterDetails>?,
        testName: String,
        createdAt: String,
        difficultyLevel: String,
        duration: Int,
        correctMark: String,
        incorrectMark: String
    ): CreateOwnTest {

        return CreateOwnTest(
            chapterParams(chapterDetails),
            null,
            createdAt,
            difficultyLevel,
            duration,
            UserData.getExamCode(),
            "avg",
            "en",
            getCurrentDateAndTime(),
            testName,
            getQuestionCount(duration),
            "test",
            subjectCodeParams(subjectDetails!!),
            "fiber",
            getTestConfig(subjectDetails),
            getMarkingScheme(correctMark, incorrectMark)

        )

    }

    private fun getMarkingScheme(correctMark: String, incorrectMark: String): MarkingScheme? {
        return MarkingScheme(incorrectMark, correctMark, false)
    }

    private fun subjectCodeParams(subjectDetails: ArrayList<SubjectDetails>): HashMap<String, SectionData> {
        var hashMap = HashMap<String, SectionData>()

        for (i in subjectDetails.indices) {
            var data = subjectDetails[i]
            var details = data.entity_codes?.get(0)!!
            var questionTypeCount =
                com.embibe.embibetvapp.model.createtest.generateowntest.request.QuestionTypeCount(
                    details.question_type_count.FillInTheBlanks,
                    details.question_type_count.SingleChoice,
                    details.question_type_count.SubjectiveAnswer,
                    details.question_type_count.TrueFalse
                )
            var entityCode = EntityCode(
                details.difficulty_level,
                details.difficulty_level_std,
                details.difficulty_order,
                details.entity_name,
                details.entity_type,
                details.ideal_time_level,
                details.ideal_time_level_std,
                details.question_count,
                questionTypeCount,
                details.question_types
            )

            var sectionData =
                SectionData(mutableListOf(entityCode), data.subjectName, true, data.sequence)
            hashMap[data.subjectName!!] = sectionData
        }


        return hashMap

    }

    private fun chapterParams(chapterDetails: ArrayList<ChapterDetails>?): HashMap<String, ChapterData> {
        val chapter_data = HashMap<String, ChapterData>()
        for (i in chapterDetails!!.indices) {
            var details = chapterDetails.get(i).chapter_data!!
            var chapterData = ChapterData(
                details.name,
                details.no_of_questions,
                details.previous_year_weight_percent,
                details.subject_name,
                details.unit_name
            )
            chapter_data[details.name] = chapterData

        }
        return chapter_data
    }

    private fun getQuestionCount(questionCount: Int): Int? {
        return (questionCount / 2).toInt()
    }

    private fun getTestConfig(subjectDetails: ArrayList<SubjectDetails>): TestConfig {
        var details = HashMap<String, Int>()
        for (data in subjectDetails) {
            details[data.subjectName!!] = data.entity_codes?.get(0)?.question_count!!
        }
        return TestConfig(
            "greedy",
            4.1,
            90,
            "Chapter",
            "avg",
            "True",
            8,
            180,
            1,
            0,
            "",
            subjectDetails.size,
            1,
            1.2,
            45,
            details
        )
    }


    fun progressRequest(
        data: GenerateOwnTestResponse,
        testName: String, createdAt: String,
        difficultyLevel: String,
        duration: Int,
        chapterDetails: java.util.ArrayList<ChapterDetails>?
    ): ProgressRequest {
        var excludeQuestionRestrictions = ExcludeQuestionRestrictions(
            ArrayList(),
            isDefault = true,
            previousYear = false,
            xMonthTests = false,
            xValue = 3
        )

        return ProgressRequest(
            data.atgId!!,
            true,
            createdAt,
            difficultyLevel,
            duration,
            UserData.getExamCode(),
            excludeQuestionRestrictions,
            "avg",
            "en",
            getCurrentDateAndTime(),
            testName,
            getQuestionCount(duration),
            data.requestId,
            "test",
            "fiber"
        )

    }

    fun getCurrentDateAndTime(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
        return sdf.format(Date())
    }


}