package com.embibe.embibetvapp.network.apis

import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.model.UpdateProfileRequest
import com.embibe.embibetvapp.model.adduser.AddUserRequest
import com.embibe.embibetvapp.model.adduser.AddUserResponse
import com.embibe.embibetvapp.model.auth.SignUpResponse
import com.embibe.embibetvapp.model.auth.SignupVerifyOtpRequest
import com.embibe.embibetvapp.model.auth.SignupVerifyOtpResponse
import com.embibe.embibetvapp.model.auth.login.LoginResponse
import com.embibe.embibetvapp.model.auth.otp.AuthOTP
import com.embibe.embibetvapp.model.auth.signup.SignupSendOtpRequest
import com.embibe.embibetvapp.model.auth.signup.SignupSendOtpResponse
import com.embibe.embibetvapp.model.auth.signup.UserExistResponse
import com.embibe.embibetvapp.model.goals.GoalsExamsRes
import com.embibe.embibetvapp.model.updateProfile.UpdateProfileRes
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface Api {
    @GET(ApiConstants.URL_USER_EXIST)
    fun userExist(@Query("id") id: String): Single<Response<UserExistResponse>>

    @POST(ApiConstants.URL_SEND_OTP_SIGNUP)
    fun signUpSendOtp(@Body signupSendOtpRequest: SignupSendOtpRequest): Single<Response<SignupSendOtpResponse>>

    @POST(ApiConstants.URL_AUTH_OTP_SIGN_IN)
    fun authOTPSignIn(@Body signupSendOtpRequest: AuthOTP): Single<Response<LoginResponse>>

    @POST(ApiConstants.URL_VERIFY_OTP)
    fun verifyOTP(@Body signupVerifyOtpRequest: SignupVerifyOtpRequest): Single<Response<SignupVerifyOtpResponse>>

    @POST(ApiConstants.URL_AUTH_OTP_SIGN_IN)
    fun signUpUser(@Body signUpRequest: AuthOTP): Single<Response<SignUpResponse>>

    @GET(ApiConstants.URL_LINKED_PROFILES)
    fun getLinkedProfiles(): Single<Response<LoginResponse>>

    @POST(ApiConstants.URL_ADD_USER)
    fun addUser(
        @Path("userid") userid: String,
        @Query("user_type") userType: String,
        @Query("first_name") name: String,
        @Query("email") email: String,
        @Query("password") password: String,
        @Query("flag") flag: String,
        @Query("primary_goal_code") goal_code: String,
        @Query("primary_exam_code") primary_exam_code: String,
        @Query("klass") klass: String,
        @Query("board") board: String,
        @Query("gender") gender: String,
        @Query("profile_pic") profile_pic: String
    ): Single<Response<AddUserResponse>>

    @POST(ApiConstants.URL_ADD_USER)
    fun addUserNew(
        @Query("id") userId: String,
        @Body addUserRequest: AddUserRequest
    ): Single<Response<AddUserResponse>>

    @PUT(ApiConstants.URL_UPDATE_PROFILE)
    fun updateProfile(@Body model: UpdateProfileRequest): Single<Response<UpdateProfileRes>>

    @GET(ApiConstants.URL_GOALS_EXAMS)
    fun getGoalsExams(): Single<Response<GoalsExamsRes>>

}

