package com.embibe.embibetvapp.network.retrofit

import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.network.apis.*
import com.embibe.embibetvapp.network.okhttp.HttpClientApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

    private var retrofit: Retrofit? = null
    private var retrofitStaging: Retrofit? = null

    fun getInstance(okHttpClient: OkHttpClient): Retrofit? {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL_PREPRODMS)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit
    }


    private val INSTANCE: Retrofit?
        get() {
            return getInstance(HttpClientApi.getOkHttp())
        }

    val service = INSTANCE?.create(Api::class.java)

    val apiDetails = INSTANCE?.create(DetailsApi::class.java)

    val homeApi = INSTANCE?.create(HomeApi::class.java)
    val stagingHomeApi = STATGING_INSTANCE?.create(HomeApi::class.java)


    fun getStagingInstance(okHttpClient: OkHttpClient): Retrofit? {
        if (retrofitStaging == null) {
            retrofitStaging = Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL_STAGING)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofitStaging
    }

    private val STATGING_INSTANCE: Retrofit?
        get() {
            return getStagingInstance(HttpClientApi.getOkHttp())
        }

    val achieveApi = STATGING_INSTANCE?.create(AchieveApi::class.java)

    val practiceSummary = STATGING_INSTANCE?.create(DetailsApi::class.java)
    val preprodCreateTestApi = INSTANCE?.create(CreateTestApi::class.java)

    val achieveApiPreprod = INSTANCE?.create(AchieveApi::class.java)

    val homeApiStaging = STATGING_INSTANCE?.create(HomeApi::class.java)

    val diagnosticTestApiPreprod = INSTANCE?.create(DiagnosticTestApi::class.java)

}

