package com.embibe.embibetvapp.network.apis

import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.model.AnnotationResponse
import com.embibe.embibetvapp.model.DEAnalysis.DEAnalysisRequest
import com.embibe.embibetvapp.model.DEAnalysis.DEBehaviorMeter
import com.embibe.embibetvapp.model.DEAnalysis.DEConceptCoverage
import com.embibe.embibetvapp.model.DEAnalysis.DEOverAllAccuracy
import com.embibe.embibetvapp.model.LastWatchedContentRes
import com.embibe.embibetvapp.model.PAJ.PostPAJUpdate
import com.embibe.embibetvapp.model.achieve.RankComparison.RankComparison
import com.embibe.embibetvapp.model.achieve.completedJourney.TestSummary
import com.embibe.embibetvapp.model.completionSummary.Topic
import com.embibe.embibetvapp.model.content.ContentStatusRequest
import com.embibe.embibetvapp.model.content.ContentStatusResponse
import com.embibe.embibetvapp.model.home.SearchRequest
import com.embibe.embibetvapp.model.home.TestForChapterRequest
import com.embibe.embibetvapp.model.learnSummary.SincerietyRes
import com.embibe.embibetvapp.model.likeAndBookmark.BookmarkedQuestion
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkRequest
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkResponse
import com.embibe.embibetvapp.model.practise.conceptSequence.ConceptSeqRes
import com.embibe.embibetvapp.model.practise.details.PracticeTopicsRequest
import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.embibe.embibetvapp.model.response.attemptquality.AttemptQualityResponse
import com.embibe.embibetvapp.model.test.*
import com.embibe.embibetvapp.model.testkg.TestKgRequest
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.google.gson.internal.LinkedTreeMap
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface DetailsApi {

    @POST(ApiConstants.URL_UPDATE_CONTENT_STATUS)
    fun updateContentStatus(
        @Query("child_id") child_id: String,
        @Body model: ContentStatusRequest
    ): Single<Response<CommonApiResponse>>

    @GET(ApiConstants.URL_GET_CONTENT_STATUS)
    fun getContentStatus(
        @Path("content_id") content_id: String,
        @Path("content_type") content_type: String,
        @Query("child_id") child_id: String
    ): Single<Response<ContentStatusResponse>>

    @POST(ApiConstants.URL_LIKE)
    fun likeApi(
        @Query("child_id") child_id: String,
        @Body model: LikeBookmarkRequest
    ): Single<Response<LikeBookmarkResponse>>

    @GET(ApiConstants.URL_GET_LIKE_STATUS)
    fun getLikeStatus(
        @Query("child_id") child_id: String,
        @Query("content_id") content_id: String
    ): Single<Response<LikeBookmarkResponse>>

    @POST(ApiConstants.URL_BOOKMARK)
    fun bookmarkApi(
        @Query("child_id") child_id: String,
        @Body model: LikeBookmarkRequest
    ): Single<Response<CommonApiResponse>>

    @GET(ApiConstants.URL_GET_BOOKMARK_STATUS)
    fun getBookmarkStatus(
        @Query("child_id") child_id: String,
        @Query("content_id") content_id: String
    ): Single<Response<LikeBookmarkResponse>>

    @GET(ApiConstants.URL_GET_BOOKMARKED_CONTENTS)
    fun getBookmarkedContents(
        @Path("content_type") content_type: String,
        @Query("learn_path_name") learn_path_name: String
    ): Single<Response<List<BookmarkedQuestion>>>

    @GET(ApiConstants.URL_ATTEMPT_QUALITY)
    fun getAttemptQualityData(
        @Query("format_reference") format_reference: String,
        @Query("learnpath_format") learnpath_format: String,
        @Query("learnpath_name") learnpath_name: String

    ): Single<Response<AttemptQualityResponse>>

    @GET(ApiConstants.URL_SINCERITY_SCORE)
    fun getSincerietyScore(
        @Query("format_reference") format_reference: String,
        @Query("learnpath_format") learnpath_format: String,
        @Query("learnpath_name") learnpath_name: String,
        @Query("type") type: String
    ): Single<Response<SincerietyRes>>


    @GET(ApiConstants.URL_LAST_WATCHED_CONTENT)
    fun getLastWatchedContent(
        @QueryMap map: HashMap<String, String>
    ): Single<Response<LastWatchedContentRes>>

    @POST(ApiConstants.URL_OVERALL_ACCURACY)
    fun getOverallAccuracy(@Body model: DEAnalysisRequest): Single<Response<DEOverAllAccuracy>>

    @POST(ApiConstants.URL_CONCEPTS_COVERAGE)
    fun getConceptsCoverage(@Body model: DEAnalysisRequest): Single<Response<DEConceptCoverage>>

    @POST(ApiConstants.URL_BEHAVIOUR_METER)
    fun getBehaviourMeter(@Body model: DEAnalysisRequest): Single<Response<DEBehaviorMeter>>

    @POST(ApiConstants.URL_CHAPTER_PRACTICES)
    fun getPracticeTopics(@Body model: PracticeTopicsRequest): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_BOOKMARK_TEST)
    fun bookmarkTestApi(@Body model: LikeBookmarkRequest): Single<Response<CommonApiResponse>>

    @POST(ApiConstants.URL_TEST_DETAILS)
    fun getTestDetails(
        @Body model: TestDetailsRequest
    ): Single<Response<TestDetailsRes>>

    @GET(ApiConstants.URL_TEST_ATTEMPTS)
    fun getAttempts(
        @Query("test_code") test_code: String,
        @Query("child_id") child_id: String
    ): Single<Response<TestAttemptsRes>>

    @GET(ApiConstants.URL_ACHIEVE_ATTEMPTS)
    fun getAchieveAttempts(
        @Query("test_code") test_code: String,
        @Query("child_id") child_id: String
    ): Single<Response<List<TestAttemptsRes>>>

    @GET(ApiConstants.URL_FEEDBACK_QUESTION_SUMMARY)
    fun getQuestionSummary(
        @Query("test_code") test_code: String,
        @Query("child_id") child_id: String
    ): Single<Response<TestQuestionSummaryRes>>

    @GET(ApiConstants.URL_FEEDBACK_TOPIC_SUMMARY)
    fun getTopicSummary(
        @Query("test_code") test_code: String,
        @Query("child_id") child_id: String,
        @Query("chapter_name") chapter_name: String,
        @Query("chapter_learning_map") chapter_learning_map: String
    ): Single<Response<List<SubFeedbackDetailMenuModel>>>

    @GET(ApiConstants.URL_TEST_QUESTIONS)
    fun getQuestions(
        @Path("test_code") test_code: String
    ): Single<Response<TestQuestionResponse>>

    @POST(ApiConstants.URL_TEST_KG_GRAPH)
    fun getTestKGGraph(
        @Body model: TestKgRequest
    ): Single<Response<List<LinkedTreeMap<Any, Any>>>>

    @GET(ApiConstants.URL_TEST_SKILLS)
    fun getSkills(
        @Query("test_code") test_code: String,
        @Query("child_id") child_id: String
    ): Single<Response<TestSkillsRes>>

    @GET(ApiConstants.URL_ACHIEVE_SKILLS)
    fun getAchieveSkills(
        @Query("test_code") test_code: String,
        @Query("child_id") child_id: String
    ): Single<Response<List<TestSummary>>>

    @GET(ApiConstants.URL_ACHIEVE_BEHAVIOURS)
    fun getAchieveBehaviours(
        @Query("test_code") test_code: String,
        @Query("child_id") child_id: String
    ): Single<Response<List<ImproveStrategy>>>

    @GET(ApiConstants.URL_ACHIEVE_FEEDBACK_TOPIC_SUMMARY)
    fun getAchieveFeedbackTopicSummary(
        @Query("test_code") test_code: String,
        @Query("child_id") child_id: String
    ): Single<Response<List<Topic>>>

    @GET(ApiConstants.URL_ACHIEVE_FEEDBACK_RANKS_COMPARISON)
    fun getAchieveFeedbackRanksComparison(
        @Query("test_code") test_code: String,
        @Query("child_id") child_id: String,
        @Query("grade") grade: String,
        @Query("exam_name") exam_name: String
    ): Single<Response<List<RankComparison>>>

    @GET(ApiConstants.URL_TEST_ACHIEVE)
    fun getAchieve(
        @Query("test_code") test_code: String,
        @Query("child_id") child_id: String,
        @Query("userId") userId: String,
        @Query("grade") grade: String,
        @Query("exam_name") exam_name: String
    ): Single<Response<TestAchieveRes>>

    @GET(ApiConstants.URL_TEST_STATUS)
    fun getTestStatus(
        @Path("test_code") test_code: String
    ): Single<Response<HashMap<String, HashMap<String, String>>>>


    @GET(ApiConstants.URL_GET_ANNOTATIONS)
    fun getAnnotations(
        @Query("videoId") videoId: String,
        @Query("subject") subject: String,
        @Query("board") board: String,
        @Query("grade") grade: Int
    ): Single<Response<AnnotationResponse>>

    @GET(ApiConstants.URL_RECOMMENDED_LEARNING)
    fun getRecommendationLearningForPractices(
        @Query("learnMapId") conceptId: String,
        @Query("contentTypes") content_id: String
    ): Single<Response<List<Results>>>

    @POST(ApiConstants.URL_TESTS_FOR_CHAPTER)
    fun getTestsForChapter(
        @Body request: TestForChapterRequest
    ): Single<Response<List<Content>>>


    @GET(ApiConstants.URL_BOOKS_AVAILABLE_FOR_PRACTICE)
    fun getBooksForPractice(
        @Query("user_id") user_id: String,
        @Query("source") source: String,
        @Query("section") section: String,
        @Query("learn_path") learn_path: String
    ): Single<Response<List<Content>>>


    @POST(ApiConstants.URL_SEARCH_RECOMMENDED_LEARNING)
    fun searchForRecommendedLearning(
        @Body request: SearchRequest
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_SEARCH_TOPIC_FOR_PRACTICE)
    fun searchForTopics(
        @Body request: SearchRequest
    ): Single<Response<List<Content>>>

    @GET(ApiConstants.URL_BOOKS_AVAILABLE_FOR_PRACTICE)
    fun searchForBooks(
        @Query("query") query: String,
        @Query("user_id") user_id: String,
        @Query("source") source: String,
        @Query("section") section: String,
        @Query("learn_path") learn_path: String
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_TEST_READINESS)
    fun testReadiness(
        @Body request: TestReadinessReq
    ): Single<Response<TestReadiness>>

    @GET(ApiConstants.URL_CONCEPT_SEQUENCE)
    fun getConceptsSequenceCoverage(
        @Query("format_reference") format_reference: String,
        @Query("learnpath_name") learnpath_name: String,
        @Query("test_code") test_code: String
    ): Single<Response<ConceptSeqRes>>

    @POST(ApiConstants.URL_SEARCH_BOOKMARKED_QUESTIONS)
    fun searchBookmarkedQuestions(
        @Body request: SearchRequest
    ): Single<Response<List<BookmarkedQuestion>>>

    @POST(ApiConstants.URL_ACHIEVE_POST_PAJ_TEST_CODE)
    fun getAchieveAchievePostPAJtestCode(
        @Body request: PostPAJUpdate
    ): Single<Response<CommonApiResponse>>

}