package com.embibe.embibetvapp.network.repo

import android.util.Log
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.model.UpdateProfileRequest
import com.embibe.embibetvapp.model.adduser.AddUserRequest
import com.embibe.embibetvapp.model.adduser.AddUserResponse
import com.embibe.embibetvapp.model.auth.SignUpResponse
import com.embibe.embibetvapp.model.auth.SignupVerifyOtpRequest
import com.embibe.embibetvapp.model.auth.SignupVerifyOtpResponse
import com.embibe.embibetvapp.model.auth.login.LoginResponse
import com.embibe.embibetvapp.model.auth.otp.AuthOTP
import com.embibe.embibetvapp.model.auth.signup.SignupSendOtpRequest
import com.embibe.embibetvapp.model.auth.signup.SignupSendOtpResponse
import com.embibe.embibetvapp.model.auth.signup.UserExistResponse
import com.embibe.embibetvapp.model.goals.GoalsExamsRes
import com.embibe.embibetvapp.model.updateProfile.UpdateProfileRes
import com.embibe.embibetvapp.network.retrofit.RetrofitClient
import com.embibejio.coreapp.preference.PreferenceHelper
import io.reactivex.Single
import retrofit2.Response

class SignUpRepository {

    fun getUserExist(id: String): Single<Response<UserExistResponse>> {
        return RetrofitClient.service!!.userExist(id)
    }

    fun getSignUpSendOtp(signupSendOtpRequest: SignupSendOtpRequest): Single<Response<SignupSendOtpResponse>> {
        return RetrofitClient.service!!.signUpSendOtp(signupSendOtpRequest)
    }

    fun getSignUpData(signUpRequest: AuthOTP): Single<Response<SignUpResponse>> {
        return RetrofitClient.service!!.signUpUser(signUpRequest)
    }

    fun getLinkedProfiles(): Single<Response<LoginResponse>> {
        val masterToken: String =
            PreferenceHelper()[AppConstants.EMBIBETOKEN, ""].toString()
        val map = HashMap<String, String>()
        map[AppConstants.EMBIBETOKEN] = masterToken
        Log.w("app-log", "token - parent - $masterToken")
        return RetrofitClient.service!!.getLinkedProfiles()
    }

    fun updateProfile(model: UpdateProfileRequest): Single<Response<UpdateProfileRes>> {
        return RetrofitClient.service!!.updateProfile(model)
    }

    fun getGoalsExams(): Single<Response<GoalsExamsRes>> {
        return RetrofitClient.service!!.getGoalsExams()
    }


    fun authOTPSignIn(authOTP: AuthOTP): Single<Response<LoginResponse>> {
        return RetrofitClient.service!!.authOTPSignIn(authOTP)
    }

    fun getVerifyOTP(signUpVerifyOtpRequest: SignupVerifyOtpRequest): Single<Response<SignupVerifyOtpResponse>> {
        return RetrofitClient.service!!.verifyOTP(signUpVerifyOtpRequest)
    }

    fun getAddUser(
        userId: String,
        addUserRequest: AddUserRequest
    ): Single<Response<AddUserResponse>> {
        return RetrofitClient.service!!.addUserNew(userId, addUserRequest)
    }

}