package com.embibe.embibetvapp.network.apis

import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.model.NextVideoRecommendationsRes
import com.embibe.embibetvapp.model.PAJ.AchieveSection
import com.embibe.embibetvapp.model.achieve.home.AchieveHome
import com.embibe.embibetvapp.model.chapter.chapterLearningObjects.ChapterLearningObject
import com.embibe.embibetvapp.model.home.*
import com.embibe.embibetvapp.model.jiostb.JioUser
import com.embibe.embibetvapp.model.prerequisites.PrerequisitesResponse
import com.embibe.embibetvapp.model.relatedConcepts.ConceptsResponse
import com.embibe.embibetvapp.model.search.SearchResponse
import com.embibe.embibetvapp.model.test.TestDetailRequest
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.newmodel.ResultsEntity
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface HomeApi {

    @GET(ApiConstants.URL_NEXT_VIDEO)
    fun getNextVideo(
        @Path("contentId") content_id: String,
        @Query("source") source: String,
        @Query("book_id") book_id: String,
        @Query("topic_learnpath_name") topicLearnPathName: String,
        @Query("learnpath_name") learnPathName: String,
        @Query("learnpath_format_name") learnPathFormatName: String
    ): Single<Response<NextVideoRecommendationsRes>>

    @GET(ApiConstants.URL_AUTO_PLAY_VIDEO)
    fun getAutoPlayVideo(
        @Path("conceptId") conceptId: String,
        @QueryMap map: HashMap<String, String>
    ): Single<Response<NextVideoRecommendationsRes>>

    @GET(ApiConstants.URL_JIO_STB_USER)
    fun getJioSTBUser(@HeaderMap headers: HashMap<String, String>): Single<Response<JioUser>>

    @POST(ApiConstants.URL_HOME)
    fun getHome(@Body model: HomeRequest): Single<Response<List<ResultsEntity>>>

    @POST(ApiConstants.URL_HOME_SECTION)
    fun getHomeLearnSection(@Body model: HomeRequest): Single<Response<List<ResultsEntity>>>

    @POST(ApiConstants.URL_HOME_PRACTICE)
    fun getHomePractice(@Body model: HomeRequest): Single<Response<List<ResultsEntity>>>

    @POST(ApiConstants.URL_HOME_PRACTICE_SECTION)
    fun getHomePracticeSection(@Body model: HomeRequest): Single<Response<List<ResultsEntity>>>

    @POST(ApiConstants.URL_HOME_SUBJECT)
    fun homeSubjectFilter(
        @Path("subject") gradeId: String,
        @Body model: HomeRequest
    ): Single<Response<List<ResultsEntity>>>

    @POST(ApiConstants.URL_HOME_SUBJECT_SECTION)
    fun homeSubjectFilterSection(
        @Path("subject") gradeId: String,
        @Body model: HomeRequest
    ): Single<Response<List<ResultsEntity>>>

    @POST(ApiConstants.URL_HOME_QUICKLINKS)
    fun homeQuickLinks(@Body model: HomeRequest): Single<Response<List<ResultsEntity>>>

    @POST(ApiConstants.URL_HOME_QUICKLINKS_SECTION)
    fun homeQuickLinksSection(@Body model: HomeRequest): Single<Response<List<ResultsEntity>>>

    @POST(ApiConstants.URL_HOME_TEST)
    fun homeTest(@Body model: HomeRequest): Single<Response<List<ResultsEntity>>>

    @POST(ApiConstants.URL_HOME_TEST)
    fun homeTestSubjectFilter(
        @Body model: HomeRequest
    ): Single<Response<List<ResultsEntity>>>

    @GET(ApiConstants.URL_RELATED_CONCEPTS)
    fun getRelatedConcepts(
        @Path("conceptId") conceptId: String,
        @Query("content_id") content_id: String
    ): Single<Response<ConceptsResponse>>

    @GET(ApiConstants.URL_PREREQUISITES)
    fun getPrerequisites(
        @Path("conceptId") conceptId: String,
        @Query("content_id") content_id: String
    ): Single<Response<PrerequisitesResponse>>

    @GET(ApiConstants.URL_MORE_CONCEPTS)
    fun getMoreConcepts(
        @Path("conceptId") conceptId: String,
        @Query("content_id") content_id: String
    ): Single<Response<ConceptsResponse>>

    @GET(ApiConstants.URL_MORE_TOPICS)
    fun getMoreTopics(
        @Path("topic_name") conceptId: String,
        @Query("content_id") content_id: String
    ): Single<Response<ConceptsResponse>>

    @POST(ApiConstants.URL_PREREQUISITES_FOR_CHAPTER)
    fun getPrerequisitesForChapter(
        @Body request: TopicForChapterRequest
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_TOPICS_FOR_CHAPTER)
    fun getTopicsForChapter(
        @Body request: TopicForChapterRequest
    ): Single<Response<List<Content>>>


    @POST(ApiConstants.URL_TESTS_FOR_CHAPTER)
    fun getTestsForChapter(
        @Body request: TestForChapterRequest
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_PRACTICES_FOR_CHAPTER)
    fun getPracticesForChapter(
        @Body request: PracticeForChapterRequest
    ): Single<Response<List<Content>>>

    @GET(ApiConstants.URL_VIDEOS_FOR_CHAPTER)
    fun getVideosForChapter(
        @Query("learnMapId") learnMapId: String,
        @Query("contentTypes") contentTypes: String
    ): Single<Response<List<ResultsEntity>>>

    @GET(ApiConstants.URL_VIDEOS_FOR_CHAPTER_V1)
    fun getVideosForChapterV1(
        @Query("learnMapId") learnMapId: String,
        @Query("contentTypes") contentTypes: String
    ): Single<Response<List<ChapterLearningObject>>>

    @POST(ApiConstants.URL_RECOMMENDED_LEARNING_FOR_TEST)
    fun getRecommendedLearningForTest(
        @Body request: TestDetailRequest
    ): Single<Response<List<Results>>>

    @POST(ApiConstants.URL_RECOMMENDED_PRACTICE_FOR_TEST)
    fun getRecommendedPracticeForTest(
        @Body request: TestDetailRequest
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_MORE_TESTS)
    fun getMoreTests(
        @Body request: TestForChapterRequest
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_PREREQUISITES_FOR_TOPIC)
    fun getPrerequisitesForTopic(
        @Body request: PracticeForChapterRequest
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_RELATED_TOPICS)
    fun getRelatedTopics(
        @Body request: PracticeForChapterRequest
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_TESTS_FOR_TOPIC)
    fun getTestsForTopic(
        @Body request: TestForChapterRequest
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_PRACTICES_FOR_TOPIC)
    fun getPracticesForTopic(
        @Body request: PracticeForChapterRequest
    ): Single<Response<List<Content>>>

    @GET(ApiConstants.URL_VIDEOS_FOR_TOPIC)
    fun getVideosForTopic(
        @Query("learnMapId") learnMapId: String,
        @Query("contentTypes") contentTypes: String
    ): Single<Response<List<ResultsEntity>>>

    @GET(ApiConstants.URL_SUGGESTIONS)
    fun getSuggestions(
        @Query("query") query: String, @Query("size") size: String,
        @Query("user_id") user_id: String,
        @Query("grade", encoded = true) grade: String,
        @Query("goal", encoded = true) goal: String,
        @Query("exam", encoded = true) exam: String
    ): Single<Response<SearchResponse>>

    @GET(ApiConstants.URL_SEARCH)
    fun getSearchResult(
        @Query("query") query: String,
        @Query("size") size: String,
        @Query("user_id") user_id: String,
        @Query("grade", encoded = true) grade: String,
        @Query("goal", encoded = true) goal: String,
        @Query("exam", encoded = true) exam: String
    ): Single<Response<SearchResponse>>

    @POST(ApiConstants.URL_SEARCH_ALL_VIDEOS)
    fun getSearchResultForAllVideos(
        @Body request: SearchRequest
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_SEARCH_CHAPTER_TOPICS)
    fun getSearchResultForCHapterTopics(
        @Body request: SearchRequest
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_SEARCH_CHAPTER_PREREQUISITE)
    fun getSearchResultForPrerequisite(
        @Body request: SearchRequest
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_SEARCH_CHAPTER_PRACTISE)
    fun getSearchResultForChapterPractice(
        @Body request: SearchRequest
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_ACHIEVE_HOME)
    fun achieveHome(
        @Body model: HomeRequest
    ): Single<Response<List<AchieveHome>>>

    @POST(ApiConstants.URL_ACHIEVE_HOME_SUBJECT)
    fun achieveHomeSubject(
        @Path("subject") subject: String,
        @Body model: HomeRequest
    ): Single<Response<List<AchieveHome>>>

    @POST(ApiConstants.URL_SEARCH_TESTS_ON_CHAPTER)
    fun searchTestsOnChapter(
        @Body request: SearchRequest
    ): Single<Response<List<Content>>>

    @POST(ApiConstants.URL_SEARCH_MORE_TESTS)
    fun searchMoreTests(
        @Body request: SearchRequest
    ): Single<Response<List<Content>>>


}