package com.embibe.embibetvapp.network.okhttp

import com.embibe.embibetvapp.network.interceptor.RequestInterceptor
import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

object HttpClientApi {

    fun getOkHttp(): OkHttpClient {

        val httpClient = OkHttpClient.Builder()

        httpClient.addNetworkInterceptor(StethoInterceptor())
        httpClient.addInterceptor(RequestInterceptor())

        httpClient.connectTimeout(30, TimeUnit.SECONDS)
        httpClient.readTimeout(30, TimeUnit.SECONDS)

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addNetworkInterceptor(logging)

        val okHttpClient = httpClient.build()

        return okHttpClient
    }

}






