package com.embibe.embibetvapp.network.apis

import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.model.createtest.configuration.TestConfigurationRes
import com.embibe.embibetvapp.model.createtest.examconfig.request.ExamConfigRequest
import com.embibe.embibetvapp.model.createtest.examconfig.response.ChapterImportance
import com.embibe.embibetvapp.model.createtest.examconfig.response.TestDetails
import com.embibe.embibetvapp.model.createtest.generateowntest.request.CreateOwnTest
import com.embibe.embibetvapp.model.createtest.generateowntest.response.GenerateOwnTestResponse
import com.embibe.embibetvapp.model.createtest.progress.request.ProgressRequest
import com.embibe.embibetvapp.model.createtest.progress.response.ProgressResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface CreateTestApi {

    @POST(ApiConstants.URL_EXAM_CONFIG)
    fun getExamConfig(@Body examConfigRequest: ExamConfigRequest): Single<Response<TestDetails>>

    @POST(ApiConstants.URL_EXAM_CONFIG)
    fun getExamConfigForChapter(@Body examConfigRequest: ExamConfigRequest): Single<Response<ChapterImportance>>

    @POST(ApiConstants.URL_GENERATE_TEST)
    fun generateTest(@Body model: CreateOwnTest): Single<Response<GenerateOwnTestResponse>>

    @POST(ApiConstants.URL_TEST_PROGRESS)
    fun progress(@Body model: ProgressRequest): Single<Response<ProgressResponse>>

    @GET(ApiConstants.URL_TEST_CONFIGURATION)
    fun getTestConfigurations(): Single<Response<TestConfigurationRes>>
}