package com.embibe.embibetvapp.network.repo

import com.embibe.embibetvapp.model.achieve.AchieveListBase
import com.embibe.embibetvapp.model.achieve.Crunching.CrunchingCount
import com.embibe.embibetvapp.model.achieve.PercentageConnectionsRes
import com.embibe.embibetvapp.model.achieve.UpdateAchieveRequest
import com.embibe.embibetvapp.model.achieve.achieveFeedback.AchieveFeedbackRes
import com.embibe.embibetvapp.model.achieve.futureSuccess.FutureSuccessRes
import com.embibe.embibetvapp.model.achieve.keyconcepts.KeyConceptModel
import com.embibe.embibetvapp.model.achieve.paj.PAJCreateReq
import com.embibe.embibetvapp.model.achieve.readiness.Readiness
import com.embibe.embibetvapp.model.achieve.successStories.SuccessStory
import com.embibe.embibetvapp.model.diagnostic.TestDiagnosticRes
import com.embibe.embibetvapp.model.potential.Potential
import com.embibe.embibetvapp.network.apis.AchieveApi
import com.google.gson.internal.LinkedTreeMap
import io.reactivex.Single
import retrofit2.Response

class AchieveRepository constructor(val api: AchieveApi) {


    fun fetchPercentageConnections(
        child_id: String,
        grade: String,
        examName: String,
        goal: String
    ): Single<Response<PercentageConnectionsRes>> {

        return api.getPercentageConnections(goal, grade,examName)
    }

    fun fetchKeyRelations(
        child_id: String,
        from_goal: String,
        to_goal: String,
        from_exam: String,
        to_exam: String,
        level: String,
        count: String
    ): Single<Response<ArrayList<KeyConceptModel>>> {
        return api.getKeyRelations(from_goal, to_goal, from_exam, to_exam, level,count)
    }

    fun fetchReadiness(
        child_id: String,
        examCode: String,
        goal: String
    ): Single<Response<Readiness>> {
        return api.getReadiness(
            child_id, "ex4"
//            true
        )
    }

    fun fetchFutureSuccess(child_id: String): Single<Response<List<FutureSuccessRes>>> {
        return api.getFutureSuccess(
            child_id
//            true
        )
    }

    fun fetchSuccessStoriess(
        child_id: String,
        grade: String
    ): Single<Response<List<SuccessStory>>> {
        return api.getSuccessStories(
            child_id,
            grade
        )
    }

    fun fetchEducationQualification(): Single<Response<AchieveListBase>> {
        return api.getEducationQualification()
    }

    fun fetchJobOptions(): Single<Response<AchieveListBase>> {
        return api.getJobOptions()
    }

    fun fetchColleges(): Single<Response<AchieveListBase>> {
        return api.getColleges()
    }

    fun fetchStates(): Single<Response<AchieveListBase>> {
        return api.getStates()
    }

    fun fetchTruePotentialPrePG(): Single<Response<List<Potential>>> {
        return api.getTruePotentialPrePG()
    }

    fun fetchTruePotentialPreUG(): Single<Response<List<Potential>>> {
        return api.getTruePotentialPreUG()
    }

    fun fetchTruePotentialK12(): Single<Response<List<Potential>>> {
        return api.getTruePotentialK12()
    }

    fun fetchCrunchingCount(): Single<Response<CrunchingCount>> {
        return api.getCrunchingCount()
    }

    fun fetchDiagnosticTest(exam: String, goal: String): Single<Response<TestDiagnosticRes>> {
        return api.getDiagnosticMockTests(exam, goal)
    }

    fun updateAchieveUserProfile(status: Boolean): Single<Response<LinkedTreeMap<Any, Any>>> {
        return api.updateAchieveUserProfile(UpdateAchieveRequest(status))
    }

    fun fetchAchieveFeedbackRes(
        bundle_code: String,
        paj_id: String
    ): Single<Response<AchieveFeedbackRes>> {
        return api.getAchieveFeedbackRes(bundle_code, paj_id)
    }

    fun createPAJ(model: PAJCreateReq): Single<Response<AchieveFeedbackRes>> {
        return api.createPAJ(model)
    }


}


