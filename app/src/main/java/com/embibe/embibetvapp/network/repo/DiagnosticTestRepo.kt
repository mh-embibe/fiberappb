package com.embibe.embibetvapp.network.repo

import com.embibe.embibetvapp.network.apis.DiagnosticTestApi

class DiagnosticTestRepo constructor(val api: DiagnosticTestApi) {

    fun getDiagnosticTestList() = api.getDiagnosticTestList()

}