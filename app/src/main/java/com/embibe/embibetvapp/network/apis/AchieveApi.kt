package com.embibe.embibetvapp.network.apis

import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.model.achieve.AchieveListBase
import com.embibe.embibetvapp.model.achieve.Crunching.CrunchingCount
import com.embibe.embibetvapp.model.achieve.PercentageConnectionsRes
import com.embibe.embibetvapp.model.achieve.UpdateAchieveRequest
import com.embibe.embibetvapp.model.achieve.achieveFeedback.AchieveFeedbackRes
import com.embibe.embibetvapp.model.achieve.futureSuccess.FutureSuccessRes
import com.embibe.embibetvapp.model.achieve.keyconcepts.KeyConceptModel
import com.embibe.embibetvapp.model.achieve.paj.PAJCreateReq
import com.embibe.embibetvapp.model.achieve.readiness.Readiness
import com.embibe.embibetvapp.model.achieve.successStories.SuccessStory
import com.embibe.embibetvapp.model.diagnostic.TestDiagnosticRes
import com.embibe.embibetvapp.model.diagnostic.test.DiagnosticTest
import com.embibe.embibetvapp.model.potential.Potential
import com.google.gson.internal.LinkedTreeMap
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface AchieveApi {

    @GET(ApiConstants.URL_ACHIEVE_PERCENTAGE_CONNECTIONS)
    fun getPercentageConnections(
        @Query("goal") goal: String,
        @Query("grade") grade: String,
        @Query("exam") exam: String
    ): Single<Response<PercentageConnectionsRes>>

    @GET(ApiConstants.URL_ACHIEVE_KEY_RELATIONS)
    fun getKeyRelations(
        @Query("from_goal") from_goal: String,
        @Query("to_goal") to_goal: String,
        @Query("from_exam") from_exam: String,
        @Query("to_exam") to_exam: String,
        @Query("level") level: String,
        @Query("count") count: String
    ): Single<Response<ArrayList<KeyConceptModel>>>


    @GET(ApiConstants.URL_ACHIEVE_READINESS)
    fun getReadiness(
        @Query("childId") childId: String,
        @Query("exam") examCode: String
//        @Query("demo") demo: Boolean
    ): Single<Response<Readiness>>

    @GET(ApiConstants.URL_ACHIEVE_FUTURE_SUCCESS)
    fun getFutureSuccess(
        @Query("childId") childId: String
//        @Query("demo") demo: Boolean
    ): Single<Response<List<FutureSuccessRes>>>


    @GET(ApiConstants.URL_ACHIEVE_SUCCESS_STORIES)
    fun getSuccessStories(
        @Query("childId") childId: String,
        @Query("grade") grade: String
    ): Single<Response<List<SuccessStory>>>

    @GET(ApiConstants.URL_ACHIEVE_EDUCATION_QUALIFICATION)
    fun getEducationQualification(): Single<Response<AchieveListBase>>

    @GET(ApiConstants.URL_ACHIEVE_JOB_OPTIONS)
    fun getJobOptions(): Single<Response<AchieveListBase>>

    @GET(ApiConstants.URL_ACHIEVE_COLLEGES)
    fun getColleges(): Single<Response<AchieveListBase>>

    @GET(ApiConstants.URL_ACHIEVE_STATES)
    fun getStates(): Single<Response<AchieveListBase>>

    @GET(ApiConstants.URL_ACHIEVE_QUESTIONNAIRE_TRUE_POTENTIAL_PREPG)
    fun getTruePotentialPrePG(): Single<Response<List<Potential>>>

    @GET(ApiConstants.URL_ACHIEVE_QUESTIONNAIRE_TRUE_POTENTIAL_PREUG)
    fun getTruePotentialPreUG(): Single<Response<List<Potential>>>

    @GET(ApiConstants.URL_ACHIEVE_QUESTIONNAIRE_TRUE_POTENTIAL_K12)
    fun getTruePotentialK12(): Single<Response<List<Potential>>>

    @GET(ApiConstants.URL_ACHIEVE_CRUNCHING_VARIABLES)
    fun getCrunchingCount(): Single<Response<CrunchingCount>>

    @GET(ApiConstants.URL_ACHIEVE_DIAGNOSTIC_MOCK_TESTS)
    fun getDiagnosticMockTests(
        @Query("exam_name") exam_name: String,
        @Query("goal_name") goal_name: String
    ): Single<Response<TestDiagnosticRes>>

    @GET(ApiConstants.URL_ACHIEVE_DIAGNOSTIC_TESTS)
    fun getDiagnosticTests(
        @Query("exam") exam_name: String,
        @Query("goal") goal_name: String
    ): Single<Response<DiagnosticTest>>




    @PUT(ApiConstants.URL_ACHIEVE_UPDATE_USER)
    fun updateAchieveUserProfile(@Body model: UpdateAchieveRequest): Single<Response<LinkedTreeMap<Any, Any>>>

    @GET(ApiConstants.URL_ACHIEVE_PAJ)
    fun getAchieveFeedbackRes(
        @Query("bundle_code") bundle_code: String,
        @Query("paj_id") paj_id: String
    ): Single<Response<AchieveFeedbackRes>>

    @POST(ApiConstants.URL_ACHIEVE_PAJ)
    fun createPAJ(
        @Body model: PAJCreateReq
    ): Single<Response<AchieveFeedbackRes>>

}