package com.embibe.embibetvapp.network.interceptor

import com.embibe.embibetvapp.constant.AppConstants
import com.embibejio.coreapp.preference.PreferenceHelper
import okhttp3.Interceptor
import okhttp3.Response

class RequestInterceptorParent : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {

        val originalRequest = chain.request()
        val requestBuilder = originalRequest.newBuilder()

        val token: String = PreferenceHelper()[AppConstants.EMBIBETOKEN, ""].toString()
        requestBuilder.header(AppConstants.EMBIBETOKEN, token) // <-- this is the important line
        requestBuilder.header("Content-Type", "application/json")

        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}
