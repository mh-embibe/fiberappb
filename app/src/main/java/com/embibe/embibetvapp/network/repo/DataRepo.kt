package com.embibe.embibetvapp.network.repo

import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.model.UserData.getExamCode
import com.embibe.embibetvapp.model.UserData.getGoalCode
import com.embibe.embibetvapp.model.UserData.getGrade
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.newmodel.ResultsEntity_
import io.objectbox.Box
import io.objectbox.android.ObjectBoxLiveData
import org.jetbrains.anko.doAsync


class DataRepo {

    //private var contentBox: Box<Content> = App.boxStore.boxFor(Content::class.java)
    private var sectionBox: Box<ResultsEntity> = App.boxStore.boxFor(ResultsEntity::class.java)
    private var noteLiveData: ObjectBoxLiveData<Content?>? = null


    fun checkSection(section: ResultsEntity): ResultsEntity? {
        return sectionBox.query()?.equal(ResultsEntity_.sectionId, section.sectionId)
            ?.equal(ResultsEntity_.subject, section.subject)
            ?.equal(ResultsEntity_.grade, section.grade)
            ?.equal(ResultsEntity_.goal, section.goal)
            ?.equal(ResultsEntity_.exam, section.exam)
            ?.equal(ResultsEntity_.page, section.page)?.build()?.findFirst()

    }


    fun getSection(sectionId: Long, page: String? = null, subject: String? = null): ResultsEntity? {
        var pageName = page
        var subjectName = subject
        if (pageName == null)
            pageName = "learn"
        if (subjectName == null)
            subjectName = "all"
        var section =
            sectionBox.query()?.equal(ResultsEntity_.sectionId, sectionId)
                ?.equal(ResultsEntity_.page, pageName)
                ?.equal(ResultsEntity_.subject, subjectName)
                ?.equal(ResultsEntity_.grade, getGrade())
                ?.equal(ResultsEntity_.goal, getGoalCode())
                ?.equal(ResultsEntity_.exam, getExamCode())
                ?.build()?.findFirst()
        return section
    }


    /* fun saveContent(content: Content) {
         if (!checkContent(content.id))
             contentBox.put(content)
     }

     fun updateContent(content: Content) {
         contentBox.put(content)
     }*/

    /*fun saveContent(contents: List<Content>, childId: Long) {
        for (content in contents) {
            content.child_id = childId
            saveContent(content)
        }
    }*/

    @JvmOverloads
    fun saveSection(section: ResultsEntity) {
        var localSection = checkSection(section)
        if (localSection != null) {
            section.objId = localSection.objId
        }
        sectionBox.put(section)
    }

    fun modifySection(section: ResultsEntity, callback: BaseViewModel.DataCallback<ResultsEntity>) {
        doAsync {
            synchronized(section)
            {
                updateSection(section, callback)
            }
        }
    }

    @JvmOverloads
    fun updateSection(
        section: ResultsEntity,
        callback: BaseViewModel.DataCallback<ResultsEntity>
    ) {
        sectionBox.put(section)
        callback.onSuccess(section)
    }

    fun getAllSectionByPage(pageName: String, childId: Long, subject: String): List<ResultsEntity> {
        var secList =
            sectionBox.query()?.equal(ResultsEntity_.page, pageName)
                ?.equal(ResultsEntity_.child_id, childId)
                ?.notEqual(ResultsEntity_.sectionId, 100L)
                ?.equal(ResultsEntity_.subject, subject)
                ?.equal(ResultsEntity_.grade, getGrade())
                ?.equal(ResultsEntity_.goal, getGoalCode())
                ?.equal(ResultsEntity_.exam, getExamCode())
                ?.build()?.find()
        return if (secList != null) secList else ArrayList<ResultsEntity>()
    }

    fun getBannerSectionByPage(
        pageName: String,
        childId: Long,
        subject: String? = null
    ): ResultsEntity? {
        var subjectName = subject
        if (subjectName == null)
            subjectName = "all"
        var secList =
            sectionBox.query()?.equal(ResultsEntity_.page, pageName)
                ?.equal(ResultsEntity_.child_id, childId)
                ?.equal(ResultsEntity_.sectionId, 100L)
                ?.equal(ResultsEntity_.subject, subjectName)
                ?.equal(ResultsEntity_.grade, getGrade())
                ?.equal(ResultsEntity_.goal, getGoalCode())
                ?.equal(ResultsEntity_.exam, getExamCode())
                ?.build()?.findFirst()
        return if (secList != null) secList else null
    }

    fun clearBox() {
        try {
            sectionBox.removeAll()
            App.boxStore.removeAllObjects()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun removeSection(section: ResultsEntity) {
        try {
            sectionBox.remove(section.objId)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun removeListData(list: ArrayList<ResultsEntity>) {
        try {
            sectionBox.remove(list)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}