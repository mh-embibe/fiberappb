package com.embibe.embibetvapp.network.repo

import com.embibe.embibetvapp.model.NextVideoRecommendationsRes
import com.embibe.embibetvapp.model.PAJ.AchieveSection
import com.embibe.embibetvapp.model.achieve.home.AchieveHome
import com.embibe.embibetvapp.model.chapter.chapterLearningObjects.ChapterLearningObject
import com.embibe.embibetvapp.model.home.*
import com.embibe.embibetvapp.model.jiostb.JioUser
import com.embibe.embibetvapp.model.prerequisites.PrerequisitesResponse
import com.embibe.embibetvapp.model.relatedConcepts.ConceptsResponse
import com.embibe.embibetvapp.model.search.SearchResponse
import com.embibe.embibetvapp.model.test.TestDetailRequest
import com.embibe.embibetvapp.network.apis.HomeApi
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.newmodel.ResultsEntity
import io.reactivex.Single
import retrofit2.Response

class HomeRepository constructor(val api: HomeApi) {

    fun fetchJioSTBUser(headers: HashMap<String, String>): Single<Response<JioUser>> {
        return api.getJioSTBUser(headers)
    }

    fun fetchHome(req: HomeRequest): Single<Response<List<ResultsEntity>>> {
        return api.getHome(req)
    }

    fun fetchHomePractice(req: HomeRequest): Single<Response<List<ResultsEntity>>> {
        return api.getHomePractice(req)
    }

    fun homeSubjectFilter(
        req: HomeRequest,
        subject: String
    ): Single<Response<List<ResultsEntity>>> {
        return api.homeSubjectFilter(subject, req)
    }

    fun homeQuickLinks(req: HomeRequest): Single<Response<List<ResultsEntity>>> {
        return api.homeQuickLinks(req)
    }

    fun homeTest(req: HomeRequest): Single<Response<List<ResultsEntity>>> {
        return api.homeTest(req)
    }

    fun homeTestSubjectFilter(
        req: HomeRequest
    ): Single<Response<List<ResultsEntity>>> {
        return api.homeTestSubjectFilter(req)
    }

    fun fetchHomeSection(req: HomeRequest): Single<Response<List<ResultsEntity>>> {
        return api.getHomeLearnSection(req)
    }

    fun fetchHomePracticeSection(req: HomeRequest): Single<Response<List<ResultsEntity>>> {
        return api.getHomePracticeSection(req)
    }

    fun homeSubjectFilterSection(
        req: HomeRequest,
        subject: String
    ): Single<Response<List<ResultsEntity>>> {
        return api.homeSubjectFilterSection(subject, req)
    }

    fun homeQuickLinksSection(req: HomeRequest): Single<Response<List<ResultsEntity>>> {
        return api.homeQuickLinksSection(req)
    }

    fun fetchRelatedConcepts(
        conceptId: String,
        contentId: String
    ): Single<Response<ConceptsResponse>> {
        return api.getRelatedConcepts(conceptId, contentId)
    }

    fun fetchMoreTopics(
        topicName: String,
        contentId: String
    ): Single<Response<ConceptsResponse>> {
        return api.getMoreTopics(topicName, contentId)
    }

    fun fetchTopicsForChapter(
        request: TopicForChapterRequest
    ): Single<Response<List<Content>>> {
        return api.getTopicsForChapter(request)
    }

    fun fetchVideosForChapter(
        learnMapId: String,
        contentTypes: String
    ): Single<Response<List<ResultsEntity>>> {
        return api.getVideosForChapter(learnMapId, contentTypes)
    }

    fun fetchVideosForChapterV1(
        learnMapId: String,
        contentTypes: String
    ): Single<Response<List<ChapterLearningObject>>> {
        return api.getVideosForChapterV1(learnMapId, contentTypes)
    }

    fun fetchTestsForChapter(
        request: TestForChapterRequest
    ): Single<Response<List<Content>>> {
        return api.getTestsForChapter(request)
    }

    fun fetchPracticesForChapter(
        request: PracticeForChapterRequest
    ): Single<Response<List<Content>>> {
        return api.getPracticesForChapter(request)
    }

    fun fetchPrerequisitesForChapter(
        request: TopicForChapterRequest
    ): Single<Response<List<Content>>> {
        return api.getPrerequisitesForChapter(request)
    }

    fun fetchRecommendedLearningForTest(
        learnMapId: TestDetailRequest
    ): Single<Response<List<Results>>> {
        return api.getRecommendedLearningForTest(learnMapId)
    }

    fun fetchRecommendedPracticeForTest(
        request: TestDetailRequest
    ): Single<Response<List<Content>>> {
        return api.getRecommendedPracticeForTest(request)
    }

    fun fetchMoreTests(
        request: TestForChapterRequest
    ): Single<Response<List<Content>>> {
        return api.getMoreTests(request)
    }

    fun fetchRelatedTopics(
        request: PracticeForChapterRequest
    ): Single<Response<List<Content>>> {
        return api.getRelatedTopics(request)
    }

    fun fetchVideosForTopic(
        learnMapId: String,
        contentTypes: String
    ): Single<Response<List<ResultsEntity>>> {
        return api.getVideosForTopic(learnMapId, contentTypes)
    }

    fun fetchTestsForTopic(
        request: TestForChapterRequest
    ): Single<Response<List<Content>>> {
        return api.getTestsForTopic(request)
    }

    fun fetchPracticesForTopic(
        request: PracticeForChapterRequest
    ): Single<Response<List<Content>>> {
        return api.getPracticesForTopic(request)
    }

    fun fetchPrerequisitesForTopic(
        request: PracticeForChapterRequest
    ): Single<Response<List<Content>>> {
        return api.getPrerequisitesForTopic(request)
    }

    fun fetchMoreConcepts(
        conceptId: String,
        contentId: String
    ): Single<Response<ConceptsResponse>> {
        return api.getMoreConcepts(conceptId, contentId)
    }

    fun fetchPrerequisites(
        conceptId: String,
        contentId: String
    ): Single<Response<PrerequisitesResponse>> {
        return api.getPrerequisites(conceptId, contentId)
    }

    fun fetchSearchSuggestions(
        query: String,
        size: String,
        userId: String,
        grade: String,
        goal: String, examName: String
    ): Single<Response<SearchResponse>> {
        return api.getSuggestions(query, size, userId, grade, goal, examName)
    }

    fun fetchSearchResults(
        query: String,
        size: String,
        userId: String,
        grade: String,
        goal: String, examName: String
    ): Single<Response<SearchResponse>> {
        return api.getSearchResult(query, size, userId, grade, goal, examName)
    }


    fun fetchNextVideoRecommendations(
        contentId: String,
        source: String,
        bookId: String,
        topicLearnPathName: String,
        learnPathName: String,
        learnPathFormatName: String
    ): Single<Response<NextVideoRecommendationsRes>> {
        return api.getNextVideo(
            contentId,
            source,
            bookId,
            topicLearnPathName,
            learnPathName,
            learnPathFormatName
        )
    }

    fun fetchAutoPlay(
        conceptId: String,
        map: HashMap<String, String>
    ): Single<Response<NextVideoRecommendationsRes>> {
        return api.getAutoPlayVideo(conceptId, map)
    }


    fun searchAllVideos(
        request: SearchRequest
    ): Single<Response<List<Content>>> {
        return api.getSearchResultForAllVideos(request)
    }

    fun searchChapterTopics(
        request: SearchRequest
    ): Single<Response<List<Content>>> {
        return api.getSearchResultForCHapterTopics(request)
    }

    fun searchPrerequisite(
        request: SearchRequest
    ): Single<Response<List<Content>>> {
        return api.getSearchResultForPrerequisite(request)
    }

    fun searchChapterPractise(
        request: SearchRequest
    ): Single<Response<List<Content>>> {
        return api.getSearchResultForChapterPractice(request)
    }

    fun fetchAchieveHome(req: HomeRequest): Single<Response<List<AchieveHome>>> {
        return api.achieveHome(req)
    }

    fun fetchAchieveHomeSubject(subject:String, req: HomeRequest): Single<Response<List<AchieveHome>>> {
        return api.achieveHomeSubject(subject,req)
    }


    fun searchMoreTest(
        request: SearchRequest
    ): Single<Response<List<Content>>> {
        return api.searchMoreTests(request)
    }



}


