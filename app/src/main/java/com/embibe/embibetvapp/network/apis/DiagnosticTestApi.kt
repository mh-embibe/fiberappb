package com.embibe.embibetvapp.network.apis

import com.embibe.embibetvapp.model.diagnostic.TestDiagnosticRes
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface DiagnosticTestApi {

    @GET("fiber_ms/home/test/diagnostic-test-list")
    fun getDiagnosticTestList(): Single<Response<TestDiagnosticRes>>

}