package com.embibe.embibetvapp.bezierpath.customviews;

import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;


import com.embibe.embibetvapp.R;
import com.embibe.embibetvapp.bezierpath.base.BezierLineGraph;

import java.util.ArrayList;
import java.util.List;

public class BezierView extends View {

    private static final String TAG = "BezierView";

    private Context mContext;
    private final Path mPath = new Path();
    private final Paint mPaint = new Paint();
    private final Paint mPaintCircle = new Paint();

    private final BezierLineGraph mBezierLineGraph = new BezierLineGraph(34);

    private List<PointF> listOfPoints;
    private int x1, y1, x2, y2;
    List<Float> xCoordinates;
    List<Float> yCoordinates;

    BezierView(Context context) {
        super(context);
        this.mContext = context;
        final Paint paint = mPaint;
        paint.setAntiAlias(true);
        paint.setColor(ContextCompat.getColor(context, R.color.gray_100));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(context.getResources().getDisplayMetrics().density * 2F);

    }

    public BezierView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        //
        initView();

    }

    public BezierView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
        listOfPoints = new ArrayList<>();
        //
        initView();
    }

    private void initView() {
        listOfPoints = new ArrayList<>();
        xCoordinates = new ArrayList<>();
        yCoordinates = new ArrayList<>();

        xCoordinate();
        yCoordinates();

        //Line
        final Paint paint = mPaint;
        paint.setAntiAlias(true);
        paint.setColor(ContextCompat.getColor(mContext, R.color.seekProgressBuffer));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(mContext.getResources().getDisplayMetrics().density * 1F);
        //Circle
        final Paint paintCircle = mPaintCircle;
        paintCircle.setAntiAlias(true);
        paintCircle.setColor(ContextCompat.getColor(mContext, R.color.seekProgressBuffer));
        paintCircle.setStyle(Paint.Style.FILL_AND_STROKE);
        paintCircle.setStrokeWidth(mContext.getResources().getDisplayMetrics().density * 4F);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        //canvas.translate(0F, getHeight()/2F);

        final float width = getWidth();
        final float density = getResources().getDisplayMetrics().density;


        List<Point> pointList = new ArrayList<>();


        for (int knot = 0, knots = mBezierLineGraph.knots(); knot < knots; knot++) {
            Log.e(TAG, String.valueOf(knots = mBezierLineGraph.knots()));

            mBezierLineGraph.set(knot, xCoordinates.get(knot), yCoordinates.get(knot));
//            canvas.drawCircle(xCoordinates.get(knot), yCoordinates.get(knot), density * 4F, mPaintCircle);
        }

        mBezierLineGraph.applyToPath(mPath);
        canvas.drawPath(mPath, mPaint);
    }


    //TODO: This hardcoded logic needs to be changed. Need to convert to PointF. Need to come fro JSON
    private void xCoordinate() {

        xCoordinates = new ArrayList<>();
        xCoordinates.add(1070.5F);
        xCoordinates.add(979.0F);
        xCoordinates.add(879.5F);
        xCoordinates.add(780.0F);
        xCoordinates.add(682.5F);
        xCoordinates.add(580.0F);
        xCoordinates.add(481.5F);
        xCoordinates.add(383.0F);
        xCoordinates.add(300.5F);
        xCoordinates.add(256.0F);
        //X 20
        xCoordinates.add(272.5F);
        xCoordinates.add(358.5F);
        xCoordinates.add(455.0F);
        xCoordinates.add(551.5F);
        xCoordinates.add(640.0F);
        xCoordinates.add(735.0F);
        xCoordinates.add(829.5F);
        xCoordinates.add(927.0F);
        xCoordinates.add(1024.0F);
        xCoordinates.add(1065.0F);
        //X 30
        xCoordinates.add(1036.5F);
        xCoordinates.add(958.5F);
        xCoordinates.add(860.5F);
        xCoordinates.add(763.5F);
        xCoordinates.add(663.5F);
        xCoordinates.add(564.5F);
        xCoordinates.add(466.0F);
        xCoordinates.add(391.0F);
        xCoordinates.add(413.5F);
        xCoordinates.add(504.5F);
        //X 34
        xCoordinates.add(603.5F);
        xCoordinates.add(703.0F);
        xCoordinates.add(799.0F);
        xCoordinates.add(861.5F);
        xCoordinates.add(851.5F);
        xCoordinates.add(841.5F);
        xCoordinates.add(854.5F);
        xCoordinates.add(914.5F);

    }

    //TODO: This hardcoded logic needs to be changed. Need to convert to PointF. Need to come fro JSON
    private void yCoordinates() {
        yCoordinates = new ArrayList<>();
        yCoordinates.add(744.5F);
        yCoordinates.add(725.0F);
        yCoordinates.add(720.5F);
        yCoordinates.add(719.5F);
        yCoordinates.add(723.0F);
        yCoordinates.add(721.5F);
        yCoordinates.add(716.0F);
        yCoordinates.add(693.0F);
        yCoordinates.add(640.5F);
        yCoordinates.add(552.5F);
        //Y 20
        yCoordinates.add(457F);
        yCoordinates.add(406.5F);
        yCoordinates.add(396.5F);
        yCoordinates.add(422.5F);
        yCoordinates.add(463.5F);
        yCoordinates.add(502.0F);
        yCoordinates.add(530.5F);
        yCoordinates.add(547.0F);
        yCoordinates.add(523.5F);
        yCoordinates.add(438.5F);

        //Y 30
        yCoordinates.add(341.5F);
        yCoordinates.add(287.0F);
        yCoordinates.add(284.0F);
        yCoordinates.add(307.0F);
        yCoordinates.add(321.5F);
        yCoordinates.add(318.0F);
        yCoordinates.add(303.0F);
        yCoordinates.add(242.0F);
        yCoordinates.add(149.0F);
        yCoordinates.add(118.0F);

        //Y 34
        yCoordinates.add(128.5F);
        yCoordinates.add(155.5F);
        yCoordinates.add(159.0F);
        yCoordinates.add(93.0F);
        yCoordinates.add(133.0F);
        yCoordinates.add(143.0F);
        yCoordinates.add(165.5F);
        yCoordinates.add(185.5F);
    }

    private ValueAnimator createAnimator() {
        PropertyValuesHolder propertyX = PropertyValuesHolder.ofInt("PROPERTY_X", 100, 300);
        PropertyValuesHolder propertyY = PropertyValuesHolder.ofInt("PROPERTY_Y", 100, 300);
        PropertyValuesHolder propertyAlpha = PropertyValuesHolder.ofInt("PROPERTY_ALPHA", 0, 255);

        ValueAnimator animator = new ValueAnimator();
        animator.setValues(propertyX, propertyY, propertyAlpha);
        animator.setDuration(2000);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //TODO invalidate view with new values
            }
        });

        return animator;
    }
}

