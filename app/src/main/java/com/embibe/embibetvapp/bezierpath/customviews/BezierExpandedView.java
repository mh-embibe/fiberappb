package com.embibe.embibetvapp.bezierpath.customviews;

import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import androidx.core.content.ContextCompat;

import com.embibe.embibetvapp.R;
import com.embibe.embibetvapp.bezierpath.base.BezierLineGraph;

import java.util.ArrayList;
import java.util.List;

public class BezierExpandedView extends View {

    private static final String TAG = "BezierExpandedView";

    private Context mContext;
    private final Path mPath = new Path();
    private final Paint mPaint = new Paint();
    private final Paint mPaintCircle = new Paint();
    private final BezierLineGraph mBezierSpline = new BezierLineGraph(40);

    List<Float> xCoordinates;
    List<Float> yCoordinates;


    BezierExpandedView(Context context) {
        super(context);
        this.mContext = context;
        final Paint paint = mPaint;
        paint.setAntiAlias(true);
        paint.setColor(ContextCompat.getColor(context, R.color.gray_100));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(context.getResources().getDisplayMetrics().density * 4F);
    }

    public BezierExpandedView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        //
        initView();

    }

    public BezierExpandedView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
        //
        initView();
    }

    private void initView() {
        xCoordinates = new ArrayList<>();
        yCoordinates = new ArrayList<>();

        xCoordinate();
        yCoordinates();
        //
        final Paint paint = mPaint;
        paint.setAntiAlias(true);
        paint.setColor(ContextCompat.getColor(mContext, R.color.floral_white));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(mContext.getResources().getDisplayMetrics().density * 1F);
        //Circle
        //Circle
        final Paint paintCircle = mPaintCircle;
        paintCircle.setAntiAlias(true);
        paintCircle.setColor(ContextCompat.getColor(mContext, R.color.gray_100));
        paintCircle.setStyle(Paint.Style.FILL_AND_STROKE);
        paintCircle.setStrokeWidth(mContext.getResources().getDisplayMetrics().density * 4F);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.translate(300,-330);

        //final float width = getWidth();
        final float density = getResources().getDisplayMetrics().density;
        final float height = getResources().getDisplayMetrics().heightPixels;
        final float width = getResources().getDisplayMetrics().widthPixels;

        List<Point> pointList = new ArrayList<>();



        for (int knot = 0, knots = mBezierSpline.knots(); knot < knots; knot++) {
            Log.e(TAG, String.valueOf(knots = mBezierSpline.knots()));
//            final float x = knot * (width / (knots - 1F));
            //final float y = (float) (Math.toDegrees(Math.sin(Math.toRadians(xCoordinates.get(knot) / density))) * density);
            //if(knot==0){
            this.animate().setStartDelay(20000);
            // }
            mBezierSpline.set(knot, xCoordinates.get(knot), yCoordinates.get(knot));
            //TODO: This needs to be
            //canvas.drawCircle(xCoordinates.get(knot), yCoordinates.get(knot), density * 4F, mPaintCircle);


        }




        mBezierSpline.applyToPath(mPath);
        canvas.drawPath(mPath, mPaint);
    }

    //TODO: This hardcoded logic needs to be changed. Need to convert to PointF. Need to come fro JSON
    private void xCoordinate() {

        xCoordinates = new ArrayList<>();
        xCoordinates.add(435.0F);
        xCoordinates.add(376.5F);
        xCoordinates.add(291.0F);
        xCoordinates.add(198.5F);
        xCoordinates.add(124.5F);
        xCoordinates.add(109.5F);
        xCoordinates.add(175.5F);
        xCoordinates.add(268.5F);
        xCoordinates.add(352.0F);
        xCoordinates.add(448.5F);
        //X 20
        xCoordinates.add(530.0F);
        xCoordinates.add(609.0F);
        xCoordinates.add(643.5F);
        xCoordinates.add(589.0F);
        xCoordinates.add(500.0F);
        xCoordinates.add(412.0F);
        xCoordinates.add(329.0F);
        xCoordinates.add(240.0F);
        xCoordinates.add(145.5F);
        xCoordinates.add(94.5F);
        //X 30
        xCoordinates.add(118.0F);
        xCoordinates.add(171.5F);
        xCoordinates.add(264.0F);
        xCoordinates.add(358.5F);
        xCoordinates.add(436.0F);
        xCoordinates.add(528.5F);
        xCoordinates.add(610.5F);
        xCoordinates.add(643.5F);
        xCoordinates.add(590.5F);
        xCoordinates.add(500.0F);
        //X 34
        xCoordinates.add(406.5F);
        xCoordinates.add(328.0F);
        xCoordinates.add(239.0F);
        xCoordinates.add(148.5F);
        xCoordinates.add(94.0F);
        xCoordinates.add(114.5F);
        xCoordinates.add(173.5F);
        xCoordinates.add(262.5F);
        xCoordinates.add(340.5F);
        xCoordinates.add(425.0F);
    }

    //TODO: This hardcoded logic needs to be changed. Need to convert to PointF. Need to come from JSON
    private void yCoordinates() {
        yCoordinates = new ArrayList<>();
        yCoordinates.add(1320.5F);
        yCoordinates.add(1258.5F);
        yCoordinates.add(1254.0F);
        yCoordinates.add(1247.0F);
        yCoordinates.add(1201.5F);
        yCoordinates.add(1119.5F);
        yCoordinates.add(1058.0F);
        yCoordinates.add(1041.5F);
        yCoordinates.add(1049.0F);
        yCoordinates.add(1066.0F);
        //Y 20
        yCoordinates.add(1067.0F);
        yCoordinates.add(1027.5F);
        yCoordinates.add(952.0F);
        yCoordinates.add(882.0F);
        yCoordinates.add(867.0F);
        yCoordinates.add(885.0F);
        yCoordinates.add(916.0F);
        yCoordinates.add(935.0F);
        yCoordinates.add(924.0F);
        yCoordinates.add(859.5F);

        //Y 30
        yCoordinates.add(772.0F);
        yCoordinates.add(733.5F);
        yCoordinates.add(716.5F);
        yCoordinates.add(726.0F);
        yCoordinates.add(742.0F);
        yCoordinates.add(743.0F);
        yCoordinates.add(708.5F);
        yCoordinates.add(623.5F);
        yCoordinates.add(558.5F);
        yCoordinates.add(546.0F);

        //Y 34
        yCoordinates.add(566.5F);
        yCoordinates.add(591.0F);
        yCoordinates.add(614.0F);
        yCoordinates.add(604.0F);
        yCoordinates.add(533.5F);
        yCoordinates.add(451.0F);
        yCoordinates.add(407.0F);
        yCoordinates.add(389.5F);
        yCoordinates.add(395.5F);
        yCoordinates.add(369.5F);
    }


    private ValueAnimator createAnimator() {
        PropertyValuesHolder propertyX = PropertyValuesHolder.ofInt("PROPERTY_X", 100, 300);
        PropertyValuesHolder propertyY = PropertyValuesHolder.ofInt("PROPERTY_Y", 100, 300);
        PropertyValuesHolder propertyAlpha = PropertyValuesHolder.ofInt("PROPERTY_ALPHA", 0, 255);

        ValueAnimator animator = new ValueAnimator();
        animator.setValues(propertyX, propertyY, propertyAlpha);
        animator.setDuration(2000);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //TODO invalidate view with new values
            }
        });

        return animator;
    }
}

