package com.embibe.embibetvapp.bezierpath.fragment

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.graphics.PointF
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.databinding.FragmentBezierCurveBinding
import com.embibe.embibetvapp.ui.custom.ArcView
import kotlin.math.atan2
import kotlin.math.pow
import kotlin.math.sqrt

class BezierFragment: BaseAppFragment() {

    private var TAG = BezierFragment::class.java.simpleName
    private lateinit var binding: FragmentBezierCurveBinding
    private lateinit var spheresPositionList: ArrayList<PointF>
    private lateinit var arcPointsList: ArrayList<Pair<PointF, PointF>>
    private lateinit var spheresList: ArrayList<ImageButton>
    private lateinit var arcsList: ArrayList<ArcView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_bezier_curve, container, false
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fillPoints()
        spheresList = ArrayList()
        showPoints(spheresPositionList)

        spheresList.forEach { sphere ->
            getPositionLogs(sphere)
        }

        printPositionLogs()

        generateArc()
    }

    private fun printPositionLogs() {
        Log.i(TAG, "Angle1 between 2 points - ${angleOf(spheresPositionList[1], spheresPositionList[0])}")
        Log.i(TAG, "Angle2 between 2 points - ${angleOf(spheresPositionList[0], spheresPositionList[1])}")
        Log.i(TAG, "Distance between 2 points - ${distanceBetweenTwoPoints(spheresPositionList[0], spheresPositionList[1])}")
    }

    private fun distanceBetweenTwoPoints(p1: PointF, p2: PointF): Int {
        return sqrt(
            (p2.x - p1.x).pow(2) + (p2.y - p1.y).pow(2)
        ).toInt()
    }

    private fun angleOf(p1: PointF, p2: PointF): Double {
        // NOTE: Remember that most math has the Y axis as positive above the X.
        // However, for screens we have Y as positive below. For this reason,
        // the Y values are inverted to get the expected results.
        val deltaY = (p1.y - p2.y).toDouble()
        val deltaX = (p2.x - p1.x).toDouble()
        val result = Math.toDegrees(atan2(deltaY, deltaX))
        return if (result < 0) 360.0 + result else result
    }

    private fun getPositionLogs(randomPoint: ImageButton?) {
        val arr1 = IntArray(2)
        randomPoint?.getLocationOnScreen(arr1)
        val arr2 = IntArray(2)
        randomPoint?.getLocationInWindow(arr2)
        val rect = Rect()
//        randomPoint?.getLocalVisibleRect(rect)
        randomPoint?.getGlobalVisibleRect(rect)
        Log.i(TAG, "X=${randomPoint?.x} Y=${randomPoint?.y}")
        Log.i(TAG, "left=${randomPoint?.left} right=${randomPoint?.right}")
        Log.i(TAG, "LOS -> X=${arr1[0]} Y=${arr1[1]}")
        Log.i(TAG, "LOW -> X=${arr2[0]} Y=${arr2[1]}")
        Log.i(TAG, "Rect -> X=${rect.left} Y=${rect.top}")
    }

    private fun generateArc() {

        var curveRadius = 400
        val angleGap = curveRadius / arcPointsList.size * 2
        arcPointsList.forEach { arcPoints ->
            val arcView = ArcView(context)
            arcView.setPointA(arcPoints.first)
            arcView.setPointB(arcPoints.second)
            arcView.setCurveRadius(curveRadius)
            arcView.draw()

            arcView.apply {
                id = View.generateViewId()
            }
            binding.bezierMainCL.addView(arcView, 0)

            arcsList.add(arcView)
            curveRadius -= angleGap
        }
    }


    private fun fillPoints() {
        spheresPositionList = ArrayList()
        spheresPositionList.add(PointF(455f,329f))
        spheresPositionList.add(PointF(1305f,329f))
        arcPointsList = ArrayList()
        arcsList = ArrayList()
        val conceptsCount = 1 //max count
        /**
         * 1,2->100
         * 4 -> 60
         * 3,5,6 -> 40
         * 7 -> 30
         * 8,9,10,11 -> 20
         * 12,13,14 -> 15
         * 15 -> 10
         */
        val conceptCountYCoordMapping = HashMap<Int, Int>()
//        arcPointsList.add(Pair(PointF(500f, 409f), PointF(1550f, 409f)))
        var y = if (conceptsCount > 3)
            389f
        else
            489f

        for (i in 0 until conceptsCount) {
            arcPointsList.add(Pair(PointF(500f, y), PointF(1550f, y)))
            y += 100f
        }
    }

    private fun showPoints(list: ArrayList<PointF>) {
        list.forEach {coord ->
            val point = createPoint()
            val pvhTransX = PropertyValuesHolder.ofFloat(View.TRANSLATION_X, coord.x)
            val pvhTransY = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, coord.y)
            ObjectAnimator.ofPropertyValuesHolder(point, pvhTransX, pvhTransY).setDuration(0).start()
            binding.bezierMainCL.addView(point, 0)
            spheresList.add(point!!)
        }
    }

    private fun createPoint(): ImageButton? {
        val hexButton = ImageButton(context)
        val dimens = ConstraintLayout.LayoutParams(300, 300)
        hexButton.apply {
            id = View.generateViewId()
            background = ContextCompat.getDrawable(context, R.drawable.key_concept)
            layoutParams = dimens
        }
        return hexButton
    }

}