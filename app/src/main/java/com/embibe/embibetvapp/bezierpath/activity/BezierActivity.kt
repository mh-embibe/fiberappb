package com.embibe.embibetvapp.bezierpath.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.bezierpath.fragment.BezierFragment


class BezierActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bezier_curve)
        supportFragmentManager.beginTransaction().replace(R.id.bezier_container_FL, BezierFragment()).commit()
    }

}