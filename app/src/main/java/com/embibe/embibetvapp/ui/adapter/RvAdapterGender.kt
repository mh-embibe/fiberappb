package com.embibe.embibetvapp.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.RowItemGenderBinding
import com.embibe.embibetvapp.utils.ContantUtils

class RvAdapterGender() : RecyclerView.Adapter<RvAdapterGender.VHGender>() {

    lateinit var binding: RowItemGenderBinding
    private var selectedItem = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHGender {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_item_gender,
            parent,
            false
        )
        return VHGender(binding)
    }

    override fun getItemCount(): Int {
        return ContantUtils.genderData.size
    }

    override fun onBindViewHolder(holder: VHGender, position: Int) {
        holder.bind(position)
    }

    //
    inner class VHGender(var binding: RowItemGenderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.tvGender.text = ContantUtils.genderData[position]

            binding.clItem.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    binding.tvGender.setTextColor(Color.parseColor("#000000"))
                } else {
                    binding.tvGender.setTextColor(Color.parseColor("#ffffff"))
                }
            }

            if (selectedItem == adapterPosition) {
                binding.clItem.setBackgroundResource(R.drawable.bg_acheive_potential_selected)
                binding.tvGender.setTextColor(Color.parseColor("#000000"))
            } else {
                binding.clItem.setBackgroundResource(R.drawable.bg_acheive_potential_selector)
                binding.tvGender.setTextColor(Color.parseColor("#ffffff"))
            }
            itemView.setOnClickListener {
                if (selectedItem != adapterPosition) {
                    selectedItem = adapterPosition
                    notifyDataSetChanged()
                } else {
                    selectedItem = -1
                    notifyDataSetChanged()
                }
            }
        }
    }
}