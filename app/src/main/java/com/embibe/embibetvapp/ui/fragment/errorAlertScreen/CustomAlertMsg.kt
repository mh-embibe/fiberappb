package com.embibe.embibetvapp.ui.fragment.errorAlertScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.FragmentCustomAlertMsgBinding

class CustomAlertMsg : DialogFragment() {

    private var msg: String? = null
    private lateinit var binding: FragmentCustomAlertMsgBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, android.R.style.ThemeOverlay)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_custom_alert_msg,
            container, false
        )
        binding.ivPositive.setImageResource(R.drawable.ic_custom_alert_msg)
        if (!msg.isNullOrEmpty())
            binding.tvMsg2.text = msg
        setBtnOkayClickListener()
        return binding.root
    }

    private fun setBtnOkayClickListener() {
        binding.btnOkay.setOnClickListener {
            dismiss()
        }
    }

    fun setMsg(msg: String) {
        this.msg = msg
    }

}