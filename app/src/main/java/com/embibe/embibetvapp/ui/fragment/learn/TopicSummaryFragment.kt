package com.embibe.embibetvapp.ui.fragment.learn

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentLearnSummaryBinding
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.activity.*
import com.embibe.embibetvapp.ui.adapter.TopicSummaryAdapter
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.insertBundle
import com.embibe.embibetvapp.utils.Utils.loadConceptsAsync
import com.embibe.embibetvapp.utils.Utils.loadLearnSummaryAsync
import com.embibe.embibetvapp.utils.Utils.loadTopicSummaryAsync
import java.util.*
import kotlin.collections.ArrayList


class TopicSummaryFragment : BaseAppFragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var results: List<Content>? = null
    private var resultsVideos: List<ResultsEntity>? = null
    private lateinit var type: String
    private lateinit var binding: FragmentLearnSummaryBinding
    private lateinit var topicSummaryAdapter: TopicSummaryAdapter
    private var TAG = this.javaClass.simpleName
    private var searchQuery: String = ""
    private var searchResultSize: Int = 0
    private var speechRecognizer: SpeechRecognizer? = null
    private var speechIntent: Intent? = null
    private var countDownTimer: CountDownTimer? = null
    private val speechTimerDuration = 10000L // 10 secs
    private var elapsedSpeechTimer = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(activity)
        setupSpeechRecognitionListener()
        speechIntent = getRecognizerIntent()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_learn_summary, container, false)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        type = this.requireArguments().getString("type").toString()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getData(view)
        micSearchButtonListeners()
    }

    private fun micSearchButtonListeners() {

        binding.micSearch.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (elapsedSpeechTimer != 0L) {
                            stopRecognition()
                            countDownTimer?.cancel()
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_DPAD_UP -> {
                        binding.micSearch.postDelayed({
                            binding.micSearch.requestFocus()
                        }, 0)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        startRecognition()
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        toggleFocusForTheView(binding.detailRecycler, true)
                    }
                    KeyEvent.KEYCODE_VOICE_ASSIST -> {
//                        Toast.makeText(context, "Voice assist pressed", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            false
        }
    }

    private fun toggleFocusForTheView(view: View, askFocus: Boolean) {

        view.postDelayed({
            when (askFocus) {
                true -> {
                    view.requestFocus()
                }
                false -> {
                    view.clearFocus()
                }
            }
        }, 10)
    }

    private fun startRecognition() {
        if (Utils.hasPermission(requireContext(), Manifest.permission.RECORD_AUDIO)) {
            speechRecognizer?.startListening(speechIntent)
            if (speechRecognizer != null) setSpeechRecognitionTimer()
        } else {
            Toast.makeText(
                context,
                requireContext().resources.getString(R.string.audio_permission_not),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun setSpeechRecognitionTimer() {
        elapsedSpeechTimer = 0L
        countDownTimer = object : CountDownTimer(speechTimerDuration, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                elapsedSpeechTimer = (speechTimerDuration - millisUntilFinished) / 1000
                Log.i(TAG, "$elapsedSpeechTimer")
            }

            override fun onFinish() {
                stopRecognition()
            }
        }
        countDownTimer?.start()
    }

    private fun stopRecognition() {
        finishMicSearchAnimation()
        speechRecognizer?.cancel()
    }

    private fun finishMicSearchAnimation() {
        binding.micSearchLav.cancelAnimation()
        binding.micSearchLav.visibility = View.INVISIBLE
        binding.micSearch.setBackgroundResource(R.drawable.bg_voice_search)
    }

    fun startMicSearchAnimation() {
        binding.micSearchLav.visibility = View.VISIBLE
        binding.micSearch.setBackgroundResource(0)
        binding.micSearchLav.playAnimation()
    }

    private fun setupSpeechRecognitionListener() {

        speechRecognizer?.setRecognitionListener(object : RecognitionListener {
            override fun onReadyForSpeech(params: Bundle?) {
                Log.v(TAG, "onReadyForSpeech")
                startMicSearchAnimation()
            }

            override fun onRmsChanged(rmsdB: Float) {
//                Log.v(TAG, "onRmsChanged $rmsdB")
            }

            override fun onBufferReceived(buffer: ByteArray?) {
//                Log.v(TAG, "onBufferReceived " + buffer?.size)
            }

            override fun onPartialResults(partialResults: Bundle?) {
                val results =
                    partialResults?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                Log.v(
                    TAG, "onPartialResults " + partialResults + " results "
                            + (results?.size ?: results)
                )
            }

            override fun onEvent(eventType: Int, params: Bundle?) {
            }

            override fun onBeginningOfSpeech() {
                Log.v(TAG, "onBeginningOfSpeech")
            }

            override fun onEndOfSpeech() {
                Log.v(TAG, "onEndOfSpeech")
                countDownTimer?.cancel()
            }

            override fun onError(error: Int) {
                when (error) {
                    SpeechRecognizer.ERROR_NETWORK_TIMEOUT -> Log.w(
                        TAG,
                        "recognizer network timeout"
                    )
                    SpeechRecognizer.ERROR_NETWORK -> Log.w(
                        TAG,
                        "recognizer network error"
                    )
                    SpeechRecognizer.ERROR_AUDIO -> {
                        Log.w(
                            TAG,
                            "recognizer audio error"
                        )
                    }
                    SpeechRecognizer.ERROR_SERVER -> Log.w(
                        TAG,
                        "recognizer server error"
                    )
                    SpeechRecognizer.ERROR_CLIENT -> Log.w(
                        TAG,
                        "recognizer client error"
                    )
                    SpeechRecognizer.ERROR_SPEECH_TIMEOUT -> Log.w(
                        TAG,
                        "recognizer speech timeout"
                    )
                    SpeechRecognizer.ERROR_NO_MATCH -> Log.w(
                        TAG,
                        "recognizer no match"
                    )
                    SpeechRecognizer.ERROR_RECOGNIZER_BUSY -> Log.w(
                        TAG,
                        "recognizer busy"
                    )
                    SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS -> Log.w(
                        TAG,
                        "recognizer insufficient permissions"
                    )
                    else -> Log.d(TAG, "recognizer other error")
                }

                countDownTimer?.cancel()
                stopRecognition()
            }

            override fun onResults(results: Bundle?) {
                val resultsList = results?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                Log.v(
                    TAG, "onResults " + resultsList + " results "
                            + (resultsList?.size ?: resultsList)
                )
                if (resultsList!!.isNotEmpty()) {
                    binding.textSearch.text.clear()
                    val searchQuery = resultsList[0].toLowerCase(Locale.ENGLISH)
                    performSearch(searchQuery)

                    finishMicSearchAnimation()
                    countDownTimer?.cancel()
                }
            }

        })
    }

    private fun getRecognizerIntent(): Intent? {
        val recognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        recognizerIntent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "en-US")
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, true)

        return recognizerIntent
    }

    override fun onPause() {
        releaseRecognizer(false)
        super.onPause()
    }

    override fun onStop() {
        countDownTimer?.cancel()
        releaseRecognizer(true)
        super.onStop()
    }

    private fun releaseRecognizer(isDestroyed: Boolean) {
        if (null != speechRecognizer) {
            speechRecognizer?.setRecognitionListener(null)
            if (isDestroyed)
                speechRecognizer?.destroy()
        }
    }

    private fun performSearch(searchQuery: String) {
        //bind search text to textSearch text view
        binding.textSearch.setText(searchQuery)

        /*api call for load suggestions only*/
//        getSearchSuggestions(searchQuery, "10")
        /*api call for load suggestions with search results*/
//        getSuggestionsAndHitSearch(searchQuery, "10")
    }

    private fun getData(view: View) {
        when (type) {
            getString(R.string.all_videos_for_this_topic) -> {
                binding.textSearch.hint = getString(R.string.search)
                binding.tvDetailName.text = type
                resultsVideos =
                    requireArguments().getParcelableArrayList(AppConstants.ALL_VIDEOS_FOR_THIS_TOPIC)
                        ?: arrayListOf()
            }
            getString(R.string.test_on_this_topic) -> {
                binding.textSearch.hint = getString(R.string.search_tests)
                binding.tvDetailName.text = getString(R.string.tests_covering_this_chapter)
                results = requireArguments().getParcelableArrayList(AppConstants.TEST_ON_THIS_TOPIC)
                    ?: arrayListOf()
            }
            getString(R.string.practice_on_this_topic) -> {
                binding.textSearch.hint = getString(R.string.search_practices)
                binding.tvDetailName.text = type
                results =
                    requireArguments().getParcelableArrayList(AppConstants.PRACTICE_ON_THIS_TOPIC)
                        ?: arrayListOf()
            }
            getString(R.string.related_topics) -> {
                binding.textSearch.hint = getString(R.string.search)
                binding.tvDetailName.text = type
                results =
                    requireArguments().getParcelableArrayList(AppConstants.RELATED_TOPICS)
                        ?: arrayListOf()
            }
            getString(R.string.pre_requisite_topics) -> {
                binding.textSearch.hint = getString(R.string.search)
                binding.tvDetailName.text = getString(R.string.pre_requisites_for_this_chapter)
                results =
                    requireArguments().getParcelableArrayList(AppConstants.PRE_REQUISITE_TOPICS)
                        ?: arrayListOf()
            }
        }

        val data = createData(results, resultsVideos)
        setDetailRecycler(view, data)
    }

    override fun onResume() {
        super.onResume()
        if (speechRecognizer != null)
            setupSpeechRecognitionListener()
    }

    private fun setDetailRecycler(view: View, data: ArrayList<Content>) {
        topicSummaryAdapter = TopicSummaryAdapter(view.context)
        val layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        binding.detailRecycler.adapter = topicSummaryAdapter
        binding.detailRecycler.layoutManager = layoutManager
        topicSummaryAdapter.setData(data, type)

        topicSummaryAdapter.onItemClick = { content ->
            startActivityByType(content)
        }
    }

    private fun startActivityByType(content: Content) {

        when ((content.type).toLowerCase()) {
            AppConstants.TYPE_VIDEO, AppConstants.TYPE_COOBO -> {
                startDetailActivity(content)
            }
            AppConstants.TYPE_BOOK -> {
                startBookDetailActivity(content)
            }
            AppConstants.TYPE_PRACTICE, AppConstants.TYPE_CHAPTER -> {
                startPracticeDetailActivity(content)
            }
            AppConstants.NAV_TYPE_SYLLABUS -> {
                startLearnSummaryActivity(content)
            }
            AppConstants.TYPE_TOPIC -> {
                startTopicSummaryActivity(content)
            }
        }
    }

    private fun startLearnSummaryActivity(item: Content) {
        loadLearnSummaryAsync(item, context)
        val intent = Intent(context, LearnSummaryActivity::class.java)
        insertBundle(item, intent)
        startActivity(intent)
    }

    private fun startTopicSummaryActivity(item: Content) {
//        getLastWatchedContent(context, AppConstants.EMBIBE_SYLLABUS, item.id)
        loadTopicSummaryAsync(item, context)
        val intent = Intent(context, TopicSummaryActivity::class.java)
        insertBundle(item, intent)
        startActivity(intent)
    }

    private fun startDetailActivity(content: Content) {
        loadConceptsAsync(content, context)
        val intent = Intent(context, DetailActivity::class.java)
        insertBundle(content, intent)
        startActivity(intent)
    }

    private fun startBookDetailActivity(content: Content) {
        var intent = Intent(context, BookDetailActivity::class.java)
        intent.putExtra(AppConstants.NAV_NAME_TYPE, AppConstants.NAV_TYPE_BOOK)
        insertBundle(content, intent)
        startActivity(intent)
    }

    private fun startPracticeDetailActivity(content: Content) {

        Utils.loadPracticeSummaryInfo(content, context)
        val intent = Intent(context, PracticeSummaryActivity::class.java)
        insertBundle(content, intent)
        startActivity(intent)
    }

    private fun createData(
        results: List<Content>?,
        resultsVideos: List<ResultsEntity>?
    ): ArrayList<Content> {
        val dataList: ArrayList<Content> = ArrayList()

        if (type == getString(R.string.all_videos_for_this_topic)) {
            makeLog("results size - ${resultsVideos?.size}")
            resultsVideos?.forEach { resultEntity ->
                resultEntity.content?.forEach { content ->
                    dataList.add(content)
                }
            }
        } else {
            makeLog("results size - ${results?.size}")
            results?.forEach { content ->
                dataList.add(content)
            }
        }
        return dataList
    }

    fun requestFocus() {
        binding.detailRecycler.postDelayed({
            binding.detailRecycler.requestFocus()
        }, 10)
    }
}
