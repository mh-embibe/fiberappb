package com.embibe.embibetvapp.ui.activity

import android.animation.Animator
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.webkit.*
import android.widget.Button
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.airbnb.lottie.LottieDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ActivityTestSummaryBinding
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkDetailModel
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkResponse
import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.embibe.embibetvapp.model.response.attemptquality.AttemptQualityResponse
import com.embibe.embibetvapp.model.test.TestDetail
import com.embibe.embibetvapp.model.test.TestDetailsRes
import com.embibe.embibetvapp.model.test.TestReadiness
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.ui.fragment.learn.TestSummaryFragment
import com.embibe.embibetvapp.ui.fragment.test.AboutTestFragment
import com.embibe.embibetvapp.ui.viewmodel.DetailsViewModel
import com.embibe.embibetvapp.ui.viewmodel.TestViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo
import com.github.penfeizhou.animation.apng.APNGDrawable
import com.github.penfeizhou.animation.loader.ResourceStreamLoader
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import java.util.*

class TestSummaryActivity : BaseFragmentActivity() {

    private var bundleId: String = ""
    private lateinit var binding: ActivityTestSummaryBinding
    private lateinit var testViewModel: TestViewModel
    private lateinit var detailsViewModel: DetailsViewModel

    private lateinit var listener: SharedPreferences.OnSharedPreferenceChangeListener
    private lateinit var data: Content
    private lateinit var bannerData: BannerData
    private var testDetail: TestDetail? = null
    private lateinit var aboutTestFragment: AboutTestFragment
    private lateinit var attemptQualityRes: AttemptQualityResponse
    private var recommendedLearningForTest: ArrayList<Results> = arrayListOf()
    private var recommendedPracticeForTest: ArrayList<Content> = arrayListOf()
    private var moreTests: ArrayList<Content> = arrayListOf()
    private var isBundleData: Boolean = false
    private var pref = PreferenceHelper()
    private var isBookMarked: Boolean = false
    private var isLiked: Boolean = false
    private var bookmarkRetryCount: Int = 0
    private var likeRetryCount: Int = 0
    private var jarType: String = ""
    private var mLastKeyDownTime: Long = 0

    var query = ""
    var token = ""
    var title = ""
    var childId = 0L

    private var preview_url =
        "https://visualintelligence.blob.core.windows.net/video-summary/prod/4397012/summary.webp"
    private var thumb1 =
        "https://cg-production.s3.amazonaws.com/images/95af3bf6-4d80-4781-924c-591fbd7b7077.webp"
    private var gif_preview = "https://media.giphy.com/media/xT5LMvV7tQfpptiuzK/giphy.gif"
    private var isFisrt = true
    private var isJarEnabled: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SegmentUtils.trackTestSummaryScreenLoadStart()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_test_summary)
        testViewModel = ViewModelProviders.of(this).get(TestViewModel::class.java)
        detailsViewModel = ViewModelProviders.of(this).get(DetailsViewModel::class.java)

        getData()
        data = if (isBundleData) {
            bannerData.testList[0]
        } else {
            data
        }
        bundleId = data.bundle_id

        hideButtons()
        setWebViewTestSummaryGraph(binding.wvPrimaryProgress)

        listenForDataPrefsUpdate()
        setClickListeners()
        setKeyListeners()
        setFocusListeners()
        permissionsCheck()
        setJarFocusListener()
        getTestReadiness()
        initViewSincerity()
        getAttemptQualityData()
        getTestDetails()
        setSearchTextChangeListener()
    }

    override fun onResume() {
        super.onResume()
        SegmentUtils.trackTestSummaryScreenLoadEnd()
    }

    private fun getTestKGData() {
        detailsViewModel.getTestKGGraph(
            data.bundle_id,
            object : BaseViewModel.APICallBacks<List<LinkedTreeMap<Any, Any>>> {
                override fun onSuccess(model: List<LinkedTreeMap<Any, Any>>?) {
                    if (model != null) {
                        var responseData = Gson().toJson(model).toString()
                        makeLog("Test KG data $responseData")
                        binding.wvPrimaryProgress.evaluateJavascript(
                            "javascript:setData($responseData)",
                            null
                        )
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("KG DAta$code$error$msg")
                }
            })
    }

    private fun getData() {
        if (intent.extras != null && intent.extras!!.containsKey(AppConstants.BANNER_CONTENT)) {
            bannerData = intent.getParcelableExtra(AppConstants.BANNER_CONTENT) as BannerData

            bindDataToUI(
                bannerData.title, bannerData.description, bannerData.embiumCoins,
                bannerData.testList.size.toString(), bannerData.subject, ""
            )

            isBundleData = true
            token = pref[AppConstants.EMBIBETOKEN, ""]
            childId = UserData.getChildId().toLong()
            query = "token=$token&child_id=$childId"

            getBookmarkStatus(
                (bannerData.testList as ArrayList<Content>)[0], AppConstants.MULTI_TYPE_TEST
            )
            getLikeStatus(
                (bannerData.testList as ArrayList<Content>)[0], AppConstants.MULTI_TYPE_TEST
            )
            Utils.setBlurBackgroundImage(null)
        } else {
            data = getContent(intent)
            updateVisibility()
            Log.w("xpath", "Logged format_id : " + data.format_id +", testStatus : ${data.test_status}")
            if (data.sub_type != AppConstants.CUSTOM_TEST) {
                bindDataToUI(
                    data.title, data.description, data.currency.toString(),
                    "", data.subject, ""
                )
            } else {
                bindDataToUI(
                    data.title, data.description, data.currency.toString(),
                    data.chapterSize, data.subject, data.sub_type
                )
                binding.tvNTests.visibility = View.VISIBLE
                binding.btnBookmark.visibility = View.GONE

            }
            isBundleData = false
            getBookmarkStatus(data, AppConstants.SINGLE_TYPE_TEST)
            getLikeStatus(data, AppConstants.SINGLE_TYPE_TEST)

            setBackground()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun bindDataToUI(
        title: String,
        description: String,
        embiumCoins: String,
        nTests: String,
        subject: String,
        type: String
    ) {
        Log.d(TAG, data.title)
        var updateStr = Utils.getTruncatedString(data.title, 25)
        binding.title.text = updateStr

        binding.textDescription.text = description
        binding.embiumsCount.text = "${getString(R.string.earn)} $embiumCoins"
        if (type != AppConstants.CUSTOM_TEST) {
            binding.linSubjectName.visibility = View.GONE
            binding.tvNTests.text = "$nTests ${getString(R.string.test).toLowerCase(Locale.ROOT)}"
            if (subject.isNotEmpty())
                binding.tvSubjectName.text = subject
            else
                binding.tvSubjectName.visibility = View.GONE

        } else {
            binding.tvNTests.text = nTests
            binding.tvSubjectName.text = subject
        }

        Utils.changeBannerTitleTextSize(binding.title)
    }

    private fun updateVisibility() {
        binding.tvNTests.visibility = View.GONE
        binding.btnTest.visibility = View.VISIBLE
        binding.btnBookmark.visibility = View.VISIBLE
    }

    private fun setWebViewTestSummaryGraph(webView: WebView) {
        val indexPath = "TestKGwidget/index.html"
        val template = "file:///android_asset/$indexPath"
        webView.webChromeClient = WebChromeClient()
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                Log.w("WEBVIEW", "Web view ready")
                getTestKGData()
            }

            override fun onReceivedError(
                view: WebView,
                request: WebResourceRequest,
                error: WebResourceError
            ) {
                //isLoaded = false
                Log.w("WEBVIEW", "Web view failed to load")
                val errorMessage = "$error"
                makeLog(errorMessage)
                //setProgressDialogVisibility(false)
                super.onReceivedError(view, request, error)
            }
        }
        webView.settings.allowFileAccess = true
        webView.settings.allowFileAccessFromFileURLs = true
        webView.settings.allowUniversalAccessFromFileURLs = true
        webView.isLongClickable = false
        webView.settings.javaScriptEnabled = true
        webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        webView.settings.domStorageEnabled = true
        webView.setBackgroundColor(Color.argb(0, 0, 0, 0))
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        webView.loadUrl(template)
    }

    private fun getTestDetails() {
        testViewModel.getTestDetails(
            data.bundle_id, object : BaseViewModel.APICallBacks<TestDetailsRes> {
                override fun onSuccess(model: TestDetailsRes?) {
                    hideProgress()
                    if (model != null) {
                        initializeTestDetail(model, data)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                }
            })
    }

    private fun initializeTestDetail(model: TestDetailsRes, content: Content) {

        testDetail = if (isBundleData) {
            TestDetail(
                "", bannerData.duration,
                bannerData.testList[0].questions, bannerData.testList[0].total_marks.toString(),
                Utils.getRound(model.topScore), Utils.getRound(model.avgScore),
                Utils.getRound(model.leastScore), Utils.getRound(model.tqs)
            )
        } else {
            TestDetail(
                content.title, Utils.convertIntoMinsTest(content.duration),
                content.questions, content.total_marks.toString(), Utils.getRound(model.topScore),
                Utils.getRound(model.avgScore), Utils.getRound(model.leastScore),
                Utils.getRound(model.tqs)
            )
        }
    }

    private fun listenForDataPrefsUpdate() {

        listener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
            when (key) {
                AppConstants.ABOUT_THE_TEST + "_" + data.id -> {
                    toggleOptionsVisibility(binding.aboutTheTest.tag.toString())
                }
                AppConstants.RECOMMENDED_LEARNING + "_" + data.id -> {
                    toggleOptionsVisibility(binding.recommendedLearning.tag.toString())
                }
                AppConstants.RECOMMENDED_PRACTICE + "_" + data.id -> {
                    toggleOptionsVisibility(binding.recommendedPractice.tag.toString())
                }
                AppConstants.MORE_TESTS + "_" + data.id -> {
                    toggleOptionsVisibility(binding.moreTests.tag.toString())
                }
            }
        }
        pref.preferences?.registerOnSharedPreferenceChangeListener(listener)
    }

    private fun toggleOptionsVisibility(tag: String) {
        when (tag) {
            getString(R.string.about_the_test) -> {
                setVisibility(binding.aboutTheTest, View.VISIBLE)
                if (::bannerData.isInitialized) {
                    binding.aboutTheTest.visibility = View.GONE
                    binding.view.visibility = View.GONE
                }
            }
            getString(R.string.recommended_learning) -> {
                recommendedLearningForTest = DataManager.instance.getRecommendedLearningForTest()
                if (recommendedLearningForTest.isNotEmpty()) {
                    setVisibility(binding.recommendedLearning, View.VISIBLE)
                } else {
                    setVisibility(binding.recommendedLearning, View.GONE)
                }
            }
            getString(R.string.recommended_practice) -> {
                recommendedPracticeForTest = DataManager.instance.getRecommendedPracticeForTest()
                if (recommendedPracticeForTest.isNotEmpty()) {
                    setVisibility(binding.recommendedPractice, View.VISIBLE)
                } else {
                    setVisibility(binding.recommendedPractice, View.GONE)
                }
            }
            getString(R.string.more_tests) -> {
                moreTests = DataManager.instance.getMoreTests()
                if (moreTests.isNotEmpty()) {
                    setVisibility(binding.moreTests, View.VISIBLE)
                } else {
                    setVisibility(binding.moreTests, View.GONE)
                }
            }
        }
//        showHideLine()
    }

    private fun setClickListeners() {
        binding.btnTest.setOnClickListener {
            SegmentUtils.trackTestSummaryScreenMainButtonClick("Start Test")
            callTestActivity(data)
        }

        binding.btnBookmark.setOnClickListener {
            Utils.isBookmarkUpdatedForTEST = true
            isBookMarked = !isBookMarked
            updateBookMarkUI(isBookMarked)
            SegmentUtils.trackEventMoreInfoBookmarkClick(isBookMarked, content = data)
            SegmentUtils.trackTestSummaryScreenBookmarkButtonClick(isBookMarked)
            timerBookmark.cancel()
            timerBookmark = Timer()
            timerBookmark.schedule(object : TimerTask() {
                override fun run() {
                    updateBookmarkForData()
                }
            }, interval)
        }

        binding.btnLike.setOnClickListener {
            Utils.isLikedForTest = true
            isLiked = !isLiked
            updateLikUI(isLiked)
            SegmentUtils.trackEventMoreInfoLikeClick(isLiked, content = data)
            SegmentUtils.trackTestSummaryScreenLikeButtonClick(isLiked)
            /*like api call*/
            timerLike.cancel()
            timerLike = Timer()
            timerLike.schedule(object : TimerTask() {
                override fun run() {
                    updateLikeForData()
                }
            }, interval)
        }

    }

    private fun setKeyListeners() {

        binding.aboutTheTest.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        SegmentUtils.trackTestSummaryScreenAvailableOptionClick(
                            AppConstants.ABOUT_THE_TEST,
                            1,
                            ""
                        )
                        when {
                            testDetail != null -> {
                                aboutTestFragment = AboutTestFragment()
                                val bundle = Bundle()
                                bundle.putString("type", getString(R.string.about_the_test))
                                bundle.putSerializable(AppConstants.TEST_DETAIL, testDetail)
                                showDetails(bundle, aboutTestFragment)
                                setStrokOnSelectedMenuOption(AppConstants.ABOUT_THE_TEST)
                            }
                        }
                    }
                }
            }

            false
        }

        binding.recommendedLearning.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                    }

                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        SegmentUtils.trackTestSummaryScreenAvailableOptionClick(
                            AppConstants.RECOMMENDED_LEARNING,
                            2,
                            ""
                        )
                        val testSummaryFragment = TestSummaryFragment()
                        val bundle = Bundle()
                        bundle.putString("type", getString(R.string.recommended_learning))
                        bundle.putParcelableArrayList(
                            AppConstants.RECOMMENDED_LEARNING,
                            recommendedLearningForTest[0].content as ArrayList<Content>
                        )
                        showDetails(bundle, testSummaryFragment)
                        setStrokOnSelectedMenuOption(AppConstants.RECOMMENDED_LEARNING)
                    }
                }
            }

            false
        }

        binding.recommendedPractice.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                    }

                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        SegmentUtils.trackTestSummaryScreenAvailableOptionClick(
                            AppConstants.RECOMMENDED_PRACTICE,
                            3,
                            ""
                        )
                        val testSummaryFragment = TestSummaryFragment()
                        val bundle = Bundle()
                        bundle.putString("type", getString(R.string.recommended_practice))
                        bundle.putParcelableArrayList(
                            AppConstants.RECOMMENDED_PRACTICE, recommendedPracticeForTest
                        )
                        showDetails(bundle, testSummaryFragment)
                        setStrokOnSelectedMenuOption(AppConstants.RECOMMENDED_PRACTICE)
                    }
                }
            }

            false
        }

        binding.moreTests.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                    }

                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        SegmentUtils.trackTestSummaryScreenAvailableOptionClick(
                            AppConstants.MORE_TESTS,
                            4,
                            ""
                        )
                        val testSummaryFragment = TestSummaryFragment()
                        val bundle = Bundle()
                        bundle.putString("type", getString(R.string.more_tests))
                        bundle.putParcelableArrayList(AppConstants.MORE_TESTS, moreTests)
                        showDetails(bundle, testSummaryFragment)
                        //binding.textSearch.requestFocus()
                        setStrokOnSelectedMenuOption(AppConstants.MORE_TESTS)
                    }
                }
            }

            false
        }
    }

    private fun setStrokOnSelectedMenuOption(optionType: String) {
        when (optionType) {
            AppConstants.ABOUT_THE_TEST -> {
                binding.aboutTheTest.setBackgroundResource(R.drawable.summary_screen_menu_selected)
                binding.recommendedLearning.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.recommendedPractice.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.moreTests.setBackgroundResource(R.drawable.summary_menu_selector)

            }
            AppConstants.RECOMMENDED_LEARNING -> {
                binding.recommendedLearning.setBackgroundResource(R.drawable.summary_screen_menu_selected)
                binding.aboutTheTest.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.recommendedPractice.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.moreTests.setBackgroundResource(R.drawable.summary_menu_selector)

            }
            AppConstants.RECOMMENDED_PRACTICE -> {
                binding.recommendedPractice.setBackgroundResource(R.drawable.summary_screen_menu_selected)
                binding.recommendedLearning.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.aboutTheTest.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.moreTests.setBackgroundResource(R.drawable.summary_menu_selector)

            }
            AppConstants.MORE_TESTS -> {
                binding.moreTests.setBackgroundResource(R.drawable.summary_screen_menu_selected)
                binding.recommendedLearning.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.recommendedPractice.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.aboutTheTest.setBackgroundResource(R.drawable.summary_menu_selector)

            }

        }
    }

    private fun setAllOptionUnselected() {
        binding.aboutTheTest.setBackgroundResource(R.drawable.summary_menu_selector)
        binding.recommendedLearning.setBackgroundResource(R.drawable.summary_menu_selector)
        binding.recommendedPractice.setBackgroundResource(R.drawable.summary_menu_selector)
        binding.moreTests.setBackgroundResource(R.drawable.summary_menu_selector)

    }

    private fun setFocusListeners() {
        binding.btnTest.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackTestSummaryScreenMainButtonFocus("Start Test")
                showCLContainer()
                setAllOptionUnselected()
            }
        }
        binding.btnBookmark.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                showCLContainer()
                SegmentUtils.trackEventMoreInfoBookmarkFocus(isBookMarked, content = data)
                SegmentUtils.trackTestSummaryScreenBookmarkButtonFocus()
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_black, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                    )
                }
            } else {
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_white, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                    )
                }
            }
        }

        binding.btnLike.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                showCLContainer()
                SegmentUtils.trackEventMoreInfoLikeFocus(isLiked, content = data)
                SegmentUtils.trackTestSummaryScreenLikeButtonFocus()
                if (isLiked) {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_filled_black, 0, 0, 0
                    )
                } else {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_stroke_black, 0, 0, 0
                    )
                }
            } else {
                if (isLiked) {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_filled_white, 0, 0, 0
                    )
                } else {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_stroke_white, 0, 0, 0
                    )
                }
            }
        }

        binding.aboutTheTest.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestSummaryScreenAvailableOptionFocus(
                AppConstants.ABOUT_THE_TEST,
                1,
                ""
            )
        }
        binding.recommendedLearning.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestSummaryScreenAvailableOptionFocus(
                AppConstants.RECOMMENDED_LEARNING,
                2,
                ""
            )
        }
        binding.recommendedPractice.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestSummaryScreenAvailableOptionFocus(
                AppConstants.RECOMMENDED_PRACTICE,
                3,
                ""
            )
        }
        binding.moreTests.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestSummaryScreenAvailableOptionFocus(
                AppConstants.MORE_TESTS,
                4,
                ""
            )
        }

    }

    private fun showCLContainer() {
        binding.landingCL.visibility = View.VISIBLE
        binding.fragmentContainerView.visibility = View.GONE
    }

    private fun showDetails(bundle: Bundle, fragment: Fragment) {
        binding.landingCL.visibility = View.GONE
        binding.fragmentContainerView.visibility = View.VISIBLE
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainerView, fragment).commit()
    }

    private fun hideButtons() {
//        binding.aboutTheTest.visibility = View.GONE
        binding.recommendedLearning.visibility = View.GONE
        binding.recommendedPractice.visibility = View.GONE
        binding.moreTests.visibility = View.GONE
//        binding.view.visibility = View.GONE
    }

    private fun callTestActivity(content: Content) {
        val testIntent = Intent(this, TestActivity::class.java)
        testIntent.putExtra(AppConstants.TEST_CODE, content.bundle_id)
        testIntent.putExtra(AppConstants.IS_DIAGNOSTIC, false)
        testIntent.putExtra(AppConstants.TEST_NAME, content.title)
        testIntent.putExtra(AppConstants.TEST_XPATH, content.xpath)
        // Utils.insertBundle(content,testIntent)
        DataManager.instance.content = content
        startActivity(testIntent)
    }

    private fun getBookmarkStatus(content: Content, type: String) {

        detailsViewModel.getBookmarkStatus(content.bundle_id,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                val data = dataList.filter { bookmarkModel ->
                                    bookmarkModel.contentId == content.bundle_id
                                }

                                bookmarkActionAsPerType(type, data)
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        } else {
                            isBookMarked = false
                            if (type == AppConstants.MULTI_TYPE_TEST) {
                                bannerData.testList[0].isBookmarked = false
                            }
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg ")

                }
            })
    }

    private fun getLikeStatus(content: Content, type: String) {

        detailsViewModel.getLikeStatus(content.bundle_id,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                val data = dataList.filter { likeModel ->
                                    likeModel.contentId == "0"
                                }

                                likeActionAsPerType(type, data)
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        } else {
                            isLiked = false
                            if (type == AppConstants.MULTI_TYPE_TEST) {
                                bannerData.testList[0].isLiked = false
                            }
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg ")
                }
            })
    }

    private fun bookmarkActionAsPerType(
        type: String,
        listBookmarkDetail: List<LikeBookmarkDetailModel>
    ) {
        when (type) {
            AppConstants.MULTI_TYPE_TEST -> {
                isBookMarked = listBookmarkDetail[0].status
                bannerData.testList[0].isBookmarked = listBookmarkDetail[0].status
            }
            AppConstants.SINGLE_TYPE_TEST -> {
                updateBookMarkUI(listBookmarkDetail[0].status)
            }
        }
    }

    private fun likeActionAsPerType(
        type: String,
        listBookmarkDetail: List<LikeBookmarkDetailModel>
    ) {
        when (type) {
            AppConstants.MULTI_TYPE_TEST -> {
                isLiked = listBookmarkDetail[0].status
                bannerData.testList[0].isLiked = listBookmarkDetail[0].status
            }
            AppConstants.SINGLE_TYPE_TEST -> {
                updateLikUI(listBookmarkDetail[0].status)
            }
        }
    }

    private fun updateBookMarkUI(bookmarked: Boolean) {
        isBookMarked = bookmarked
        data.isBookmarked = bookmarked

        if (binding.btnBookmark.hasFocus()) {
            if (isBookMarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        } else {
            if (isBookMarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        }
    }

    private fun updateLikUI(liked: Boolean) {
        isLiked = liked
        data.isLiked = liked

        if (binding.btnLike.hasFocus()) {
            if (isLiked) {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_filled_black, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.liked)
            } else {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_stroke_black, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.like)
            }
        } else {
            if (isLiked) {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_filled_white, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.liked)
            } else {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_stroke_white, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.like)
            }
        }
    }

    private fun updateBookmarkForData() {
        detailsViewModel.bookmark(
            data.bundle_id, data.type, isBookMarked,
            object : BaseViewModel.APICallBacks<CommonApiResponse> {
                override fun onSuccess(model: CommonApiResponse?) {
                    if (model != null && model.success) {
                        Utils.isBookmarkUpdatedForTEST = true
                        updateBookMarkUI(isBookMarked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (bookmarkRetryCount < retryAttempt) {
                        bookmarkRetryCount++
                        updateBookmarkForData()
                        return
                    } else {
                        /*reset retry count*/
                        bookmarkRetryCount = 0
                        /*revert back to previous state of ui */
//                        updateBookMarkUI(!isBookMarked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_BOOKMARK)
                    }
                }
            })
    }

    private fun updateLikeForData() {
        detailsViewModel.like(
            data.id, data.type, isLiked,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!!) {
                        Utils.isLikedForTest = true
                        updateLikUI(isLiked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (likeRetryCount < retryAttempt) {
                        likeRetryCount++
                        updateLikeForData()
                        return
                    } else {
                        /*reset retry count*/
                        likeRetryCount = 0
                        /*revert back to previous state of ui */
                        updateLikUI(!isLiked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_LIKE)
                    }
                }
            })
    }

    private fun setVisibility(view: Button, visibility: Int) {
        view.visibility = visibility
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::listener.isInitialized) {
            pref.preferences?.unregisterOnSharedPreferenceChangeListener(listener)
        }
    }


    private fun setBackground() {
        if (data.bg_thumb.isEmpty())
            Utils.setTestBlurBackgroundImage(
                Utils.getBackgroundUrl(data),
                binding.imgDescBackground
            )
        else
            Utils.setTestBlurBackgroundImage(data.bg_thumb, binding.imgDescBackground)
    }

    private fun initViewAttemptQuality(jarType: String) {
        val requestOptions = RequestOptions().transform(RoundedCorners(10))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        //binding.cardAttemptScore.tvTopic.text = "0% " + jarType
        when (jarType) {
            AppConstants.CORRECT -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_too_fast_correct)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.PERFECT -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_perfect_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.OVERTIME_CORRECT -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_overtime_correct)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.WASTED -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_wasted_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }

            AppConstants.WASTED_IN_FOUR -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_wasted_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.WRONG -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_incorrect_answer)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.OVERTIME_WRONG -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_overtime_incorrect)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.CONCEPTUAL_THINKING -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_conceptual_thinking)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }

            AppConstants.RECALL -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_recall)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }

            AppConstants.ANALYTICAL_THINKING -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_analytical_thinking)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
        }

        binding.cardAttemptScore.layout.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.cardAttemptScore.lvJar.visibility = View.VISIBLE
                binding.cardAttemptScore.ivPlay.visibility = View.INVISIBLE
                binding.cardAttemptScore.ivImg.visibility = View.INVISIBLE
                when (jarType) {
                    AppConstants.CORRECT -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_correct)
                        SegmentUtils.trackTestSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.PERFECT -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_perfect_attempts)
                        SegmentUtils.trackTestSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.OVERTIME_CORRECT -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_overtime_correct)
                        SegmentUtils.trackTestSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.WASTED -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_wasted_attempt)
                        SegmentUtils.trackTestSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.WRONG -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_wrong)
                        SegmentUtils.trackTestSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.WASTED_IN_FOUR -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_wasted_attempt)
                        SegmentUtils.trackTestSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.OVERTIME_WRONG -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_overtime_incorrect)
                        SegmentUtils.trackTestSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.RECALL -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.jar_recall)
                        SegmentUtils.trackTestSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.ANALYTICAL_THINKING -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.jar_analytical_tinking)
                        SegmentUtils.trackTestSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.CONCEPTUAL_THINKING -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.jar_conceptual_thinking)
                        SegmentUtils.trackTestSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                }
                binding.cardAttemptScore.lvJar.repeatCount = LottieDrawable.INFINITE
                binding.cardAttemptScore.lvJar.playAnimation()
            } else {
                binding.cardAttemptScore.lvJar.visibility = View.INVISIBLE
                binding.cardAttemptScore.ivPlay.visibility = View.VISIBLE
                binding.cardAttemptScore.ivImg.visibility = View.VISIBLE
            }
        }
    }

    private fun setCardAttemptScoreClickListener(jarType: String) {
        binding.cardAttemptScore.layout.setOnClickListener {
            SegmentUtils.trackTestSummaryScreenAttemptBucketDescriptionClick(jarType)
        }
    }

    private fun setJarFocusListener() {
        binding.cardAttemptQuality.jarCorrectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.CORRECT)
                SegmentUtils.trackTestSummaryScreenAttemptBucketFocus(AppConstants.CORRECT, 1)
            }
        }
        binding.cardAttemptQuality.jarPerfectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.PERFECT)
                SegmentUtils.trackTestSummaryScreenAttemptBucketFocus(AppConstants.PERFECT, 2)
            }
        }
        binding.cardAttemptQuality.jarOvertimeCorrectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.OVERTIME_CORRECT)
                SegmentUtils.trackTestSummaryScreenAttemptBucketFocus(
                    AppConstants.OVERTIME_CORRECT,
                    3
                )
            }
        }
        binding.cardAttemptQuality.jarWastedAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.WASTED)
                SegmentUtils.trackTestSummaryScreenAttemptBucketFocus(AppConstants.WASTED, 4)
            }
        }
        binding.cardAttemptQuality.jarWrongAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.WRONG)
                SegmentUtils.trackTestSummaryScreenAttemptBucketFocus(AppConstants.WRONG, 5)
            }
        }
        binding.cardAttemptQuality.jarOvertimeWrongAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.OVERTIME_WRONG)
                SegmentUtils.trackTestSummaryScreenAttemptBucketFocus(
                    AppConstants.OVERTIME_WRONG,
                    6
                )
            }
        }
        binding.cardAttemptQuality.jarRecall.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.RECALL)
                SegmentUtils.trackTestSummaryScreenAttemptBucketFocus(AppConstants.RECALL, 7)
            }
        }
        binding.cardAttemptQuality.jarAnalyticalThinking.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.ANALYTICAL_THINKING)
                SegmentUtils.trackTestSummaryScreenAttemptBucketFocus(
                    AppConstants.ANALYTICAL_THINKING,
                    8
                )
            }
        }
        binding.cardAttemptQuality.jarConceptualThinking.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.CONCEPTUAL_THINKING)
                SegmentUtils.trackTestSummaryScreenAttemptBucketFocus(
                    AppConstants.CONCEPTUAL_THINKING,
                    9
                )
            }
        }
        binding.cardAttemptQuality.jarWastedAttemptNew.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.WASTED_IN_FOUR)
                SegmentUtils.trackTestSummaryScreenAttemptBucketFocus(
                    AppConstants.WASTED_IN_FOUR,
                    10
                )
            }
        }
    }

    private fun setJarClickListener(jarType: String) {
        when (jarType) {
            AppConstants.CORRECT -> binding.cardAttemptQuality.jarCorrectAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackTestSummaryScreenAttemptBucketClick(jarType, 1)
            }
            AppConstants.PERFECT -> binding.cardAttemptQuality.jarPerfectAttempt.setOnClickListener {
                animateJar(jarType)
                SegmentUtils.trackTestSummaryScreenAttemptBucketClick(jarType, 2)
                // setAttemptQualityView(jarType)
            }
            AppConstants.OVERTIME_CORRECT -> binding.cardAttemptQuality.jarOvertimeCorrectAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackTestSummaryScreenAttemptBucketClick(jarType, 3)
            }
            AppConstants.WASTED -> binding.cardAttemptQuality.jarWastedAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackTestSummaryScreenAttemptBucketClick(jarType, 4)
            }
            AppConstants.WRONG -> binding.cardAttemptQuality.jarWrongAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackTestSummaryScreenAttemptBucketClick(jarType, 5)
            }
            AppConstants.OVERTIME_WRONG -> binding.cardAttemptQuality.jarOvertimeWrongAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackTestSummaryScreenAttemptBucketClick(jarType, 6)
            }
            AppConstants.RECALL -> binding.cardAttemptQuality.jarRecall.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackTestSummaryScreenAttemptBucketClick(jarType, 7)
            }
            AppConstants.ANALYTICAL_THINKING -> binding.cardAttemptQuality.jarAnalyticalThinking.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackTestSummaryScreenAttemptBucketClick(jarType, 8)
            }
            AppConstants.CONCEPTUAL_THINKING -> binding.cardAttemptQuality.jarConceptualThinking.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackTestSummaryScreenAttemptBucketClick(jarType, 9)
            }
            AppConstants.WASTED_IN_FOUR -> binding.cardAttemptQuality.jarWastedAttemptNew.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackTestSummaryScreenAttemptBucketClick(jarType, 10)
            }
        }

    }

    private fun animateJar(jarType: String) {
        updateAttemptQualityData(jarType)
        when (jarType) {
            AppConstants.CORRECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                10f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(1)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.CONCEPTUAL_THINKING -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -10f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(7)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.PERFECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -40f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(2)

                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500
                    }

                    override fun onAnimationCancel(animation: Animator) {}
                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.RECALL -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -80f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(8)

                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500
                    }

                    override fun onAnimationCancel(animation: Animator) {}
                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.OVERTIME_CORRECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -120f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(3)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.ANALYTICAL_THINKING -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -160f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(9)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.WASTED_IN_FOUR -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -360f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(10)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.WASTED -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -180f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(4)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.WRONG -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -260f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(5)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.OVERTIME_WRONG -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -320f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(6)

                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500
                    }

                    override fun onAnimationCancel(animation: Animator) {}
                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
        }
    }


    private fun setAttemptQualityView(jarType: String) {
        this.jarType = jarType
        initViewAttemptQuality(jarType)
        //binding.cardFour.layout.visibility = View.VISIBLE
        binding.cardAttemptScore.layout.visibility = View.VISIBLE
        binding.cardAttemptScore.layout.postDelayed({
            binding.cardAttemptScore.layout.requestFocus()
            binding.cardAttemptQuality.layout.visibility = View.GONE
        }, 10)

    }

    private fun viewAnimationImage(position: Int) {
        setAttemptQualityLayoutVisibility(false)
        isJarEnabled = true
        binding.imgAttemptJarIcon.alpha = 1.0F
        binding.imgAttemptJarIcon.visibility = View.VISIBLE

        when (position) {
            1 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_too_fast_correct)
            }
            2 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_perfect_attempt)

            }
            3 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_correct)

            }
            4 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_wasted_attempt)

            }
            5 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_incorrect_answer)

            }
            6 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_incorrect)
            }
            8 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_recall)
            }
            7 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_conceptual_thinking)
            }
            9 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_analytical_thinking)
            }
            10 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_wasted_attempt)
            }

        }

        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        if (position < 5)
            lp.setMargins((position - 1) * 60, 20, 0, 0)
        if (position == 5)
            lp.setMargins((position - 1) * 70, 20, 0, 0)
        if (position == 6)
            lp.setMargins((position - 1) * 80, 20, 0, 0)

        binding.imgAttemptJarIcon.layoutParams = lp
    }

    private fun getAttemptQualityData() {
        detailsViewModel.getUserAttemptQualityData(
            "dvdvdvdfvdfvd",
            "test",
            data.bundle_id,
            object : BaseViewModel.APICallBacks<AttemptQualityResponse> {
                override fun onSuccess(model: AttemptQualityResponse?) {
                    if (model != null) {
                        attemptQualityRes = model
                        setJarVisibility()
                    }
                    makeLog("Data")
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("Attempt Failed" + code + error + msg)
                }
            })
    }

    private fun setJarVisibility() {
        if (::attemptQualityRes.isInitialized) {
            if (attemptQualityRes.data.conceptual_thinking != null) {
                binding.cardAttemptQuality.layoutFourJars.visibility = View.VISIBLE
                binding.cardAttemptQuality.layoutSixJars.visibility = View.GONE
            } else {
                binding.cardAttemptQuality.layoutFourJars.visibility = View.GONE
                binding.cardAttemptQuality.layoutSixJars.visibility = View.VISIBLE
            }
        }
    }

    private fun updateAttemptQualityData(jarType: String) {
        var jarName = jarType
        when (jarName) {
            AppConstants.RECALL -> {
                jarName = "Recall"
            }
            AppConstants.ANALYTICAL_THINKING -> {
                jarName = "Analytical Thinking"
            }
            AppConstants.CONCEPTUAL_THINKING -> {
                jarName = "Conceptual Thinking"
            }
            AppConstants.WRONG -> {
                jarName = "Wrong"
            }
            AppConstants.WASTED_IN_FOUR -> {
                jarName = "Wasted"
            }
        }
        if (::attemptQualityRes.isInitialized) {
            val percentage: Int = when (jarType) {
                AppConstants.CORRECT -> getAttemptPercentage(attemptQualityRes.data.overtime_correct!!.value) + getAttemptPercentage(
                    attemptQualityRes.data.perfect_attempt!!.value
                )
                AppConstants.PERFECT -> getAttemptPercentage(attemptQualityRes.data.perfect_attempt!!.value)
                AppConstants.OVERTIME_CORRECT -> getAttemptPercentage(attemptQualityRes.data.overtime_correct!!.value)
                AppConstants.WASTED -> getAttemptPercentage(attemptQualityRes.data.wasted_attempt!!.value)
                AppConstants.WRONG -> getAttemptPercentage(attemptQualityRes.data.wasted_attempt!!.value) + getAttemptPercentage(
                    attemptQualityRes.data.wasted_attempt!!.value
                )
                AppConstants.OVERTIME_WRONG -> getAttemptPercentage(attemptQualityRes.data.overtime_incorrect!!.value)
                AppConstants.RECALL -> getAttemptPercentage(attemptQualityRes.data.recall!!.value)
                AppConstants.ANALYTICAL_THINKING -> getAttemptPercentage(attemptQualityRes.data.analytical_thinking!!.value)
                AppConstants.CONCEPTUAL_THINKING -> getAttemptPercentage(attemptQualityRes.data.conceptual_thinking!!.value)
                AppConstants.WASTED_IN_FOUR -> getAttemptPercentage(attemptQualityRes.data.wasted_attempt!!.value)
                else -> 0
            }
            binding.cardAttemptScore.tvTopic.text = "$percentage% ${jarName}"
        } else
            binding.cardAttemptScore.tvTopic.text = "0% $jarName"
    }

    private fun getAttemptPercentage(value: String): Int {
        Log.i("attempt data", value)
        if (value.isNotEmpty() || value.isNotBlank())
            return value.toDouble().toInt()
        return 0
    }

    /*getTestReadiness*/
    private fun getTestReadiness() {

        testViewModel.getTestReadiness(bundleId,
            object : BaseViewModel.APICallBacks<TestReadiness> {
                override fun onSuccess(model: TestReadiness?) {
                    if (model != null) {
                        updateReadiness(model)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("Error in getConceptSequenceCoverage : $code $msg")
                }

            })
    }

    private fun updateReadiness(model: TestReadiness) {
        val value = round(model.readiness ?: 0.0)
        binding.btnCheckProgress.visibility = View.VISIBLE
        binding.btnCheckProgress.text = "Test Readiness: $value%"
    }

    private fun round(d: Double): Int {
        val dAbs = Math.abs(d)
        val i = dAbs.toInt()
        val result = dAbs - i.toDouble()
        return if (result < 0.5) {
            if (d < 0) -i else i
        } else {
            if (d < 0) -(i + 1) else i + 1
        }
    }

    override fun onBackPressed() {
        /*if (binding.cardAttemptQuality.layout.isVisible)
            super.onBackPressed()
        else {
            binding.cardAttemptQuality.layout.visibility = View.VISIBLE
            binding.cardFour.layout.visibility = View.GONE
        }*/


        if (isJarEnabled) {
            restoreJarFocus()
            setAttemptQualityLayoutVisibility(true)
            binding.cardAttemptQuality.layout.visibility = View.VISIBLE
            binding.cardAttemptScore.layout.visibility = View.GONE
            isJarEnabled = false
            this.jarType = ""
        } else {
            super.onBackPressed()
        }

    }

    private fun initViewSincerity() {
        val requestOptions = RequestOptions().transform(RoundedCorners(5))
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        Glide.with(App.context)
            .applyDefaultRequestOptions(requestOptions)
            .load(thumb1)
            .placeholder(R.drawable.video_placeholder)
            .into(binding.cardSincerityScore.ivImg)

        binding.cardSincerityScore.layout.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        SegmentUtils.trackTestSummaryScreenSincerityScoreClick("IDK")
                    }
                }
            }

            false
        }

        binding.cardSincerityScore.layout.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackTestSummaryScreenSincerityScoreFocus("IDK")
                if (isFisrt) {
                    showSincerityScoreFlipAnimation()
                } else {
                    binding.cardSincerityScore.cardView.visibility = View.VISIBLE
                    binding.cardSincerityScore.ivGifView.visibility = View.VISIBLE
                    val controller = Fresco.newDraweeControllerBuilder()
                    controller.autoPlayAnimations = true
                    controller.setUri(preview_url)
                    controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                        override fun onFinalImageSet(
                            id: String?,
                            imageInfo: ImageInfo?,
                            animatable: Animatable?
                        ) {
                            val anim = animatable as AnimatedDrawable2
                            anim.setAnimationListener(object : AnimationListener {
                                override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                                override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                                    binding.cardSincerityScore.ivImg.visibility = View.INVISIBLE
                                }

                                override fun onAnimationFrame(
                                    drawable: AnimatedDrawable2?,
                                    frameNumber: Int
                                ) {

                                }

                                override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                                    binding.cardSincerityScore.ivImg.visibility = View.VISIBLE
                                    binding.cardSincerityScore.ivGifView.visibility = View.INVISIBLE
                                    binding.cardSincerityScore.cardView.visibility = View.INVISIBLE
                                    binding.cardSincerityScore.ivPlay.visibility = View.VISIBLE
                                }

                                override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                            })
                        }
                    }
                    binding.cardSincerityScore.ivGifView.controller = controller.build()
                }
            } else {
                binding.cardSincerityScore.ivImg.visibility = View.VISIBLE
                binding.cardSincerityScore.ivGifView.visibility = View.INVISIBLE
                binding.cardSincerityScore.cardView.visibility = View.INVISIBLE
                binding.cardSincerityScore.ivPlay.visibility = View.VISIBLE
            }
        }
    }

    private fun showSincerityScoreFlipAnimation() {
        isFisrt = false
        var resourceLoader = ResourceStreamLoader(App.context, R.drawable.flip_anim_apng)
        var apngDrawable = APNGDrawable(resourceLoader)
        binding.cardSincerityScore.ivImg.setImageDrawable(apngDrawable)

        val requestOptions = RequestOptions().transform(RoundedCorners(5))
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        Handler().postDelayed({
            Glide.with(App.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(gif_preview)
                .placeholder(R.drawable.video_placeholder)
                .into(binding.cardSincerityScore.ivImg)

            binding.cardSincerityScore.tvTopic.text = "Marathoner"
        }, 2000)
    }

    private fun restoreJarFocus() {
        when (jarType) {
            AppConstants.CORRECT -> binding.cardAttemptQuality.jarCorrectAttempt.requestFocus()
            AppConstants.PERFECT -> binding.cardAttemptQuality.jarPerfectAttempt.requestFocus()
            AppConstants.OVERTIME_CORRECT -> binding.cardAttemptQuality.jarOvertimeCorrectAttempt.requestFocus()
            AppConstants.WASTED -> binding.cardAttemptQuality.jarWastedAttempt.requestFocus()
            AppConstants.WRONG -> binding.cardAttemptQuality.jarWrongAttempt.requestFocus()
            AppConstants.OVERTIME_WRONG -> binding.cardAttemptQuality.jarOvertimeWrongAttempt.requestFocus()
            AppConstants.RECALL -> binding.cardAttemptQuality.jarRecall.requestFocus()
            AppConstants.ANALYTICAL_THINKING -> binding.cardAttemptQuality.jarAnalyticalThinking.requestFocus()
            AppConstants.CONCEPTUAL_THINKING -> binding.cardAttemptQuality.jarConceptualThinking.requestFocus()
            AppConstants.WASTED_IN_FOUR -> binding.cardAttemptQuality.jarWastedAttemptNew.requestFocus()
        }
    }

    private fun setAttemptQualityLayoutVisibility(visibility: Boolean) {
        if (visibility) {
            binding.cardAttemptQuality.title.visibility = View.VISIBLE
            binding.cardAttemptQuality.jarCorrectAttempt.visibility = View.VISIBLE
            binding.cardAttemptQuality.jarOvertimeCorrectAttempt.visibility = View.VISIBLE
            binding.cardAttemptQuality.jarPerfectAttempt.visibility = View.VISIBLE
            binding.cardAttemptQuality.jarWrongAttempt.visibility = View.VISIBLE
            binding.cardAttemptQuality.jarWastedAttempt.visibility = View.VISIBLE
            binding.cardAttemptQuality.jarOvertimeWrongAttempt.visibility = View.VISIBLE
        } else {
            binding.cardAttemptQuality.title.visibility = View.GONE
            binding.cardAttemptQuality.jarCorrectAttempt.visibility = View.GONE
            binding.cardAttemptQuality.jarOvertimeCorrectAttempt.visibility = View.GONE
            binding.cardAttemptQuality.jarPerfectAttempt.visibility = View.GONE
            binding.cardAttemptQuality.jarWrongAttempt.visibility = View.GONE
            binding.cardAttemptQuality.jarWastedAttempt.visibility = View.GONE
            binding.cardAttemptQuality.jarOvertimeWrongAttempt.visibility = View.GONE
        }
    }

    companion object {
        private const val TAG = "TestSummaryActivity"
    }


    private fun setSearchTextChangeListener() {

        binding.textSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (!s.isNullOrEmpty()) {
                    getSuggestionsAndHitSearch(s.toString())
                } else {
                    displayData()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })


    }

    private fun getSuggestionsAndHitSearch(searchQuery: String) {
        SegmentUtils.trackEventSearch(searchQuery)
        if (::data.isInitialized) {
            testViewModel.searchResults(searchQuery, data,
                object : BaseViewModel.APICallBacks<List<Content>> {
                    override fun onSuccess(models: List<Content>?) {

                        if (models!!.isNotEmpty()) {
                            binding.textNoResult.visibility = View.GONE
                            binding.fragmentContainerView.visibility = View.VISIBLE
                            /* setFragment(
                                 models,
                                 practiceDetailMenuType!!
                             )*/
                        } else {
                            noSearchResults()
                        }
                    }

                    override fun onFailed(code: Int, error: String, msg: String) {
                        makeLog("$code $error $msg")
                    }
                })
        }
    }

    private fun displayData() {
        binding.textNoResult.visibility = View.GONE
        binding.fragmentContainerView.visibility = View.VISIBLE

        /*if (practiceDetailMenuType == AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE!!) {
            if (::topicsData.isInitialized) {
                setPracticeFragment(
                    topicsData,
                    practiceDetailMenuType!!
                )
            }
        }*/

    }

    private fun noSearchResults() {
        /*show the results not found UI here*/
        binding.textNoResult.visibility = View.VISIBLE
        binding.fragmentProductDetailView.visibility = View.GONE
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {

        if (event.keyCode == KeyEvent.KEYCODE_SEARCH) {
            startActivity(Intent(this, SearchActivity::class.java))
            return true
        }
        val current = System.currentTimeMillis()
        var res = false
        if (current - mLastKeyDownTime < 300) {
            res = true
        } else {
            res = super.onKeyDown(keyCode, event)
            mLastKeyDownTime = current
        }
        return res
    }
}