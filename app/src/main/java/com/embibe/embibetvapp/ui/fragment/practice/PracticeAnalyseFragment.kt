package com.embibe.embibetvapp.ui.fragment.practice

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentPracticeAnalyseBinding
import com.embibe.embibetvapp.model.DEAnalysis.DEAnalysisModel
import java.math.BigDecimal
import java.math.RoundingMode
import kotlin.math.floor
import kotlin.math.roundToInt

class PracticeAnalyseFragment : BaseAppFragment() {

    private var socialConceptsPercentage: Double = 0.0
    private var userConceptsPercentage: Double = 0.0
    private var conceptsCount: Int = 0
    private var title: String? = null
    private var model: DEAnalysisModel? = null
    private lateinit var binding: FragmentPracticeAnalyseBinding
    private val userRed by lazy { 0xfffa5c66.toInt() }
    private val socialYellow by lazy { 0xffffb155.toInt() }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_practice_analyse, container, false)
        model = arguments?.getParcelable(AppConstants.ANALYSIS_DATA)
        conceptsCount = arguments?.getInt("concept_count") ?: 0
        title = arguments?.getString(AppConstants.TITLE) ?: ""
        if (model?.accuracyModel != null) {
            /*accuracy*/
            binding.cardOne.title.text = getString(R.string.overall_accuracy)
            val userPercentage = getAccuracyInPercentage(model?.accuracyModel?.userAccuracy!!)
            val socialPercentage = getAccuracyInPercentage(model?.accuracyModel?.socialAccuracy!!)
            binding.cardOne.textPercentageOne.text = getAccuracy(userPercentage)
            binding.cardOne.textPercentageTwo.text = getAccuracy(socialPercentage)
            binding.cardOne.arcProgressLowest.percent =
                getLowestValue(userPercentage, socialPercentage)
            binding.cardOne.arcProgressHighest.percent =
                getHighestValue(userPercentage, socialPercentage)
            binding.cardOne.ivAccuracyAndConcept.setImageResource(R.drawable.overall_accuracy)

            if (userPercentage > socialPercentage) {
                binding.cardOne.arcProgressLowest.fgColorEnd = socialYellow
                binding.cardOne.arcProgressLowest.fgColorStart = socialYellow

                binding.cardOne.arcProgressHighest.fgColorEnd = userRed
                binding.cardOne.arcProgressHighest.fgColorStart = userRed
            }

            /*behaviour*/
            binding.cardTwo.textBehaviourCommand.text =
                setBehaviour(model?.behaviourMerterModel?.userBehaviour?.behaviourCommand.toString())
            binding.cardTwo.textSocialBehaviourCommand.text =
                setBehaviour(model?.behaviourMerterModel?.socialBehaviour?.behaviourCommand.toString())

            /*concept Coverage*/
            binding.cardThree.title.text = getString(R.string.total_concepts_studied)
            val userConcepts = model?.conceptCoverageModel?.userConceptCoverage ?: 0
            val socialConcepts = model?.conceptCoverageModel?.socialConceptCoverage ?: 0
            binding.cardThree.textPercentageOne.text = userConcepts.toString()
            binding.cardThree.textPercentageTwo.text = socialConcepts.toString()



            socialConceptsPercentage = if (conceptsCount > socialConcepts) {
                (socialConcepts.toDouble() / conceptsCount) * 100
            } else 100.0
            userConceptsPercentage = if (conceptsCount > userConcepts) {
                (userConcepts.toDouble() / conceptsCount) * 100
            } else 100.0

            binding.cardThree.arcProgressLowest.percent =
                getLowestValue(userConceptsPercentage.toInt(), socialConceptsPercentage.toInt())
            binding.cardThree.arcProgressHighest.percent =
                getHighestValue(userConceptsPercentage.toInt(), socialConceptsPercentage.toInt())
            binding.cardThree.ivAccuracyAndConcept.setImageResource(R.drawable.concept)
            if (userConceptsPercentage > socialConceptsPercentage) {
                binding.cardThree.arcProgressLowest.fgColorEnd = socialYellow
                binding.cardThree.arcProgressLowest.fgColorStart = socialYellow

                binding.cardThree.arcProgressHighest.fgColorEnd = userRed
                binding.cardThree.arcProgressHighest.fgColorStart = userRed
            }
        }


        return binding.root
    }


    private fun setBehaviour(userBehaviour: String): String {
        return userBehaviour.substring(0, 1).toUpperCase() + userBehaviour.substring(1)
            .toLowerCase()
    }

    private fun getHighestValue(first: Int, second: Int): Float {
        var ret = first
        ret = Math.max(ret, second)
        return ret.toFloat()
    }

    private fun getLowestValue(first: Int, second: Int): Float {
        var ret = first
        ret = Math.min(ret, second)
        return ret.toFloat()
    }

    private fun getAccuracy(percentage: Int): String? {
        return "$percentage${AppConstants.PERCENTAGE}"
    }

    private fun getAccuracyInPercentage(accuracy: Double): Int {
        return if (accuracy == 0.0) {
            0
        } else {
            val rounded = floor(accuracy + 0.5)
            val percentage = (rounded * 100) / 100
            percentage.roundToInt()
        }
    }

    fun Double.round(decimals: Int): Double {
        val decimal = BigDecimal(this).setScale(decimals, RoundingMode.HALF_EVEN)
        return decimal.toDouble()
    }
}
