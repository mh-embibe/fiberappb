package com.embibe.embibetvapp.ui.fragment.errorAlertScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentPracticeSessionAlertMsgBinding
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener

class PracticeSessionAlertMsg : DialogFragment() {

    private lateinit var binding: FragmentPracticeSessionAlertMsgBinding
    private var msg: String? = null
    private var errorScreensBtnClickListener: ErrorScreensBtnClickListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, android.R.style.ThemeOverlay)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_practice_session_alert_msg,
            container, false
        )
        if (!msg.isNullOrEmpty())
            binding.tvMsg2.text = msg
        setBtnContinueClickListener()
        setBtnEndClickListener()
        return binding.root
    }

    private fun setBtnEndClickListener() {
        binding.btnEndSession.setOnClickListener {
            errorScreensBtnClickListener?.screenUpdate(AppConstants.END_SESSION)
        }
    }

    private fun setBtnContinueClickListener() {
        binding.btnContinueSession.setOnClickListener {
            errorScreensBtnClickListener?.screenUpdate(AppConstants.CONTINUE_PRACTICE)
            dismiss()
        }
    }

    fun setMsg(msg: String) {
        this.msg = msg
    }

    fun setListener(errorListener: ErrorScreensBtnClickListener) {
        errorScreensBtnClickListener = errorListener
    }


}