package com.embibe.embibetvapp.ui.fragment.achieve

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.embibe.embibetvapp.BuildConfig
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.FragmentDiagnosticCrunchingVariableBinding
import com.embibe.embibetvapp.ui.activity.DiagnosticTestActivity
import com.embibe.embibetvapp.ui.activity.DiagnosticTransitionActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*


@Suppress("DEPRECATION", "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DiagnosticCrunchingVariableFragment(
    var context: DiagnosticTransitionActivity,
    var variables: String
) : Fragment() {
    private lateinit var binding: FragmentDiagnosticCrunchingVariableBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_diagnostic_crunching_variable,
                container,
                false
            )
        configureVideoView()
        processVariable()
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        configureVideoView()
    }

    private fun setText() {
//        val spannable = SpannableStringBuilder("Crunching\n$variables\nVariables...")
//        spannable.setSpan(
//            RelativeSizeSpan(1.5f),
//            10, 20,
//            Spannable.SPAN_EXCLUSIVE_INCLUSIVE
//        )
//        spannable.setSpan(
//            ForegroundColorSpan(resources.getColor(R.color.embiumsColor)),
//            10, 20,
//            Spannable.SPAN_EXCLUSIVE_INCLUSIVE
//        )
//        binding.tvCrunchVariable.text = spannable
        binding.tvCrunchVariable.visibility = View.VISIBLE
        binding.llText.visibility = View.VISIBLE
    }

    private fun configureVideoView() {
        val metrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(metrics)
        val uriPath =
            "android.resource://" + BuildConfig.APPLICATION_ID + "/" + R.raw.crunching_variable_video
        binding.crunchingVideo.setVideoPath(uriPath)
        binding.crunchingVideo.setOnInfoListener(object : MediaPlayer.OnInfoListener {
            override fun onInfo(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    setText()
                    binding.placeholder.visibility = View.GONE
                    return true
                }
                return false
            }
        })
        binding.crunchingVideo.start()
    }

    private fun processVariable() {
        val animator = ValueAnimator.ofInt(0, variables.replace(",", "").toInt())
        animator.duration = 7000
        val formatter = NumberFormat.getIntegerInstance(Locale("en", "in"))
        animator.addUpdateListener { animation ->
            binding.tvCrunchVariable.text =
                formatter.format(BigDecimal(animation.animatedValue.toString()))

        }
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                loadNextScreen()
            }
        })
        animator.start()
    }

    private fun loadNextScreen() {
        CoroutineScope(Dispatchers.Main).launch {
            delay(1000)
            val intent = Intent(context, DiagnosticTestActivity::class.java)
            startActivity(intent)
            requireActivity().finish()
        }
    }


}