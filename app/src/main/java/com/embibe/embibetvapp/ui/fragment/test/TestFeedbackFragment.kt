package com.embibe.embibetvapp.ui.fragment.test
//
//import android.animation.AnimatorSet
//import android.animation.ObjectAnimator
//import android.animation.ValueAnimator
//import android.os.Bundle
//import android.util.Log
//import android.view.LayoutInflater
//import android.view.View
//import android.view.View.VISIBLE
//import android.view.ViewGroup
//import android.view.ViewTreeObserver
//import android.widget.ImageView
//import android.widget.LinearLayout
//import android.widget.TextView
//import androidx.constraintlayout.widget.ConstraintLayout
//import com.embibe.embibetvapp.base.BaseAppFragment
//import com.embibe.embibetvapp.databinding.FragmentTestFeedbackBinding
//import com.embibe.embibetvapp.databinding.LayoutTestFeedbackClickToWatchBinding
//import com.embibe.embibetvapp.databinding.LayoutTestFeedbackRow1Binding
//import com.embibe.embibetvapp.databinding.LayoutTestFeedbackTopSkillRowBinding
//import com.embibe.embibetvapp.ui.adapter.TestBarAdapter
//import com.embibe.embibetvapp.ui.adapter.TopSkillAdapter
//import kotlin.math.abs
//
//class TestFeedbackFragment : BaseAppFragment(), View.OnClickListener {
//    var progressLayoutWidth: Int = 0
//    var height: Int = 0
//    var list = ArrayList<String>()
//    var listTopSkill = ArrayList<String>()
//    lateinit var inflater: LayoutInflater
//    var circleList = ArrayList<String>()
//    lateinit var binding: FragmentTestFeedbackBinding
//    private var layoutTestFeedbackRow1Binding: LayoutTestFeedbackRow1Binding? = null
//    private var layoutTestFeedbackClickToWatchBinding: LayoutTestFeedbackClickToWatchBinding? = null
//    private var layoutTestFeedbackTopSkillRowBinding: LayoutTestFeedbackTopSkillRowBinding? = null
//    private var progressBarList = ArrayList<ImageView>()
//    private var progressTextViewList = ArrayList<TextView>()
//    val TAG = this.javaClass.name
//
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        this.inflater = inflater
//        binding = FragmentTestFeedbackBinding.inflate(inflater)
//        //If data exist in API add this child to parent
//        addScorePorgressRowToParent()
//        addClickToWatchToParent()
//        addTopSkillToParent()
//        return binding.root
//    }
//
//    private fun addScorePorgressRowToParent() {
//        layoutTestFeedbackRow1Binding = LayoutTestFeedbackRow1Binding.inflate(inflater)
//        binding.llParent.addView(layoutTestFeedbackRow1Binding?.root)
//        var params = layoutTestFeedbackRow1Binding?.root?.layoutParams
//        params?.height = 300
//        layoutTestFeedbackRow1Binding?.root?.layoutParams = params
//        setMargin()
//        setBar()
//        setAndAnimateProgress()
//        layoutTestFeedbackRow1Binding?.clRow1?.setOnClickListener(this)
//    }
//
//    private fun addTopSkillToParent() {
//        layoutTestFeedbackTopSkillRowBinding =
//            LayoutTestFeedbackTopSkillRowBinding.inflate(inflater)
//        binding.llParent.addView(layoutTestFeedbackTopSkillRowBinding?.root)
//        var paramsTopSkill = layoutTestFeedbackTopSkillRowBinding?.root?.layoutParams
//        layoutTestFeedbackTopSkillRowBinding?.root?.layoutParams = paramsTopSkill
//        paramsTopSkill?.height = 250
//        setMarginTopSkill()
//        setBarTopSkill()
//        layoutTestFeedbackClickToWatchBinding?.constraintClickToWatch?.setOnClickListener(this)
//    }
//
//    private fun addClickToWatchToParent() {
//        layoutTestFeedbackClickToWatchBinding =
//            LayoutTestFeedbackClickToWatchBinding.inflate(inflater)
//        binding.llParent.addView(layoutTestFeedbackClickToWatchBinding?.root)
//        var paramsReplay = layoutTestFeedbackClickToWatchBinding?.root?.layoutParams
//        paramsReplay?.height = 230
//        layoutTestFeedbackClickToWatchBinding?.root?.layoutParams = paramsReplay
//        setMarginClickToWatch()
//    }
//
//    private fun setMarginTopSkill() {
//        var params =
//            layoutTestFeedbackTopSkillRowBinding?.clRow3?.layoutParams as ViewGroup.MarginLayoutParams
//        params.leftMargin = 60
//        params.rightMargin = 60
//        params.topMargin = 30
//        layoutTestFeedbackTopSkillRowBinding?.clRow3?.requestLayout()
//    }
//
//    private fun setBarTopSkill() {
//        listTopSkill.add("40%")
//        listTopSkill.add("80%")
//        listTopSkill.add("30%")
//        listTopSkill.add("10%")
//        listTopSkill.add("60%")
//        listTopSkill.add("38%")
//
//        var adapter = TopSkillAdapter()
//        adapter.setItems(listTopSkill)
//        layoutTestFeedbackTopSkillRowBinding?.layoutCorrectlyAnswered?.rvScoreBar?.adapter = adapter
//
//    }
//
//    private fun setAndAnimateProgress() {
////        progressBarList.add(layoutTestFeedbackRow1Binding?.layoutFeedbackProgress?.progressBarCutOff!!)
////        progressTextViewList.add(layoutTestFeedbackRow1Binding?.layoutFeedbackProgress?.tvCutOff!!)
//        var goodScore = 75
//        var yourScore = -12
//        var cutOffScore = 50
//        animate(
//            goodScore,
//            layoutTestFeedbackRow1Binding?.layoutFeedbackProgress?.progressBarGoodScore!!,
//            layoutTestFeedbackRow1Binding?.layoutFeedbackProgress?.tvGoodScore!!
//        )
//        moveVerticalBaseLineBiasBasedOnScore(yourScore)
//        moveCutOffLineBiasBasedOnScore(cutOffScore)
//        if (yourScore < 0) {
//            animate(
//                yourScore,
//                layoutTestFeedbackRow1Binding?.layoutFeedbackProgress?.progressBarNegative!!,
//                layoutTestFeedbackRow1Binding?.layoutFeedbackProgress?.tvNegativeScore!!
//            )
//        } else {
//            animate(
//                yourScore,
//                layoutTestFeedbackRow1Binding?.layoutFeedbackProgress?.progressBarYourScore!!,
//                layoutTestFeedbackRow1Binding?.layoutFeedbackProgress?.tvYourScore!!
//            )
//        }
//    }
//
//    private fun moveCutOffLineBiasBasedOnScore(cutOffScore: Int) {
//        // AnyScore/totalScore to get bias value
//        //totalMarks 130, Negative Marks -12
//
//        var calScore = (cutOffScore.toDouble().div(130))
//        Log.e(TAG, "cal Score $calScore")
//        var cutOffLine = layoutTestFeedbackRow1Binding?.layoutFeedbackProgress?.ivCutOffMarker
//        var cutOffLabel = layoutTestFeedbackRow1Binding?.layoutFeedbackProgress?.tvCutOffMarkerLabel
//        var cutOffLineParams = cutOffLine?.layoutParams as ConstraintLayout.LayoutParams
//        var cutOffLineAnimator = ValueAnimator.ofFloat(0f, calScore.toFloat())
//        cutOffLineAnimator.addUpdateListener { value ->
//            cutOffLineParams.horizontalBias = (value.animatedValue as Float)
//            cutOffLine.layoutParams = cutOffLineParams
//        }
//
//        var cutOffLabelAnimator = ValueAnimator.ofInt(0, cutOffScore)
//        cutOffLabelAnimator.addUpdateListener { value ->
//            cutOffLabel?.text = (value.animatedValue as Int).toString()
//        }
//        var animatorSet = AnimatorSet()
//        animatorSet.playTogether(cutOffLabelAnimator, cutOffLineAnimator)
//        animatorSet.duration = 1500
//        animatorSet.start()
//    }
//
//    private fun moveVerticalBaseLineBiasBasedOnScore(yourScore: Int) {
//        var baseLineVertical = layoutTestFeedbackRow1Binding?.layoutFeedbackProgress?.viewLine
//        if (yourScore < 0) {
//            var baseLineParams = baseLineVertical?.layoutParams as ConstraintLayout.LayoutParams
//            baseLineParams.horizontalBias =
//                baseLineParams.horizontalBias + (abs(yourScore).toFloat() / 100f)
//            baseLineVertical.layoutParams = baseLineParams
//            baseLineVertical.requestLayout()
//        }
//    }
//
//    private fun animate(progressValue: Int, progressBar: ImageView, tv: TextView) {
//        animateBar(
//            progressBar,
//            progressValue
//        )
//        tv?.animateWithProgressBar(progressValue)
//    }
//
//    private fun TextView.animateWithProgressBar(progressValue: Int) {
//
//        var animator = ObjectAnimator.ofInt(0, progressValue)
//        animator.addUpdateListener { animationValue ->
//            this?.text = animationValue.animatedValue.toString()
//        }
//        var animatorSet = AnimatorSet()
//        animatorSet.playTogether(animator)
//        animatorSet.duration = 2000
//        animatorSet.start()
//    }
//
//    private fun animateBar(bar: ImageView, progressValue: Int) {
//        var progressLayout = layoutTestFeedbackRow1Binding?.layoutFeedbackProgress
//        var vto = progressLayout?.root?.viewTreeObserver
//        var newProgressValue = 0
//        newProgressValue = if (progressValue < 0) {
//            abs(progressValue)
//        } else {
//            progressValue
//        }
//        var listener: ViewTreeObserver.OnGlobalLayoutListener? = null
//        listener = ViewTreeObserver.OnGlobalLayoutListener {
//            progressLayoutWidth = progressLayout?.root?.width!!
//            Log.e(TAG, "width $progressLayoutWidth")
//            height = progressLayout?.root?.height
//
//            if (newProgressValue < progressLayoutWidth!!) {
//                var calValue = newProgressValue.toDouble() / 130 * progressLayoutWidth
//                var animator = ValueAnimator.ofInt(0, calValue.toInt())
//                animator.addUpdateListener { valueAnimator ->
//                    var value = valueAnimator.animatedValue
//                    var params = bar.layoutParams as ConstraintLayout.LayoutParams
//                    if (value as Int > 0) {
//                        bar.visibility = VISIBLE
//                    } else
//                        bar.visibility = View.GONE
//                    params.width = value as Int
//                    bar.layoutParams = params
//                }
//
//                animator.duration = 1500
//                animator.start()
//            } else {
//                /*if progresValue is more than width
//                    newWidth = (currentMarks/TotalMarks) * layoutWidth
//                */
//            }
//            layoutTestFeedbackRow1Binding?.layoutFeedbackProgress?.root?.viewTreeObserver?.removeOnGlobalLayoutListener(
//                listener!!
//            )
//        }
//        vto?.addOnGlobalLayoutListener(listener)
//
//    }
//
//    private fun setMargin() {
//        var params =
//            layoutTestFeedbackRow1Binding?.clRow1?.layoutParams as ViewGroup.MarginLayoutParams
//        params.leftMargin = 60
//        params.rightMargin = 60
//        params.topMargin = 60
//        layoutTestFeedbackRow1Binding?.clRow1?.requestLayout()
//    }
//
//    private fun setMarginClickToWatch() {
//        var params =
//            layoutTestFeedbackClickToWatchBinding?.constraintClickToWatch?.layoutParams as ViewGroup.MarginLayoutParams
//        params.leftMargin = 60
//        params.rightMargin = 60
//        params.topMargin = 30
//        layoutTestFeedbackClickToWatchBinding?.constraintClickToWatch?.requestLayout()
//    }
//
//    private fun setBar() {
//        list.add("30 marks")
//        list.add("45 marks")
//        list.add("-39 marks")
////        list.add("20 marks")
////        list.add("25 marks")
//
//        var adapter = TestBarAdapter()
//        adapter.setItems(list)
//        layoutTestFeedbackRow1Binding?.layoutCorrectlyAnswered?.rvScoreBar?.adapter = adapter
//    }
//
//    override fun onClick(view: View?) {
//        when (view) {
//            layoutTestFeedbackRow1Binding?.clRow1!! -> {
//                expandLayoutWithAnimation()
//            }
//            layoutTestFeedbackClickToWatchBinding?.constraintClickToWatch!! -> {
//
//            }
//
//        }
//    }
//
//    private fun expandLayoutWithAnimation() {
//
//      if(list.size < 5) {
//         animateCorrectlyAnsweredText()
//      }
//
//        var params =
//            layoutTestFeedbackRow1Binding?.clRow1!!.layoutParams as LinearLayout.LayoutParams
//        Log.e(TAG, "height: ${params.height}")
//        var animator = ValueAnimator.ofInt(0, 60)
//        animator.addUpdateListener { animatorValue ->
//            var value = animatorValue.animatedValue
//            var calVal = 500 + value as Int
//            params.height = calVal
//            Log.e(TAG, "height: ${params.height}")
//            layoutTestFeedbackRow1Binding?.clRow1?.layoutParams = params
//            layoutTestFeedbackRow1Binding?.layoutTestScore?.tvGrade?.visibility = VISIBLE
//            layoutTestFeedbackRow1Binding?.layoutTestScore?.tvGradeLabel?.visibility = VISIBLE
//        }
//        animator.duration = 1000
//        animator.start()
//    }
//
//    private fun animateCorrectlyAnsweredText() {
//        var motionLayout = layoutTestFeedbackRow1Binding?.layoutCorrectlyAnswered?.constraintLayout
//        motionLayout?.transitionToStart()
//        motionLayout?.transitionToEnd()
//    }
//}