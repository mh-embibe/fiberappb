package com.embibe.embibetvapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemJarBinding
import com.embibe.embibetvapp.model.test.TestFeedbackJarModel
import java.util.*

@Suppress("DEPRECATION")
class TestJarAdapter(var listener: Listener) :
    RecyclerView.Adapter<TestJarAdapter.TestJarViewHolder>() {
    private var listJar = ArrayList<TestFeedbackJarModel>()
    var onItemClick: ((TestFeedbackJarModel) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TestJarAdapter.TestJarViewHolder {
        val binding: ItemJarBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_jar, parent, false
        )
        return TestJarViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TestJarViewHolder, position: Int) {
        holder.bind(listJar[position])
    }

    override fun getItemCount(): Int {
        return listJar.size
    }

    fun setData(list: ArrayList<TestFeedbackJarModel>) {
        this.listJar = list
        notifyDataSetChanged()
    }

    interface Listener {
        fun onJarSelected(item: TestFeedbackJarModel)
    }

    inner class TestJarViewHolder(var binding: ItemJarBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TestFeedbackJarModel) {

            binding.imageJar.setImageResource(item.jar)
            binding.textJar.text = item.nQuestions.toString()

            binding.ivFocus.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    itemView.animate().translationY(-4f).duration = 500
                else
                    itemView.animate().translationY(0.0f).duration = 500
            }

            binding.ivFocus.setOnClickListener {
                onItemClick?.invoke(listJar[adapterPosition])
//                listener.onJarSelected(item)

            }
        }
    }
}

