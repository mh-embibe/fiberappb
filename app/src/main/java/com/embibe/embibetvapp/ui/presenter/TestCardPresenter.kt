package com.embibe.embibetvapp.ui.presenter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.CountDownTimer
import android.text.TextUtils
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.leanback.widget.BaseCardView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.utils.DateTimeUtils
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


class TestCardPresenter(context: Context, var type: String) :
    AbstractCardPresenter<BaseCardView>(context) {

    //val timeRemain = arrayOf("2.05.04", "2.15.35", "1.05.04", "0.05.04", "1.20.14")
    override fun onCreateView(): BaseCardView {
        val cardView = BaseCardView(context, null, R.style.SideInfoCardStyle)
        cardView.addView(
            LayoutInflater.from(context).inflate(
                R.layout.card_test,
                null
            )
        )
        cardView.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            val viewAlpha = cardView.findViewById<View>(R.id.view_focus)
            if (hasFocus) {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_focused_alpha_value
                )
            } else {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_unfocused_alpha_value
                )
            }
        }
        cardView.isFocusable = true
        return cardView
    }

    ;
    override fun onBindViewHolder(item: Any, cardView: BaseCardView) {

        val card = item as Content
        val imgThumbnailView: ImageView = cardView.findViewById(R.id.iv_thumbnail)
        val tvSubjectName: TextView = cardView.findViewById(R.id.tvSubjectName)
        val tvTestTitle: TextView = cardView.findViewById(R.id.tvTestTitle)
        val tvTestCategory: TextView = cardView.findViewById(R.id.tvTestCategory)
        val ivCategory: ImageView = cardView.findViewById(R.id.ivCategory)
        val tvExpireTime: TextView = cardView.findViewById(R.id.tvTestExpireTime)

        //Test expiration details
        updateExpiryText(card,tvExpireTime)

        if (card.subject.isNotEmpty())
            tvSubjectName.text = card.subject
        else
            tvSubjectName.visibility = View.INVISIBLE
        (tvSubjectName.background as GradientDrawable).setColor(
            context.resources.getColor(getColorCode(card.subject))
        )
        tvTestTitle.text = card.title
        tvTestCategory.text = card.category
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(context).setDefaultRequestOptions(requestOptions)
            .load(card.thumb)
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.video_placeholder)
            .transform(RoundedCorners(8))
            .into(imgThumbnailView)

        Glide.with(context)
            .load(card.category_thumb)
            .into(ivCategory)
        applyShadowLayerToTextView(tvTestTitle)
        applyShadowLayerToTextView(tvTestCategory)
    }

    private fun updateExpiryText(card: Content,tvExpireTime:TextView){
        if (card != null && (card.startedAtInMilliSeconds != null ||
                    !TextUtils.isEmpty(card.startedAtInMilliSeconds))
            && (card.duration > 0) && (!TextUtils.isEmpty(card.test_status)
                    && card.test_status.contentEquals("started"))) {

            try {
                val startMillis = card.startedAtInMilliSeconds.toLong()
                Log.e(TAG, "startedAtInMilliSeconds: " + card.startedAtInMilliSeconds.toLong())
                //Current time formatted
                val obj = Calendar.getInstance()
                val nowDate = obj.time.time
                //Diff
                val difference = nowDate - startMillis
                val finalDateMilliSec = card.duration * DateTimeUtils.ONE_SECOND_IN_MILLI - difference
                Log.e(TAG, "Remaining Time: $finalDateMilliSec")

                if (finalDateMilliSec > 0) {
                    var timer = object : CountDownTimer(finalDateMilliSec, DateTimeUtils.ONE_SECOND_IN_MILLI ) {
                        override fun onTick(millisUntilFinished: Long) {
                            val millis = millisUntilFinished
                            tvExpireTime.visibility = View.VISIBLE
                            tvExpireTime.text = context.getString(
                                R.string.expiring_in,
                                DateTimeUtils.getFormattedTimerString(millis)
                            )
                        }

                        override fun onFinish() {
                            tvExpireTime.text = context.getString(R.string.expired)
                        }
                    }
                    timer.start()

                } else {
                    tvExpireTime.visibility = View.VISIBLE
                    tvExpireTime.text = context.getString(R.string.expired)
                }

            } catch (ex: Exception) {
                Log.e(TAG, "Error: " + ex.localizedMessage)
            }
        } else tvExpireTime.visibility = View.GONE

    }

    private fun applyShadowLayerToTextView(tv: TextView) {
        tv.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        tv.setShadowLayer(30f, 0f, 4f, Color.BLACK)
    }

    private fun getColorCode(subject: String?): Int {
        return when (subject?.toLowerCase(Locale.ENGLISH)) {
            "maths", "mathematics" -> return R.color.sub_maths
            "chemistry" -> return R.color.sub_chemistry
            "biology" -> return R.color.sub_biology
            "physics" -> return R.color.sub_physics
            else -> R.color.sub_all
        }
    }


    companion object {
        private const val TAG = "TestCardPresenter"
    }

}
