package com.embibe.embibetvapp.ui.custom

import android.content.Context
import android.graphics.Rect
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.TvKeysToSearchRes
import com.embibe.embibetvapp.utils.SegmentUtils
import java.util.*
import kotlin.collections.ArrayList

class CustomEasyTVKeyboard @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    private lateinit var tvSearchView: TextView
    private lateinit var spaceBarButton: Button
    private lateinit var rvKeyList: RecyclerView
    private lateinit var backSpaceButton: Button
    private lateinit var mAdapter: MyAdapter
    private lateinit var focusToSearchRes: TvKeysToSearchRes
    private lateinit var dPadKeysListener: DPadKeysListener

    private var numberOfQuickLinks = 0
    private var searchResultsCount = 0
    private var mOnItemClickListener: OnRecyclerViewItemClickListener? = null
    private var mOnTextChangedListener: OnTextChangedListener? = null

    private val leftKeyBoardKeys: ArrayList<String> = ArrayList()
    private val bottomKeyBoardKeys: ArrayList<String> = ArrayList()
    private val rightKeyBoardKeys: ArrayList<String> = ArrayList()
    private val searchText: String = ""
    var actualInputText: String? = ""
    var showText: String? = ""

    private fun initView(context: Context?, attrs: AttributeSet?) {

        fillLeftKeys()
        fillBottomKeys()
        fillRightKeys()

        View.inflate(context, R.layout.custom_input_board, this)
        tvSearchView = findViewById(R.id.tv_input)
        rvKeyList = findViewById(R.id.rv_keylist)
        backSpaceButton = findViewById(R.id.backspace_button)
        spaceBarButton = findViewById(R.id.spaceBar_button)
        tvSearchView.visibility = View.INVISIBLE
        tvSearchView.addTextChangedListener(textWatcher)
        spaceBarButtonListeners()
        backSpaceButtonListeners()
        val layoutManager =
            GridLayoutManager(context, 6)
        rvKeyList.layoutManager = layoutManager
        val wideSpacingInPixels =
            resources.getDimensionPixelSize(R.dimen.recycleview_item_spacing_20)
        val heightSpacingInPixels =
            resources.getDimensionPixelSize(R.dimen.recycleview_item_spacing_30)
        rvKeyList.setHasFixedSize(true)
        rvKeyList.addItemDecoration(
            SpacesItemDecoration(
                wideSpacingInPixels
            )
        )
        mAdapter =
            MyAdapter(inputList)
        rvKeyList.adapter = mAdapter
        mOnItemClickListener =
            object : OnRecyclerViewItemClickListener {
                override fun onItemClick(view: View?, data: String?) {

                    actualInputText += data
                    showText = transferShowingText(actualInputText)
                    tvSearchView.text = showText
                }
            }
        mAdapter.setOnItemClickListener(mOnItemClickListener)
        SegmentUtils.trackEventSearchKeyBoardKeyPress(
            actualInputText!!,
            actualInputText!!.length.toString(),
            "Search Keypad"
        )
    }

    private fun backSpaceButtonListeners() {
        backSpaceButton.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                        deleteSearchText()
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        backSpaceButton.postDelayed({
                            backSpaceButton.requestFocus()
                        }, 0)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        dPadKeysListener.isKeyPadRight(true, "BackSpaceButton")
                    }
                }
            }

            false
        }
        backSpaceButton.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    SegmentUtils.trackEventSearchKeyBoardKeyFocus()
                }
            }
        }
    }

    private fun spaceBarButtonListeners() {

        spaceBarButton.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        dPadKeysListener.isKeyPadLeft(true, "SpaceBarButton")
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        spaceBarButton.postDelayed({
                            spaceBarButton.requestFocus()
                        }, 0)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        actualInputText = getSearchText() + " "
                        SegmentUtils.trackEventSearchKeyBoardKeyPress(
                            actualInputText!!,
                            actualInputText!!.length.toString(),
                            "Search Keypad"
                        )
                    }
                }
            }
            false
        }
        spaceBarButton.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    SegmentUtils.trackEventSearchKeyBoardKeyFocus()
                }
            }
        }
    }

    private fun fillRightKeys() {
        rightKeyBoardKeys.add("F")
        rightKeyBoardKeys.add("L")
        rightKeyBoardKeys.add("R")
        rightKeyBoardKeys.add("X")
        rightKeyBoardKeys.add("3")
        rightKeyBoardKeys.add("9")
    }

    private fun fillBottomKeys() {
        bottomKeyBoardKeys.add("4")
        bottomKeyBoardKeys.add("5")
        bottomKeyBoardKeys.add("6")
        bottomKeyBoardKeys.add("7")
        bottomKeyBoardKeys.add("8")
        bottomKeyBoardKeys.add("9")
    }

    /**
     * left most column keyboard keys
     */
    private fun fillLeftKeys() {
        leftKeyBoardKeys.add("A")
        leftKeyBoardKeys.add("G")
        leftKeyBoardKeys.add("M")
        leftKeyBoardKeys.add("S")
        leftKeyBoardKeys.add("Y")
        leftKeyBoardKeys.add("4")
    }

    private fun getSearchText(): String? {
        return actualInputText
    }

    fun setSearchText(search: String?) {
        actualInputText = search
        showText = transferShowingText(actualInputText)
        tvSearchView.text = showText
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var widthMeasureSpec = widthMeasureSpec
        var heightMeasureSpec = heightMeasureSpec
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(574, MeasureSpec.EXACTLY)
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(512, MeasureSpec.EXACTLY)
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    private inner class MyAdapter(datas: Array<String>?) :
        RecyclerView.Adapter<MyAdapter.ViewHolder>(),
        OnClickListener {
        private var initFlag = true
        var datas: Array<String>? = null
        var onRecyclerViewItemClickListener: OnRecyclerViewItemClickListener? =
            null

        override fun onCreateViewHolder(
            viewGroup: ViewGroup,
            viewType: Int
        ): ViewHolder {
            val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.input_board_item, viewGroup, false)
            val vh =
                ViewHolder(view)
            view.setOnClickListener(this)
            return vh
        }

        override fun onBindViewHolder(
            viewHolder: ViewHolder,
            position: Int
        ) {
            val keyboardValue = datas!![position].toUpperCase(Locale.US)
            viewHolder.mTextView.text = keyboardValue
            viewHolder.itemView.tag = datas!![position]
            if (initFlag && position == 0) {
                initFlag = false
                viewHolder.itemView.requestFocus()
            }
//            viewHolder.itemView.nextFocusRightId = nextFocusRightId
//            viewHolder.itemView.nextFocusDownId = nextFocusDownId

            viewHolder.itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_LEFT -> {
                            if (leftKeyBoardKeys.contains(keyboardValue)) {
                                dPadKeysListener.isKeyPadLeft(true, "Keyboard")
                            }
                        }
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            if (bottomKeyBoardKeys.contains(keyboardValue)
                                &&
                                numberOfQuickLinks == 0
                            ) {
                                viewHolder.itemView.postDelayed({
                                    viewHolder.itemView.requestFocus()
                                }, 0)
                            }
                        }
                        KeyEvent.KEYCODE_DPAD_RIGHT -> {
                            if (rightKeyBoardKeys.contains(keyboardValue)
                                &&
                                searchResultsCount == 0
                            ) {
                                viewHolder.itemView.postDelayed({
                                    viewHolder.itemView.requestFocus()
                                }, 0)
                            }
                        }
                    }
                }

                false
            }
            viewHolder.itemView.setOnFocusChangeListener { v, hasFocus ->
                when (hasFocus) {
                    true -> {
                        SegmentUtils.trackEventSearchKeyBoardKeyFocus()
                    }
                }
            }
        }

        override fun getItemCount(): Int {
            return datas!!.size
        }

        override fun onClick(v: View) {
            if (onRecyclerViewItemClickListener != null) {
                onRecyclerViewItemClickListener!!.onItemClick(v, v.tag as String)
            }
        }

        inner class ViewHolder(view: View) :
            RecyclerView.ViewHolder(view) {
            var mTextView: TextView = view.findViewById(R.id.tv_textInput)

        }

        fun setOnItemClickListener(listener: OnRecyclerViewItemClickListener?) {
            onRecyclerViewItemClickListener = listener
        }

        init {
            this.datas = datas
        }
    }

    /*public void setTextWatcher(TextWatcher mTextWatcher) {
        this.mTextWatcher = mTextWatcher;
    }*/
    interface OnTextChangedListener {
        //void beforeTextChaned();
        fun onTextChanged(text: String?)
    }

    fun setTextChangedListener(mOnTextChangedListener: OnTextChangedListener?) {
        this.mOnTextChangedListener = mOnTextChangedListener
    }

    private var textWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable) {
            if (mOnTextChangedListener != null) {
                mOnTextChangedListener!!.onTextChanged(s.toString())
            }
        }
    }

    private fun deleteSearchText() {
        if (actualInputText != null) {
            val len = actualInputText!!.length

            if (len > 1) {
                actualInputText = actualInputText!!.substring(0, len - 1)
                showText = transferShowingText(actualInputText)
                tvSearchView.text = showText
            } else {
                actualInputText = ""
                showText = ""
                tvSearchView.text = showText
            }

        } else {
            actualInputText = ""
            showText = ""
            tvSearchView.text = showText
        }
        SegmentUtils.trackEventSearchKeyBoardKeyPress(
            actualInputText!!,
            actualInputText!!.length.toString(),
            "Search Keypad"
        )
    }

    private fun transferShowingText(actualText: String?): String? {
        return actualText
    }

    fun restoreSelection(from: String) {
        when (from) {
            "Mic" -> {
                backSpaceButton.postDelayed({
                    backSpaceButton.requestFocus()
                }, 0)
            }
            "Nav" -> {
                backSpaceButton.postDelayed({
                    backSpaceButton.requestFocus()
                }, 0)
            }
        }
//        backSpaceButton.focusSearch(FOCUS_LEFT).requestFocus()
    }

    fun setQuickLinksSize(quickLinksSize: Int) {
        numberOfQuickLinks = quickLinksSize
    }

    fun setSearchResultsSize(searchResultsSize: Int) {
        searchResultsCount = searchResultsSize
    }

    fun setDpadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysListener = callback
    }

    interface OnRecyclerViewItemClickListener {
        fun onItemClick(view: View?, data: String?)
    }

    fun setFocusToSearchListener(listener: TvKeysToSearchRes) {
        focusToSearchRes = listener
    }

    class SpacesItemDecoration(private val space: Int) : ItemDecoration() {
        override fun getItemOffsets(
            outRect: Rect, view: View,
            parent: RecyclerView, state: RecyclerView.State
        ) {
            outRect.bottom = space
            outRect.top = space
        }

    }

    companion object {
        private const val TAG = "CustomEasyTVKeyboard"
        private val inputList = arrayOf(
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
            "g",
            "h",
            "i",
            "j",
            "k",
            "l",
            "m",
            "n",
            "o",
            "p",
            "q",
            "r",
            "s",
            "t",
            "u",
            "v",
            "w",
            "x",
            "y",
            "z",
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"
        )
    }

    init {
        initView(context, attrs)
    }
}