package com.embibe.embibetvapp.ui.fragment.test

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.databinding.FragmentBannerImgFlipperBinding
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.ui.interfaces.BannerImageDataToHostListener
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.data.DataManager
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import kotlin.properties.Delegates

class TestBannerImgFlipper : BaseAppFragment() {

    private lateinit var binding: FragmentBannerImgFlipperBinding
    private lateinit var bannerImageDataToHostListener: BannerImageDataToHostListener

    private val classTag = TestBannerImgFlipper::class.java.simpleName
    private var imageViewList = ArrayList<ImageView>()
    private lateinit var homeViewModel: HomeViewModel
    private var bannerDataSize by Delegates.notNull<Int>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_banner_img_flipper, container, false
        )
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        doAsync {
            val bannerData = if (DataManager.instance.getHeroBannerDataTest().data.isNotEmpty()) {
                DataManager.instance.getHeroBannerDataTest().data
            } else {
                homeViewModel.fetchBannerSectionByPageName(com.embibe.embibetvapp.constant.AppConstants.TEST)
                    ?: arrayListOf()
            }
            uiThread {
                bannerData as ArrayList
                bannerDataSize = bannerData.size
                initViewFlipper(bannerData)
                viewFlipperListener(bannerData)
            }
        }
        return binding.root
    }

    private fun viewFlipperListener(data: ArrayList<BannerData>) {
        binding.vfBanner.addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            for (slidePosition in 0 until imageViewList.size) {
                if (binding.vfBanner.currentView == imageViewList[slidePosition]) {
                    bannerImageDataToHostListener.setBannerImageData(
                        data[slidePosition],
                        slidePosition + 1
                    )
                    break
                }
            }
        }
    }

    private fun initViewFlipper(data: ArrayList<BannerData>) {

        for (i in 0 until data.size) {
            context?.let {
                val imageView = ImageView(it)
                imageView.apply {
                    scaleType = ImageView.ScaleType.CENTER_CROP
                }
                imageViewList.add(imageView)
            }
        }

        for (i in 0 until data.size) {
            if (i < imageViewList.size) {
                addImgViewToFlipper(data[i].imgUrl, imageViewList[i])
            }
        }
    }

    private fun addImgViewToFlipper(imgUrl: String, imageView: ImageView) {
        val uri = Uri.parse(imgUrl)
        setImage(imageView, uri)
        binding.vfBanner.addView(imageView)
        //don't play instantly for the first time hence the delay
        binding.vfBanner.postDelayed({
            startImgFlipping()
        }, FlipperPanel.FLIP_INTERVAL.toLong())
    }

    private fun setImage(imageView: ImageView, uri: Uri) {
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        context?.let {
            Glide.with(it).setDefaultRequestOptions(requestOptions).load(uri)
                .into(imageView)
        }
    }

    fun bannerImageDataListener(callback: BannerImageDataToHostListener) {
        this.bannerImageDataToHostListener = callback
    }

    fun stopImgFlipping() {
        binding.vfBanner.stopFlipping()
    }

    fun startImgFlipping() {
        if (bannerDataSize > 1)
            context?.let {
                binding.vfBanner.inAnimation = null
                binding.vfBanner.outAnimation = null
                binding.vfBanner.startFlipping()
                binding.vfBanner.flipInterval = FlipperPanel.FLIP_INTERVAL
                binding.vfBanner.setInAnimation(it, R.anim.enter_from_right)
                binding.vfBanner.setOutAnimation(it, R.anim.exit_from_left)
            }
    }

    object FlipperPanel {
        const val FLIP_INTERVAL = 10000
    }

}
