package com.embibe.embibetvapp.ui.fragment.errorAlertScreen

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.ALERT_MSG_DIALOG
import com.embibe.embibetvapp.constant.AppConstants.APP_CLOSE_DIALOG
import com.embibe.embibetvapp.constant.AppConstants.CANCEL
import com.embibe.embibetvapp.constant.AppConstants.CLOSE_VIDEO
import com.embibe.embibetvapp.constant.AppConstants.CONTINUE_PRACTICE
import com.embibe.embibetvapp.constant.AppConstants.CONTINUE_TEST
import com.embibe.embibetvapp.constant.AppConstants.CUSTOM_ALERT_DIALOG
import com.embibe.embibetvapp.constant.AppConstants.END_SESSION
import com.embibe.embibetvapp.constant.AppConstants.ERROR_404_DIALOG
import com.embibe.embibetvapp.constant.AppConstants.ERROR_SERVER_NOT_REACHABLE
import com.embibe.embibetvapp.constant.AppConstants.LEAVE_APP
import com.embibe.embibetvapp.constant.AppConstants.NO_CONNECTIVITY
import com.embibe.embibetvapp.constant.AppConstants.NO_MORE_QUESTIONS_DIALOG
import com.embibe.embibetvapp.constant.AppConstants.ON_BACK_PRESS_DIALOG_FRAGMENT
import com.embibe.embibetvapp.constant.AppConstants.PRACTICE_DIALOG
import com.embibe.embibetvapp.constant.AppConstants.PRACTICE_SESSION_ALERT_DIALOG
import com.embibe.embibetvapp.constant.AppConstants.PracticeModes.AdhocPractice
import com.embibe.embibetvapp.constant.AppConstants.SELECT_DAY_MONTH_YEAR
import com.embibe.embibetvapp.constant.AppConstants.SERVER_DOWN
import com.embibe.embibetvapp.constant.AppConstants.TEST_SESSION_ALERT_DIALOG
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CLOSE_DIALOG
import com.embibe.embibetvapp.databinding.*
import com.embibe.embibetvapp.ui.activity.PracticeActivity
import com.embibe.embibetvapp.ui.adapter.RvAdapterCalender
import com.embibe.embibetvapp.ui.interfaces.CommonAndroidAPI
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener
import com.embibe.embibetvapp.ui.interfaces.VideoPlayerInterface
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibejio.coreapp.application.LibApp
import com.embibejio.coreapp.preference.PreferenceHelper
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent


class CustomScreenDialog : DialogFragment() {

    private var layout404ErrorBinding: Layout404ErrorBinding? = null
    private var layoutServerDownBinding: LayoutServerDownBinding? = null
    private var layoutServerNotReachableBinding: LayoutServerDownBinding? = null
    private var layoutNoMoreQuestionsBinding: LayoutNoMoreQuestionsBinding? = null
    private var layoutNoConnectivityBinding: LayoutNoConnectivityBinding? = null
    private var fragmentAppClosingAlertMsg: FragmentAppCloseAlertMsgBinding? = null
    private var fragmentCloseVideoAlertMsgBinding: FragmentCloseVideoAlertMsgBinding? = null

    private var fragmentAlertMsgBinding: FragmentAlertMsgBinding? = null
    private var fragmentCustomAlertMsgBinding: FragmentCustomAlertMsgBinding? = null
    private var fragmentPracticeSessionAlertMsgBinding: FragmentPracticeSessionAlertMsgBinding? =
        null
    private var fragmentTestSessionAlertMsgBinding: FragmentTestSessionAlertMsgBinding? =
        null
    private var fragmentDayWeekMonthBinding: FragmentDayWeekMonthBinding? = null
    private var practiceDialogBinding: ActivityPracticeDialogBinding? = null
    private var inflater: LayoutInflater? = null
    private var container: ViewGroup? = null

    val TEMPLATE_PRACTICE = "practice/index.html"
    private var msg: String? = null

    private var dialogConstant: String? = null
    private var conceptCode: String = ""

    private var errorScreensBtnClickListener: ErrorScreensBtnClickListener? = null
    private var androidAPIListener: CommonAndroidAPI.AndroidAPIOnCallListener? = null
    private var videoPlayerListener: VideoPlayerInterface? = null
    private var bacKPressListener: BaseViewModel.BacKPressListener? = null

    private var callBacks: BaseViewModel.ErrorCallBacks? = null
    private var language = "en"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Utils.isDialogShowing = true
        setStyle(STYLE_NORMAL, android.R.style.ThemeOverlay)
        //isCancelable = false
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return object : Dialog(activity!!, theme) {
            override fun onBackPressed() {
                when (dialogConstant) {

                    PRACTICE_DIALOG -> {
                        practiceDialogBinding!!.webView.evaluateJavascript(
                            "CommonWebviewAPI.isKeyboardVisible()"
                        ) { value ->
                            if (value == "false")
                                videoPlayerListener!!.backPressed()
                        }

                    }

                    NO_MORE_QUESTIONS_DIALOG -> {
                    }

                    VIDEO_CLOSE_DIALOG -> {
                        closeDialog()
                        errorScreensBtnClickListener?.screenUpdate(
                            ON_BACK_PRESS_DIALOG_FRAGMENT
                        )
                    }
                    NO_CONNECTIVITY -> bacKPressListener?.onCustomBackPressed()

                    else -> {
                        Utils.isDialogShowing = false
                        super.onBackPressed()
                    }
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.inflater = inflater
        this.container = container
        when (dialogConstant) {
            ERROR_404_DIALOG -> {
                SegmentUtils.trackErrorLoadStart()
                layout404ErrorBinding = binder(R.layout.layout_404_error)
                setOnClickListenerAndDataToViews(layout404ErrorBinding)
                SegmentUtils.trackErrorLoadEnd()
                return layout404ErrorBinding?.root
            }
            APP_CLOSE_DIALOG -> {
                fragmentAppClosingAlertMsg = binder(R.layout.fragment_app_close_alert_msg)
                setOnClickListenerAndDataToViews(fragmentAppClosingAlertMsg)
                return fragmentAppClosingAlertMsg?.root
            }
            VIDEO_CLOSE_DIALOG -> {
                fragmentCloseVideoAlertMsgBinding = binder(
                    R.layout.fragment_close_video_alert_msg
                )
                setOnClickListenerAndDataToViews(fragmentCloseVideoAlertMsgBinding)
                return fragmentCloseVideoAlertMsgBinding?.root
            }
            ALERT_MSG_DIALOG -> {
                fragmentAlertMsgBinding = binder(R.layout.fragment_alert_msg)
                setOnClickListenerAndDataToViews(fragmentAlertMsgBinding)
                return fragmentAlertMsgBinding?.root
            }
            CUSTOM_ALERT_DIALOG -> {
                fragmentCustomAlertMsgBinding = binder(R.layout.fragment_custom_alert_msg)
                setOnClickListenerAndDataToViews(fragmentCustomAlertMsgBinding)
                return fragmentCustomAlertMsgBinding?.root
            }
            NO_MORE_QUESTIONS_DIALOG -> {
                layoutNoMoreQuestionsBinding = binder(R.layout.layout_no_more_questions)
                setOnClickListenerAndDataToViews(layoutNoMoreQuestionsBinding)
                return layoutNoMoreQuestionsBinding?.root
            }
            ERROR_SERVER_NOT_REACHABLE -> {
                SegmentUtils.trackErrorLoadStart()
                layoutServerNotReachableBinding = binder(R.layout.layout_server_down)
                setOnClickListenerAndDataToViews(layoutServerNotReachableBinding)
                SegmentUtils.trackErrorLoadEnd()
                return layoutServerNotReachableBinding?.root
            }
            SERVER_DOWN -> {
                SegmentUtils.trackErrorLoadStart()
                layoutServerDownBinding = binder(R.layout.layout_server_down)
                setOnClickListenerAndDataToViews(layoutServerDownBinding)
                /* var listener = object : View.OnKeyLisstener {
                     override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                         if (keyCode == KeyEvent.KEYCODE_BACK) {
                             if (event!!.action != KeyEvent.ACTION_DOWN) {
                                 return false
                             }
                             return true
                         }
                         return true
                     }
                 }*/

                //layoutServerDownBinding?.btnSure?.setOnKeyListener(listener)
                SegmentUtils.trackErrorLoadEnd()
                return layoutServerDownBinding?.root
            }
            NO_CONNECTIVITY -> {
                SegmentUtils.trackErrorLoadStart()
                layoutNoConnectivityBinding = binder(R.layout.layout_no_connectivity)
                setOnClickListenerAndDataToViews(layoutNoConnectivityBinding)
                SegmentUtils.trackErrorLoadEnd()
                return layoutNoConnectivityBinding?.root
            }
            PRACTICE_SESSION_ALERT_DIALOG -> {
                fragmentPracticeSessionAlertMsgBinding = binder(
                    R.layout.fragment_practice_session_alert_msg
                )
                setOnClickListenerAndDataToViews(fragmentPracticeSessionAlertMsgBinding)
                return fragmentPracticeSessionAlertMsgBinding?.root
            }

            TEST_SESSION_ALERT_DIALOG -> {
                fragmentTestSessionAlertMsgBinding = binder(
                    R.layout.fragment_test_session_alert_msg
                )
                setOnClickListenerAndDataToViews(fragmentTestSessionAlertMsgBinding)
                return fragmentTestSessionAlertMsgBinding?.root
            }

            PRACTICE_DIALOG -> {
                // set color transpartent
                dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                practiceDialogBinding = binder(R.layout.activity_practice_dialog)
                setWebView(practiceDialogBinding)
                return practiceDialogBinding?.root

            }
            SELECT_DAY_MONTH_YEAR -> {
                fragmentDayWeekMonthBinding = binder(R.layout.fragment_day_week_month)
                setFunctionalityAndView(fragmentDayWeekMonthBinding)
                return fragmentDayWeekMonthBinding?.root
            }
        }

        return null
    }

    private fun setWebView(practiceDialogBinding: ActivityPracticeDialogBinding?) {
        if (practiceDialogBinding != null) {
            practiceDialogBinding.webView.webChromeClient = WebChromeClient()
            practiceDialogBinding.webView.webViewClient = object : WebViewClient() {


                override fun onPageFinished(view: WebView, url: String) {
                    super.onPageFinished(view, url)
                    Log.d("WEBVIEW", "Web view ready")
                }
            }
            practiceDialogBinding.webView.settings.allowFileAccess = true
            practiceDialogBinding.webView.isLongClickable = false
            practiceDialogBinding.webView.settings.javaScriptEnabled = true
            practiceDialogBinding.webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
            practiceDialogBinding.webView.setBackgroundColor(Color.argb(0, 0, 0, 0))
            practiceDialogBinding.webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
            practiceDialogBinding.webView.addJavascriptInterface(
                CommonAndroidAPI(androidAPIListener),
                CommonAndroidAPI.NAME
            )
            if (Build.VERSION.SDK_INT > 19) {
                practiceDialogBinding.webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
            } else {
                practiceDialogBinding.webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
            }
            val base = LibApp.context.filesDir.path.toString() + "/assets/"
            var token = PreferenceHelper()[AppConstants.EMBIBETOKEN, ""]
            var mode = AdhocPractice
            language = Utils.getCurrentLocale()
            var query = "token=$token&mode=$mode&questionCode=$msg&conceptCode=$conceptCode"
            if (language != null)
                query += "&language=$language"
            val template = "file:///android_asset/${PracticeActivity.TEMPLATE_PRACTICE}?$query"
            //disableLongPress(webView)
            //practiceDialogBinding.webView.loadUrl("file:///data/user/0/com.embibe.jioembibe.debug/files/assets/practice/index.html?token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTExNzEwOTEyNywiZW1haWwiOm51bGwsIm9yZ2FuaXphdGlvbl9pZCI6bnVsbCwiaXNfZ3Vlc3QiOmZhbHNlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA0LTMwVDA3OjExOjM0Ljk0NVoifQ.qK4Mib43ngBFTcqd_7oU4jRqNMWxhIbkzKedNS4skA-sYyVHzjJ2SP7XmKuFZo9DQzZxA7izKVNpnzJLWwT0ZA&practice_id=5e9b573028e1174ef2785c2d&title=Carbon and its Compounds&lm_level=chapter&lm_code=ch221851&exam_code=ex221725")
            practiceDialogBinding.webView.loadUrl(template)
        }
    }


    fun closeDialog() {
        Utils.isDialogShowing = false
        if (dialog != null && dialog?.isShowing!!) {
            dismiss()
        }
    }

    fun setMsg(msg: String) {
        this.msg = msg
    }

    fun setDialogConstant(constant: String) {
        dialogConstant = constant
    }

    fun setListener(listener: Any?) {
        if (listener is ErrorScreensBtnClickListener)
            errorScreensBtnClickListener = listener
        if (listener is CommonAndroidAPI.AndroidAPIOnCallListener)
            androidAPIListener = listener
        if (listener is VideoPlayerInterface) {
            androidAPIListener = listener.androidAPIOnCallListener
            videoPlayerListener = listener
        }
        if (listener is BaseViewModel.BacKPressListener) {
            bacKPressListener = listener
        }
    }

    fun updateSearchQuery(searchQuery: String) {
        practiceDialogBinding?.webView?.evaluateJavascript(
            "CommonWebviewAPI.onFinishVoiceInput('" + searchQuery + "')",
            null
        )
    }

    fun setCallBacks(callBacks: BaseViewModel.ErrorCallBacks?) {
        this.callBacks = callBacks
    }

    fun <T : ViewDataBinding> binder(layout: Int): T? {
        return DataBindingUtil.inflate<T>(inflater!!, layout, container, false)

    }

    fun setOnClickListenerAndDataToViews(binding: Any?) {
        when (binding) {
            is Layout404ErrorBinding -> {
                binding.iv404Error.setImageResource(R.drawable.ic_404error)
                setMsgToView(binding.tvMsg)
                binding.btnReturn.setOnClickListener {
                    SegmentUtils.trackErrorCTAClick("close")
                    closeDialog()
                    callBacks?.onDismiss()
                }
            }
            is FragmentAppCloseAlertMsgBinding -> {
                binding.ivAlertMsg.setImageResource(R.drawable.ic_alert_msg)
                setMsgToView(binding.tvMsg2)
                binding.btnCancel.setOnClickListener {
                    closeDialog()
                    errorScreensBtnClickListener?.screenUpdate(CANCEL)
                }
                binding.btnLeaveApp.setOnClickListener {
                    errorScreensBtnClickListener?.screenUpdate(LEAVE_APP)
                }
            }
            is FragmentCloseVideoAlertMsgBinding -> {
                binding.ivAlertMsg.setImageResource(R.drawable.ic_alert_msg)
                setMsgToView(binding.tvMsg2)
                binding.btnCancel.setOnClickListener {
                    closeDialog()
                    errorScreensBtnClickListener?.screenUpdate(CANCEL)
                }
                binding.btnLeaveApp.setOnClickListener {
                    errorScreensBtnClickListener?.screenUpdate(CLOSE_VIDEO)
                }
            }
            is FragmentAlertMsgBinding -> {
                binding.ivAlertMsg.setImageResource(R.drawable.ic_alert_msg)
                setMsgToView(binding.tvMsg2)
                binding.btnOkay.setOnClickListener {
                    closeDialog()
                }
            }
            is FragmentCustomAlertMsgBinding -> {
                binding.ivPositive.setImageResource(R.drawable.ic_custom_alert_msg)
                setMsgToView(binding.tvMsg2)
                binding.btnOkay.setOnClickListener {
                    closeDialog()
                }
            }
            is LayoutServerDownBinding -> {
                binding.ivNoNetwork.setImageResource(R.drawable.ic_no_network)
                setMsgToView(binding.tvMsg2)
                binding.btnSure.setOnClickListener {
                    SegmentUtils.trackErrorCTAClick("close")
                    closeDialog()
                    //callBacks?.onRetry(msg ?: "")
                }
            }
            is LayoutNoConnectivityBinding -> {
                binding.ivNoWifi.setImageResource(R.drawable.ic_no_wifi)
                setMsgToView(binding.tvMsg)
                binding.btnReturn.setOnClickListener {
                    SegmentUtils.trackErrorCTAClick("retry")
                    closeDialog()
                    callBacks?.onRetry(msg ?: "")
                }
                binding.btnCancel2.setOnClickListener {
                    SegmentUtils.trackErrorCTAClick("close")
                    closeDialog()
                    callBacks?.onDismiss()
                }
            }
            is FragmentPracticeSessionAlertMsgBinding -> {
                setMsgToView(binding.tvMsg2)
                binding.btnEndSession.setOnClickListener {
                    errorScreensBtnClickListener?.screenUpdate(END_SESSION)
                }
                binding.btnContinueSession.setOnClickListener {
                    errorScreensBtnClickListener?.screenUpdate(CONTINUE_PRACTICE)
                    closeDialog()
                }
            }

            is FragmentTestSessionAlertMsgBinding -> {
                setMsgToView(binding.tvMsg2)
                binding.btnEndSession.setOnClickListener {
                    errorScreensBtnClickListener?.screenUpdate(END_SESSION)
                }
                binding.btnContinueSession.setOnClickListener {
                    errorScreensBtnClickListener?.screenUpdate(CONTINUE_TEST)
                    closeDialog()
                }
            }

            is LayoutNoMoreQuestionsBinding -> {
                setMsgToView(binding.tvMsg2)
                binding.btnGoBack.setOnClickListener {
                    Utils.isDialogShowing = false
                    androidAPIListener?.close()
                }
            }

        }
    }

    fun setMsgToView(textView: TextView) {
        if (!msg.isNullOrEmpty())
            textView.text = msg
    }

    fun setConceptCode(concept_code: String?) {
        if (concept_code != null)
            conceptCode = concept_code
    }

    fun setFunctionalityAndView(binding: FragmentDayWeekMonthBinding?) {
        var adapterCalender = RvAdapterCalender()
        makeGridCenter(binding)
        binding!!.rvDayMonthYear.adapter = adapterCalender

        adapterCalender.onItemClick = { calenderType ->
            errorScreensBtnClickListener?.screenUpdate(calenderType)
            closeDialog()
        }
    }

    fun makeGridCenter(binding: FragmentDayWeekMonthBinding?) {
        val layoutManager = FlexboxLayoutManager(context)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.CENTER
        layoutManager.alignItems = AlignItems.CENTER
        binding!!.rvDayMonthYear.layoutManager = layoutManager

    }

}


