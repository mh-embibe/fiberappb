package com.embibe.embibetvapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.application.App.Companion.context
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.APP_CLOSE_DIALOG
import com.embibe.embibetvapp.constant.AppConstants.LEAVE_APP
import com.embibe.embibetvapp.databinding.ActivityMainBinding
import com.embibe.embibetvapp.jobScheduler.MyInternetTest
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.fragment.achieve.DiscoverFragment2
import com.embibe.embibetvapp.ui.fragment.achieve.home.view.AchieveHostFragment
import com.embibe.embibetvapp.ui.fragment.learn.BannerHeaderFragment
import com.embibe.embibetvapp.ui.fragment.learn.LearnHostFragment
import com.embibe.embibetvapp.ui.fragment.navigation.NavMenuFragment
import com.embibe.embibetvapp.ui.fragment.practice.PracticeHostFragment
import com.embibe.embibetvapp.ui.fragment.signup.ParentProfileFragment
import com.embibe.embibetvapp.ui.fragment.test.TestHostFragment
import com.embibe.embibetvapp.ui.interfaces.*
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.MainViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibejio.coreapp.events.EventParams
import com.embibejio.coreapp.events.SegmentEvents
import com.embibejio.coreapp.events.SegmentIO
import com.embibejio.coreapp.preference.PreferenceHelper

class MainActivity : BaseFragmentActivity(), NavigationMenuCallback, FragmentChangeListener,
    NavigationStateListener, HostToNavigationListener, ErrorScreensBtnClickListener,
    MyInternetTest.NetworkSchedulerService.ConnectivityReceiverListener {

    private lateinit var activityMainBinding: ActivityMainBinding
    private lateinit var mainViewModel: MainViewModel

    private var navMenuFragment = NavMenuFragment()
    private var learnHostFragment = LearnHostFragment()
    private var practiceHostFragment = PracticeHostFragment()
    private var testHostFragment = TestHostFragment()
    private var profileFragment = ParentProfileFragment()
    lateinit var homeViewModel: HomeViewModel
    private var mLastKeyDownTime: Long = 0
    private var currentSelectedFragment = AppConstants.LEARN
    var preferenceHelper = PreferenceHelper()

    companion object {
        const val TAG = "MainActivity"
    }

    private var discoverFragment = DiscoverFragment2()

    private var achieveHomeFragment = AchieveHostFragment()

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        SegmentUtils.trackEventHomeStart()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            super.onCreate(savedInstanceState)

            activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
            mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
            homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
            SegmentUtils.trackEventHomeStart()
            supportFragmentManager.beginTransaction()
                .replace(activityMainBinding.navFragment.id, navMenuFragment).commit()

            setLearnHostFragment()
            loadPracticeDataFromApiAsync(true)
            loadTestDataFromApiAsync(true)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        removeRowsAndItemsPrefs()

    }

    private fun removeRowsAndItemsPrefs() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        val editor = sharedPref.edit()
        editor.remove(getString(R.string.last_selected_practice_row_item))
        editor.remove(getString(R.string.last_selected_practice_row))
        editor.remove(getString(R.string.last_selected_learn_row_item))
        editor.remove(getString(R.string.last_selected_learn_row))
        editor.remove(getString(R.string.last_selected_test_row))
        editor.remove(getString(R.string.last_selected_test_row_item))
        editor.remove(getString(R.string.last_selected_dashboard_row_item))
        editor.remove(getString(R.string.last_selected_dashboard_row))
        editor.remove(getString(R.string.last_selected_achieve_row))
        editor.remove(getString(R.string.last_selected_achieve_row_item))
        editor.apply()
    }

    private fun setLearnHostFragment() {
        activityMainBinding.flRow.visibility = View.GONE
        activityMainBinding.flHomeBanner.visibility = View.GONE
        activityMainBinding.flBannerBg.visibility = View.GONE
        activityMainBinding.flHost.visibility = View.VISIBLE
        setFragment(learnHostFragment, activityMainBinding.flHost.id)
    }

    override fun switchFragment(fragmentName: String) {
        SegmentUtils.trackEventHomeNavigationBarClick(fragmentName)
        when (fragmentName) {

            AppConstants.NAV_NAME_SEARCH -> {
                navMenuToggle(false)
                startActivity(Intent(this, SearchActivity::class.java))
            }
            AppConstants.NAV_NAME_LEARN -> {
                loadLearn()
            }
            AppConstants.NAV_NAME_PRACTICE -> {
                loadPractice()
            }
            AppConstants.NAV_NAME_TEST -> {
                loadTest()
            }
            AppConstants.NAV_NAME_PROFILE -> {
                loadProfile()
            }
            AppConstants.NAV_NAME_ACHIEVE -> {
                loadAchieve()
            }
        }
    }

    private fun loadAchieve() {
        lastSelectedFragment(currentSelectedFragment)
        currentSelectedFragment = AppConstants.ACHIEVE
        setFrameLayoutsVisibility(View.VISIBLE)
//        achieveFragment = AchieveFragment()
        discoverFragment = DiscoverFragment2()
        achieveHomeFragment = AchieveHostFragment()
        activityMainBinding.flHomeBanner.visibility = View.GONE
        activityMainBinding.flBannerBg.visibility = View.GONE
        if (UserData.getCurrentProfile()?.isAchieve!!) {
            setFragment(achieveHomeFragment, activityMainBinding.flRow.id)
        } else {
            setFragment(discoverFragment, activityMainBinding.flRow.id)
        }
    }

    private fun loadTest() {
        setFrameLayoutsVisibility(View.GONE)
        currentSelectedFragment = AppConstants.TEST
        testHostFragment = TestHostFragment()
        SegmentUtils.trackTestScreenLoadStart()
        setFragment(testHostFragment, activityMainBinding.flHost.id)
    }

    private fun loadProfile() {
        lastSelectedFragment(currentSelectedFragment)
        currentSelectedFragment = AppConstants.PROFILE
        setFrameLayoutsVisibility(View.VISIBLE)
        profileFragment = ParentProfileFragment()
        activityMainBinding.flHomeBanner.visibility = View.GONE
        activityMainBinding.flBannerBg.visibility = View.GONE
        setFragment(profileFragment, activityMainBinding.flRow.id)
    }

    private fun loadLearn() {
        setFrameLayoutsVisibility(View.GONE)
        currentSelectedFragment = AppConstants.LEARN
        learnHostFragment = LearnHostFragment()
        setFragment(learnHostFragment, activityMainBinding.flHost.id)
    }

    private fun loadPractice() {
        currentSelectedFragment = AppConstants.PRACTICE
        practiceHostFragment = PracticeHostFragment()
        setFrameLayoutsVisibility(View.GONE)
        setFragment(practiceHostFragment, activityMainBinding.flHost.id)
    }

    private fun trackEventMenuSelection(fragmentName: String) {
        val eventParams = EventParams()
        eventParams.selected_menu = fragmentName
        SegmentIO.getInstance().track(
            SegmentEvents.LOG_TYPE_MAIN_WINDOW,
            SegmentEvents.EVENT_MENU_SELECTION, eventParams
        )
    }

    private fun setFrameLayoutsVisibility(visibility: Int) {
        activityMainBinding.flRow.visibility = visibility
        activityMainBinding.flHomeBanner.visibility = visibility
        activityMainBinding.flBannerBg.visibility = visibility
        activityMainBinding.flHost.visibility = View.GONE - visibility
    }

    fun setFragment(fragment: Fragment, layoutResId: Int) {
        fragment.view?.clearFocus()
        supportFragmentManager.beginTransaction().replace(
            layoutResId,
            fragment
        ).commit()
    }

    override fun onAttachFragment(fragment: Fragment) {

        when (fragment) {
            is ParentProfileFragment -> {
                fragment.setNavigationMenuListener(this)
            }
            is NavMenuFragment -> {
                fragment.setFragmentChangeListener(this)
                fragment.setNavigationStateListener(this)
            }
            is BannerHeaderFragment -> {
                fragment.setNavigationMenuListener(this)
            }
            is PracticeHostFragment -> {
                fragment.setHostToNavigationListener(this)
            }
            is LearnHostFragment -> {
                fragment.setHostToNavigationListener(this)
            }
            is TestHostFragment -> {
                fragment.setHostToNavigationListener(this)
            }
            is DiscoverFragment2 -> {
                fragment.setNavigationMenuListener(this)
            }
            is AchieveHostFragment -> {
                fragment.setNavigationMenuListener(this)
                fragment.setHostToNavigationListener(this)
            }
        }
    }

    /**this
     * communication from right-side content to left-side navigation
     */

    override fun navMenuToggle(toShow: Boolean) {

        try {
            if (toShow) {
//                activityMainBinding.navFragment.setBackgroundResource(R.drawable.ic_nav_bg)
                activityMainBinding.navFragment.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.fastlane_background)
                )
                activityMainBinding.flRow.clearFocus()
                activityMainBinding.navFragment.requestFocus()
                navEnterAnimation()
                navMenuFragment.openNav()
            } else {
                activityMainBinding.navFragment.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.fastlane_background)
                )
                activityMainBinding.navFragment.clearFocus()
                activityMainBinding.flRow.requestFocus()
                navMenuFragment.closeNav()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun navEnterAnimation() {
        val animate = AnimationUtils.loadAnimation(context, R.anim.slide_in_left)
        activityMainBinding.navFragment.startAnimation(animate)
    }

    /**
     * communication from left-side navigation to right-side content
     */

    override fun onStateChanged(expanded: Boolean, lastSelectedMenu: String) {
        if (!expanded) {
            activityMainBinding.navFragment.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.fastlane_background
                )
            )
            activityMainBinding.navFragment.clearFocus()

            when (currentSelectedFragment) {
                AppConstants.LEARN -> {
                    learnHostFragment.restoreLastSelection(true)
                    learnHostFragment.resumeBannerSliding()
                }
                AppConstants.PRACTICE -> {
                    practiceHostFragment.restoreLastSelection(true)
                    practiceHostFragment.resumeBannerSliding()
                }
                AppConstants.TEST -> {
                    testHostFragment.restoreLastSelection(true)
                    testHostFragment.resumeBannerSliding()
                }
                AppConstants.PROFILE -> {
                    profileFragment.restoreLastSelection()
                }
                AppConstants.ACHIEVE -> {
                    if (UserData.getCurrentProfile()?.isAchieve!!) {
                        achieveHomeFragment.restoreLastSelection()
                    } else {
                        when {
                            discoverFragment.isReadinessViewActive() ->
                                DiscoverFragment2.readinessFragment.setFocusToLastItem()
                            discoverFragment.isAchieverViewActive() ->
                                discoverFragment.setFocusToAchiever()
                            else -> discoverFragment.restoreSelection()
                        }
                    }
                }
            }
        } else {
            when (currentSelectedFragment) {
              //  AppConstants.LEARN -> learnHostFragment.pauseBannerSliding()
              //  AppConstants.PRACTICE -> practiceHostFragment.pauseBannerSliding()
              //  AppConstants.TEST -> testHostFragment.pauseBannerSliding()
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {

        if (event.keyCode == KeyEvent.KEYCODE_SEARCH) {
            navMenuToggle(false)
            startActivity(Intent(this, SearchActivity::class.java))
            return true
        }
        val current = System.currentTimeMillis()
        var res = false
        if (current - mLastKeyDownTime < 300) {
            res = true
        } else {
            res = super.onKeyDown(keyCode, event)
            mLastKeyDownTime = current
        }
        return res
    }

    override fun onResume() {
        super.onResume()
        MyInternetTest.scheduleJob()
        App.setConnectivityListener(this)
        Log.i(TAG, "onResume ")
        SegmentUtils.trackEventHomeStart()
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        Log.e(TAG, "Network: $isConnected")
        /* if (!isConnected)*/
        /*Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT)
            .show()*/
    }

    override fun showNavigation(toShow: Boolean) {
        //todo: remove and keep relevant interfaces
        navMenuToggle(toShow)
    }

    override fun switchToPractice() {
        loadPractice()
        navMenuFragment.setSelectedMenu(AppConstants.NAV_NAME_PRACTICE)
    }

    override fun switchToLearn() {
        loadLearn()
        navMenuFragment.setSelectedMenu(AppConstants.NAV_NAME_LEARN)
    }

    override fun switchToTest() {
        loadTest()
        navMenuFragment.setSelectedMenu(AppConstants.NAV_NAME_TEST)
    }

    override fun onBackPressed() {
        if (discoverFragment.isReadinessViewActive() || discoverFragment.isSphereSelected())
            discoverFragment.handleBackPress()
        else {
            Utils.showAlertDialog(APP_CLOSE_DIALOG, supportFragmentManager, this)
        }
    }

    override fun screenUpdate(type: String) {
        when (type) {
            LEAVE_APP -> {
                finish()
                SegmentUtils.trackEventHomeEnd()
            }
        }
    }

    fun loadPracticeDataFromApiAsync(isAsync: Boolean) {
        if (!isAsync) {
            showProgress()
        }
        homeViewModel.homePracticeApi(object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
            override fun onSuccess(model: List<ResultsEntity>?) {
                if (model != null) {
                    homeViewModel.saveResultToDb(model, page = AppConstants.PRACTICE)
                }

            }

            override fun onFailed(code: Int, error: String, msg: String) {
                if (!isAsync) {
                    hideProgress()
                    if (Utils.isApiFailed(code)) {
                        Utils.showError(
                            context,
                            code,
                            object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: String) {
                                    loadPracticeDataFromApiAsync(isAsync)
                                }

                                override fun onDismiss() {

                                }
                            })

                    } else {
                        // showToast(error)
                    }
                }

            }
        })
    }

    fun loadTestDataFromApiAsync(isAsync: Boolean) {
        if (!isAsync) {
            showProgress()
        }
        homeViewModel.homeTest(object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
            override fun onSuccess(model: List<ResultsEntity>?) {
                if (model != null) {
                    homeViewModel.saveResultToDb(model, page = AppConstants.TEST)
                }

            }

            override fun onFailed(code: Int, error: String, msg: String) {
                if (!isAsync) {
                    hideProgress()
                    if (Utils.isApiFailed(code)) {
                        Utils.showError(
                            context,
                            code,
                            object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: String) {
                                    loadTestDataFromApiAsync(isAsync)
                                }

                                override fun onDismiss() {

                                }
                            })

                    } else {
                        //showToast(error)
                    }
                }

            }
        })
    }

    private fun lastSelectedFragment(fragmentName: String) {
        when (fragmentName) {
            AppConstants.LEARN -> {
                learnHostFragment.pauseBannerSliding()
            }
            AppConstants.PRACTICE -> {
                practiceHostFragment.pauseBannerSliding()
            }
            AppConstants.TEST -> {
                testHostFragment.pauseBannerSliding()
            }
        }
    }

}
