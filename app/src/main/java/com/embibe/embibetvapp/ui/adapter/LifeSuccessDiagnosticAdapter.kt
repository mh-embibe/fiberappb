package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemDiagnosticLifeSuccessBinding
import com.embibe.embibetvapp.model.diagnostic.LifeSucces
import com.embibe.embibetvapp.model.diagnostic.ReadninessData

class LifeSuccessDiagnosticAdapter(var context: Context) :
    RecyclerView.Adapter<LifeSuccessDiagnosticAdapter.ViewHolder>() {

    private var adapterList: List<LifeSucces> = ArrayList()
    private val childDiagnosticAdapter = ImprovementDiagnosticAdapter()

    var onItemClick: ((ReadninessData) -> Unit)? = null
        set(value) {
            field = value
            childDiagnosticAdapter.onItemClick = value
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemDiagnosticLifeSuccessBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.item_diagnostic_life_success,
            parent, false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = adapterList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(adapterList[position])
    }

    fun setData(list: List<LifeSucces>) {
        adapterList = list
        notifyDataSetChanged()
    }

    inner class ViewHolder(var binding: ItemDiagnosticLifeSuccessBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: LifeSucces) {
            if (item.tests.isNotEmpty()) {
//                binding.tvRvHeader.visibility = View.VISIBLE
                binding.rvItem.visibility = View.VISIBLE

//                binding.tvRvHeader.text = Utils.removeUnderScoreAndCapitalize(item.goal)
                setTestRecycler(binding, itemView.context, item.tests)
            } else {
                binding.tvRvHeader.visibility = View.GONE
                binding.rvItem.visibility = View.GONE
            }
        }
    }

    private fun setTestRecycler(
        binding: ItemDiagnosticLifeSuccessBinding,
        context: Context,
        item: List<ReadninessData>
    ) {
        binding.rvItem.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvItem.adapter = childDiagnosticAdapter
        childDiagnosticAdapter.setData(item)
    }
}


