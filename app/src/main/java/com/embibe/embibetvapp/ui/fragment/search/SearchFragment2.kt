package com.embibe.embibetvapp.ui.fragment.search


import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.speech.SpeechRecognizer
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.databinding.FragmentSearch2Binding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.UserData.getExamName
import com.embibe.embibetvapp.model.UserData.getGoal
import com.embibe.embibetvapp.model.UserData.getGrade
import com.embibe.embibetvapp.model.home.HomeModel
import com.embibe.embibetvapp.model.search.SearchResponse
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.ui.adapter.SimpleRVAdapter
import com.embibe.embibetvapp.ui.custom.CustomEasyTVKeyboard
import com.embibe.embibetvapp.ui.interfaces.*
import com.embibe.embibetvapp.ui.viewmodel.SearchViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.SpeechToTextConverter
import com.embibe.embibetvapp.utils.Utils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*


class SearchFragment2 : BaseAppFragment(), DPadKeysListener, SearchDetailToTvKeysListener,
    TvKeysToSearchRes,
    SearchResultsListener {

    private lateinit var binding: FragmentSearch2Binding
    private var searchDetailFragment: SearchDetailFragment? = null
    private lateinit var searchViewModel: SearchViewModel
    private lateinit var inputSuggestions: Array<String>
    private var TAG = this.javaClass.simpleName
    private var hostToNavigationListener: NavigationMenuCallback? = null
    private var searchQuery: String = ""
    private var searchResultSize: Int = 0
    private var speechRecognizer: SpeechRecognizer? = null
    private var speechIntent: Intent? = null
    private var speechTimer: CountDownTimer? = null
    private val speechTimerDuration = 10000L // 10 secs
    private var elapsedSpeechTimer = 0L
    private var speechToTextConverter: SpeechToTextConverter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeSpeechToText()
        startRecognition()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_2, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.customInputView.setFocusToSearchListener(this)
        binding.rvQuickLinks.layoutManager = LinearLayoutManager(context)

        setSearchTextChangeListener()
        micSearchButtonListeners()
    }

    private fun setSearchTextChangeListener() {
        binding.customInputView.setTextChangedListener(object :
            CustomEasyTVKeyboard.OnTextChangedListener {
            override fun onTextChanged(text: String?) {
                if (text != null && text.isNotEmpty()) {
                    searchQuery = Utils.convertFirstCharCapital(text)
                    performSearch(searchQuery)
                    SegmentUtils.trackEventSearchResultLoadStart(
                        searchQuery,
                        searchQuery.length.toString()
                    )
                } else {
                    binding.textSearch.setText("")
                    binding.textNoResult.visibility = View.GONE
                    binding.quickLinkText.visibility = View.INVISIBLE
                    binding.rvQuickLinks.visibility = View.INVISIBLE
                    binding.fragmentContainerView.visibility = View.GONE
                    searchDetailFragment?.clearAdapter()
                    noSuggestions()
                    noSearchResults()
                }
            }

        })

        binding.customInputView.setDpadKeysListener(this)
        hostToNavigationListener?.navMenuToggle(false)

    }

    private fun getSearchSuggestions(searchSuggestionQuery: String, size: String) {

        searchViewModel.getSearchSuggestions(
            searchSuggestionQuery, size,
            UserData.getCurrentProfile()!!.userId,
            getGrade(),
            getGoal(),
            getExamName(),
            object : BaseViewModel.APICallBacks<SearchResponse> {
                override fun onSuccess(searchResponse: SearchResponse?) {
                    if (searchResponse != null) {
                        if (!searchResponse.data!!.quicklinks.isNullOrEmpty()) {
                            setSuggestions(searchResponse.data.quicklinks)
                            setSuggestionAdapter(searchSuggestionQuery)
                        } else {
                            noSuggestions()
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    //showToast(error)
                }
            })
    }

    private fun setSuggestions(quickLinks: List<String>) {
        val newTempText: ArrayList<String> = arrayListOf()

        for (item in quickLinks) {
            if (item.isNotEmpty()) {
                newTempText.add(Utils.convertFirstCharCapital(item))
            }
        }

        inputSuggestions = newTempText.toArray(arrayOfNulls<String>(0))
    }

    private fun micSearchButtonListeners() {

        binding.micSearch.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.micSearch.setImageResource(R.mipmap.ic_mic_selected)
            } else {
                binding.micSearch.setImageResource(R.mipmap.ic_mic_unselected)
            }
        }

        binding.micSearch.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (elapsedSpeechTimer != 0L) {
                            stopRecognition()
                            speechTimer?.cancel()
                        }
                        binding.customInputView.restoreSelection("Mic")
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_DPAD_UP -> {
                        binding.micSearch.postDelayed({
                            binding.micSearch.requestFocus()
                        }, 0)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        startRecognition()
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        focusToSearch(true)
                    }
                    KeyEvent.KEYCODE_VOICE_ASSIST -> {
//                        Toast.makeText(context, "Voice assist pressed", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            false
        }
    }

    fun startRecognition() {
        if (Utils.hasPermission(requireContext(), Manifest.permission.RECORD_AUDIO)) {
            speechToTextConverter?.startListening()
            setSpeechRecognitionTimer()
        } else {
            Toast.makeText(
                context,
                requireContext().resources.getString(R.string.audio_permission_not),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun setSpeechRecognitionTimer() {
        elapsedSpeechTimer = 0L
        speechTimer = object : CountDownTimer(speechTimerDuration, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                elapsedSpeechTimer = (speechTimerDuration - millisUntilFinished) / 1000
                Log.i(TAG, "$elapsedSpeechTimer")
            }

            override fun onFinish() {
                finishVoiceSearch()
            }
        }
        speechTimer?.start()
    }

    private fun stopRecognition() {
        finishMicSearchAnimation()
        speechRecognizer?.cancel()
    }

    fun startMicSearchAnimation() {
        binding.micSearchLav.visibility = View.VISIBLE
        binding.micSearch.setImageResource(0)
        binding.micSearchLav.playAnimation()
    }

    private fun finishMicSearchAnimation() {
        binding.micSearchLav.cancelAnimation()
        binding.micSearchLav.visibility = View.INVISIBLE
        binding.micSearch.setImageResource(R.mipmap.ic_mic_selected)
    }

    private fun setSuggestionAdapter(searchSuggestionQuery: String) {
        val listSuggestions: ArrayList<String> = arrayListOf()
        for (suggestion in inputSuggestions) {
            listSuggestions.add(suggestion)
        }
        if (listSuggestions.isNotEmpty()) {
            makeLog("listSuggestions size : ${listSuggestions.size} $listSuggestions ")
            binding.customInputView.setQuickLinksSize(listSuggestions.size)
            binding.quickLinkText.visibility = View.VISIBLE
            binding.rvQuickLinks.visibility = View.VISIBLE
        } else {
            binding.quickLinkText.visibility = View.GONE
            binding.rvQuickLinks.visibility = View.GONE
        }

        val arraySuggestions: Array<String> = listSuggestions.toArray(arrayOfNulls<String>(0))
        var adapter = SimpleRVAdapter(arraySuggestions)
        binding.rvQuickLinks.adapter = adapter
        adapter.onItemClick = { text ->
            binding.textSearchVoice.text.clear()
            binding.textSearch.setText(text)
            getSuggestionsAndHitSearch(text, "50")
            searchDetailFragment?.setFlagToFocusSearch()
        }
    }

    private fun noSuggestions() {
        setSuggestions(arrayListOf())
        setSuggestionAdapter("")
        binding.customInputView.setQuickLinksSize(0)
    }

    private fun noSearchResults() {
        /*show the results not found UI here*/
        binding.textNoResult.visibility = View.VISIBLE
        binding.fragmentContainerView.visibility = View.GONE
    }

    private fun getSuggestionsAndHitSearch(searchQuery: String, size: String) {
        SegmentUtils.trackEventSearch(searchQuery)
        var mergeCallback = object : BaseViewModel.APIMergeCallBack {
            override fun onSuccessCallBack(it: Any) {
                var response = it as HomeModel

                if (searchQuery.isNotEmpty()) {
                    if (response.quickLinks != null && response.quickLinks?.isNotEmpty()!!) {
                        setSuggestions(response.quickLinks ?: arrayListOf())
                        setSuggestionAdapter(searchQuery)
                        SegmentUtils.trackEventSearchResultLoadEnd(response.quickLinks?.size.toString())

                    } else {
                        noSuggestions()
                    }
                    if (response.searchResults != null && response.searchResults?.isNotEmpty()!!) {
                        if (!isContentPresent(response.searchResults ?: arrayListOf())) {
                            noSearchResults()
                        } else {
                            binding.textNoResult.visibility = View.GONE
                            binding.fragmentContainerView.visibility = View.VISIBLE
                            moveToSearch(response.searchResults!!, "")

                        }
                    } else {
                        noSearchResults()
                    }
                } else {
                    noSearchResults()
                    noSuggestions()
                }

            }

            override fun onErrorCallBack(it: Throwable) {
                /*hide ProgressBar here */
                /*hideProgress()*/
                //App.context.toast(it.localizedMessage ?: "Error")
            }
        }
        /*showProgress()*/
        searchViewModel.combineSearchResults(
            searchQuery, size,
            UserData.getCurrentProfile()!!.userId.toString(),
            getGrade(),
            getGoal(),
            getExamName(),
            mergeCallback
        )
    }

    private fun isContentPresent(searchResults: List<Results>): Boolean {
        var isContentPresent = false
        for (index in searchResults.indices) {
            if (searchResults[index].content.isNotEmpty()) return true
        }
        return isContentPresent
    }

    private fun moveToSearch(results: List<Results>?, text: String) {

        searchDetailFragment =
            SearchDetailFragment()
        searchDetailFragment?.setSearchDetailToTvListener(this)
        searchDetailFragment?.setSearchResultsListener(this)
        val bundle = Bundle()

        if (results != null && results.isNotEmpty()) {
            bundle.putParcelableArrayList("results", results as ArrayList<Results>)
            searchResultSize = results.size
        } else {
            searchResultSize = 0
        }

        searchDetailFragment?.arguments = bundle
        childFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainerView, searchDetailFragment!!)
            .commit()

    }


    fun restoreLastSelection() {
        binding.customInputView.restoreSelection("Nav")
    }

    override fun forceFocusToKeys(setFocus: Boolean) {
        if (setFocus) {
            CoroutineScope(Dispatchers.Main).launch {
                delay(2)
                binding.micSearch.requestFocus()
            }
        }
    }

    override fun focusToSearch(setFocus: Boolean) {
        if (setFocus && searchResultSize > 0) {
            searchDetailFragment?.setFocusToDetailRv()
        }
    }

    override fun searchResultsCount(count: Int) {
        binding.customInputView.setSearchResultsSize(count)
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.hostToNavigationListener = callback
    }

    override fun onPause() {
        speechToTextConverter?.releaseListener(false)
        super.onPause()
    }

    override fun onStop() {
        speechTimer?.cancel()
        speechToTextConverter?.releaseListener(true)
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        speechToTextConverter?.setRecognitionListener()
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {

    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
        if (isLeft) {
            when (from) {
                "Keyboard", "SpaceBarButton" -> {
                    hostToNavigationListener?.navMenuToggle(isLeft)
                }
            }
        }
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {
        if (isRight) {
            when (from) {
                "BackSpaceButton" -> {
                    binding.micSearch.postDelayed({
                        binding.micSearch.requestFocus()
                    }, 0)
                }
            }
        }
    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {}

    private fun performSearch(searchQuery: String) {
        binding.textSearchVoice.text.clear()
        //bind search text to textSearch text view
        binding.textSearch.setText(searchQuery)

        /*api call for load suggestions only*/
        getSearchSuggestions(searchQuery, "10")
        /*api call for load suggestions with search results*/
        getSuggestionsAndHitSearch(searchQuery, "10")
    }

    private fun performVoiceSearch(searchQuery: String) {
        binding.textSearch.text.clear()
        binding.textNoResult.visibility = View.INVISIBLE
        //bind search text to textSearch text view
        binding.textSearchVoice.setText(searchQuery)

        /*api call for load suggestions only*/
        getSearchSuggestions(searchQuery, "10")
        /*api call for load suggestions with search results*/
        getSuggestionsAndHitSearch(searchQuery, "10")
    }

    fun finishVoiceSearch() {
//        speechToTextConverter?.stopListening()
        binding.micSearchLav.cancelAnimation()
        binding.micSearchLav.visibility = View.INVISIBLE
        binding.micSearch.setImageResource(R.mipmap.ic_mic_selected)
        speechTimer?.cancel()
    }

    //not using currently
    private fun initializeSpeechToText() {
        speechToTextConverter = SpeechToTextConverter(object : ConversionCallback {
            override fun onReady() {
                Log.i(TAG, "On Ready")
                startMicSearchAnimation()
            }

            override fun onSuccess(result: ArrayList<String>?) {
                Log.i(TAG, "On Success")
                if (result!!.isNotEmpty()) {
                    val searchQuery = result[0].toLowerCase(Locale.ENGLISH)
                    binding.textSearch.text.clear()
                    performVoiceSearch(searchQuery)
                }
                finishVoiceSearch()
            }

            override fun onCompletion() {
                Log.i(TAG, "On Completion")
            }

            override fun onErrorOccurred(errorMessage: String) {
                Log.i(TAG, "On Error $errorMessage")
                finishVoiceSearch()
            }

        })

        speechToTextConverter?.initialize(activity)
    }


}






