package com.embibe.embibetvapp.ui.fragment.practice

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentTopicForPracticeBinding
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.activity.BookDetailActivity
import com.embibe.embibetvapp.ui.activity.DetailActivity
import com.embibe.embibetvapp.ui.activity.PracticeActivity
import com.embibe.embibetvapp.ui.adapter.TopicsForPracticeAdapter
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.unity.UnityPlayerActivity
import com.embibe.embibetvapp.utils.Utils

class TopicsForPracticeFragment(data: List<Content>) : BaseAppFragment() {

    private lateinit var binding: FragmentTopicForPracticeBinding
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var practiceDetailAdapter: TopicsForPracticeAdapter
    private var listData = data

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_topic_for_practice,
                container,
                false
            )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        setPracticeRecycler(view)
    }

    private fun setPracticeRecycler(view: View) {
        practiceDetailAdapter = TopicsForPracticeAdapter()
        binding.recyclerPractice.layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerPractice.adapter = practiceDetailAdapter
        practiceDetailAdapter.setData(listData as ArrayList<Content>)
        practiceDetailAdapter.onItemClick = { content ->
            startActivityByType(content)
        }
    }

    private fun startActivityByType(content: Content) {

        when ((content.type).toLowerCase()) {
            AppConstants.TYPE_VIDEO -> {
                startDetailActivity(content)
            }
            AppConstants.TYPE_COOBO -> {
                callCoobo(content)
            }
            AppConstants.TYPE_BOOK -> {
                startBookDetailActivity(content)
            }
            AppConstants.TYPE_PRACTICE, AppConstants.TYPE_CHAPTER -> {
                startPracticeDetailActivity(content)
            }
        }
    }

    private fun startDetailActivity(content: Content) {
        Utils.loadConceptsAsync(content, context)
        val intent = Intent(App.context, DetailActivity::class.java)
        Utils.insertBundle(content, intent)
        startActivity(intent)
    }

    private fun startBookDetailActivity(content: Content) {
        var intent = Intent(App.context, BookDetailActivity::class.java)
        intent.putExtra(AppConstants.NAV_NAME_TYPE, AppConstants.NAV_TYPE_BOOK)
        Utils.insertBundle(content, intent)
        startActivity(intent)
    }

    private fun startPracticeDetailActivity(data: Content) {
        val intent = Intent(requireView().context, PracticeActivity::class.java)
        intent.putExtra("id", data.id)
        intent.putExtra("title", data.title)
        intent.putExtra("subject", data.subject)
        intent.putExtra(AppConstants.TOPIC_LEARN_PATH, data.learning_map.topicLearnPathName)
        intent.putExtra(AppConstants.CONCEPT_ID, data.learning_map.conceptId)
        intent.putExtra(AppConstants.LENGTH, data.length)
        intent.putExtra(AppConstants.FORMAT_ID, data.learning_map.format_id)
        intent.putExtra(AppConstants.LM_NAME, data.learnmap_id)
        intent.putExtra(AppConstants.PRACTICE_MODE, AppConstants.PracticeModes.Normal)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, data.learnpath_name)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, data.learnpath_format_name)

        try {
            val parts = data.learning_map.lpcode.split("--")
            val lmCode = parts[parts.size - 1]
            val lmLevel = Utils.getLevelUsingCode(lmCode)
            val examCode = Utils.getExamLevelCode(parts)
            intent.putExtra("lm_level", lmLevel)
            intent.putExtra("lm_code", lmCode)
            intent.putExtra("exam_code", examCode)
            intent.putExtra(AppConstants.PRACTICE_MODE, AppConstants.PracticeModes.Normal)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        startActivity(intent)
    }


    private fun callCoobo(data: Content) {
        val i = Intent(requireContext(), UnityPlayerActivity::class.java)
        i.putExtra(AppConstants.COOBO_URL, data.url)
        i.putExtra(AppConstants.DETAILS_ID, data.id)
        i.putExtra(AppConstants.DURATION_LENGTH, data.length)
        i.putExtra(AppConstants.IS_RECOMMENDATION_ENABLED, true)
        Utils.insertBundle(data, i)
        startActivity(i)
    }
}
