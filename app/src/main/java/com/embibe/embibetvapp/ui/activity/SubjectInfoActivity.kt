package com.embibe.embibetvapp.ui.activity

import android.os.Bundle
import android.view.KeyEvent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.databinding.ActivitySubjectInfoBinding
import com.embibe.embibetvapp.ui.fragment.learn.SubjectInfoFragment
import com.embibe.embibetvapp.ui.viewmodel.MainViewModel

class SubjectInfoActivity : BaseFragmentActivity() {

    private lateinit var subjectInfoBinding: ActivitySubjectInfoBinding
    private lateinit var mainViewModel: MainViewModel

    private var mLastKeyDownTime: Long = 0
    private var subjectInfoFragment = SubjectInfoFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        subjectInfoBinding = DataBindingUtil.setContentView(this, R.layout.activity_subject_info)
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        subjectInfoFragment.arguments = intent.extras
        setFragment(subjectInfoFragment, subjectInfoBinding.flRow.id)
    }

    private fun setFragment(fragment: Fragment, layoutResId: Int) {
        fragment.view?.clearFocus()
        supportFragmentManager.beginTransaction().replace(layoutResId, fragment).commit()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        val current = System.currentTimeMillis()
        var res = false
        if (current - mLastKeyDownTime < 300) {
            res = true
        } else {
            res = super.onKeyDown(keyCode, event)
            mLastKeyDownTime = current
        }
        return res
    }
}
