package com.embibe.embibetvapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.utils.FontHelper
import java.util.ArrayList

class SearchExolanersAdapter(private val inputString: ArrayList<String>) :
    RecyclerView.Adapter<SearchExolanersAdapter.SimpleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchExolanersAdapter.SimpleViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_rv_explaners, parent,
                false
            )
        return SimpleViewHolder(v)
    }

    override fun onBindViewHolder(holder: SearchExolanersAdapter.SimpleViewHolder, position: Int) {
        holder.txtName?.text = inputString[position]
    }

    override fun getItemCount(): Int {
        return inputString.size
    }

    inner class SimpleViewHolder(itemView: View?) :
        RecyclerView.ViewHolder(itemView!!) {
        val txtName = itemView?.findViewById<TextView>(R.id.text_explaners)

        init {

            txtName?.setOnFocusChangeListener { v, hasFocus ->
                when (hasFocus) {
                    true -> {
                        FontHelper().setFontFace(FontHelper.FontType.GILROY_BOLD, txtName)
                    }
                    false -> {
                        FontHelper().setFontFace(FontHelper.FontType.GILROY_BOLD, txtName)
                    }

                }
            }
        }
    }
}