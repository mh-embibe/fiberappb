package com.embibe.embibetvapp.ui.fragment.practice

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.VolleyError
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentPracticeDetailBinding
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.activity.BookDetailActivity
import com.embibe.embibetvapp.ui.activity.PracticeActivity
import com.embibe.embibetvapp.ui.activity.TestActivity
import com.embibe.embibetvapp.ui.activity.VideoPlayerActivity
import com.embibe.embibetvapp.ui.adapter.PracticeSummaryAdapter
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.SearchViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.VideoUtils
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


class FragmentPracticeDetail : BaseAppFragment() {
    private lateinit var practiceDetailType: String
    private lateinit var binding: FragmentPracticeDetailBinding
    private lateinit var homeViewModel: HomeViewModel
    private var listData = ArrayList<Content>()
    private lateinit var searchViewModel: SearchViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_practice_detail, container, false)
        listData = requireArguments().getParcelableArrayList(AppConstants.CONTENT)!!
        practiceDetailType = requireArguments().getString(AppConstants.PRACTICE_DETAIL_TYPE)!!
        SegmentUtils.trackPracticeSummaryScreenAvailableOptionContentLoadStart(practiceDetailType)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        binding.recyclerPractice.visibility = View.GONE
        setAdapter()
    }

    override fun onResume() {
        super.onResume()
        SegmentUtils.trackPracticeSummaryScreenAvailableOptionContentLoadEnd(practiceDetailType)
    }

    private fun setAdapter() {
        binding.recyclerPractice.visibility = View.VISIBLE
        when (practiceDetailType) {
            AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE -> {
                setBookAdapter()
            }
            AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING -> {
                setOtherAdapter()
            }
            AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE -> {
                setOtherAdapter()
            }
            AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER -> {
                setOtherAdapter()
            }
        }
    }

    private fun setBookAdapter() {
        SegmentUtils.trackPracticeSummaryScreenSearchLoadEnd()
        var bookAvailableAdapter = PracticeSummaryAdapter(requireActivity())
        binding.recyclerPractice.layoutManager =
            GridLayoutManager(requireActivity(), 3)
        binding.recyclerPractice.adapter = bookAvailableAdapter
        bookAvailableAdapter.setData(listData, practiceDetailType)
        bookAvailableAdapter.onItemClick = { content ->
            startActivityByType(content)
        }
    }

    private fun setOtherAdapter() {
        SegmentUtils.trackPracticeSummaryScreenSearchLoadEnd()
        var practiceSummaryAdapter = PracticeSummaryAdapter(requireActivity())
        binding.recyclerPractice.layoutManager =
            LinearLayoutManager(requireActivity())
        binding.recyclerPractice.adapter = practiceSummaryAdapter
        practiceSummaryAdapter.setData(listData, practiceDetailType)
        practiceSummaryAdapter.onItemClick = { content ->
            startActivityByType(content)
        }

    }


    private fun startActivityByType(content: Content) {
        when ((content.type).toLowerCase()) {
            AppConstants.TYPE_VIDEO -> {
                startVideoActivity(content)
            }
            AppConstants.TYPE_BOOK -> {
                startBookDetailActivity(content)
            }
            AppConstants.TEST -> {
                startTestScreen(content)
            }
            AppConstants.TYPE_PRACTICE, AppConstants.TYPE_CHAPTER, AppConstants.TYPE_TOPIC -> {
                startPracticeDetailActivity(content)
            }
        }
    }


    private fun startTestScreen(data: Content) {
        val testIntent = Intent(requireContext(), TestActivity::class.java)
        testIntent.putExtra(AppConstants.TEST_XPATH, data.xpath)
        testIntent.putExtra(AppConstants.TEST_CODE, data.bundle_id)
        testIntent.putExtra(AppConstants.CONCEPT_ID, data.learning_map.conceptId)
        testIntent.putExtra(AppConstants.TEST, data.bundle_id)
        testIntent.putExtra(AppConstants.TEST_NAME, data.title)
        startActivity(testIntent)
    }

    private fun startVideoActivity(content: Content) {
        doAsync {
            val url = content.url
            val type = Utils.getVideoTypeUsingUrl(content.url)
            val title = content.title
            val id = content.id
            var conceptId = ""
            var subject = ""
            var chapter = ""
            var totalDuration = ""
            var topicLearnPath = ""
            conceptId = content.learning_map.conceptId
            chapter = content.learning_map.chapter
            subject = content.learning_map.subject
            topicLearnPath = content.learning_map.topicLearnPathName
            totalDuration = content.length.toString()
            uiThread {
                hideProgress()
                callVideo(
                    id,
                    AppConstants.VIDEO,
                    conceptId,
                    url,
                    title,
                    "",
                    type.toLowerCase(),
                    subject,
                    chapter,
                    "",
                    totalDuration,
                    topicLearnPath,
                    content
                )
            }
        }
    }

    private fun startBookDetailActivity(content: Content) {
        var intent = Intent(App.context, BookDetailActivity::class.java)
        intent.putExtra(AppConstants.NAV_NAME_TYPE, AppConstants.NAV_TYPE_BOOK)
        Utils.insertBundle(content, intent)
        startActivity(intent)
    }

    private fun startPracticeDetailActivity(data: Content) {

        val intent = Intent(requireView().context, PracticeActivity::class.java)
        intent.putExtra("id", data.id)
        intent.putExtra("title", data.title)
        intent.putExtra("subject", data.subject)
        intent.putExtra(AppConstants.TOPIC_LEARN_PATH, data.learning_map.topicLearnPathName)
        intent.putExtra(AppConstants.FORMAT_ID, data.learning_map.format_id)
        intent.putExtra(AppConstants.LM_NAME, data.learnmap_id)
        intent.putExtra(AppConstants.PRACTICE_MODE, AppConstants.PracticeModes.Normal)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, data.learnpath_name)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, data.learnpath_format_name)
        try {
            val parts = data.learning_map.lpcode.split("--")
            val lmCode = parts[parts.size - 1]
            val lmLevel = Utils.getLevelUsingCode(lmCode)
            val examCode = Utils.getExamLevelCode(parts)
            intent.putExtra("lm_level", lmLevel)
            intent.putExtra("lm_code", lmCode)
            intent.putExtra("exam_code", examCode)
            intent.putExtra(AppConstants.PRACTICE_MODE, AppConstants.PracticeModes.Normal)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        startActivity(intent)
    }

    private fun callVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        videoTotalDuration: String,
        topicLearnpath: String,
        content: Content
    ) {
        if (source == AppConstants.PLAYER_TYPE_YOUTUBE) {
            if (Utils.isYouTubeAppAvailable(requireContext())) {
                Utils.startYoutubeApp(requireContext(), url)
            } else {
                startVideo(
                    id,
                    type,
                    conceptId,
                    url,
                    title,
                    description,
                    source,
                    subject,
                    chapter,
                    authors,
                    videoTotalDuration,
                    topicLearnpath,
                    content
                )
            }
        } else {
            val callback = object : BaseViewModel.APICallBackVolley {
                override fun <T> onSuccessCallBack(response: T) {
                    hideProgress()
                    response as JSONObject
                    val playableUrl = Utils.getVimeoHD((response["files"] as JSONArray))
                    startVideo(
                        id,
                        type,
                        conceptId,
                        playableUrl,
                        title,
                        description,
                        AppConstants.PLAYER_TYPE_EXOPLAYER,
                        subject,
                        chapter,
                        authors,
                        videoTotalDuration,
                        topicLearnpath,
                        content
                    )
                    /*hide ProgressBar here */

                }

                override fun onErrorCallBack(e: VolleyError) {
                    try {
                        makeLog("Error:  $e")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    /*hide ProgressBar here */
                    hideProgress()
                }
            }
            showProgress()
            val videoId = VideoUtils.getVimeoVideoId(url).toString()
            Utils.getVideoURL(requireActivity(), videoId, content.owner_info.key, callback)
        }

    }

    fun startVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        videoTotalDuration: String,
        topicLearnpath: String,
        data: Content
    ) {
        val intent = Intent(App.context, VideoPlayerActivity::class.java)

        intent.putExtra(AppConstants.VIDEO_ID, id)
        intent.putExtra(AppConstants.CONTENT_TYPE, type)
        intent.putExtra(AppConstants.VIDEO_CONCEPT_ID, conceptId)
        intent.putExtra(AppConstants.VIDEO_URL, url)
        intent.putExtra(AppConstants.VIDEO_TITLE, title)
        intent.putExtra(AppConstants.VIDEO_DESCRIPTION, description)
        intent.putExtra(AppConstants.VIDEO_SUBJECT, subject)
        intent.putExtra(AppConstants.VIDEO_CHAPTER, chapter)
        intent.putExtra(AppConstants.VIDEO_AUTHORS, authors)
        intent.putExtra(AppConstants.SOURCE_REFERENCE, AppConstants.PRACTICE)
        intent.putExtra(AppConstants.VIDEO_SOURCE, source)
        intent.putExtra(AppConstants.TOPIC_LEARN_PATH, topicLearnpath)
        intent.putExtra(AppConstants.IS_RECOMMENDATION_ENABLED, true)
        intent.putExtra(AppConstants.VIDEO_TOTAL_DURATION, videoTotalDuration)
        intent.putExtra(AppConstants.FORMAT_ID, data.learning_map.format_id)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, data.learnpath_name)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, data.learnpath_format_name)
        startActivity(intent)
    }

}

