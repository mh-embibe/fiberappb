package com.embibe.embibetvapp.ui.progressBar

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import com.embibe.embibetvapp.application.App.Companion.context
import com.embibe.embibetvapp.ui.activity.Loader
import com.embibe.embibetvapp.utils.data.DataManager


interface LoadingManager : View.OnKeyListener {


    fun setContext(context: Context) {
        //sContext = context
    }

    companion object {
        var isLoading: Boolean = false
        var activity: Loader? = null


        /*control Dpad keys while loading*/
        var listener = object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (keyCode == KeyEvent.KEYCODE_HOME) {
                    if (event!!.action != KeyEvent.ACTION_DOWN) {
                        return true
                    }
                    return false
                }
                return true
            }
        }

        /*focusing view and set keylistener*/
        fun setDPadListener(view: View) {
            view.isFocusableInTouchMode = true
            view.requestFocus()
            view.setOnKeyListener(listener)
        }

        /*focusing view and set keylistener*/
        fun removeDPadListener(view: View) {
            view.isFocusableInTouchMode = true
            view.requestFocus()
            view.setOnKeyListener(null)
        }


    }


    /*show progress*/
    fun showProgress() {
        if (!DataManager.isLoading) {
            DataManager.isLoading = true
            val intent = Intent(context, Loader::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
            //startActivityForResult(sContext!! as BaseFragmentActivity,intent,1,null)
            //(sContext as BaseFragmentActivity).overridePendingTransition(0,0 )
        }
    }

    /*show progress*/
    fun hideProgress(sec: Long = 500) {
        Handler().postDelayed({
            if (activity != null) {
                removeDPadListener(Loader.progressView!!)
                activity!!.hideActivity()
            }
        }, sec)

    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            if (event!!.action != KeyEvent.ACTION_DOWN) {
                return false
            }
            return false
        }
        return true
    }


}