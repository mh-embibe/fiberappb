package com.embibe.embibetvapp.ui.fragment.editUser

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.databinding.FragmentPrimaryGoalsBinding
import com.embibe.embibetvapp.ui.activity.AddGoalsExamsActivity
import com.embibe.embibetvapp.ui.activity.AddGoalsExamsActivity.Companion.primaryGoalCode
import com.embibe.embibetvapp.ui.adapter.PrimaryGoalAdapter
import com.embibe.embibetvapp.utils.data.DataManager
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent


class PrimaryGoalsFragment : BaseAppFragment() {

    private lateinit var binding: FragmentPrimaryGoalsBinding
    private val fragment = PrimaryGoalsExamFragment()
    private lateinit var goalAdapter: PrimaryGoalAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_primary_goals, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        primaryGoalCode = ""
        setGoalRecyclerView(view)
        setBtnClickListener()
    }

    private fun setGoalRecyclerView(view: View) {
        var data = DataManager.instance.getPrimaryGoals()
        goalAdapter = PrimaryGoalAdapter(view.context)

        makeGridCenter()
        goalAdapter.setData(data)


        binding.rvPrimaryGoal.adapter = goalAdapter

        goalAdapter.onItemClick = { goal ->
            if (goal.supported) {
                AddGoalsExamsActivity.primaryGoal = goal.name
                primaryGoalCode = goal.code
            }
            if (!goal.supported) {
                binding.textGoalNotSupported.visibility = View.VISIBLE
                binding.textGoalNotSupported.text =
                    "${resources.getString(R.string.text_goal_not_supported)} ${goal.name}"
            } else {
                binding.textGoalNotSupported.visibility = View.GONE
            }
        }

        goalAdapter.onFocusChange = { goal, hasFocus ->
            if (!hasFocus) {
                binding.textGoalNotSupported.visibility = View.GONE
            }
        }

        binding.rvPrimaryGoal.requestFocus()
    }

    private fun makeGridCenter() {
        val layoutManager = FlexboxLayoutManager(context)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.CENTER
        layoutManager.alignItems = AlignItems.CENTER
        binding.rvPrimaryGoal.layoutManager = layoutManager

    }


    private fun setBtnClickListener() {
        binding.btnContinue.setOnClickListener {
            if (goalAdapter.selectedItem != -1) {
                (activity as AddGoalsExamsActivity).setPrimaryGoalExam(
                    fragment,
                    "primary_goals_exam_fragment"
                )
            } else {
                showToast(resources.getString(R.string.please_select_goal))
            }
        }
    }

    private fun isValidGoal(): Boolean {
        return primaryGoalCode.isNotEmpty()
    }
}