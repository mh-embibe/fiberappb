package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.graphics.drawable.Animatable
import android.graphics.drawable.GradientDrawable
import android.os.SystemClock
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.*
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser

class SearchDetailAdapter(private val mContext: Context) :
    RecyclerView.Adapter<SearchDetailAdapter.ItemViewHolder<*>>() {
    var list: ArrayList<Content> = ArrayList()
    val TAG = this.javaClass.name
    var onItemClick: ((Content) -> Unit)? = null
    var onItemFocused: ((Int) -> Unit?)? = null
    var onItemDpadHit: ((Int) -> Unit)? = null
    var onItemDpadUpHit: ((Int) -> Unit)? = null
    var lastKeyPress: Long = 0L
    var coroutineScope = CoroutineScope(Dispatchers.Main)

    object Values {
        const val maxLines = 3
    }

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_CARD = 1
        private const val TYPE_BOOK = 2
        private const val TYPE_PRACTICE = 3
        private const val TYPE_TEST = 4
        private const val TYPE_GIF = 5

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder<Content> {
        return when (viewType) {
            TYPE_HEADER -> {
                val binding: ItemDetailHeaderBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_detail_header,
                    parent,
                    false
                )
                HeaderViewHolder(binding)
            }
            TYPE_CARD -> {
                val binding: ItemDetailCardBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_detail_card,
                    parent,
                    false
                )
                CardViewHolder(binding)
            }
            TYPE_BOOK -> {
                val binding: ItemDetailBookNoTextsBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_detail_book_no_texts,
                    parent,
                    false
                )
                BookViewHolder(binding)
            }
            TYPE_GIF -> {
                val binding: ItemDetailCardGifBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_detail_card_gif, parent, false
                )
                GifCardViewHolder(binding)
            }
            TYPE_PRACTICE -> {
                val binding: ItemDetailPracticeNoHighlightBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_detail_practice_no_highlight,
                    parent,
                    false
                )
                PracticeViewHolder(binding)
            }
            TYPE_TEST -> {
                val binding: ItemDetailFiberTestBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_detail_fiber_test,
                    parent,
                    false
                )
                TestViewHolder(binding)
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder<*>, position: Int) {
        val item = list.get(position)
        when (holder) {
            is HeaderViewHolder -> holder.bind(item)
            is CardViewHolder -> holder.bind(item)
            is BookViewHolder -> holder.bind(item)
            is PracticeViewHolder -> holder.bind(item)
            is TestViewHolder -> holder.bind(item)
            is GifCardViewHolder -> holder.bind(item)
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemViewType(position: Int): Int {
        var value: Int = 0
        var item: Content = list[position]
//        Log.i(TAG, "${item.type}")
        value = when (item.type.toLowerCase()) {
            AppConstants.TYPE_HEADER -> TYPE_HEADER
            AppConstants.TYPE_VIDEO -> {
                return if (item.preview_url.isNullOrEmpty()) {
                    TYPE_CARD
                } else {
                    TYPE_GIF
                }
            }
            AppConstants.TYPE_CARD, AppConstants.TYPE_COOBO -> TYPE_CARD
            AppConstants.TYPE_BOOK -> TYPE_BOOK
            AppConstants.TYPE_PRACTICE, AppConstants.TYPE_CHAPTER -> TYPE_PRACTICE
            AppConstants.TYPE_TEST -> TYPE_TEST
            else -> TYPE_PRACTICE
        }
        return value
    }

    fun setData(data: ArrayList<Content>) {
        list = data
//        Log.i(TAG, "${list.size}")
        notifyDataSetChanged()
    }

    abstract class ItemViewHolder<Content>(binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        abstract fun bind(item: Content)
    }

    inner class HeaderViewHolder(var binding: ItemDetailHeaderBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {

            if (list.isNotEmpty()) {
                binding.contentModel = item
                binding.tvHeader.visibility = View.VISIBLE
            } else
                binding.tvHeader.visibility = View.GONE
        }

        init {
            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    onItemFocused?.invoke(adapterPosition)
                    SegmentUtils.trackEventSearchTileCaroselFocus(
                        itemView.verticalScrollbarPosition.toString(),
                        TYPE_HEADER.toString(),
                        JSONParser().parse(Gson().toJson(binding.tvHeader)) as JSONObject,
                        "Search",
                        "Header"
                    )
                }
            }
        }
    }

    inner class GifCardViewHolder(var binding: ItemDetailCardGifBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.layout.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    binding.ivGifView.visibility = View.VISIBLE
                    val controller = Fresco.newDraweeControllerBuilder()
                    controller.autoPlayAnimations = true
                    controller.setUri(item.preview_url)
                    controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                        override fun onFinalImageSet(
                            id: String?,
                            imageInfo: ImageInfo?,
                            animatable: Animatable?
                        ) {
                            val anim = animatable as AnimatedDrawable2
                            anim.setAnimationListener(object : AnimationListener {
                                override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                                override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                                    binding.ivImg.visibility = View.INVISIBLE
                                }

                                override fun onAnimationFrame(
                                    drawable: AnimatedDrawable2?,
                                    frameNumber: Int
                                ) {

                                }

                                override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                                    binding.ivImg.visibility = View.VISIBLE
                                    binding.ivGifView.visibility = View.INVISIBLE
                                }

                                override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                            })
                        }
                    }
                    binding.ivGifView.controller = controller.build()

                } else {
                    binding.ivImg.visibility = View.VISIBLE
                    binding.ivGifView.visibility = View.INVISIBLE
                }
            }
            binding.detailItem = item
            Glide.with(mContext).load(item.owner_info.copy_logo).into(binding.ivEmbibeLogo)
            if (item.type != "header") {
                if (item.category_thumb.isNotEmpty()) {
                    binding.ivCategory.visibility = View.VISIBLE
                    Glide.with(itemView.context)
                        .load(item.category_thumb)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(binding.ivCategory)
                } else {
                    binding.ivCategory.visibility = View.GONE
                }

                binding.textTime.text = Utils.convertIntoMins(item.length)
            }

            binding.tvSubjectName.visibility = View.GONE

            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            if (item.thumb == "thumb" || item.thumb == "") {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            } else {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(item.thumb)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            }
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
                var item: Content? = binding.detailItem
                if (item != null) {
                    SegmentUtils.trackEventMoreInfoMenuItemTileClick(item)
                }
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(adapterPosition)
                var item: Content? = binding.detailItem
                if (item != null) {
                    SegmentUtils.trackEventMoreInfoMenuItemTileFocus(item)
/*
                    SegmentUtils.trackEventSearchTileCaroselFocus()
*/
                }

            }

            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            onItemDpadHit?.invoke(adapterPosition)
                        }
                    }

                }

                false
            }
        }
    }

    inner class CardViewHolder(var binding: ItemDetailCardBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
            setDynamicDetails(item)
            Glide.with(itemView.context)
                .load(item.owner_info.copy_logo)
                .into(binding.ivEmbibeLogo)

            if (item.category_thumb.isNotEmpty()) {
                binding.ivCategory.visibility = View.VISIBLE
                Glide.with(itemView.context)
                    .load(item.category_thumb)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(binding.ivCategory)
            } else {
                binding.ivCategory.visibility = View.GONE
            }

            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            if (item.thumb == "thumb" || item.thumb == "") {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.video_placeholder)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(binding.ivImg)
            } else {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(item.thumb)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            }
        }

        private fun setDynamicDetails(item: Content) {
            binding.textTime.text = Utils.convertIntoMins(item.length)
            if (item.subject.isNotEmpty()) {
                binding.tvSubjectName.visibility = View.VISIBLE
                binding.tvSubjectName.text = item.subject
                (binding.tvSubjectName.background as GradientDrawable).setColor(
                    itemView.context.resources.getColor(
                        Utils.getColorCode(
                            item.subject
                        )
                    )
                )
            } else {
                binding.tvSubjectName.visibility = View.GONE
            }
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])

                SegmentUtils.trackEventSearchTileCaroselClick(
                    itemView.verticalScrollbarPosition.toString(),
                    TYPE_CARD.toString(),
                    JSONParser().parse(getJson(binding.detailItem)) as JSONObject,
                    "Search",
                    "CARD"
                )
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    onItemFocused?.invoke(adapterPosition)
                    SegmentUtils.trackEventSearchTileCaroselFocus(
                        itemView.verticalScrollbarPosition.toString(),
                        TYPE_CARD.toString(),
                        JSONParser().parse(getJson(binding.detailItem)) as JSONObject,
                        "Search",
                        "CARD"
                    )
                }
            }

            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            onItemDpadHit?.invoke(adapterPosition)
                        }
                        KeyEvent.KEYCODE_DPAD_UP -> {
                            if (adapterPosition == 0) {
                                onItemDpadUpHit?.invoke(adapterPosition)
                            }
                        }
                    }

                }

                false
            }
        }
    }

    private fun getJson(detailItem: Content?): String {
        try {
            val gsonBuilder = GsonBuilder()
            val gson: Gson = Gson()
            return gson.toJson(detailItem)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

    inner class BookViewHolder(var binding: ItemDetailBookNoTextsBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
        }

        init {
            itemView.setOnClickListener {
                try {
                    onItemClick?.invoke(list[adapterPosition])
                    SegmentUtils.trackEventSearchTileCaroselClick(
                        itemView.verticalScrollbarPosition.toString(),
                        TYPE_BOOK.toString(),
                        JSONParser().parse(getJson(binding.detailItem)) as JSONObject,
                        "Search",
                        "BOOK"
                    )
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_RIGHT -> {
                            return@setOnKeyListener keyPressLimiter(200) {
                                if (adapterPosition == list.size - 1)
                                    coroutineScope.launch {
                                        delay(10)
                                        itemView.requestFocus()
                                    }
                            }
                        }
                    }
                }
                false
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    try {
                        onItemFocused?.invoke(adapterPosition)
                        SegmentUtils.trackEventSearchTileCaroselFocus(
                            itemView.verticalScrollbarPosition.toString(),
                            TYPE_BOOK.toString(),
                            JSONParser().parse(getJson(binding.detailItem)) as JSONObject,
                            "Search",
                            "BOOK"
                        )
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    inner class PracticeViewHolder(var binding: ItemDetailPracticeNoHighlightBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
            val requestOptions = RequestOptions().transform(RoundedCorners(16))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            if (item.thumb == null || item.thumb == "thumb" || item.thumb == "") {
//                Log.i(TAG, "no thumbnail")
                binding.ivImg.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        mContext.resources,
                        R.drawable.practice_placeholder, null
                    )
                )
            } else {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(item.thumb)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .placeholder(R.drawable.practice_placeholder)
                    .into(binding.ivImg)
            }
        }

        init {

            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
                SegmentUtils.trackEventSearchTileCaroselClick(
                    itemView.verticalScrollbarPosition.toString(),
                    TYPE_PRACTICE.toString(),
                    JSONParser().parse(getJson(binding.detailItem)) as JSONObject,
                    "Search",
                    "PRACTICE"
                )

            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    onItemFocused?.invoke(adapterPosition)
                    SegmentUtils.trackEventSearchTileCaroselFocus(
                        itemView.verticalScrollbarPosition.toString(),
                        TYPE_PRACTICE.toString(),
                        JSONParser().parse(getJson(binding.detailItem)) as JSONObject,
                        "Search",
                        "PRACTICE"
                    )
                }
            }

            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_RIGHT -> {
                            return@setOnKeyListener keyPressLimiter(300) {
                                if (adapterPosition == list.size - 1)
                                    coroutineScope.launch {
                                        delay(10)
                                        itemView.requestFocus()
                                    }
                            }
                        }
                    }
                }
                false
            }
        }
    }


    inner class TestViewHolder(var binding: ItemDetailFiberTestBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
            val requestOptions = RequestOptions().transform(RoundedCorners(12))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            if (item.thumb == "thumb" || item.thumb == "") {
//                Log.i(TAG, "no thumbnail")
                binding.ivImg.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        mContext.resources,
                        R.drawable.practice_placeholder, null
                    )
                )
            } else {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(item.thumb)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .placeholder(R.drawable.practice_placeholder)
                    .into(binding.ivImg)
            }
        }

        init {

            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    onItemFocused?.invoke(adapterPosition)
                }
            }

            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_RIGHT -> {
                            return@setOnKeyListener keyPressLimiter(200) {
                                if (adapterPosition == list.size - 1)
                                    coroutineScope.launch {
                                        delay(10)
                                        itemView.requestFocus()
                                    }
                            }
                        }
                    }
                }
                false
            }
        }
    }

    private fun keyPressLimiter(thresHold: Long = 1500L, action: () -> Unit): Boolean {
        if (SystemClock.elapsedRealtime() - lastKeyPress < thresHold) {
            return true
        } else {
            action.invoke()
        }

        lastKeyPress = SystemClock.elapsedRealtime()
        return false
    }
}