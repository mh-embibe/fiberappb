package com.embibe.embibetvapp.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.RowItemDiffAbledBinding
import com.embibe.embibetvapp.utils.ContantUtils

class RvAdapterDiffAdbled() : RecyclerView.Adapter<RvAdapterDiffAdbled.VHDiffAbled>() {

    private lateinit var binding: RowItemDiffAbledBinding
    private var selectedItem = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHDiffAbled {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_item_diff_abled,
            parent,
            false
        )
        return VHDiffAbled(binding)
    }

    override fun getItemCount(): Int {
        return ContantUtils.diffAbledData.size
    }

    override fun onBindViewHolder(holder: VHDiffAbled, position: Int) {
        holder.bind(position)
    }

    //
    inner class VHDiffAbled(var binding: RowItemDiffAbledBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.tvDiffAble.text = ContantUtils.diffAbledData[position]
            binding.clItem.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    binding.tvDiffAble.setTextColor(Color.parseColor("#000000"))
                } else {
                    binding.tvDiffAble.setTextColor(Color.parseColor("#ffffff"))
                }
            }

            if (selectedItem == adapterPosition) {
                binding.clItem.setBackgroundResource(R.drawable.bg_acheive_potential_selected)
                binding.tvDiffAble.setTextColor(Color.parseColor("#000000"))
            } else {
                binding.clItem.setBackgroundResource(R.drawable.bg_acheive_potential_selector)
                binding.tvDiffAble.setTextColor(Color.parseColor("#ffffff"))
            }
            itemView.setOnClickListener {
                if (selectedItem != adapterPosition) {
                    selectedItem = adapterPosition
                    notifyDataSetChanged()
                } else {
                    selectedItem = -1
                    notifyDataSetChanged()
                }
            }
        }
    }
}