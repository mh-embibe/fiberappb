package com.embibe.embibetvapp.ui.fragment.addUser

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentSwitchGoalBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.UserData.setUserPrefExamWithGoal
import com.embibe.embibetvapp.model.adduser.Goal
import com.embibe.embibetvapp.network.repo.DataRepo
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.activity.MainActivity
import com.embibe.embibetvapp.ui.adapter.SwitchExamAdapter
import com.embibe.embibetvapp.ui.adapter.SwitchGoalAdapter
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class SwitchGoalsExamFragment : BaseAppFragment() {

    private val classTag = SwitchGoalsExamFragment::class.java.simpleName
    private lateinit var mCurrentView: View
    private lateinit var binding: FragmentSwitchGoalBinding
    private lateinit var homeViewModel: HomeViewModel

    private lateinit var goalsAdapter: SwitchGoalAdapter
    private lateinit var examsAdapter: SwitchExamAdapter

    private var selectedExamCode = UserData.getExamCode()
    private var selectedGoalCode = UserData.getGoalCode()
    private lateinit var goalsList: List<Goal>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        goalsList = DataManager.instance.getGoalsExamsList()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_switch_goal, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCurrentView = view
        val primaryGoalTitle = StringBuilder()
        primaryGoalTitle.append(resources.getText(R.string.select_goal))
        binding.textSelectGoal.text = primaryGoalTitle
        val secondaryGoalTitle = StringBuilder()
        secondaryGoalTitle.append(resources.getText(R.string.select_exam))
        binding.textSelectExam.text = secondaryGoalTitle

        updateResult(mCurrentView)
    }

    private fun updateResult(view: View) {
        setGoalsRecyclerView(view)
        setExamsRecyclerView(view)
        registerDoneBtn()
    }

    @SuppressLint("SetTextI18n")
    private fun setGoalsRecyclerView(view: View) {

        goalsAdapter = SwitchGoalAdapter(view.context)

        makeGridCenter(binding.rvGoals)

        binding.rvGoals.adapter = goalsAdapter

        goalsAdapter.setData(goalsList)
        goalsAdapter.onItemClick = { goal ->
            examsAdapter.setData(goal.exams)
            selectedExamCode = "" //resetting selectedExamCode on every goal change
            selectedGoalCode = when {
                goal.supported -> goal.code
                else -> ""
            }
        }

        goalsAdapter.onFocusChange = { goal, hasFocus ->
            if (!goal.supported) {
                binding.textExamNotSupported.visibility = View.VISIBLE
                binding.textExamNotSupported.text =
                    "${resources.getString(R.string.text_goal_not_supported)} ${goal.name}"
            } else {
                binding.textExamNotSupported.visibility = View.GONE
            }
            if (!hasFocus) {
                binding.textExamNotSupported.visibility = View.GONE
            }
        }
    }


    @SuppressLint("SetTextI18n")
    private fun setExamsRecyclerView(view: View) {

        examsAdapter = SwitchExamAdapter(view.context)

        makeGridCenter(binding.rvExams)

        binding.rvExams.adapter = examsAdapter

        when {
            goalsList.isNotEmpty() -> {
                val goal = goalsList.find { it.code == selectedGoalCode }
                if (goal != null)
                    examsAdapter.setData(goal.exams)
                else
                    examsAdapter.setData(goalsList[0].exams)
            }
        }

        examsAdapter.onItemClick = { exam ->
            selectedExamCode = when {
                exam.supported -> exam.code
                else -> ""
            }
        }

        examsAdapter.onFocusChange = { exam, hasFocus ->
            if (!exam.supported) {
                binding.textExamNotSupported.visibility = View.VISIBLE
                binding.textExamNotSupported.text =
                    "${resources.getString(R.string.text_goal_not_supported)} ${exam.name}"
            } else {
                binding.textExamNotSupported.visibility = View.GONE
            }
            if (!hasFocus) {
                binding.textExamNotSupported.visibility = View.GONE
            }
        }
    }


    private fun registerDoneBtn() {
        binding.buttonDone.setOnClickListener {
            // board corresponds to Goal and kClass corresponds to exam
            SegmentUtils.trackSigninGoalSelectionDoneClick(
                AppConstants.TEMP_BOARD,
                AppConstants.TEMP_K_CLASS
            )

            when {
                selectedGoalCode.isEmpty() -> {
                    showToast(resources.getString(R.string.please_select_valid_goal))
                }
                selectedExamCode.isEmpty() -> {
                    //either exam is not selected or exams list is empty
                    val goal = goalsList.find { it.code == selectedGoalCode }
                    if (goal != null) {
                        when {
                            goal.exams.isNotEmpty() -> showToast(resources.getString(R.string.please_select_valid_exam))
                            else -> showToast(resources.getString(R.string.no_exams_available_for_this_goal))
                        }
                    }
                }
                else -> {
                    Log.i(classTag, "Goal:$selectedGoalCode Exam:$selectedExamCode")
                    setUserPrefExamWithGoal(selectedGoalCode, selectedExamCode)
                    // Utils.isSwitchGoalDone = true
                    getApiCall()
                }
            }
        }
    }


    private fun getApiCall() {

        showProgress()
        SegmentUtils.trackSWitchUserLoadStart()
        homeViewModel.homeApi(

            object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
                override fun onSuccess(model: List<ResultsEntity>?) {
                    if (model != null) {
                        homeViewModel.saveResultToDb(model, AppConstants.LEARN)
                        doAsync {
                            DataManager.instance.setHome(
                                model as ArrayList<ResultsEntity>,
                                object : BaseViewModel.DataCallback<ArrayList<ResultsEntity>> {
                                    override fun onSuccess(model: ArrayList<ResultsEntity>?) {
                                        getQuickLinksApiAsync()
                                    }

                                })
                        }

                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    hideProgress()
                    if (Utils.isApiFailed(code)) {
                        Utils.showError(
                            context,
                            code,
                            object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: String) {
                                    getApiCall()
                                }

                                override fun onDismiss() {

                                }
                            })
                    } else {
                        showToast(error)
                    }

                }
            })
    }

    fun getQuickLinksApiAsync() {
        homeViewModel.homeQuickLinksApi(object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
            override fun onSuccess(model: List<ResultsEntity>?) {
                if (model != null) {
                    homeViewModel.saveResultToDb(model, AppConstants.QUICK_LINKS)
                    hideProgress()
                    doAsync {
                        uiThread { updateAPIresult() }
                    }
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                updateAPIresult()
            }
        })
    }

    private fun updateAPIresult() {
        gotoHomeActivity()
    }

    private fun gotoHomeActivity() {
        try {
            val intent = Intent(activity, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            Handler().postDelayed({
                activity?.finish()
            }, 1)
        } catch (e: Exception) {
            makeLog("Exception ${e.localizedMessage}")
        }
    }


}