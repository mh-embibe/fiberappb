package com.embibe.embibetvapp.ui.fragment.errorAlertScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.FragmentAlertMsgBinding

class AlertMsg : DialogFragment() {

    private lateinit var binding: FragmentAlertMsgBinding
    private var msg: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, android.R.style.ThemeOverlay)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_alert_msg, container, false)
        binding.ivAlertMsg.setImageResource(R.drawable.ic_alert_msg)
        if (!msg.isNullOrEmpty())
            binding.tvMsg2.text = msg
        setBtnOkayClickListener()
        return binding.root
    }

    private fun setBtnOkayClickListener() {
        binding.btnOkay.setOnClickListener {
            dismiss()
        }
    }

    fun setMsg(msg: String) {
        this.msg = msg
    }


}