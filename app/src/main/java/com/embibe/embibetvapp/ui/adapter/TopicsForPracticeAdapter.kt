package com.embibe.embibetvapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemTopicsForPracticeBinding
import com.embibe.embibetvapp.newmodel.Content

class TopicsForPracticeAdapter : RecyclerView.Adapter<TopicsForPracticeAdapter.TestsViewHolder>() {

    var list = ArrayList<Content>()
    var onItemClick: ((Content) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestsViewHolder {
        val binding: ItemTopicsForPracticeBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_topics_for_practice, parent, false
        )
        return TestsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (list.size > 0) list.size else 0
    }

    override fun onBindViewHolder(holder: TestsViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun setData(list: ArrayList<Content>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class TestsViewHolder(var binding: ItemTopicsForPracticeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Content) {
            Glide.with(itemView.context).load(item.category_thumb).into(binding.ivCategory)
            binding.textNoOfQuestions.text = item.question_book_tag

            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)


            Glide.with(itemView.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(item.thumb)
                .placeholder(R.drawable.practice_placeholder)
                .error(R.drawable.practice_placeholder)
                .into(binding.ivThumbnail)

            binding.executePendingBindings()
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }
        }
    }
}