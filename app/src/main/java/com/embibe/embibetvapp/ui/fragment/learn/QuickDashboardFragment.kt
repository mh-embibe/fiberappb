package com.embibe.embibetvapp.ui.fragment.learn

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.CountDownTimer
import android.view.KeyEvent
import android.view.View
import androidx.leanback.widget.*
import androidx.lifecycle.ViewModelProviders
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseRowsSupportFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.ui.presenter.CardPresenterSelector
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.CardListRow
import com.embibe.embibetvapp.utils.GifCardView
import com.embibe.embibetvapp.utils.ShadowRowPresenterSelector
import com.embibe.embibetvapp.utils.Utils
import com.embibejio.coreapp.preference.PreferenceHelper
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class QuickDashboardFragment : BaseRowsSupportFragment() {
    private var results: List<ResultsEntity> = arrayListOf()
    private val mRowsAdapter: ArrayObjectAdapter = ArrayObjectAdapter(ShadowRowPresenterSelector())
    private lateinit var volleyRequest: RequestQueue
    private lateinit var navigationMenuCallback: NavigationMenuCallback
    private lateinit var dPadKeysCallback: DPadKeysListener
    private val classTag = QuickDashboardFragment::class.java.toString()
    private lateinit var homeViewModel: HomeViewModel
    private var continueLearningRowIndex = 0
    private var isContinueLearningRowFound = false
    private var isContinueLearningSectionFound = false
    private var continueLearningItemsSize = 0
    private var previewUrlTimer: CountDownTimer? = null
    var pref = PreferenceHelper()
    var offset: Int = 0
    private lateinit var listener: SharedPreferences.OnSharedPreferenceChangeListener

    init {
        initializeListeners()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createRows()
    }

    override fun onResume() {
        super.onResume()
        val preferences: SharedPreferences =
            App.context.getSharedPreferences(
                "app_embibe",
                Context.MODE_MULTI_PROCESS
            )
        if (Utils.isContentStatusUpdatedForQuickLinks || Utils.isContentStatusUpdatedForQuickLinksInLearn || preferences.getBoolean(
                AppConstants.ISCOOBO_UPDATED_FOR_QUICKLINKS,
                false
            )
        ) {
            Utils.isContentStatusUpdatedForQuickLinks = false
            Utils.isContentStatusUpdatedForQuickLinksInLearn = false
            preferences.edit().putBoolean(AppConstants.ISCOOBO_UPDATED_FOR_QUICKLINKS, false)
                .commit()

            /*refreshing quicklinks*/
            if (mRowsAdapter.size() > 0) {
                val row = mRowsAdapter.get(0) as CardListRow
                if (row.headerItem.name == "Continue Learning") {
                    isContinueLearningRowFound = true
                    continueLearningRowIndex = 0
                } else {
                    isContinueLearningRowFound = false
                }
            }
            if (isContinueLearningRowFound) {
                val section = results[continueLearningRowIndex]
                paginateSection(section)
            } else {
                getQuickLinksApiAsync()
                //callHomeApi()
            }
        } else if (Utils.isLikeBookmarkUpdatedInLearn || Utils.isLikeBookmarkUpdatedForQuickLinks) {
            Utils.isLikeBookmarkUpdatedInLearn = false
            Utils.isLikeBookmarkUpdatedForQuickLinks = false
            getQuickLinksApiAsync()
        }
    }

    private fun callHomeApi() {
        homeViewModel.homeQuickLinksApi(object :
            BaseViewModel.APICallBacks<List<ResultsEntity>> {
            override fun onSuccess(model: List<ResultsEntity>?) {
                if (model != null && model.isNotEmpty()) {
                    try {
                        if (model.size > 1) {
                            val continueSection = model[1]
                            if (continueSection.sectionId == 2L) {
                                insertNewRowAsContinueLearning(model, continueSection)
                            }
                        } else {
                            clearUI()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
            }
        })
    }


    private fun paginateSection(section: ResultsEntity) {
        section.size = continueLearningItemsSize + paginationSize
        homeViewModel.homeQuickLinksSectionApi(
            section.content_section_type,
            0,
            section.size,
            object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
                override fun onSuccess(model: List<ResultsEntity>?) {
                    if (model != null && model.isNotEmpty()) {
                        val newSection = model[0]
                        newSection.objId = section.objId
                        insertNewRowAsContinueLearning(model, newSection)
                    } else {
                        removeSection()
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {


                }
            })
    }

    private fun updateContinueLearning(newSection: ResultsEntity) {
        /*update new Row as ContinueLearning*/
        val row = mRowsAdapter.get(continueLearningRowIndex) as CardListRow
        val rowItemsAdapter = row.adapter as ArrayObjectAdapter
        rowItemsAdapter.setItems(getContentItems(newSection), null)
        mRowsAdapter.replace(
            continueLearningRowIndex,
            CardListRow(HeaderItem(newSection.section_name), rowItemsAdapter)
        )
        saveContinueLearningIndex(newSection.section_name, continueLearningRowIndex)
    }

    private fun insertNewRowAsContinueLearning(
        newResults: List<ResultsEntity>,
        section: ResultsEntity
    ) {
        /*create new Row as ContinueLearning*/
        if (results.get(0).sectionId != 2L) {
            (results as ArrayList).add(0, section)
            val callback = object : BaseViewModel.DataCallback<String> {
                override fun onSuccess(model: String?) {
                    makeLog("Updated Saved to Local!")
                }

            }
            dataRepo.removeListData(results as ArrayList<ResultsEntity>)
            homeViewModel.saveResultToDb(newResults, AppConstants.QUICK_LINKS, null, callback)
        }

        mRowsAdapter.add(0, creatingNewRow(section))
        saveContinueLearningIndex(section.section_name, 0)
    }

    private fun removeSection() {
        if (results[0].sectionId == 2L) {
            val row = mRowsAdapter.get(0) as CardListRow
            val rowItemsAdapter = row.adapter as ArrayObjectAdapter
            mRowsAdapter.remove(results[0])
            mRowsAdapter.removeItems(0, 1)
            dataRepo.removeSection(results[0])
        }
    }

    private fun updateSectionToDB(section: ResultsEntity) {
        dataRepo.modifySection(section, object : BaseViewModel.DataCallback<ResultsEntity> {
            override fun onSuccess(model: ResultsEntity?) {

            }
        })
    }


    private fun updateRows() {
        doAsync {
            results = homeViewModel.fetchSectionByPageName(AppConstants.QUICK_LINKS)
            uiThread {
                if (results.isNotEmpty()) {
                    for ((index, video) in results.withIndex()) {
                        val row = mRowsAdapter.get(index) as CardListRow
                        val rowItemsAdapter = row.adapter as ArrayObjectAdapter
                        rowItemsAdapter.setItems(getContentItems(video), null)
                        mRowsAdapter.replace(
                            index,
                            CardListRow(HeaderItem(video.section_name), rowItemsAdapter)
                        )
                        saveContinueLearningIndex(video.section_name, index)
                    }
                    restoreLastSelection(false)
                }
            }
        }
    }

    private fun saveContinueLearningIndex(sectionName: String, index: Int) {
        if (sectionName == "Continue Learning") {
            continueLearningRowIndex = index
            isContinueLearningSectionFound = true
        }
    }

    private fun getQuickLinksApiAsync() {
        homeViewModel.homeQuickLinksApi(object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
            override fun onSuccess(model: List<ResultsEntity>?) {
                hideProgress()
                if (model != null) {
                    if (model.size > 1) {
                        /*model.size-1 for reducing the Subjects row */
                        if (model.size - 1 != mRowsAdapter.size()) {
                            makeLog("quicklinks Size differs")
                            dataRepo.removeListData(results as ArrayList<ResultsEntity>)
                        }
                        results = model

                        homeViewModel.saveResultToDb(
                            model,
                            AppConstants.QUICK_LINKS,
                            null,
                            object : BaseViewModel.DataCallback<String> {
                                override fun onSuccess(model: String?) {
                                    createRows()
                                }

                            })
                    } else {
                        clearUI()
                    }
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {

            }
        })
    }

    private fun clearUI() {
        dataRepo.removeListData(results as ArrayList<ResultsEntity>)
        results = arrayListOf()
        mRowsAdapter.clear()
        dPadKeysCallback.isKeyPadDown(true, "Dashboard")
    }

    private fun updateRowItemForAdapter(
        indexOfRow: Int,
        lastIndexOfRow: Int,
        arrayObjectAdapter: ArrayObjectAdapter
    ) {
        if (results.isNotEmpty()) {
            paginateApiAsync(indexOfRow, lastIndexOfRow, arrayObjectAdapter)

        }
    }

    fun paginateApiAsync(
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter
    ) {
        val section = results[indexOfRow]
        if (section.total > calculateOffset(section)) {
            section.size = paginationSize
            homeViewModel.homeQuickLinksSectionApi(
                section.content_section_type,
                calculateOffset(section),
                section.size,
                object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
                    override fun onSuccess(model: List<ResultsEntity>?) {
                        if (model != null && model.isNotEmpty()) {
                            updateSection(
                                model[0],
                                section,
                                indexOfRow,
                                lastIndexOfRow,
                                currentAdapter
                            )
                        }
                    }

                    override fun onFailed(code: Int, error: String, msg: String) {


                    }
                })

        }

    }

    private fun updateSection(
        resultsEntity: ResultsEntity,
        section: ResultsEntity,
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter
    ) {
        resultsEntity.content?.let { section.content?.addAll(it) }
        section.offset = resultsEntity.offset
        section.size = resultsEntity.size
        /*local DB*/
        dataRepo.modifySection(section, object : BaseViewModel.DataCallback<ResultsEntity> {
            override fun onSuccess(model: ResultsEntity?) {
                getUpdatedSection(
                    section.sectionId,
                    indexOfRow,
                    lastIndexOfRow,
                    currentAdapter,
                    resultsEntity.content ?: arrayListOf()
                )
            }
        })
        /*UI*/
        bindSectionToUI(
            section,
            indexOfRow,
            lastIndexOfRow,
            currentAdapter,
            resultsEntity.content ?: arrayListOf()
        )

    }

    private fun getUpdatedSection(
        sectionId: Long,
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter,
        newContentList: ArrayList<Content>
    ) {
        val updatedSection = homeViewModel.getSectionById(sectionId, AppConstants.QUICK_LINKS)
        //bindSectionToUI(updatedSection, indexOfRow, lastIndexOfRow, currentAdapter,newContentList)
    }

    private fun bindSectionToUI(
        updatedSection: ResultsEntity?,
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter,
        newContentList: ArrayList<Content>
    ) {
        /*update the Section */
        currentAdapter.addAll(lastIndexOfRow, newContentList)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        volleyRequest = Volley.newRequestQueue(activity)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

        navigationMenuCallback.navMenuToggle(false)

    }

    private fun createRows() {
        doAsync {
            results = homeViewModel.fetchSectionByPageName(AppConstants.QUICK_LINKS)
            if (results.isEmpty()) {
                getQuickLinksApiAsync()
            } else {
                uiThread {
                    mRowsAdapter.clear()
                    for ((index, video) in results.withIndex()) {
                        mRowsAdapter.add(creatingNewRow(video))
                        saveContinueLearningIndex(video.section_name, index)
                    }
                }
            }
        }
    }

    private fun creatingNewRow(videos: ResultsEntity): Row {
        val presenterSelector = activity?.baseContext?.let { CardPresenterSelector(it) }
        val adapter = ArrayObjectAdapter(presenterSelector)
        for (video in videos.content!!) {
            adapter.add(video)
        }
        val headerItem = HeaderItem(videos.section_name)
        return CardListRow(headerItem, adapter)
    }

    inline fun <reified T : Any> Any.cast(): T {
        return this as T
    }


    private fun initializeListeners() {
        adapter = mRowsAdapter
        onItemViewClickedListener =
            OnItemViewClickedListener { itemViewHolder, item, rowViewHolder, row ->
                Utils.subjectFilterBy = AppConstants.QUICK_LINKS
                onClickItem(item as Content, row, mRowsAdapter, itemViewHolder)
            }

        onItemViewSelectedListener =
            OnItemViewSelectedListener { itemViewHolder, item, rowViewHolder, row ->
                var currentRowSize = ((row as CardListRow).adapter as ArrayObjectAdapter).size()
                val indexOfItem = (row.adapter as ArrayObjectAdapter).indexOf(item)
                val indexOfRow = mRowsAdapter.indexOf(row)
                if ((currentRowSize - (indexOfItem + 1)) == 5) {
                    updateRowItemForAdapter(
                        indexOfRow,
                        currentRowSize - 1,
                        (row.adapter as ArrayObjectAdapter)
                    )
                }
                if (item is Content) {
                    val rowView = rowViewHolder.view
                    val itemView = itemViewHolder.view
                    if (itemView is GifCardView) {
                        addDelayToPreviewUrls(item, itemView)
                    }
                    if (itemView.isFocusable && !itemView.isFocused) itemView.requestFocus()

                    saveLastSelected(indexOfItem, indexOfRow)

                    itemView.setOnKeyListener { v, keyCode, event ->
                        //checking only for down presses
                        if (event.action == KeyEvent.ACTION_DOWN) {
                            when (keyCode) {
                                KeyEvent.KEYCODE_DPAD_LEFT -> {
                                    previewUrlTimer?.cancel()
                                    if (itemView is GifCardView) {
                                        itemView.ivVideo.controller?.animatable?.stop()
                                    }
                                    if (indexOfItem == 0) {
                                        navigationMenuCallback.navMenuToggle(true)
                                    }
                                }
                                KeyEvent.KEYCODE_DPAD_RIGHT -> {
                                    previewUrlTimer?.cancel()
                                    if (itemView is GifCardView) {
                                        itemView.ivVideo.controller?.animatable?.stop()
                                    }
                                }
                                KeyEvent.KEYCODE_DPAD_UP -> {
                                    previewUrlTimer?.cancel()
                                    if (itemView is GifCardView) {
                                        itemView.ivVideo.controller?.animatable?.stop()
                                    }
                                    if (indexOfRow == 0) {
                                        itemView.postDelayed({
                                            itemView.requestFocus()
                                        }, 0)
                                    }
                                }
                                KeyEvent.KEYCODE_DPAD_DOWN -> {
                                    previewUrlTimer?.cancel()
                                    if (itemView is GifCardView) {
                                        itemView.ivVideo.controller?.animatable?.stop()
                                    }
                                    /* if (!Utils.isKeyPressedTooFast())
                                         dPadKeysCallback.isKeyPadDown(true, "Dashboard")*/
                                    if (indexOfRow == mRowsAdapter.size() - 1) {
                                        if (!Utils.isKeyPressedTooFast(1000))
                                            dPadKeysCallback.isKeyPadDown(true, "Dashboard")
                                    }

//                                    //for last row
//                                    if (indexOfRow == mRowsAdapter.size() - 1) {
//
//                                    }
                                }
                            }
                        }
                        false
                    }
                }
            }
    }

    private fun addDelayToPreviewUrls(
        item: Content,
        itemView: GifCardView
    ) {
        previewUrlTimer = object : CountDownTimer(4000, 1000) {
            override fun onFinish() {
                if (item.preview_url.isNotEmpty())
                    itemView.showVideoView(item.preview_url)
            }

            override fun onTick(millisUntilFinished: Long) {}
        }
        previewUrlTimer?.start()
    }


    private fun saveLastSelected(indexOfItem: Int, indexOfRow: Int) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        with(sharedPref?.edit()) {
            this?.putInt(getString(R.string.last_selected_dashboard_row_item), indexOfItem)
            this?.putInt(getString(R.string.last_selected_dashboard_row), indexOfRow)
            this?.apply()
        }
    }

    fun restoreLastSelection(fromNavigation: Boolean) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        var itemIndex = sharedPref.getInt(getString(R.string.last_selected_dashboard_row_item), 0)
        val rowIndex = sharedPref.getInt(getString(R.string.last_selected_dashboard_row), 0)

        if (itemIndex == 0) {//for 0th item and Navigation interaction case
            val rvh = getRowViewHolder(rowIndex)
            rvh?.selectedItemViewHolder?.view?.focusSearch(View.FOCUS_LEFT)?.requestFocus()
        } else {
            //For Continue Learning row, 0th item has to be focused
            if (rowIndex == continueLearningRowIndex)
                itemIndex = 0

            val rowsSize = mRowsAdapter.size()
            if (rowIndex < rowsSize) {
                val rowItemsSize = (mRowsAdapter.get(rowIndex) as CardListRow).adapter.size()
                if (itemIndex < rowItemsSize) {
                    setSelectedPosition(
                        rowIndex,
                        true,
                        object : ListRowPresenter.SelectItemViewHolderTask(itemIndex) {
                            override fun run(holder: Presenter.ViewHolder?) {
                                super.run(holder)//imp line
                                holder?.view?.postDelayed({
                                    holder.view.requestFocus()
                                    hideProgress()
                                }, 10)
                            }
                        })
                }
            }
        }
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }

}