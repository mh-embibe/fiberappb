package com.embibe.embibetvapp.ui.fragment.achieve

import android.animation.*
import android.graphics.PointF
import android.graphics.Rect
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.get
import androidx.core.view.updateLayoutParams
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import com.airbnb.lottie.LottieAnimationView
import com.airbnb.lottie.LottieDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.integration.webp.decoder.WebpDrawable
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.databinding.FragmentDiscoverKnowledgeBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.achieve.*
import com.embibe.embibetvapp.model.achieve.futureSuccess.FutureSuccessRes
import com.embibe.embibetvapp.model.achieve.keyconcepts.KeyConceptModel
import com.embibe.embibetvapp.model.achieve.readiness.Readiness
import com.embibe.embibetvapp.ui.custom.LineView
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.ui.viewmodel.AchieveViewModel
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo
import com.github.penfeizhou.animation.apng.APNGDrawable
import com.github.penfeizhou.animation.loader.ResourceStreamLoader
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.collections.set
import kotlin.math.*

class DiscoverFragment : BaseAppFragment(), DPadKeysListener {

    private var isAchieverShown: Boolean = false
    private lateinit var achieveViewModel: AchieveViewModel
    private lateinit var binding: FragmentDiscoverKnowledgeBinding
    private val classTag = DiscoverFragment::class.java.simpleName
    private lateinit var requestOptions: RequestOptions
    private var isEnterClicked = false
    private var isReadinessDataAvailable = false
    private var isFutureSuccessDataAvailable = false
    private val trianglesValuesMap = HashMap<ConstraintLayout, TriangleValue>()
    private val linesMap = HashMap<LineView, LinePoint>()
    private val sphereSelectionLinePointsMap = HashMap<String, Pair<PointF, PointF>>()
    private val spheresPositionsMap = HashMap<ConstraintLayout, PointF>()
    private val sphereTriangleMap = HashMap<ConstraintLayout, ConstraintLayout>()
    private var keyConceptPointsList = ArrayList<Pair<ImageButton, TextView>>()
    private var relatedConceptsViewsList = ArrayList<RelatedConceptView>()
    private var keyConceptsCoordinatesList = ArrayList<PointF>()
    private var keyConceptsList = ArrayList<KeyConceptModel>()
    private var percentageConnectionsList = ArrayList<PercentageData>()
    private val currentGrade = UserData.getGrade()
    private val currentGoal = UserData.getGoal()
    private lateinit var navigationMenuCallback: NavigationMenuCallback
    private lateinit var currentSelectedAnimation: Animation
    private var isReadinessActive = false
    private var isSphereSelected = false
    private var whichSphereClicked = ""
    private lateinit var selectedLine: LineView
    private val sphereClickDelayInterval = 2500
    private val pointFadeInOutDuration = 700
    private val achieversFragment: AchieversFragment by lazy {
        AchieversFragment()
    }

    companion object {
        var readinessFragment: ReadinessFragment = ReadinessFragment()
    }

    private val minSize = 100
    private val maxSize = 200

    private val SPHERE_SUBTLE_GLOW_INDEX = 0
    private val SPHERE_GLOW_INDEX = 1
    private val SPHERE_TEXT_INDEX = 2

    lateinit var centerSphere: ConstraintLayout
    lateinit var sphere9: ConstraintLayout
    lateinit var sphere9selected: ConstraintLayout
    lateinit var sphere8selected: ConstraintLayout
    lateinit var sphere7selected: ConstraintLayout
    lateinit var sphere6selected: ConstraintLayout
    lateinit var sphereNEETselected: ConstraintLayout
    lateinit var sphereJEEselected: ConstraintLayout
    lateinit var sphere11selected: ConstraintLayout
    lateinit var sphere12selected: ConstraintLayout
    lateinit var sphere8: ConstraintLayout
    lateinit var sphere7: ConstraintLayout
    lateinit var sphere6: ConstraintLayout
    lateinit var sphere11: ConstraintLayout
    lateinit var sphere12: ConstraintLayout
    lateinit var sphereNEET: ConstraintLayout
    lateinit var sphereJEE: ConstraintLayout

    lateinit var triangle6: ConstraintLayout
    lateinit var triangle7: ConstraintLayout
    lateinit var triangle8: ConstraintLayout
    lateinit var triangle9: ConstraintLayout
    lateinit var triangle11: ConstraintLayout
    lateinit var triangle12: ConstraintLayout
    lateinit var triangleNEET: ConstraintLayout
    lateinit var triangleJEE: ConstraintLayout

    private var coroutineScope: CoroutineScope? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coroutineScope = CoroutineScope(Dispatchers.Main)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_discover_knowledge, container,
            false
        )
        achieveViewModel = ViewModelProviders.of(this).get(AchieveViewModel::class.java)
        navigationMenuCallback.navMenuToggle(false)
        initSpheres()
        initTriangles()
        return binding.root
    }

    private fun initTriangles() {
        triangle6 = binding.layoutTriangle6.clTriangle6
        triangle7 = binding.layoutTriangle7.clTriangle7
        triangle8 = binding.layoutTriangle8.clTriangle8
        triangle9 = binding.layoutTriangle9.clTriangle9
        triangle11 = binding.layoutTriangle11.clTriangle11
        triangle12 = binding.layoutTriangle12.clTriangle12
        triangleNEET = binding.layoutTriangleNEET.clTriangleNEET
        triangleJEE = binding.layoutTriangleJEE.clTriangleJEE
    }

    private fun initSpheres() {
        centerSphere = binding.layoutSphereCenter.clCenterSphere
        sphere9 = binding.layoutSphere9.clSphere9
        sphere9selected = binding.layoutSphereSelected9.clSphere9
        sphere8selected = binding.layoutSphereSelected8.clSphere8
        sphere7selected = binding.layoutSphereSelected7.clSphere7
        sphere6selected = binding.layoutSphereSelected6.clSphere6
        sphere11selected = binding.layoutSphereSelected11.clSphere11
        sphere12selected = binding.layoutSphereSelected12.clSphere12
        sphereJEEselected = binding.layoutSphereSelectedJEE.clSphereJEE
        sphereNEETselected = binding.layoutSphereSelectedNEET.clSphereNEET
        sphere9selected.visibility = GONE
        sphere8selected.visibility = GONE
        sphere7selected.visibility = GONE
        sphere6selected.visibility = GONE
        sphere11selected.visibility = GONE
        sphere12selected.visibility = GONE
        sphereJEEselected.visibility = GONE
        sphereNEETselected.visibility = GONE
        sphere8 = binding.layoutSphere8.clSphere8
        sphere7 = binding.layoutSphere7.clSphere7
        sphere6 = binding.layoutSphere6.clSphere6
        sphere11 = binding.layoutSphere11.clSphere11
        sphere12 = binding.layoutSphere12.clSphere12
        sphereNEET = binding.layoutSphereNEET.clSphereNEET
        sphereJEE = binding.layoutSphereJEE.clSphereJEE
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentReplacer(binding.flAchievers.id, achieversFragment)
        setAchieversKeyListener()
        discoverWhereYouStandListeners()
        enterButtonKeyListeners()
        goalSpheresListeners()
        setInitialView3()
        getPercentageConnectionsApi()
        callReadinessApiAsync()
        callFutureSuccessApi()
    }

    private fun toggleShadowVisibilityForDiscoverArrow(visibility: Int) {
        if (visibility == VISIBLE)
            ObjectAnimator.ofFloat(binding.lvDiscoverWhereYouStand, ALPHA, 0.7f, 1f).start()
        else
            ObjectAnimator.ofFloat(binding.lvDiscoverWhereYouStand, ALPHA, 1f, 0.7f).start()

        binding.ivDiscoverShadow.visibility = visibility
    }

    private fun goalSpheresListeners() {

        centerSphere.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                (centerSphere[0] as ImageView).changeFocusBg()
            } else {
                (centerSphere[0] as ImageView)
                    .setBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            android.R.color.transparent,
                            null
                        )
                    )
            }
        }
        centerSphere.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        val view = centerSphere.focusSearch(FOCUS_LEFT)
                        if (view == null)
                            Log.i(classTag, "Focus Search left null")
                        sphere9.postDelayed({
                            sphere9.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        showAchievers(600)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphereNEET.postDelayed({
                            sphereNEET.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere12.postDelayed({
                            sphere12.requestFocus()
                        }, 10)
                    }
                }
            }

            false
        }

        sphere11selected.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                changeSphereBg(sphere11selected[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere11selected[SPHERE_GLOW_INDEX] as ImageView)
            }
        }

        sphere11selected.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP, KeyEvent.KEYCODE_DPAD_RIGHT,
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        updateFocusForThisView(sphere11selected, true)
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (keyConceptsList.isNotEmpty()) {
                            shiftFocusToKeyConceptPoints(
                                sphere11selected,
                                CoordinatePlaneValues.BOTTOM_LEFT_11
                            )
                        } else {
                            updateFocusForThisView(sphere11selected, true)
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        resetSpheres(whichSphereClicked)
                    }
                }
            }
            false
        }

        sphere11.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphere11[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere11[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphere11.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        sphere12.postDelayed({
                            sphere12.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphere11.postDelayed({
                            sphere11.clearFocus()
                        }, 10)
                        binding.lvDiscoverWhereYouStand.postDelayed({
                            binding.lvDiscoverWhereYouStand.requestFocus()
                            loadLottieAnimation(
                                binding.lvDiscoverWhereYouStand,
                                R.raw.discover_where_you_stand_2,
                                false
                            )
                        }, 10)
                        toggleShadowVisibilityForDiscoverArrow(VISIBLE)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere11.postDelayed({
                            sphere11.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        sphereNEET.postDelayed({
                            sphereNEET.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphere11selected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(
                            GONE
                        )
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphere11,
                                sphere11selected,
                                triangle11,
                                binding.connection1011,
                                CoordinatePlaneValues.BOTTOM_LEFT_11
                            )
                            isSphereSelected = true
                            whichSphereClicked = "Sphere11"
                        }
                    }
                }
            }
            false
        }

        sphere12selected.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                changeSphereBg(sphere12selected[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere12selected[SPHERE_GLOW_INDEX] as ImageView)
            }
        }

        sphere12selected.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP, KeyEvent.KEYCODE_DPAD_RIGHT,
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        updateFocusForThisView(sphere12selected, true)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (keyConceptsList.isNotEmpty()) {
                            shiftFocusToKeyConceptPoints(
                                sphere12selected,
                                CoordinatePlaneValues.BOTTOM_DOWN_12
                            )
                        } else {
                            updateFocusForThisView(sphere12selected, true)
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        resetSpheres(whichSphereClicked)
                    }
                }
            }
            false
        }

        sphere12.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphere12[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere12[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphere12.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        sphere6.postDelayed({
                            sphere6.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphere11.postDelayed({
                            sphere11.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere12.postDelayed({
                            sphere12.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!centerSphere.isFocusable)
                            centerSphere.isFocusable = true
                        centerSphere.postDelayed({
                            centerSphere.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphere12selected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(
                            GONE
                        )
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphere12,
                                sphere12selected,
                                triangle12,
                                binding.connection1012,
                                CoordinatePlaneValues.BOTTOM_DOWN_12
                            )
                            isSphereSelected = true
                            whichSphereClicked = "Sphere12"
                        }
                    }
                }
            }
            false
        }

        sphereNEETselected.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                changeSphereBg(sphereNEETselected[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphereNEETselected[SPHERE_GLOW_INDEX] as ImageView)
            }
        }

        sphereNEETselected.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP, KeyEvent.KEYCODE_DPAD_RIGHT,
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        updateFocusForThisView(sphereNEETselected, true)
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (keyConceptsList.isNotEmpty()) {
                            shiftFocusToKeyConceptPoints(
                                sphereNEETselected,
                                CoordinatePlaneValues.CENTER_LEFT_NEET
                            )
                        } else {
                            updateFocusForThisView(sphereNEETselected, true)
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        resetSpheres(whichSphereClicked)
                    }
                }
            }
            false
        }

        sphereNEET.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphereNEET[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphereNEET[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphereNEET.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (!centerSphere.isFocusable)
                            centerSphere.isFocusable = true
                        centerSphere.postDelayed({
                            centerSphere.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphereNEET.postDelayed({
                            sphereNEET.clearFocus()
                        }, 10)
                        binding.lvDiscoverWhereYouStand.postDelayed({
                            binding.lvDiscoverWhereYouStand.requestFocus()
                            loadLottieAnimation(
                                binding.lvDiscoverWhereYouStand,
                                R.raw.discover_where_you_stand_2,
                                false
                            )
                        }, 10)
                        toggleShadowVisibilityForDiscoverArrow(VISIBLE)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere11.postDelayed({
                            sphere11.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        sphereJEE.postDelayed({
                            sphereJEE.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphereNEETselected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(
                            GONE
                        )
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphereNEET,
                                sphereNEETselected,
                                triangleNEET,
                                binding.connection10NEET,
                                CoordinatePlaneValues.CENTER_LEFT_NEET
                            )
                            isSphereSelected = true
                            whichSphereClicked = "SphereNEET"
                        }
                    }
                }
            }
            false
        }

        sphereJEEselected.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                changeSphereBg(sphereJEEselected[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphereJEEselected[SPHERE_GLOW_INDEX] as ImageView)
            }
        }

        sphereJEEselected.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP, KeyEvent.KEYCODE_DPAD_RIGHT,
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        updateFocusForThisView(sphereJEEselected, true)
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (keyConceptsList.isNotEmpty()) {
                            shiftFocusToKeyConceptPoints(
                                sphereJEEselected,
                                CoordinatePlaneValues.TOP_LEFT_JEE
                            )
                        } else {
                            updateFocusForThisView(sphereJEEselected, true)
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        resetSpheres(whichSphereClicked)
                    }
                }
            }
            false
        }

        sphereJEE.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphereJEE[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphereJEE[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphereJEE.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (!centerSphere.isFocusable)
                            centerSphere.isFocusable = true
                        centerSphere.postDelayed({
                            centerSphere.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphereJEE.postDelayed({
                            sphereJEE.clearFocus()
                        }, 10)
                        binding.lvDiscoverWhereYouStand.postDelayed({
                            binding.lvDiscoverWhereYouStand.requestFocus()
                            loadLottieAnimation(
                                binding.lvDiscoverWhereYouStand,
                                R.raw.discover_where_you_stand_2,
                                false
                            )
                        }, 10)
                        toggleShadowVisibilityForDiscoverArrow(VISIBLE)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphereNEET.postDelayed({
                            sphereNEET.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        showAchievers(600)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphereJEEselected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(
                            GONE
                        )
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphereJEE,
                                sphereJEEselected,
                                triangleJEE,
                                binding.connection10JEE,
                                CoordinatePlaneValues.TOP_LEFT_JEE
                            )
                            isSphereSelected = true
                            whichSphereClicked = "SphereJEE"
                        }
                    }
                }
            }
            false
        }

        sphere6selected.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                changeSphereBg(sphere6selected[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere6selected[SPHERE_GLOW_INDEX] as ImageView)
            }
        }

        sphere6selected.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP,
                    KeyEvent.KEYCODE_DPAD_DOWN, KeyEvent.KEYCODE_DPAD_LEFT -> {
                        updateFocusForThisView(sphere6selected, true)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (keyConceptsList.isNotEmpty()) {
                            shiftFocusToKeyConceptPoints(
                                sphere6selected,
                                CoordinatePlaneValues.BOTTOM_RIGHT_6
                            )
                        } else {
                            updateFocusForThisView(sphere6selected, true)
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        resetSpheres(whichSphereClicked)
                    }
                }
            }
            false
        }


        sphere6.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphere6[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere6[SPHERE_GLOW_INDEX] as ImageView)

            }
        }
        sphere6.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        sphere7.postDelayed({
                            sphere7.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphere12.postDelayed({
                            sphere12.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere6.postDelayed({
                            sphere6.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!centerSphere.isFocusable)
                            centerSphere.isFocusable = true
                        centerSphere.postDelayed({
                            centerSphere.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphere6selected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(GONE)
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphere6,
                                sphere6selected,
                                triangle6,
                                binding.connection106,
                                CoordinatePlaneValues.BOTTOM_RIGHT_6
                            )
                            isSphereSelected = true
                            whichSphereClicked = "Sphere6"
                        }
                    }
                }
            }
            false
        }

        sphere7selected.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                changeSphereBg(sphere7selected[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere7selected[SPHERE_GLOW_INDEX] as ImageView)
            }
        }

        sphere7selected.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP,
                    KeyEvent.KEYCODE_DPAD_DOWN, KeyEvent.KEYCODE_DPAD_LEFT -> {
                        updateFocusForThisView(sphere7selected, true)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (keyConceptsList.isNotEmpty()) {
                            shiftFocusToKeyConceptPoints(
                                sphere7selected,
                                CoordinatePlaneValues.BOTTOM_RIGHT_7
                            )
                        } else {
                            updateFocusForThisView(sphere7selected, true)
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        resetSpheres(whichSphereClicked)
                    }

                }
            }
            false
        }

        sphere7.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphere7[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere7[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphere7.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navigationMenuCallback.navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        sphere9.postDelayed({
                            sphere9.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphere6.postDelayed({
                            sphere6.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere7.postDelayed({
                            sphere7.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphere7selected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(GONE)
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphere7,
                                sphere7selected,
                                triangle7,
                                binding.connection107,
                                CoordinatePlaneValues.BOTTOM_RIGHT_7
                            )
                            isSphereSelected = true
                            whichSphereClicked = "Sphere7"
                        }
                    }
                }
            }
            false
        }

        sphere8selected.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                changeSphereBg(sphere8selected[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere8selected[SPHERE_GLOW_INDEX] as ImageView)
            }
        }

        sphere8selected.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP,
                    KeyEvent.KEYCODE_DPAD_DOWN, KeyEvent.KEYCODE_DPAD_LEFT -> {
                        updateFocusForThisView(sphere8selected, true)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (keyConceptsList.isNotEmpty()) {
                            shiftFocusToKeyConceptPoints(
                                sphere8selected,
                                CoordinatePlaneValues.TOP_RIGHT_8
                            )
                        } else {
                            updateFocusForThisView(sphere8selected, true)
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        resetSpheres(whichSphereClicked)
                    }
                }
            }
            false
        }

        sphere8.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphere8[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere8[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphere8.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navigationMenuCallback.navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        showAchievers(600)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (!centerSphere.isFocusable)
                            centerSphere.isFocusable = true
                        centerSphere.postDelayed({
                            centerSphere.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere9.postDelayed({
                            sphere9.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphere8selected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(GONE)
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphere8,
                                sphere8selected,
                                triangle8,
                                binding.connection108,
                                CoordinatePlaneValues.TOP_RIGHT_8
                            )
                            isSphereSelected = true
                            whichSphereClicked = "Sphere8"
                        }
                    }
                }
            }
            false
        }

        sphere9selected.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                changeSphereBg(sphere9selected[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere9selected[SPHERE_GLOW_INDEX] as ImageView)
            }
        }

        sphere9selected.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP,
                    KeyEvent.KEYCODE_DPAD_DOWN, KeyEvent.KEYCODE_DPAD_LEFT -> {
                        updateFocusForThisView(sphere9selected, true)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (keyConceptsList.isNotEmpty()) {
                            shiftFocusToKeyConceptPoints(
                                sphere9selected,
                                CoordinatePlaneValues.CENTER_RIGHT_9
                            )
                        } else {
                            updateFocusForThisView(sphere9selected, true)
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        resetSpheres(whichSphereClicked)
                    }
                }
            }
            false
        }

        sphere9.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphere9[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere9[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphere9.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navigationMenuCallback.navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        sphere8.postDelayed({
                            sphere8.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (!centerSphere.isFocusable)
                            centerSphere.isFocusable = true
                        centerSphere.postDelayed({
                            centerSphere.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere7.postDelayed({
                            sphere7.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphere9selected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(GONE)
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphere9,
                                sphere9selected,
                                triangle9,
                                binding.connection109,
                                CoordinatePlaneValues.CENTER_RIGHT_9
                            )
                            isSphereSelected = true
                            whichSphereClicked = "Sphere9"
                        }
                    }
                }
            }
            false
        }

    }

    private fun ImageView.hideSubtleGlow(visibility: Int) {
        this.visibility = visibility
    }

    private fun shiftFocusToKeyConceptPoints(
        selectedSphere: ConstraintLayout,
        direction: Pair<Float, Float>
    ) {
        updateFocusForThisView(selectedSphere, false)
        selectedSphere.isFocusable = false
        focusToKeyConceptPoints(selectedSphere, direction, 0)
    }

    private fun focusToKeyConceptPoints(
        selectedSphere: ConstraintLayout,
        direction: Pair<Float, Float>,
        conceptPointFocusPosition: Int
    ) {
        if (keyConceptPointsList.isNotEmpty()) {
            when {
                conceptPointFocusPosition >= keyConceptPointsList.size -> {
                    toggleRelatedConceptsVisibility(VISIBLE, keyConceptPointsList.size - 1)
                    updateFocusForThisView(keyConceptPointsList.last().first, true)
                }
                conceptPointFocusPosition < 0 -> {
                    updateFocusForThisView(keyConceptPointsList.first().first, false)
                    selectedSphere.isFocusable = true
                    updateFocusForThisView(selectedSphere, true)
                }
                else -> {
                    val keyConceptPoint = keyConceptPointsList[conceptPointFocusPosition]
                    updateFocusForThisView(keyConceptPoint.first, true)
                    keyConceptPointListeners(
                        selectedSphere,
                        keyConceptPoint,
                        direction,
                        conceptPointFocusPosition
                    )
                    if (conceptPointFocusPosition < relatedConceptsViewsList.size) {
                        toggleRelatedConceptsVisibility(VISIBLE, conceptPointFocusPosition)
                    } else {
//                        showRelatedConcepts(conceptPointFocusPosition, direction)
                    }
                }
            }
        }
    }

    private fun keyConceptPointListeners(
        selectedSphere: ConstraintLayout,
        keyConceptPoint: Pair<ImageButton, TextView>,
        direction: Pair<Float, Float>,
        conceptPointFocusPosition: Int
    ) {
        keyConceptPoint.first.setOnFocusChangeListener { v, hasFocus ->
            val conceptText = keyConceptPoint.second
            val conceptImageButton = keyConceptPoint.first
            if (hasFocus) {
                conceptText.apply {
                    animate().apply {
                        alpha(1f)
                        duration = 500
                    }
                }
                conceptImageButton.apply {
                    background = ContextCompat.getDrawable(context, R.drawable.key_concept)
                    updateLayoutParams {
                        width = 25
                        height = 25
                    }
                }
            } else {
                conceptText.apply {
                    animate().apply {
                        alpha(0f)
                        duration = 500
                    }
                }
                conceptImageButton.apply {
                    background = ContextCompat.getDrawable(context, R.drawable.key_concept_white)
                    updateLayoutParams {
                        width = 20
                        height = 20
                    }
                }
            }
        }

        keyConceptPoint.first.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (direction) {
                    CoordinatePlaneValues.BOTTOM_LEFT_11,
                    CoordinatePlaneValues.CENTER_LEFT_NEET,
                    CoordinatePlaneValues.TOP_LEFT_JEE -> {
                        if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT
                            && conceptPointFocusPosition < keyConceptPointsList.size
                        ) {
                            toggleRelatedConceptsVisibility(INVISIBLE, conceptPointFocusPosition)
                            focusToKeyConceptPoints(
                                selectedSphere,
                                direction,
                                conceptPointFocusPosition + 1
                            )
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT
                            && conceptPointFocusPosition < keyConceptPointsList.size
                        ) {
                            toggleRelatedConceptsVisibility(INVISIBLE, conceptPointFocusPosition)
                            focusToKeyConceptPoints(
                                selectedSphere,
                                direction,
                                conceptPointFocusPosition - 1
                            )
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                            updateFocusForThisView(keyConceptPoint.first, true)//blocking it
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                            //move focus to related key concepts points
                            updateFocusForThisView(keyConceptPoint.first, true)//blocking it
                        }
                    }
                    CoordinatePlaneValues.BOTTOM_RIGHT_6,
                    CoordinatePlaneValues.BOTTOM_RIGHT_7,
                    CoordinatePlaneValues.TOP_RIGHT_8,
                    CoordinatePlaneValues.CENTER_RIGHT_9 -> {
                        if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT
                            && conceptPointFocusPosition < keyConceptPointsList.size
                        ) {
                            toggleRelatedConceptsVisibility(INVISIBLE, conceptPointFocusPosition)
                            focusToKeyConceptPoints(
                                selectedSphere,
                                direction,
                                conceptPointFocusPosition + 1
                            )
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT
                            && conceptPointFocusPosition < keyConceptPointsList.size
                        ) {
                            toggleRelatedConceptsVisibility(INVISIBLE, conceptPointFocusPosition)
                            focusToKeyConceptPoints(
                                selectedSphere,
                                direction,
                                conceptPointFocusPosition - 1
                            )
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                            updateFocusForThisView(keyConceptPoint.first, true)//blocking it
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                            //move focus to related key concepts points
                            updateFocusForThisView(keyConceptPoint.first, true)//blocking it
                        }
                    }
                    CoordinatePlaneValues.BOTTOM_DOWN_12 -> {
                        if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN
                            && conceptPointFocusPosition < keyConceptPointsList.size
                        ) {
                            toggleRelatedConceptsVisibility(INVISIBLE, conceptPointFocusPosition)
                            focusToKeyConceptPoints(
                                selectedSphere,
                                direction,
                                conceptPointFocusPosition + 1
                            )
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP
                            && conceptPointFocusPosition < keyConceptPointsList.size
                        ) {
                            toggleRelatedConceptsVisibility(INVISIBLE, conceptPointFocusPosition)
                            focusToKeyConceptPoints(
                                selectedSphere,
                                direction,
                                conceptPointFocusPosition - 1
                            )
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                            updateFocusForThisView(keyConceptPoint.first, true)//blocking it
                        }
                    }
                }
            }
            false
        }
    }

    private fun toggleRelatedConceptsVisibility(
        visibility: Int,
        conceptPointFocusPosition: Int
    ) {
        when {
            conceptPointFocusPosition >= 0 && conceptPointFocusPosition < relatedConceptsViewsList.size -> {
                val relatedConceptView = relatedConceptsViewsList[conceptPointFocusPosition]
                when (visibility) {
                    INVISIBLE -> {
                        relatedConceptView.connectingLine.clearAnimation()
                        relatedConceptView.connectingPoint.first.clearAnimation()
                        relatedConceptView.connectingPoint.second.clearAnimation()
                    }
                    VISIBLE -> {
                        relatedConceptView.connectingLine.animation = currentSelectedAnimation
                        relatedConceptView.connectingPoint.first.animation =
                            currentSelectedAnimation
                        relatedConceptView.connectingPoint.second.animation =
                            currentSelectedAnimation
                    }
                }
                relatedConceptView.connectingLine.visibility = visibility //connecting line
                relatedConceptView.connectingPoint.first.visibility = visibility //point
                relatedConceptView.connectingPoint.second.visibility = visibility //label
                val relatedConcepts = relatedConceptView.relatedConcepts
                relatedConcepts.forEach { relatedConcept ->
                    when (visibility) {
                        INVISIBLE -> {
                            relatedConcept.first.clearAnimation()
                            relatedConcept.second.first.clearAnimation()
                            relatedConcept.second.second.clearAnimation()
                        }
                        VISIBLE -> {
                            relatedConcept.first.animation = currentSelectedAnimation
                            relatedConcept.second.first.animation = currentSelectedAnimation
                            relatedConcept.second.second.animation = currentSelectedAnimation
                        }
                    }
                    relatedConcept.first.visibility = visibility
                    relatedConcept.second.first.visibility = visibility
                    relatedConcept.second.second.visibility = visibility
                }
            }
        }
    }

    private fun showRelatedConcepts(
        conceptPointFocusPosition: Int,
        direction: Pair<Float, Float>
    ) {
        //todo: refactor delta values, combine them into one as per direction
        val secondPointDeltaX = 25f
        val secondPointDeltaY = 100f
        var relatedConceptDeltaX1 = -120f//for line
        var relatedConceptDeltaX2 = -128f//for point
        var relatedConceptDeltaY1 = -70f//for line
        var relatedConceptDeltaY2 = -115f//for point

        val relatedConcepts = keyConceptsList[conceptPointFocusPosition].relatedConcepts

        val firstPoint = keyConceptsCoordinatesList[conceptPointFocusPosition]
        var firstPointAdjustment = PointF(firstPoint.x + 8f, firstPoint.y + 5f)

        var originPoint = PointF(firstPoint.x - secondPointDeltaX, firstPoint.y - secondPointDeltaY)
        var originPointAdjustment = PointF(originPoint.x - 7f, originPoint.y - 7f)

        when (direction) {
            CoordinatePlaneValues.TOP_RIGHT_8,
            CoordinatePlaneValues.BOTTOM_DOWN_12 -> {
                originPoint =
                    PointF(firstPoint.x - 200f, firstPoint.y)
                originPointAdjustment =
                    PointF(originPoint.x - 8f, originPoint.y - 7f)
                firstPointAdjustment = PointF(firstPoint.x + 12f, firstPoint.y + 9f)
            }
            CoordinatePlaneValues.BOTTOM_RIGHT_6,
            CoordinatePlaneValues.BOTTOM_RIGHT_7,
            CoordinatePlaneValues.TOP_LEFT_JEE -> {
                originPoint =
                    PointF(firstPoint.x + secondPointDeltaX, firstPoint.y - secondPointDeltaY)
                originPointAdjustment =
                    PointF(originPoint.x - 8f, originPoint.y - 7f)
                firstPointAdjustment = PointF(firstPoint.x + 12f, firstPoint.y + 9f)
            }

        }

        val connectingLine = drawRelatedConceptLine(firstPointAdjustment, originPoint)
        val connectingPoint =
            generateRelatedConceptPoints(originPointAdjustment, null, -1, direction)
        //first time animation setting for related concepts
        connectingLine.animation = currentSelectedAnimation
        connectingPoint.first.animation = currentSelectedAnimation
        connectingPoint.second.animation = currentSelectedAnimation

        val relatedViewsList = ArrayList<Pair<LineView, Pair<ImageView, TextView>>>()
        /**
         * This is fixed implementation of showing first 3 related concepts
         */
        relatedConcepts?.forEachIndexed { index, relatedConcept ->
            if (index < 3) {
                val relatedConceptPoint = calculateRelatedKeyConceptPoints(
                    index, relatedConcepts.size, originPoint, relatedConceptDeltaX1,
                    relatedConceptDeltaX2, relatedConceptDeltaY1, relatedConceptDeltaY2, direction
                )
                val lineEndPoint = relatedConceptPoint.first
                val pointEndPoint = relatedConceptPoint.second
                val line = drawRelatedConceptLine(originPoint, lineEndPoint)
                val point =
                    generateRelatedConceptPoints(
                        pointEndPoint,
                        relatedConcept.conceptName,
                        index,
                        direction
                    )
                line.animation = currentSelectedAnimation
                point.first.animation = currentSelectedAnimation
                point.second.animation = currentSelectedAnimation
                relatedViewsList.add(Pair(line, point))
            }
        }

        relatedConceptsViewsList.add(
            RelatedConceptView(
                connectingLine,
                connectingPoint,
                relatedViewsList
            )
        )

    }

    private fun calculateRelatedKeyConceptPoints(
        index: Int,
        relatedConceptsSize: Int,
        originPoint: PointF,
        relatedConceptDeltaX1: Float,
        relatedConceptDeltaX2: Float,
        relatedConceptDeltaY1: Float,
        relatedConceptDeltaY2: Float,
        direction: Pair<Float, Float>
    ): Pair<PointF, PointF> {
        //initializing points
        var lineEndPoint = PointF(originPoint.x, originPoint.y)
        var pointEndPoint = PointF(originPoint.x, originPoint.y)
        when (index) {
            0 -> {
                when (direction) {
                    CoordinatePlaneValues.TOP_LEFT_JEE,
                    CoordinatePlaneValues.BOTTOM_RIGHT_6,
                    CoordinatePlaneValues.BOTTOM_RIGHT_7 -> {
                        lineEndPoint = PointF(
                            originPoint.x + relatedConceptDeltaX1 + 260f,
                            originPoint.y + relatedConceptDeltaY1 + 60f
                        )
                        pointEndPoint = PointF(
                            originPoint.x + relatedConceptDeltaX2 + 260f,
                            originPoint.y + relatedConceptDeltaY2 + 95f
                        )
                    }
                    CoordinatePlaneValues.TOP_RIGHT_8,
                    CoordinatePlaneValues.BOTTOM_DOWN_12 -> {
                        lineEndPoint = PointF(
                            originPoint.x - 80f,
                            originPoint.y + 150f
                        )
                        pointEndPoint = PointF(
                            originPoint.x - 90f,
                            originPoint.y + 140f
                        )
                    }
                    else -> {
                        lineEndPoint = PointF(
                            originPoint.x + relatedConceptDeltaX1,
                            originPoint.y + relatedConceptDeltaY1
                        )
                        pointEndPoint = PointF(
                            originPoint.x + relatedConceptDeltaX2,
                            originPoint.y + relatedConceptDeltaY2 + 30f
                        )
                    }
                }

            }
            1 -> {
                when {
                    relatedConceptsSize == 2 -> {
                        lineEndPoint = PointF(
                            originPoint.x + relatedConceptDeltaX1 + 200f,
                            originPoint.y + relatedConceptDeltaY1 - 20f
                        )
                        pointEndPoint = PointF(
                            originPoint.x + relatedConceptDeltaX2 + 200f,
                            originPoint.y + relatedConceptDeltaY2 + 10f
                        )
                    }
                    direction == (CoordinatePlaneValues.TOP_RIGHT_8) -> {
                        lineEndPoint = PointF(
                            originPoint.x - 100f,
                            originPoint.y
                        )
                        pointEndPoint = PointF(
                            originPoint.x - 105f,
                            originPoint.y - 7f
                        )
                    }
                    direction == (CoordinatePlaneValues.BOTTOM_DOWN_12) -> {
                        lineEndPoint = PointF(
                            originPoint.x - 100f,
                            originPoint.y
                        )
                        pointEndPoint = PointF(
                            originPoint.x - 105f,
                            originPoint.y - 7f
                        )
                    }
                    else -> {
                        lineEndPoint = PointF(
                            originPoint.x + relatedConceptDeltaX1 + 100f,
                            originPoint.y + relatedConceptDeltaY1 - 50f
                        )
                        pointEndPoint = PointF(
                            originPoint.x + relatedConceptDeltaX2 + 100f,
                            originPoint.y + relatedConceptDeltaY2 - 20f
                        )
                    }
                }
            }
            2 -> {
                when (direction) {
                    CoordinatePlaneValues.BOTTOM_LEFT_11,
                    CoordinatePlaneValues.CENTER_RIGHT_9 -> {
                        lineEndPoint = PointF(
                            originPoint.x + relatedConceptDeltaX1 - 50f,
                            originPoint.y + relatedConceptDeltaY1 + 60f
                        )
                        pointEndPoint = PointF(
                            originPoint.x + relatedConceptDeltaX2 - 50f,
                            originPoint.y + relatedConceptDeltaY2 + 95f
                        )
                    }
                    CoordinatePlaneValues.TOP_RIGHT_8,
                    CoordinatePlaneValues.BOTTOM_DOWN_12 -> {
                        lineEndPoint = PointF(
                            originPoint.x - 50f,
                            originPoint.y - 150f
                        )
                        pointEndPoint = PointF(
                            originPoint.x - 58f,
                            originPoint.y - 160f
                        )
                    }
                    else -> {
                        lineEndPoint = PointF(
                            originPoint.x + relatedConceptDeltaX1 + 200f,
                            originPoint.y + relatedConceptDeltaY1 - 20f
                        )
                        pointEndPoint = PointF(
                            originPoint.x + relatedConceptDeltaX2 + 200f,
                            originPoint.y + relatedConceptDeltaY2 + 10f
                        )
                    }
                }
            }
        }

        return Pair(lineEndPoint, pointEndPoint)
    }

    private fun updateFocusForThisView(view: View, requestFocus: Boolean) {
        coroutineScope?.launch {
            delay(10)
            view.isFocusable = requestFocus
            if (requestFocus)
                view.requestFocus()
            else
                view.clearFocus()
        }
    }

    private fun defaultSphereBg(imageView: ImageView) {
        imageView.defaultBg()
    }

    private fun changeSphereBg(imageView: ImageView) {
        imageView.changeFocusBg()
    }

    @JvmOverloads
    fun ConstraintLayout.scale(x: Float = 1.5f, y: Float = 1.5f) {
        this.scaleX = x
        this.scaleY = y
    }

    private fun makeKeyConceptsAPICall(
        sphere: ConstraintLayout,
        selectedSphere: ConstraintLayout,
        triangle: ConstraintLayout,
        connection: LineView,
        direction: Pair<Float, Float>
    ) {
        val sphereTag = sphere.tag.toString()
        val selectedGrade = (sphere[SPHERE_TEXT_INDEX] as TextView).text.toString()
        Log.i(classTag, "Selected Grade = $selectedGrade")
        var fromGoal = ""
        var toGoal = ""
        var fromGrade = ""
        var toGrade = ""
        percentageConnectionsList.forEach { percentageData ->
            if (currentGrade == percentageData.toGrade && selectedGrade == percentageData.fromGrade) {
                toGrade = currentGrade
                fromGrade = selectedGrade
                toGoal = percentageData.toGoal.toString()
                fromGoal = percentageData.fromGoal.toString()
            } else if (currentGrade == percentageData.fromGrade && selectedGrade == percentageData.toGrade) {
                toGrade = selectedGrade
                fromGrade = currentGrade
                toGoal = percentageData.toGoal.toString()
                fromGoal = percentageData.fromGoal.toString()
            }
        }
        bindKeyConceptsData(
            fromGoal, toGoal,
            fromGrade, toGrade, "concept","10", sphere, selectedSphere, triangle, connection, direction
        )
    }

    private fun setSphereSelection(
        sphere: ConstraintLayout,
        selectedSphere: ConstraintLayout,
        triangle: ConstraintLayout,
        connection: LineView,
        direction: Pair<Float, Float>,
        keyConcepts: ArrayList<KeyConceptModel>?
    ) {
        (selectedSphere[SPHERE_TEXT_INDEX] as TextView).text =
            (sphere[SPHERE_TEXT_INDEX] as TextView).text //setting text of previous sphere to selected sphere
        updateFocusForThisView(selectedSphere, true)
        currentSelectedAnimation = selectedSphere.animation
        toggleFocusOnSpheres(false)
        binding.lvDiscoverWhereYouStand.isFocusable = false
        selectedSphere.visibility = VISIBLE

        drawLine(sphere, direction, keyConcepts)

        //alpha animators of views
        val sphereAlphaAnimator = ObjectAnimator.ofFloat(sphere, ALPHA, 1f, 0f).setDuration(10)
        val triangleAlphaAnimator = ObjectAnimator.ofFloat(triangle, ALPHA, 1f, 0f).setDuration(10)
        val lineAlphaAnimator = ObjectAnimator.ofFloat(connection, ALPHA, 1f, 0f).setDuration(10)
        val discoverWhereYouStandAlphaAnimator =
            ObjectAnimator.ofFloat(binding.lvDiscoverWhereYouStand, ALPHA, 0.7f, 0f)
                .setDuration(500)
        val discoverWhereYouStandTextAlphaAnimator =
            ObjectAnimator.ofFloat(binding.tvDiscoverWhereYouStand, ALPHA, 1f, 0f).setDuration(500)
        val selectedSphereAlphaAnimator =
            ObjectAnimator.ofFloat(selectedSphere, ALPHA, 0f, 1f).setDuration(10)
        val selectedLineAlphaAnimator =
            ObjectAnimator.ofFloat(selectedLine, ALPHA, 0f, 1f).setDuration(700)

        //sphere BG particles animators
        val scaleBgParticlesXPVH = PropertyValuesHolder.ofFloat(SCALE_X, 1f, 1.1f)
        val scaleBgParticlesYPVH = PropertyValuesHolder.ofFloat(SCALE_Y, 1f, 1.1f)
        val particlesScaleAnimator = ObjectAnimator.ofPropertyValuesHolder(
            binding.sphereBgParticles,
            scaleBgParticlesXPVH,
            scaleBgParticlesYPVH
        ).setDuration(500)
        val particlesAlphaAnimator =
            ObjectAnimator.ofFloat(binding.sphereBgParticles, ALPHA, 0.7f, 0.2f).setDuration(500)

        //scale-up
        val scaleUpXPVH = PropertyValuesHolder.ofFloat(SCALE_X, 1f, 1.5f)
        val scaleUpYPVH = PropertyValuesHolder.ofFloat(SCALE_Y, 1f, 1.5f)
        val sphereScaleUpAnimator =
            ObjectAnimator.ofPropertyValuesHolder(selectedSphere, scaleUpXPVH, scaleUpYPVH)
                .setDuration(1000)
        //translate and scale down
        val scaleDownXPVH = PropertyValuesHolder.ofFloat(SCALE_X, 1f, 0.4f)
        val scaleDownYPVH = PropertyValuesHolder.ofFloat(SCALE_Y, 1f, 0.4f)
        val translateXPVH = PropertyValuesHolder.ofFloat(TRANSLATION_X, direction.first)
        val translateYPVH = PropertyValuesHolder.ofFloat(TRANSLATION_Y, direction.second)
        val spheresViewScaleTransAnimator = ObjectAnimator.ofPropertyValuesHolder(
            binding.classesCL,
            scaleDownXPVH,
            scaleDownYPVH,
            translateXPVH,
            translateYPVH
        ).setDuration(700)

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(
            selectedLineAlphaAnimator,
            sphereAlphaAnimator,
            triangleAlphaAnimator,
            lineAlphaAnimator,
            discoverWhereYouStandAlphaAnimator,
            discoverWhereYouStandTextAlphaAnimator,
            selectedSphereAlphaAnimator,
            particlesAlphaAnimator,
            particlesScaleAnimator,
            sphereScaleUpAnimator,
            spheresViewScaleTransAnimator
        )
        animatorSet.start()
    }

    private fun toggleFocusOnSpheres(focus: Boolean) {
        val spheresList = spheresPositionsMap.keys.toList()
        spheresList.forEach { sphere ->
            sphere.isFocusable = focus
        }
    }

    private fun resetSphereSelection(
        sphere: ConstraintLayout,
        selectedSphere: ConstraintLayout,
        triangle: ConstraintLayout,
        connection: LineView,
        pair: Pair<Float, Float>
    ) {
        //scale-down
        val scaleDownXPVH = PropertyValuesHolder.ofFloat(SCALE_X, 1.5f, 1.1f)
        val scaleDownYPVH = PropertyValuesHolder.ofFloat(SCALE_Y, 1.5f, 1.1f)
        val selectedSphereAnimator =
            ObjectAnimator.ofPropertyValuesHolder(selectedSphere, scaleDownXPVH, scaleDownYPVH)
                .apply {
                    duration = 700
                }
        //translate and scale Up
        val scaleUpXPVH = PropertyValuesHolder.ofFloat(SCALE_X, 0.4f, 1f)
        val scaleUpYPVH = PropertyValuesHolder.ofFloat(SCALE_Y, 0.4f, 1f)
        val translateXPVH = PropertyValuesHolder.ofFloat(TRANSLATION_X, pair.first, 0f)
        val translateYPVH = PropertyValuesHolder.ofFloat(TRANSLATION_Y, pair.second, 0f)
        val spheresViewScaleTransAnimator = ObjectAnimator.ofPropertyValuesHolder(
            binding.classesCL,
            scaleUpXPVH,
            scaleUpYPVH,
            translateXPVH,
            translateYPVH
        ).apply {
            duration = 800
            doOnStart {
                resetOtherViews()
            }
            doOnEnd {
                toggleFocusOnSpheres(true)
                updateFocusForThisView(sphere, true)

                val sphereAlphaAnimator =
                    ObjectAnimator.ofFloat(sphere, ALPHA, 0f, 1f).setDuration(200)
                val triangleAlphaAnimator =
                    ObjectAnimator.ofFloat(triangle, ALPHA, 0f, 1f).setDuration(500)
                val connectionAlphaAnimator =
                    ObjectAnimator.ofFloat(connection, ALPHA, 0f, 1f).setDuration(500)

                val selectedSphereAlphaAnimator =
                    ObjectAnimator.ofFloat(selectedSphere, ALPHA, 1f, 0f).setDuration(200)
                val endAnimatorSet = AnimatorSet()
                endAnimatorSet.playTogether(
                    sphereAlphaAnimator,
                    triangleAlphaAnimator,
                    connectionAlphaAnimator,
                    selectedSphereAlphaAnimator
                )
                endAnimatorSet.start()

            }
        }
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(selectedSphereAnimator, spheresViewScaleTransAnimator)
        animatorSet.start()

    }

    private fun resetOtherViews() {
        binding.lvDiscoverWhereYouStand.isFocusable = true
        val discoverWhereYouStandAnimator =
            ObjectAnimator.ofFloat(binding.lvDiscoverWhereYouStand, ALPHA, 0f, 0.7f)
                .setDuration(500)
        val discoverWhereYouStandTextAnimator =
            ObjectAnimator.ofFloat(binding.tvDiscoverWhereYouStand, ALPHA, 0f, 1f).setDuration(500)

        val particlesAlphaAnimator =
            ObjectAnimator.ofFloat(binding.sphereBgParticles, ALPHA, 0.2f, 0.7f).setDuration(500)
        val selectedLineAlphaAnimator =
            ObjectAnimator.ofFloat(selectedLine, ALPHA, 1f, 0f).setDuration(800)
        val scaleDownParticlesXPVH = PropertyValuesHolder.ofFloat(SCALE_X, 1.1f, 1f)
        val scaleDownParticlesYPVH = PropertyValuesHolder.ofFloat(SCALE_Y, 1.1f, 1f)
        val scaleParticlesAnimator = ObjectAnimator.ofPropertyValuesHolder(
            binding.sphereBgParticles,
            scaleDownParticlesXPVH,
            scaleDownParticlesYPVH
        ).setDuration(500)

        removePointsFromLine()
        removeRelatedConceptsView()
        keyConceptsCoordinatesList.clear()

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(
            selectedLineAlphaAnimator,
            particlesAlphaAnimator,
            scaleParticlesAnimator,
            discoverWhereYouStandAnimator,
            discoverWhereYouStandTextAnimator
        )
        animatorSet.start()
    }

    private fun removePointsFromLine() {

        fadeInOutKeyConcepts(1f, 0f, true)

        //Remove point views after key concepts have faded out[Animation]
        view?.postDelayed({
            keyConceptPointsList.forEach { point ->
                point.first.clearAnimation()
                point.second.clearAnimation()
                removeView(point.first)
                removeView(point.second)
            }
            keyConceptPointsList.clear()
        }, pointFadeInOutDuration.toLong())
    }

    private fun fadeInOutKeyConcepts(fromAlpha: Float, toAlpha: Float, removePoint: Boolean) {
        val alphaAnimatorsList = ArrayList<ObjectAnimator>()
        keyConceptPointsList.forEach { point ->
            alphaAnimatorsList.add(ObjectAnimator.ofFloat(point.first, ALPHA, fromAlpha, toAlpha))
            if (removePoint)//in case point is being removed, on d-pad Back need to fade out label as well
                alphaAnimatorsList.add(ObjectAnimator.ofFloat(point.second, ALPHA, toAlpha))
        }
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(alphaAnimatorsList as Collection<Animator>?)
        animatorSet.duration = pointFadeInOutDuration.toLong()
        animatorSet.start()
    }

    private fun removeView(view: View) {
        updateFocusForThisView(view, false)
        var parent = view.parent
        if (parent != null) {
            parent = parent as ConstraintLayout
            parent.removeView(view)
        }
    }

    private fun removeRelatedConceptsView() {
        relatedConceptsViewsList.forEach { relatedConceptView ->
            relatedConceptView.connectingPoint.first.clearAnimation()
            relatedConceptView.connectingPoint.second.clearAnimation()
            relatedConceptView.connectingLine.clearAnimation()
            removeView(relatedConceptView.connectingPoint.first) // remove point
            removeView(relatedConceptView.connectingPoint.second) // remove label
            removeView(relatedConceptView.connectingLine) // remove connecting line
            val relatedConcepts = relatedConceptView.relatedConcepts
            relatedConcepts.forEach { relatedConcept ->
                relatedConcept.first.clearAnimation()
                relatedConcept.second.first.clearAnimation()
                relatedConcept.second.second.clearAnimation()
                removeView(relatedConcept.first) // remove line
                removeView(relatedConcept.second.first) // remove point
                removeView(relatedConcept.second.second) // remove label
            }
        }
        relatedConceptsViewsList.clear()
    }

    private fun drawLine(
        sphere: ConstraintLayout,
        direction: Pair<Float, Float>,
        keyConcepts: ArrayList<KeyConceptModel>?
    ) {
        val line = generateLine()
        selectedLine = line
        selectedLine.animation = currentSelectedAnimation
        val points = sphereSelectionLinePointsMap[sphere.tag]
//        calculatePointsOnLine(sphere, points, direction, keyConcepts)
        val first = points?.first
        val second = points?.second
        line.setPointA(first)
        line.setPointB(second)
        line.isRelatedKeyConcept(false)
        line.reverseGradient(true)
        line.draw()
    }

    private fun drawRelatedConceptLine(
        firstPoint: PointF,
        secondPointF: PointF
    ): LineView {

        val line = generateLine()
        line.setPointA(firstPoint)
        line.setPointB(secondPointF)
        line.isRelatedKeyConcept(true)
        line.draw()

        return line
    }

    private fun calculatePointsOnLine(
        sphere: ConstraintLayout,
        points: Pair<PointF, PointF>?,
        direction: Pair<Float, Float>,
        keyConcepts: ArrayList<KeyConceptModel>?
    ) {
        val firstPoint = points?.first
        val secondPoint = points?.second

        val keyConceptsCount = keyConcepts?.size!!

        var gap = when (direction) {
            CoordinatePlaneValues.TOP_RIGHT_8,
            CoordinatePlaneValues.BOTTOM_RIGHT_7,
            CoordinatePlaneValues.BOTTOM_RIGHT_6 -> sphere.width.toFloat() * 1 / 2
            else -> sphere.width.toFloat() * 3 / 4
        }
        var delta_x_12 = 55
        for (i in 0 until (keyConceptsCount)) {
            val deltaX = when (direction) {
                CoordinatePlaneValues.CENTER_RIGHT_9,
                CoordinatePlaneValues.BOTTOM_RIGHT_6,
                CoordinatePlaneValues.BOTTOM_RIGHT_7,
                CoordinatePlaneValues.TOP_RIGHT_8 -> gap
                CoordinatePlaneValues.BOTTOM_DOWN_12 -> -gap / 2
                else -> -gap
            }
            val deltaY = when (direction) {
                CoordinatePlaneValues.BOTTOM_LEFT_11 -> -17f
                CoordinatePlaneValues.TOP_RIGHT_8 -> -22f
                CoordinatePlaneValues.CENTER_RIGHT_9,
                CoordinatePlaneValues.CENTER_LEFT_NEET -> -12f
                CoordinatePlaneValues.BOTTOM_RIGHT_6 -> -2f
                CoordinatePlaneValues.BOTTOM_DOWN_12 -> 0f
                else -> -7f
            }

            //y = mx + b
            var slope =
                (secondPoint!!.y.minus(firstPoint!!.y)).div(secondPoint.x.minus(firstPoint.x))
            if (direction == CoordinatePlaneValues.BOTTOM_DOWN_12) {
                slope /= 2
            }
            val constant = firstPoint.y.minus(slope.times(firstPoint.x)) + deltaY
            var linePointX = firstPoint.x + deltaX
            var linePointY = linePointX.times(slope).plus(constant)
            Log.i(classTag, "Line Point X = $linePointX Line Point Y = $linePointY")
            if (direction == CoordinatePlaneValues.BOTTOM_DOWN_12) {
                linePointY -= 350
                linePointX += delta_x_12
            }
            generateKeyConceptPoints(linePointX, linePointY, direction, keyConcepts[i], false)
            keyConceptsCoordinatesList.add(PointF(linePointX, linePointY))
            gap += when (direction) {
                CoordinatePlaneValues.TOP_RIGHT_8,
                CoordinatePlaneValues.BOTTOM_RIGHT_6 -> 30f
                CoordinatePlaneValues.BOTTOM_RIGHT_7 -> 50f
                CoordinatePlaneValues.BOTTOM_DOWN_12 -> 20f
                else -> 60f
            }
            delta_x_12 += 5
        }

        fadeInOutKeyConcepts(0f, 1f, false)
    }

    private fun generateKeyConceptPoints(
        linePointX: Float,
        linePointY: Float,
        direction: Pair<Float, Float>,
        keyConcept: KeyConceptModel,
        isRelated: Boolean
    ) {

        Log.i(classTag, "key concept name = ${keyConcept.conceptName}")
        val conceptImageButton = ImageButton(context)
        val conceptIVLP = ConstraintLayout.LayoutParams(20, 20)
        conceptImageButton.apply {
            id = generateViewId()
            background = ContextCompat.getDrawable(context, R.drawable.key_concept_white)
            layoutParams = conceptIVLP
        }
        val conceptTextView = TextView(context)
        val conceptTVLP = ConstraintLayout.LayoutParams(150, ConstraintSet.WRAP_CONTENT)
        when (direction) {
            CoordinatePlaneValues.CENTER_RIGHT_9,
            CoordinatePlaneValues.CENTER_LEFT_NEET -> {
                conceptTVLP.topMargin = 10
                conceptTVLP.topToBottom = conceptImageButton.id
                conceptTVLP.startToStart = conceptImageButton.id
                conceptTVLP.endToEnd = conceptImageButton.id
            }
            CoordinatePlaneValues.BOTTOM_LEFT_11 -> {
                conceptTVLP.rightMargin = 10
                conceptTVLP.topToBottom = conceptImageButton.id
            }
            CoordinatePlaneValues.TOP_LEFT_JEE -> {
                conceptTVLP.topMargin = 20
                conceptTVLP.rightMargin = 10
                conceptTVLP.endToStart = conceptImageButton.id
            }
            CoordinatePlaneValues.BOTTOM_RIGHT_6 -> {
                conceptTVLP.topMargin = 10
                conceptTVLP.endToStart = conceptImageButton.id
            }
            CoordinatePlaneValues.BOTTOM_RIGHT_7 -> {
                conceptTVLP.topMargin = 10
                conceptTVLP.topToBottom = conceptImageButton.id
                conceptTVLP.startToStart = conceptImageButton.id
                conceptTVLP.endToEnd = conceptImageButton.id
            }
            CoordinatePlaneValues.TOP_RIGHT_8 -> {
                conceptTVLP.leftMargin = 20
                conceptTVLP.startToEnd = conceptImageButton.id
            }
            CoordinatePlaneValues.BOTTOM_DOWN_12 -> {
                conceptTVLP.leftMargin = 20
                conceptTVLP.startToEnd = conceptImageButton.id
                conceptTVLP.topToTop = conceptImageButton.id
                conceptTVLP.bottomToBottom = conceptImageButton.id
            }
        }
        conceptTextView.apply {
            id = generateViewId()
            text = keyConcept.conceptName
            alpha = 0f
            setTextColor(ContextCompat.getColor(context, R.color.line_grad_1))
            textSize = 12f
            layoutParams = conceptTVLP
            when (direction) {
                CoordinatePlaneValues.BOTTOM_LEFT_11 -> {
                    gravity = Gravity.CENTER
                }
                CoordinatePlaneValues.TOP_LEFT_JEE -> {
                    gravity = Gravity.END
                }
                CoordinatePlaneValues.BOTTOM_RIGHT_6 -> {
                    gravity = Gravity.CENTER
                }
                CoordinatePlaneValues.BOTTOM_RIGHT_7 -> {
                    gravity = Gravity.START
                }
            }
        }

        binding.discoverCL.addView(conceptImageButton, 3)
        binding.discoverCL.addView(conceptTextView, 3)
        keyConceptPointsList.add(Pair(conceptImageButton, conceptTextView))

        val conceptLayoutPVHX = PropertyValuesHolder.ofFloat(TRANSLATION_X, linePointX)
        val conceptLayoutPVHY = PropertyValuesHolder.ofFloat(TRANSLATION_Y, linePointY)
        val conceptIBTransAnimator = ObjectAnimator.ofPropertyValuesHolder(
            conceptImageButton,
            conceptLayoutPVHX,
            conceptLayoutPVHY
        )
        val conceptTVTransAnimator = ObjectAnimator.ofPropertyValuesHolder(
            conceptTextView,
            conceptLayoutPVHX,
            conceptLayoutPVHY
        )

        conceptImageButton.animation = currentSelectedAnimation
        conceptTextView.animation = currentSelectedAnimation
        val animatorSet = AnimatorSet()
        animatorSet.duration = 0
        animatorSet.playTogether(conceptIBTransAnimator, conceptTVTransAnimator)
        animatorSet.start()
    }

    private fun generateRelatedConceptPoints(
        point: PointF,
        relatedConcept: String?,
        position: Int,
        direction: Pair<Float, Float>
    ): Pair<ImageView, TextView> {

        val conceptImageView = ImageView(context)
        val conceptIVLP = ConstraintLayout.LayoutParams(20, 20)

        conceptImageView.apply {
            id = generateViewId()
            setImageResource(R.drawable.key_concept)
            layoutParams = conceptIVLP
        }

        val conceptTextView = TextView(context)
        val conceptTVLP = ConstraintLayout.LayoutParams(220, ConstraintSet.WRAP_CONTENT)
        when (position) {
            0 -> {
                when (direction) {
                    CoordinatePlaneValues.TOP_LEFT_JEE,
                    CoordinatePlaneValues.BOTTOM_RIGHT_7,
                    CoordinatePlaneValues.BOTTOM_RIGHT_6 -> {
                        placeConceptOnRight(conceptTVLP, conceptImageView)
                    }
                    else -> {
                        placeConceptOnLeft(conceptTVLP, conceptImageView)
                    }
                }
            }
            1 -> {
                when (direction) {
                    CoordinatePlaneValues.TOP_RIGHT_8,
                    CoordinatePlaneValues.BOTTOM_DOWN_12 -> placeConceptOnLeft(
                        conceptTVLP,
                        conceptImageView
                    )
                    else -> placeConceptOnTop(conceptTVLP, conceptImageView)
                }
            }
            2 -> {
                when (direction) {
                    CoordinatePlaneValues.BOTTOM_LEFT_11 -> {
                        placeConceptOnBottom(conceptTVLP, conceptImageView)
                    }
                    CoordinatePlaneValues.CENTER_RIGHT_9,
                    CoordinatePlaneValues.TOP_RIGHT_8,
                    CoordinatePlaneValues.BOTTOM_DOWN_12 -> {
                        placeConceptOnLeft(conceptTVLP, conceptImageView)
                    }
                    else -> {
                        placeConceptOnRight(conceptTVLP, conceptImageView)
                    }
                }
            }
        }

        var conceptName = relatedConcept ?: ""
        if (conceptName.length >= 50 && position != 1) {
            conceptName = conceptName.substring(0, 50) + "..."
        }
        conceptTextView.apply {
            id = generateViewId()
            text = conceptName
            alpha = 1f
            setTextColor(ContextCompat.getColor(context, R.color.line_grad_1))
            textSize = 11f
            maxLines = 3
            layoutParams = conceptTVLP
            when (position) {
                0 -> {
                    gravity =
                        when (direction) {
                            CoordinatePlaneValues.TOP_LEFT_JEE,
                            CoordinatePlaneValues.BOTTOM_RIGHT_7,
                            CoordinatePlaneValues.BOTTOM_RIGHT_6 -> {
                                Gravity.START
                            }
                            else -> {
                                Gravity.END
                            }
                        }
                }
                1 -> {
                    gravity = when (direction) {
                        CoordinatePlaneValues.TOP_RIGHT_8,
                        CoordinatePlaneValues.BOTTOM_DOWN_12 -> Gravity.END
                        else -> Gravity.CENTER
                    }
                }
                2 -> {
                    gravity = when (direction) {
                        CoordinatePlaneValues.BOTTOM_LEFT_11 -> {
                            Gravity.CENTER
                        }
                        CoordinatePlaneValues.TOP_RIGHT_8,
                        CoordinatePlaneValues.CENTER_RIGHT_9,
                        CoordinatePlaneValues.BOTTOM_DOWN_12 -> {
                            Gravity.END
                        }
                        else -> {
                            Gravity.START
                        }
                    }
                }
            }
        }

        binding.discoverCL.addView(conceptTextView, 3)
        binding.discoverCL.addView(conceptImageView, 3)

        val conceptTVPVHX = PropertyValuesHolder.ofFloat(TRANSLATION_X, point.x)
        val conceptTVPVHY = PropertyValuesHolder.ofFloat(TRANSLATION_Y, point.y)
        val conceptIVPVHX = PropertyValuesHolder.ofFloat(TRANSLATION_X, point.x)
        val conceptIVPVHY = PropertyValuesHolder.ofFloat(TRANSLATION_Y, point.y)

        val conceptTVTransAnimator =
            ObjectAnimator.ofPropertyValuesHolder(conceptTextView, conceptTVPVHX, conceptTVPVHY)
                .apply {
                    duration = 0
                }
        val conceptIVTransAnimator =
            ObjectAnimator.ofPropertyValuesHolder(conceptImageView, conceptIVPVHX, conceptIVPVHY)
                .apply {
                    duration = 0
                }

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(conceptTVTransAnimator, conceptIVTransAnimator)
        animatorSet.start()

        return Pair(conceptImageView, conceptTextView)
    }

    private fun placeConceptOnBottom(
        conceptTVLP: ConstraintLayout.LayoutParams,
        conceptImageView: ImageView
    ) {
        conceptTVLP.topToBottom = conceptImageView.id
        conceptTVLP.startToStart = conceptImageView.id
        conceptTVLP.endToEnd = conceptImageView.id
        conceptTVLP.topMargin = 20
    }

    private fun placeConceptOnTop(
        conceptTVLP: ConstraintLayout.LayoutParams,
        conceptImageView: ImageView
    ) {
        conceptTVLP.bottomToTop = conceptImageView.id
        conceptTVLP.startToStart = conceptImageView.id
        conceptTVLP.endToEnd = conceptImageView.id
        conceptTVLP.bottomMargin = 10
    }

    private fun placeConceptOnLeft(
        conceptTVLP: ConstraintLayout.LayoutParams,
        conceptImageView: ImageView
    ) {
        conceptTVLP.endToStart = conceptImageView.id
        conceptTVLP.topToTop = conceptImageView.id
        conceptTVLP.bottomToBottom = conceptImageView.id
        conceptTVLP.marginEnd = 20
    }

    private fun placeConceptOnRight(
        conceptTVLP: ConstraintLayout.LayoutParams,
        conceptImageView: ImageView
    ) {
        conceptTVLP.startToEnd = conceptImageView.id
        conceptTVLP.topToTop = conceptImageView.id
        conceptTVLP.bottomToBottom = conceptImageView.id
        conceptTVLP.marginStart = 20
    }

    private fun generateLine(): LineView {
        val lineView = LineView(context)
        val constraintSet = ConstraintSet()
        constraintSet.clone(binding.discoverCL)
        lineView.apply {
            id = generateViewId()
        }
        binding.discoverCL.addView(lineView, 2)
        constraintSet.constrainHeight(lineView.id, ConstraintSet.WRAP_CONTENT)
        constraintSet.constrainWidth(lineView.id, ConstraintSet.WRAP_CONTENT)
        constraintSet.applyTo(binding.discoverCL)

        return lineView
    }

    private fun calculateDistance(x1: Float, y1: Float, x2: Float, y2: Float): Float {
        return sqrt((x2 - x1).toDouble().pow(2.0) + (y2 - y1).toDouble().pow(2.0)).toFloat()
    }

    //ToRefactor: remove setInitialView1,2,3 once library is decided
    /**
     * with aPng4Android
     */
    private fun setInitialView3() {
        setHeader()
        updateGlobeVisibility(VISIBLE)
        val assetLoader = ResourceStreamLoader(context, R.drawable.k_graph_p1)
        val aPngDrawable = APNGDrawable(assetLoader)
        aPngDrawable.setLoopLimit(1)
        binding.ivGlobeExpanding.setImageDrawable(aPngDrawable)
        aPngDrawable.registerAnimationCallback(object : Animatable2Compat.AnimationCallback() {
            override fun onAnimationStart(drawable: Drawable?) {
                fadeInEnterButton()
            }

            override fun onAnimationEnd(drawable: Drawable?) {
                if (!isEnterClicked) {
                    fadeInExpandedGlobe()
                    fadeOutExpandingGlobe()
                }

            }
        })

        binding.lvDownPointer.apply {
            setAnimation(R.raw.chevron_animation_two)
            playAnimation()
        }
    }

    /**
     * with glide
     */
    private fun setInitialView2() {
        updateGlobeVisibility(VISIBLE)
        requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(requireContext())
            .load(R.drawable.k_graph_part_1_new)
            .apply(requestOptions).listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    fadeInEnterButton()
                    (resource as WebpDrawable).loopCount = 1
                    resource.registerAnimationCallback(object :
                        Animatable2Compat.AnimationCallback() {
                        override fun onAnimationEnd(drawable: Drawable) {
                            fadeInExpandedGlobe()
                            fadeOutExpandingGlobe()
                        }
                    })
                    return false
                }
            })
            .into(binding.ivGlobeExpanding)

        binding.lvDownPointer.apply {
            setAnimation(R.raw.chevron_animation_two)
            playAnimation()
        }
    }

    /**
     * with fresco
     */
    private fun setInitialView() {
        setHeader()
        updateGlobeVisibility(VISIBLE)
        requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
//        playWebPAnimationWithFresco(binding.ivGlobeExpanding, R.drawable.k_graph_part_1_new, true)

        binding.lvDownPointer.apply {
            setAnimation(R.raw.chevron_animation_two)
            playAnimation()
        }
    }

    private fun setHeader() {
        val allLearningTranslationFirst =
            getString(R.string.all_learning_is_connected_including_all_subjects_first)
        val allLearningTranslationSecond =
            getString(R.string.all_learning_is_connected_including_all_subjects_second)
        val classTranslation = getString(R.string.this_class)
        if (Utils.getCurrentLocale() == "en")
            binding.tvAllLearning.text =
                "$allLearningTranslationFirst $currentGoal $classTranslation $currentGrade $allLearningTranslationSecond"
        else if (Utils.getCurrentLocale() == "hi") {
            binding.tvAllLearning.text =
                "$allLearningTranslationFirst $currentGoal $currentGrade $classTranslation $allLearningTranslationSecond"
        }
    }

    private fun playWebPAnimationWithFresco(
        view: SimpleDraweeView,
        resource: Int,
        setController: Boolean
    ) {
        val controller = Fresco.newDraweeControllerBuilder()
        if (setController) {
            controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                override fun onFinalImageSet(
                    id: String?,
                    imageInfo: ImageInfo?,
                    animatable: Animatable?
                ) {
                    val anim = animatable as AnimatedDrawable2
                    anim.setAnimationListener(object : AnimationListener {
                        override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                        override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                            fadeInEnterButton()
                        }

                        override fun onAnimationFrame(
                            drawable: AnimatedDrawable2?,
                            frameNumber: Int
                        ) {
                        }

                        override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                            fadeInExpandedGlobe()
                            fadeOutExpandingGlobe()
                            Log.i(classTag, "K-graph 1 animation ended")
                        }

                        override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                    })
                }
            }
        }
        controller.autoPlayAnimations = true
        val res = "res:/" + resource
        controller.setUri(Uri.parse(res))
        view.controller = controller.build()
    }

    override fun onDestroy() {
//        binding.ivGlobeExpanding.visibility = GONE
//        if (!activity?.isFinishing!!) {
//            Glide.with(requireContext()).clear(binding.ivGlobeExpanding)
//        }
        Log.i(classTag, "onDestroy")
        super.onDestroy()
    }

    private fun fadeInExpandedGlobe() {
        binding.ivGlobeExpanded.apply {
            alpha = 0f
            visibility = VISIBLE
//            Glide.with(requireContext()).load(R.drawable.k_graph_part_2_loop_new).into(this)
//            playWebPAnimationWithFresco(
//                binding.ivGlobeExpanded,
//                R.drawable.k_graph_part_2_loop_new,
//                false
//            )
            val resourceStreamLoader = ResourceStreamLoader(context, R.drawable.k_graph_p2)
            val aPngDrawable = APNGDrawable(resourceStreamLoader)
            setImageDrawable(aPngDrawable)
            animate().apply {
                alpha(1f)
                duration = 300
            }
        }
    }

    private fun fadeInEnterButton() {
        binding.lvEnterKey.apply {
            alpha = 0f
            visibility = VISIBLE
            animate().apply {
                alpha(1f)
                duration = 5000
            }
            postDelayed({
                this.requestFocus()
            }, 10)
        }
        loadLottieAnimation(binding.lvEnterKey, R.raw.enter_button_new, true)
        loadLottieAnimation(binding.lvEnterKey, R.raw.enter_button_new, false)
    }

    private fun fadeOutExpandingGlobe() {
        binding.ivGlobeExpanding.apply {
            alpha = 1f
            animate().apply {
                alpha(0f)
                duration = 500
                setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator?) {
                        binding.ivGlobeExpanding.visibility = GONE
                    }
                })
            }
        }
    }

    private fun discoverWhereYouStandListeners() {

        binding.lvDiscoverWhereYouStand.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        binding.discoverCL.visibility = GONE
                        binding.flAchievers.visibility = GONE
                        readinessFragment = ReadinessFragment()
                        fragmentReplacer(binding.flReadiness.id, readinessFragment)
                        removeFocusFromRightArrow()
                        toggleShadowVisibilityForDiscoverArrow(GONE)
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT, KeyEvent.KEYCODE_DPAD_DOWN,
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        centerSphere.isFocusable = false
                        sphereNEET.postDelayed({
                            sphereNEET.requestFocus()
                        }, 10)
                        stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                        toggleShadowVisibilityForDiscoverArrow(GONE)
                    }
                }
            }
            false
        }
    }

    private fun removeFocusFromRightArrow() {
        binding.lvDiscoverWhereYouStand.postDelayed({
            binding.lvDiscoverWhereYouStand.clearFocus()
        }, 10)
        stopLottieAnimation(binding.lvDiscoverWhereYouStand)
    }

    private fun enterButtonKeyListeners() {

        binding.lvEnterKey.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                        isEnterClicked = true
                        setGoalsSpheresView()
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navigationMenuCallback.navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_UP, KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        binding.lvEnterKey.postDelayed({
                            binding.lvEnterKey.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        showAchievers(600)
                    }
                }
            }
            false
        }
    }

    private fun setGoalsSpheresView() {
        binding.lvEnterKey.visibility = GONE
        hideGlobe()
        updateGlobeVisibility(GONE)
        if (isReadinessDataAvailable && isFutureSuccessDataAvailable) {
            loadLottieAnimation(
                binding.lvDiscoverWhereYouStand,
                R.raw.discover_where_you_stand_no_text, true
            )
            binding.tvDiscoverWhereYouStand.visibility = VISIBLE
        }
        val sphereBgParticlesAnimator =
            ObjectAnimator.ofFloat(binding.sphereBgParticles, ALPHA, 0f, 0.7f).apply {
                duration = 500
            }
        val sphereAnimator = ObjectAnimator.ofFloat(binding.classesCL, ALPHA, 0f, 1f).apply {
            duration = 500
        }
        val animatorSet = AnimatorSet()
        animatorSet.apply {
            playTogether(sphereBgParticlesAnimator, sphereAnimator)

            doOnStart {
                loadLottieAnimation(binding.sphereBgParticles, R.raw.circle_dots_2, true)
                loadLottieAnimation(binding.sphereBgParticles, R.raw.circle_dots_2, false)
                setSpheres()
            }

            start()
        }

    }

    private fun hideGlobe() {
        binding.ivGlobeExpanded.visibility = GONE
        binding.ivGlobeExpanding.visibility = GONE
    }

    private fun updateGlobeVisibility(visibility: Int) {
        binding.ivGlobeExpanding.visibility = visibility
        binding.tvAllLearning.visibility = visibility
    }

    private fun setSpheres() {
        binding.ivGlobeExpanded.visibility = GONE
        binding.classesCL.visibility = VISIBLE
        //getting default focus on center sphere
        (centerSphere[1] as TextView).text = currentGrade
        centerSphere.postDelayed({
            centerSphere.requestFocus()
        }, 10)

        val float1Anim = AnimationUtils.loadAnimation(context, R.anim.anim_float_random_1)
        val float2Anim = AnimationUtils.loadAnimation(context, R.anim.anim_float_random_2)
        val float3Anim = AnimationUtils.loadAnimation(context, R.anim.anim_float_random_3)

        spheresPositionsMap.forEach { (sphere, spherePosition) ->
            if (sphere.tag != "CenterSphere" && (sphere[SPHERE_TEXT_INDEX] as TextView).text.toString() != currentGrade) {
                val triangle = sphereTriangleMap[sphere]
                val triangleValues = trianglesValuesMap[triangle]!!
                val trianglePosition = triangleValues.trianglePosition
                triangle?.animate()?.apply {
                    translationX(trianglePosition.x)
                    translationY(trianglePosition.y)
                    duration = 1200
                }
                val connection = showLine(sphere)
                sphere.animate()?.apply {
                    translationX(spherePosition.x)
                    translationY(spherePosition.y)
                    duration = 1000
                    withEndAction {
                        val randomFloatAnim = listOf(float3Anim, float1Anim, float2Anim).random()
                        sphere.startAnimation(randomFloatAnim)
                        triangle?.startAnimation(randomFloatAnim)
                        connection?.startAnimation(randomFloatAnim)
                    }
                }
                setSelectedSphereTranslation(sphere, spherePosition)
            }
        }
    }

    private fun setSelectedSphereTranslation(
        originalSphere: ConstraintLayout,
        spherePosition: PointF
    ) {
        when (originalSphere.tag) {
            "Sphere9" -> {
                setSelectedSphereAnimation(sphere9selected, originalSphere, spherePosition)
            }
            "Sphere8" -> {
                setSelectedSphereAnimation(sphere8selected, originalSphere, spherePosition)
            }
            "Sphere7" -> {
                setSelectedSphereAnimation(sphere7selected, originalSphere, spherePosition)
            }
            "Sphere6" -> {
                setSelectedSphereAnimation(sphere6selected, originalSphere, spherePosition)
            }
            "Sphere11" -> {
                setSelectedSphereAnimation(sphere11selected, originalSphere, spherePosition)
            }
            "Sphere12" -> {
                setSelectedSphereAnimation(sphere12selected, originalSphere, spherePosition)
            }
            "SphereNEET" -> {
                setSelectedSphereAnimation(sphereNEETselected, originalSphere, spherePosition)
            }
            "SphereJEE" -> {
                setSelectedSphereAnimation(sphereJEEselected, originalSphere, spherePosition)
            }
        }
    }

    private fun setSelectedSphereAnimation(
        selectedSphere: ConstraintLayout,
        originalSphere: ConstraintLayout,
        spherePosition: PointF
    ) {
        selectedSphere.animate()?.apply {
            translationX(spherePosition.x)
            translationY(spherePosition.y)
            duration = 1000
            withEndAction {
                selectedSphere.startAnimation(originalSphere.animation)
            }
        }
    }

    private fun showLine(
        sphere: ConstraintLayout
    ): LineView? {
        var lineView: LineView? = null
        when (sphere.tag) {
            "Sphere9" -> {
                setSphereConnection(binding.connection109)
                lineView = binding.connection109
            }
            "Sphere11" -> {
                setSphereConnection(binding.connection1011)
                lineView = binding.connection1011
            }
            "Sphere12" -> {
                setSphereConnection(binding.connection1012)
                lineView = binding.connection1012
            }
            "Sphere6" -> {
                setSphereConnection(binding.connection106)
                lineView = binding.connection106
            }
            "Sphere7" -> {
                setSphereConnection(binding.connection107)
                lineView = binding.connection107
            }
            "Sphere8" -> {
                setSphereConnection(binding.connection108)
                lineView = binding.connection108
            }
            "SphereJEE" -> {
                setSphereConnection(binding.connection10JEE)
                lineView = binding.connection10JEE
            }
            "SphereNEET" -> {
                setSphereConnection(binding.connection10NEET)
                lineView = binding.connection10NEET
            }
        }

        return lineView
    }

    private fun setSphereConnection(
        connection: LineView
    ) {
//        getRelativeCoordinates(binding.centerSphere)
        val linePoints = linesMap[connection]
        connection.setPointA(linePoints?.startPoint)
        connection.setPointB(linePoints?.endPoint)
        connection.draw()
        connection.animate().apply {
            scaleX(1f)
            scaleY(1f)
            duration = 1200
        }
    }

    private fun getRelativeCoordinates(sphere: ConstraintLayout) {
//        val arr = IntArray(2)
//        sphere.getLocationOnScreen(arr)

        val offsetViewBounds = Rect()
        //returns the visible bounds
        sphere.postDelayed({
            sphere.getDrawingRect(offsetViewBounds)
            Log.i(
                classTag,
                "child - ${offsetViewBounds.exactCenterX()} ${offsetViewBounds.exactCenterY()}"
            )
        }, 1000)

        // calculates the relative coordinates to the parent
        binding.classesCL.postDelayed({
            binding.classesCL.offsetDescendantRectToMyCoords(sphere, offsetViewBounds)
            Log.i(
                classTag,
                "parent1 - ${offsetViewBounds.exactCenterX()} ${offsetViewBounds.exactCenterY()}"
            )
        }, 1000)
    }

    private fun distanceBetweenTwoPoints(p1: PointF, p2: PointF): Int {
        return sqrt(
            (p2.x - p1.x).pow(2) + (p2.y - p1.y).pow(2)
        ).toInt()
    }

    private fun angleOf(p1: PointF, p2: PointF): Double {
        // NOTE: Remember that most math has the Y axis as positive above the X.
        // However, for screens we have Y as positive below. For this reason,
        // the Y values are inverted to get the expected results.
        val deltaY = (p1.y - p2.y).toDouble()
        val deltaX = (p2.x - p1.x).toDouble()
        val result = Math.toDegrees(atan2(deltaY, deltaX))
        return if (result < 0) 360.0 + result else result
    }

    private fun printPropertyLogs(it: Map.Entry<Button, PointF>) {
        Log.i(classTag, "class${it.key.text} x = ${it.key.x} class${it.key.text} y = ${it.key.y}")
        Log.i(
            classTag,
            "class${it.key.text} left = ${it.key.left} class${it.key.text} right = ${it.key.right}"
        )
        Log.i(
            classTag,
            "class${it.key.text} top = ${it.key.top} class${it.key.text} bottom = ${it.key.bottom}"
        )
        Log.i(
            classTag,
            "class${it.key.text} Px = ${it.key.pivotX} class${it.key.text} Py = ${it.key.pivotY}"
        )
//      Log.i(classTag, "class${it.key.text} Rx = ${it.key.rotationX} class${it.key.text} Ry = ${it.key.rotationY}")
    }


    private fun loadSpheres(
        currentGrade: String,
        percentageData: ArrayList<PercentageData>?
    ) {
        //this arrangement is fo 10
        spheresPositionsMap[centerSphere] = PointF(0f, 0f)
        spheresPositionsMap[sphere8] = PointF(-240f, 240f)
        spheresPositionsMap[sphere11] = PointF(220f, -200f)
        spheresPositionsMap[sphere7] = PointF(-350f, -160f)
        spheresPositionsMap[sphere9] = PointF(-390f, 40f)
        spheresPositionsMap[sphere12] = PointF(30f, -280f)
        spheresPositionsMap[sphere6] = PointF(-210f, -270f)
        spheresPositionsMap[sphereJEE] = PointF(300f, 210f)
        spheresPositionsMap[sphereNEET] = PointF(400f, -30f)

        percentageData?.forEach {
            if (it.toGrade != currentGrade) {
                setSphereValue(it, false)
            }
            if (it.fromGrade != currentGrade) {
                setSphereValue(it, true)
            }
        }
    }

    private fun setSphereValue(it: PercentageData, from: Boolean) {
        val grade = if (from)
            it.fromGrade
        else
            it.toGrade
        val resId = resources.getIdentifier("layout_sphere_$grade", "id", context?.packageName)
        var sphere = view?.findViewById<ConstraintLayout>(resId)

        //As 10 & 6 share same position for grade 1-5, if grade is already set(i.e for 10) then return for 6
        if (grade == "6" && (sphere?.get(SPHERE_TEXT_INDEX) as TextView).text.isNotEmpty()) {
            return
        }

        //this solution is temporary as design & requirement of showing all grades is not clear
        if (sphere == null && grade == "10") {
            //if sphere is null and we have to set position for 10, we will put 10 into current grade's old position
            sphere = view?.findViewById(
                resources.getIdentifier("layout_sphere_$currentGrade", "id", context?.packageName)
            )

            //if sphere is still null that means current grade is in bw 1-5, in that case taking sphere 6's position
            if (sphere == null) {
                sphere = view?.findViewById(
                    resources.getIdentifier("layout_sphere_6", "id", context?.packageName)
                )
            }
        }
        sphere?.apply {
            try {
                val textView = this[SPHERE_TEXT_INDEX] as TextView
                textView.text = grade
                val sphereScaleSize =
                    minSize + it.percentageConnection!!.div(100) * abs(maxSize - minSize)
                val params = sphere.layoutParams
                val sizeDelta = 70
                params.width = sphereScaleSize.toInt() + sizeDelta
                params.height = sphereScaleSize.toInt() + sizeDelta
                Log.i("sphere", "${sphere.tag} ${sphereScaleSize.toInt()}")
                sphere.requestLayout()
            } catch (e: Exception) {

            }
        }
    }

    private fun loadPolygons(percentageData: ArrayList<PercentageData>?) {
        //same order as spheres
        trianglesValuesMap[triangle8] = TriangleValue(PointF(-158f, 160f), 44f)
        sphereTriangleMap[sphere8] = triangle8

        trianglesValuesMap[triangle11] = TriangleValue(PointF(129f, -115f), 50f)
        sphereTriangleMap[sphere11] = triangle11

        trianglesValuesMap[triangle7] = TriangleValue(PointF(-215f, -105f), 110f)
        sphereTriangleMap[sphere7] = triangle7

        trianglesValuesMap[triangle9] = TriangleValue(PointF(-270f, 18f), 85f)
        sphereTriangleMap[sphere9] = triangle9

        trianglesValuesMap[triangle12] = TriangleValue(PointF(7f, -160f), 10f)
        sphereTriangleMap[sphere12] = triangle12

        trianglesValuesMap[triangle6] = TriangleValue(PointF(-135f, -190f), 145f)
        sphereTriangleMap[sphere6] = triangle6

        trianglesValuesMap[triangleJEE] = TriangleValue(PointF(150f, 95f), 130f)
        sphereTriangleMap[sphereJEE] = triangleJEE

        trianglesValuesMap[triangleNEET] = TriangleValue(PointF(200f, -27f), 90f)
        sphereTriangleMap[sphereNEET] = triangleNEET

        percentageData?.forEach {
            if (it.toGrade != currentGrade) {
                setTriangleValue(it, false)
            }
            if (it.fromGrade != currentGrade) {
                setTriangleValue(it, true)
            }
        }

    }

    private fun setTriangleValue(it: PercentageData, from: Boolean) {
        var invertTriangle = false
        val grade = if (from)
            it.fromGrade
        else
            it.toGrade
        var triangle = this.view?.findViewWithTag<ConstraintLayout>("Triangle${grade}")

        //this solution is temporary as design & requirement of showing all grades is not clear

        //As 10 & 6 share same position for grade 1-5, if grade is already set(i.e for 10) then return for 6
        if (grade == "6" && (triangle?.get(0) as TextView).text.isNotEmpty()) {
            return
        }

        if (triangle == null && grade == "10") {
            //if triangle is null and we have to set position for 10, we will put 10 into current grade's old triangle position
            invertTriangle = true
            triangle = this.view?.findViewWithTag("Triangle${currentGrade}")

            //if triangle is still null that means current grade is in bw 1-5, in that case taking triangle 6's position
            if (triangle == null) {
                triangle = this.view?.findViewWithTag("Triangle6")
            }
        } else {
            if (grade == "7" || grade == "8" || grade == "9" || grade == "6") {
                //this handles from 1-6
                if (currentGrade.toInt() <= 6)
                    invertTriangle = true
                else if (currentGrade.toInt() < grade.toInt())
                    invertTriangle = true

            }
        }

        triangle?.apply {

            rotation = if (!invertTriangle)
                trianglesValuesMap[triangle]?.triangleAngle!!
            else {
                if (currentGrade == "12")
                    trianglesValuesMap[triangle]?.triangleAngle!! + 173 //rotation adjustment for 12
                else
                    trianglesValuesMap[triangle]?.triangleAngle!! + 180 //180 bcz triangle needs to be inverted
            }
            (this[0] as TextView).apply {
                text = "${it.percentageConnection?.roundToInt().toString()}%"
                rotation = if (!invertTriangle)
                    -trianglesValuesMap[triangle]?.triangleAngle!!
                else
                    -trianglesValuesMap[triangle]?.triangleAngle!! + 180
            }
        }
    }

    private fun loadLines() {
        linesMap[binding.connection109] = LinePoint(PointF(940f, 350f), PointF(600f, 390f))
        linesMap[binding.connection108] = LinePoint(PointF(940f, 350f), PointF(740f, 570f))
        linesMap[binding.connection107] = LinePoint(PointF(940f, 350f), PointF(570f, 190f))
        linesMap[binding.connection106] = LinePoint(PointF(940f, 350f), PointF(700f, 40f))
        linesMap[binding.connection1011] = LinePoint(PointF(940f, 350f), PointF(1160f, 180f))
        linesMap[binding.connection1012] = LinePoint(PointF(940f, 350f), PointF(960f, 70f))
        linesMap[binding.connection10JEE] = LinePoint(PointF(940f, 350f), PointF(1200f, 540f))
        linesMap[binding.connection10NEET] = LinePoint(PointF(940f, 350f), PointF(1300f, 330f))
    }

    private fun loadSphereSelectionLinePoints() {
        val sphere9SelectionPair = Pair(PointF(610f, 570f), PointF(1350f, 500f))
        sphereSelectionLinePointsMap["Sphere9"] = sphere9SelectionPair

        val sphere8SelectionPair = Pair(PointF(730f, 770f), PointF(1080f, 350f))
        sphereSelectionLinePointsMap["Sphere8"] = sphere8SelectionPair

        val sphere7SelectionPair = Pair(PointF(650f, 410f), PointF(1200f, 625f))
        sphereSelectionLinePointsMap["Sphere7"] = sphere7SelectionPair

        val sphere6SelectionPair = Pair(PointF(770f, 330f), PointF(1125f, 790f))
        sphereSelectionLinePointsMap["Sphere6"] = sphere6SelectionPair

        val sphere11SelectionPair = Pair(PointF(1180f, 350f), PointF(650f, 720f))
        sphereSelectionLinePointsMap["Sphere11"] = sphere11SelectionPair

        val sphere12SelectionPair = Pair(PointF(950f, 350f), PointF(910f, 800f))
        sphereSelectionLinePointsMap["Sphere12"] = sphere12SelectionPair

        val sphereJEESelectionPair = Pair(PointF(1200f, 730f), PointF(550f, 280f))
        sphereSelectionLinePointsMap["SphereJEE"] = sphereJEESelectionPair

        val sphereNEETSelectionPair = Pair(PointF(1300f, 520f), PointF(575f, 525f))
        sphereSelectionLinePointsMap["SphereNEET"] = sphereNEETSelectionPair

    }

    private fun loadLottieAnimation(
        lv: LottieAnimationView,
        animJson: Int,
        loadAnimation: Boolean
    ) {
        if (loadAnimation) {
            if (lv.visibility != VISIBLE)
                lv.visibility = VISIBLE
            lv.setAnimation(animJson)
        } else {
            //just play as its already loaded
            lv.repeatMode = LottieDrawable.RESTART
            lv.repeatCount = LottieDrawable.INFINITE
            lv.playAnimation()

        }
    }

    private fun stopLottieAnimation(lv: LottieAnimationView) {
        lv.pauseAnimation()
    }

    fun restoreSelection() {
        if (isEnterClicked) {
            centerSphere.postDelayed({
                centerSphere.requestFocus()
            }, 10)
        } else {
            binding.lvEnterKey.postDelayed({
                binding.lvEnterKey.requestFocus()
            }, 10)
        }

    }


    /*load Concepts from api*/
    fun bindKeyConceptsData(
        from_goal: String,
        to_goal: String,
        from_exam: String,
        to_exam: String,
        level: String,
        count: String,
        sphere: ConstraintLayout,
        selectedSphere: ConstraintLayout,
        triangle: ConstraintLayout,
        connection: LineView,
        direction: Pair<Float, Float>
    ) {
        //   showProgress()
        achieveViewModel.getKeyRelationsApi(
            from_goal,
            to_goal,
            from_exam,
            to_exam,
            level,
            count,
            object : BaseViewModel.APICallBacks<ArrayList<KeyConceptModel>> {

                override fun onSuccess(model: ArrayList<KeyConceptModel>?) {
                    //hideProgress()
                    if (model != null) {
                        val keyConcepts = model
                        setSphereSelection(
                            sphere,
                            selectedSphere,
                            triangle,
                            connection,
                            direction,
                            keyConcepts
                        )
                        if (keyConcepts != null) {
                            keyConceptsList.clear()
                            keyConceptsList.addAll(keyConcepts)
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    //hideProgress()
                    if (Utils.isApiFailed(code)) {
                        Utils.showError(
                            context,
                            code,
                            object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: String) {
                                    bindKeyConceptsData(
                                        from_goal,
                                        to_goal,
                                        from_exam,
                                        to_exam,
                                        level,
                                        count,
                                        sphere,
                                        selectedSphere,
                                        triangle,
                                        connection,
                                        direction
                                    )
                                }

                                override fun onDismiss() {}
                            })
                    } else {
                        showToast(error)
                    }
                }
            })
    }

    /*load connections from api*/
    fun getPercentageConnectionsApi() {
        // showProgress()
        achieveViewModel.getPercentageConnectionsApi(object :
            BaseViewModel.APICallBacks<PercentageConnectionsRes> {

            override fun onSuccess(model: PercentageConnectionsRes?) {
                // hideProgress()
                if (model != null) {
                    val percentageData = model.data
                    setConnections(percentageData)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                // hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getPercentageConnectionsApi()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }
        })
    }

    private fun setConnections(percentageData: ArrayList<PercentageData>?) {
        percentageConnectionsList.clear()
        if (percentageData != null) {
            percentageConnectionsList.addAll(percentageData)
            loadSpheres(currentGrade, percentageData)
            loadPolygons(percentageData)
            loadLines()
            loadSphereSelectionLinePoints()
        }
    }

    private fun callFutureSuccessApi() {
        achieveViewModel.getFutureSuccessApi(object :
            BaseViewModel.APICallBacks<List<FutureSuccessRes>> {
            override fun onSuccess(model: List<FutureSuccessRes>?) {
                DataManager.instance.setFutureSuccessData(model)
                isFutureSuccessDataAvailable = true
            }

            override fun onFailed(code: Int, error: String, msg: String) {
            }

        })
    }


    private fun callReadinessApiAsync() {
        achieveViewModel.getReadinessApi(DataManager.instance.getExamCodeFromContent(null),
            object : BaseViewModel.APICallBacks<Readiness> {
                override fun onSuccess(model: Readiness?) {
                    if (model != null) {
                        updateReadinessResult(model)
                        isReadinessDataAvailable = true
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    Log.e(classTag, msg)
                }
            })
    }

    private fun updateReadinessResult(model: Readiness) {
        DataManager.instance.setReadiness(model)
    }

    private fun setAchieversKeyListener() {
        binding.flAchievers.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        showDiscover(600)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN, KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        binding.flAchievers.postDelayed({
                            binding.flAchievers.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navigationMenuCallback.navMenuToggle(true)
                    }
                }
            }
            false
        }
    }

    private fun fragmentReplacer(flId: Int, fragment: Fragment) {
        when (fragment) {
            is ReadinessFragment -> {
                isReadinessActive = true
                binding.flReadiness.visibility = VISIBLE
                childFragmentManager.beginTransaction().add(flId, fragment)
                    .addToBackStack(null)
                    .commit()
            }
            is AchieversFragment -> {
                isReadinessActive = false
                childFragmentManager.beginTransaction().replace(flId, fragment)
                    .commit()
            }
        }
    }

    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        when (childFragment) {
            is AchieversFragment -> {
                childFragment.setDPadKeysListener(this)
            }
            is ReadinessFragment -> {
                childFragment.setDPadKeysListener(this)
            }
        }
    }

    private fun hideShowFrag(fragOneVisibility: Int, fragTwoVisibility: Int) {
        binding.discoverCL.visibility = fragOneVisibility
        binding.flAchievers.visibility = fragTwoVisibility
        if (fragTwoVisibility == VISIBLE) {
            binding.flAchievers.postDelayed({
                binding.flAchievers.requestFocus()
            }, 10)
        }
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    private fun showAchievers(delay: Long) {
        isAchieverShown = true
        binding.discoverCL.animate().translationY(-1100f).duration = 800
        binding.flAchievers.animate().translationY(0f)
        CoroutineScope(Dispatchers.Main).launch {
            delay(delay)
            hideShowFrag(GONE, VISIBLE)
            achieversFragment.startCardFlip()
        }
    }

    private fun showDiscover(viewDelay: Long) {
        isAchieverShown = false
        binding.flAchievers.animate().translationY(1100f).duration = 800
        binding.discoverCL.animate().translationY(0f)
        CoroutineScope(Dispatchers.Main).launch {
            delay(viewDelay)
            if (viewDelay == 600L) { // this means showDiscover call is coming from achievers
                achieversFragment.stopCardFlip()
            }
            hideShowFrag(VISIBLE, GONE)
        }
        restoreSelection()
    }

    fun handleBackPress() {
        if (isReadinessActive) {
            if (readinessFragment.isFutureSuccessActive()) {
                //show 'readiness'
                readinessFragment.showReadiness()
            } else {
                //show 'discover the knowledge space'
                showDiscover(100)
                childFragmentManager.popBackStack()
                binding.flReadiness.visibility = GONE
                isReadinessActive = false
            }
        } else if (isSphereSelected) {
            //handle back press for spheres
            resetSpheres(whichSphereClicked)
        }
    }

    private fun resetSpheres(whichSphereClicked: String) {
        when (whichSphereClicked) {
            "Sphere9" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphere9,
                        sphere9selected,
                        triangle9,
                        binding.connection109,
                        CoordinatePlaneValues.CENTER_RIGHT_9
                    )
                    isSphereSelected = false
                }
            }
            "Sphere8" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphere8,
                        sphere8selected,
                        triangle8,
                        binding.connection108,
                        CoordinatePlaneValues.TOP_RIGHT_8
                    )
                    isSphereSelected = false
                }
            }
            "Sphere7" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphere7,
                        sphere7selected,
                        triangle7,
                        binding.connection107,
                        CoordinatePlaneValues.BOTTOM_RIGHT_7
                    )
                    isSphereSelected = false
                }
            }
            "Sphere6" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphere6,
                        sphere6selected,
                        triangle6,
                        binding.connection106,
                        CoordinatePlaneValues.BOTTOM_RIGHT_6
                    )
                    isSphereSelected = false
                }
            }
            "Sphere11" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphere11,
                        sphere11selected,
                        triangle11,
                        binding.connection1011,
                        CoordinatePlaneValues.BOTTOM_LEFT_11
                    )
                    isSphereSelected = false
                }
            }
            "Sphere12" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphere12,
                        sphere12selected,
                        triangle12,
                        binding.connection1012,
                        CoordinatePlaneValues.BOTTOM_DOWN_12
                    )
                    isSphereSelected = false
                }
            }
            "SphereJEE" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphereJEE,
                        sphereJEEselected,
                        triangleJEE,
                        binding.connection10JEE,
                        CoordinatePlaneValues.TOP_LEFT_JEE
                    )
                    isSphereSelected = false
                }
            }
            "SphereNEET" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphereNEET,
                        sphereNEETselected,
                        triangleNEET,
                        binding.connection10NEET,
                        CoordinatePlaneValues.CENTER_LEFT_NEET
                    )
                    isSphereSelected = false
                }
            }
        }
    }

    fun isReadinessViewActive(): Boolean = isReadinessActive

    fun isAchieverViewActive(): Boolean = isAchieverShown

    override fun isKeyPadDown(isDown: Boolean, from: String) {

    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
        if (isLeft) {
            navigationMenuCallback.navMenuToggle(true)
        }
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {
    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {
    }

    private fun ImageView.defaultBg() {
        this.setBackgroundResource(R.drawable.layer_circle_transparent)
    }

    private fun ImageView.changeFocusBg() {
        this.setBackgroundResource(R.drawable.layer_circle_glow_cyan)
    }

    fun isSphereSelected(): Boolean = isSphereSelected

    fun setFocusToAchiever() {
        coroutineScope?.launch {
            delay(100)
            binding.flAchievers.requestFocus()
        }
    }

    object CoordinatePlaneValues {
        val TOP_LEFT_JEE = Pair(-400f, -275f)
        val CENTER_RIGHT_9 = Pair(400f, -40f)
        val TOP_RIGHT_8 = Pair(150f, -200f)
        val BOTTOM_RIGHT_6 = Pair(200f, 270f)
        val BOTTOM_RIGHT_7 = Pair(300f, 100f)
        val BOTTOM_LEFT_11 = Pair(-320f, 200f)
        val BOTTOM_DOWN_12 = Pair(-30f, 300f)
        val CENTER_LEFT_NEET = Pair(-400f, -14f)
    }
}


