package com.embibe.embibetvapp.ui.adapter

import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemTestDetailMenuBinding
import com.embibe.embibetvapp.model.test.ChapterListData
import com.embibe.embibetvapp.model.test.SubFeedbackDetailMenuModel
import com.embibe.embibetvapp.utils.SegmentUtils

class TestFeedbackDetailMenuAdapter :
    RecyclerView.Adapter<TestFeedbackDetailMenuAdapter.TestsViewHolder>() {

    var list = ArrayList<ChapterListData>()
    var onItemClick: ((ChapterListData, ItemTestDetailMenuBinding, View) -> Unit)? = null
    private lateinit var subMenuAdapter: TestFeedbackDetailSubMenuAdapter
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestsViewHolder {
        val binding: ItemTestDetailMenuBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_test_detail_menu, parent, false
        )
        return TestsViewHolder(binding)
    }

    override fun getItemCount() = if (list.size > 0) list.size else 0

    override fun onBindViewHolder(holder: TestsViewHolder, position: Int) {
        holder.bind(list[position])

//        if (position == selectedPosition) {
//            if (list[position].subItems.size > 0) {
//                holder.binding.imgAdd.setImageDrawable(holder.itemView.resources.getDrawable(R.drawable.ic_minus_white_24))
//                holder.binding.recyclerSubItem.visibility = View.VISIBLE
//            }
//        }
    }

    fun setData(list: ArrayList<ChapterListData>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class TestsViewHolder(var binding: ItemTestDetailMenuBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ChapterListData) {
            binding.title.text = item.chapter_name

            binding.recyclerSubItem.visibility = View.GONE
            /* if (subItems.size > 0) {
                 binding.imgAdd.visibility = View.VISIBLE
                 binding.imgAdd.setImageDrawable(itemView.resources.getDrawable(R.drawable.ic_add_white_24dp))
                 setRecycler(binding, itemView, item)
             } else {
                 binding.imgAdd.visibility = View.GONE
             }*/

        }

        init {

            binding.imgLayout.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                            SegmentUtils.trackTestFeedbackScreenChapterWiseAnalysisClick(
                                list[adapterPosition].chapter_name.toString(),adapterPosition+1)

//                            if (selectedPosition != adapterPosition) {
//                                val previousPosition = selectedPosition
//                                notifyItemChanged(previousPosition)
//
//                                selectedPosition = adapterPosition
//                                notifyItemChanged(adapterPosition)
//                            } else {
//                                // make recycler visible and previous item back to original position
//                                toggleRecyclerVisibility(binding, itemView, adapterPosition)
//                            }
                        }
                    }
                }

                false
            }
            binding.imgLayout.setOnFocusChangeListener { v, hasFocus ->
                when(hasFocus) {
                    true -> {
                        SegmentUtils.trackTestFeedbackScreenChapterWiseAnalysisFocus(
                            list[adapterPosition].chapter_name.toString(),adapterPosition+1)
                    }
                }
            }

            binding.imgLayout.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition], binding, itemView)
                SegmentUtils.trackTestFeedbackScreenChapterWiseAnalysisClick(
                    list[adapterPosition].chapter_name.toString(),adapterPosition+1)
            }
        }
    }

    private fun toggleRecyclerVisibility(
        binding: ItemTestDetailMenuBinding,
        itemView: View,
        adapterPosition: Int
    ) {
//        if (list[adapterPosition].subItems.size > 0) {
//            if (binding.recyclerSubItem.visibility == View.GONE) {
//
//                binding.recyclerSubItem.visibility = View.VISIBLE
//                binding.imgAdd.setImageDrawable(itemView.resources.getDrawable(R.drawable.ic_minus_white_24))
//            } else if (binding.recyclerSubItem.visibility == View.VISIBLE) {
//
//                binding.recyclerSubItem.visibility = View.GONE
//                binding.imgAdd.setImageDrawable(itemView.resources.getDrawable(R.drawable.ic_add_white_24dp))
//            }
//        }
    }

    fun setRecycler(
        binding: ItemTestDetailMenuBinding,
        itemView: View,
        items: ArrayList<SubFeedbackDetailMenuModel>
    ) {
        binding.imgAdd.visibility = View.VISIBLE
        binding.imgAdd.setImageDrawable(itemView.resources.getDrawable(R.drawable.ic_add_white_24dp))
        subMenuAdapter = TestFeedbackDetailSubMenuAdapter()
        val layoutManager =
            LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerSubItem.adapter = subMenuAdapter
        binding.recyclerSubItem.layoutManager = layoutManager
        binding.recyclerSubItem.visibility = VISIBLE
        subMenuAdapter.setData(items)
    }
}