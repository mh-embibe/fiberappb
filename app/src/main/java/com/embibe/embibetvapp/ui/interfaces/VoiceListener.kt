package com.embibe.embibetvapp.ui.interfaces

interface VoiceListener {
    fun onVoiceInit()
    fun onVoiceSuccess(data: String)
    fun onVoiceFailed(msg: String)
    fun onVoiceCancel(msg: String)
}