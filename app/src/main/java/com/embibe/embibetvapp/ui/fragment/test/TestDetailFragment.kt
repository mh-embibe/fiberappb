package com.embibe.embibetvapp.ui.fragment.test

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentTestDetailBinding
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkResponse
import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.embibe.embibetvapp.model.test.TestDetailModel
import com.embibe.embibetvapp.model.test.TestDetailsRes
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.adapter.TestDetailAdapter
import com.embibe.embibetvapp.ui.viewmodel.DetailsViewModel
import com.embibe.embibetvapp.ui.viewmodel.TestViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import kotlinx.android.synthetic.main.layout_correctly_answered.*
import java.util.*

class TestDetailFragment : BaseAppFragment() {

    private lateinit var model: TestDetailsRes
    private lateinit var binding: FragmentTestDetailBinding
    private lateinit var detailsViewModel: DetailsViewModel
    private lateinit var testDetailAdapter: TestDetailAdapter
    private var correctlyAnsweredMotionLayout: MotionLayout? = null
    var list = ArrayList<String>()

    private var progressLayoutWidth: Int = 0
    var height: Int = 0

    private lateinit var testViewModel: TestViewModel
    private var bookmarkRetryCount: Int = 0
    private var isBookMarked: Boolean = false
    private var isLiked: Boolean = false
    private lateinit var data: Content

    companion object {
        lateinit var title: String
        lateinit var totalDuration: String
        lateinit var totalQuestions: String
        lateinit var totalMarks: String
        lateinit var topScore: String
        lateinit var averageScore: String
        lateinit var lowScore: String
        lateinit var TQS: String
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_test_detail, container, false)
        detailsViewModel = ViewModelProviders.of(this).get(DetailsViewModel::class.java)
        testViewModel = ViewModelProviders.of(this).get(TestViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.testName.text = title


        binding.btnBookmark.setOnClickListener {
            initBookmark()
        }
        data = arguments?.getParcelable(AppConstants.CONTENT)!!

        Log.w("TAG", "Feagment Data    " + data.toString())
        focusListener()
        setTestDetailRecycler(view, createDummyData())
        getBookmarkStatus()
        setAndAnimateProgress()
    }

    private fun setTestDetailRecycler(view: View, data: ArrayList<TestDetailModel>) {

        testDetailAdapter = TestDetailAdapter()
        val layoutManager = GridLayoutManager(view.context, 3, GridLayoutManager.VERTICAL, false)
        binding.recyclerTestDetail.adapter = testDetailAdapter
        binding.recyclerTestDetail.layoutManager = layoutManager
        testDetailAdapter.setData(data)
        binding.textPercentage.text = TQS
    }

    private fun createDummyData(): ArrayList<TestDetailModel> {
        var customTestInformation = ArrayList<TestDetailModel>()
        if (data.sub_type.equals(AppConstants.CUSTOM_TEST)) {
            customTestInformation = arrayListOf(
                TestDetailModel(
                    R.drawable.ic_duration,
                    totalDuration,
                    getString(R.string.duration)
                ),
                TestDetailModel(
                    R.drawable.ic_questions,
                    totalQuestions,
                    getString(R.string.questions)
                ),
                TestDetailModel(R.drawable.ic_marks, totalMarks, getString(R.string.marks))
            )
        } else {


            customTestInformation = arrayListOf(
                TestDetailModel(
                    R.drawable.ic_duration,
                    totalDuration,
                    getString(R.string.duration)
                ),
                TestDetailModel(
                    R.drawable.ic_questions,
                    totalQuestions,
                    getString(R.string.questions)
                ),
                TestDetailModel(R.drawable.ic_marks, totalMarks, getString(R.string.marks)),

                TestDetailModel(
                    R.drawable.ic_top_score,
                    topScore.toString(),
                    getString(R.string.top_score)
                ),
                TestDetailModel(
                    R.drawable.avg_score,
                    averageScore.toString(),
                    getString(R.string.avr_score)
                ),
                TestDetailModel(
                    R.drawable.ic_low_score,
                    lowScore.toString(),
                    getString(R.string.low_score)
                )
            )
        }
        return customTestInformation
    }

    fun onTestRecyclerRightClick(position: Int) {
        Handler().postDelayed({
            binding.btnBookmark.requestFocus()
        }, 100)
    }

    private fun setAndAnimateProgress() {
        val goodScore = 75
        val yourScore = 60
        val cutOffScore = 50

        if (list.size >= 5)
            changeMotionLayoutConstraintSetToFit()

        animate(
            goodScore,
            binding.cardTestAnalysis.progressBarGoodScore,
            binding.cardTestAnalysis.tvGoodScore
        )
        moveCutOffLineBiasBasedOnScore(cutOffScore)
        if (yourScore < 0) {
            animate(
                yourScore,
                binding.cardTestAnalysis.progressBarNegative,
                binding.cardTestAnalysis.tvNegativeScore
            )
        } else {
            animate(
                yourScore,
                binding.cardTestAnalysis.progressBarYourScore,
                binding.cardTestAnalysis.tvYourScore
            )
        }
    }


    fun setValue(
        Curentitle: String,
        currentTotalDuration: String, currentTotalQuestions: String,
        currentTotalMarks: String,
        currentTopScore: String,
        currentAverageScore: String, currentLowScore: String, currentTQS: String
    ) {
        title = Curentitle
        totalDuration = currentTotalDuration
        totalQuestions = currentTotalQuestions
        totalMarks = currentTotalMarks
        topScore = currentTopScore
        averageScore = currentAverageScore
        lowScore = currentLowScore
        TQS = currentTQS
    }

    fun setBookmarkFocus() {
        Handler().postDelayed({
            binding.btnBookmark.requestFocus()
        }, 0)

    }

    fun initBookmark() {
        Utils.isBookmarkUpdatedForTEST = true
        isBookMarked = !isBookMarked
        updateLikeBookMarkUI(isBookMarked, isLiked)
        SegmentUtils.trackEventMoreInfoBookmarkClick(isBookMarked, content = data)
        timerBookmark.cancel()
        timerBookmark = Timer()
        timerBookmark.schedule(object : TimerTask() {
            override fun run() {
                updateLikeBookmark(AppConstants.ACTIVITY_TYPE_BOOKMARK, isBookMarked)
            }
        }, interval)
    }

    fun initLike() {
        isLiked = !isLiked
        updateLikeBookMarkUI(isBookMarked, isLiked)
        SegmentUtils.trackEventMoreInfoBookmarkClick(isLiked, content = data)
        timerLike.cancel()
        timerLike = Timer()
        timerLike.schedule(object : TimerTask() {
            override fun run() {
                updateLikeBookmark(AppConstants.ACTIVITY_TYPE_LIKE, isLiked)
            }
        }, interval)
    }

    private fun updateLikeBookmark(type: String, status: Boolean) {
        testViewModel.likeBookmarkTest(data.bundle_id, data.type, type, status,
            object : BaseViewModel.APICallBacks<CommonApiResponse> {
                override fun onSuccess(model: CommonApiResponse?) {
                    if (model?.success!!) {
                        Utils.isBookmarkUpdatedForTEST = true
                        updateLikeBookMarkUI(isBookMarked, isLiked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (bookmarkRetryCount < retryAttempt) {
                        bookmarkRetryCount++
                        updateLikeBookmark(type, status)
                        return
                    } else {
                        /*reset retry count*/
                        bookmarkRetryCount = 0
                        /*revert back to previous state of ui */
                        updateLikeBookMarkUI(isBookMarked, isLiked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_BOOKMARK)
                    }
                }
            })
    }

    private fun updateLikeBookMarkUI(bookmarked: Boolean, liked: Boolean) {
        isBookMarked = bookmarked
        isLiked = liked
        updateBookMarkUI(isBookMarked)
    }

    private fun getBookmarkStatus() {
        detailsViewModel.getBookmarkStatus(data.bundle_id,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                val data = dataList.filter {
                                    it.contentId.equals(data.bundle_id)
                                }
                                updateBookMarkUI(data[0].status)
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg ")

                }
            })
    }

    fun focusListener() {
        binding.btnBookmark.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackEventMoreInfoBookmarkFocus(isBookMarked, content = data)
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_black, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                    )
                }
            } else {
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_white, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                    )
                }
            }
        }
    }

    private fun updateBookMarkUI(bookmarked: Boolean) {
        isBookMarked = bookmarked
        data.isBookmarked = bookmarked

        if (binding.btnBookmark.hasFocus()) {
            if (isBookMarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        } else {
            if (isBookMarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        }
    }

    private fun changeMotionLayoutConstraintSetToFit() {
        val startSet: ConstraintSet = correctlyAnsweredMotionLayout?.getConstraintSet(R.id.start)!!
        startSet.connect(
            R.id.tv_currently_answered,
            ConstraintSet.TOP, R.id.iv_text_score,
            ConstraintSet.TOP, 10
        )
        startSet.connect(
            R.id.tv_currently_answered,
            ConstraintSet.START, R.id.iv_text_score,
            ConstraintSet.START
        )
        startSet.connect(
            R.id.tv_currently_answered,
            ConstraintSet.END, R.id.iv_text_score,
            ConstraintSet.END
        )
        startSet.connect(
            R.id.tv_currently_answered,
            ConstraintSet.BOTTOM, R.id.iv_text_score,
            ConstraintSet.BOTTOM
        )
        startSet.connect(
            R.id.tv_score,
            ConstraintSet.START, R.id.tv_currently_answered,
            ConstraintSet.END, 5
        )
        startSet.connect(
            R.id.tv_score,
            ConstraintSet.TOP, R.id.tv_currently_answered,
            ConstraintSet.TOP
        )
        startSet.connect(
            R.id.tv_score,
            ConstraintSet.BOTTOM, R.id.tv_currently_answered,
            ConstraintSet.BOTTOM
        )
        startSet.connect(R.id.tv_score, ConstraintSet.END, R.id.iv_text_score, ConstraintSet.END)
        startSet.connect(
            R.id.rv_score_bar,
            ConstraintSet.START, constraintLayout.id,
            ConstraintSet.START
        )
        startSet.connect(
            R.id.rv_score_bar,
            ConstraintSet.END, constraintLayout.id,
            ConstraintSet.END
        )
        startSet.connect(
            R.id.rv_score_bar,
            ConstraintSet.TOP, constraintLayout.id,
            ConstraintSet.TOP
        )
        startSet.connect(
            R.id.rv_score_bar,
            ConstraintSet.BOTTOM, constraintLayout.id,
            ConstraintSet.BOTTOM
        )
        startSet.setVerticalBias(R.id.rv_score_bar, 0.7f)
        startSet.setHorizontalBias(R.id.rv_score_bar, 0.3f)
        startSet.setHorizontalBias(R.id.tv_currently_answered, 0.3f)
        startSet.setVerticalBias(R.id.tv_currently_answered, 0.0f)
        startSet.setHorizontalBias(R.id.tv_score, 0.0f)
        startSet.applyTo(correctlyAnsweredMotionLayout)

        val endSet: ConstraintSet = correctlyAnsweredMotionLayout?.getConstraintSet(R.id.end)!!
        endSet.connect(
            R.id.tv_currently_answered,
            ConstraintSet.TOP, R.id.iv_text_score,
            ConstraintSet.TOP, 20
        )
        endSet.connect(
            R.id.tv_currently_answered,
            ConstraintSet.START, R.id.iv_text_score,
            ConstraintSet.START
        )
        endSet.connect(
            R.id.tv_currently_answered,
            ConstraintSet.END, R.id.iv_text_score,
            ConstraintSet.END
        )
        endSet.connect(
            R.id.tv_currently_answered,
            ConstraintSet.BOTTOM, R.id.iv_text_score,
            ConstraintSet.BOTTOM
        )
        endSet.connect(
            R.id.tv_score,
            ConstraintSet.START, R.id.tv_currently_answered,
            ConstraintSet.END, 5
        )
        endSet.connect(
            R.id.tv_score,
            ConstraintSet.TOP, R.id.tv_currently_answered,
            ConstraintSet.TOP
        )
        endSet.connect(
            R.id.tv_score,
            ConstraintSet.BOTTOM, R.id.tv_currently_answered,
            ConstraintSet.BOTTOM
        )
        endSet.connect(R.id.tv_score, ConstraintSet.END, R.id.iv_text_score, ConstraintSet.END)
        endSet.connect(
            R.id.rv_score_bar,
            ConstraintSet.START, constraintLayout.id,
            ConstraintSet.START
        )
        endSet.connect(R.id.rv_score_bar, ConstraintSet.END, constraintLayout.id, ConstraintSet.END)
        endSet.connect(R.id.rv_score_bar, ConstraintSet.TOP, constraintLayout.id, ConstraintSet.TOP)
        endSet.connect(
            R.id.rv_score_bar,
            ConstraintSet.BOTTOM, constraintLayout.id,
            ConstraintSet.BOTTOM
        )
        endSet.setVerticalBias(R.id.rv_score_bar, 0.7f)
        endSet.setHorizontalBias(R.id.rv_score_bar, 0.3f)
        endSet.setHorizontalBias(R.id.tv_currently_answered, 0.3f)
        endSet.setVerticalBias(R.id.tv_currently_answered, 0.0f)
        endSet.setHorizontalBias(R.id.tv_score, 0.0f)
        endSet.applyTo(correctlyAnsweredMotionLayout)

    }

    private fun moveCutOffLineBiasBasedOnScore(cutOffScore: Int) {
        // AnyScore/totalScore to get bias value
        //totalMarks 130, Negative Marks -12

        val calScore = (cutOffScore.toDouble().div(130))
        val cutOffLine = binding.cardTestAnalysis.ivCutOffMarker
        val cutOffLabel = binding.cardTestAnalysis.tvCutOffMarkerLabel
        val cutOffLineParams = cutOffLine.layoutParams as ConstraintLayout.LayoutParams
        val cutOffLineAnimator = ValueAnimator.ofFloat(0f, calScore.toFloat())
        cutOffLineAnimator.addUpdateListener { value ->
            cutOffLineParams.horizontalBias = (value.animatedValue as Float)
            cutOffLine.layoutParams = cutOffLineParams
        }

        val cutOffLabelAnimator = ValueAnimator.ofInt(0, cutOffScore)
        cutOffLabelAnimator.addUpdateListener { value ->
            cutOffLabel.text = (value.animatedValue as Int).toString()
        }
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(cutOffLabelAnimator, cutOffLineAnimator)
        animatorSet.duration = 1500
        animatorSet.start()
    }

    private fun animate(progressValue: Int, progressBar: ImageView, tv: TextView) {
        animateBar(progressBar, progressValue)
        tv.animateWithProgressBar(progressValue)
    }

    private fun TextView.animateWithProgressBar(progressValue: Int) {

        val animator = ObjectAnimator.ofInt(0, progressValue)
        animator.addUpdateListener { animationValue ->
            this.text = animationValue.animatedValue.toString()
        }
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(animator)
        animatorSet.duration = 2000
        animatorSet.start()
    }

    private fun animateBar(bar: ImageView, progressValue: Int) {
        val progressLayout = binding.cardTestAnalysis
        val vto = progressLayout.root.viewTreeObserver
        var newProgressValue = 0
        newProgressValue = Math.abs(progressValue)
        var listener: ViewTreeObserver.OnGlobalLayoutListener? = null
        listener = ViewTreeObserver.OnGlobalLayoutListener {
            progressLayoutWidth = progressLayout.root.width
            height = progressLayout.root.height

            if (newProgressValue < progressLayoutWidth) {
                val calValue = newProgressValue.toDouble() / 130 * progressLayoutWidth
                val animator = ValueAnimator.ofInt(0, calValue.toInt())
                animator.addUpdateListener { valueAnimator ->
                    val value = valueAnimator.animatedValue
                    val params = bar.layoutParams as ConstraintLayout.LayoutParams
                    if (value as Int > 0) {
                        bar.visibility = View.VISIBLE
                    } else
                        bar.visibility = View.GONE
                    params.width = value
                    bar.layoutParams = params
                }

                animator.duration = 1500
                animator.start()
            } else {
                /*if progresValue is more than width
                    newWidth = (currentMarks/TotalMarks) * layoutWidth
                */
            }
            binding.cardTestAnalysis.root.viewTreeObserver?.removeOnGlobalLayoutListener(
                listener!!
            )
        }
        vto?.addOnGlobalLayoutListener(listener)
    }
}
