package com.embibe.embibetvapp.ui.presenter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import androidx.core.content.res.ResourcesCompat
import androidx.leanback.widget.BaseCardView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.newmodel.Content

class StoryWorldPresenter(context: Context) :
    AbstractCardPresenter<BaseCardView>(context) {

    override fun onCreateView(): BaseCardView {
        val cardView = BaseCardView(context, null, R.style.SideInfoCardStyle)
        cardView.addView(
            LayoutInflater.from(context).inflate(
                R.layout.card_story_world,
                null
            )
        )
        cardView.isFocusable = true
        cardView.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            val viewAlpha = cardView.findViewById<View>(R.id.view_focus)
            if (hasFocus) {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_focused_alpha_value
                )
            } else {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_unfocused_alpha_value
                )
            }
        }

        return cardView
    }

    override fun onBindViewHolder(item: Any, cardView: BaseCardView) {
        val card = item as Content
        val ivThumbnail: ImageView = cardView.findViewById(R.id.iv_thumbnail)
        val width = context.resources
            .getDimension(R.dimen.practice_image_width).toInt()
        val height = context.resources
            .getDimension(R.dimen.practice_image_height).toInt()

        val requestOptions = RequestOptions().transform(RoundedCorners(8)).override(width, height)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(context)
            .applyDefaultRequestOptions(requestOptions)
            .load(card.thumb)
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.story_placeholder)
            .into(ivThumbnail)
    }

}