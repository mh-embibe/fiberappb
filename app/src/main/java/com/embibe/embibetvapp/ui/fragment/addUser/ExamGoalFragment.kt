package com.embibe.embibetvapp.ui.fragment.addUser

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.GOALS
import com.embibe.embibetvapp.databinding.FragmentExamGoalBinding
import com.embibe.embibetvapp.model.adduser.Exam
import com.embibe.embibetvapp.ui.activity.AddUserActivity
import com.embibe.embibetvapp.ui.adapter.RvAdapterExamGoal
import com.embibe.embibetvapp.ui.interfaces.ComponentClickListener
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Status
import com.embibe.embibetvapp.utils.Utils
import com.embibejio.coreapp.preference.PreferenceHelper
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import org.json.JSONArray
import org.json.JSONObject

class ExamGoalFragment : BaseAppFragment(),
    ComponentClickListener {
    private lateinit var binding: FragmentExamGoalBinding
    private lateinit var signInViewModel: SignInViewModel
    private lateinit var goalsList: List<Exam>
    private lateinit var examGoalAdapter: RvAdapterExamGoal
    private var position: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signInViewModel =
            ViewModelProviders.of(activity as AddUserActivity).get(SignInViewModel::class.java)
        SegmentUtils.trackAddGoalsScreenLoadStart()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_exam_goal, container, false)
        setAdapterOnRV()
        registerExamGoalObserver()
        handleExamGoalApiCall()
        registerDoneBtn()
        return binding.root
    }

    private fun handleExamGoalApiCall() {
        if (PreferenceHelper()[GOALS, ""] != "") {
            setGoalData()
        } else {
            /* signInViewModel.getGoals()*/
        }
    }

    private fun setGoalData() {
        goalsList = Utils.getExams()
        examGoalAdapter.updateData(goalsList)
    }

    private fun registerExamGoalObserver() {
        signInViewModel.examGoalLiveData.observe(viewLifecycleOwner, Observer { resource ->
            if (resource != null) {
                when (resource.status) {
                    Status.LOADING -> {
                        binding.progress.visibility = View.VISIBLE
                    }
                    Status.SUCCESS -> {
                        binding.progress.visibility = View.GONE
                        val goalsHaspMap =
                            ((resource.data as retrofit2.Response<*>).body() as LinkedTreeMap<*, *>)[AppConstants.DATA] as LinkedTreeMap<*, *>
                        val jsonFromMap = JSONObject(Gson().toJson(goalsHaspMap))
                        val goalsArray = JSONArray()
                        for (key in goalsHaspMap.keys) {
                            if (key == "cbse")
                                goalsArray.put(jsonFromMap[key as String])
                        }
                        PreferenceHelper().put(GOALS, goalsArray.toString())
                        setGoalData()
                    }
                    Status.ERROR -> {
                        binding.progress.visibility = View.GONE
                        if (Utils.isApiFailed(resource)) {
                            Utils.showError(
                                context,
                                resource,
                                object : BaseViewModel.ErrorCallBacks {
                                    override fun onRetry(msg: String) {
                                        /*signInViewModel.getGoals()*/
                                    }

                                    override fun onDismiss() {

                                    }
                                })
                        } else {
                            /*update error msg in ui*/
                        }
                    }
                }
            }

        })
    }

    private fun setAdapterOnRV() {
        val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space2)

        val manager = GridLayoutManager(context, 4)
        manager.orientation = GridLayoutManager.HORIZONTAL
        manager.supportsPredictiveItemAnimations()
        binding.rvExamGoal.layoutManager = manager

        examGoalAdapter = RvAdapterExamGoal(context!!, this)
        binding.rvExamGoal.adapter = examGoalAdapter
        binding.rvExamGoal.addItemDecoration(SpaceItemDecoration(itemSpace))

        binding.rvExamGoal.requestFocus()

    }

    inner class SpaceItemDecoration internal constructor(private val space: Int) :
        RecyclerView.ItemDecoration() {
        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            outRect.left = space
            outRect.bottom = space
        }

    }

    override fun onClicked(type: String, data: Any) {
        position = data as Int
    }

    private fun registerDoneBtn() {
        binding.buttonDone.setOnClickListener {
            if (position != -1) {
                signInViewModel.setGoal(position)
                fragmentManager!!.popBackStack()
                SegmentUtils.trackAddGoalsScreenDoneClick(goalsList[position].name, "", "")
            } else {
                Utils.showToast(context!!, getString(R.string.exam_select_limit))
            }
        }
        SegmentUtils.trackAddGoalsScreenLoadEnd()
    }

}