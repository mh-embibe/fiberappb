package com.embibe.embibetvapp.ui.fragment

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.FragmentComplexUi1Binding
import com.embibe.embibetvapp.model.Performance
import com.embibe.embibetvapp.ui.adapter.PotentialAdapter

class ComplexUICheckFragment : Fragment() {

    private var list: ArrayList<Performance> = ArrayList()

    private lateinit var binding: FragmentComplexUi1Binding
    private var msg: String? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_complex_ui_1, container, false)
        loadList()
        initRv()
        loadLottieAnimation()
        return binding.root
    }

    private fun loadLottieAnimation() {
//        var animationView = binding.lavTrophy
//        animationView.setAnimation(R.raw.circle_rays)
//        animationView.playAnimation()


    }

    private fun initRv() {
        var adapter = PotentialAdapter(requireContext())
        adapter.setData(list)
        val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space1)
        binding.rvPotential.addItemDecoration(SpaceItemDecoration(itemSpace))
        binding.rvPotential.layoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.HORIZONTAL, false
        )
        binding.rvPotential.adapter = adapter
    }

    private fun loadList() {
        list.add(Performance(R.drawable.ic_like_potential, "Your Predicted Exam in JEE Main"))
        list.add(
            Performance(
                R.drawable.ic_dislike_potential,
                "Your Potential performance in JEE Main"
            )
        )
        list.add(Performance(R.drawable.unleash_true_potential, "Unleash your true potential"))
    }

    inner class SpaceItemDecoration internal constructor(private val space: Int) :
        RecyclerView.ItemDecoration() {
        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            outRect.left = space
        }

    }

}