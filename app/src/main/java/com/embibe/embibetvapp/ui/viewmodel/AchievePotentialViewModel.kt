package com.embibe.embibetvapp.ui.viewmodel

import android.content.Context
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.model.potential.Potential
import com.embibe.embibetvapp.utils.Utils.readJSONFromAsset

class AchievePotentialViewModel : AchieveViewModel() {


    /*load the Potential model data from the json response file*/
    fun getStepsToAchieveTruePotential(context: Context): List<Potential> {

        return fromJson(
            readJSONFromAsset("steps_to_achieve_true_potential_response", context) ?: ""
        )
    }


}