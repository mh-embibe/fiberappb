package com.embibe.embibetvapp.ui.custom

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import com.embibe.embibetvapp.R


class LineView : View {
    private var classTag = LineView::class.java.simpleName
    private val paint = Paint()
    private var pointA: PointF? = null
    private var pointB: PointF? = null
    private var reverseGradient: Boolean = false
    private var isRelatedKeyConcept: Boolean = false

    constructor(context: Context?) : super(context)
    constructor(
        context: Context?,
        @Nullable attrs: AttributeSet?
    ) : super(context, attrs)

    constructor(
        context: Context?,
        @Nullable attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr)

    override fun onDraw(canvas: Canvas) {
        if (pointA != null && pointB != null) {
            setPaint()
            canvas.drawLine(pointA!!.x, pointA!!.y, pointB!!.x, pointB!!.y, paint)
//            Log.i(classTag, "PointA = $pointA PointB = $pointB")
        } else {
//            Log.i(classTag, "PointA = $pointA PointB = $pointB")
        }
        super.onDraw(canvas)
    }

    private fun setPaint() {
        val lineGradient = if (!isRelatedKeyConcept) {
            intArrayOf(
                ContextCompat.getColor(
                    context,
                    R.color.line_grad_1
                ),
                ContextCompat.getColor(
                    context,
                    R.color.line_grad_2
                ),
                ContextCompat.getColor(
                    context,
                    R.color.line_grad_3
                )
            )
        } else {
            intArrayOf(
                ContextCompat.getColor(
                    context,
                    R.color.white_opc72
                ),
                ContextCompat.getColor(
                    context,
                    R.color.white_opc72
                )
            )
        }
        if (!reverseGradient) {
            paint.shader = LinearGradient(
                pointB!!.x,
                pointB!!.y,
                pointA!!.x,
                pointA!!.y,
                lineGradient,
                null,
                Shader.TileMode.MIRROR
            )
        } else {
            paint.shader = LinearGradient(
                pointA!!.x,
                pointA!!.y,
                pointB!!.x,
                pointB!!.y,
                lineGradient,
                null,
                Shader.TileMode.MIRROR
            )
        }

        if (!isRelatedKeyConcept)
            paint.strokeWidth = 4f
        else
            paint.strokeWidth = 2f

        paint.apply {
            style = Paint.Style.STROKE
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val parentWidth = MeasureSpec.getSize(widthMeasureSpec)
        val parentHeight = MeasureSpec.getSize(heightMeasureSpec)
        setMeasuredDimension(parentWidth, parentHeight)
    }

    fun setPointA(point: PointF?) {
        pointA = point
    }

    fun setPointB(point: PointF?) {
        pointB = point
    }

    fun draw() {
        invalidate()
        requestLayout()
    }

    fun isRelatedKeyConcept(value: Boolean) {
        this.isRelatedKeyConcept = value
    }

    fun reverseGradient(reverseGradient: Boolean) {
        this.reverseGradient = reverseGradient
    }
}