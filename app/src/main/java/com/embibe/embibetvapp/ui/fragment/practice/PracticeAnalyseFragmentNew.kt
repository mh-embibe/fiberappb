package com.embibe.embibetvapp.ui.fragment.practice

import android.animation.Animator
import android.animation.ObjectAnimator
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentPracticeAnalyseNewBinding
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo

class PracticeAnalyseFragmentNew : BaseAppFragment() {

    private lateinit var binding: FragmentPracticeAnalyseNewBinding
    private var preview_url =
        "https://visualintelligence.blob.core.windows.net/video-summary/prod/4397012/summary.webp"
    private var jar_url = "https://assets7.lottiefiles.com/packages/lf20_G9rzGD.json"
    private var thumb =
        "https://content-grail-production.s3.amazonaws.com/practice-temp-tiles/1p45wyBVwdAeFP29gv57nU28ymT793kC1.webp"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_practice_analyse_new,
            container,
            false
        )
        initViewSincerity()
        setClickListener()
        setJarFocusListener()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewSincerity()
        setJarFocusListener()
        setClickListener()

    }

    private fun initViewSincerity() {
        val requestOptions = RequestOptions().transform(RoundedCorners(10))
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        Glide.with(requireContext())
            .applyDefaultRequestOptions(requestOptions)
            .load(thumb)
            .placeholder(R.drawable.video_placeholder)
            .into(binding.cardTwo.ivImg)

        binding.cardTwo.ivPlay.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.cardTwo.cardView.visibility = View.VISIBLE
                binding.cardTwo.ivGifView.visibility = View.VISIBLE
                val controller = Fresco.newDraweeControllerBuilder()
                controller.autoPlayAnimations = true
                controller.setUri(preview_url)
                controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                    override fun onFinalImageSet(
                        id: String?,
                        imageInfo: ImageInfo?,
                        animatable: Animatable?
                    ) {
                        val anim = animatable as AnimatedDrawable2
                        anim.setAnimationListener(object : AnimationListener {
                            override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                            override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                                binding.cardTwo.ivImg.visibility = View.INVISIBLE
                            }

                            override fun onAnimationFrame(
                                drawable: AnimatedDrawable2?,
                                frameNumber: Int
                            ) {

                            }

                            override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                                binding.cardTwo.ivImg.visibility = View.VISIBLE
                                binding.cardTwo.ivGifView.visibility = View.INVISIBLE
                                binding.cardTwo.cardView.visibility = View.INVISIBLE
                                binding.cardTwo.ivPlay.visibility = View.VISIBLE
                            }

                            override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                        })
                    }
                }
                binding.cardTwo.ivGifView.controller = controller.build()
            } else {
                binding.cardTwo.ivImg.visibility = View.VISIBLE
                binding.cardTwo.ivGifView.visibility = View.INVISIBLE
                binding.cardTwo.cardView.visibility = View.INVISIBLE
                binding.cardTwo.ivPlay.visibility = View.VISIBLE
            }
        }
    }


    private fun initViewAttemptQuality(jarType: String) {
        val requestOptions = RequestOptions().transform(RoundedCorners(10))
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        binding.cardFour.tvTopic.text = "30% " + jarType
        when (jarType) {
            AppConstants.CORRECT -> {
                Glide.with(requireContext())
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_too_fast_correct)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardFour.ivImg)
            }
            AppConstants.PERFECT -> {
                Glide.with(requireContext())
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_perfect_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardFour.ivImg)

            }
            AppConstants.OVERTIME_CORRECT -> {
                Glide.with(requireContext())
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_overtime_correct)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardFour.ivImg)

            }
            AppConstants.WASTED -> {
                Glide.with(requireContext())
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_wasted_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardFour.ivImg)

            }
            AppConstants.WRONG -> {
                Glide.with(requireContext())
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_incorrect_answer)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardFour.ivImg)

            }
            AppConstants.OVERTIME_WRONG -> {
                Glide.with(requireContext())
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_overtime_incorrect)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardFour.ivImg)

            }
        }

        binding.cardFour.ivPlay.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.cardFour.lvJar.visibility = View.VISIBLE
                binding.cardFour.ivImg.visibility = View.INVISIBLE
                when (jarType) {
                    AppConstants.CORRECT -> binding.cardFour.lvJar.setAnimation(R.raw.jar_correct_attempt)
                    AppConstants.PERFECT -> binding.cardFour.lvJar.setAnimation(R.raw.jar_perfect_attempt)
                    AppConstants.OVERTIME_CORRECT -> binding.cardFour.lvJar.setAnimation(R.raw.jar_overtime_correct_attempt)
                    AppConstants.WASTED -> binding.cardFour.lvJar.setAnimation(R.raw.jar_wasted_attempt)
                    AppConstants.WRONG -> binding.cardFour.lvJar.setAnimation(R.raw.jar_wrong_attempt)
                    AppConstants.OVERTIME_WRONG -> binding.cardFour.lvJar.setAnimation(R.raw.jar_overtime_incorrect_attempt)
                }
                binding.cardFour.lvJar.loop(true)
                binding.cardFour.lvJar.playAnimation()
            } else {
                binding.cardFour.lvJar.visibility = View.INVISIBLE
                binding.cardFour.ivPlay.visibility = View.VISIBLE
                binding.cardFour.ivImg.visibility = View.VISIBLE
            }
        }

        binding.cardThree.layout.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setClickListener()

            } else {

            }
        }
    }

    private fun setClickListener() {
        binding.cardFour.ivPlay.setOnClickListener {
            binding.cardThree.layout.visibility = View.VISIBLE
            binding.cardFour.layout.postDelayed({

                binding.cardThree.jarCorrectAttempt.requestFocus()
                binding.cardFour.layout.visibility = View.GONE
            }, 10)
        }

    }

    private fun setJarFocusListener() {

        binding.cardThree.jarCorrectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.CORRECT)
            }
        }
        binding.cardThree.jarPerfectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.PERFECT)
            }
        }
        binding.cardThree.jarOvertimeCorrectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.OVERTIME_CORRECT)
            }
        }
        binding.cardThree.jarWastedAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.WASTED)
            }
        }
        binding.cardThree.jarWrongAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.WRONG)
            }
        }
        binding.cardThree.jarOvertimeWrongAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.OVERTIME_WRONG)
            }
        }
    }

    private fun setJarClickListener(jarType: String) {
        when (jarType) {
            AppConstants.CORRECT -> binding.cardThree.jarCorrectAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)

                animateJar(jarType)
            }
            AppConstants.PERFECT -> binding.cardThree.jarPerfectAttempt.setOnClickListener {
                animateJar(jarType)

                // setAttemptQualityView(jarType)
            }
            AppConstants.OVERTIME_CORRECT -> binding.cardThree.jarOvertimeCorrectAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)

            }
            AppConstants.WASTED -> binding.cardThree.jarWastedAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)

            }
            AppConstants.WRONG -> binding.cardThree.jarWrongAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)

            }
            AppConstants.OVERTIME_WRONG -> binding.cardThree.jarOvertimeWrongAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)

            }
        }

    }

    private fun setAttemptQualityView(jarType: String) {
        initViewAttemptQuality(jarType)
        binding.cardThree.layout.visibility = View.GONE
        binding.cardFour.layout.visibility = View.VISIBLE
        binding.cardFour.layout.postDelayed({
            binding.cardFour.ivPlay.requestFocus()
            binding.cardThree.layout.visibility = View.GONE
        }, 10)
    }

    private fun animateJar(jarType: String) {
        when (jarType) {
            AppConstants.CORRECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -60f
            ).apply {
                duration = 1000
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(1)


                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 1000

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.PERFECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -120f
            ).apply {
                duration = 1000
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(2)


                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 1000


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.OVERTIME_CORRECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -180f
            ).apply {
                duration = 1000
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(3)


                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 1000


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.WASTED -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -240f
            ).apply {
                duration = 1000
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(4)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 1000


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.WRONG -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -300f
            ).apply {
                duration = 1000
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(5)


                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 1000


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.OVERTIME_WRONG -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -360f
            ).apply {
                duration = 1000
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(6)


                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 1000


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
        }
    }

    private fun viewAnimationImage(position: Int) {
        binding.imgAttemptJarIcon.alpha = 1.0F
        binding.imgAttemptJarIcon.visibility = View.VISIBLE
        when (position) {
            1 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_too_fast_correct)
            }
            2 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_perfect_attempt)

            }
            3 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_correct)

            }
            4 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_wasted_attempt)

            }
            5 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_incorrect_answer)

            }
            6 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_incorrect)

            }
        }
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        if (position < 5)
            lp.setMargins((position - 1) * 60, 20, 0, 0)
        if (position == 5)
            lp.setMargins((position - 1) * 70, 20, 0, 0)
        if (position == 6)
            lp.setMargins((position - 1) * 80, 20, 0, 0)

        binding.imgAttemptJarIcon.layoutParams = lp
    }


}