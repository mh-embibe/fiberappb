package com.embibe.embibetvapp.ui.fragment.achieve

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentDiagnosticTestBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.diagnostic.LifeSucces
import com.embibe.embibetvapp.model.diagnostic.ReadninessData
import com.embibe.embibetvapp.model.diagnostic.TestDiagnosticRes
import com.embibe.embibetvapp.ui.activity.TestActivity
import com.embibe.embibetvapp.ui.adapter.ImprovementDiagnosticAdapter
import com.embibe.embibetvapp.ui.adapter.LifeSuccessDiagnosticAdapter
import com.embibe.embibetvapp.ui.viewmodel.AchieveViewModel
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.Utils
import kotlinx.android.synthetic.main.fragment_diagnostic_test.view.*

class DiagnosticTestFragment : BaseAppFragment() {

    private lateinit var binding: FragmentDiagnosticTestBinding
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var achieveViewModel: AchieveViewModel

    private lateinit var syllabusDiagnosticAdapter: ImprovementDiagnosticAdapter
    private lateinit var improvementDiagnosticAdapter: ImprovementDiagnosticAdapter
    private lateinit var lifeSuccessDiagnosticAdapter: LifeSuccessDiagnosticAdapter

    private var listSyllabusReadiness: ArrayList<ReadninessData> = ArrayList()
    private var listImprovementPlan: ArrayList<ReadninessData> = ArrayList()
    private var listLifeSuccess: ArrayList<LifeSucces> = ArrayList()

    private val userGrade = UserData.getGrade()
    private val userGoal = UserData.getGoal()

    private var hideViewIfNoResponseCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        achieveViewModel = ViewModelProviders.of(this).get(AchieveViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_diagnostic_test, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHeaderTextNames()
        getDiagnosticTest()
    }

    private fun setHeaderTextNames() {
        binding.linearLayoutCompat2.titleSyllabusReadiness.text =
            getString(R.string.tests_for_scoring_high_marks_in_class) + " " + userGrade
        binding.linearLayoutCompat.titleLifeSuccess.text =
            getString(R.string.tests_for_securing_your_future_by_cementing_your_knowledge_till_class) + " " + userGrade
    }

    /*load DiagnosticTest from api*/
    fun getDiagnosticTest() {
        showProgress()
        achieveViewModel.getDiagnosticTest(UserData.getExamName(), UserData.getGoal(), object :
            BaseViewModel.APICallBacks<TestDiagnosticRes> {

            override fun onSuccess(model: TestDiagnosticRes?) {
                hideProgress()
                if (model != null) {

                    setTitleDescriptions(model)
                    listSyllabusReadiness = model.syllabusReadiness as ArrayList<ReadninessData>
                    listImprovementPlan = model.improvementPlan as ArrayList<ReadninessData>
                    listLifeSuccess = model.lifeSuccess as ArrayList<LifeSucces>

                    setSyllabusReadinessRecycler(requireView(), listSyllabusReadiness)
                    setImprovementPlanRecycler(requireView(), listImprovementPlan)
                    setLifeSuccessRecycler(requireView(), listLifeSuccess)
                    if (hideViewIfNoResponseCount == 3) {
                        binding.tvNoResponseMsg.visibility = View.VISIBLE
                    }
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getDiagnosticTest()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }
        })
    }

    private fun setTitleDescriptions(model: TestDiagnosticRes) {

        /*binding.titleSyllabusReadiness.text = model.syllabusReadiness?.title
        binding.descriptionSyllabusReadiness.text = model.syllabusReadiness?.text

        binding.titleImprovementPlan.text = model.improvementPlan?.title

        binding.titleLifeSuccess.text = model.lifeSuccess?.title
        binding.descriptionLifeSuccess.text = model.lifeSuccess?.text*/
    }

    private fun tvRvVisibilltyChange(tv: TextView, rv: RecyclerView, visibility: Int) {
        tv.visibility = visibility
        rv.visibility = visibility
    }

    private fun setSyllabusReadinessRecycler(view: View, result: ArrayList<ReadninessData>) {
        if (result.isNullOrEmpty()) {
            tvRvVisibilltyChange(
                binding.titleSyllabusReadiness,
                binding.recyclerSyllabusReadiness,
                View.GONE
            )
            binding.descriptionSyllabusReadiness.visibility = View.GONE
            hideViewIfNoResponseCount++
        } else {
            tvRvVisibilltyChange(
                binding.titleSyllabusReadiness,
                binding.recyclerSyllabusReadiness,
                View.VISIBLE
            )
            binding.descriptionSyllabusReadiness.visibility = View.VISIBLE
        }
        syllabusDiagnosticAdapter = ImprovementDiagnosticAdapter()
        binding.recyclerSyllabusReadiness.layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerSyllabusReadiness.adapter = syllabusDiagnosticAdapter
        syllabusDiagnosticAdapter.setData(result)

        syllabusDiagnosticAdapter.onItemClick = { test ->
            callTestActivity(test)
        }
        binding.recyclerSyllabusReadiness.requestFocus()
    }

    private fun setImprovementPlanRecycler(view: View, result: ArrayList<ReadninessData>) {
        if (result.isNullOrEmpty()) {
            tvRvVisibilltyChange(
                binding.titleImprovementPlan,
                binding.recyclerImprovementPlan,
                View.GONE
            )
            hideViewIfNoResponseCount++
        } else {
            tvRvVisibilltyChange(
                binding.titleImprovementPlan,
                binding.recyclerImprovementPlan,
                View.VISIBLE
            )
        }
        improvementDiagnosticAdapter = ImprovementDiagnosticAdapter()
        binding.recyclerImprovementPlan.layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerImprovementPlan.adapter = improvementDiagnosticAdapter
        improvementDiagnosticAdapter.setData(result)

            syllabusDiagnosticAdapter.onItemClick = { test ->
                callTestActivity(test)
            }

    }

    private fun setLifeSuccessRecycler(view: View, result: ArrayList<LifeSucces>) {
        var testEmptyFlag = false
        if (result.isNullOrEmpty()) {
            tvRvVisibilltyChange(binding.titleLifeSuccess, binding.recyclerLifeSuccess, View.GONE)
            binding.descriptionLifeSuccess.visibility = View.GONE
            hideViewIfNoResponseCount++
        } else {
//            for (res in result) {
//                if (res.tests.isEmpty())
//                    testEmptyFlag = true
//            }
            for (resIndex in 0 until result.size) {
                if(result[resIndex].tests.isNotEmpty()) {
                    testEmptyFlag = false
                    break
                } else if(result[resIndex].tests.isEmpty()) {
                    testEmptyFlag = true
                }
            }
                if (testEmptyFlag) {
                    tvRvVisibilltyChange(
                        binding.titleLifeSuccess,
                        binding.recyclerLifeSuccess,
                        View.GONE
                    )
                    binding.descriptionLifeSuccess.visibility = View.GONE
                    hideViewIfNoResponseCount++
                } else {
                    tvRvVisibilltyChange(
                        binding.titleLifeSuccess,
                        binding.recyclerLifeSuccess,
                        View.VISIBLE
                    )
                    binding.descriptionLifeSuccess.visibility = View.VISIBLE
                }
        }
        lifeSuccessDiagnosticAdapter = LifeSuccessDiagnosticAdapter(view.context)
        binding.recyclerLifeSuccess.layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerLifeSuccess.adapter = lifeSuccessDiagnosticAdapter
        lifeSuccessDiagnosticAdapter.setData(result)

        lifeSuccessDiagnosticAdapter.onItemClick = { test ->
            callTestActivity(test)
        }
    }

    private fun callTestActivity(test: ReadninessData) {
        val testIntent = Intent(requireContext(), TestActivity::class.java)
        testIntent.putExtra(AppConstants.TEST_XPATH, test.xpath)
        testIntent.putExtra(AppConstants.TEST_CODE, test.bundle_id)
        testIntent.putExtra(AppConstants.IS_DIAGNOSTIC, true)
        startActivity(testIntent)
        activity?.finish()
    }
}