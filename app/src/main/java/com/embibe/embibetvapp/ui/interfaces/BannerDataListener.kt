package com.embibe.embibetvapp.ui.interfaces

import com.embibe.embibetvapp.model.HeaderBannerData

interface BannerDataListener {
    fun onHeaderItemChange(data: HeaderBannerData)
}