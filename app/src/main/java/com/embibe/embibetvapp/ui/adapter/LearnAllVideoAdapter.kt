package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.graphics.drawable.Animatable
import android.graphics.drawable.GradientDrawable
import android.os.CountDownTimer
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ItemLearnCardBinding
import com.embibe.embibetvapp.databinding.ItemLearnCardGifBinding
import com.embibe.embibetvapp.databinding.ItemLearnHeaderBinding
import com.embibe.embibetvapp.databinding.ItemLearnSubHeaderBinding
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.util.*
import kotlin.collections.ArrayList

class LearnAllVideoAdapter(private val mContext: Context) :
    RecyclerView.Adapter<LearnAllVideoAdapter.ItemViewHolder<*>>() {
    var list: ArrayList<Content> = ArrayList()
    var labelsWithSequenceList: ArrayList<String> = ArrayList()
    val TAG = this.javaClass.name
    var onItemClick: ((Content) -> Unit)? = null
    var onItemFocused: ((Int) -> Unit?)? = null
    var onItemDpadHit: ((Int) -> Unit)? = null
    var onItemDpadUpHit: ((Int) -> Unit)? = null
    private var previewTimer: CountDownTimer? = null

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_SUB_HEADER = 1
        private const val TYPE_CARD = 2
        private const val TYPE_GIF = 3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder<Content> {
        return when (viewType) {
            TYPE_HEADER -> {
                val binding: ItemLearnHeaderBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_learn_header,
                    parent,
                    false
                )
                HeaderViewHolder(binding)
            }
            TYPE_SUB_HEADER -> {
                val binding: ItemLearnSubHeaderBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_learn_sub_header,
                    parent,
                    false
                )
                SubHeaderViewHolder(binding)
            }
            TYPE_CARD -> {
                val binding: ItemLearnCardBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_learn_card,
                    parent,
                    false
                )
                CardViewHolder(binding)
            }
            TYPE_GIF -> {
                val binding: ItemLearnCardGifBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_learn_card_gif, parent, false
                )
                GifCardViewHolder(binding)
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder<*>, position: Int) {
        val item = list[position]
        when (holder) {
            is HeaderViewHolder -> holder.bind(item)
            is SubHeaderViewHolder -> holder.bind(item)
            is CardViewHolder -> holder.bind(item)
            is GifCardViewHolder -> holder.bind(item)
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemViewType(position: Int): Int {
        val value: Int
        val item: Content = list[position]
        value = when (item.type.toLowerCase(Locale.getDefault())) {
            AppConstants.TYPE_HEADER -> TYPE_HEADER
            AppConstants.TYPE_SUB_HEADER -> TYPE_SUB_HEADER
            AppConstants.TYPE_VIDEO -> {
                TYPE_GIF

            }
            AppConstants.TYPE_CARD, AppConstants.TYPE_COOBO -> TYPE_CARD
            else -> TYPE_CARD
        }
        return value
    }

    fun setData(data: ArrayList<Content>) {
        labelsWithSequenceList.clear()
        list = data
        var incr = 0
        list.forEach { content ->
            if (content.type.equals(AppConstants.TYPE_VIDEO, true)) {
                incr += 1
                labelsWithSequenceList.add("${incr}. ${content.title}")
            } else {
                incr = 0
                labelsWithSequenceList.add("")
            }
        }
        notifyDataSetChanged()
    }

    abstract class ItemViewHolder<Content>(binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        abstract fun bind(item: Content)
    }

    inner class HeaderViewHolder(var binding: ItemLearnHeaderBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {

            if (list.isNotEmpty()) {
                binding.contentModel = item
                if (item.currency == 0) {
                    binding.embiumsCount.visibility = View.GONE
                } else {
                    binding.embiumsCount.visibility = View.VISIBLE
                }
                binding.tvHeader.visibility = View.VISIBLE
            } else
                binding.tvHeader.visibility = View.GONE
        }

        init {
            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    onItemFocused?.invoke(adapterPosition)
                    SegmentUtils.trackEventSearchTileCaroselFocus(
                        itemView.verticalScrollbarPosition.toString(),
                        TYPE_HEADER.toString(),
                        JSONParser().parse(Gson().toJson(binding.tvHeader)) as JSONObject,
                        "Search",
                        "Header"
                    )
                }
            }
        }
    }

    inner class SubHeaderViewHolder(var binding: ItemLearnSubHeaderBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {

            if (list.isNotEmpty()) {
                binding.contentModel = item
                if (item.currency == 0) {
                    binding.embiumsCount.visibility = View.GONE
                } else {
                    binding.embiumsCount.visibility = View.VISIBLE
                }
                binding.tvHeader.visibility = View.VISIBLE
            } else
                binding.tvHeader.visibility = View.GONE
        }

        init {
            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    onItemFocused?.invoke(adapterPosition)
                    SegmentUtils.trackEventSearchTileCaroselFocus(
                        itemView.verticalScrollbarPosition.toString(),
                        TYPE_HEADER.toString(),
                        JSONParser().parse(Gson().toJson(binding.tvHeader)) as JSONObject,
                        "Search",
                        "Header"
                    )
                }
            }
        }
    }

    inner class GifCardViewHolder(var binding: ItemLearnCardGifBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            if (item.preview_url.isNotEmpty()) {
                binding.layout.setOnFocusChangeListener { v, hasFocus ->
                    if (hasFocus) {
                        previewWithDelay()
                    } else {
                        previewTimer?.cancel()
                        binding.ivGifView.controller?.animatable?.stop()
                        binding.ivImg.visibility = View.VISIBLE
                        binding.ivGifView.visibility = View.INVISIBLE
                    }
                }
            }
            binding.detailItem = item
            Glide.with(mContext).load(item.owner_info.copy_logo).into(binding.ivEmbibeLogo)
            if (item.type != "header") {
                if (item.category_thumb.isNotEmpty()) {
                    binding.ivCategory.visibility = View.VISIBLE
                    Glide.with(itemView.context)
                        .load(item.category_thumb)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(binding.ivCategory)
                } else {
                    binding.ivCategory.visibility = View.GONE
                }

                binding.textTime.text = Utils.convertIntoMinsTest(item.length)
            }
            binding.tvTitle.text = labelsWithSequenceList[list.indexOf(item)]
            binding.tvSubjectName.visibility = View.GONE

            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            if (item.thumb == "thumb" || item.thumb == "") {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            } else {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(item.thumb)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            }

            for (tag in item.learning_map.tags!!) {
                if (tag.toLowerCase() == "important") {
                    binding.tvImportantTag.visibility = View.VISIBLE
                } else {
                    binding.tvImportantTag.visibility = View.GONE
                }
            }
        }

        private fun previewWithDelay() {
            previewTimer = object : CountDownTimer(4000, 1000) {
                override fun onFinish() {
                    showPreview()
                }

                override fun onTick(millisUntilFinished: Long) {}
            }.start()
        }

        private fun showPreview() {
            binding.ivGifView.visibility = View.VISIBLE
            val controller = Fresco.newDraweeControllerBuilder()
            controller.autoPlayAnimations = true
            controller.setUri(list[adapterPosition].preview_url)
            controller.controllerListener =
                object : BaseControllerListener<ImageInfo>() {
                    override fun onFinalImageSet(
                        id: String?,
                        imageInfo: ImageInfo?,
                        animatable: Animatable?
                    ) {
                        val anim = animatable as AnimatedDrawable2
                        anim.setAnimationListener(object : AnimationListener {
                            override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                            override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                                binding.ivImg.visibility = View.INVISIBLE
                            }

                            override fun onAnimationFrame(
                                drawable: AnimatedDrawable2?,
                                frameNumber: Int
                            ) {

                            }

                            override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                                binding.ivImg.visibility = View.VISIBLE
                                binding.ivGifView.visibility = View.INVISIBLE
                            }

                            override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                        })
                    }
                }
            binding.ivGifView.controller = controller.build()
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
                var item: Content? = binding.detailItem
                if (item != null) {
                    SegmentUtils.trackEventMoreInfoMenuItemTileClick(item)
                }
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(adapterPosition)
                var item: Content? = binding.detailItem
                if (item != null) {
                    SegmentUtils.trackEventMoreInfoMenuItemTileFocus(item)
/*
                    SegmentUtils.trackEventSearchTileCaroselFocus()
*/
                }

            }

            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            onItemDpadHit?.invoke(adapterPosition)
                        }
                    }

                }

                false
            }
        }
    }

    inner class CardViewHolder(var binding: ItemLearnCardBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
            setDynamicDetails(item)
            Glide.with(itemView.context)
                .load(item.owner_info.copy_logo)
                .into(binding.ivEmbibeLogo)

            if (item.category_thumb.isNotEmpty()) {
                binding.ivCategory.visibility = View.VISIBLE
                Glide.with(itemView.context)
                    .load(item.category_thumb)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(binding.ivCategory)
            } else {
                binding.ivCategory.visibility = View.GONE
            }

            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            if (item.thumb == "thumb" || item.thumb == "") {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.video_placeholder)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(binding.ivImg)
            } else {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(item.thumb)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            }
        }

        private fun setDynamicDetails(item: Content) {
            binding.textTime.text = Utils.convertIntoMinsTest(item.length)
            if (item.subject.isNotEmpty()) {
                binding.tvSubjectName.visibility = View.VISIBLE
                binding.tvSubjectName.text = item.subject
                (binding.tvSubjectName.background as GradientDrawable).setColor(
                    itemView.context.resources.getColor(
                        Utils.getColorCode(
                            item.subject
                        )
                    )
                )
            } else {
                binding.tvSubjectName.visibility = View.GONE
            }
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])

                SegmentUtils.trackEventSearchTileCaroselClick(
                    itemView.verticalScrollbarPosition.toString(),
                    TYPE_CARD.toString(),
                    JSONParser().parse(getJson(binding.detailItem)) as JSONObject,
                    "Search",
                    "CARD"
                )
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    onItemFocused?.invoke(adapterPosition)
                    SegmentUtils.trackEventSearchTileCaroselFocus(
                        itemView.verticalScrollbarPosition.toString(),
                        TYPE_CARD.toString(),
                        JSONParser().parse(getJson(binding.detailItem)) as JSONObject,
                        "Search",
                        "CARD"
                    )
                }
            }

            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            onItemDpadHit?.invoke(adapterPosition)
                        }
                        KeyEvent.KEYCODE_DPAD_UP -> {
                            if (adapterPosition == 0) {
                                onItemDpadUpHit?.invoke(adapterPosition)
                            }
                        }
                    }

                }

                false
            }
        }
    }

    private fun getJson(detailItem: Content?): String {
        try {
            val gsonBuilder = GsonBuilder()
            val gson: Gson = Gson()
            return gson.toJson(detailItem)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }
}