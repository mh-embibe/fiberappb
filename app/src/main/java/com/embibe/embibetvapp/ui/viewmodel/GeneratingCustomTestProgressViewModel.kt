package com.embibe.embibetvapp.ui.viewmodel

import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.model.createtest.examconfig.ChapterDetails
import com.embibe.embibetvapp.model.createtest.generateowntest.response.GenerateOwnTestResponse
import com.embibe.embibetvapp.model.createtest.progress.response.ProgressResponse
import com.embibe.embibetvapp.network.repo.CreateTestRepository
import java.util.*

class GeneratingCustomTestProgressViewModel
    : BaseViewModel() {
    var repo = CreateTestRepository()

    fun testProgress(
        data: GenerateOwnTestResponse, testName: String,
        createdAt: String,
        difficultyLevel: String,
        duration: Int, chapterDetails: ArrayList<ChapterDetails>?,
        callback: APICallBacks<ProgressResponse>
    ) {

        var requestParamsGeneratingTest =
            repo.progressRequest(
                data,
                testName,
                createdAt,
                difficultyLevel,
                duration,
                chapterDetails
            )
        safeApi(repo.progress(requestParamsGeneratingTest), callback)
    }
}