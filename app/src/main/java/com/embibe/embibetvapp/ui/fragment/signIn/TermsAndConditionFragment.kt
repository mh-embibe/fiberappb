package com.embibe.embibetvapp.ui.fragment.signIn

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.databinding.FragmentTermsAndConditionBinding
import com.embibe.embibetvapp.utils.SegmentUtils

class TermsAndConditionFragment : BaseAppFragment() {

    private lateinit var binding: FragmentTermsAndConditionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SegmentUtils.trackTermsAndConditionLoadStart()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_terms_and_condition,
            container,
            false
        )
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        SegmentUtils.trackTermsAndConditionLoadEnd()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val html =
            "<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "  <head>\n" +
                    "    <meta charset=\"UTF-8\" />\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n" +
                    "    <title>Document</title>\n" +
                    "  </head>\n" +
                    "  <style>\n" +
                    "    .body {\n" +
                    "      margin: 6%;\n" +
                    "      background-image: linear-gradient(45deg, rgb(13, 65, 99), rgb(3, 16, 79), rgb(13, 65, 99));\n" +
                    "    /* color: rgb(255, 255, 255); */\n" +
                    "color: white;\n" +
                    "    }\n" +
                    "    .toc,\n" +
                    "    .ibu {\n" +
                    "      font-size: 25px;\n" +
//                    "      font-weight: bold;   \n" +
                    "    }\n" +
                    "    .create-account{\n" +
                    "        margin-left: 20px;\n" +
                    "    }\n" +
                    "    .p {\n" +
                    "        font-size: 16px;\n" +
                    "    }\n" +
                    "  </style>\n" +
                    "  <body class=\"body\">\n" +
                    "    <div class=\"toc\">\n" +
                    "      Terms of Service\n" +
                    "    </div>\n" +
                    "    <div class=\"ibu\">\n" +
                    "      INFORMATION ABOUT USE\n" +
                    "    </div>\n" +
                    "    <br />\n" +
                    "    <p>\n" +
                    "      Welcome to Individual Learning Private Limited (”Embibe” or “We” or “Us”),\n" +
                    "      a company incorporated under the Companies Act, 1956, operating from its\n" +
                    "      web site being https://www.embibe.com/ and its related application\n" +
                    "      programming interfaces (API’s), mobile applications and online services\n" +
                    "      (collectively, “Website”).\n" +
                    "    </p>\n" +
                    "    <br />\n" +
                    "\n" +
                    "    <p>\n" +
                    "      &nbsp; &nbsp; &nbsp; This Terms of Service is a legal contract between you\n" +
                    "      (“You”) and Embibe regarding your use of the Website. For ease of\n" +
                    "      reference, the visitors and users of the Website are referred to\n" +
                    "      individually as “User” and collectively as “Users”.\n" +
                    "    </p>\n" +
                    "    <p>\n" +
                    "      &nbsp; &nbsp; &nbsp; PLEASE READ THIS TERMS OF SERVICE CAREFULLY. BY\n" +
                    "      REGISTERING FOR, ACCESSING, BROWSING, OR USING THE WEBSITE, YOU\n" +
                    "      ACKNOWLEDGE THAT YOU HAVE READ, UNDERSTOOD, AND AGREE TO BE BOUND BY THIS\n" +
                    "      TERMS OF SERVICE, INCLUDING THE EMBIBE PRIVACY NOTICE\n" +
                    "      (https://www.embibe.com/privacy-policy) , THIRD PARTY CONTENT POLICY\n" +
                    "      (include appropriate URL as and when it is made live) AND ANY ADDITIONAL\n" +
                    "      GUIDELINES (AS DEFINED BELOW) (COLLECTIVELY, THE “TERMS” or “TERMS OF\n" +
                    "      SERVICE”).\n" +
                    "    </p>\n" +
                    "    <p>\n" +
                    "      1. ELIGIBILITY <br />\n" +
                    "      You may use our services only if you form a binding contract with Embibe,\n" +
                    "      and only in compliance with these Terms and the applicable laws.\n" +
                    "    </p>\n" +
                    "    <p>\n" +
                    "      2. COMPLIANCE WITH APPLICABLE LAWS <br />\n" +
                    "      As a condition of Your access to and use of the Website, You agree that\n" +
                    "      You will not use our services for any purpose that is unlawful or\n" +
                    "      prohibited by these Terms and that you will comply with all applicable\n" +
                    "      laws and any conditions or restrictions imposed by these Terms.\n" +
                    "    </p>\n" +
                    "    <p>\n" +
                    "      3. ACCOUNT <br />\n" +
                    "      You need not register with the Embibe to simply visit and view the\n" +
                    "      Website, but to access and participate in certain features of the Website,\n" +
                    "      You will need to create a password-protected account (Account). <br />\n" +
                    "    <div class=\"create-account\"> To create an Account, you must submit your name, email address and such\n" +
                    "      other details as may be requested through the Account registration page on\n" +
                    "      the Website and create a password. You will also have the ability to\n" +
                    "      provide additional optional information, which is not required to register\n" +
                    "      for an Account but shall be helpful to Embibe in providing you with a more\n" +
                    "      customized experience when using the Website. You may also register for an\n" +
                    "      Account using Your existing Facebook, Google or any other account\n" +
                    "      integrated by the Embibe with the Website and their respective log-in\n" +
                    "      credentials.\n" +
                    "    </div>\n" +
                    "    <br />\n" +
                    "    <div >\n" +
                    "      When You create your account with Embibe in order to use\n" +
                    "      certain features of the Website and/or other platforms whereby our\n" +
                    "      services are provided, You may be asked to provide a password in\n" +
                    "      connection with your account.\n" +
                    "    </div>\n" +
                    "    <div class=\"create-account\" > \n" +
                    "    This creation of an account entitles You to\n" +
                    "      be Registered User with Embibe. You acknowledge and agree that:</div>\n" +
                    "      <br />\n" +
                    "      <div class=\"create-account\"> \n" +
                    "      <p>\n" +
                    "      3.1 You\n" +
                    "      are solely responsible for maintaining the confidentiality of your account\n" +
                    "      and password, and for all the activities that occur in relation to your\n" +
                    "      account or password;\n" +
                    "    </p>\n" +
                    "    <p>\n" +
                    "      3.2 The information provided by You to Embibe is\n" +
                    "      true, accurate, current, complete and will be updated by You as and when\n" +
                    "      required\n" +
                    "    </p>\n" +
                    "    <p>\n" +
                    "     3.3 You may immediately notify Embibe in an event of unauthorized\n" +
                    "      use of your account \n" +
                    "    </p>\n" +
                    "    <p>\n" +
                    "    3.4 Embibe will not be held liable for any loss or\n" +
                    "      damage whatsoever resulting from the disclosure of your password contrary\n" +
                    "      to these Terms\n" +
                    "    </p>\n" +
                    "    </div>\n" +
                    "\n" +
                    "    </p>\n" +
                    "  <p>\n" +
                    "    YOU ACKNOWLEDGE THAT YOU HAVE READ, UNDERSTOOD, AND AGREE TO BE BOUND BY\n" +
                    "    THIS TERMS OF SERVICE, INCLUDING THE EMBIBE PRIVACY NOTICE\n" +
                    "    (https://www.embibe.com/privacy-policy) , THIRD PARTY CONTENT POLICY\n" +
                    "    (include appropriate URL as and when it is made live) AND ANY ADDITIONAL\n" +
                    "    GUIDELINES (AS DEFINED BELOW) (COLLECTIVELY, THE “TERMS” or “TERMS OF\n" +
                    "    SERVICE”). \n" +
                    "  </p>\n" +
                    "    \n" +
                    "    <p>\n" +
                    "    1. ELIGIBILITY <br />You may use our services only if you form a\n" +
                    "    binding contract with Embibe, and only in compliance with these Terms and\n" +
                    "    the applicable laws.\n" +
                    "</p>\n" +
                    "    <p>\n" +
                    "    2. COMPLIANCE WITH APPLICABLE LAWS <br />As a condition of\n" +
                    "    Your access to and use of the Website, You agree that You will not use our\n" +
                    "    services for any purpose that is unlawful or prohibited by these Terms and\n" +
                    "    that you will comply with all applicable laws and any conditions or\n" +
                    "    restrictions imposed by these Terms. \n" +
                    "</p>\n" +
                    "<p>\n" +
                    "    3. ACCOUNT <br /> You need not register with\n" +
                    "    the Embibe to simply visit and view the Website, but to access and\n" +
                    "    participate in certain features of the Website, You will need to create a\n" +
                    "    password-protected account (Account). To create an Account, you must submit\n" +
                    "    your name, email address and such other details as may be requested through\n" +
                    "    the Account registration page on the Website and create a password. You will\n" +
                    "    also have the ability to provide additional optional information, which is\n" +
                    "    not required to register for an Account but shall be helpful to Embibe in\n" +
                    "    providing you with a more customized experience when using the Website. You\n" +
                    "    may also register for an Account using Your existing Facebook, Google or any\n" +
                    "    other account integrated by the Embibe with the Website and their respective\n" +
                    "    log-in credentials. \n" +
                    "    </p>\n" +
                    "    <p>\n" +
                    "    When You create your account with Embibe in order to use\n" +
                    "    certain features of the Website and/or other platforms whereby our services\n" +
                    "    are provided, You may be asked to provide a password in connection with your\n" +
                    "    account. This creation of an account entitles You to be Registered User with\n" +
                    "    Embibe. You acknowledge and agree that:\n" +
                    "</p>\n" +
                    "<div class=\"create-account\">\n" +
                    "<p>\n" +
                    "      3.1 You are solely responsible for\n" +
                    "    maintaining the confidentiality of your account and password,  and for all\n" +
                    "    the activities that occur in relation to your account or password; </p>\n" +
                    "    \n" +
                    "   <p> 3.2 The\n" +
                    "    information provided by You to Embibe is true, accurate, current, complete\n" +
                    "    and will be updated by You as and when required </p> \n" +
                    "    <p>\n" +
                    "    3.3 You may immediately\n" +
                    "    notify Embibe in an event of unauthorized use of your account 3.4 Embibe\n" +
                    "    will not be held liable for any loss or damage whatsoever resulting from the\n" +
                    "    disclosure of your password contrary to these Terms\n" +
                    "</p>\n" +
                    "</div>\n" +
                    "  </body>\n" +
                    "</html>"

        binding.webviewTnc.loadDataWithBaseURL("", html, "text/html", "UTF-8", "")

        binding.webviewTnc.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            SegmentUtils.trackTermsAndConditionScroll()
        }
    }


}