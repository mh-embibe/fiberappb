package com.embibe.embibetvapp.ui.adapter

import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View.INVISIBLE
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemSubSubTestMenuBinding
import com.embibe.embibetvapp.model.test.TestBehaviour
import com.embibe.embibetvapp.ui.activity.TestCWAActivity
import com.embibe.embibetvapp.utils.SegmentUtils

class TestFeedbackSubSubMenuAdapter :
    RecyclerView.Adapter<TestFeedbackSubSubMenuAdapter.TestsViewHolder>() {

    var list = ArrayList<TestBehaviour>()
    var onItemClick: ((TestBehaviour) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestsViewHolder {
        val binding: ItemSubSubTestMenuBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_sub_sub_test_menu, parent, false
        )
        return TestsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (list.size > 0) list.size else 0
    }

    override fun onBindViewHolder(holder: TestsViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun setData(list: ArrayList<TestBehaviour>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class TestsViewHolder(var binding: ItemSubSubTestMenuBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: TestBehaviour) {
            binding.textBehaviourType.text = item.behavior_name
            binding.executePendingBindings()

            if (adapterPosition == list.size - 1) {
                binding.verticalViewBottom.visibility = INVISIBLE
            }

            val requestOptions = RequestOptions().transform(RoundedCorners(16))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            if (item.content_thumb == null || item.content_thumb == "thumb" || item.content_thumb == "") {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            } else {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(item.content_thumb)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            }
            binding.subImgLayout.setOnFocusChangeListener { v, hasFocus ->
                when(hasFocus) {
                    true -> {
                        SegmentUtils.trackTestFeedbackScreenImproveTestTakingStrategyVideoFocus(
                            list[adapterPosition].content_type.toString(),item.behavior_name,null)
                    }

                }
            }
            binding.subImgLayout.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        SegmentUtils.trackTestFeedbackScreenImproveTestTakingStrategyVideoClick(
                            list[adapterPosition].content_type.toString(),item.behavior_name,null)
                        }
                    }
                }

                false
            }

        }
    }
}