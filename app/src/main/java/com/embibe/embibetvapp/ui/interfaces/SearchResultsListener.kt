package com.embibe.embibetvapp.ui.interfaces

/**
 * @author Arpit Johri
 *
 * This interface will provide callback for search results
 */

interface SearchResultsListener {

    /**
     * @param count signifies the number of search results returned
     */
    fun searchResultsCount(count: Int)

}