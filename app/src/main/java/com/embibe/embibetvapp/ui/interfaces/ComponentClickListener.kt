package com.embibe.embibetvapp.ui.interfaces

interface ComponentClickListener {
    fun onClicked(type: String, data: Any)
}