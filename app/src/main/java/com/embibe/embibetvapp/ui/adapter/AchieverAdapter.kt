package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemEmbibeAchieversBinding
import com.embibe.embibetvapp.model.Achiever
import com.embibe.embibetvapp.ui.custom.FlipView
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.view.SimpleDraweeView
import kotlinx.android.synthetic.main.item_embibe_achievers.view.*


class AchieverAdapter(var context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var binding: ItemEmbibeAchieversBinding
    private val classTag = AchieverAdapter::class.java.simpleName
    var list = ArrayList<Achiever>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_embibe_achievers,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (list.size > 0) list.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val randomIndex = list.indexOf(list.random())
        viewHolder.bind(list[position], list[(list.size - 1) - randomIndex])
        if (viewHolder.itemView.flipView.currentFlipState == FlipView.FlipState.FRONT_SIDE && list[position].isFlipped) {
            viewHolder.itemView.flipView.setFlipDuration(0)
            viewHolder.itemView.flipView.flipTheView()
        }
    }

    fun setData(list: ArrayList<Achiever>) {
        this.list = list
        notifyDataSetChanged()
    }


    inner class ViewHolder(var binding: ItemEmbibeAchieversBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var front: Achiever? = null
        var back: Achiever? = null

        fun bind(front: Achiever, back: Achiever) {
            this.front = front
            this.back = back
            when (adapterPosition) {
                0, 1, 2, 3, 4 -> {
                    startBinding(front, back)
                }
                else -> {
                    fadeInAndBind(front, back)
                }
            }
        }

        private fun fadeInAndBind(front: Achiever, back: Achiever) {
            itemView.visibility = View.GONE
            itemView.animate().alpha(0.2f)
            itemView.postDelayed({
                itemView.visibility = View.VISIBLE
                itemView.animate().alpha(1.0f)
            }, SubjectReadinessAdapter.Value.DURATION)
            startBinding(front, back)
        }

        private fun startBinding(
            front: Achiever,
            back: Achiever
        ) {
            binding.item1 = front
            if (front.image.isNullOrEmpty()) {
                playWebPAnimationWithFresco(
                    binding.ivEmbibeAchieverFront,
                    R.drawable.embibe_achievers_gif,
                    ""
                )
            } else {
                playWebPAnimationWithFresco(binding.ivEmbibeAchieverFront, 0, front.image!!)
            }
            binding.executePendingBindings()
        }
    }

    private fun playWebPAnimationWithFresco(
        view: SimpleDraweeView,
        resource: Int,
        image: String
    ) {
        val controller = Fresco.newDraweeControllerBuilder()
        controller.autoPlayAnimations = true
        if (resource == 0)
            controller.setUri(Uri.parse(image))
        else {
            val res = "res:/" + resource
            controller.setUri(Uri.parse(res))
        }

        view.controller = controller.build()
    }
}