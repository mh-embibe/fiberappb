package com.embibe.embibetvapp.ui.fragment.learn

import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.databinding.CardHeaderBannerBinding
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.google.android.exoplayer2.util.Log
import kotlinx.android.synthetic.main.card_header_banner.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class BannerHeaderFragment(var type: String) : BaseAppFragment() {


    lateinit var binding: CardHeaderBannerBinding
    lateinit var DPadKeysListener: DPadKeysListener
    lateinit var navigationMenuCallback: NavigationMenuCallback
    val TAG = "BannerHeader"
    var whichBtnFocused = ""
    var BTN_LEARN = "0"
    var BTN_MORE_INFO = "1"
    var BTN_GRADE = "2"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.card_header_banner, container,
            false
        )
        setViewAccToType()
        whichBtnFocused = "0"

        if (type == "learn") {
            binding.btnLearn.postDelayed({
                binding.btnLearn.requestFocus()
            }, 100)
        }

        if (type == "practice" || type == "test") {
            binding.title.text = "Polynomials"
            binding.embiumsCount.text = "Earn 100"
            binding.textTime.text = "32 Concepts"
            binding.textTopic.text = "Mathematics"
            binding.textDescription.text =
                "A polynomial is an expression consisting of variables (also called indeterminates) and coefficients, that involves only the operations of addition, subtraction, multiplication and non-negative integer exponents of variables. "
            binding.constraint1.visibility = View.GONE
            binding.constraint2.visibility = View.GONE
            binding.constraint3.visibility = View.GONE
            binding.btnPractice.postDelayed({
                binding.btnPractice.requestFocus()
            }, 100)
        }
        binding.btnPractice.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.btnPractice.setOnKeyListener { v, keyCode, event ->
                    if (event.action == KeyEvent.ACTION_DOWN) {
                        when (keyCode) {
                            KeyEvent.KEYCODE_DPAD_DOWN -> {
                                DPadKeysListener.isKeyPadDown(true, "Banner")
                            }
                            KeyEvent.KEYCODE_DPAD_LEFT -> {
                                navigationMenuCallback.navMenuToggle(true)
                            }
                        }
                    }
                    false
                }
            }
        }
        binding.btnLearn.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                trackEventHomeMainBtnFocus(
                    bannerData,
                    com.embibejio.coreapp.constant.AppConstants.SLIDE_TYPE_COOBO
                )
                btnLearnKeyListener()
                btnLearn.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_play_circle_filled_black_24dp,
                    0,
                    0,
                    0
                )
                whichBtnFocused = BTN_LEARN
                Log.e(TAG, " btnlearn ${getBtnLastFocused()}")
            } else {
                btnLearn.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_play_circle_filled_white_24dp,
                    0,
                    0,
                    0
                )
            }
        }

        binding.btnLearn.setOnClickListener {

        }

        binding.btnMoreInfo.setOnFocusChangeListener { v, hasFocus ->

            if (hasFocus) {
                trackEventHomeMoreInfoBtnFocus(bannerData)
                btnMoreInfoListener()
                whichBtnFocused = BTN_MORE_INFO
                btnMoreInfo.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_add_black_24dp,
                    0,
                    0,
                    0
                )
                Log.e(TAG, " btnMoreInfo  ${getBtnLastFocused()}")
            } else {
                btnMoreInfo.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_add_white_24dp,
                    0,
                    0,
                    0
                )


            }
        }

        binding.btnGrade.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                btnGradeListeners()
                whichBtnFocused = BTN_GRADE
                //update focused view
                binding.btnGrade.setBackgroundResource(R.drawable.ic_banner_grade_selected)
            } else {
                //update non focused view
                binding.btnGrade.setBackgroundResource(R.drawable.ic_banner_grade)
            }
        }
        itemViewClickListener()
        return binding.root

    }

    private fun setViewAccToType() {

        if (type == "learn") {
            binding.constraint3.visibility = View.GONE
            binding.btnPractice.visibility = View.GONE
            binding.btnLearn.visibility = View.VISIBLE
            binding.btnMoreInfo.visibility = View.VISIBLE
            binding.btnGrade.nextFocusLeftId = R.id.btnMoreInfo

        } else if (type == "practice") {
            binding.constraint3.visibility = View.VISIBLE
            binding.btnPractice.visibility = View.VISIBLE
            binding.btnLearn.visibility = View.GONE
            binding.btnMoreInfo.visibility = View.GONE
            binding.btnLearn.clearFocus()
            binding.btnPractice.requestFocus()
            binding.btnGrade.nextFocusLeftId = R.id.btnPractice

        } else if (type == "test") {
            binding.constraint3.visibility = View.VISIBLE
            binding.btnPractice.visibility = View.VISIBLE
            binding.btnPractice.text = "Start Test"
            binding.btnLearn.visibility = View.GONE
            binding.btnMoreInfo.visibility = View.GONE
            binding.btnLearn.clearFocus()
            binding.btnPractice.requestFocus()
            binding.btnGrade.nextFocusLeftId = R.id.btnPractice
        }
    }

    private fun btnGradeListeners() {
        binding.btnGrade.setOnClickListener {
//            Toast.makeText(activity,"Switch Grade",Toast.LENGTH_SHORT).show()
            // startActivity(Intent(context, AddGoalsActivity::class.java))
        }

        binding.btnGrade.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        DPadKeysListener.isKeyPadDown(true, "Banner")
                    }
                }
            }
            false
        }
    }

    fun getBtnLastFocused(): String = whichBtnFocused

    private fun btnMoreInfoListener() {
        binding.btnMoreInfo.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        DPadKeysListener.isKeyPadDown(true, "Banner")
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        trackEventHomeMoreInfoBtnClick(
                            bannerData,
                            com.embibejio.coreapp.constant.AppConstants.SLIDE_TYPE_COOBO
                        )
                    }
                }
            }
            false
        }
    }

    private fun btnLearnKeyListener() {
        binding.btnLearn.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {

                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        DPadKeysListener.isKeyPadDown(true, "Banner")
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navigationMenuCallback.navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER -> {
                        trackEventHomeMainBtnClick(
                            bannerData,
                            com.embibejio.coreapp.constant.AppConstants.SLIDE_TYPE_COOBO
                        )
                    }
                }
            }
            false
        }

    }

    private fun itemViewClickListener() {

    }

    fun setBannerButtonListener(callback: DPadKeysListener) {
        this.DPadKeysListener = callback
    }

    fun setLastFocusedBtn() {
        binding.btnLearn.requestFocus()
    }

    fun setData(data: BannerData, position: Int) {

        trackEventHomeBannerChange(
            data,
            position,
            com.embibejio.coreapp.constant.AppConstants.SLIDE_TYPE_COOBO
        )
        if (data.title_image_url == "") {
            binding.title.text = data.title
            showView(binding.title)
            binding.titleImage.visibility = View.GONE
            binding.btnMoreInfo.visibility = View.VISIBLE
            binding.textTopic.text = data.subject
            binding.textTime.text = data.duration
            binding.embiumsCount.text = data.embiumCoins
            binding.textTopic.visibility = View.VISIBLE
            binding.textTime.visibility = View.VISIBLE
            binding.embiumsCount.visibility = View.VISIBLE
//            showView(binding.constraint1)
//            showView(binding.constraint2)
//            showView(binding.constraint3)

            binding.rateBar.visibility = View.VISIBLE
        } else {
            binding.title.visibility = View.INVISIBLE
            setImage(data.title_image_url)
            showView(binding.titleImage)
            binding.btnMoreInfo.visibility = View.GONE
            binding.textTopic.visibility = View.INVISIBLE
            binding.textTime.visibility = View.INVISIBLE
            binding.embiumsCount.visibility = View.INVISIBLE
            binding.rateBar.visibility = View.INVISIBLE
//            binding.constraint1.visibility = View.INVISIBLE
//            binding.constraint2.visibility = View.INVISIBLE
//            binding.constraint3.visibility = View.INVISIBLE

            GlobalScope.launch(Dispatchers.Main) {
                delay(2)
                binding.btnLearn.requestFocus()
            }
        }
        binding.textDescription.text = data.description
        showView(binding.textDescription)

        if (data.social) {
//           binding.btnMoreInfo.visibility = View.VISIBLE
//           binding.textTopic.text = data.subject
//            binding.textTime.text = data.duration
//            binding.embiumsCount.text = data.embiumCoins
//            binding.textTopic.visibility = View.VISIBLE
//            binding.textTime.visibility = View.VISIBLE
//            binding.embiumsCount.visibility = View.VISIBLE
//            showView(binding.constraint1)
//            showView(binding.constraint2)
//            showView(binding.constraint3)
//            binding.rateBar.visibility = View.VISIBLE
        } else {
//            binding.btnMoreInfo.visibility = View.GONE
//           binding.textTopic.visibility = View.INVISIBLE
//            binding.textTime.visibility = View.INVISIBLE
//            binding.embiumsCount.visibility = View.INVISIBLE
//            binding.rateBar.visibility = View.INVISIBLE
//           hideView(binding.constraint1)
//           hideView(binding.constraint2)
//           hideView(binding.constraint3)
//           GlobalScope.launch(Dispatchers.Main) {
//               delay(2)
//               binding.btnLearn.requestFocus()
//           }
        }

        binding.btnLearn.text = data.buttonText

    }

    fun hideDetails() {
        binding.headerCard.visibility = View.GONE
    }

    fun showDetails() {
        binding.headerCard.visibility = View.VISIBLE
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }


    private fun hideView(view: View) {
        val animation: Animation =
            AnimationUtils.loadAnimation(requireContext(), R.anim.anim_fade_out)
        animation.duration =
            AnimSettings.FADE_OUT_DURATION
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) {
            }

            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                view.visibility = View.INVISIBLE
            }
        })
        view.startAnimation(animation)
    }

    private fun showView(view: View) {
        val animation: Animation =
            AnimationUtils.loadAnimation(requireContext(), R.anim.anim_fade_in)
        animation.duration =
            AnimSettings.FADE_IN_DURATION
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) {}
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationEnd(animation: Animation?) {
                view.visibility = View.VISIBLE
            }
        })
        view.startAnimation(animation)
    }

    fun hideAll(bool: Boolean) {
        if (bool)
            binding.headerCard.visibility = View.INVISIBLE
        else
            binding.headerCard.visibility = View.VISIBLE
    }

    object AnimSettings {
        const val FADE_IN_DURATION = 800L
        const val FADE_OUT_DURATION = 10L
    }

    fun setImage(imgUrl: String) {
        Glide.with(requireContext())
            .load(Uri.parse(imgUrl))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(binding.titleImage)
    }


}