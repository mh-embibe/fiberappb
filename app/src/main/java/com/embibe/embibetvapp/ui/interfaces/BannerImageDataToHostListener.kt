package com.embibe.embibetvapp.ui.interfaces

import com.embibe.embibetvapp.model.BannerData

interface BannerImageDataToHostListener {

    fun setBannerImageData(data: BannerData, position: Int)
}