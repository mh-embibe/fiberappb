package com.embibe.embibetvapp.ui.fragment.practice

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.constant.AppConstants.FORMAT_ID
import com.embibe.embibetvapp.constant.AppConstants.LM_NAME
import com.embibe.embibetvapp.constant.AppConstants.PRACTICE_MODE
import com.embibe.embibetvapp.constant.AppConstants.PracticeModes.Normal
import com.embibe.embibetvapp.constant.AppConstants.TOPIC_LEARN_PATH
import com.embibe.embibetvapp.databinding.CardBannerPracticeBinding
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.ui.activity.PracticeActivity
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.utils.Utils
import com.embibejio.coreapp.constant.AppConstants

class PracticeBannerFragment : BaseAppFragment() {

    private lateinit var binding: CardBannerPracticeBinding
    private lateinit var dPadKeysCallback: DPadKeysListener
    private lateinit var navigationMenuCallback: NavigationMenuCallback

    val classTag = PracticeBannerFragment::class.java.toString()

    private var whichBtnFocused = ""
    private var BTN_PRACTICE = "0"
    private var BTN_MORE_INFO = "1"
    private var BTN_GRADE = "2"
    private var canGainFocus = true
    private var currentId = ""
    private var currentTitle = ""
    private var currentSubject = ""
    lateinit var b_Data: BannerData

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.card_banner_practice, container,
            false
        )
        setViewAccToType()
        whichBtnFocused = BTN_PRACTICE

        initialBannerViewSet()

        binding.btnPractice.postDelayed({
            binding.btnPractice.requestFocus()
        }, 100)

        btnPracticeListeners()
        btnGradeListeners()
        btnMoreInfoListeners()

        return binding.root

    }

    private fun initialBannerViewSet() {
        /*binding.title.text = "Polynomials"
        binding.embiumsCount.text = "Earn 100"
        binding.textTime.text = "32 Concepts"
        binding.textTopic.text = "Mathematics"
        binding.textDescription.text = getString(R.string.polynomial_topic_desc)*/
        binding.constraint1.visibility = View.GONE
        binding.constraint2.visibility = View.GONE
        binding.constraint3.visibility = View.GONE
    }

    private fun btnMoreInfoListeners() {
        binding.btnMoreInfo.setOnFocusChangeListener { v, hasFocus ->

            if (hasFocus) {
                trackEventHomeMoreInfoBtnFocus(bannerData)
                if (canGainFocus) {
                    whichBtnFocused = BTN_MORE_INFO
                    binding.btnMoreInfo.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_add_black_24dp,
                        0,
                        0,
                        0
                    )
                } else {
                    canGainFocus = true
                }
            } else {
                binding.btnMoreInfo.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_add_white_24dp,
                    0,
                    0,
                    0
                )


            }
        }

        binding.btnMoreInfo.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!Utils.isKeyPressedTooFast(500))
                            dPadKeysCallback.isKeyPadDown(true, "Banner")
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        binding.btnGrade.postDelayed({
                            binding.btnGrade.requestFocus()
                        }, 0)
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        binding.btnPractice.postDelayed({
                            binding.btnPractice.requestFocus()
                        }, 0)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        trackEventHomeMoreInfoBtnClick(bannerData, AppConstants.SLIDE_TYPE_PRACTICE)
                    }
                }
            }
            false
        }
    }

    private fun setViewAccToType() {
        binding.constraint3.visibility = View.VISIBLE
        binding.btnPractice.visibility = View.VISIBLE
        binding.btnMoreInfo.visibility = View.GONE
        binding.btnPractice.requestFocus()
        binding.btnGrade.nextFocusLeftId = R.id.btnPractice

    }

    private fun btnPracticeListeners() {

        binding.btnPractice.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                trackEventHomeMainBtnFocus(bannerData, AppConstants.SLIDE_TYPE_PRACTICE)
                canGainFocus = true
                whichBtnFocused = BTN_PRACTICE
            }
        }

        binding.btnPractice.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!Utils.isKeyPressedTooFast(500))
                            dPadKeysCallback.isKeyPadDown(true, "Banner")
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navigationMenuCallback.navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (binding.btnMoreInfo.visibility == View.VISIBLE) {
                            binding.btnMoreInfo.postDelayed({
                                binding.btnMoreInfo.requestFocus()
                            }, 0)
                        } else if (binding.btnGrade.visibility == View.VISIBLE) {
                            binding.btnGrade.postDelayed({
                                binding.btnGrade.requestFocus()
                            }, 0)
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        trackEventHomeMainBtnClick(bannerData, AppConstants.SLIDE_TYPE_PRACTICE)
                        try {
                            var intent = Intent(App.context, PracticeActivity::class.java)
                            intent.putExtra("subject", currentSubject)
                            val parts = currentId.split("--")
                            val lmCode = parts[parts.size - 1]
                            val lmLevel = Utils.getLevelUsingCode(lmCode)
                            val examCode = Utils.getExamLevelCode(parts)
                            intent.putExtra(PRACTICE_MODE, Normal)
                            intent.putExtra("title", currentTitle)
                            intent.putExtra("lm_level", lmLevel)
                            intent.putExtra("lm_code", lmCode)
                            intent.putExtra("exam_code", examCode)
                            intent.putExtra(LM_NAME, b_Data.learnmap_id)
                            intent.putExtra(FORMAT_ID, b_Data.learning_map.format_id)
                            intent.putExtra(
                                TOPIC_LEARN_PATH,
                                b_Data.learning_map.topicLearnPathName
                            )
                            intent.putExtra(
                                com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_NAME,
                                b_Data.learnpath_name
                            )
                            intent.putExtra(
                                com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_FORMAT_NAME,
                                b_Data.learnpath_format_name
                            )
                            startActivity(intent)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }
            false
        }
    }

    private fun btnGradeListeners() {

        binding.btnGrade.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                if (canGainFocus) {
                    whichBtnFocused = BTN_GRADE
                    //update focused view
                    binding.btnGrade.setBackgroundResource(R.drawable.ic_banner_grade_selected)
                } else {
                    canGainFocus = true
                }
            } else {
                //update non focused view
                binding.btnGrade.setBackgroundResource(R.drawable.ic_banner_grade)
            }
        }

        binding.btnGrade.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!Utils.isKeyPressedTooFast(500))
                            dPadKeysCallback.isKeyPadDown(true, "Banner")
                    }
                    KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                        // startActivity(Intent(context, AddGoalsActivity::class.java))
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (binding.btnMoreInfo.visibility == View.VISIBLE) {
                            binding.btnMoreInfo.postDelayed({
                                binding.btnMoreInfo.requestFocus()
                            }, 0)
                        } else if (binding.btnPractice.visibility == View.VISIBLE) {
                            binding.btnPractice.postDelayed({
                                binding.btnPractice.requestFocus()
                            }, 0)
                        }
                    }
                }
            }
            false
        }
    }

    fun setLastFocusedBtn() {
        canGainFocus = false
        binding.btnPractice.postDelayed({
            binding.btnPractice.requestFocus()
        }, 0)
    }

    fun setData(data: BannerData, position: Int) {
        currentSubject = data.subject
        currentId = data.id
        currentTitle = data.title
        b_Data = data
        trackEventHomeBannerChange(data, position, AppConstants.SLIDE_TYPE_PRACTICE)
        if (data.title_image_url == "") {
            binding.title.text = data.title
            binding.textTime.text = data.duration
            binding.textDescription.text = data.description
            binding.textTopic.text = data.subject
            binding.embiumsCount.text = getString(R.string.earn) + " ${data.embiumCoins}"
            binding.textTopic.visibility = View.VISIBLE
            binding.btnMoreInfo.visibility = View.GONE
            binding.textTime.visibility = View.INVISIBLE
            binding.textTime.visibility = View.VISIBLE
            binding.embiumsCount.visibility = View.VISIBLE
        } else {
            binding.textTopic.visibility = View.INVISIBLE
            binding.textTime.visibility = View.INVISIBLE
            binding.embiumsCount.visibility = View.INVISIBLE
            binding.btnMoreInfo.visibility = View.INVISIBLE
        }
        Utils.changeBannerTitleTextSize(binding.title)
    }

    fun hideDetails() {
        binding.headerCard.visibility = View.GONE
    }

    fun showDetails() {
        binding.headerCard.visibility = View.VISIBLE
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }

}
