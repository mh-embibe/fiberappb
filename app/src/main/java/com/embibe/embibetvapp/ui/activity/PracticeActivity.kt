package com.embibe.embibetvapp.ui.activity

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.CONCEPT_ID
import com.embibe.embibetvapp.constant.AppConstants.CONTENT_TYPE
import com.embibe.embibetvapp.constant.AppConstants.CONTINUE_PRACTICE
import com.embibe.embibetvapp.constant.AppConstants.END_SESSION
import com.embibe.embibetvapp.constant.AppConstants.FORMAT_ID
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_FORMAT_NAME
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_NAME
import com.embibe.embibetvapp.constant.AppConstants.LENGTH
import com.embibe.embibetvapp.constant.AppConstants.NO_MORE_QUESTIONS_DIALOG
import com.embibe.embibetvapp.constant.AppConstants.PRACTICE_SESSION_ALERT_DIALOG
import com.embibe.embibetvapp.constant.AppConstants.PracticeModes
import com.embibe.embibetvapp.constant.AppConstants.PracticeModes.CFUPractice
import com.embibe.embibetvapp.constant.AppConstants.SOURCE_REFERENCE
import com.embibe.embibetvapp.constant.AppConstants.TOPIC_LEARN_PATH
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_AUTHORS
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CONCEPT_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_DESCRIPTION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SOURCE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SUBJECT
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TITLE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TOTAL_DURATION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_URL
import com.embibe.embibetvapp.databinding.ActivityPracticeBinding
import com.embibe.embibetvapp.model.achieve.achieveFeedback.AchieveFeedbackRes
import com.embibe.embibetvapp.model.achieve.achieveFeedback.Data
import com.embibe.embibetvapp.ui.fragment.errorAlertScreen.NextPAJAlertDialog
import com.embibe.embibetvapp.ui.interfaces.CommonAndroidAPI
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener
import com.embibe.embibetvapp.ui.interfaces.VoiceListener
import com.embibe.embibetvapp.ui.viewmodel.AchieveViewModel
import com.embibe.embibetvapp.ui.viewmodel.PracticeViewModel
import com.embibe.embibetvapp.unity.UnityPlayerActivity
import com.embibe.embibetvapp.utils.ConnectionManager
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.VideoUtils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_practice.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class PracticeActivity : BaseFragmentActivity(), CommonAndroidAPI.AndroidAPIOnCallListener,
    ErrorScreensBtnClickListener, VoiceListener {

    private lateinit var binding: ActivityPracticeBinding
    private lateinit var practiceViewModel: PracticeViewModel
    private var pref = PreferenceHelper()
    private var contentId: String? = ""
    private var topicLearnpath: String = ""
    private var conceptId: String = ""
    private var length: String = ""
    private var practiceId = ""
    private var lmLevel = ""
    private var lmCode = ""
    private var lmName = ""
    private var examCode = ""
    private var language = "en"
    private var mLastKeyDownTime: Long = 0

    var query = ""
    var mode: PracticeModes = CFUPractice
    var token = ""
    var title = ""
    var subject = ""
    var formatId = ""
    var learnPathName = ""
    var learnPathFormatName = ""
    var childId = 0L
    var is_Achieve = false

    companion object {
        const val TEMPLATE_PRACTICE = "practice/index.html"
        val TAG = PracticeActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        voiceListener = this
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_practice)
        practiceViewModel = ViewModelProviders.of(this).get(PracticeViewModel::class.java)
        //Get the Language
        language = Utils.getCurrentLocale()
        showProgress()
        permissionsCheck()
        if (intent.hasExtra(TOPIC_LEARN_PATH)) {
            topicLearnpath = intent.getStringExtra(TOPIC_LEARN_PATH)
        }
        if (intent.hasExtra(CONCEPT_ID)) {
            conceptId = intent.getStringExtra(CONCEPT_ID)
        }
        if (intent.hasExtra("is_achieve")) {
            is_Achieve = intent.getBooleanExtra("is_achieve", false)
        }
        if (intent.hasExtra(LENGTH)) {
            length = intent.getIntExtra(LENGTH, 0).toString()
        }

        doAsync {
            token = pref[AppConstants.EMBIBETOKEN, ""]
            query = "token=$token"
            if (intent.hasExtra(AppConstants.ID)) {
                practiceId = intent.getStringExtra(AppConstants.ID)
                query += "&practice_id=$practiceId"
            }

            if (intent.hasExtra(AppConstants.TITLE)) {
                title = intent.getStringExtra(AppConstants.TITLE)
                query += "&title=$title"
            }

            if (intent.hasExtra(AppConstants.PRACTICE_MODE)) {
                mode = intent.getSerializableExtra(AppConstants.PRACTICE_MODE) as PracticeModes
                query += "&mode=${mode.typeName}"
            }
            if (intent.hasExtra("subject"))
                subject = intent.getStringExtra("subject")
            setBackGround()
            if (intent.hasExtra(AppConstants.FORMAT_ID)) {
                formatId = intent.getStringExtra(AppConstants.FORMAT_ID)
                query += "&format_id=$formatId"
            }
            if (intent.hasExtra(AppConstants.LEARN_PATH_NAME)) {
                if (intent.getStringExtra(AppConstants.LEARN_PATH_NAME) != null)
                    learnPathName = intent.getStringExtra(AppConstants.LEARN_PATH_NAME)
            }
            if (intent.hasExtra(AppConstants.LEARN_PATH_FORMAT_NAME)) {
                if (intent.getStringExtra(AppConstants.LEARN_PATH_FORMAT_NAME) != null)
                    learnPathFormatName = intent.getStringExtra(AppConstants.LEARN_PATH_FORMAT_NAME)
            }

            if (intent.hasExtra(AppConstants.LM_NAME)) {
                if (intent.getStringExtra(AppConstants.LM_NAME) != null)
                    lmName = intent.getStringExtra(AppConstants.LM_NAME).replace("/", "--")
                query += "&lm_name=$lmName"
            }

            if (intent.hasExtra("lm_level") && intent.hasExtra("lm_code") && intent.hasExtra("exam_code")) {
                lmLevel = intent.getStringExtra("lm_level")
                lmCode = intent.getStringExtra("lm_code")
                examCode = intent.getStringExtra("exam_code")
                query += "&lm_level=$lmLevel&lm_code=$lmCode&exam_code=$examCode"
            }

            if (intent.hasExtra(AppConstants.PAJ_ID)) {
                var paj_id = intent.getStringExtra(AppConstants.PAJ_ID)
                query += "&paj_id=${paj_id}"
            }

            if (intent.hasExtra(AppConstants.PAJ_STEP_INDEX)) {
                var paj_step_index = intent.getIntExtra(AppConstants.PAJ_STEP_INDEX, 0)
                query += "&paj_step_index=$paj_step_index"
            }

            if (intent.hasExtra(AppConstants.PAJ_STEP_PATH_INDEX)) {
                var paj_step_path_index = intent.getIntExtra(AppConstants.PAJ_STEP_PATH_INDEX, 0)
                query += "&paj_step_path_index=$paj_step_path_index"
            }

            if (intent.hasExtra(AppConstants.PAJ_STEP_PATH)) {
                var paj_step_path = intent.getStringExtra(AppConstants.PAJ_STEP_PATH)
                query += "&paj_step_path=$paj_step_path"
            }


            if (intent.hasExtra(AppConstants.CALL_ID)) {
                var call_id = intent.getIntExtra(AppConstants.CALL_ID, 0)
                query += "&call_id=$call_id"
            }


            if (intent.hasExtra(AppConstants.PROGRESS_PERCENT)) {
                var progress_percent = intent.getIntExtra(AppConstants.PROGRESS_PERCENT, 0)
                query += "&progress_percent=$progress_percent"
            }


            if (intent.hasExtra(AppConstants.QUESTION_CODE)) {
                var question_code = intent.getStringExtra(AppConstants.QUESTION_CODE)
                query += "&questionCode=$question_code"
            }
            query += "&language=$language"

            uiThread {
                loadWebView()
            }
        }

    }

    private fun loadWebView() {
        if (ConnectionManager.instance.hasNetworkAvailable()) {
            setPracticeWebView()
        } else {
            hideProgress()
            Utils.showError(this@PracticeActivity, ApiConstants.API_CODE_NO_NETWORK,
                object : BaseViewModel.ErrorCallBacks {
                    override fun onRetry(msg: String) {
                        loadWebView()
                    }

                    override fun onDismiss() {
                        close()
                    }
                }, object : BaseViewModel.BacKPressListener {
                    override fun onCustomBackPressed() {
                        /* back press event */
                    }

                })
        }
    }

    private fun setBackGround() {
        try {
            Glide.with(this)
                .load(Utils.getSubjectBackground(subject))
                .placeholder(R.drawable.banner_placeholder)
                .error(R.drawable.banner_placeholder)
                .into(imageView)
        } catch (e: Exception) {
            if (e is OutOfMemoryError) {
                Log.d("error", "OutOfMemoryError")
            }
        }

    }


    fun getPixelValue(context: Context, dimenId: Int): Int {
        val resources = context.resources
        val dp = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dimenId.toFloat(),
            resources.displayMetrics
        )
        return dp.toInt()
    }

    private fun setPracticeWebView() {
        //setCustomTopMargin(-300,300)

        webView.webChromeClient = WebChromeClient()
        webView.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                Log.d("WEBVIEW", "Web view ready")
            }
        }
        webView.settings.allowFileAccess = true
        webView.isLongClickable = false
        webView.settings.javaScriptEnabled = true
        webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        webView.settings.domStorageEnabled
        webView.setBackgroundColor(Color.argb(0, 0, 0, 0))
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
        webView.addJavascriptInterface(CommonAndroidAPI(this), CommonAndroidAPI.NAME)
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        val template = "file:///android_asset/$TEMPLATE_PRACTICE?$query"
        //disableLongPress(webView)
        Log.i("practice url", template)
        webView.loadUrl(template)
    }

    private fun setCustomTopMargin(topMargin: Int, bottomMargin: Int) {
        val layoutParams = webView.layoutParams as ConstraintLayout.LayoutParams
        layoutParams.setMargins(0, getPixelValue(this, topMargin), 0, bottomMargin)
        webView.layoutParams = layoutParams
    }

    fun disableLongPress(mWebView: WebView?) {
        if (mWebView != null) {
            mWebView.setOnLongClickListener(View.OnLongClickListener { v: View? -> true })
            mWebView.isLongClickable = false
            mWebView.isHapticFeedbackEnabled = false
        }
    }

    override val question: String?
        get() = ""

    override fun getQuestion(questionCode: String?): String? {
        return ""
    }

    override val concept: String?
        get() = ""
    override val hint: String?
        get() = ""

    override fun setAnswer(): String? {
        return ""
    }

    override fun getSections(): String? {
        return ""
    }

    override fun getAnswer(): String? {
        return ""
    }

    override fun getQuestionSet(sectionId: String?): String? {
        return ""
    }

    override fun getData(): String? {
        return ""
    }

    override fun getData(data: String) {
        try {
            val obj = JSONObject(data)
            val type = obj.getString(AppConstants.DETAILS_TYPE)
            when (type.toLowerCase()) {
                AppConstants.PRACTICE -> {
                    val data = obj.getJSONObject(AppConstants.DATA)
                    contentId = data.getString(AppConstants.PRACTICE_ID)
                    var status = data.getString(AppConstants.STATUS)
                    if (status.toUpperCase() == AppConstants.CONTENT_STATUS_COMPLETED || status.toUpperCase() == AppConstants.CONTENT_STATUS_FINISHED) {
                        status = AppConstants.CONTENT_STATUS_COMPLETED
                    }
                    practiceViewModel.updatedContentStatus(
                        practiceId,
                        AppConstants.STATUS_CONTENT_TYPE_PRACTISE_CHAPTERS,
                        0,
                        status.toUpperCase(),
                        topicLearnpath,
                        learnPathName,
                        learnPathFormatName,
                        formatId,
                        conceptId,
                        length
                    )
                    if (status.toUpperCase() == AppConstants.CONTENT_STATUS_COMPLETED) {
                        closeLoader()
                        noMoreQuestionUI()
                    }
                }

                AppConstants.VIDEO -> {
                    doAsync {
                        val content = obj.getJSONObject(AppConstants.DATA)
                        val url = content.getString("url")
                        val type = Utils.getVideoTypeUsingUrl(url)
                        val title = content.getString("title")
                        val id = content.getString("id")
                        var conceptId = ""
                        var subject = ""
                        var chapter = ""
                        var totalDuration = ""
                        var topicLearnPath = ""
                        var formatId = ""
                        var learnPathName = ""
                        var learnPathFormatName = ""
                        var token_key = ""
                        if (content.has("learning_map")) {
                            val learningMap = content.getJSONObject("learning_map")
                            if (learningMap.has("concept_id")) {
                                conceptId = learningMap.getString("concept_id")
                            }
                            if (learningMap.has("chapter")) {
                                chapter = learningMap.getString("chapter")
                            }
                            if (content.has("subject")) {
                                subject = content.getString("subject")
                            }
                            if (content.has("topic_learnpath_name")) {
                                topicLearnPath = content.getString("topic_learnpath_name")
                            }
                            if (content.has("format_id")) {
                                formatId = content.getString("format_id")
                            }

                        }
                        if (content.has("owner_info")) {
                            if (content.getJSONObject("owner_info").has("key"))
                                token_key = content.getJSONObject("owner_info").getString("key")
                        }
                        if (content.has("length"))
                            totalDuration = content.getString("length")
                        if (content.has("learn_path_name"))
                            learnPathName = content.getString("learn_path_name")
                        if (content.has("learn_path_format_name"))
                            learnPathFormatName = content.getString("learn_path_format_name")
                        uiThread {
                            hideProgress()
                            callVideo(
                                id,
                                AppConstants.VIDEO,
                                conceptId,
                                url,
                                title,
                                "",
                                type.toLowerCase(),
                                subject,
                                chapter,
                                "",
                                totalDuration,
                                topicLearnPath,
                                learnPathName,
                                learnPathFormatName, formatId, token_key
                            )
                        }
                    }

                }

                AppConstants.COOBO -> {

                    var content = obj.getJSONObject(AppConstants.DATA)
                    var url = content.getString("url")
                    var id = content.getString("id")
                    var duration = content.getInt("length")
                    var watchedDuration = content.getInt("watched_duration")
                    var learningMap = content.getJSONObject("learning_map").getString("lpcode")

                    var formatId = ""
                    var learnPathName = ""
                    var learnPathFormatName = ""
                    if (content.has("learn_path_name"))
                        learnPathName = content.getString("learn_path_name")
                    if (content.has("learn_path_format_name"))
                        learnPathFormatName = content.getString("learn_path_format_name")
                    if (content.has("learn_path_format_name"))
                        learnPathFormatName = content.getString("learn_path_format_name")

                    var format_id = content.getJSONObject("learning_map").getString("format_id")
                    callCoobo(
                        url,
                        id,
                        duration,
                        watchedDuration,
                        learningMap,
                        learnPathName,
                        learnPathFormatName,
                        formatId
                    )
                }

                AppConstants.ERROR -> {
                    Log.e(TAG, obj.toString())
                }
            }
        } catch (ex: JSONException) {
            Log.e(TAG, ex.toString())
        }
    }

    fun callCoobo(
        url: String, id: String, length: Int, watchedDuration: Int, learningMap: String,
        learnPathName: String, learnPathFormatName: String, formatId: String
    ) {
        val i = Intent(this, UnityPlayerActivity::class.java)
        i.putExtra(AppConstants.DETAILS_ID, id)
        i.putExtra(AppConstants.DURATION_LENGTH, length)
        i.putExtra(AppConstants.SLIDE_COUNT, watchedDuration)
        i.putExtra(AppConstants.COOBO_URL, url)
        i.putExtra(AppConstants.LEARNMAP_CODE, learningMap)
        i.putExtra(AppConstants.FORMAT_ID, formatId)
        i.putExtra(AppConstants.LEARN_PATH_NAME, learnPathName)
        i.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, learnPathFormatName)
        startActivity(i)
    }

    fun startVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        videoTotalDuration: String,
        topicLearnpath: String,
        learnPathName: String,
        learnPathFormatName: String,
        formatId: String
    ) {
        val intent = Intent(App.context, VideoPlayerActivity::class.java)

        intent.putExtra(VIDEO_ID, id)
        intent.putExtra(CONTENT_TYPE, type)
        intent.putExtra(VIDEO_CONCEPT_ID, conceptId)
        intent.putExtra(VIDEO_URL, url)
        intent.putExtra(VIDEO_TITLE, title)
        intent.putExtra(VIDEO_DESCRIPTION, description)
        intent.putExtra(VIDEO_SUBJECT, subject)
        intent.putExtra(VIDEO_CHAPTER, chapter)
        intent.putExtra(VIDEO_AUTHORS, authors)
        intent.putExtra(SOURCE_REFERENCE, AppConstants.PRACTICE)
        intent.putExtra(VIDEO_SOURCE, source)
        intent.putExtra(TOPIC_LEARN_PATH, topicLearnpath)
        intent.putExtra(VIDEO_TOTAL_DURATION, videoTotalDuration)
        intent.putExtra(FORMAT_ID, formatId)
        intent.putExtra(LEARN_PATH_NAME, learnPathName)
        intent.putExtra(LEARN_PATH_FORMAT_NAME, learnPathFormatName)
        startActivity(intent)
    }

    private fun callVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        videoTotalDuration: String,
        topicLearnpath: String,
        learnPathName: String,
        learnPathFormatName: String,
        formatId: String, token_key: String
    ) {
        if (source == AppConstants.PLAYER_TYPE_YOUTUBE) {
            if (Utils.isYouTubeAppAvailable(this)) {
                Utils.startYoutubeApp(this, url)
            } else {
                startVideo(
                    id,
                    type,
                    conceptId,
                    url,
                    title,
                    description,
                    source,
                    subject,
                    chapter,
                    authors,
                    videoTotalDuration,
                    topicLearnpath,
                    learnPathName,
                    learnPathFormatName, formatId
                )
            }

        } else {
            val callback = object : BaseViewModel.APICallBackVolley {
                override fun <T> onSuccessCallBack(response: T) {
                    hideProgress()
                    response as JSONObject
                    val playableUrl = Utils.getVimeoHD((response["files"] as JSONArray))
                    startVideo(
                        id,
                        type,
                        conceptId,
                        playableUrl,
                        title,
                        description,
                        AppConstants.PLAYER_TYPE_EXOPLAYER,
                        subject,
                        chapter,
                        authors,
                        videoTotalDuration,
                        topicLearnpath,
                        learnPathName,
                        learnPathFormatName, formatId
                    )
                    /*hide ProgressBar here */

                }

                override fun onErrorCallBack(e: VolleyError) {
                    try {
                        makeLog("Error:  $e")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    /*hide ProgressBar here */
                    hideProgress()
                }
            }
            showProgress()
            val videoId = VideoUtils.getVimeoVideoId(url).toString()
            Utils.getVideoURL(this, videoId, token_key, callback)
        }

    }

    override fun getEvent(data: String) {
        try {
            val obj = JSONObject(data)
            val type = obj.getString(AppConstants.DETAILS_TYPE)
            when (type.toLowerCase()) {
                AppConstants.EVENT -> {
                    SegmentUtils.trackPracticeEvent(obj.getJSONObject(AppConstants.DATA))
                }
            }

        } catch (ex: JSONException) {
            Log.e(TAG, ex.toString())
        }
    }

    override fun sendToFeedback(testCode: String) {
        TODO("Not yet implemented")
    }

    override fun showLoader() {

    }

    override fun closeLoader() {
        if (DataManager.isLoading)
            hideProgress()
    }

    override fun selectAnswer(value: String?) {

    }

    override fun deselectAnswer(value: String?) {

    }

    override fun inputAnswer(value: String?) {

    }

    override fun close() {
        webView.destroy()
        finish()
    }

    override val questionNumber: Int
        get() = 0
    override val sectionName: String?
        get() = ""
    override val chapterJson: String?
        get() = ""
    override val chapterName: String?
        get() = ""
    override val singQuestionJson: String?
        get() = TODO("Not yet implemented")
    override val questionListJson: String?
        get() = TODO("Not yet implemented")

    override fun showConcept(): Boolean? {
        return true
    }

    override fun hideKeyboard() {
        closeKeyBoard(binding.root)
    }

    override fun focusUp() {
        TODO("Not yet implemented")
    }

    override fun focusDown() {
        TODO("Not yet implemented")
    }

    override fun focusRight() {
        TODO("Not yet implemented")
    }

    override fun focusLeft() {
        TODO("Not yet implemented")
    }

    override fun getSelectedQuestion(qListJson: String) {
        TODO("Not yet implemented")
    }

    override fun setQuestion(singQuestionJson: String) {
        TODO("Not yet implemented")
    }

    override fun getSelectedVideo(videoData: String) {

    }

    override fun screenUpdate(type: String) {
        when (type) {
            END_SESSION -> {
                practiceViewModel.updatedContentStatus(
                    practiceId,
                    AppConstants.STATUS_CONTENT_TYPE_PRACTISE_CHAPTERS,
                    0,
                    AppConstants.CONTENT_STATUS_INPROGRESS,
                    topicLearnpath, learnPathName, learnPathFormatName, formatId,
                    conceptId,
                    length
                )
                close()
            }
            CONTINUE_PRACTICE -> {
                Handler().postDelayed({
                    if (isDisconnected) {
                        showNoInternet()
                    }

                }, 1500)

            }
        }
    }

    override fun onBackPressed() {
        binding.webView.evaluateJavascript("CommonWebviewAPI.isKeyboardVisible()") { value ->
            println(value)
            if (value == "false")
                Utils.showAlertDialog(
                    PRACTICE_SESSION_ALERT_DIALOG, supportFragmentManager,
                    this
                )
        }

    }


    private fun noMoreQuestionUI() {
        if (is_Achieve) {
            callAchieveNextSteps()
        } else
            Utils.showAlertDialog(
                NO_MORE_QUESTIONS_DIALOG, supportFragmentManager,
                this
            )
    }

    private fun callAchieveNextSteps() {
        if (DataManager.instance.movePosition() == -1) {
            finish()
        } else {
            moveToNext(data = DataManager.instance.getNextFeedbackData())
        }
    }

    private fun moveToNext(data: Data?) {
        getPAJ(data)
    }

    /*load getPAJ from api*/
    fun getPAJ(data: Data?) {
        if (DataManager.instance.getTestPAJ() != null) {
            val testPAJData =
                Gson().fromJson(DataManager.instance.getTestPAJ(), AchieveFeedbackRes::class.java)
            if (testPAJData != null) {
                val achieveViewModel = ViewModelProviders.of(this).get(AchieveViewModel::class.java)
                achieveViewModel.getAchieveFeedbackRes(
                    testPAJData.bundleCode ?: "",
                    testPAJData.pajId ?: "",
                    object :
                        BaseViewModel.APICallBacks<AchieveFeedbackRes> {

                        override fun onSuccess(model: AchieveFeedbackRes?) {
                            hideProgress()
                            if (model != null) {
                                DataManager.instance.setTestPAJ(Gson().toJson(model))
                                supportFragmentManager.let {
                                    val nextPAJDialog: DialogFragment =
                                        NextPAJAlertDialog(
                                            this@PracticeActivity,
                                            AppConstants.VIDEO,
                                            data!!
                                        )
                                    nextPAJDialog.show(supportFragmentManager, "nextPAJDialog")
                                }
                            }
                        }

                        override fun onFailed(code: Int, error: String, msg: String) {
                            hideProgress()
                            if (Utils.isApiFailed(code)) {
                                Utils.showError(
                                    this@PracticeActivity,
                                    code,
                                    object : BaseViewModel.ErrorCallBacks {
                                        override fun onRetry(msg: String) {
                                        }

                                        override fun onDismiss() {}
                                    })
                            } else {
                                //showToast(error)
                            }
                        }
                    })
            } else {
                supportFragmentManager.let {
                    val nextPAJDialog: DialogFragment =
                        NextPAJAlertDialog(this@PracticeActivity, AppConstants.VIDEO, data!!)
                    nextPAJDialog.show(supportFragmentManager, "nextPAJDialog")
                }
            }
        }

    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        //makeLog("NetworkConnectionChanged $isConnected")
        when (isConnected) {
            true -> {
                //loadWebView()
                if (isDisconnected) {
                    Utils.showToast(this, "Internet connected!!")
                }
                isDisconnected = false

            }
            false -> {
                isDisconnected = true
                showNoInternet()
            }
        }
    }

    private fun showNoInternet() {

        Utils.showError(this@PracticeActivity, ApiConstants.API_CODE_NO_NETWORK,
            object : BaseViewModel.ErrorCallBacks {
                override fun onRetry(msg: String) {
                    if (!ConnectionManager.instance.hasNetworkAvailable()) {
                        showNoInternet()
                    }
                }

                override fun onDismiss() {
                    close()
                }
            }, object : BaseViewModel.BacKPressListener {
                override fun onCustomBackPressed() {
                    /* back press event */
                }

            })
    }


    override fun initVoiceInput(value: String) {
        runOnUiThread {
            startRecognition()
        }
    }

    override fun onVoiceInit() {

    }

    override fun onVoiceSuccess(data: String) {
        updateSearchQuery(data)
    }

    override fun onVoiceFailed(msg: String) {
        updateSearchQuery("")
    }

    override fun onVoiceCancel(msg: String) {
        updateSearchQuery("")
    }

    private fun updateSearchQuery(searchQuery: String) {
        webView.evaluateJavascript(
            "CommonWebviewAPI.onFinishVoiceInput('" + searchQuery + "')",
            null
        )
    }

    override fun movePAJNextActivity(data: String) {
        callAchieveNextSteps()
    }
//    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
//
//        if (event.keyCode == KeyEvent.KEYCODE_SEARCH) {
//            initVoiceInput("")
//            return true
//        }
//        val current = System.currentTimeMillis()
//        var res = false
//        if (current - mLastKeyDownTime < 300) {
//            res = true
//        } else {
//            res = super.onKeyDown(keyCode, event)
//            mLastKeyDownTime = current
//        }
//        return res
//    }

}
