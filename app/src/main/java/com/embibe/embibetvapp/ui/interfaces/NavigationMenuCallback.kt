package com.embibe.embibetvapp.ui.interfaces

interface NavigationMenuCallback {
    fun navMenuToggle(toShow: Boolean)
}