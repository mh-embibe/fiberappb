package com.embibe.embibetvapp.ui.fragment.errorAlertScreen

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentNextPAJAlertBinding
import com.embibe.embibetvapp.model.achieve.achieveFeedback.Data
import com.embibe.embibetvapp.model.achieve.achieveFeedback.PracticeData
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.activity.PracticeActivity
import com.embibe.embibetvapp.ui.activity.VideoPlayerActivity
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.VideoUtils
import kotlinx.android.synthetic.main.component_paj_header.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject

class NextPAJAlertDialog(var activityContext: Context, var type: String, var content: Data) :
    DialogFragment() {
    private lateinit var binding: FragmentNextPAJAlertBinding
    private var countDownTimer: CountDownTimer? = null
    private var countDownInSeconds: Int = 11
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Utils.isDialogShowing = true
        setStyle(STYLE_NORMAL, android.R.style.ThemeOverlay)
        //isCancelable = false
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return object : Dialog(requireActivity(), theme) {
            override fun onBackPressed() {
                requireActivity().finish()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_next_p_a_j_alert,
            container, false
        )
        return binding.root
    }

    override fun onDestroy() {
        countDownTimer?.cancel()
        super.onDestroy()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.pajHeader.seekBar.progress=content.progress
        when (content.type) {
            "learn", "video" -> {
                binding.tvPlayingNext.text = "Back to Video in "
                binding.tvNextVideoTitle.text = (content.contentMeta)?.title
                val requestOptions = RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                Glide.with(requireContext()).setDefaultRequestOptions(requestOptions)
                    .load((content.contentMeta?.thumb))
                    .placeholder(R.drawable.video_placeholder)
                    .transform(RoundedCorners(8))
                    .into(binding.ivNextVideoScreen)
            }
            "question" -> {
                binding.tvPlayingNext.text = "Back to Practice in "
                binding.tvNextVideoTitle.text = (content.practiceMeta)?.title
                val requestOptions = RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                Glide.with(requireContext()).setDefaultRequestOptions(requestOptions)
                    .load("")
                    .placeholder(R.drawable.video_placeholder)
                    .transform(RoundedCorners(8))
                    .into(binding.ivNextVideoScreen)
                binding.pajHeader.seekBar.progress =
                    (content.practiceMeta)?.progress_percent!!

            }
        }

        setTimer()
    }

    private fun setTimer() {
        countDownTimer = object : CountDownTimer(10000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                countDownInSeconds = (millisUntilFinished / 1000).toInt()
                if (countDownInSeconds > 0)
                    binding.tvPlayingNextCountdown.text = "${millisUntilFinished / 1000}"
            }

            override fun onFinish() {
                //play next video
                SegmentUtils.trackEventPlayNextVideoTimerCompleted()
                playNext()
            }
        }
        countDownTimer?.start()
    }

    private fun playNext() {
        when (content.type) {
            "learn", "video" -> {
                startVideoActivity(content.contentMeta!!)
            }

            "question" -> {
                callPracticeActivity(content.practiceMeta!!)
            }
        }
    }


    private fun startVideoActivity(content: Content) {
        doAsync {
            val url = content.url
            val type = Utils.getVideoTypeUsingUrl(content.url)
            uiThread {
                callVideo(
                    url,
                    type.toLowerCase(),
                    content
                )
            }
        }
    }


    private fun callVideo(
        url: String,
        source: String,
        content: Content
    ) {
        if (source == AppConstants.PLAYER_TYPE_YOUTUBE) {

        } else {
            val callback = object : BaseViewModel.APICallBackVolley {
                override fun <T> onSuccessCallBack(response: T) {
                    response as JSONObject
                    val playableUrl = Utils.getVimeoHD((response["files"] as JSONArray))
                    startVideo(playableUrl, content)
                }

                override fun onErrorCallBack(e: VolleyError) {

                }
            }
            val videoId = VideoUtils.getVimeoVideoId(url).toString()
            Utils.getVideoURL(activityContext, videoId, content.owner_info.key, callback)
        }

    }


    fun startVideo(
        url: String, content: Content
    ) {
        val intent = Intent(activityContext, VideoPlayerActivity::class.java)
        intent.putExtra(AppConstants.VIDEO_TITLE, content.title)
        intent.putExtra(AppConstants.IS_ACHIEVE, true)
        val source = Utils.getVideoTypeUsingUrl(content.url)
        intent.putExtra(AppConstants.VIDEO_ID, content.id)
        intent.putExtra(AppConstants.CONTENT_TYPE, content.type)
        intent.putExtra(AppConstants.VIDEO_CONCEPT_ID, content.learning_map.conceptId)
        intent.putExtra(AppConstants.VIDEO_URL, url)
        intent.putExtra(AppConstants.VIDEO_DESCRIPTION, content.description)
        intent.putExtra(AppConstants.VIDEO_SUBJECT, content.subject)
        intent.putExtra(AppConstants.VIDEO_CHAPTER, content.learning_map.chapter)
        intent.putExtra(AppConstants.VIDEO_AUTHORS, content.authors)
        intent.putExtra(AppConstants.SOURCE_REFERENCE, AppConstants.PRACTICE)
        intent.putExtra(AppConstants.VIDEO_SOURCE, source)
        intent.putExtra(AppConstants.TOPIC_LEARN_PATH, content.learning_map.topicLearnPathName)
        intent.putExtra(AppConstants.VIDEO_TOTAL_DURATION, 216.toString())
        intent.putExtra(AppConstants.FORMAT_ID, content.learning_map.format_id)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, content.learnpath_name)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, content.learnpath_format_name)
        startActivity(intent)
        activity?.finish()
    }


    private fun callPracticeActivity(data: PracticeData) {
        val intent = Intent(activityContext, PracticeActivity::class.java)
        intent.putExtra(AppConstants.TITLE, data.title)
        intent.putExtra(AppConstants.PRACTICE_MODE, AppConstants.PracticeModes.PAJPractice)
        intent.putExtra(AppConstants.PAJ_ID, data.paj_id)
        intent.putExtra(AppConstants.PAJ_STEP_INDEX, data.paj_step_index)
        intent.putExtra(AppConstants.PAJ_STEP_PATH, data.paj_step_path)
        intent.putExtra(AppConstants.PAJ_STEP_PATH_INDEX, data.paj_step_path_index)
        intent.putExtra(AppConstants.CALL_ID, data.call_id)
        intent.putExtra(AppConstants.PROGRESS_PERCENT, data.progress_percent)
        intent.putExtra(AppConstants.QUESTION_CODE, data.questionCode)
        context?.startActivity(intent)
        activity?.finish()
    }
}
