package com.embibe.embibetvapp.ui.fragment.editUser

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.CURRENT_PROFILE
import com.embibe.embibetvapp.databinding.FragmentPrimaryGoalsExamBinding
import com.embibe.embibetvapp.model.UpdateProfileRequest
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.updateProfile.Errors
import com.embibe.embibetvapp.model.updateProfile.UpdateProfileRes
import com.embibe.embibetvapp.ui.activity.AddGoalsExamsActivity
import com.embibe.embibetvapp.ui.adapter.PrimaryGoalExamAdapter
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent

class PrimaryGoalsExamFragment : BaseAppFragment() {

    private lateinit var binding: FragmentPrimaryGoalsExamBinding
    private lateinit var goalExamAdapter: PrimaryGoalExamAdapter
    private val fragment = SecondaryGoalsFragment()
    private lateinit var signInViewModel: SignInViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(
                inflater, R.layout.fragment_primary_goals_exam, container, false
            )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        AddGoalsExamsActivity.primaryGoalExamCode = ""
        setExamRecyclerView(view)
        setDoneClickListener()
        setBackClickListener()
    }

    private fun setExamRecyclerView(view: View) {

        val data = DataManager.instance.getPrimaryExamGoals(AddGoalsExamsActivity.primaryGoalCode)
        goalExamAdapter = PrimaryGoalExamAdapter(view.context)

        makeGridCenter()

        goalExamAdapter.setData(data)

        binding.rvPrimaryGoalExam.adapter = goalExamAdapter

        goalExamAdapter.onItemClick = { exam ->
            if (exam.supported) {
                AddGoalsExamsActivity.primaryGoalExamCode = exam.code
            }
            if (!exam.supported) {
                binding.textGoalNotSupported.visibility = View.VISIBLE
                binding.textGoalNotSupported.text =
                    "${resources.getString(R.string.text_goal_not_supported)} ${exam.name}"
            } else {
                binding.textGoalNotSupported.visibility = View.GONE
            }
        }
        goalExamAdapter.onFocusChange = { exam, hasFocus ->

            if (!hasFocus) {
                binding.textGoalNotSupported.visibility = View.GONE
            }
        }

        binding.rvPrimaryGoalExam.requestFocus()
    }

    private fun makeGridCenter() {
        val layoutManager = FlexboxLayoutManager(context)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.CENTER
        layoutManager.alignItems = AlignItems.CENTER
        binding.rvPrimaryGoalExam.layoutManager = layoutManager

    }

    private fun setDoneClickListener() {
        binding.btnDone.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    SegmentUtils.trackProfileExamPrimaryGoalDoneFocus(
                        AddGoalsExamsActivity.primaryGoalExamCode,
                        AddGoalsExamsActivity.primaryGoal
                    )
                }
            }
        }
        binding.btnDone.setOnClickListener {
            SegmentUtils.trackProfileExamPrimaryGoalDoneCLick(
                AddGoalsExamsActivity.primaryGoalExamCode,
                AddGoalsExamsActivity.primaryGoal
            )
            if (goalExamAdapter.selectedItem != -1) {
                if (isValidExams()) {
                    updateGoalExams()
                } else {
                    showToast(resources.getString(R.string.please_select_exam))
                }
            } else {
                showToast(getString(R.string.please_select_exam))
            }
        }
    }

    private fun setBackClickListener() {
        binding.btnBack.setOnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        }
    }

    private fun isValidExams(): Boolean {
        return AddGoalsExamsActivity.primaryGoalExamCode.isNotEmpty()
    }

    fun updateGoalExams() {
        val model = UpdateProfileRequest()

        model.primaryGoal = AddGoalsExamsActivity.primaryGoalCode
        model.primaryExamCode = AddGoalsExamsActivity.primaryGoalExamCode

        makeLog(
            "results : ${AddGoalsExamsActivity.primaryGoalCode} " +
                    " ${AddGoalsExamsActivity.primaryGoalExamCode}  " +
                    " ${AddGoalsExamsActivity.secondaryGoalCode} " +
                    " ${AddGoalsExamsActivity.secondaryGoalExams.joinToString()} "
        )

        updateProfile(model)
    }


    private fun profileUpdated() {
        AppConstants.ISPROFILEUPDATED = true
        (activity as AddGoalsExamsActivity).setSecondaryGoal(fragment)
    }

    private fun failedToUpdateProfile(errors: Errors?) {
        if (errors != null) {
            val profile = errors.profile
            if (profile.equals("Nothing to change")) {
                (activity as AddGoalsExamsActivity).setSecondaryGoal(fragment)
            }
        }
    }


    fun updateProfile(model: UpdateProfileRequest) {
        showProgress()
        signInViewModel.updateProfile(model, object : BaseViewModel.APICallBacks<UpdateProfileRes> {
            override fun onSuccess(model: UpdateProfileRes?) {
                hideProgress()
                if (model != null) {
                    if (model.success) {
                        profileUpdated(model)
                    } else {
                        failedToUpdateProfile(model.errors)
                    }
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(activity, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            updateProfile(model)
                        }

                        override fun onDismiss() {

                        }
                    })

                } else {
                    Utils.showToast(activity!!, error)
                }

            }
        })
    }

    private fun profileUpdated(model: UpdateProfileRes) {
        PreferenceHelper().remove(CURRENT_PROFILE)
        UserData.updateUserData(model, AddGoalsExamsActivity.userId)
        (activity as AddGoalsExamsActivity).setSecondaryGoal(
            fragment
        )
    }


}