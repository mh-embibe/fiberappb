package com.embibe.embibetvapp.ui.fragment.achieve.home.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import androidx.leanback.widget.*
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseRowsSupportFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.model.achieve.achieveFeedback.AchieveFeedbackRes
import com.embibe.embibetvapp.model.achieve.home.AchieveHome
import com.embibe.embibetvapp.model.achieve.home.Content
import com.embibe.embibetvapp.model.home.HomeModel
import com.embibe.embibetvapp.model.test.TestSkillsRes
import com.embibe.embibetvapp.ui.activity.TestActivity
import com.embibe.embibetvapp.ui.activity.TestFeedbackActivity
import com.embibe.embibetvapp.ui.fragment.achieve.home.viewmodel.CarouselsAchieveViewModel
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.ui.interfaces.RowsToBannerListener
import com.embibe.embibetvapp.ui.presenter.CardPresenterSelector
import com.embibe.embibetvapp.ui.viewmodel.AchieveViewModel
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.TestViewModel
import com.embibe.embibetvapp.utils.CardListRow
import com.embibe.embibetvapp.utils.ShadowRowPresenterSelector
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.google.gson.Gson

class AchieveRowsFragment : BaseRowsSupportFragment(), DPadKeysListener {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var viewModel: CarouselsAchieveViewModel
    private lateinit var navigationMenuCallback: NavigationMenuCallback
    private lateinit var rowsToBannerListener: RowsToBannerListener
    private var mRowsAdapter: ArrayObjectAdapter = ArrayObjectAdapter(ShadowRowPresenterSelector())
    private lateinit var dPadKeysCallback: DPadKeysListener
    private lateinit var achieveViewModel: AchieveViewModel
    private lateinit var testViewModel: TestViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CarouselsAchieveViewModel::class.java)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        achieveViewModel = ViewModelProviders.of(this).get(AchieveViewModel::class.java)
        testViewModel = ViewModelProviders.of(this).get(TestViewModel::class.java)
        adapter = mRowsAdapter
        navigationMenuCallback.navMenuToggle(false)
        //loadData()
        loadAchieveHome()
    }

    init {
        initializeListeners()
    }

    private fun initializeListeners() {
        adapter = mRowsAdapter
        onItemViewClickedListener =
            OnItemViewClickedListener { itemViewHolder, item, rowViewHolder, row ->
                item as Content

                /*restrict the click*/
                if (item.status.toLowerCase().equals("locked")) {
                    return@OnItemViewClickedListener
                }

                Utils.isSubjectFilterFromPractice = false
                Utils.subjectFilterBy = AppConstants.LEARN
                ((row as CardListRow).adapter as ArrayObjectAdapter).indexOf(item)
                mRowsAdapter.indexOf(row)
                when (item.type.toLowerCase()) {
                    AppConstants.SUBJECT -> {
                        if (item.subject != "All Subjects") {
                            loadAchieveHomeSubject(item.subject)
                        }else {
                            loadAchieveHome()
                        }
                    }
                    /*completedPAJ xpath and PostXpath*/
                    AppConstants.PAJ -> {
                        /*if (item.completion_status.toLowerCase().equals("completed")) {
                            val intent = Intent(context, ImprovementActivity::class.java)
                            intent.putStringArrayListExtra(
                                "testCode",
                                arrayListOf(item.bundle_id, item.post_test_bundle_id)
                            )
                            intent.putExtra("testName", item.title)
                            startActivity(intent)
                        } else {
                            val intent = Intent(context, TestFeedbackActivity::class.java)
                            intent.putStringArrayListExtra(
                                "testCode",
                                arrayListOf(item.bundle_id, item.post_test_bundle_id)
                            )
                            intent.putExtra("testName", item.title)
                            startActivity(intent)
                        }*/

                    }
                    AppConstants.PAJ_STEP -> {
                        /*1 completed //if (item.covered_steps)
                        * 2. InProgress -100% (normal Feedback)*/
                        /*testAPIS*/
                        getSkills(item)

                    }

                    AppConstants.TEST -> {
                        moveToTestActivity(item.title, item.bundle_id, item.xpath)
                    }

                    else -> {
                        getSkills(item)
                    }


                }
            }

        onItemViewSelectedListener =
            OnItemViewSelectedListener { itemViewHolder, item, rowViewHolder, row ->
                var currentRowSize = ((row as CardListRow).adapter as ArrayObjectAdapter).size()
                val indexOfItem = (row.adapter as ArrayObjectAdapter).indexOf(item)
                val indexOfRow = mRowsAdapter.indexOf(row)
                if (item is Content) {
                    val itemView = itemViewHolder.view
                    if (itemView.isFocusable && !itemView.isFocused) itemView.requestFocus()
                    saveLastSelected(indexOfItem, indexOfRow)
                    itemView.setOnKeyListener { v, keyCode, event ->
                        //checking only for down presses
                        if (event.action == KeyEvent.ACTION_DOWN) {
                            when (keyCode) {
                                KeyEvent.KEYCODE_DPAD_LEFT -> {
                                    if (indexOfItem == 0) {
                                        navigationMenuCallback.navMenuToggle(true)
                                    }
                                }
                                KeyEvent.KEYCODE_DPAD_RIGHT -> {
                                }
                                KeyEvent.KEYCODE_DPAD_UP -> {
                                    if (indexOfRow == 0 && !Utils.isKeyPressedTooFast(500)) {
                                        rowsToBannerListener.rowsToBanner()
                                    }
                                }
                                KeyEvent.KEYCODE_DPAD_DOWN -> {
                                    //for last row
                                    if (indexOfRow == mRowsAdapter.size() - 1) {
                                        itemView.postDelayed({
                                            itemView.requestFocus()
                                        }, 0)
                                    }
                                }
                            }
                        }
                        false
                    }
                }
            }
    }

    private fun getSkills(content: Content) {
        showProgress()

        testViewModel.getSkills(
            content.bundle_id,
            object : BaseViewModel.APICallBacks<TestSkillsRes> {
                override fun onSuccess(testSkill: TestSkillsRes?) {

                    if (testSkill != null && testSkill.questions > 0) {
                        testViewModel.getTestMergeCall(
                            content.bundle_id,
                            callback = object : BaseViewModel.APIMergeCallBacks<HomeModel> {
                                override fun onSuccess(model: HomeModel?) {
                                    DataManager.instance.setTestSkills(Gson().toJson(testSkill))
                                    DataManager.instance.setTestAttempts(Gson().toJson(model?.mTestAttempts))
                                    DataManager.instance.setTestAchieve(Gson().toJson(model?.mTestAchieveRes))
                                    DataManager.instance.setTestQuestionSummary(Gson().toJson(model?.mTestQuestionSummaryRes))
                                    DataManager.instance.setTestQuestion(Gson().toJson(model?.mTestQuestionResponse))
                                    getPAJ(content)
                                }

                                override fun onFailed(code: Int, error: String, msg: String) {
                                    makeLog("onFailed : getTestMergeCall $")
                                }

                            })


                    } else {
                        Handler().postDelayed({
                            getSkills(content)
                        }, 2000)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    hideProgress()
                    makeLog("$code - $error - $msg")
                }

            })
    }

    private fun moveToTestFeedBackActivity(testName: String, testCode: String) {
        val intent = Intent(context, TestFeedbackActivity::class.java)
        intent.putExtra("testName", testName)
        intent.putExtra("testCode", testCode)
        intent.putExtra(AppConstants.IS_DIAGNOSTIC, true)
        DataManager.instance.isDiagnosticTest = true
        startActivity(intent)
    }


    private fun moveToTestActivity(testName: String, testCode: String, xPath: String) {
        val intent = Intent(context, TestActivity::class.java)
        intent.putExtra("testName", testName)
        intent.putExtra("testCode", testCode)
        intent.putExtra(AppConstants.TEST_XPATH, xPath)
        startActivity(intent)
    }

    /*load getPAJ from api*/
    fun getPAJ(item: Content) {
        achieveViewModel.getAchieveFeedbackRes(item.bundle_id, item.id, object :
            BaseViewModel.APICallBacks<AchieveFeedbackRes> {

            override fun onSuccess(model: AchieveFeedbackRes?) {
                hideProgress()
                if (model != null) {
                    DataManager.instance.setTestPAJ(Gson().toJson(model))
                    moveToTestFeedBackActivity(item.title, item.bundle_id)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getPAJ(item)
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }
        })
    }


    private fun createRows(achieveHomeResponse: ArrayList<AchieveHome>) {
        try {
            if (mRowsAdapter.size() > 0) {
                mRowsAdapter.clear()
            }

            for ((index, section) in achieveHomeResponse.withIndex()) {
                if (section.section_name != "banner") {
                    mRowsAdapter.add(creatingNewRow(section))
                }

            }
        } catch (e: Exception) {
            println("errror")
        }
    }


    private fun creatingNewRow(section: AchieveHome): Row {
        val presenterSelector = activity?.baseContext?.let { CardPresenterSelector(it) }
        presenterSelector?.setDPadKeysListener(this)
        val adapter = ArrayObjectAdapter(presenterSelector)

        if (section.content.isNotEmpty()) {
            for (content in section.content) {
                adapter.add(content)
            }
        }
        val headerItem: HeaderItem =
            if (section.section_name != "Subjects") {
                HeaderItem(section.section_name)
            } else {
                HeaderItem(section.section_name)
            }
        return CardListRow(headerItem, adapter)
    }

    fun setFirstRowFocus() {
        Handler().postDelayed({
            mainFragmentRowsAdapter.setSelectedPosition(0, true)
        }, 0)
        mainFragmentRowsAdapter.setSelectedPosition(1, false)
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {
    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
        navigationMenuCallback.navMenuToggle(true)
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {
    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {
        dPadKeysCallback.isKeyPadEnter(true, "Rows", cardType)
    }


    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    fun setRowsToBannerListener(callback: RowsToBannerListener) {
        this.rowsToBannerListener = callback
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }

    private fun saveLastSelected(indexOfItem: Int, indexOfRow: Int) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        with(sharedPref?.edit()) {
            this?.putInt(getString(R.string.last_selected_achieve_row_item), indexOfItem)
            this?.putInt(getString(R.string.last_selected_achieve_row), indexOfRow)
            this?.apply()
        }
    }

    fun restoreLastSelection() {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        var itemIndex = sharedPref.getInt(getString(R.string.last_selected_achieve_row_item), 0)
        val rowIndex = sharedPref.getInt(getString(R.string.last_selected_achieve_row), 0)


        if (itemIndex == 0) {//for 0th item and Navigation interaction case
            val rvh = getRowViewHolder(rowIndex)
            rvh?.selectedItemViewHolder?.view?.focusSearch(View.FOCUS_LEFT)?.requestFocus()
        } else {
            //For Continue Learning row, 0th item has to be focused
            val rowsSize = mRowsAdapter.size()
            if (rowIndex < rowsSize) {
                val rowItemsSize = (mRowsAdapter.get(rowIndex) as CardListRow).adapter.size()
                if (itemIndex < rowItemsSize) {
                    setSelectedPosition(
                        rowIndex,
                        true,
                        object : ListRowPresenter.SelectItemViewHolderTask(itemIndex) {
                            override fun run(holder: Presenter.ViewHolder?) {
                                super.run(holder)//imp line
                                holder?.view?.postDelayed({
                                    holder.view.requestFocus()
                                    //hideProgress()
                                }, 10)
                            }
                        })
                }
            }
        }
    }

    /*load Achieve Home api*/
    private fun loadAchieveHome() {
        showProgress()
        homeViewModel.homeAchieveApi(object :
            BaseViewModel.APICallBacks<List<AchieveHome>> {
            override fun onSuccess(model: List<AchieveHome>?) {
                hideProgress()
                if (model != null) {
                    if (model.isNotEmpty()) {
                        updateHomeUI(model)
                    }

                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(
                        App.context,
                        code,
                        object : BaseViewModel.ErrorCallBacks {
                            override fun onRetry(msg: String) {
                                loadAchieveHome()
                            }

                            override fun onDismiss() {

                            }
                        })

                } else {
                    //showToast(error)
                }
            }

        })
    }

    private fun updateHomeUI(model: List<AchieveHome>) {
        createRows(model as ArrayList<AchieveHome>)
    }

    /*load Achieve subject filter Home api*/
    private fun loadAchieveHomeSubject(subject: String) {
        showProgress()
        homeViewModel.homeAchieveSubjectApi(subject,object :
            BaseViewModel.APICallBacks<List<AchieveHome>> {
            override fun onSuccess(model: List<AchieveHome>?) {
                hideProgress()
                if (model != null) {
                    if (model.isNotEmpty()) {
                        updateHomeUI(model)
                    }

                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(
                        App.context,
                        code,
                        object : BaseViewModel.ErrorCallBacks {
                            override fun onRetry(msg: String) {
                                loadAchieveHomeSubject(subject)
                            }

                            override fun onDismiss() {

                            }
                        })

                } else {
                    //showToast(error)
                }
            }

        })
    }
}