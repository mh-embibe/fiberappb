package com.embibe.embibetvapp.ui.fragment.learn

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.CountDownTimer
import android.view.KeyEvent
import android.view.View
import androidx.leanback.widget.*
import androidx.lifecycle.ViewModelProviders
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseRowsSupportFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.presenter.CardPresenterSelector
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.*
import com.embibejio.coreapp.preference.PreferenceHelper
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class SubjectInfoFragment : BaseRowsSupportFragment() {
    private var results: List<ResultsEntity> = arrayListOf()
    private val mRowsAdapter: ArrayObjectAdapter = ArrayObjectAdapter(ShadowRowPresenterSelector())
    private lateinit var volleyRequest: RequestQueue
    private lateinit var homeViewModel: HomeViewModel
    private var continueLearningRowIndex = 0
    private var isContinueLearningRowFound = false
    private var continueLearningItemsSize = 0
    private var previewUrlTimer: CountDownTimer? = null
    var pref = PreferenceHelper()
    private lateinit var listener: SharedPreferences.OnSharedPreferenceChangeListener


    var content: Content? = null

    init {
        initializeListeners()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        content = getContent(arguments)
        showProgress()
        doAsync {
            results = getLocalData()
            uiThread {
                if (results.isEmpty()) {
                    getSubjectFiltersApi(
                        false, getSubjectName(),
                        false
                    )
                } else {
                    hideProgress(5)
                    createRows(results)
                    getSubjectFiltersApi(
                        true, getSubjectName(),
                        true
                    )
                }
            }
        }

    }

    private fun getIsFromPractice(): Boolean {
        return when (Utils.subjectFilterBy) {
            AppConstants.LEARN -> {
                false
            }
            AppConstants.PRACTICE -> {
                true
            }
            AppConstants.TEST -> {
                false
            }
            AppConstants.QUICK_LINKS -> {
                false
            }
            AppConstants.SUBJECT -> {
                false
            }
            else -> false
        }
    }

    private fun getLocalData(): List<ResultsEntity> {
        return homeViewModel.fetchSectionByPageName(getPage(), getSubjectName())
    }

    private fun getPage(): String {
        return when (Utils.subjectFilterBy) {
            AppConstants.LEARN -> {
                AppConstants.SUBJECT_LEARN
            }
            AppConstants.PRACTICE -> {
                AppConstants.SUBJECT_PRACTICE
            }
            AppConstants.TEST -> {
                AppConstants.SUBJECT_TEST
            }
            AppConstants.QUICK_LINKS -> {
                AppConstants.SUBJECT_QUICK_LINKS
            }
            AppConstants.SUBJECT -> {
                getSubjectName()
            }
            else -> ""
        }
    }

    private fun getSubjectName(): String {
        if (content == null) {
            content = getContent(arguments)
        }
        return content?.subject ?: ""
    }

    override fun onResume() {
        super.onResume()
        /*check for content updated status*/
        if (Utils.isStatusUpdatedForVideosAndCoobos || preferences.getBoolean(
                AppConstants.ISCOOBO_UPDATED,
                false
            )
        ) {
            Utils.isStatusUpdatedForVideosAndCoobos = false
            preferences.edit().putBoolean(AppConstants.ISCOOBO_UPDATED, false).apply()
            /*refreshing subjects filter screen*/
            if (isFromTest()) {
                return
            }
            if (results.isNotEmpty() && results[1].sectionId == 300L) {
                isContinueLearningRowFound = true
                continueLearningRowIndex = 1
                val section = results[continueLearningRowIndex]
                paginateSection(section)
            } else {
                /* ContinueLearning Not found*/
                isContinueLearningRowFound = false
                homeViewModel.homeSubjectFilter(
                    getSubjectName(),
                    Utils.isSubjectFilterFromPractice,
                    object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
                        override fun onSuccess(model: List<ResultsEntity>?) {
                            if (model != null && model.isNotEmpty()) {
                                val continueSection = model[1]
                                if (continueSection.sectionId == 300L) {
                                    updateSectionToDB(continueSection)
                                    if (results.isNotEmpty()) {
                                        insertNewRowAsContinueLearning(continueSection)
                                    } else {
                                        results = model
                                        createRows(results)
                                    }

                                }
                            }
                        }

                        override fun onFailed(code: Int, error: String, msg: String) {
                        }
                    })
            }
        }
    }


    private fun updateSectionToDB(section: ResultsEntity) {
        dataRepo.modifySection(section, object : BaseViewModel.DataCallback<ResultsEntity> {
            override fun onSuccess(model: ResultsEntity?) {

            }
        })
    }

    private fun paginateSection(section: ResultsEntity) {
        section.size = continueLearningItemsSize + paginationSize
        homeViewModel.homeSubjectFilterSection(
            getSubjectName(), Utils.isSubjectFilterFromPractice,
            section.content_section_type,
            0,
            section.size,
            object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
                override fun onSuccess(model: List<ResultsEntity>?) {
                    if (model != null && model.isNotEmpty()) {
                        val newSection = model[0]
                        newSection.objId = section.objId
                        if (results.isNotEmpty() && results[1].sectionId == 300L) {
                            continueLearningRowIndex = 1
                            updateContinueLearning(newSection)
                        } else {
                            insertNewRowAsContinueLearning(newSection)
                        }
                        updateSectionToDB(newSection)
                    } else {
                        removeSection()
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {


                }
            })
    }

    private fun updateContinueLearning(newSection: ResultsEntity) {
        /*update new Row as ContinueLearning*/
        val row = mRowsAdapter.get(continueLearningRowIndex) as CardListRow
        val rowItemsAdapter = row.adapter as ArrayObjectAdapter
        rowItemsAdapter.setItems(getContentItems(newSection), null)
        mRowsAdapter.replace(
            continueLearningRowIndex,
            CardListRow(HeaderItem(newSection.section_name), rowItemsAdapter)
        )
        saveContinueLearningIndex(newSection.section_name, continueLearningRowIndex)
    }

    private fun insertNewRowAsContinueLearning(section: ResultsEntity) {
        /*create new Row as ContinueLearning*/
        (results as ArrayList).add(1, section)
        mRowsAdapter.add(1, creatingNewRow(section))
        saveContinueLearningIndex(section.section_name, 1)
    }

    private fun removeSection() {
        if (results[1].sectionId == 300L) {
            val row = mRowsAdapter.get(1) as CardListRow
            val rowItemsAdapter = row.adapter as ArrayObjectAdapter
            mRowsAdapter.remove(results[1])
            mRowsAdapter.removeItems(1, 1)
            dataRepo.removeSection(results[1])
            makeLog("ContinueLearning Removed")
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        volleyRequest = Volley.newRequestQueue(activity)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
    }

    fun getSubjectFiltersApi(
        isAsync: Boolean,
        subject: String,
        isRefresh: Boolean
    ) {
        if (!isAsync) showProgress()
        val callback = object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
            override fun onSuccess(model: List<ResultsEntity>?) {
                if (model != null && model.isNotEmpty()) {
                    if (!isRefresh) {
                        mRowsAdapter.clear()
                        createRows(model as ArrayList<ResultsEntity>)
                    }
                    doAsync {
                        val callback = object : BaseViewModel.DataCallback<String> {
                            override fun onSuccess(model: String?) {
                                if (!isAsync) {
                                    uiThread {
                                        updateRows()
                                    }
                                }
                            }

                        }
                        homeViewModel.saveResultToDb(model, getPage(), getSubjectName(), callback)
                    }


                } else {
                    makeLog("homeSubjectFilter data empty")

                }
                if (!isAsync) {
                    hideProgress()
                }

            }

            override fun onFailed(code: Int, error: String, msg: String) {
                if (!isAsync) {
                    hideProgress()
                    if (Utils.isApiFailed(code)) {
                        Utils.showError(
                            context,
                            code,
                            object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: String) {
                                    getSubjectFiltersApi(
                                        false, getSubjectName(),
                                        false
                                    )
                                }

                                override fun onDismiss() {
                                    activity?.finish()
                                }
                            })
                    } else {
                        showToast(error)
                    }
                }


            }
        }
        when (Utils.subjectFilterBy) {
            AppConstants.LEARN -> {
                homeViewModel.homeSubjectFilter(subject, false, callback)
            }
            AppConstants.PRACTICE -> {
                homeViewModel.homeSubjectFilter(subject, true, callback)
            }
            AppConstants.TEST -> {
                homeViewModel.homeTestSubjectFilter(subject, callback)
            }
            AppConstants.QUICK_LINKS -> {
                homeViewModel.homeSubjectFilter(subject, false, callback)
            }
            AppConstants.SUBJECT -> {
                homeViewModel.homeSubjectFilter(subject, false, callback)
            }
        }


    }


    private fun updateRowItemForAdapter(
        indexOfRow: Int,
        lastIndexOfRow: Int,
        arrayObjectAdapter: ArrayObjectAdapter
    ) {
        if (results.isNotEmpty()) {
            if (!isFromTest()) {
                paginateApiAsync(indexOfRow, lastIndexOfRow, arrayObjectAdapter)
            }

        }
    }

    private fun isFromTest(): Boolean {
        return when (Utils.subjectFilterBy) {
            AppConstants.LEARN -> {
                false
            }
            AppConstants.PRACTICE -> {
                false
            }
            AppConstants.TEST -> {
                true
            }
            AppConstants.QUICK_LINKS -> {
                false
            }
            AppConstants.SUBJECT -> {
                false
            }
            else -> false
        }
    }

    fun paginateApiAsync(
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter
    ) {
        val section = results[indexOfRow]
        if (section.total > calculateOffset(section)) {
            section.size = paginationSize
            homeViewModel.homeSubjectFilterSection(
                getSubjectName(),
                Utils.isSubjectFilterFromPractice,
                section.content_section_type,
                calculateOffset(section),
                section.size,
                object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
                    override fun onSuccess(model: List<ResultsEntity>?) {
                        if (model != null && model.isNotEmpty()) {
                            updateSection(
                                model[0],
                                section,
                                indexOfRow,
                                lastIndexOfRow,
                                currentAdapter
                            )
                        }
                    }

                    override fun onFailed(code: Int, error: String, msg: String) {


                    }
                })

        }

    }

    private fun updateSection(
        resultsEntity: ResultsEntity,
        section: ResultsEntity,
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter
    ) {
        resultsEntity.content?.let { section.content?.addAll(it) }
        section.offset = resultsEntity.offset
        section.size = resultsEntity.size
        makeLog("updatedSection old section to DB Size : ${section.content?.size}")
        /*local DB*/
        dataRepo.modifySection(section, object : BaseViewModel.DataCallback<ResultsEntity> {
            override fun onSuccess(model: ResultsEntity?) {
                getUpdatedSection(
                    section.sectionId,
                    indexOfRow,
                    lastIndexOfRow,
                    currentAdapter,
                    resultsEntity.content ?: arrayListOf()
                )
            }
        })
        /*UI*/
        bindSectionToUI(
            section,
            indexOfRow,
            lastIndexOfRow,
            currentAdapter,
            resultsEntity.content ?: arrayListOf()
        )

    }

    private fun getUpdatedSection(
        sectionId: Long,
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter,
        newContentList: ArrayList<Content>
    ) {
        val updatedSection = homeViewModel.getSectionById(sectionId, getPage())
        //bindSectionToUI(updatedSection, indexOfRow, lastIndexOfRow, currentAdapter,newContentList)
    }

    private fun bindSectionToUI(
        updatedSection: ResultsEntity?,
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter,
        newContentList: ArrayList<Content>
    ) {
        /*update the Section */
        currentAdapter.addAll(lastIndexOfRow, newContentList)
        makeLog("updatedSection?.content size :  ${updatedSection?.content?.size}")
    }


    private fun updateRows() {

        results = getLocalData()
        if (results.isNotEmpty()) {
            for ((index, video) in results.withIndex()) {
                val row = mRowsAdapter.get(index) as CardListRow
                val rowItemsAdapter = row.adapter as ArrayObjectAdapter
                rowItemsAdapter.setItems(getContentItems(video), null)
                mRowsAdapter.replace(
                    index,
                    CardListRow(HeaderItem(video.section_name), rowItemsAdapter)
                )
                saveContinueLearningIndex(video.section_name, index)
            }
            restoreLastSelection()
        }
    }

    private fun restoreLastSelection() {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        var itemIndex =
            sharedPref.getInt(getString(R.string.last_selected_subject_info_row_item), 0)
        val rowIndex = sharedPref.getInt(getString(R.string.last_selected_subject_info_row), 0)

        //For Continue Learning row, 0th item has to be focused
        if (rowIndex == continueLearningRowIndex)
            itemIndex = 0

        val rowsSize = mRowsAdapter.size()
        val rowItemsSize = (mRowsAdapter.get(rowIndex) as CardListRow).adapter.size()
        if (rowIndex < rowsSize && itemIndex < rowItemsSize) {
            setSelectedPosition(
                rowIndex,
                true,
                object : ListRowPresenter.SelectItemViewHolderTask(itemIndex) {
                    override fun run(holder: Presenter.ViewHolder?) {
                        super.run(holder)//imp line
                        holder?.view?.postDelayed({
                            holder.view.requestFocus()
                            hideProgress()
                        }, 10)
                    }
                })
        }

    }

    private fun saveContinueLearningIndex(sectionName: String, index: Int) {
        if (sectionName == "Continue Learning")
            continueLearningRowIndex = index
    }

    private fun createRows(results: List<ResultsEntity>) {
        if (mRowsAdapter.size() > 0) {
            mRowsAdapter.clear()
        }
        for ((index, video) in results.withIndex()) {
            mRowsAdapter.add(creatingNewRow(video))
            saveContinueLearningIndex(video.section_name, index)
        }
    }

    private fun creatingNewRow(videos: ResultsEntity): Row {
        val presenterSelector = activity?.baseContext?.let { CardPresenterSelector(it) }
        val adapter = ArrayObjectAdapter(presenterSelector)
        for (video in videos.content!!) {
            adapter.add(video)
        }
        val headerItem = HeaderItem(videos.section_name)
        return CardListRow(headerItem, adapter)
    }

    inline fun <reified T : Any> Any.cast(): T {
        return this as T
    }

    private fun initializeListeners() {
        adapter = mRowsAdapter
        onItemViewClickedListener =
            OnItemViewClickedListener { itemViewHolder, item, rowViewHolder, row ->
                item as Content
                if (item.type.toLowerCase().equals(AppConstants.SUBJECT)) {
                    if (item.subject != "All Subjects") {

                    }
                } else {
                    Utils.subjectFilterBy = AppConstants.SUBJECT
                    onClickItem(item, row, mRowsAdapter, itemViewHolder)
                }
            }

        onItemViewSelectedListener =
            OnItemViewSelectedListener { itemViewHolder, item, rowViewHolder, row ->
                var currentRowSize = ((row as CardListRow).adapter as ArrayObjectAdapter).size()
                val indexOfItem = (row.adapter as ArrayObjectAdapter).indexOf(item)
                val indexOfRow = mRowsAdapter.indexOf(row)

                if ((currentRowSize - (indexOfItem + 1)) == 5) {
                    updateRowItemForAdapter(
                        indexOfRow,
                        currentRowSize - 1,
                        (row.adapter as ArrayObjectAdapter)
                    )
                }
                if (item is Content) {
                    val indexOfRow = mRowsAdapter.indexOf(row)
                    val indexOfItem =
                        (row.adapter as ArrayObjectAdapter).indexOf(item)

                    SegmentUtils.trackEventHomeTileFocus(
                        item,
                        row.headerItem.name,
                        indexOfRow,
                        indexOfItem
                    )

                    val rowView = rowViewHolder.view
                    val itemView = itemViewHolder.view
                    if (itemView is GifCardView) {
                        addDelayToPreviewUrls(item, itemView)
                    }

                    if (itemView.isFocusable && !itemView.isFocused) itemView.requestFocus()

                    saveLastSelected(indexOfItem, indexOfRow)

                    itemView.setOnKeyListener { v, keyCode, event ->
                        //checking only for down presses
                        if (event.action == KeyEvent.ACTION_DOWN) {
                            when (keyCode) {
                                KeyEvent.KEYCODE_DPAD_LEFT,
                                KeyEvent.KEYCODE_DPAD_RIGHT,
                                KeyEvent.KEYCODE_DPAD_UP,
                                KeyEvent.KEYCODE_DPAD_DOWN -> {
                                    previewUrlTimer?.cancel()
                                    if (itemView is GifCardView) {
                                        itemView.ivVideo.controller?.animatable?.stop()
                                    }
                                }
                            }
                        }
                        false
                    }
                }
            }
    }

    private fun addDelayToPreviewUrls(
        item: Content,
        itemView: GifCardView
    ) {
        previewUrlTimer = object : CountDownTimer(4000, 1000) {
            override fun onFinish() {
                if (item.preview_url.isNotEmpty())
                    itemView.showVideoView(item.preview_url)
            }

            override fun onTick(millisUntilFinished: Long) {}
        }
        previewUrlTimer?.start()
    }

    private fun saveLastSelected(indexOfItem: Int, indexOfRow: Int) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        with(sharedPref?.edit()) {
            this?.putInt(getString(R.string.last_selected_subject_info_row_item), indexOfItem)
            this?.putInt(getString(R.string.last_selected_subject_info_row), indexOfRow)
            this?.apply()
        }
    }


}