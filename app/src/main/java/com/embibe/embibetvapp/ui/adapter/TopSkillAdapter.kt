package com.embibe.embibetvapp.ui.adapter

import android.view.ViewGroup
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemTopSkillBarBinding
import com.embibe.embibetvapp.model.test.SkillSet
import com.embibe.embibetvapp.ui.viewholders.BaseViewHolder
import com.embibe.embibetvapp.ui.viewholders.TopSkillBarViewHolder
import com.embibe.embibetvapp.utils.Utils

class TopSkillAdapter : BaseAdapter<SkillSet>() {
    override fun getLayoutId(position: Int, obj: SkillSet) = R.layout.item_top_skill_bar

    override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<SkillSet> {
        val binding = Utils.binder<ItemTopSkillBarBinding>(R.layout.item_top_skill_bar, parent)
        return TopSkillBarViewHolder(binding, listItems)
    }
}