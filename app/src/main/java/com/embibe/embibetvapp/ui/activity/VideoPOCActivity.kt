package com.embibe.embibetvapp.ui.activity

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.view.View
import android.view.View.*
import android.view.ViewTreeObserver
import android.view.WindowManager
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.ui.adapter.JarListAdapter
import com.embibe.videoplayer.model.*
import com.embibe.videoplayer.player.audioPlayer.AudioFinishCallBack
import com.embibe.videoplayer.player.audioPlayer.AudioPlayer
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_video_poc.*
import kotlinx.android.synthetic.main.layout_test_analysis_progress.*
import kotlinx.android.synthetic.main.layout_test_analysis_progress.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.InputStream


class VideoPOCActivity : ScatterGraphActivity() {

    private var coordinateModel: CoordinateModel? = null
    private lateinit var dynamicTextView: TextView
    private lateinit var sceneDataModel: SceneDataModel
    private lateinit var sceneDetailsModel: SceneDetailsModel
    private var sceneDetailsModels: List<SceneDetailsModel>? = null
    private val CLEARTEXT: String = "0"
    private val SHOWTEXT: String = "1"
    private val SHOWGRAPH: String = "2"
    private val SHOWJARS: String = "3"
    private val HIDEJARS: String = "4"
    private val PLOT: String = "5"
    private val GRAPHTEXT: String = "6"
    private val MOVEHITEMS: String = "7"
    private val HIDEGRAPH: String = "8"
    private val MOVEHITEMSINREVERSE: String = "9"

    private var scenes: List<Scene>? = null
    private var sceneDetails: List<SceneDetailsModel>? = null
    private var coordinatesList: List<CoordinateModel>? = null
    private var sceneTypes: List<SceneTypeModel>? = null
    private var mCurrentPosition: Int = 0
    private val animDuration: Long = 300

    private lateinit var array: JSONArray
    private var text: String = ""
    private var videoFileName: String = ""
    private var jsonFileName: String = ""
    private var data: JSONObject = JSONObject()
    private var coordinates: JSONObject = JSONObject()
    private var type: String = ""
    private lateinit var callBack: Runnable
    private var isVideoLooking: Boolean = false
    private var audioTrackName: String = ""
    private var audioTrackPath: String = ""
    private lateinit var player: SimpleExoPlayer
    private lateinit var playerAlpha: VideoView
    private var scenePosition: Int = 0
    val bandwidthMeter: BandwidthMeter = DefaultBandwidthMeter()

    val handler = Handler()
    val audioPlayer = AudioPlayer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setContentView(R.layout.activity_video_poc)

        initUI()
        jsonFileName = intent.getStringExtra("data") ?: "achiever"
        openData(jsonFileName)
        initPlayer()
        setVideoPlayerListener()
        startScatterChart()
        clickListener()
        setAndAnimateProgress()
    }

    private fun clickListener() {

        back.setOnClickListener {
            stopAudio()
            stopPlayer()
            super.onBackPressed()
        }
    }


    /*initUI*/
    private fun initUI() {
        plot = findViewById<View>(R.id.plotArea) as FrameLayout
        xLabelsView = findViewById<View>(R.id.x_labels) as FrameLayout
        yLabelsView = findViewById<View>(R.id.y_labels) as FrameLayout
        graph = findViewById<View>(R.id.graph) as FrameLayout
    }

    private fun setAndAnimateProgress() {
        val goodScore = 75
        val yourScore = 215
        val cutOffScore = 35

        /*if (list.size >= 5)
            changeMotionLayoutConstraintSetToFit()*/

        animate(
            goodScore,
            progress_bar_good_score,
            tv_good_score
        )
        moveCutOffLineBiasBasedOnScore(cutOffScore)
        if (yourScore < 0) {
            animate(
                yourScore,
                progress_bar_negative,
                tv_negative_score
            )
        } else {
            animate(
                yourScore,
                progress_bar_your_score,
                tv_your_score
            )
        }
    }

    private fun moveCutOffLineBiasBasedOnScore(cutOffScore: Int) {
        // AnyScore/totalScore to get bias value
        //totalMarks 130, Negative Marks -12

        val calScore = (cutOffScore.toDouble().div(130))
        val cutOffLine = iv_cut_off_marker
        val cutOffLabel = cardTestAnalysis.tv_cut_off_marker_label
        val cutOffLineParams = cutOffLine.layoutParams as ConstraintLayout.LayoutParams
        val cutOffLineAnimator = ValueAnimator.ofFloat(0f, calScore.toFloat())
        cutOffLineAnimator.addUpdateListener { value ->
            cutOffLineParams.horizontalBias = (value.animatedValue as Float)
            cutOffLine.layoutParams = cutOffLineParams
        }

        val cutOffLabelAnimator = ValueAnimator.ofInt(0, cutOffScore)
        cutOffLabelAnimator.addUpdateListener { value ->
            cutOffLabel.text = (value.animatedValue as Int).toString()
        }
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(cutOffLabelAnimator, cutOffLineAnimator)
        animatorSet.duration = 1500
        animatorSet.start()
    }

    private fun animate(progressValue: Int, progressBar: ImageView, tv: TextView) {
        animateBar(progressBar, progressValue)
        tv.animateWithProgressBar(progressValue)
    }

    private fun animateBar(bar: ImageView, progressValue: Int) {
        var progressLayoutWidth: Int = 0
        var height: Int = 0

        val progressLayout = cardTestAnalysis
        val vto = progressLayout.rootView.viewTreeObserver
        var newProgressValue = 0
        newProgressValue = Math.abs(progressValue)
        var listener: ViewTreeObserver.OnGlobalLayoutListener? = null
        listener = ViewTreeObserver.OnGlobalLayoutListener {
            progressLayoutWidth = progressLayout.rootView.width
            height = progressLayout.rootView.height

            if (newProgressValue < progressLayoutWidth) {
                val calValue = newProgressValue.toDouble() / 130 * progressLayoutWidth
                val animator = ValueAnimator.ofInt(0, calValue.toInt())
                animator.addUpdateListener { valueAnimator ->
                    val value = valueAnimator.animatedValue
                    val params = bar.layoutParams as ConstraintLayout.LayoutParams
                    if (value as Int > 0) {
                        bar.visibility = View.VISIBLE
                    } else
                        bar.visibility = View.GONE
                    params.width = value
                    bar.layoutParams = params
                }

                animator.duration = 1500
                animator.start()
            } else {
                /*if progresValue is more than width
                    newWidth = (currentMarks/TotalMarks) * layoutWidth
                */

            }
            cardTestAnalysis.rootView.viewTreeObserver?.removeOnGlobalLayoutListener(
                listener!!
            )
        }
        vto?.addOnGlobalLayoutListener(listener)
    }

    private fun TextView.animateWithProgressBar(progressValue: Int) {

        val animator = ObjectAnimator.ofInt(0, progressValue)
        animator.addUpdateListener { animationValue ->
            this.text = animationValue.animatedValue.toString()
        }
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(animator)
        animatorSet.duration = 2000
        animatorSet.start()
    }

    private fun readJSONFromAsset(jsonDataFileName: String): String? {
        var json: String? = null
        try {
            val inputStream: InputStream = assets.open("$jsonDataFileName.json")
            json = inputStream.bufferedReader().use { it.readText() }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    fun openData(jsonDataFileName: String) {
        try {
            val obj = JSONObject(readJSONFromAsset(jsonDataFileName) ?: "")
            val acheiver: Achiever = fromJson(obj.toString())
            videoFileName = acheiver.video
            scenes = acheiver.scenes ?: arrayListOf()
            val sceneModel = scenes?.get(scenePosition)
            updateSceneData(sceneModel)
        } catch (e: Exception) {
            Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
        }

    }

    private fun updateSceneData(sceneModel: Scene?) {
        audioTrackName = sceneModel?.audio ?: ""
        coordinatesList = sceneModel?.map?.coordinatesList
        sceneTypes = sceneModel?.map?.sceneTypes
        sceneDetails = sceneModel?.map?.sceneDetails
    }

    inline fun <reified T> fromJson(json: String): T {
        return Gson().fromJson(json, object : TypeToken<T>() {}.type)
    }


    private fun initPlayer() {

        try {
            val resId = resources.getIdentifier(videoFileName, "raw", packageName)
            val videoUri = RawResourceDataSource.buildRawResourceUri(resId)
            val trackSelector: TrackSelector =
                DefaultTrackSelector(AdaptiveTrackSelection.Factory(bandwidthMeter))
            player = ExoPlayerFactory.newSimpleInstance(this, trackSelector)
            //val dataSourceFactory = DefaultHttpDataSourceFactory("exoplayer-codelab")
            val dataSourceFactory: DataSource.Factory =
                DefaultDataSourceFactory(this, Util.getUserAgent(this, "exoplayer-codelab"))

            player = ExoPlayerFactory.newSimpleInstance(
                this,
                DefaultTrackSelector(),
                DefaultLoadControl()
            )

            val mediaSource: MediaSource = ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(videoUri)
            val metrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(metrics)
            exo_player.player = player
            player.volume = 0f
            player.prepare(mediaSource, true, false)
            play()
        } catch (e: Exception) {
            Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
        }
    }

    /*video changes listener*/
    private fun setVideoPlayerListener() {

        player.addListener(object : Player.DefaultEventListener() {
            override fun onPlayerStateChanged(
                playWhenReady: Boolean,
                playbackState: Int
            ) {
                when (playbackState) {
                    ExoPlayer.STATE_BUFFERING -> {
                    }
                    ExoPlayer.STATE_ENDED -> {
                        /*looping video*/
                        isVideoLooking = true
                        player.seekTo(0)
                    }
                    ExoPlayer.STATE_IDLE -> {
                    }
                    ExoPlayer.STATE_READY -> {
                        if (!isVideoLooking) {
                            playAudio()
                        }
                    }
                    else -> {
                    }
                }
            }

            fun onPlayWhenReadyCommitted() {}

            override fun onPlayerError(error: ExoPlaybackException) {
                makeLog("onPlayerError: " + error.cause!!.message)
            }
        })
    }


    fun play() {
        player.seekTo(0)
        player.playWhenReady = true
    }


    fun playAudio() {
        listenForPositionChange()
        audioTrackPath = "Female/$audioTrackName.mp3"
        audioPlayer.playAudio(this, audioTrackPath, object : AudioFinishCallBack {
            override fun onCompleted(msg: String) {
                ++scenePosition
                if (scenes?.size!! > scenePosition) {
                    val sceneModel = scenes?.get(scenePosition)
                    updateSceneData(sceneModel)
                    playAudio()
                } else {
                    clearText()
                    stopHandler()
                }
            }
        })

    }


    private fun bindText(textView: View, x: Int, y: Int) {
        runOnUiThread {
            surface_view.addView(textView)
            setPosition(textView, x, y)
        }

    }

    private fun setPosition(view: View, x: Int?, y: Int?) {
        val lp: FrameLayout.LayoutParams =
            FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
            )
        lp.setMargins(x!!, y!!, 0, 0)

        view.layoutParams = lp
    }


    private fun listenForPositionChange() {
        if (::callBack.isInitialized)
            stopHandler()
        val delay = 1000L  //milliseconds

        callBack = object : Runnable {
            override fun run() {
                repeatTask()
                handler.postDelayed(this, delay)
            }
        }
        handler.postDelayed(callBack, delay)
    }

    private fun repeatTask() {
        mCurrentPosition = audioPlayer.mMediaPlayer.currentPosition / 1000
        //bindText("Seconds : $mCurrentPosition", (1600).toInt()+mCurrentPosition, 200)
        sceneDetailsModels = sceneDetails?.filter { it.seconds == mCurrentPosition.toString() }
        if (sceneDetailsModels != null && sceneDetailsModels?.isNotEmpty()!!) {
            sceneDetailsModel = sceneDetailsModels!![0]
            sceneDataModel = sceneDetailsModel.sceneDataModel!!
            when (sceneDataModel.type) {
                CLEARTEXT -> {
                    clearText()
                }
                SHOWTEXT -> {
                    hideGraph()
                    showTextUI(sceneDataModel)
                }
                SHOWGRAPH -> {
                    showGraph()
                }
                SHOWJARS -> {
                    /*recyclerView*/
                    showTextUI(sceneDataModel)
                    setRecyclerItems(sceneDataModel.jarDetails)
                }
                HIDEJARS -> {
                    /*hide recyclerView*/
                    recyclerView.visibility = INVISIBLE
                    clearText()
                }

                PLOT -> {
                    plotPoints(getAllData())
                }
                GRAPHTEXT -> {
                    showGraph()
                    showTextUI(sceneDataModel)
                }
                MOVEHITEMS -> {
                    moveHexagons()
                }
                HIDEGRAPH -> {
                    hideGraph()
                }
                MOVEHITEMSINREVERSE -> {
                    reverseMoveHexagons()
                }

                else -> {

                }
            }
        }
    }

    private fun setRecyclerItems(models: List<JarModel>?) {

        recyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        /*var list = arrayListOf<JarModel>()
        for (i in 0 until models?.size!!){
            val model = JarModel()
            model.title = models.getJSONObject(i).getString("title")
            list.add(model)
        }*/
        val listAdapter = models?.let { JarListAdapter(this, it) }
        recyclerView.adapter = listAdapter

        val lp: FrameLayout.LayoutParams =
            FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
            )
        lp.setMargins(240, 400, 0, 0)

        recyclerView.layoutParams = lp
        recyclerView
    }

    private fun showTextUI(model: SceneDataModel) {
        text = model.text
        val coordinatePointsId = model.coordinatePointId

        if (!model.texts.isNullOrEmpty()) {
            surface_view.removeAllViews()
            for (textModel in model.texts) {
                coordinateModel = coordinatesList?.find { it.id == textModel.coordinatePointId }
                dynamicTextView = TextView(this)
                dynamicTextView.text = textModel.text ?: ""
                dynamicTextView.textSize = 25f
                dynamicTextView.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))

                bindText(dynamicTextView, coordinateModel?.x_axis!!, coordinateModel?.y_axis!!)
            }
        }
    }

    private fun clearText() {
        text = ""
        dynamicTextView.text = ""
        //bindText(text,0,0)
    }

    override fun onPause() {
        super.onPause()
        stopPlayer()
        stopAudio()
    }

    private fun stopPlayer() {
        exo_player.player = null
        player.stop()
        player.release()
    }


    override fun onDestroy() {
        super.onDestroy()
        stopAudio()
        stopPlayer()
        stopHandler()
    }

    private fun stopHandler() {
        try {
            handler.removeCallbacks(callBack)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun stopAudio() {
        audioPlayer.stopAudio()
        stopHandler()
    }

    override fun onBackPressed() {
        makeLog("BackPress disabled")
        super.onBackPressed()

    }


    fun hideGraph() {
        graph.visibility = GONE
    }

    fun showGraph() {
        graph.visibility = VISIBLE
    }

    private fun setScatterPlot() {
        plotPoints(getAllData())
    }


}