package com.embibe.embibetvapp.ui.fragment.practice

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import androidx.leanback.widget.*
import androidx.lifecycle.ViewModelProviders
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseRowsSupportFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.ui.interfaces.RowsToBannerListener
import com.embibe.embibetvapp.ui.presenter.CardPresenterSelector
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.*
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class PracticeRowsFragment : BaseRowsSupportFragment(), DPadKeysListener {
    private lateinit var results: List<ResultsEntity>
    private val mRowsAdapter: ArrayObjectAdapter = ArrayObjectAdapter(ShadowRowPresenterSelector())
    private lateinit var volleyRequest: RequestQueue
    private lateinit var navigationMenuCallback: NavigationMenuCallback
    private lateinit var rowsToBannerListener: RowsToBannerListener
    private lateinit var dPadKeysCallback: DPadKeysListener
    private var isBannerVisible = true
    private val classTag = PracticeRowsFragment::class.java.toString()
    private lateinit var homeViewModel: HomeViewModel
    var pref = PreferenceHelper()
    private var previewUrlTimer: CountDownTimer? = null
    var offset: Int = 0
    var continuePracticeRowIndex: Int = 0
    private var isContinuePracticeRowFound = false
    private var continueLearningItemsSize = 0

    init {
        initializeListeners()
    }


    companion object {
        var rowFocusedPosition = -1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        volleyRequest = Volley.newRequestQueue(activity)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        navigationMenuCallback.navMenuToggle(false)

        if (DataManager.instance.isHomePracticeDataRemoved) {
            loadData(false)
        } else {
            results = DataManager.instance.getHomePractice()
            createRows(results)
            loadPracticeDataFromApiAsync(true)
        }
    }

    override fun onResume() {
        super.onResume()
        /*check for content updated status*/
        restoreLastSelection(false)
        if (Utils.isContentStatusUpdatedForPractice) {
            Utils.isContentStatusUpdatedForPractice = !Utils.isContentStatusUpdatedForPractice
            /*refreshing home screen*/
            if (mRowsAdapter.size() > 0) {
                val row = mRowsAdapter.get(1) as CardListRow
                if (row.headerItem.name == "Continue Practice") {
                    isContinuePracticeRowFound = true
                    continuePracticeRowIndex = 1
                    val section = results[continuePracticeRowIndex]
                    paginateSection(section)
                } else {
                    /* ContinueLearning Not found*/
                    isContinuePracticeRowFound = false
                    homeViewModel.homePracticeApi(object :
                        BaseViewModel.APICallBacks<List<ResultsEntity>> {
                        override fun onSuccess(model: List<ResultsEntity>?) {
                            if (model != null && model.isNotEmpty()) {
                                val continueSection = model[2]
                                if (continueSection.sectionId == 300L) {
                                    insertNewRowAsContinueLearning(model, continueSection)
                                }
                            }
                        }

                        override fun onFailed(code: Int, error: String, msg: String) {
                        }
                    })
                }
            }
        }
    }

    private fun paginateSection(section: ResultsEntity) {
        section.size = continueLearningItemsSize + paginationSize
        homeViewModel.homePracticeSectionApi(
            section.content_section_type,
            0,
            section.size,
            object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
                override fun onSuccess(model: List<ResultsEntity>?) {
                    if (model != null && model.isNotEmpty()) {
                        val newSection = model[0]
                        updateContinueLearning(newSection)

                        if (results.get(1).sectionId != 300L) {
                            dataRepo.removeListData(results as ArrayList<ResultsEntity>)
                            val callback = object : BaseViewModel.DataCallback<String> {
                                override fun onSuccess(model: String?) {
                                    makeLog("Updated Saved to Local!")
                                }

                            }
                            (results as ArrayList).add(1, newSection)
                            homeViewModel.saveResultToDb(
                                results,
                                AppConstants.PRACTICE,
                                null,
                                callback
                            )
                            homeViewModel.saveResultToDb(
                                results,
                                AppConstants.PRACTICE,
                                null,
                                callback
                            )
                        } else {
                            newSection.objId = section.objId
                            updateSectionToDB(null, AppConstants.PRACTICE, newSection)
                        }

                    } else {
                        removeSection()
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {


                }
            })
    }

    private fun updateContinueLearning(newSection: ResultsEntity) {
        /*update new Row as ContinueLearning*/
        val row = mRowsAdapter.get(continuePracticeRowIndex) as CardListRow
        val rowItemsAdapter = row.adapter as ArrayObjectAdapter
        rowItemsAdapter.setItems(getContentItems(newSection), null)
        mRowsAdapter.replace(
            continuePracticeRowIndex,
            CardListRow(HeaderItem(newSection.section_name), rowItemsAdapter)
        )
        saveContinuePracticeIndex(newSection.section_name, continuePracticeRowIndex)
    }

    private fun insertNewRowAsContinueLearning(
        newResults: List<ResultsEntity>,
        section: ResultsEntity
    ) {
        /*create new Row as ContinueLearning*/
        if (results.get(1).sectionId != 300L) {

            dataRepo.removeListData(results as ArrayList<ResultsEntity>)
            val callback = object : BaseViewModel.DataCallback<String> {
                override fun onSuccess(model: String?) {
                    makeLog("Updated Saved to Local!")
                }

            }
            (results as ArrayList).add(1, section)
            homeViewModel.saveResultToDb(newResults, AppConstants.PRACTICE, null, callback)
        }
        mRowsAdapter.add(1, creatingNewRow(section))
        saveContinuePracticeIndex(section.section_name, 1)
    }

    private fun removeSection() {
        if (results[1].sectionId == 300L) {
            val row = mRowsAdapter.get(1) as CardListRow
            val rowItemsAdapter = row.adapter as ArrayObjectAdapter
            mRowsAdapter.remove(results[1])
            mRowsAdapter.removeItems(1, 1)
            dataRepo.removeSection(results[1])
            makeLog("ContinueLearning Removed")
        }
    }

    private fun updateSectionToDB(subject: String? = null, page: String, section: ResultsEntity) {
        section.subject = subject ?: "all"
        section.child_id = UserData.getChildId().toLong()
        section.page = page
        section.grade = UserData.getGrade()
        section.goal = UserData.getGoalCode()
        section.exam = UserData.getExamCode()
        dataRepo.modifySection(section, object : BaseViewModel.DataCallback<ResultsEntity> {
            override fun onSuccess(model: ResultsEntity?) {

            }
        })
    }

    private fun saveContinuePracticeIndex(sectionName: String, index: Int) {
        if (sectionName == "Continue Practice") {
            continuePracticeRowIndex = index
            makeLog("continuePracticeRowIndex $continuePracticeRowIndex sectionName $sectionName")
        }
    }


    private fun updateRows(results: List<ResultsEntity>) {
        try {
            for ((index, video) in results.withIndex()) {
                val row = mRowsAdapter.get(index) as CardListRow
                val rowItemsAdapter = row.adapter as ArrayObjectAdapter
                rowItemsAdapter.setItems(getContentItems(video), null)

                mRowsAdapter.replace(
                    index,
                    CardListRow(HeaderItem(video.section_name), rowItemsAdapter)
                )
                saveContinuePracticeIndex(video.section_name, continuePracticeRowIndex)
            }
            restoreLastSelection(false)
        } catch (e: Exception) {
            makeLog("app ${e.localizedMessage}")
            e.printStackTrace()
        }
    }


    fun loadPracticeDataFromApiAsync(isAsync: Boolean) {
        if (!isAsync) {
            //showProgress()
        }
        homeViewModel.homePracticeApi(object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
            override fun onSuccess(model: List<ResultsEntity>?) {
                try {
                    doAsync {
                        if (model != null) {
                            if (model.size - 1 != mRowsAdapter.size()) {
                                makeLog("size differs ${model.size - 1} : ${mRowsAdapter.size()}")
                                dataRepo.removeListData(results as ArrayList<ResultsEntity>)
                                homeViewModel.saveResultToDb(
                                    model,
                                    AppConstants.PRACTICE,
                                    null,
                                    object : BaseViewModel.DataCallback<String> {
                                        override fun onSuccess(s: String?) {
                                            loadData(false)
                                        }
                                    })
                            } else {
                                makeLog("size same")
                                val callback = object : BaseViewModel.DataCallback<String> {
                                    override fun onSuccess(model: String?) {
                                        DataManager.instance.setHomePractice(arrayListOf(), null)
                                        if (!isAsync) {
                                            loadData(true)
                                        }
                                    }

                                }
                                homeViewModel.saveResultToDb(
                                    model,
                                    AppConstants.PRACTICE,
                                    null,
                                    callback
                                )
                            }
                        }
                    }
                    if (!isAsync) {
                        // hideProgress()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    if (!isAsync) {
                        //hideProgress()
                    }
                }

            }

            override fun onFailed(code: Int, error: String, msg: String) {
                if (!isAsync) {
                    if (code == 204) //no content success
                        rowsToBannerListener.notifyHost(AppConstants.NO_CONTENT)

                    // hideProgress()
                    if (Utils.isApiFailed(code)) {
                        Utils.showError(
                            context,
                            code,
                            object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: String) {
                                    loadPracticeDataFromApiAsync(isAsync)
                                }

                                override fun onDismiss() {

                                }
                            })

                    } else {
                        showToast(error)
                    }
                }

            }
        })
    }

    private fun loadData(isUpdate: Boolean) {
        if (!isUpdate) {
            //showProgress()
        }
        doAsync {
            results = homeViewModel.fetchSectionByPageName(AppConstants.PRACTICE)
            makeLog("size of local Data : ${results.size}")
            uiThread {
                if (results.isEmpty()) {
                    loadPracticeDataFromApiAsync(false)
                } else {
                    rowsToBannerListener.notifyHost(AppConstants.CONTENT_AVAILABLE)
                    if (isUpdate) {
                        updateRows(results)
                    } else {
                        //hideProgress()
                        createRows(results)
                    }
                    if (!isUpdate) {
                        loadPracticeDataFromApiAsync(true)
                    }
                    rowsToBannerListener.notifyBanner("Synced")
                }

            }
        }
    }

    private fun updateRowItemForAdapter(
        indexOfRow: Int,
        lastIndexOfRow: Int,
        arrayObjectAdapter: ArrayObjectAdapter
    ) {
        if (::results.isInitialized) {
            paginateApiAsync(indexOfRow, lastIndexOfRow, arrayObjectAdapter)

        }
    }


    fun paginateApiAsync(
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter
    ) {
        val section = results[indexOfRow]
        if (section.total > calculateOffset(section)) {
            section.size = paginationSize
            homeViewModel.homePracticeSectionApi(
                section.content_section_type,
                calculateOffset(section),
                section.size,
                object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
                    override fun onSuccess(model: List<ResultsEntity>?) {
                        if (model != null && model.isNotEmpty()) {
                            updateSection(
                                model[0],
                                section,
                                indexOfRow,
                                lastIndexOfRow,
                                currentAdapter
                            )
                        }
                    }

                    override fun onFailed(code: Int, error: String, msg: String) {


                    }
                })
        }

    }


    private fun updateSection(
        resultsEntity: ResultsEntity,
        section: ResultsEntity,
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter
    ) {
        resultsEntity.content?.let { section.content?.addAll(it) }
        section.offset = resultsEntity.offset
        section.size = resultsEntity.size
        makeLog("updatedSection old section to DB Size : ${section.content?.size}")
        /*local DB*/
        dataRepo.modifySection(section, object : BaseViewModel.DataCallback<ResultsEntity> {
            override fun onSuccess(model: ResultsEntity?) {
                getUpdatedSection(
                    section.sectionId,
                    indexOfRow,
                    lastIndexOfRow,
                    currentAdapter,
                    resultsEntity.content ?: arrayListOf()
                )
            }
        })
        /*UI*/
        bindSectionToUI(
            section,
            indexOfRow,
            lastIndexOfRow,
            currentAdapter,
            resultsEntity.content ?: arrayListOf()
        )

    }

    private fun getUpdatedSection(
        sectionId: Long,
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter,
        newContentList: ArrayList<Content>
    ) {
        val updatedSection = homeViewModel.getSectionById(sectionId, AppConstants.PRACTICE)
        //bindSectionToUI(updatedSection, indexOfRow, lastIndexOfRow, currentAdapter,newContentList)
    }

    private fun bindSectionToUI(
        updatedSection: ResultsEntity?,
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter,
        newContentList: ArrayList<Content>
    ) {
        /*update the Section */
        currentAdapter.addAll(lastIndexOfRow, newContentList)
        makeLog("updatedSection?.content size :  ${updatedSection?.content?.size}")
    }

    private fun createRows(results: List<ResultsEntity>) {
        if (mRowsAdapter.size() > 0) {
            mRowsAdapter.clear()
        }
        setBookDetails(results.filter { it.sectionId == 15L })
        for (video in results) {
            mRowsAdapter.add(creatingNewRow(video))
            saveContinuePracticeIndex(video.section_name, continuePracticeRowIndex)
        }
    }

    private fun creatingNewRow(videos: ResultsEntity): Row {
        val presenterSelector = activity?.baseContext?.let { CardPresenterSelector(it) }
        presenterSelector?.setDPadKeysListener(this)
        val adapter = ArrayObjectAdapter(presenterSelector)
        for (video in getContentItems(videos)) {
            adapter.add(video)
        }
        val headerItem = HeaderItem(videos.section_name)
        return CardListRow(headerItem, adapter)
    }

    inline fun <reified T : Any> Any.cast(): T {
        return this as T
    }

    private fun initializeListeners() {
        adapter = mRowsAdapter
        onItemViewClickedListener =
            OnItemViewClickedListener { itemViewHolder, item, rowViewHolder, row ->
                Utils.isSubjectFilterFromPractice = true
                Utils.subjectFilterBy = AppConstants.PRACTICE
                onClickItem(item as Content, row, mRowsAdapter, itemViewHolder)
            }

        onItemViewSelectedListener =
            OnItemViewSelectedListener { itemViewHolder, item, rowViewHolder, row ->
                var currentRowSize = ((row as CardListRow).adapter as ArrayObjectAdapter).size()
                val indexOfItem = (row.adapter as ArrayObjectAdapter).indexOf(item)
                val indexOfRow = mRowsAdapter.indexOf(row)
                makeLog("update currentRowSize : $currentRowSize" + " update indexOfRow :${indexOfRow}")
                if ((currentRowSize - (indexOfItem + 1)) == 5) {
                    updateRowItemForAdapter(
                        indexOfRow,
                        currentRowSize,
                        (row.adapter as ArrayObjectAdapter)
                    )
                }
                if (item is Content) {
                    val indexOfRow = mRowsAdapter.indexOf(row)
                    val indexOfItem =
                        (row.adapter as ArrayObjectAdapter).indexOf(item)

                    SegmentUtils.trackEventHomeTileFocus(
                        item,
                        row.headerItem.name,
                        indexOfRow,
                        indexOfItem
                    )

                    val itemView = itemViewHolder.view
                    if (itemView is BookVideoCardView) {
                       // addDelayToVideoUrls(indexOfRow, item, itemView)
                    }

                    if (itemView.isFocusable && !itemView.isFocused) itemView.requestFocus()

                    saveLastSelected(indexOfItem, indexOfRow)
                    rowFocusedPosition = indexOfItem
                    itemView.setOnKeyListener { v, keyCode, event ->
                        //checking only for down presses
                        if (event.action == KeyEvent.ACTION_DOWN) {
                            when (keyCode) {
                                KeyEvent.KEYCODE_DPAD_LEFT -> {
                                    previewUrlTimer?.cancel()

                                    if (indexOfItem == 0) {
                                        navigationMenuCallback.navMenuToggle(true)
                                    }
                                }
                                KeyEvent.KEYCODE_DPAD_RIGHT -> {
                                    previewUrlTimer?.cancel()

                                }
                                KeyEvent.KEYCODE_DPAD_UP -> {
                                    previewUrlTimer?.cancel()

                                    if (indexOfRow == 0 && !Utils.isKeyPressedTooFast(500))
                                        rowsToBannerListener.rowsToBanner()
                                }
                                KeyEvent.KEYCODE_DPAD_DOWN -> {
                                    if (isBannerVisible)
                                        dPadKeysCallback.isKeyPadDown(true, "Rows")
                                    //for last row
                                    previewUrlTimer?.cancel()
                                    if (indexOfRow == mRowsAdapter.size() - 1) {
                                        itemView.postDelayed({
                                            itemView.requestFocus()
                                        }, 0)
                                    }
                                }
                            }
                        }
                        false
                    }
                }
            }
    }

    fun isBannerVisible(visible: Boolean) {
        isBannerVisible = visible
    }


    private fun saveLastSelected(indexOfItem: Int, indexOfRow: Int) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        with(sharedPref?.edit()) {
            this?.putInt(getString(R.string.last_selected_practice_row_item), indexOfItem)
            this?.putInt(getString(R.string.last_selected_practice_row), indexOfRow)
            this?.apply()
        }
    }

    fun restoreLastSelection(fromNavigation: Boolean) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        var itemIndex = sharedPref.getInt(getString(R.string.last_selected_practice_row_item), 0)
        val rowIndex = sharedPref.getInt(getString(R.string.last_selected_practice_row), 0)

        if (fromNavigation) {//for 0th item and Navigation interaction case
            val rvh = getRowViewHolder(rowIndex)
            rvh?.selectedItemViewHolder?.view?.focusSearch(View.FOCUS_LEFT)?.requestFocus()
        } else {
            val rowsSize = mRowsAdapter.size()
            if (rowIndex < rowsSize) {
                val rowItemsSize = (mRowsAdapter.get(rowIndex) as CardListRow).adapter.size()
                if (itemIndex < rowItemsSize) {
                    setSelectedPosition(
                        rowIndex,
                        true,
                        object : ListRowPresenter.SelectItemViewHolderTask(itemIndex) {
                            override fun run(holder: Presenter.ViewHolder?) {
                                super.run(holder)//imp line
                                holder?.view?.postDelayed({
                                    holder.view.requestFocus()
                                    //hideProgress()
                                }, 10)
                            }
                        })
                }
            }
        }
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    fun setRowsToBannerListener(callback: RowsToBannerListener) {
        this.rowsToBannerListener = callback
    }

    fun setFirstRowFocus() {
        Handler().postDelayed({
            mainFragmentRowsAdapter.setSelectedPosition(0, true)
        }, 0)
        mainFragmentRowsAdapter.setSelectedPosition(1, false)
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {

    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {

    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
        navigationMenuCallback.navMenuToggle(true)
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {

    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {
        dPadKeysCallback.isKeyPadEnter(true, "Rows", cardType)
    }

    private fun addDelayToVideoUrls(indexOfRow: Int, item: Content, itemView: BookVideoCardView) {
        previewUrlTimer = object : CountDownTimer(4000, 1000) {
            override fun onFinish() {
                itemView.showVideoView(item.teaser_url)
            }

            override fun onTick(millisUntilFinished: Long) {}
        }
        previewUrlTimer?.start()
    }

}