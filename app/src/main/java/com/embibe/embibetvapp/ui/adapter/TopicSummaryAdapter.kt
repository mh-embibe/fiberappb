package com.embibe.embibetvapp.ui.adapter

import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.drawable.Animatable
import android.os.CountDownTimer
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.View.SCALE_X
import android.view.View.SCALE_Y
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.text.bold
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemPracticeForChapterBinding
import com.embibe.embibetvapp.databinding.ItemTestForChapterBinding
import com.embibe.embibetvapp.databinding.ItemTopicPreRequisiteForChapterBinding
import com.embibe.embibetvapp.databinding.ItemVideosForChapterBinding
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.utils.Utils
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo
import kotlinx.android.synthetic.main.item_practice_for_chapter.view.*
import kotlinx.android.synthetic.main.item_topic_pre_requisite_for_chapter.view.*
import kotlinx.android.synthetic.main.item_videos_for_chapter.view.*

class TopicSummaryAdapter(private val mContext: Context) :
    RecyclerView.Adapter<TopicSummaryAdapter.ItemViewHolder<*>>() {
    var list: ArrayList<Content> = ArrayList()
    val TAG = this.javaClass.name
    var onItemClick: ((Content) -> Unit)? = null
    var onItemFocused: ((Int) -> Unit?)? = null
    var onItemDpadHit: ((Int) -> Unit)? = null
    private lateinit var viewType: String
    private var previewTimer: CountDownTimer? = null

    companion object {
        private const val ALL_VIDEOS_FOR_THIS_TOPIC = 0
        private const val RELATED_TOPICS = 1
        private const val PRE_REQUISITES_FOR_THIS_TOPIC = 2
        private const val TESTS_FOR_THIS_TOPIC = 3
        private const val PRACTICES_FOR_THIS_TOPIC = 4
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder<Content> {
        return when (viewType) {
            ALL_VIDEOS_FOR_THIS_TOPIC -> {
                val binding: ItemVideosForChapterBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_videos_for_chapter,
                    parent,
                    false
                )
                VideosForChapterVH(binding)
            }
            RELATED_TOPICS,
            PRE_REQUISITES_FOR_THIS_TOPIC -> {
                val binding: ItemTopicPreRequisiteForChapterBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_topic_pre_requisite_for_chapter,
                    parent,
                    false
                )
                TopicPreRequisiteForChapterVH(binding)
            }
            TESTS_FOR_THIS_TOPIC -> {
                val binding: ItemTestForChapterBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_test_for_chapter,
                    parent,
                    false
                )
                TestForChapterVH(binding)
            }
            PRACTICES_FOR_THIS_TOPIC -> {
                val binding: ItemPracticeForChapterBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_practice_for_chapter,
                    parent,
                    false
                )
                PracticeForChapterVH(binding)
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder<*>, position: Int) {
        val item = list[position]
        when (holder) {
            is TopicPreRequisiteForChapterVH -> holder.bind(item)
            is PracticeForChapterVH -> holder.bind(item)
            is VideosForChapterVH -> holder.bind(item)
            is TestForChapterVH -> holder.bind(item)
            else -> throw IllegalArgumentException()
        }
//        holder.itemView.setOnFocusChangeListener { view, hasFocus ->
//            if (hasFocus)
//                updateViewScaling(view, 1f, 1.06f)
//            else
//                updateViewScaling(view, 1.06f, 1f)
//        }
    }

    private fun updateViewScaling(view: View, from: Float, to: Float) {
        ObjectAnimator.ofFloat(view, SCALE_X, from, to).start()
        ObjectAnimator.ofFloat(view, SCALE_Y, from, to).start()
    }

    override fun getItemViewType(position: Int): Int {
        return when (viewType) {
            mContext.getString(R.string.all_videos_for_this_topic) -> ALL_VIDEOS_FOR_THIS_TOPIC
            mContext.getString(R.string.related_topics) -> RELATED_TOPICS
            mContext.getString(R.string.pre_requisite_topics) -> PRE_REQUISITES_FOR_THIS_TOPIC
            mContext.getString(R.string.test_on_this_topic) -> TESTS_FOR_THIS_TOPIC
            mContext.getString(R.string.practice_on_this_topic) -> PRACTICES_FOR_THIS_TOPIC
            else -> ALL_VIDEOS_FOR_THIS_TOPIC
        }
    }

    fun setData(data: ArrayList<Content>, type: String) {
        list = data
        viewType = type
        notifyDataSetChanged()
    }

    abstract class ItemViewHolder<Content>(binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        abstract fun bind(item: Content)
    }

    inner class VideosForChapterVH(var binding: ItemVideosForChapterBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
            Glide.with(mContext).load(item.owner_info.copy_logo).into(binding.ivEmbibeLogo)
            Glide.with(itemView.context)
                .load(item.category_thumb)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(binding.ivCategory)

            loadThumb(item, itemView.context, itemView.ivImg)
        }

        init {
            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    previewWithADelay()
                } else {
                    previewTimer?.cancel()
                    binding.ivImg.visibility = View.VISIBLE
                    binding.ivGifView.visibility = View.INVISIBLE
                    //binding.cardView.visibility = View.INVISIBLE
                }
            }

            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }

        }

        private fun previewWithADelay() {
            previewTimer = object : CountDownTimer(2000, 1000) {
                override fun onFinish() {
                    showPreview()
                }

                override fun onTick(millisUntilFinished: Long) {}
            }.start()
        }

        private fun showPreview() {
            //binding.cardView.visibility = View.VISIBLE
            binding.ivGifView.visibility = View.VISIBLE
            val controller = Fresco.newDraweeControllerBuilder()
            controller.autoPlayAnimations = true
            controller.setUri(list[adapterPosition].preview_url)
            controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                override fun onFinalImageSet(
                    id: String?,
                    imageInfo: ImageInfo?,
                    animatable: Animatable?
                ) {
                    val anim = animatable as AnimatedDrawable2
                    anim.setAnimationListener(object : AnimationListener {
                        override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                        override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                            binding.ivImg.visibility = View.INVISIBLE
                        }

                        override fun onAnimationFrame(
                            drawable: AnimatedDrawable2?,
                            frameNumber: Int
                        ) {
                        }

                        override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                            binding.ivImg.visibility = View.VISIBLE
                            binding.ivGifView.visibility = View.INVISIBLE
                            //binding.cardView.visibility = View.INVISIBLE
                        }

                        override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                    })
                }
            }
            binding.ivGifView.controller = controller.build()
        }
    }

    private fun loadThumb(
        item: Content,
        context: Context,
        ivImg: ImageView
    ) {
        val requestOptions = RequestOptions().transform(RoundedCorners(10))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        if (item.thumb.isEmpty()) {
            Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(R.drawable.video_placeholder)
                .into(ivImg)
        } else {
            Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(item.thumb)
                .placeholder(R.drawable.video_placeholder)
                .into(ivImg)
        }
    }

    inner class PracticeForChapterVH(var binding: ItemPracticeForChapterBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.questionBooksTagTV.text = item.question_book_tag
            loadThumb(item, itemView.context, itemView.thumbPracticeForChapterIV)
            binding.detailItem = item
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(adapterPosition)
            }
        }
    }

    inner class TopicPreRequisiteForChapterVH(var binding: ItemTopicPreRequisiteForChapterBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            loadThumb(item, itemView.context, itemView.topicPrerequisiteIV)
            binding.detailItem = item
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(adapterPosition)
            }
        }
    }

    inner class TestForChapterVH(var binding: ItemTestForChapterBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
            loadThumb(item, itemView.context, binding.ivImg)
            val time = Utils.convertIntoMinsTest(item.duration).split("\\s".toRegex())
            binding.testTime.text = setTextHTML(time[0], " " + time[1].toUpperCase())
            binding.testMarks.text = setTextHTML(item.total_marks.toString(), " Marks")
            binding.testQuestions.text = setTextHTML(item.questions, " Questions")
            binding.testQualityScore.text =
                setTextHTML(item.test_quality_score + "%", " Test Quality Score")
            binding.tvTestName.text = item.title
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(adapterPosition)
            }
        }
    }

    fun setTextHTML(boldValue: String, normalValue: String): Spanned {
        val sValue = SpannableStringBuilder()
            .bold { append(boldValue) }
            .append(normalValue)
        return sValue
    }
}