package com.embibe.embibetvapp.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.RowItemCategoryBinding
import com.embibe.embibetvapp.utils.ContantUtils

class RvAdapterCategory() : RecyclerView.Adapter<RvAdapterCategory.VHCategory>() {

    lateinit var binding: RowItemCategoryBinding
    private var selectedItem = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHCategory {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_item_category,
            parent,
            false
        )
        return VHCategory(binding)
    }

    override fun getItemCount(): Int {
        return ContantUtils.categoryData.size
    }

    override fun onBindViewHolder(holder: VHCategory, position: Int) {
        holder.bind(position)
    }

    //
    inner class VHCategory(var binding: RowItemCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.tvCategory.text = ContantUtils.categoryData[position]
            binding.clItem.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    binding.tvCategory.setTextColor(Color.parseColor("#000000"))
                } else {
                    binding.tvCategory.setTextColor(Color.parseColor("#ffffff"))
                }
            }

            if (selectedItem == adapterPosition) {
                binding.clItem.setBackgroundResource(R.drawable.bg_acheive_potential_selected)
                binding.tvCategory.setTextColor(Color.parseColor("#000000"))
            } else {
                binding.clItem.setBackgroundResource(R.drawable.bg_acheive_potential_selector)
                binding.tvCategory.setTextColor(Color.parseColor("#ffffff"))
            }
            itemView.setOnClickListener {
                if (selectedItem != adapterPosition) {
                    selectedItem = adapterPosition
                    notifyDataSetChanged()
                } else {
                    selectedItem = -1
                    notifyDataSetChanged()
                }
            }
        }
    }
}