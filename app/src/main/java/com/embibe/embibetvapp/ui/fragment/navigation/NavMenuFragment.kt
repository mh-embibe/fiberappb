package com.embibe.embibetvapp.ui.fragment.navigation

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.constant.AppConstants.NAV_NAME_ACHIEVE
import com.embibe.embibetvapp.constant.AppConstants.NAV_NAME_LEARN
import com.embibe.embibetvapp.constant.AppConstants.NAV_NAME_PRACTICE
import com.embibe.embibetvapp.constant.AppConstants.NAV_NAME_SEARCH
import com.embibe.embibetvapp.constant.AppConstants.NAV_NAME_TEST
import com.embibe.embibetvapp.databinding.FragmentNavMenuBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.ui.activity.UserSwitchActivity
import com.embibe.embibetvapp.ui.interfaces.FragmentChangeListener
import com.embibe.embibetvapp.ui.interfaces.NavigationStateListener
import com.embibe.embibetvapp.ui.viewmodel.RowHeaderViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils

class NavMenuFragment : BaseAppFragment() {

    private lateinit var binding: FragmentNavMenuBinding
    private lateinit var rowHeaderViewModel: RowHeaderViewModel

    private lateinit var fragmentChangeListener: FragmentChangeListener
    private lateinit var navigationToHostListener: NavigationStateListener

    private var TAG_CLASS_NAME = NavMenuFragment::class.java.toString()
    private var lastSelectedMenu: String = NAV_NAME_LEARN
    private var searchAllowedToGainFocus = false
    private var achieveAllowedToGainFocus = false
    private var practiceAllowedToGainFocus = false
    private var learnAllowedToGainFocus = true
    private var testAllowedToGainFocus = false
    private var switchUserAllowedToGainFocus = false
    private var menuTextAnimationDelay = 0//200

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_nav_menu, container, false)
        rowHeaderViewModel = ViewModelProviders.of(this).get(RowHeaderViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //by default selection
        setMenuIconFocusView(R.drawable.ic_learn_selected, binding.learnIB, true)

        //Navigation Menu Options Focus, Key Listeners
        practiceListeners()

        searchListeners()

        learnListeners()

        achieveListeners()

        testListeners()

        switchUserListeners()
    }

    private fun switchUserListeners() {
        binding.switchUserIB.setOnClickListener {
            startActivity(Intent(context, UserSwitchActivity::class.java))
            //activity?.finish()
            closeNav()
            navigationToHostListener.onStateChanged(false, lastSelectedMenu)
        }

        binding.switchUserIB.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.dummyBar.visibility = INVISIBLE
                if (switchUserAllowedToGainFocus) {
                    binding.switchUserIB.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.navigation_menu_focus_color
                        )
                    )
                    binding.accountNameTV.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.navigation_menu_focus_color
                        )
                    )
                }
            } else {
                binding.dummyBar.visibility = GONE
                binding.switchUserIB.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.grey_1000_w
                    )
                )
                setMenuNameFocusView(binding.accountNameTV, false)
                binding.accountNameTV.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.grey_1000_w
                    )
                )
            }

        }
    }

    private fun searchListeners() {

        Log.i(
            TAG_CLASS_NAME, "searchAllowedToGainFocus = $searchAllowedToGainFocus" +
                    "is search this last selected = ${lastSelectedMenu == NAV_NAME_SEARCH}" +
                    "is search nav open = ${isNavigationOpen()}"
        )

        binding.searchIB.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                if (isNavigationOpen()) {
                    setFocusedView(binding.searchIB, R.drawable.new_search_selected)
                    setMenuNameFocusView(binding.searchTV, true)
//                    focusIn(binding.searchIB, 0)
                }
            } else {
                if (isNavigationOpen()) {
                    setOutOfFocusedView(binding.searchIB, R.drawable.new_search_unselected)
                    setMenuNameFocusView(binding.searchTV, false)
//                    focusOut(binding.searchIB, 0)
                }
            }
        }

        binding.searchIB.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {//only when key is pressed down
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        closeNav()
                        navigationToHostListener.onStateChanged(false, lastSelectedMenu)
                    }
                    KeyEvent.KEYCODE_ENTER -> {
//                        lastSelectedMenu = NAV_NAME_SEARCH
                        fragmentChangeListener.switchFragment(NAV_NAME_SEARCH)
                        closeNav()
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        if (!binding.switchUserIB.isFocusable)
                            binding.switchUserIB.isFocusable = true
                        switchUserAllowedToGainFocus = true
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER -> {
//                        lastSelectedMenu = NAV_NAME_SEARCH
                        fragmentChangeListener.switchFragment(NAV_NAME_SEARCH)
                        closeNav()
                    }
                }
            }
            false
        }
    }

    private fun learnListeners() {

        Log.i(
            TAG_CLASS_NAME, "learnAllowedToGainFocus = $learnAllowedToGainFocus" +
                    "is learn this last selected = ${lastSelectedMenu == NAV_NAME_LEARN}" +
                    "is learn nav open = ${isNavigationOpen()}"
        )

        binding.learnIB.setOnFocusChangeListener { v, hasFocus ->

            if (hasFocus) {
                if (isNavigationOpen()) {
                    setFocusedView(binding.learnIB, R.drawable.new_learn_selected)
                    setMenuNameFocusView(binding.learnTV, true)
//                    focusIn(binding.learnIB, 0)
                }
            } else {
                if (isNavigationOpen()) {
                    setOutOfFocusedView(binding.learnIB, R.drawable.new_learn_unselected)
                    setMenuNameFocusView(binding.learnTV, false)
//                    focusOut(binding.learnIB, 0)
                }
            }
        }

        binding.learnIB.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {//only when key is pressed down
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        closeNav()
                        navigationToHostListener.onStateChanged(false, lastSelectedMenu)
                    }
                    KeyEvent.KEYCODE_ENTER -> {
                        lastSelectedMenu = NAV_NAME_LEARN
                        fragmentChangeListener.switchFragment(NAV_NAME_LEARN)
                        closeNav()
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        if (!binding.switchUserIB.isFocusable)
                            binding.switchUserIB.isFocusable = true
                        switchUserAllowedToGainFocus = true
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER -> {
                        lastSelectedMenu = NAV_NAME_LEARN
                        fragmentChangeListener.switchFragment(NAV_NAME_LEARN)
                        closeNav()
                    }
                }
            }
            false
        }

    }

    private fun practiceListeners() {

        Log.i(
            TAG_CLASS_NAME, "practiceAllowedToGainFocus = $practiceAllowedToGainFocus, " +
                    "is practice this last selected = ${lastSelectedMenu == NAV_NAME_PRACTICE}" +
                    "is practice nav open = ${isNavigationOpen()}"
        )

        binding.practiceIB.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                if (isNavigationOpen()) {
                    setFocusedView(binding.practiceIB, R.drawable.new_practice_selected)
                    setMenuNameFocusView(binding.practiceTV, true)
//                    focusIn(binding.practiceIB, 0)
                }

            } else {
                if (isNavigationOpen()) {
                    setOutOfFocusedView(binding.practiceIB, R.drawable.new_practice_unselected)
                    setMenuNameFocusView(binding.practiceTV, false)
//                    focusOut(binding.practiceIB, 0)
                }
            }


            // Redraw, make the drawing order adjustment of items take effect
            val parent = v.parent as ViewGroup
            parent.requestLayout()
            parent.postInvalidate()
        }

        binding.practiceIB.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {//only when key is pressed down
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        closeNav()
                        navigationToHostListener.onStateChanged(false, lastSelectedMenu)
                    }
                    KeyEvent.KEYCODE_ENTER -> {
                        lastSelectedMenu = NAV_NAME_PRACTICE
                        fragmentChangeListener.switchFragment(NAV_NAME_PRACTICE)
                        closeNav()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER -> {
                        lastSelectedMenu = NAV_NAME_PRACTICE
                        fragmentChangeListener.switchFragment(NAV_NAME_PRACTICE)
                        closeNav()
                    }
                }
            }
            false
        }
    }

    private fun achieveListeners() {
        Log.i(
            TAG_CLASS_NAME, "achieveAllowedToGainFocus = $achieveAllowedToGainFocus, " +
                    "is achieve this last selected = ${lastSelectedMenu == NAV_NAME_ACHIEVE}" +
                    "is achieve nav open = ${isNavigationOpen()}"
        )

        binding.achieveIB.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                if (isNavigationOpen()) {
                    setFocusedView(binding.achieveIB, R.drawable.new_achieve_selected)
                    setMenuNameFocusView(binding.achieveTV, true)
//                    focusIn(binding.practiceIB, 0)
                }

            } else {
                if (isNavigationOpen()) {
                    setOutOfFocusedView(binding.achieveIB, R.drawable.new_achieve_unselected)
                    setMenuNameFocusView(binding.achieveTV, false)
//                    focusOut(binding.practiceIB, 0)
                }
            }


            // Redraw, make the drawing order adjustment of items take effect
//            val parent = v.parent as ViewGroup
//            parent.requestLayout()
//            parent.postInvalidate()
            binding.achieveIB.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {//only when key is pressed down
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_RIGHT -> {
                            closeNav()
                            navigationToHostListener.onStateChanged(false, lastSelectedMenu)
                        }
                        KeyEvent.KEYCODE_ENTER -> {
                            lastSelectedMenu = NAV_NAME_ACHIEVE
                            fragmentChangeListener.switchFragment(NAV_NAME_ACHIEVE)
                            closeNav()
                        }
                        KeyEvent.KEYCODE_DPAD_UP -> {
                            if (!binding.switchUserIB.isFocusable)
                                binding.switchUserIB.isFocusable = true
                            switchUserAllowedToGainFocus = true
                        }
                        KeyEvent.KEYCODE_DPAD_CENTER -> {
                            lastSelectedMenu = NAV_NAME_ACHIEVE
                            fragmentChangeListener.switchFragment(NAV_NAME_ACHIEVE)
                            closeNav()
                        }
                    }
                }
                false
            }

        }

        binding.achieveIB.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {//only when key is pressed down
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        closeNav()
                        navigationToHostListener.onStateChanged(false, lastSelectedMenu)
                    }
                    KeyEvent.KEYCODE_ENTER -> {
                        lastSelectedMenu = NAV_NAME_ACHIEVE
                        fragmentChangeListener.switchFragment(NAV_NAME_ACHIEVE)
                        closeNav()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER -> {
                        lastSelectedMenu = NAV_NAME_ACHIEVE
                        fragmentChangeListener.switchFragment(NAV_NAME_ACHIEVE)
                        closeNav()
                    }
                }
            }
            false
        }

    }

    private fun testListeners() {

        binding.testIB.setOnFocusChangeListener { v, hasFocus ->

            if (hasFocus) {
                if (isNavigationOpen()) {
                    setFocusedView(binding.testIB, R.drawable.ic_test_selected)
                    setMenuNameFocusView(binding.testTV, true)
//                    focusIn(binding.profileIB, 0)
                }
            } else {
                if (isNavigationOpen()) {
                    setOutOfFocusedView(binding.testIB, R.drawable.ic_test_unselected)
                    setMenuNameFocusView(binding.testTV, false)
//                    focusOut(binding.profileIB, 0)
                }
            }
        }

        binding.testIB.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {//only when key is pressed down
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        closeNav()
                        navigationToHostListener.onStateChanged(false, lastSelectedMenu)
                    }
                    KeyEvent.KEYCODE_ENTER -> {
                        lastSelectedMenu = NAV_NAME_TEST
                        fragmentChangeListener.switchFragment(NAV_NAME_TEST)
                        closeNav()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER -> {
                        lastSelectedMenu = NAV_NAME_TEST
                        fragmentChangeListener.switchFragment(NAV_NAME_TEST)
                        closeNav()
                    }
                }
            }
            false
        }
    }

    override fun onResume() {
        super.onResume()
        binding.accountNameTV.text = setFirstName(UserData.getName())
        Utils.setAvatarImage(
            requireActivity(),
            binding.userAccountIcon,
            UserData.getCurrentProfile()!!.profilePic
        )
        Utils.setAvatarImage(
            requireActivity(),
            binding.userAccountIconBig,
            UserData.getCurrentProfile()!!.profilePic
        )
    }

    private fun setFirstName(name: String): String {
        if (name != null && name.length > 12 && name.contains(" ")) {
            return name.substring(0, name.indexOf(" "))
        } else return name
    }

    private fun setOutOfFocusedView(view: ImageButton, resource: Int) {
        setMenuIconFocusView(resource, view, false)
    }

    private fun setFocusedView(view: ImageButton, resource: Int) {
        setMenuIconFocusView(resource, view, true)
    }


    /**
     * Setting animation when focus is lost
     */
    fun focusOut(v: View, position: Int) {
        val scaleX = ObjectAnimator.ofFloat(v, "scaleX", 1.05f, 1.0f)
        val scaleY = ObjectAnimator.ofFloat(v, "scaleY", 1.05f, 1.0f)
        val set = AnimatorSet()
        set.play(scaleX).with(scaleY)
        set.start()
    }

    /**
     * Setting the animation when getting focus
     */
    fun focusIn(v: View, position: Int) {
        val scaleX = ObjectAnimator.ofFloat(v, "scaleX", 1.0f, 1.05f)
        val scaleY = ObjectAnimator.ofFloat(v, "scaleY", 1.0f, 1.05f)
        val set = AnimatorSet()
        set.play(scaleX).with(scaleY)
        set.start()
    }

    private fun setMenuIconFocusView(resource: Int, view: ImageButton, inFocus: Boolean) {
        view.setImageResource(resource)
        when (view.tag) {
            getString(R.string.search) -> {
                when {
                    inFocus -> binding.rightSearchBar.visibility = VISIBLE
                    else -> binding.rightSearchBar.visibility = GONE
                }
                binding.leftSearchBar.visibility = INVISIBLE
            }
            getString(R.string.learn) -> {
                when {
                    inFocus -> binding.rightLearnBar.visibility = VISIBLE
                    else -> binding.rightLearnBar.visibility = GONE
                }
                binding.leftLearnBar.visibility = INVISIBLE
            }
            getString(R.string.practice) -> {
                when {
                    inFocus -> binding.rightPracticeBar.visibility = VISIBLE
                    else -> binding.rightPracticeBar.visibility = GONE
                }
                binding.leftPracticeBar.visibility = INVISIBLE
            }
            getString(R.string.test) -> {
                when {
                    inFocus -> binding.rightTestBar.visibility = VISIBLE
                    else -> binding.rightTestBar.visibility = GONE
                }
                binding.leftTestBar.visibility = INVISIBLE
            }
            getString(R.string.achieve) -> {
                when {
                    inFocus -> binding.rightAchieveBar.visibility = VISIBLE
                    else -> binding.rightAchieveBar.visibility = GONE
                }
                binding.leftAchieveBar.visibility = INVISIBLE
            }
        }
    }

    private fun setMenuNameFocusView(view: TextView, inFocus: Boolean) {
        if (inFocus) {
            SegmentUtils.trackEventHomeNavigationBarFocus(view.text.toString(), lastSelectedMenu)
            view.apply {
                typeface = Typeface.createFromAsset(activity?.assets, "fonts/gilroy_semibold.ttf")
                setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.navigation_menu_focus_color
                    )
                )
            }
        } else {
            view.apply {
                typeface = Typeface.createFromAsset(activity?.assets, "fonts/gilroy_regular.ttf")
                setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.navigation_menu_focus_out_color
                    )
                )
            }
        }
    }

    fun openNav() {
        SegmentUtils.trackEventHomeNavigationBarViewOpen(lastSelectedMenu)
        enableNavMenuViews(VISIBLE)
        val lp = FrameLayout.LayoutParams(WRAP_CONTENT, MATCH_PARENT)
        binding.openNavCL.layoutParams = lp
        navigationToHostListener.onStateChanged(true, lastSelectedMenu)

        when (lastSelectedMenu) {

            NAV_NAME_LEARN -> {
                binding.learnIB.requestFocus()
                learnAllowedToGainFocus = true
                setMenuNameFocusView(binding.learnTV, true)
                binding.leftLearnBar.visibility = INVISIBLE
                binding.rightLearnBar.visibility = VISIBLE
            }
            NAV_NAME_SEARCH -> {
                binding.searchIB.requestFocus()
                searchAllowedToGainFocus = true
                setMenuNameFocusView(binding.searchTV, true)
                binding.leftSearchBar.visibility = INVISIBLE
                binding.rightSearchBar.visibility = VISIBLE
            }
            NAV_NAME_PRACTICE -> {
                binding.practiceIB.requestFocus()
                practiceAllowedToGainFocus = true
                setMenuNameFocusView(binding.practiceTV, true)
                binding.leftPracticeBar.visibility = INVISIBLE
                binding.rightPracticeBar.visibility = VISIBLE
            }
            NAV_NAME_TEST -> {
                binding.testIB.requestFocus()
                testAllowedToGainFocus = true
                setMenuNameFocusView(binding.testTV, true)
                binding.leftTestBar.visibility = INVISIBLE
                binding.rightTestBar.visibility = VISIBLE
            }
            NAV_NAME_ACHIEVE -> {
                binding.achieveIB.requestFocus()
                achieveAllowedToGainFocus = true
                setMenuNameFocusView(binding.achieveTV, true)
                binding.leftAchieveBar.visibility = INVISIBLE
                binding.rightAchieveBar.visibility = VISIBLE
            }

        }

    }

    fun closeNav() {
        SegmentUtils.trackEventHomeNavigationBarView(lastSelectedMenu)
        enableNavMenuViews(GONE)
        val lp = FrameLayout.LayoutParams(WRAP_CONTENT, MATCH_PARENT)
        binding.openNavCL.layoutParams = lp

        //highlighting last selected menu icon
        highlightMenuSelection(lastSelectedMenu)

        //Setting out of focus views for menu icons, names
        unHighlightMenuSelections(lastSelectedMenu)

    }

    private fun unHighlightMenuSelections(lastSelectedMenu: String) {
        if (!lastSelectedMenu.equals(NAV_NAME_PRACTICE, true)) {
            setOutOfFocusedView(binding.practiceIB, R.drawable.new_practice_unselected)
            setMenuNameFocusView(binding.practiceTV, false)
            binding.rightPracticeBar.visibility = GONE
        }
        if (!lastSelectedMenu.equals(NAV_NAME_SEARCH, true)) {
            setOutOfFocusedView(binding.searchIB, R.drawable.new_search_unselected)
            setMenuNameFocusView(binding.searchTV, false)
            binding.rightSearchBar.visibility = GONE
        }
        if (!lastSelectedMenu.equals(NAV_NAME_LEARN, true)) {
            setOutOfFocusedView(binding.learnIB, R.drawable.new_learn_unselected)
            setMenuNameFocusView(binding.learnTV, false)
            binding.rightLearnBar.visibility = GONE
        }
        if (!lastSelectedMenu.equals(NAV_NAME_TEST, true)) {
            setOutOfFocusedView(binding.testIB, R.drawable.new_test_unselected)
            setMenuNameFocusView(binding.testTV, false)
            binding.rightTestBar.visibility = GONE

        }
        if (!lastSelectedMenu.equals(NAV_NAME_ACHIEVE, true)) {
            setOutOfFocusedView(binding.achieveIB, R.drawable.new_achieve_unselected)
            setMenuNameFocusView(binding.achieveTV, false)
            binding.rightAchieveBar.visibility = GONE
        }
    }

    private fun highlightMenuSelection(lastSelectedMenu: String) {
        when (lastSelectedMenu) {
            NAV_NAME_LEARN -> {
                setFocusedView(binding.learnIB, R.drawable.new_learn_selected)
                binding.leftLearnBar.visibility = VISIBLE
                binding.rightLearnBar.visibility = GONE
            }
            NAV_NAME_SEARCH -> {
                setFocusedView(binding.searchIB, R.drawable.new_search_selected)
                binding.leftSearchBar.visibility = VISIBLE
                binding.rightSearchBar.visibility = GONE
            }
            NAV_NAME_PRACTICE -> {
                setFocusedView(binding.practiceIB, R.drawable.new_practice_selected)
                binding.leftPracticeBar.visibility = VISIBLE
                binding.rightPracticeBar.visibility = GONE
            }
            NAV_NAME_TEST -> {
                setFocusedView(binding.testIB, R.drawable.new_test_selected)
                binding.leftTestBar.visibility = VISIBLE
                binding.rightTestBar.visibility = GONE
            }
            NAV_NAME_ACHIEVE -> {
                setFocusedView(binding.achieveIB, R.drawable.new_achieve_selected)
                binding.leftAchieveBar.visibility = VISIBLE
                binding.rightAchieveBar.visibility = GONE
            }
        }
    }

    private fun enableNavMenuViews(visibility: Int) {

        if (visibility == GONE) {
            menuTextAnimationDelay = 0//200 //reset
            binding.learnTV.visibility = visibility
            binding.searchTV.visibility = visibility
            binding.testTV.visibility = visibility
            binding.achieveTV.visibility = visibility
            binding.practiceTV.visibility = visibility
        } else {
            animateMenuNamesEntry(binding.searchTV, visibility, 1)
        }
//        if (visibility == VISIBLE) {
//            var params = binding.userAccountIconBig.layoutParams as ViewGroup.MarginLayoutParams
//            params.marginStart = 70
//            binding.userAccountIconBig.layoutParams = params
//        }
        //nav menu option names
        binding.userAccountIconBig.visibility = visibility
        if (visibility == VISIBLE)
            binding.userAccountIcon.visibility = INVISIBLE
        else
            binding.userAccountIcon.visibility = VISIBLE

        binding.switchUserIB.visibility = visibility
        binding.accountNameTV.visibility = visibility
        binding.embiumCoinCountTV2.visibility = visibility

    }

    private fun animateMenuNamesEntry(view: View, visibility: Int, viewCode: Int) {
        view.postDelayed({
            view.visibility = visibility
//            val animate = AnimationUtils.loadAnimation(App.context, R.anim.slide_in_left_menu_name)
//            view.startAnimation(animate)
            menuTextAnimationDelay = 0//50
            when (viewCode) {
                1 -> {
                    animateMenuNamesEntry(binding.learnTV, visibility, viewCode + 1)
                }
                2 -> {
                    animateMenuNamesEntry(binding.practiceTV, visibility, viewCode + 1)
                }
                3 -> {
                    animateMenuNamesEntry(binding.testTV, visibility, viewCode + 1)
                }
                4 -> {
                    animateMenuNamesEntry(binding.achieveTV, visibility, viewCode + 1)
                }
            }
        }, menuTextAnimationDelay.toLong())
    }

    fun isNavigationOpen() = binding.learnTV.visibility == VISIBLE

    fun setFragmentChangeListener(callback: FragmentChangeListener) {
        this.fragmentChangeListener = callback
    }

    fun setNavigationStateListener(callback: NavigationStateListener) {
        this.navigationToHostListener = callback
    }

    fun setSelectedMenu(navMenuName: String) {
        when (navMenuName) {
            NAV_NAME_PRACTICE -> {
                lastSelectedMenu = NAV_NAME_PRACTICE
            }
            NAV_NAME_LEARN -> {
                lastSelectedMenu = NAV_NAME_LEARN
            }
        }

        highlightMenuSelection(lastSelectedMenu)
        unHighlightMenuSelections(lastSelectedMenu)

    }


}
