package com.embibe.embibetvapp.ui.fragment.editUser

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentSigninOtpBinding
import com.embibe.embibetvapp.model.UpdateProfileRequest
import com.embibe.embibetvapp.model.auth.SignupVerifyOtpRequest
import com.embibe.embibetvapp.model.auth.SignupVerifyOtpResponse
import com.embibe.embibetvapp.model.auth.signup.SignupSendOtpRequest
import com.embibe.embibetvapp.model.auth.signup.SignupSendOtpResponse
import com.embibe.embibetvapp.model.updateProfile.Errors
import com.embibe.embibetvapp.model.updateProfile.UpdateProfileRes
import com.embibe.embibetvapp.ui.activity.EditUserActivity
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils

class UpdateUserOtpFragment : BaseAppFragment() {

    private lateinit var binding: FragmentSigninOtpBinding
    private lateinit var signInViewModel: SignInViewModel
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var updateProfileModel: UpdateProfileRequest

    private var mobileOrEmail: String? = null
    private var mobile: String? = null
    private var email: String? = null
    private var isTwoVerificationRequired: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_signin_otp, container, false)

        updateProfileModel = arguments!!.getSerializable("updateModel") as UpdateProfileRequest
        isTwoVerificationRequired = arguments!!.getBoolean("isTwoVerificationRequired")
        mobileOrEmail = arguments!!.getString("mobileOrEmail", "")

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        clearAllOTP()
        setTextWatcher()
        clickListeners(view)

        when (Utils.getInputType(mobileOrEmail!!)) {
            1 -> {
                binding.tv2.text = "Enter the OTP sent on \nyour mobile number"
                mobile = mobileOrEmail
            }
            2 -> {
                binding.tv2.text = "Enter the OTP sent on \nyour email ID"
                email = mobileOrEmail
            }
        }

        callSendOtpApi(view)
    }

    private fun clearAllOTP() {
        binding.etOtp1.text.clear()
        binding.etOtp2.text.clear()
        binding.etOtp3.text.clear()
        binding.etOtp4.text.clear()
        binding.etOtp5.text.clear()
        binding.etOtp6.text.clear()
        binding.etOtp1.requestFocus()
    }

    private fun callSendOtpApi(view: View) {
        val model = SignupSendOtpRequest(
            mobile, email, "embibe_fiber_app", "600", "2"
        )

        showProgress()
        signInViewModel.sendOtp(model, object : BaseViewModel.APICallBacks<SignupSendOtpResponse> {
            override fun onSuccess(model: SignupSendOtpResponse?) {
                hideProgress()
                if (model?.result.equals("OTP sent.")) {
                    updateUIOnSuccess(view)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                retrySendOTPApi(code, error, view)
            }
        })
    }

    private fun retrySendOTPApi(code: Int, error: String, view: View) {
        if (Utils.isApiFailed(code)) {
            Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                override fun onRetry(msg: String) {
                    callSendOtpApi(view)
                }

                override fun onDismiss() {
                }
            })
        } else {
            /*update error msg in ui*/
        }
    }

    private fun updateUIOnSuccess(view: View) {
        /*OTP sent successfully!*/
        Toast.makeText(
            view.context,
            view.context.resources.getString(R.string.otp_sent_successfully),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun clickListeners(view: View) {
        binding.btnResendOtp.setOnClickListener {
            SegmentUtils.trackResendOtpBtnClick()
            clearAllOTP()
            callSendOtpApi(view)
        }

        binding.btnVerify.setOnClickListener {
            SegmentUtils.trackLoginVerifyOtpBtnClick()
            if (getEnterOtpText().length < 6) {
                Utils.showToast(requireContext(), activity!!.getString(R.string.enter_corrent_otp))
            } else {
                /* call verify otp api here */
                verifyOTP(getEnterOtpText(), mobileOrEmail ?: "")
            }
        }

        binding.btnClearOtp.setOnClickListener {
            SegmentUtils.tracClearOtpClick()
            binding.etOtp1.text.clear()
            binding.etOtp2.text.clear()
            binding.etOtp3.text.clear()
            binding.etOtp4.text.clear()
            binding.etOtp5.text.clear()
            binding.etOtp6.text.clear()
            binding.etOtp1.requestFocus()
        }
    }

    private fun verifyOTP(otp: String, id: String) {
        val model = SignupVerifyOtpRequest(otp, id)

        showProgress()
        signInViewModel.verifyOtp(
            model, object : BaseViewModel.APICallBacks<SignupVerifyOtpResponse> {
                override fun onSuccess(model: SignupVerifyOtpResponse?) {
                    hideProgress()
                    if (model?.result.equals("Success")) {
                        verificationSuccess()
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    hideProgress()
                    verificationFailed(msg)
                }
            })
    }

    private fun verificationSuccess() {
        Toast.makeText(
            requireView().context,
            requireView().context.resources.getString(R.string.otp_verified_successfully),
            Toast.LENGTH_SHORT
        ).show()
        if (isTwoVerificationRequired) {
            callUpdateOTPFragment(updateProfileModel, updateProfileModel.mobile!!, false)
        } else {
            updateProfile(updateProfileModel)
        }
    }

    private fun verificationFailed(msg: String) {
        /* Failed*/
        Toast.makeText(
            requireView().context,
            requireView().context.resources.getString(R.string.otp_verification_failed),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun callUpdateOTPFragment(
        model: UpdateProfileRequest,
        valueToVerify: String,
        isTwoUpdateRequired: Boolean
    ) {
        val bundle = Bundle()
        bundle.putSerializable("updateModel", model)
        bundle.putSerializable("isTwoUpdateRequired", isTwoUpdateRequired)
        bundle.putString("mobileOrEmail", valueToVerify)

        val fragment = UpdateUserOtpFragment()
        fragment.arguments = bundle
        (activity as EditUserActivity).setFragment(fragment, "update_profile_otp_fragment")
    }

    fun updateProfile(model: UpdateProfileRequest) {
        signInViewModel.updateProfile(model, object : BaseViewModel.APICallBacks<UpdateProfileRes> {
            override fun onSuccess(model: UpdateProfileRes?) {
                hideProgress()
                if (model != null) {
                    if (model.success) {
                        profileUpdated(model)
                    } else {
                        failedToUpdateProfile(model.errors)
                    }
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                retryUpdateProfile(code, error, model)

            }
        })
    }

    private fun retryUpdateProfile(code: Int, error: String, model: UpdateProfileRequest) {
        if (Utils.isApiFailed(code)) {
            Utils.showError(activity, code, object : BaseViewModel.ErrorCallBacks {
                override fun onRetry(msg: String) {
                    updateProfile(model)
                }

                override fun onDismiss() {
                }
            })

        } else {
            Utils.showToast(activity!!, error)
        }
    }

    private fun profileUpdated(model: UpdateProfileRes) {
        AppConstants.ISPROFILEUPDATED = true
        (activity as EditUserActivity).finish()
    }

    private fun failedToUpdateProfile(errors: Errors?) {
        if (errors != null) {
            val email = errors.email
            val profile = errors.profile
            val mobile = errors.mobile

            if (profile.equals("Nothing to change")) {
                (activity as EditUserActivity).finish()
            } else if (mobile?.contains("has already been taken")!!) {
                showToast(getString(R.string.edit_profile_mobile_exist))
                (activity as EditUserActivity).finish()
            }
        }
    }

//    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
//        if(event?.action == KeyEvent.ACTION_DOWN) {
//            when(keyCode) {
//                KeyEvent.KEYCODE_BACK -> {
//                    childFragmentManager.popBackStack()
//                }
//            }
//        }
//        return false
//    }

    private fun setTextWatcher() {
        binding.etOtp1.addTextChangedListener(GenericTextWatcher(binding.etOtp1))
        binding.etOtp2.addTextChangedListener(GenericTextWatcher(binding.etOtp2))
        binding.etOtp3.addTextChangedListener(GenericTextWatcher(binding.etOtp3))
        binding.etOtp4.addTextChangedListener(GenericTextWatcher(binding.etOtp4))
        binding.etOtp5.addTextChangedListener(GenericTextWatcher(binding.etOtp5))
        binding.etOtp6.addTextChangedListener(GenericTextWatcher(binding.etOtp6))
    }

    private fun getEnterOtpText(): String {
        return binding.etOtp1.text.toString() + binding.etOtp2.text.toString() + binding.etOtp3.text.toString() + binding.etOtp4.text.toString() + binding.etOtp5.text.toString() + binding.etOtp6.text.toString()
    }

    private fun setBackgroundHighlight() {
        binding.etOtp1.background =
            requireContext().getDrawable(R.drawable.shape_rec_white_border_maroon)
        binding.etOtp2.background =
            requireContext().getDrawable(R.drawable.shape_rec_white_border_maroon)
        binding.etOtp3.background =
            requireContext().getDrawable(R.drawable.shape_rec_white_border_maroon)
        binding.etOtp4.background =
            requireContext().getDrawable(R.drawable.shape_rec_white_border_maroon)
        binding.etOtp5.background =
            requireContext().getDrawable(R.drawable.shape_rec_white_border_maroon)
        binding.etOtp6.background =
            requireContext().getDrawable(R.drawable.shape_rec_white_border_maroon)
    }

    private fun setBackgroundNormal() {
        binding.etOtp1.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp2.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp3.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp4.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp5.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp6.background = requireContext().getDrawable(R.drawable.shape_rec_white)
    }

    inner class GenericTextWatcher(var view: View) : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            if (binding.tvWrongMessage.visibility == View.VISIBLE)
                binding.tvWrongMessage.visibility = View.GONE
            setBackgroundNormal()
            val text = editable.toString()
            when (view.id) {
                R.id.et_otp_1 -> if (text.length == 1) binding.etOtp2.requestFocus()
                R.id.et_otp_2 -> if (text.length == 1) binding.etOtp3.requestFocus() else if (text.isEmpty()) binding.etOtp1.requestFocus()
                R.id.et_otp_3 -> if (text.length == 1) binding.etOtp4.requestFocus() else if (text.isEmpty()) binding.etOtp2.requestFocus()
                R.id.et_otp_4 -> if (text.length == 1) binding.etOtp5.requestFocus() else if (text.isEmpty()) binding.etOtp3.requestFocus()
                R.id.et_otp_5 -> if (text.length == 1) binding.etOtp6.requestFocus() else if (text.isEmpty()) binding.etOtp4.requestFocus()
                R.id.et_otp_6 -> {
                    if (text.isEmpty()) binding.etOtp5.requestFocus()
                    if (text.length == 1) {
                        binding.btnVerify.requestFocus()
                        Utils.hideKeyboardFrom(context, binding.etOtp3.rootView)
                    }
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }


}