package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.ui.viewholders.JarViewHolder
import com.embibe.videoplayer.model.JarModel

class JarListAdapter(private val context: Context, private val list: List<JarModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return JarViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model: JarModel = list[position]
        when (holder) {
            is JarViewHolder -> {
                holder.bind(context, model)
            }
        }

    }

    override fun getItemCount(): Int = list.size

}