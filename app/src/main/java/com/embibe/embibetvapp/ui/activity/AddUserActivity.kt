package com.embibe.embibetvapp.ui.activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.ui.fragment.addUser.AddUserFragment
import com.embibe.embibetvapp.utils.SegmentUtils

class AddUserActivity : BaseFragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user)
        SegmentUtils.trackAddUserScreenLoadStart()
        supportFragmentManager.beginTransaction()
            .add(R.id.fl_main_layout, AddUserFragment()).commit()
    }

    fun setFragment(fragment: Fragment, name: String) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_main_layout, fragment)
            .addToBackStack(name)
            .commit()
        SegmentUtils.trackAddUserScreenLoadEnd()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        supportFragmentManager.popBackStack()
    }
}