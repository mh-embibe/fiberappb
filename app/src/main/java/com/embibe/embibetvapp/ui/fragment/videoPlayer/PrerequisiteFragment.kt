package com.embibe.embibetvapp.ui.fragment.videoPlayer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.KeyEvent
import android.view.View
import androidx.leanback.app.RowsSupportFragment
import androidx.leanback.widget.ArrayObjectAdapter
import androidx.leanback.widget.HeaderItem
import androidx.leanback.widget.OnItemViewClickedListener
import androidx.leanback.widget.OnItemViewSelectedListener
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.ui.activity.DetailActivity
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.ui.presenter.CardPresenterSelector
import com.embibe.embibetvapp.utils.CardListRow
import com.embibe.embibetvapp.utils.GifCardView
import com.embibe.embibetvapp.utils.ShadowRowPresenterSelectorPreReq
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.loadConceptsAsync
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PrerequisiteFragment : RowsSupportFragment() {
    private lateinit var mRowsAdapter: ArrayObjectAdapter
    private lateinit var volleyRequest: RequestQueue
    private lateinit var navigationMenuCallback: NavigationMenuCallback
    private var firstElementOfFirstRow: View? = null
    private var previewUrlTimer: CountDownTimer? = null
        set(value) {
            field = value
            Log.d("First", value.toString())
        }


    fun requestFocus() {
        firstElementOfFirstRow?.postDelayed({ firstElementOfFirstRow?.requestFocus() }, 10)
    }

    fun clearFocus() {
        firstElementOfFirstRow?.postDelayed({ firstElementOfFirstRow?.clearFocus() }, 10)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mRowsAdapter =
            ArrayObjectAdapter(activity?.baseContext?.let { ShadowRowPresenterSelectorPreReq() })
        initializeListeners()
        volleyRequest = Volley.newRequestQueue(activity)
        navigationMenuCallback.navMenuToggle(false)
        // createRows(DataManager.instance.getPrerequisites() as ArrayList<Datum>)
    }

    private fun createRows(datum: ArrayList<Results>) {
        if (datum.isNotEmpty()) {
            CoroutineScope(Dispatchers.Default).launch {
                val xyz = mutableListOf<Content>()
                datum.forEach { video ->
                    xyz += video.content as MutableList
                }
                val presenterSelector = activity?.baseContext?.let { CardPresenterSelector(it) }
                val adapter = ArrayObjectAdapter(presenterSelector)
                xyz.forEachIndexed { _, video ->
                    adapter.add(video)
                }
                mRowsAdapter.add(CardListRow(HeaderItem("PreRequisite"), adapter))
            }
        }
    }

    private fun initializeListeners() {
        adapter = mRowsAdapter
        onItemViewClickedListener =
            OnItemViewClickedListener { itemViewHolder, item, rowViewHolder, row ->
                if (item is Content) {
                    if (item.type == AppConstants.SUBJECT) {
                        startActivity(Intent(context, DetailActivity::class.java))
                    } else {
                        loadConceptsAsync(item, context)
                        val intent = Intent(context, DetailActivity::class.java)
                        Utils.insertBundle(item, intent)
                        startActivity(intent)
                        //getVideoURL(VideoUtils.getVimeoVideoId(item.url).toString(), item.title, item.category)
                    }
                }
            }


        onItemViewSelectedListener =
            OnItemViewSelectedListener { itemViewHolder, item, rowViewHolder, row ->

                if (item is Content) {
                    val indexOfRow = mRowsAdapter.indexOf(row)
                    val indexOfItem =
                        ((row as CardListRow).adapter as ArrayObjectAdapter).indexOf(item)
                    val itemView = itemViewHolder.view

                    if (itemView.isFocusable && !itemView.isFocused)
                        itemView.requestFocus()

                    if (itemView is GifCardView) {
                        addDelayToPreviewUrls(item, itemView)
                    }

                    saveLastSelected(indexOfItem, indexOfRow)
                    Log.e("First", indexOfItem.toString())
                    Log.e("Second", indexOfRow.toString())



                    itemView.setOnKeyListener { v, keyCode, event ->
                        //checking only for down presses
                        if (event.action == KeyEvent.ACTION_DOWN) {
                            when (keyCode) {
                                KeyEvent.KEYCODE_DPAD_LEFT,
                                KeyEvent.KEYCODE_DPAD_RIGHT,
                                KeyEvent.KEYCODE_DPAD_DOWN -> {
                                    previewUrlTimer?.cancel()
                                    if (itemView is GifCardView) {
                                        itemView.ivVideo.controller?.animatable?.stop()
                                    }
                                }
                                KeyEvent.KEYCODE_DPAD_UP -> {
                                    previewUrlTimer?.cancel()
                                    if (itemView is GifCardView) {
                                        itemView.ivVideo.controller?.animatable?.stop()
                                    }
                                    navigationMenuCallback.navMenuToggle(false)
                                }
                            }
                        }
                        false
                    }
                }
            }
    }

    private fun addDelayToPreviewUrls(
        item: Content,
        itemView: GifCardView
    ) {
        previewUrlTimer = object : CountDownTimer(4000, 1000) {
            override fun onFinish() {
                if (item.preview_url.isNotEmpty())
                    itemView.showVideoView(item.preview_url)
            }

            override fun onTick(millisUntilFinished: Long) {}
        }
        previewUrlTimer?.start()
    }

    private fun saveLastSelected(indexOfItem: Int, indexOfRow: Int) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        with(sharedPref?.edit()) {
            this?.putInt(
                getString(com.embibe.embibetvapp.R.string.last_selected_row_item),
                indexOfItem
            )
            this?.putInt(getString(com.embibe.embibetvapp.R.string.last_selected_row), indexOfRow)
            this?.apply()
        }
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    fun setData(data: ArrayList<Results>) {
        createRows(data)
    }


}