package com.embibe.embibetvapp.ui.adapter

import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.drawable.Animatable
import android.os.CountDownTimer
import android.os.SystemClock
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.text.bold
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.*
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo
import kotlinx.android.synthetic.main.item_videos_for_chapter.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class PracticeSummaryAdapter(private val mContext: Context) :
    RecyclerView.Adapter<PracticeSummaryAdapter.ItemViewHolder<*>>() {
    var list: ArrayList<Content> = ArrayList()
    var labelsWithSequenceList: ArrayList<String> = ArrayList()
    val TAG = this.javaClass.name
    var onItemClick: ((Content) -> Unit)? = null
    var onItemFocused: ((Int) -> Unit?)? = null
    var onItemDpadHit: ((Int) -> Unit)? = null
    private lateinit var viewType: String
    private var previewTimer: CountDownTimer? = null
    var coroutineScope = CoroutineScope(Dispatchers.Main)
    var lastKeyPress: Long = 0L


    companion object {
        private const val BOOK_AVAILABLE = 0
        private const val RECOMMENDED_LEARNING = 1
        private const val TOPICS_FOR_PRACTICE = 2
        private const val TESTS_FOR_THIS_CHAPTER = 3
        private const val TYPE_GIF = 4

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder<Content> {
        return when (viewType) {
            RECOMMENDED_LEARNING -> {
                val binding: ItemPracticeRecommendedLearningBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_practice_recommended_learning,
                    parent,
                    false
                )

                return VideosForChapterVH(binding)
            }
            TOPICS_FOR_PRACTICE -> {
                val binding: ItemTopicsForPracticeBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_topics_for_practice,
                    parent,
                    false
                )
                return TopicForPracticeVH(binding)
            }
            TESTS_FOR_THIS_CHAPTER -> {
                val binding: ItemPracticeTestForChapterBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_practice_test_for_chapter,
                    parent,
                    false
                )
                return TestForChapterVH(binding)
            }
            BOOK_AVAILABLE -> {
                val binding: ItemPracticeDetailBookAvailableBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_practice_detail_book_available,
                    parent,
                    false
                )
                return BookViewHolder(binding)
            }
            TYPE_GIF -> {
                val binding: ItemRecommendedLearningGifBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_recommended_learning_gif,
                    parent,
                    false
                )
                return GifCardViewHolder(binding)
            }


            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder<*>, position: Int) {
        val item = list[position]
        when (holder) {
            is TopicForPracticeVH -> holder.bind(item)
            is BookViewHolder -> holder.bind(item)
            is VideosForChapterVH -> holder.bind(item)
            is TestForChapterVH -> holder.bind(item)
            is GifCardViewHolder -> holder.bind(item)
            else -> throw IllegalArgumentException()
        }
        if (!viewType.equals(AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING) && !viewType.equals(
                AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER
            )
        ) {
//            holder.itemView.setOnFocusChangeListener { view, hasFocus ->
//                if (hasFocus) {
//                    updateViewScaling(view, 1f, 1.06f)
//                } else {
//                    updateViewScaling(view, 1.06f, 1f)
//                }
//            }
        }

        holder.itemView.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        return@setOnKeyListener keyPressLimiter(200) {
                            if (holder.adapterPosition == list.size - 1)
                                coroutineScope.launch {
                                    delay(10)
                                    holder.itemView.requestFocus()
                                }
                        }
                    }
                }
            }

            false
        }
    }


    private fun updateViewScaling(view: View, from: Float, to: Float) {
        ObjectAnimator.ofFloat(view, View.SCALE_X, from, to).start()
        ObjectAnimator.ofFloat(view, View.SCALE_Y, from, to).start()
    }

    override fun getItemViewType(position: Int): Int {
        var item: Content = list[position]
        return when (viewType) {

            AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE -> BOOK_AVAILABLE
            AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING -> {
                return if (item.preview_url.isNullOrEmpty()) {
                    RECOMMENDED_LEARNING
                } else {
                    TYPE_GIF
                }
            }
            AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE -> TOPICS_FOR_PRACTICE
            AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER -> TESTS_FOR_THIS_CHAPTER
            else -> RECOMMENDED_LEARNING
        }
    }

    fun setData(data: ArrayList<Content>, type: String) {
        labelsWithSequenceList.clear()
        list = data
        viewType = type
        var incr = 0
        list.forEach { content ->
            if (content.type.equals(AppConstants.TYPE_VIDEO, true)) {
                incr += 1
                labelsWithSequenceList.add("${incr}. ${content.title}")
            } else {
                incr = 0
                labelsWithSequenceList.add("")
            }
        }
        notifyDataSetChanged()
    }

    abstract class ItemViewHolder<Content>(binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        abstract fun bind(item: Content)
    }

    inner class VideosForChapterVH(var binding: ItemPracticeRecommendedLearningBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
            binding.txtWastedAttempts.text = labelsWithSequenceList[list.indexOf(item)]
            Glide.with(mContext).load(item.owner_info.copy_logo).into(binding.ivEmbibeLogo)
            Glide.with(itemView.context)
                .load(item.category_thumb)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(binding.ivCategory)

            loadThumb(item, itemView.context, itemView.ivImg)
        }

        init {

            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }


        }
    }

    private fun loadThumb(
        item: Content,
        context: Context,
        ivImg: ImageView
    ) {
        val requestOptions = RequestOptions().transform(RoundedCorners(10))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        if (item.thumb.isEmpty()) {
            Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(R.drawable.video_placeholder)
                .into(ivImg)
        } else {
            Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(item.thumb)
                .placeholder(R.drawable.video_placeholder)
                .into(ivImg)
        }
    }


    inner class TestForChapterVH(var binding: ItemPracticeTestForChapterBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            //binding.detailItem = item
            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)

            Glide.with(itemView.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(item.thumb)
                .placeholder(R.drawable.practice_placeholder)
                .error(R.drawable.practice_placeholder)
                .into(binding.ivImg)

            var time = Utils.convertIntoMinsTest(item.duration).split("\\s".toRegex())
            binding.testTime.text = setTextHTML(time[0], " " + time[1].toUpperCase())
            binding.testMarks.text = setTextHTML(item.total_marks.toString(), " Marks")
            binding.testQuestions.text = setTextHTML(item.questions, " Questions")
            binding.testQualityScore.text =
                setTextHTML(item.test_quality_score + "%", " Test Quality Score")
            binding.tvTestName.text = item.title
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }
            itemView.setOnFocusChangeListener { v, hasFocus ->
//                if (hasFocus) {
//                    updateViewScaling(itemView, 1f, 1.06f)
//                    binding.ivBg.visibility = View.VISIBLE
//                    binding.btnTestFeedback.visibility = View.VISIBLE
//                } else {
//                    updateViewScaling(itemView, 1.06f, 1f)
//
//                    binding.ivBg.visibility = View.GONE
//                    binding.btnTestFeedback.visibility = View.GONE
//                }
            }
        }
    }

    inner class TopicForPracticeVH(var binding: ItemTopicsForPracticeBinding) :
        ItemViewHolder<Content>(binding) {

        override fun bind(item: Content) {
            Glide.with(itemView.context).load(item.category_thumb).into(binding.ivCategory)
            binding.textNoOfQuestions.text = item.question_book_tag

            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)


            Glide.with(itemView.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(item.thumb)
                .placeholder(R.drawable.practice_placeholder)
                .error(R.drawable.practice_placeholder)
                .into(binding.ivThumbnail)

            binding.executePendingBindings()
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }
        }
    }

    inner class BookViewHolder(var binding: ItemPracticeDetailBookAvailableBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)

            Glide.with(itemView.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(item.thumb)
                .placeholder(R.drawable.practice_placeholder)
                .error(R.drawable.practice_placeholder)
                .into(binding.ivThumbnail)



            binding.executePendingBindings()
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(adapterPosition)
            }
        }
    }

    inner class GifCardViewHolder(var binding: ItemRecommendedLearningGifBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.layout.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    previewWithADelay()
                } else {
                    previewTimer?.cancel()
                    binding.ivImg.visibility = View.VISIBLE
                    binding.ivGifView.visibility = View.INVISIBLE
//                    updateViewScaling(binding.layout, 1.06f, 1f)
                }
            }
            binding.detailItem = item
            binding.txtWastedAttempts.text = labelsWithSequenceList[list.indexOf(item)]
            Glide.with(mContext).load(item.owner_info.copy_logo).into(binding.ivEmbibeLogo)
            if (item.type != "header") {
                if (item.category_thumb.isNotEmpty()) {
                    binding.ivCategory.visibility = View.VISIBLE
                    Glide.with(itemView.context)
                        .load(item.category_thumb)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(binding.ivCategory)
                } else {
                    binding.ivCategory.visibility = View.GONE
                }

                binding.textTime.text = Utils.convertIntoMins(item.length)
            }

            binding.tvSubjectName.visibility = View.GONE

            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            if (item.thumb == "thumb" || item.thumb == "") {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            } else {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(item.thumb)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            }
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
                var item: Content? = binding.detailItem
                if (item != null) {
                    SegmentUtils.trackEventMoreInfoMenuItemTileClick(item)
                }
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(adapterPosition)
                var item: Content? = binding.detailItem
                if (item != null) {
                    SegmentUtils.trackEventMoreInfoMenuItemTileFocus(item)

                }

            }

        }

        private fun previewWithADelay() {
            previewTimer = object : CountDownTimer(4000, 1000) {
                override fun onFinish() {
                    showPreview()
                }

                override fun onTick(millisUntilFinished: Long) {}
            }.start()
        }

        private fun showPreview() {
            binding.ivGifView.visibility = View.VISIBLE
            val controller = Fresco.newDraweeControllerBuilder()
            controller.autoPlayAnimations = true
            controller.setUri(list[adapterPosition].preview_url)
            controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                override fun onFinalImageSet(
                    id: String?,
                    imageInfo: ImageInfo?,
                    animatable: Animatable?
                ) {
                    val anim = animatable as AnimatedDrawable2
                    anim.setAnimationListener(object : AnimationListener {
                        override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                        override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                            binding.ivImg.visibility = View.INVISIBLE
                        }

                        override fun onAnimationFrame(
                            drawable: AnimatedDrawable2?,
                            frameNumber: Int
                        ) {
                        }

                        override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                            binding.ivImg.visibility = View.VISIBLE
                            binding.ivGifView.visibility = View.INVISIBLE
                        }

                        override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                    })
                }
            }
            binding.ivGifView.controller = controller.build()
        }
    }


    fun setTextHTML(boldValue: String, normalValue: String): Spanned {
        val sValue = SpannableStringBuilder()
            .bold { append(boldValue) }
            .append(normalValue)
        return sValue
    }

    private fun keyPressLimiter(thresHold: Long = 1500L, action: () -> Unit): Boolean {
        if (SystemClock.elapsedRealtime() - lastKeyPress < thresHold) {
            return true
        } else {
            action.invoke()
        }

        lastKeyPress = SystemClock.elapsedRealtime()
        return false
    }

}