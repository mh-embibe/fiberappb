package com.embibe.embibetvapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.RowItemCalenderBinding
import com.embibe.embibetvapp.model.adduser.Goal
import com.embibe.embibetvapp.utils.ContantUtils

class RvAdapterCalender() : RecyclerView.Adapter<RvAdapterCalender.VHCalender>() {

    private lateinit var binding: RowItemCalenderBinding
    var onItemClick: ((String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHCalender {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_item_calender,
            parent,
            false
        )
        return VHCalender(binding)
    }

    override fun getItemCount(): Int {
        return ContantUtils.calenderData.size
    }

    override fun onBindViewHolder(holder: VHCalender, position: Int) {
        holder.bind(position)
    }

    inner class VHCalender(var binding: RowItemCalenderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.textCalender.text = ContantUtils.calenderData[position]
            binding.textCalender.setOnClickListener {
                onItemClick?.invoke(ContantUtils.calenderData[position])
            }
        }
    }
}