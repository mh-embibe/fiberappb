package com.embibe.embibetvapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.BOOK_ID
import com.embibe.embibetvapp.constant.AppConstants.CONTENT_STATUS_COMPLETED
import com.embibe.embibetvapp.constant.AppConstants.CONTENT_STATUS_STARTED
import com.embibe.embibetvapp.constant.AppConstants.CONTENT_TYPE
import com.embibe.embibetvapp.constant.AppConstants.CONTINUE_LEARNING
import com.embibe.embibetvapp.constant.AppConstants.FORMAT_ID
import com.embibe.embibetvapp.constant.AppConstants.IS_ACHIEVE
import com.embibe.embibetvapp.constant.AppConstants.IS_EMBIBE_SYLLABUS
import com.embibe.embibetvapp.constant.AppConstants.IS_RECOMMENDATION_ENABLED
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_FORMAT_NAME
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_NAME
import com.embibe.embibetvapp.constant.AppConstants.SOURCE_REFERENCE
import com.embibe.embibetvapp.constant.AppConstants.TOPIC_LEARN_PATH
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_AUTHORS
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CONCEPT_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_DESCRIPTION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SOURCE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SUBJECT
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TITLE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TOTAL_DURATION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_URL
import com.embibe.embibetvapp.databinding.ActivityVideoPlayerBinding
import com.embibe.embibetvapp.model.achieve.achieveFeedback.AchieveFeedbackRes
import com.embibe.embibetvapp.model.achieve.achieveFeedback.Data
import com.embibe.embibetvapp.model.content.ContentStatusResponse
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkResponse
import com.embibe.embibetvapp.model.prerequisites.PrerequisitesResponse
import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.embibe.embibetvapp.newmodel.Annotation
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.ui.fragment.errorAlertScreen.NextPAJAlertDialog
import com.embibe.embibetvapp.ui.fragment.videoPlayer.ExoPlayerFragment
import com.embibe.embibetvapp.ui.fragment.videoPlayer.VideoPlayerDetailControlFragment
import com.embibe.embibetvapp.ui.fragment.videoPlayer.YoutubePlayerFragment
import com.embibe.embibetvapp.ui.interfaces.*
import com.embibe.embibetvapp.ui.viewmodel.AchieveViewModel
import com.embibe.embibetvapp.ui.viewmodel.VideosViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.events.SegmentIO
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_practice.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


class VideoPlayerActivity : BaseFragmentActivity(), PlayerToControlListener,
    ErrorScreensBtnClickListener, CommonAndroidAPI.AndroidAPIOnCallListener,
    ControlToPlayerListener, VideoPlayerInterface, VoiceListener {

    private lateinit var binding: ActivityVideoPlayerBinding
    private lateinit var annotationList: HashMap<Long, Annotation>
    private lateinit var videosViewModel: VideosViewModel
    private lateinit var videoUrl: String
    private lateinit var videoTitle: String
    private lateinit var videoDescription: String
    private lateinit var videoSubject: String
    private lateinit var videoChapter: String
    private var videoAuthors: String? = ""
    private lateinit var videoType: String
    private lateinit var videoTotalDuration: String
    private lateinit var videoId: String
    private lateinit var videoConceptId: String
    private lateinit var contentType: String
    private var topicLearnPathName: String = ""
    private var learnPathName: String = ""
    private var learnPathFormatName: String = ""
    private var format_id: String = ""
    private var continueWatchingDuration: Long = 0
    private var sourceReference: String = ""
    private var isEmbibeSyllabus: Boolean = false
    private var bookId: String = ""
    private var isAchieve: Boolean = false

    private var videoControllerFragment = VideoPlayerDetailControlFragment()
    private var youtubePlayerFragment = YoutubePlayerFragment()
    private var exoPlayerFragment = ExoPlayerFragment()

    private var isLiked: Boolean = false
    private var isBookMarked: Boolean = false
    private var playerState: String = ""
    private var totalDuration = 0L
    private val PLAY = "PLAY_VIDEO"
    private val PAUSE = "PAUSE_VIDEO"
    private var isPlaying = false
    private var isRecommendation = false
    private var bookmarkRetryCount: Int = 0
    private var likeRetryCount: Int = 0
    private var achieveNextDialogIsShowing = false

    private var lastTime: Long? = null
    var annotationEnabled = true

    companion object {
        lateinit var playerFragment: Fragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        voiceListener = this
        super.onCreate(savedInstanceState)
        SegmentIO.getInstance().setVideoSessionId()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_video_player)
        videosViewModel = ViewModelProviders.of(this).get(VideosViewModel::class.java)
        init()
    }

    fun init() {
        loadData()
        setVideoControllerFragment(videoControllerFragment)
        setPlayerFragment()
        getPrerequisites(videoConceptId)
        getContentStatus()
        getBookmarkStatus()
        getLikeStatus()
    }

    private fun loadData() {
        try {
            annotationEnabled = intent.getBooleanExtra(AppConstants.ANNOTATION_DETAILS, false)
            if (annotationEnabled) {
                val annotationModel =
                    intent.extras!!.getParcelableArrayList<Annotation>(AppConstants.ANNOTATION_MODEL) as ArrayList<Annotation>
                annotationList = Utils.parseHashMapFromAnnotation(annotationModel)
            }
            videoUrl = intent.getStringExtra(VIDEO_URL)
            videoTitle = intent.getStringExtra(VIDEO_TITLE)
            videoDescription = intent.getStringExtra(VIDEO_DESCRIPTION)
            videoSubject = intent.getStringExtra(VIDEO_SUBJECT)
            videoChapter = intent.getStringExtra(VIDEO_CHAPTER)
            videoAuthors = intent.getStringExtra(VIDEO_AUTHORS)
            videoType = intent.getStringExtra(VIDEO_SOURCE)
            videoId = intent.getStringExtra(VIDEO_ID)
            videoConceptId = intent.getStringExtra(VIDEO_CONCEPT_ID)
            contentType = intent.getStringExtra(CONTENT_TYPE)
            if (intent.hasExtra(VIDEO_TOTAL_DURATION))
                videoTotalDuration = intent.getStringExtra(VIDEO_TOTAL_DURATION)
            if (intent.hasExtra(IS_RECOMMENDATION_ENABLED))
                isRecommendation = intent.getBooleanExtra(IS_RECOMMENDATION_ENABLED, false)
            if (intent.hasExtra(SOURCE_REFERENCE))
                sourceReference = intent.getStringExtra(SOURCE_REFERENCE)
            if (intent.hasExtra(CONTINUE_LEARNING))
                continueWatchingDuration =
                    intent.getStringExtra(CONTINUE_LEARNING).toLong()
            if (intent.hasExtra(IS_EMBIBE_SYLLABUS))
                isEmbibeSyllabus = intent.getBooleanExtra(IS_EMBIBE_SYLLABUS, false)
            if (intent.hasExtra(BOOK_ID))
                bookId = intent.getStringExtra(BOOK_ID)
            if (intent.hasExtra(TOPIC_LEARN_PATH))
                topicLearnPathName = intent.getStringExtra(TOPIC_LEARN_PATH)
            if (intent.hasExtra(LEARN_PATH_NAME))
                learnPathName = intent.getStringExtra(LEARN_PATH_NAME)
            if (intent.hasExtra(LEARN_PATH_FORMAT_NAME))
                learnPathFormatName = intent.getStringExtra(LEARN_PATH_FORMAT_NAME)
            if (intent.hasExtra(FORMAT_ID))
                format_id = intent.getStringExtra(FORMAT_ID)
            if (intent.hasExtra(IS_ACHIEVE))
                isAchieve = intent.getBooleanExtra(IS_ACHIEVE, false)
        } catch (e: Exception) {
            e.printStackTrace()
            makeLog("Error ** ${e.localizedMessage}")
        }

    }

    private fun setVideoControllerFragment(fragment: Fragment) {
        val bundle = Bundle()
        bundle.putString(VIDEO_TITLE, videoTitle)
        bundle.putString(VIDEO_DESCRIPTION, videoDescription)
        bundle.putString(VIDEO_SUBJECT, videoSubject)
        bundle.putString(VIDEO_CHAPTER, videoChapter)
        if (videoAuthors.isNullOrEmpty())
            bundle.putString(VIDEO_AUTHORS, "")
        bundle.putString(VIDEO_AUTHORS, videoAuthors)
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction()
            .add(binding.playerDetailControlFragment.id, videoControllerFragment)
            .commit()
    }

    private fun setPlayerFragment() {
        playerFragment = when (videoType) {
            AppConstants.PLAYER_TYPE_YOUTUBE -> {
                youtubePlayerFragment.continueWatchingDuration = continueWatchingDuration.toFloat()
                setFragment(youtubePlayerFragment)
            }
            else -> {
                exoPlayerFragment.continueWatchingDuration = continueWatchingDuration
                setFragment(exoPlayerFragment)
            }
        }
    }

    private fun setFragment(fragment: Fragment): Fragment {
        val bundle = Bundle()
        bundle.putString(VIDEO_URL, videoUrl)
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction().replace(binding.playerFragment.id, fragment)
            .commit()

        return fragment
    }


    // get API calls
    private fun getPrerequisites(conceptId: String) {
        videosViewModel.getPrerequisites(conceptId, videoId,
            object : BaseViewModel.APICallBacks<PrerequisitesResponse> {
                override fun onSuccess(model: PrerequisitesResponse?) {
                    try {
                        if (model?.data != null && model.data.content.isNotEmpty()) {
                            val arrayList = ArrayList<Results>()
                            arrayList.add(model.data)
                            videoControllerFragment.isPreRequisitePresent = true
                            videoControllerFragment.setData(arrayList)
                        } else {
                            videoControllerFragment.isPreRequisitePresent = false
                        }
                    } catch (e: Exception) {
                        Log.e("Error", e.message!!)
                        /*show Error in UI*/
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    /*ignored*/
                }
            })
    }

    private fun getContentStatus() {
        videosViewModel.getContentStatus(videoId, contentType,
            object : BaseViewModel.APICallBacks<ContentStatusResponse> {
                override fun onSuccess(model: ContentStatusResponse?) {
                    if (model?.success!! && model.data != null) {
                        val contentStatusModel = model.data
                        if (contentStatusModel?.isWatched!!) {
                            val watchedDuration = contentStatusModel.watchedDuration
                            /*updated video duration ui*/
                            updateVideo(watchedDuration, contentStatusModel.contentStatus)
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg")
                }
            })
    }

    private fun getBookmarkStatus() {
        videosViewModel.getBookmarkStatus(videoId,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                val data = dataList.filter {
                                    it.contentId.equals(videoId)
                                }
                                updateBookMarkUI(data[0].status)
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg")
                }
            })
    }

    private fun getLikeStatus() {
        videosViewModel.getLikeStatus(videoId,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                if (dataList.isNotEmpty()) {
                                    val data = dataList.filter {
                                        it.contentId.equals(videoId)
                                    }
                                    updateLikUI(data[0].status)
                                }
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg")
                }
            })
    }

    // PlayerToController interface functions
    override fun isPlaying(isPlaying: Boolean) {
        this.isPlaying = isPlaying
        videoControllerFragment.updatePlayPauseUI(isPlaying)
    }

    override fun isForwarded(isForwarded: Boolean) {
        if (isForwarded) {
            if (playerFragment is YoutubePlayerFragment) {
//                Toast.makeText(this, "Youtube player forwarded", Toast.LENGTH_SHORT).show()
            } else {
//                Toast.makeText(this, "Exo player forwarded", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun isRewinded(isRewinded: Boolean) {
        if (isRewinded) {
            if (playerFragment is YoutubePlayerFragment) {
//                Toast.makeText(this, "Youtube player rewinded", Toast.LENGTH_SHORT).show()
            } else {
//                Toast.makeText(this, "Exo player rewinded", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onVideoReady(isReady: Boolean) {
        videoControllerFragment.onVideoReady(isReady)
        if (isReady) {
            videoControllerFragment.setVideoTotalDuration()
            updateVideoStatus(getCurrentDuration(), CONTENT_STATUS_STARTED)
        }
    }

    override fun onVideoEnd(isEnd: Boolean) {
        if (isEnd) {
            updateVideoStatus(totalDuration, CONTENT_STATUS_COMPLETED)
            videoControllerFragment.onVideoEnd()
            if (isAchieve) {
                callAchieveNextSteps()
            } else {
                if (isRecommendation)
                    startVideoPlayer()
                else
                    finish()
            }
        }
    }

    private fun callAchieveNextSteps() {
        if (DataManager.instance.movePosition() == -1) {
            finish()
        } else {
            moveToNext(data = DataManager.instance.getNextFeedbackData())
        }
    }

    override fun getPlayerState(state: String) {
        playerState = state
    }

    override fun videoLoadingStart(start_position: Long, bit_rate: String) {
        SegmentUtils.trackEventVideoLoadingStart(
            start_position,
            bit_rate,
            videoUrl,
            videoTitle,
            videoType,
            videoId,
            if (::videoTotalDuration.isInitialized) videoTotalDuration else null,
            format_id, learnPathName, learnPathFormatName
        )
    }

    override fun videoLoadingEnd(bit_rate: String) {
        SegmentUtils.trackEventVideoLoadingEnd(
            bit_rate, videoUrl, videoTitle, videoType, videoId,
            if (::videoTotalDuration.isInitialized) videoTotalDuration else null,
            format_id, learnPathName, learnPathFormatName
        )
    }

    override fun videoBufferStart(
        current_position: Long, initial_resolution: String,
        change_resolution: String, current_network_state: String
    ) {
        SegmentUtils.trackEventVideoBufferStart(
            current_position, initial_resolution,
            change_resolution, current_network_state, videoUrl, videoTitle, videoType, videoId,
            if (::videoTotalDuration.isInitialized) videoTotalDuration else null
        )
        videoControllerFragment.onVideoBuffering(true)
    }

    override fun videoSeek(seek_position: Long, start: Long, end: Long, seek_mode: String) {
        SegmentUtils.trackEventVideoSeek(
            seek_position,
            start,
            end,
            seek_mode,
            videoUrl,
            videoTitle,
            videoType,
            videoId,
            if (::videoTotalDuration.isInitialized) videoTotalDuration else null,
            format_id, learnPathName, learnPathFormatName
        )
        updateVideoStatus(seek_position, AppConstants.CONTENT_STATUS_INPROGRESS)
    }

    override fun videoPlayPauseClick(
        current_position: Long, video_player_mode: String,
        current_play_state: String, audio_bitrate: String, video_bitrate: String
    ) {
        SegmentUtils.trackEventVideoPlayPauseClick(
            current_position,
            video_player_mode,
            current_play_state,
            audio_bitrate,
            video_bitrate,
            videoUrl,
            videoTitle,
            videoType,
            videoId,
            if (::videoTotalDuration.isInitialized) videoTotalDuration else null,
            format_id, learnPathName, learnPathFormatName
        )
    }

    override fun videoBufferEnd(
        current_position: Long, change_resolution: String, current_network_state: String
    ) {
        SegmentUtils.trackEventVideoBufferEnd(
            current_position,
            change_resolution,
            current_network_state,
            videoUrl,
            videoTitle,
            videoType,
            videoId,
            if (::videoTotalDuration.isInitialized) videoTotalDuration else null,
            format_id, learnPathName, learnPathFormatName
        )
        videoControllerFragment.onVideoBuffering(false)
    }

    override fun videoVolume(
        current_position: Long, current_volume: Int, current_play_state: String,
        volume_change: String
    ) {
        SegmentUtils.trackEventVideoVolume(
            current_position,
            current_volume,
            current_play_state,
            volume_change,
            videoUrl,
            videoTitle,
            videoType,
            videoId,
            if (::videoTotalDuration.isInitialized) videoTotalDuration else null,
            format_id, learnPathName, learnPathFormatName
        )
    }


    // ControlToPlayerListener interface functions
    override fun playPause() {
        if (playerFragment is YoutubePlayerFragment) youtubePlayerFragment.playPause()
        else exoPlayerFragment.playPause()
    }

    private fun playPause(isPlaying: Boolean, duration: Long) {
        Log.i("playing state", isPlaying.toString())
        when (isPlaying) {
            true -> {
                if (playerFragment is YoutubePlayerFragment) {
                    (playerFragment as YoutubePlayerFragment).play()
                    videoControllerFragment.updatePlayPauseUI(isPlaying)
                } else {
                    (playerFragment as ExoPlayerFragment).play()
                    videoControllerFragment.updatePlayPauseUI(isPlaying)
                }
                videoPlayPauseClick(
                    duration,
                    AppConstants.PLAYER_FULLSCREEN,
                    AppConstants.PLAYER_PLAY,
                    "",
                    ""
                )
            }
            false -> {
                if (playerFragment is YoutubePlayerFragment) {
                    (playerFragment as YoutubePlayerFragment).pause()
                    videoControllerFragment.updatePlayPauseUI(isPlaying)
                } else {
                    (playerFragment as ExoPlayerFragment).pause()
                    videoControllerFragment.updatePlayPauseUI(isPlaying)
                }
                videoPlayPauseClick(
                    duration,
                    AppConstants.PLAYER_FULLSCREEN,
                    AppConstants.PLAYER_PAUSE,
                    "",
                    ""
                )
            }
        }
    }

    override fun rewind10Seconds() {
        if (playerFragment is YoutubePlayerFragment) youtubePlayerFragment.seekRewind10Second()
        else exoPlayerFragment.seekRewind10Second()
    }

    override fun forward10Seconds() {
        if (playerFragment is YoutubePlayerFragment) youtubePlayerFragment.seekForward10Second()
        else exoPlayerFragment.seekForward10Second()
    }

    override fun rewindByPercentage(seconds: Int) {
        if (playerFragment is YoutubePlayerFragment)
            youtubePlayerFragment.seekRewindByPercentage(seconds)
        else exoPlayerFragment.seekRewindByPercentage(seconds)
    }

    override fun forwardByPercentage(seconds: Int) {
        if (playerFragment is YoutubePlayerFragment)
            youtubePlayerFragment.seekForwardByPercentage(seconds)
        else exoPlayerFragment.seekForwardByPercentage(seconds)
    }

    override fun bookMark() {
        isBookMarked = !isBookMarked
        updateBookMarkUI(isBookMarked)
        Utils.isLikeBookmarkUpdatedForQuickLinks = true
        SegmentUtils.trackEventVideoBookmark(
            0,
            playerState,
            isBookMarked,
            videoUrl,
            videoTitle,
            videoType,
            videoId,
            if (::videoTotalDuration.isInitialized) videoTotalDuration else null,
            format_id, learnPathName, learnPathFormatName
        )

        timerBookmark.cancel()
        timerBookmark = Timer()
        timerBookmark.schedule(object : TimerTask() {
            override fun run() {
                updateBookmark()
            }
        }, interval)
    }

    override fun bookMarkFocus() {
        SegmentUtils.trackEventVideoBookmarkFocus(
            0,
            playerState,
            isBookMarked,
            videoUrl,
            videoTitle,
            videoType,
            videoId,
            if (::videoTotalDuration.isInitialized) videoTotalDuration else null
        )
    }

    override fun likeFocus() {
        SegmentUtils.trackEventVideoLikeFocus(
            0,
            playerState,
            isLiked,
            videoUrl,
            videoTitle,
            videoType,
            videoId,
            if (::videoTotalDuration.isInitialized) videoTotalDuration else null
        )
    }

    override fun like() {
        isLiked = !isLiked
        updateLikUI(isLiked)
        SegmentUtils.trackEventVideoLike(
            0,
            playerState,
            isLiked,
            videoUrl,
            videoTitle,
            videoType,
            videoId,
            if (::videoTotalDuration.isInitialized) videoTotalDuration else null
        )

        timerLike.cancel()
        timerLike = Timer()
        timerLike.schedule(object : TimerTask() {
            override fun run() {
                updateLike()
            }
        }, interval)
    }

    override fun getCurrentDuration(): Long {
        val duration = if (playerFragment is YoutubePlayerFragment) {
            youtubePlayerFragment.currentDuration.toLong()
        } else exoPlayerFragment.getCurrentPosition()
        return duration
    }

    fun checkAnnotation(duration: Long): Boolean {
        if (lastTime == null || lastTime != duration) {
            /*  if (duration == 6L || duration == 10L) {
                  lastTime = duration
                  return true
              }*/
            for (annotateTime in annotationList.keys) {
                if (annotateTime == duration /*&& (annotationList.get(annotateTime))!!.status == true*/) {
                    lastTime = duration
                    return true
                }
            }
        }
        return false
    }

    override fun annotationPoint(duration: Long) {
        playPause(false, duration)
        var listener: VideoPlayerInterface = this
        showLoader()
        Utils.showAlertDialog(
            AppConstants.PRACTICE_DIALOG, supportFragmentManager,
            listener, null, annotationList.get(duration)!!.elementId!!, videoConceptId
        )

    }

    override fun getVideoTotalDuration(): Long {
        totalDuration = if (playerFragment is YoutubePlayerFragment) {
            youtubePlayerFragment.totalDuration.toLong()
        } else exoPlayerFragment.getDuration()
        return totalDuration
    }

    // update API calls
    private fun updateVideoStatus(position: Long, status: String) {
        videosViewModel.updatedContentStatus(
            videoId,
            "Video",
            position.toInt(),
            status,
            topicLearnPathName, learnPathName, learnPathFormatName, format_id,
            videoConceptId,
            videoTotalDuration
        )
    }

    private fun updateBookmark() {
        videosViewModel.bookmark(videoId, contentType, isBookMarked,
            object : BaseViewModel.APICallBacks<CommonApiResponse> {
                override fun onSuccess(model: CommonApiResponse?) {
                    if (model?.success!!) {
                        Utils.isLikeBookmarkUpdatedForQuickLinks = true
                        updateBookMarkUI(isBookMarked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (bookmarkRetryCount < retryAttempt) {
                        bookmarkRetryCount++
                        updateBookmark()
                    } else {
                        /*reset retry count*/
                        bookmarkRetryCount = 0
                        /*revert back to previous state of ui */
                        updateBookMarkUI(!isBookMarked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_BOOKMARK)
                    }
                }
            })
    }

    private fun updateLike() {
        videosViewModel.like(videoId, contentType, isLiked,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!!) {
                        Utils.isLikeBookmarkUpdatedForQuickLinks = true
                        updateLikUI(isLiked)
                    }

                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (likeRetryCount < retryAttempt) {
                        likeRetryCount++
                        updateLike()
                    } else {
                        /*reset retry count*/
                        likeRetryCount = 0
                        /*revert back to previous state of ui */
                        updateLikUI(!isLiked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_LIKE)
                    }
                }
            })
    }

    private fun updateBookMarkUI(bookmarked: Boolean?) {
        isBookMarked = bookmarked!!
        makeLog("Bookmark $isBookMarked")
        videoControllerFragment.updateBookmarkUI(bookmarked)
    }

    private fun updateLikUI(liked: Boolean) {
        isLiked = liked
        makeLog("Like $isBookMarked")
        videoControllerFragment.updateLikeUI(liked)
    }

    private fun updateVideo(watchedDuration: Long, contentStatus: String) {

    }


    override fun onAttachFragment(fragment: Fragment) {

        when (fragment) {
            is YoutubePlayerFragment -> {
                fragment.setPlayerToControllerListener(this)
            }
            is ExoPlayerFragment -> {
                fragment.setPlayerToControllerListener(this)
            }
            is VideoPlayerDetailControlFragment -> {
                fragment.setControllerToPlayerListener(this)
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (videoControllerFragment.isControllerShown) {
            when (keyCode) {
                KeyEvent.KEYCODE_DPAD_UP, KeyEvent.KEYCODE_DPAD_DOWN, KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_DPAD_LEFT, KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                    videoControllerFragment.restartTimer()
                    return false
                }
            }
        }

        when (keyCode) {
            KeyEvent.KEYCODE_BACK, KeyEvent.KEYCODE_ESCAPE -> {
                onBackPressed()
                return false
            }
            KeyEvent.KEYCODE_MEDIA_REWIND -> {
                rewind10Seconds()
                return false
            }
            KeyEvent.KEYCODE_MEDIA_FAST_FORWARD -> {
                forward10Seconds()
                return false
            }
        }
        return false
    }

    override fun onBackPressed() {
        Utils.showAlertDialog(
            AppConstants.VIDEO_CLOSE_DIALOG, supportFragmentManager,
            this
        )
        playPause(false, videoControllerFragment.currentDuration)
    }


    override fun videoPlaybackError(
        current_position: Long,
        player_error: String
    ) {
        SegmentUtils.trackEventVideoPlaybackError(current_position, player_error)
    }

    override fun videoExit(
        current_position: Long,
        current_play_state: String,
        current_network_state: String
    ) {
        SegmentUtils.trackEventVideoExit(
            current_position,
            current_play_state,
            current_network_state
        )
        if (totalDuration != 0L) {
            if (current_position == totalDuration) {
                updateVideoStatus(
                    current_position,
                    CONTENT_STATUS_COMPLETED
                )
            } else {
                updateVideoStatus(
                    current_position,
                    AppConstants.CONTENT_STATUS_INPROGRESS
                )
            }
        }

    }

    override fun screenUpdate(type: String) {
        when (type) {
            AppConstants.CLOSE_VIDEO -> {
                if (playerFragment is YoutubePlayerFragment) {
                    var playerFragment = playerFragment as YoutubePlayerFragment
                    DataManager.instance.setLastSeek(
                        videoId,
                        playerFragment.currentDuration.toLong()
                    )
                } else {
                    var playerFragment = playerFragment as ExoPlayerFragment
                    DataManager.instance.setLastSeek(videoId, playerFragment.getCurrentPosition())
                }
                videoControllerFragment.closeHandler()
                finish()
            }
            AppConstants.CANCEL -> {
                if (!Utils.practiceDialogFragment.isVisible)
                    playPause(true, videoControllerFragment.currentDuration)
            }
            AppConstants.ON_BACK_PRESS_DIALOG_FRAGMENT -> {
                if (!Utils.practiceDialogFragment.isVisible)
                    playPause(true, videoControllerFragment.currentDuration)
            }
        }
    }


    private fun startVideoPlayer() {
        val intent = Intent(App.context, RecommendationActivity::class.java)
        intent.putExtra(VIDEO_URL, videoUrl)
        intent.putExtra(VIDEO_ID, videoId)
        intent.putExtra(VIDEO_CONCEPT_ID, videoConceptId)
        intent.putExtra(CONTENT_TYPE, contentType)
        intent.putExtra(VIDEO_TITLE, videoTitle)
        intent.putExtra(VIDEO_DESCRIPTION, videoDescription)
        intent.putExtra(VIDEO_SUBJECT, videoSubject)
        intent.putExtra(VIDEO_CHAPTER, videoChapter)
        intent.putExtra(VIDEO_AUTHORS, videoAuthors)
        intent.putExtra(TOPIC_LEARN_PATH, topicLearnPathName)
        intent.putExtra(
            VIDEO_TOTAL_DURATION,
            if (::videoTotalDuration.isInitialized) videoTotalDuration else ""
        )
        intent.putExtra(VIDEO_SOURCE, videoType)
        intent.putExtra(IS_EMBIBE_SYLLABUS, isEmbibeSyllabus)
        intent.putExtra(BOOK_ID, bookId)
        intent.putExtra(FORMAT_ID, format_id)
        intent.putExtra(LEARN_PATH_NAME, learnPathName)
        intent.putExtra(LEARN_PATH_FORMAT_NAME, learnPathFormatName)

        startActivity(intent)
        finish()
    }

    //practice listener function


    override val question: String?
        get() = ""

    override fun getQuestion(questionCode: String?): String? {
        return ""
    }

    override val concept: String?
        get() = ""
    override val hint: String?
        get() = ""

    override fun setAnswer(): String? {
        return ""
    }

    override fun getSections(): String? {
        return ""
    }

    override fun getAnswer(): String? {
        return ""
    }

    override fun getQuestionSet(sectionId: String?): String? {
        return ""
    }

    override fun getData(): String? {
        return ""
    }

    override fun getData(data: String) {
        try {
            var obj = JSONObject(data)
            var type = obj.getString(AppConstants.DETAILS_TYPE)
            when (type.toLowerCase()) {
                AppConstants.PRACTICE -> {
                    var data = obj.getJSONObject(AppConstants.DATA)
                    var content_id = data.getString(AppConstants.PRACTICE_ID)
                    var status = data.getString(AppConstants.STATUS)
                    if (status.toUpperCase() == CONTENT_STATUS_COMPLETED || status.toUpperCase() == AppConstants.CONTENT_STATUS_FINISHED) {
                        status = CONTENT_STATUS_COMPLETED
                    }
                    if (status.toUpperCase() == CONTENT_STATUS_COMPLETED) {
                        closeLoader()
                        closeDialog()
                    }
                }

                AppConstants.ERROR -> {
                    var errorType = obj.getString(AppConstants.ERROR_TYPE)
                    closePracticeDialog()
                    when (errorType.toLowerCase()) {
                        AppConstants.ERROR_SERVER_NOT_REACHABLE -> {
                            Utils.showAlertDialog(
                                AppConstants.ERROR_SERVER_NOT_REACHABLE, supportFragmentManager,
                                this
                            )
                        }
                    }
                    Log.e("Error", obj.toString())
                }
            }
        } catch (ex: JSONException) {
            Log.e("Error", ex.toString())
        }
    }

    private fun closePracticeDialog() {
        if (Utils.practiceDialogFragment.isVisible)
            Utils.practiceDialogFragment.dismiss()
    }

    override fun getEvent(data: String) {
        try {
            val obj = JSONObject(data)
            val type = obj.getString(AppConstants.DETAILS_TYPE)
            when (type.toLowerCase(Locale.getDefault())) {
                AppConstants.EVENT -> {
                    val dataJSONObject = obj.getJSONObject(AppConstants.DATA)
                    if (dataJSONObject.has("extra_params") && !dataJSONObject.isNull("extra_params")) {
                        val extraParams = dataJSONObject.getJSONObject("extra_params")
                        extraParams.put(AppConstants.CONTENT_ID, videoConceptId)
                        dataJSONObject.put("extra_params", extraParams)
                    } else {
                        val extraParams = JSONObject()
                        extraParams.put(AppConstants.CONTENT_ID, videoConceptId)
                        dataJSONObject.put("extra_params", extraParams)
                    }
                    SegmentUtils.trackPracticeEvent(dataJSONObject)
                }
            }

        } catch (ex: JSONException) {
            Log.e("error", ex.toString())
        }
    }

    override fun sendToFeedback(testCode: String) {
        TODO("Not yet implemented")
    }

    override fun showLoader() {
        if (!DataManager.isLoading)
            showProgress()
    }

    override fun closeLoader() {
        if (DataManager.isLoading)
            hideProgress()
    }

    override fun hideKeyboard() {
        closeKeyBoard(binding.root)
    }

    override fun focusUp() {
        TODO("Not yet implemented")
    }

    override fun focusDown() {
        TODO("Not yet implemented")
    }

    override fun focusRight() {
        TODO("Not yet implemented")
    }

    override fun focusLeft() {
        TODO("Not yet implemented")
    }

    override fun initVoiceInput(value: String) {
        runOnUiThread {
            startRecognition()
        }
    }

    override fun getSelectedQuestion(qListJson: String) {
        TODO("Not yet implemented")
    }

    override fun setQuestion(singQuestionJson: String) {
        TODO("Not yet implemented")
    }

    override fun getSelectedVideo(videoData: String) {

    }

    override fun selectAnswer(value: String?) {

    }

    override fun deselectAnswer(value: String?) {

    }

    override fun inputAnswer(value: String?) {

    }

    override fun close() {
        webView.destroy()
        finish()
    }

    private fun closeDialog() {
        Utils.closeDialog(AppConstants.PRACTICE_DIALOG)
        playPause(true, videoControllerFragment.currentDuration)
    }

    override val questionNumber: Int
        get() = 0
    override val sectionName: String?
        get() = ""
    override val chapterJson: String?
        get() = ""
    override val chapterName: String?
        get() = ""
    override val singQuestionJson: String?
        get() = TODO("Not yet implemented")
    override val questionListJson: String?
        get() = TODO("Not yet implemented")

    override fun showConcept(): Boolean? {
        return true
    }

    override fun backPressed() {
        Utils.showAlertDialog(
            AppConstants.VIDEO_CLOSE_DIALOG, supportFragmentManager,
            this
        )
    }

    override val androidAPIOnCallListener: CommonAndroidAPI.AndroidAPIOnCallListener
        get() = this

    override fun onVoiceInit() {

    }

    override fun onVoiceSuccess(data: String) {
        Utils.dialogWebViewAction(data)
    }

    override fun onVoiceFailed(msg: String) {
        Utils.dialogWebViewAction("")
    }

    override fun onVoiceCancel(msg: String) {
        Utils.dialogWebViewAction("")
    }


    override fun movePAJNextActivity(data: String) {

    }

    private fun moveToNext(data  : Data?) {
        getPAJ(data)
    }

    /*load getPAJ from api*/
    fun getPAJ(data  : Data?) {
        if (DataManager.instance.getTestPAJ()!=null) {
            val testPAJData =
                Gson().fromJson(DataManager.instance.getTestPAJ(), AchieveFeedbackRes::class.java)
            if (testPAJData != null) {
                val achieveViewModel = ViewModelProviders.of(this).get(AchieveViewModel::class.java)
                achieveViewModel.getAchieveFeedbackRes(testPAJData.bundleCode?:"",testPAJData.pajId?:"",object :
                    BaseViewModel.APICallBacks<AchieveFeedbackRes> {

                    override fun onSuccess(model: AchieveFeedbackRes?) {
                        hideProgress()
                        if (model != null) {
                            DataManager.instance.setTestPAJ(Gson().toJson(model))
                            supportFragmentManager.let {
                                val nextPAJDialog: DialogFragment =
                                    NextPAJAlertDialog(this@VideoPlayerActivity, AppConstants.VIDEO, data!!)
                                nextPAJDialog.show(supportFragmentManager, "nextPAJDialog")
                            }
                        }
                    }

                    override fun onFailed(code: Int, error: String, msg: String) {
                        hideProgress()
                        if (Utils.isApiFailed(code)) {
                            Utils.showError(this@VideoPlayerActivity, code, object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: String) {
                                }

                                override fun onDismiss() {}
                            })
                        } else {
                            //showToast(error)
                        }
                    }
                })
            }else{
                supportFragmentManager.let {
                    val nextPAJDialog: DialogFragment =
                        NextPAJAlertDialog(this@VideoPlayerActivity, AppConstants.VIDEO, data!!)
                    nextPAJDialog.show(supportFragmentManager, "nextPAJDialog")
                }
            }
        }

    }


}

