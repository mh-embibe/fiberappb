package com.embibe.embibetvapp.ui.fragment.addUser


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.embibe.app.utils.Resource
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentEditUserBinding
import com.embibe.embibetvapp.model.UpdateProfileRequest
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.auth.signup.UserExistResponse
import com.embibe.embibetvapp.model.updateProfile.Errors
import com.embibe.embibetvapp.model.updateProfile.UpdateProfileRes
import com.embibe.embibetvapp.ui.activity.AddGoalsExamsActivity
import com.embibe.embibetvapp.ui.activity.EditUserActivity
import com.embibe.embibetvapp.ui.fragment.editUser.UpdateUserOtpFragment
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.ContantUtils
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Status
import com.embibe.embibetvapp.utils.Utils
import com.embibejio.coreapp.model.LinkedProfile
import com.google.gson.Gson

class EditUserFragment : BaseAppFragment() {

    private lateinit var binding: FragmentEditUserBinding
    private lateinit var signInViewModel: SignInViewModel
    private lateinit var linkedProfile: LinkedProfile

    private var userType: String = ""
    private var email: String = ""
    private var username: String = ""
    private var mobile: String = ""
    private var avatarPosition: String = ContantUtils.mImgIds[0]
    private var userExistsType = "email"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        linkedProfile = Gson().fromJson(
            requireArguments().getString(AppConstants.USER_DATA)!!, LinkedProfile::class.java
        )
        userType = requireArguments().getString(AppConstants.USER_TYPE)!!
        signInViewModel =
            ViewModelProviders.of(activity as EditUserActivity).get(SignInViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_user, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setTextWatcher()
        setUserData()

        registerContinueBtn()
        registerCancelBtn()
        registerAvatarObserver()
        registerChooseAvatarBtn()
        binding.btnContinue.requestFocus()
    }

    private fun setTextWatcher() {
        binding.etName.addTextChangedListener(GenericTextWatcher(binding.etName))
        binding.etEmail.addTextChangedListener(GenericTextWatcher(binding.etEmail))
        binding.etMobile.addTextChangedListener(GenericTextWatcher(binding.etMobile))
    }

    private fun setUserData() {
        binding.btnContinue.text = resources.getString(R.string.update)
        if (userType == "parent") {
            binding.etName.setText(linkedProfile.firstName)
            binding.etMobile.setText(linkedProfile.mobile)
            binding.etEmail.setText(linkedProfile.email)
            Utils.setAvatarImage(requireActivity(), binding.ivAvatar, linkedProfile.profilePic!!)


            if (linkedProfile.uid!! == linkedProfile.mobile!!) {
                restrictMobileUpdate()
            }

            if (linkedProfile.uid!! == linkedProfile.email) {
                restrictEmailUpdate()
            }
        } else {
            binding.etName.setText(linkedProfile.firstName)
            binding.etEmail.setText(linkedProfile.email)
            Utils.setAvatarImage(requireActivity(), binding.ivAvatar, linkedProfile.profilePic)
            if (binding.etMobile.visibility == View.VISIBLE) binding.etMobile.visibility = View.GONE
            if (binding.tvMobile.visibility == View.VISIBLE) binding.tvMobile.visibility = View.GONE
            if (linkedProfile.uid!! == linkedProfile.email) {
                restrictEmailUpdate()
            }
            binding.btnChangeGoals.setOnClickListener {
                startActivity(
                    AddGoalsExamsActivity.getAddGoalsExamIntent(
                        requireContext(), linkedProfile.userId, linkedProfile.embibe_token
                    )
                )
            }
        }
    }

    private fun restrictEmailUpdate() {
        if ((userType.equals("child", true) || userType.equals("student", true))) {
            binding.etEmail.keyListener = null
            binding.etEmail.isEnabled = false
            binding.btnContinue.nextFocusUpId = binding.etName.id
            binding.btnCancel.nextFocusUpId = binding.etName.id
            binding.etName.nextFocusDownId = binding.btnContinue.id
        } else {
            binding.etEmail.keyListener = null
            binding.etEmail.isEnabled = false
            binding.etMobile.nextFocusUpId = binding.etName.id
            binding.etName.nextFocusDownId = binding.etMobile.id
        }
        binding.tvValidEmail.visibility = View.VISIBLE
        binding.tvValidEmail.text = resources.getString(R.string.non_editable_email)
    }

    private fun restrictMobileUpdate() {
        binding.etMobile.keyListener = null
        binding.etMobile.isEnabled = false
        binding.etEmail.nextFocusUpId = binding.etName.id
        binding.etEmail.nextFocusDownId = binding.btnContinue.id
        binding.etName.nextFocusDownId = binding.etEmail.id
        binding.btnContinue.nextFocusUpId = binding.etEmail.id
        binding.btnCancel.nextFocusUpId = binding.etEmail.id
        binding.tvInvalidMobile.visibility = View.VISIBLE
        binding.tvInvalidMobile.text = resources.getString(R.string.non_editable_mobile)
    }

    private fun registerChooseAvatarBtn() {
        binding.btnChooseAvatar.setOnClickListener {
            signInViewModel.setDefaultAvatar(getDefaultAvatar(linkedProfile.profilePic!!))
            (activity as EditUserActivity).setFragment(
                SelectAvatarFragment(), "select_avatar_fragment"
            )
        }
    }

    private fun getDefaultAvatar(profilePic: String): Int {
        for (i in ContantUtils.mImgIds.indices) {
            if (ContantUtils.mImgIds[i].equals(profilePic)) return i
        }
        return 0
    }

    private fun registerCancelBtn() {
        binding.btnCancel.setOnClickListener {
            SegmentUtils.trackAddUserCancelClick()
            (activity as EditUserActivity).finish()
        }
    }


    private fun registerContinueBtn() {
        binding.btnContinue.setOnClickListener {
            username = binding.etName.text.toString().trim()
            email = binding.etEmail.text.toString().trim()
            mobile = binding.etMobile.text.toString().trim()

            if (username.length < 3) {
                binding.tvValidName.visibility = View.VISIBLE
                binding.etName.background =
                    requireContext().getDrawable(R.drawable.ic_input_field_error)
            }
            if (email.length < 10 || !Utils.isEmailValid(email)) {
                binding.tvValidEmail.visibility = View.VISIBLE
                binding.etEmail.background =
                    requireContext().getDrawable(R.drawable.ic_input_field_error)
            }
            if (userType.equals("parent", true) && mobile.length < 10) {
                binding.tvInvalidMobile.visibility = View.VISIBLE
                binding.etMobile.background =
                    requireContext().getDrawable(R.drawable.ic_input_field_error)
            }

            checkUserAlreadyExist()
            registerUserExistObserver()
            SegmentUtils.trackAddUserScreenAddClick(username, userType, email, mobile, false)
            SegmentUtils.trackAddUserContinueClick()
            SegmentUtils.trackAddUserRoleClick(userType)
        }
    }

    private fun registerAvatarObserver() {
        signInViewModel.selectAvatarLiveData.observe(activity as EditUserActivity, Observer { t ->
            avatarPosition = t
            if (t != null)
                Utils.setAvatarImage(requireActivity(), binding.ivAvatar, avatarPosition)
        })
    }

    private fun checkUserAlreadyExist() {
        if (userType.equals("parent", true) &&
            username.length >= 3 && email.length >= 10 && Utils.isEmailValid(email) && mobile.length == 10
        ) {
            if (!linkedProfile.email.equals(email)) {
                signInViewModel.getUserExist(email)
                userExistsType = "email"
            }

            if (!linkedProfile.mobile.equals(mobile)) {
                signInViewModel.getUserExist(mobile)
                userExistsType = "mobile"
            } else actionAsPerUserType()
        }

        if ((userType.equals("child", true) || userType.equals("student", true)) &&
            username.length >= 3 && email.length >= 10 && Utils.isEmailValid(email)
        ) {
            if (!linkedProfile.email.equals(email)) {
                signInViewModel.getUserExist(email)
                userExistsType = "email"
            } else actionAsPerUserType()
        }
    }

    private fun registerUserExistObserver() {
        signInViewModel.userExistLiveData.observe(viewLifecycleOwner, Observer { resource ->
            if (resource != null) {
                when (resource.status) {
                    Status.LOADING -> showProgress()
                    Status.SUCCESS -> {
                        hideProgress()
                        val response = resource.data as UserExistResponse
                        if (!response.userExists) {
                            actionAsPerUserType()
                        } else {
                            if (userExistsType == "email") {
                                binding.tvValidEmail.visibility = View.VISIBLE
                                binding.tvValidEmail.text = getString(R.string.email_already_taken)
                                binding.etEmail.background =
                                    requireContext().getDrawable(R.drawable.ic_input_field_error)
                            } else if (userExistsType == "mobile") {
                                binding.tvInvalidMobile.visibility = View.VISIBLE
                                binding.tvInvalidMobile.text =
                                    getString(R.string.mobile_already_taken)
                                binding.etMobile.background =
                                    requireContext().getDrawable(R.drawable.ic_input_field_error)
                            }
                        }

                    }

                    Status.ERROR -> {
                        hideProgress()
                        retryUserExists(resource)
                    }
                }
            }
        })
    }

    private fun retryUserExists(resource: Resource<UserExistResponse>) {
        if (Utils.isApiFailed(resource)) {
            Utils.showError(context, resource, object : BaseViewModel.ErrorCallBacks {
                override fun onRetry(msg: String) {
                    checkUserAlreadyExist()
                }

                override fun onDismiss() {
                }
            })
        } else {
            /*update error msg in ui*/
        }
    }

    private fun actionAsPerUserType() {
        if (userType.equals("parent", true)) {
            updateProfileForParent()
        } else if (userType.equals("child", true)
            || userType.equals("student", true)
        ) {
            updateProfileForChildStudent()
        }
    }

    private fun updateProfileForParent() {
        val model = UpdateProfileRequest()
        if (linkedProfile.email.equals(email) && linkedProfile.mobile.equals(mobile)) {

            checkForNameAndAvatar(model)
            updateProfile(model)
        } else {
            // email, name, avatar changed not mobile
            if (!linkedProfile.email.equals(email) && linkedProfile.mobile.equals(mobile)) {

                model.email = email
                checkForNameAndAvatar(model)
                callUpdateOTPFragment(model, email, false)
            } else if (!linkedProfile.mobile.equals(mobile) && linkedProfile.email.equals(email)) {

                model.mobile = mobile
                checkForNameAndAvatar(model)
                callUpdateOTPFragment(model, mobile, false)
            } else {

                model.email = email
                model.mobile = mobile
                checkForNameAndAvatar(model)
                callUpdateOTPFragment(model, email, true)
            }
        }
    }

    private fun updateProfileForChildStudent() {
        val model = UpdateProfileRequest()
        if (linkedProfile.email.equals(email)) {
            checkForNameAndAvatar(model)
            updateProfile(model)
        } else {
            model.email = email
            checkForNameAndAvatar(model)
            callUpdateOTPFragment(model, email, false)
        }
    }

    private fun checkForNameAndAvatar(model: UpdateProfileRequest) {
        if (!linkedProfile.firstName.equals(username))
            model.firstName = username
        if (!linkedProfile.profilePic.equals(avatarPosition))
            model.profilePic = avatarPosition
    }

    private fun callUpdateOTPFragment(
        model: UpdateProfileRequest,
        valueToVerify: String,
        isTwoVerificationRequired: Boolean
    ) {
        val bundle = Bundle()
        bundle.putSerializable("updateModel", model)
        bundle.putSerializable("isTwoVerificationRequired", isTwoVerificationRequired)
        bundle.putString("mobileOrEmail", valueToVerify)

        val fragment = UpdateUserOtpFragment()
        fragment.arguments = bundle
        (activity as EditUserActivity).setFragment(fragment, "update_profile_otp_fragment")
    }

    fun updateProfile(model: UpdateProfileRequest) {
        signInViewModel.updateProfile(model, object : BaseViewModel.APICallBacks<UpdateProfileRes> {
            override fun onSuccess(model: UpdateProfileRes?) {
                hideProgress()
                if (model != null) {
                    if (model.success) {
                        profileUpdated(model)
                    } else {
                        failedToUpdateProfile(model.errors)
                    }
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                retryUpdateProfile(code, error, model)

            }
        })
    }

    private fun retryUpdateProfile(code: Int, error: String, model: UpdateProfileRequest) {
        if (Utils.isApiFailed(code)) {
            Utils.showError(activity, code, object : BaseViewModel.ErrorCallBacks {
                override fun onRetry(msg: String) {
                    updateProfile(model)
                }

                override fun onDismiss() {
                }
            })

        } else {
            Utils.showToast(requireActivity(), error)
        }
    }

    private fun profileUpdated(model: UpdateProfileRes) {
        UserData.updateUserData(model, linkedProfile.userId)
        AppConstants.ISPROFILEUPDATED = true
        (activity as EditUserActivity).finish()
    }

    private fun failedToUpdateProfile(errors: Errors?) {
        if (errors != null) {
            val email = errors.email
            val profile = errors.profile
            val mobile = errors.mobile

            if (profile.equals("Nothing to change")) {
                (activity as EditUserActivity).finish()
            } else if (mobile?.contains("has already been taken")!!) {
                showToast(getString(R.string.edit_profile_mobile_exist))
                (activity as EditUserActivity).finish()
            }
        }
    }

    inner class GenericTextWatcher(var view: View) : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {


            val text = editable.toString()
            when (view.id) {
                R.id.et_name -> {
                    SegmentUtils.trackAddUserProfileNameClick(text)
                    binding.etName.background =
                        context!!.getDrawable(R.drawable.ic_input_field_white)
                    if (binding.tvName.visibility == View.VISIBLE)
                        binding.tvValidName.visibility = View.GONE
                }
                R.id.et_email -> {
                    SegmentUtils.trackAddUserEmailClick(text)
                    binding.etEmail.background =
                        context!!.getDrawable(R.drawable.ic_input_field_white)
                    binding.tvValidEmail.text =
                        getString(R.string.please_enter_a_valid_email_address)
                    if (binding.tvValidEmail.visibility == View.VISIBLE)
                        binding.tvValidEmail.visibility = View.GONE
                }
                R.id.et_mobile -> {
                    binding.etMobile.background =
                        context!!.getDrawable(R.drawable.ic_input_field_white)
                    binding.tvInvalidMobile.text =
                        getString(R.string.please_enter_a_valid_mobile_number)
                    if (binding.tvInvalidMobile.visibility == View.VISIBLE)
                        binding.tvInvalidMobile.visibility = View.GONE
                    if (text.length == 10) {
                        binding.btnContinue.requestFocus()
                        Utils.hideKeyboardFrom(context, binding.etMobile.rootView)
                    }
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }
}