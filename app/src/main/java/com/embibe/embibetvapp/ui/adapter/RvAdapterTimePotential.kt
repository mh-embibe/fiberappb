package com.embibe.embibetvapp.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.RowItemTimePotentialBinding
import com.embibe.embibetvapp.model.potential.Option
import com.embibe.embibetvapp.utils.ContantUtils
import com.embibe.embibetvapp.utils.FontHelper

class RvAdapterTimePotential() : RecyclerView.Adapter<RvAdapterTimePotential.VHTimePotential>() {

    private lateinit var binding: RowItemTimePotentialBinding
    private var selectedItem = -1
    private var currentPosition = -1
    var onItemClick: ((String) -> Unit)? = null
    var isFirst: Boolean = true
    var choiceList: List<String>? = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHTimePotential {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_item_time_potential,
            parent,
            false
        )
        return VHTimePotential(binding)
    }

    override fun getItemCount(): Int {
        return ContantUtils.timeSpendData.size
        return if (choiceList!!.size > 0) choiceList!!.size else 0
    }

    override fun onBindViewHolder(holder: VHTimePotential, position: Int) {
        holder.bind(position)
        if (isFirst) {
            if (position == 0) holder.binding.clItem.requestFocus()
            isFirst = false
        }
        if (selectedItem == position) holder.binding.clItem.requestFocus()
    }

    fun setData(optionList: List<Option>?) {
        this.choiceList = optionList?.get(0)?.choices
        notifyDataSetChanged()
    }


    //******ViewHolder************
    inner class VHTimePotential(var binding: RowItemTimePotentialBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.title.text = choiceList?.get(position)
            //binding.title.text = ContantUtils.timeSpendData[position]
            binding.clItem.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    FontHelper().setFontFace(FontHelper.FontType.GILROY_SEMI_BOLD, binding.title)
                } else {
                    if (adapterPosition != selectedItem) {
                        FontHelper().setFontFace(FontHelper.FontType.GILROY_MEDIUM, binding.title)
                    }

                }
            }

            if (selectedItem == adapterPosition) {
                binding.clItem.setBackgroundResource(R.drawable.bg_achieve_potential_selected_selector)
                binding.title.setTextColor(Color.parseColor("#000000"))
                FontHelper().setFontFace(FontHelper.FontType.GILROY_SEMI_BOLD, binding.title)
            } else {
                binding.clItem.setBackgroundResource(R.drawable.bg_acheive_potential_selector)
                binding.title.setTextColor(Color.parseColor("#ffffff"))
                FontHelper().setFontFace(FontHelper.FontType.GILROY_MEDIUM, binding.title)
            }

            itemView.setOnClickListener {
                onItemClick?.invoke(ContantUtils.timeSpendData[position])

                if (selectedItem != adapterPosition) {
                    val previousItem = selectedItem
                    selectedItem = adapterPosition
                    notifyDataSetChanged()
                } else {
                    selectedItem = -1
                    notifyDataSetChanged()
                }
            }

        }
    }
}