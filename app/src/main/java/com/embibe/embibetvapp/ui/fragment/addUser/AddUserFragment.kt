package com.embibe.embibetvapp.ui.fragment.addUser


import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.ConstraintSet.BOTTOM
import androidx.constraintlayout.widget.ConstraintSet.TOP
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.embibe.app.utils.Resource
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.databinding.FragmentAddUserBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.adduser.AddUserRequest
import com.embibe.embibetvapp.model.auth.signup.UserExistResponse
import com.embibe.embibetvapp.ui.activity.AddUserActivity
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.ContantUtils
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Status
import com.embibe.embibetvapp.utils.Utils


class AddUserFragment : BaseAppFragment() {

    private lateinit var binding: FragmentAddUserBinding
    private lateinit var signInViewModel: SignInViewModel
    private var userType: String = ""
    private var email: String = ""
    private var username: String = ""
    private var mobile: String = ""
    private var avatarPosition: String = ContantUtils.mImgIds[0]
    private var isFirst: Boolean = false
    private var userExistsType = "email"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signInViewModel =
            ViewModelProviders.of(activity as AddUserActivity).get(SignInViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_user, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setTextWatcher()

        registerChooseAvatarBtn()
        registerContinueBtn()
        registerCancelBtn()

        registerAvatarObserver()
        setFocusInView()
        binding.etName.requestFocus()
        userTypeSelectionValidation()
    }

    private fun setTextWatcher() {
        binding.etName.addTextChangedListener(GenericTextWatcher(binding.etName))
        binding.etEmail.addTextChangedListener(GenericTextWatcher(binding.etEmail))
        binding.etMobile.addTextChangedListener(GenericTextWatcher(binding.etMobile))
    }

    private fun userTypeSelectionValidation() {
        if (UserData.getAddParentCount() >= 2) {
            binding.buttonParent.visibility = View.GONE
            hideMobileET()
            hideEmailET()
            updateContinueBtnConstraint(binding.btnChild)
            updateRoleConstraint(binding.btnChild)
        }

        if (UserData.getAddchildCount() >= 4) {
            binding.btnChild.visibility = View.GONE
            setMarginOnParentBtn()
            showMobileET()
            showEmailET()
            updateContinueBtnConstraint(binding.etMobile)
            updateRoleConstraint(binding.buttonParent)
            binding.buttonParent.requestFocus()
            binding.etName.nextFocusDownId = R.id.button_parent
            binding.etEmail.nextFocusUpId = R.id.button_parent
            binding.buttonParent.nextFocusLeftId = R.id.btn_choose_avatar
        }
    }

    private fun updateRoleConstraint(view: View) {
        val constraintLayout: ConstraintLayout = binding.mainLayout
        val constraintSet = ConstraintSet()
        constraintSet.clone(constraintLayout)
        constraintSet.connect(binding.tvUserType.id, TOP, view.id, TOP, 0)
        constraintSet.connect(binding.tvUserType.id, BOTTOM, view.id, BOTTOM, 0)
        constraintSet.applyTo(constraintLayout)
    }

    private fun updateContinueBtnConstraint(view: View) {
        val constraintLayout: ConstraintLayout = binding.mainLayout
        val constraintSet = ConstraintSet()
        constraintSet.clone(constraintLayout)
        constraintSet.connect(binding.btnContinue.id, TOP, view.id, BOTTOM, 40)
        constraintSet.applyTo(constraintLayout)
    }

    private fun setMarginOnParentBtn() {
        val param = binding.buttonParent.layoutParams as ViewGroup.MarginLayoutParams
        param.marginStart = 0
        binding.buttonParent.layoutParams = param
    }

    private fun setFocusInView() {

        binding.btnChild.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    if (userType.equals("parent", true)) {
                        binding.buttonParent.background =
                            requireContext().getDrawable(R.drawable.btn_whiteborder_selecter)
                        binding.buttonParent.setTextColor(resources.getColor(R.color.white))
                    } else {
                        binding.buttonParent.background =
                            requireContext().getDrawable(R.drawable.btn_whiteborder_selecter)
                        binding.buttonParent.setTextColor(resources.getColor(R.color.white))
                    }
                    userType = "child"
                    binding.btnChild.background =
                        requireContext().getDrawable(R.drawable.shape_rec_white)
                    binding.btnChild.setTextColor(resources.getColor(R.color.email_font_color))

                    hideMobileET()
                    hideEmailET()
                    updateContinueBtnConstraint(binding.btnChild)
                    SegmentUtils.trackAddUserRoleFocus(userType)
                }
                false -> {
                    binding.btnChild.background =
                        requireContext().getDrawable(R.drawable.btn_whiteborder_selecter)
                    binding.btnChild.setTextColor(resources.getColor(R.color.white))

                }
            }
        }
        binding.buttonParent.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    if (userType.equals("child", true) || userType.equals("student", true)) {
                        binding.btnChild.background =
                            requireContext().getDrawable(R.drawable.btn_whiteborder_selecter)
                        binding.btnChild.setTextColor(resources.getColor(R.color.white))
                    }
                    userType = "parent"
                    binding.buttonParent.background =
                        requireContext().getDrawable(R.drawable.shape_rec_white)
                    binding.buttonParent.setTextColor(resources.getColor(R.color.email_font_color))

                    showMobileET()
                    showEmailET()
                    updateContinueBtnConstraint(binding.etMobile)
                    SegmentUtils.trackAddUserRoleFocus(userType)
                }
                false -> {
                    binding.buttonParent.background =
                        requireContext().getDrawable(R.drawable.btn_whiteborder_selecter)
                    binding.buttonParent.setTextColor(resources.getColor(R.color.white))

                }
            }
        }

        binding.etName.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    if (userType.equals("child", true) || userType.equals("student", true)) {
                        binding.btnChild.background =
                            requireContext().getDrawable(R.drawable.shape_rec_white)
                        binding.btnChild.setTextColor(resources.getColor(R.color.email_font_color))
                        SegmentUtils.trackAddUserScreenBoxFocus(username)

                    } else if (userType.equals("parent", true)) {
                        binding.buttonParent.background =
                            requireContext().getDrawable(R.drawable.shape_rec_white)
                        binding.buttonParent.setTextColor(resources.getColor(R.color.email_font_color))
                        SegmentUtils.trackAddUserScreenBoxFocus(username)
                    }
                }
                false -> {
                    if (userType.equals("child", true) || userType.equals("student", true)) {
                        binding.btnChild.background =
                            requireContext().getDrawable(R.drawable.shape_rec_white)
                        binding.btnChild.setTextColor(resources.getColor(R.color.email_font_color))

                    } else if (userType.equals("parent", true)) {
                        binding.buttonParent.background =
                            requireContext().getDrawable(R.drawable.shape_rec_white)
                        binding.buttonParent.setTextColor(resources.getColor(R.color.email_font_color))

                    }
                }
            }

        }

        binding.etEmail.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    if (userType.equals("child", true) || userType.equals("student", true)) {
                        binding.btnChild.background =
                            requireContext().getDrawable(R.drawable.shape_rec_white)
                        binding.btnChild.setTextColor(resources.getColor(R.color.email_font_color))
                        SegmentUtils.trackAddUserScreenBoxFocus(email)
                    } else {
                        binding.buttonParent.background =
                            requireContext().getDrawable(R.drawable.shape_rec_white)
                        binding.buttonParent.setTextColor(resources.getColor(R.color.email_font_color))
                        SegmentUtils.trackAddUserScreenBoxFocus(email)
                    }
                }
                false -> {
                    if (userType.equals("child", true) || userType.equals("student", true)) {
                        binding.btnChild.background =
                            requireContext().getDrawable(R.drawable.shape_rec_white)
                        binding.btnChild.setTextColor(resources.getColor(R.color.email_font_color))

                    } else {
                        binding.buttonParent.background =
                            requireContext().getDrawable(R.drawable.shape_rec_white)
                        binding.buttonParent.setTextColor(resources.getColor(R.color.email_font_color))

                    }
                }
            }
        }

        binding.etEmail.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event!!.action == KeyEvent.ACTION_DOWN) {
                        if (userType.equals("child", true) || userType.equals("student", true)) {
                            binding.btnContinue.requestFocus()
                        } else {
                            binding.etMobile.requestFocus()
                        }

                        return true
                    }
                    return false
                }
                return false
            }
        })

        binding.btnContinue.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    Utils.hideKeyboardFrom(context, binding.btnContinue.rootView)
                    SegmentUtils.trackAddUserContinueFocus()
                    if (userType.equals("child", true) || userType.equals("student", true)) {
                        binding.btnChild.background =
                            requireContext().getDrawable(R.drawable.shape_rec_white)
                        binding.btnChild.setTextColor(resources.getColor(R.color.email_font_color))
                        SegmentUtils.trackAddUserScreenBoxFocus(email)
                    } else {
                        binding.buttonParent.background =
                            requireContext().getDrawable(R.drawable.shape_rec_white)
                        binding.buttonParent.setTextColor(resources.getColor(R.color.email_font_color))
                        SegmentUtils.trackAddUserScreenBoxFocus(email)
                    }
                }
                false -> {
                    if (userType.equals("child", true) || userType.equals("student", true)) {
                        binding.btnChild.background =
                            requireContext().getDrawable(R.drawable.shape_rec_white)
                        binding.btnChild.setTextColor(resources.getColor(R.color.email_font_color))

                    } else if (userType.equals("parent", true)) {
                        binding.buttonParent.background =
                            requireContext().getDrawable(R.drawable.shape_rec_white)
                        binding.buttonParent.setTextColor(resources.getColor(R.color.email_font_color))

                    }
                }
            }
        }

        binding.btnCancel.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    SegmentUtils.trackAddUserCancelFocus()
                }
            }
        }
    }

    private fun hideMobileET() {
        if (binding.etMobile.visibility == View.VISIBLE) binding.etMobile.visibility = View.GONE
        if (binding.tvMobile.visibility == View.VISIBLE) binding.tvMobile.visibility = View.GONE
        if (binding.tvInvalidMobile.visibility == View.VISIBLE) binding.tvInvalidMobile.visibility =
            View.GONE
        if (binding.btnCancel.visibility == View.GONE) binding.btnCancel.visibility == View.VISIBLE
    }

    private fun showMobileET() {
        if (binding.etMobile.visibility == View.GONE) binding.etMobile.visibility = View.VISIBLE
        if (binding.tvMobile.visibility == View.GONE) binding.tvMobile.visibility = View.VISIBLE
        if (binding.btnCancel.visibility == View.VISIBLE) binding.btnCancel.visibility == View.GONE
    }

    private fun hideEmailET() {
        if (binding.etEmail.visibility == View.VISIBLE) binding.etEmail.visibility = View.GONE
        if (binding.tvEmail.visibility == View.VISIBLE) binding.tvEmail.visibility = View.GONE
        if (binding.tvValidEmail.visibility == View.VISIBLE) binding.tvValidEmail.visibility =
            View.GONE
        if (binding.btnCancel.visibility == View.GONE) binding.btnCancel.visibility == View.VISIBLE
    }

    private fun showEmailET() {
        if (binding.etEmail.visibility == View.GONE) binding.etEmail.visibility = View.VISIBLE
        if (binding.tvEmail.visibility == View.GONE) binding.tvEmail.visibility = View.VISIBLE
        if (binding.btnCancel.visibility == View.VISIBLE) binding.btnCancel.visibility == View.GONE
    }

    private fun registerContinueBtn() {
        binding.btnContinue.setOnClickListener {
            username = binding.etName.text.toString().trim()
            email = binding.etEmail.text.toString().trim()
            mobile = binding.etMobile.text.toString().trim()

            if ((userType.equals("child", true) || userType.equals("student", true))) {
                if (username.length < 3) {
                    binding.tvValidName.visibility = View.VISIBLE
                    binding.etName.background =
                        requireContext().getDrawable(R.drawable.ic_input_field_error)
                } else {
                    val fragment = SelectGoalExamFragment()
                    val bundle = Bundle()
                    bundle.putString("user_type", userType)
                    bundle.putString("user_name", username)
//                bundle.putString("email", email)
                    bundle.putString("avatarPosition", avatarPosition)
                    fragment.arguments = bundle
                    (activity as AddUserActivity).setFragment(fragment, "select_goal_exam_fragment")
                }

            } else {
                if (username.length < 3) {
                    binding.tvValidName.visibility = View.VISIBLE
                    binding.etName.background =
                        requireContext().getDrawable(R.drawable.ic_input_field_error)
                }
                if (email.length < 10 || !Utils.isEmailValid(email)) {
                    binding.tvValidEmail.visibility = View.VISIBLE
                    binding.etEmail.background =
                        requireContext().getDrawable(R.drawable.ic_input_field_error)
                }
                if (userType.equals("parent", true) && mobile.length < 10) {
                    binding.tvInvalidMobile.visibility = View.VISIBLE
                    binding.etMobile.background =
                        requireContext().getDrawable(R.drawable.ic_input_field_error)
                }

                checkUserAlreadyExist()
                registerUserExistObserver()
                SegmentUtils.trackAddUserScreenAddClick(username, userType, email, mobile, false)
                SegmentUtils.trackAddUserContinueClick()
                SegmentUtils.trackAddUserRoleClick(userType)
            }
        }
    }

    private fun checkUserAlreadyExist() {
        if (userType.equals("parent", true) &&
            username.length >= 3 && email.length >= 10 && Utils.isEmailValid(email) && mobile.length == 10
        ) {
            signInViewModel.getUserExist(email)
            userExistsType = "email"

        }
        if ((userType.equals("child", true) || userType.equals("student", true)) &&
            username.length >= 3 && email.length >= 10 && Utils.isEmailValid(email)
        ) {
            signInViewModel.getUserExist(email)
            isFirst = true
        }
    }

    private fun registerUserExistObserver() {
        signInViewModel.userExistLiveData.observe(viewLifecycleOwner, Observer { resource ->
            if (resource != null) {
                when (resource.status) {
                    Status.LOADING -> showProgress()
                    Status.SUCCESS -> {
                        hideProgress()
                        val response = resource.data as UserExistResponse
                        if (userType.equals("parent", ignoreCase = true)) {
                            userExistsFlowForParent(response)
                        }
                        if (userType.equals("child", true) || userType.equals("student", true)) {
                            userExistsFlowForChild(response)
                        }
                    }

                    Status.ERROR -> {
                        hideProgress()
                        retryUserExistsAPI(resource)
                    }
                }
            }
        })
    }

    private fun userExistsFlowForParent(response: UserExistResponse) {
        if (userExistsType == "email") {
            if (!response.userExists) {
                signInViewModel.getUserExist(mobile)
                userExistsType = "mobile"
            } else {
                binding.tvValidEmail.visibility = View.VISIBLE
                binding.tvValidEmail.text = getString(R.string.email_already_taken)
                binding.etEmail.background =
                    requireContext().getDrawable(R.drawable.ic_input_field_error)
            }
        } else if (userExistsType == "mobile") {
            if (!response.userExists) {
                actionAsPerUserType()
            } else {
                binding.tvInvalidMobile.visibility = View.VISIBLE
                binding.tvInvalidMobile.text =
                    getString(R.string.mobile_already_taken)
                binding.etMobile.background =
                    requireContext().getDrawable(R.drawable.ic_input_field_error)
            }
        }
    }

    private fun userExistsFlowForChild(response: UserExistResponse) {
        if (userExistsType == "email") {
            if (!response.userExists) {
                actionAsPerUserType()
            } else {
                binding.tvValidEmail.visibility = View.VISIBLE
                binding.tvValidEmail.text = getString(R.string.email_already_taken)
                binding.etEmail.background =
                    requireContext().getDrawable(R.drawable.ic_input_field_error)
            }
        }
    }

    private fun retryUserExistsAPI(resource: Resource<UserExistResponse>) {
        if (Utils.isApiFailed(resource)) {
            Utils.showError(context, resource, object : BaseViewModel.ErrorCallBacks {
                override fun onRetry(msg: String) {
                    checkUserAlreadyExist()
                }

                override fun onDismiss() {

                }
            })
        } else {
            /*update error msg in ui*/
        }
    }

    override fun onResume() {
        super.onResume()
        if (binding.etName.text.toString() == "") {
            binding.etName.requestFocus()
            val imm: InputMethodManager =
                requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(binding.etName, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    private fun actionAsPerUserType() {
        val avatarName = avatarPosition

        if (userType.equals("parent", true)) {
            val model = AddUserRequest(
                userType, username, email, "embibe1234", "sp", "",
                "", "", "", "", avatarName, mobile
            )
            callAddOTPFragment(model, email, true)
        }

        if (userType.equals("child", true) || userType.equals("student", true)) {
            val model = AddUserRequest(
                userType, username, email, "embibe1234", "sp", "",
                "", "", "", "", avatarName, ""
            )
            callAddOTPFragment(model, email, false)
        }
    }

    private fun callAddOTPFragment(
        model: AddUserRequest,
        valueToVerify: String,
        isTwoVerificationRequired: Boolean
    ) {
        val bundle = Bundle()
        bundle.putSerializable("addUserModel", model)
        bundle.putSerializable("isTwoVerificationRequired", isTwoVerificationRequired)
        bundle.putString("mobileOrEmail", valueToVerify)
        bundle.putString("userType", userType)
        bundle.putBoolean("isFirst", isFirst)

        val fragment = AddUserOtpFragment()
        fragment.arguments = bundle
        (activity as AddUserActivity).setFragment(fragment, "add_user_otp_fragment_email")
    }

    private fun registerCancelBtn() {
        binding.btnCancel.setOnClickListener {
            SegmentUtils.trackAddUserCancelClick()
            (activity as AddUserActivity).finish()
        }
    }

    private fun registerChooseAvatarBtn() {
        binding.btnChooseAvatar.setOnClickListener {
            (activity as AddUserActivity).setFragment(
                SelectAvatarFragment(),
                "select_avatar_fragment"
            )
        }
    }

    private fun registerAvatarObserver() {
        signInViewModel.selectAvatarLiveData.observe(viewLifecycleOwner, Observer { position ->
            avatarPosition = position
            Utils.setAvatarImage(requireActivity(), binding.ivAvatar, avatarPosition)
        })
    }


    inner class GenericTextWatcher(var view: View) : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {

            val text = editable.toString()
            when (view.id) {
                R.id.et_name -> {
                    SegmentUtils.trackAddUserProfileNameClick(text)
                    binding.etName.background =
                        context!!.getDrawable(R.drawable.ic_input_field_white)
                    if (binding.tvName.visibility == View.VISIBLE) binding.tvValidName.visibility =
                        View.GONE
                }
                R.id.et_email -> {
                    SegmentUtils.trackAddUserEmailClick(text)
                    binding.etEmail.background =
                        context!!.getDrawable(R.drawable.ic_input_field_white)
                    binding.tvValidEmail.text =
                        getString(R.string.please_enter_a_valid_email_address)
                    if (binding.tvValidEmail.visibility == View.VISIBLE) binding.tvValidEmail.visibility =
                        View.GONE
                }
                R.id.et_mobile -> {
                    binding.etMobile.background =
                        context!!.getDrawable(R.drawable.ic_input_field_white)
                    binding.tvInvalidMobile.text =
                        getString(R.string.please_enter_a_valid_mobile_number)
                    if (binding.tvInvalidMobile.visibility == View.VISIBLE) binding.tvInvalidMobile.visibility =
                        View.GONE
                    if (text.length == 10) {
                        binding.btnContinue.requestFocus()
                        Utils.hideKeyboardFrom(
                            context,
                            binding.etMobile.rootView
                        )
                    }
                }

            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }
}