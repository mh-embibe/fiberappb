package com.embibe.embibetvapp.ui.viewholders

import android.animation.ValueAnimator
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemBarBinding
import com.embibe.embibetvapp.model.test.SectionSummary
import kotlin.math.roundToInt

class TestBarViewHolder(
    val binding: ItemBarBinding,
    var listItems: MutableList<SectionSummary>
) : BaseViewHolder<SectionSummary>(binding) {

    override fun bind(item: SectionSummary) {

        if (item.marksScored >= 0) {
            updateUIForPositiveMarks(binding)
            binding.tvSubjectLabel.text = item.name?.capitalize()
            binding.ivBarUp.setBackgroundResource(getChartColorByPosition(adapterPosition))
            animateBar(binding.ivBarUp, manipulateHeight(item.marksScored.roundToInt()))
            binding.tvMarksUp.text = """${item.marksScored.toInt()} marks"""
        } else {
            updateUIForNegativeMarks(binding)
            binding.ivBarDown.setBackgroundResource(getChartColorByPosition(adapterPosition))
            animateBar(binding.ivBarDown, manipulateHeight(Math.abs(item.marksScored.roundToInt())))
            binding.tvMarksDown.text = """${item.marksScored.roundToInt()} marks"""
        }
    }

    private fun updateUIForPositiveMarks(binding: ItemBarBinding) {
        binding.ivBarDown.visibility = GONE
        binding.tvMarksDown.visibility = GONE
        binding.ivBarUp.visibility = VISIBLE
        binding.tvMarksUp.visibility = VISIBLE
    }

    private fun updateUIForNegativeMarks(binding: ItemBarBinding) {
        binding.ivBarUp.visibility = GONE
        binding.tvMarksUp.visibility = GONE
        binding.ivBarDown.visibility = VISIBLE
        binding.tvMarksDown.visibility = VISIBLE
    }

    private fun manipulateHeight(value: Int) = value.toDouble().div(40).times(10).toInt()

    private fun getChartColorByPosition(position: Int): Int {
        return when (position) {
            0 -> R.drawable.ic_bar_chart_purple
            1 -> R.drawable.ic_bar_chart_red
            2 -> R.drawable.ic_bar_chart_blue
            3 -> R.drawable.ic_bar_chart_pink
            4 -> R.drawable.ic_bar_chart_orange
            5 -> R.drawable.ic_bar_chart_green
            else -> R.drawable.ic_bar_chart_red
        }
    }

    private fun animateBar(bar: ImageView, progressValue: Int) {
        val animator = ValueAnimator.ofInt(0, progressValue)
        var listener: ValueAnimator.AnimatorUpdateListener? = null
        listener = ValueAnimator.AnimatorUpdateListener { valueAnimator ->
            val value = valueAnimator.animatedValue
            val params = bar.layoutParams as ConstraintLayout.LayoutParams
            if (value as Int > 0) {
                params.height = value
                bar.visibility = VISIBLE
            } else
                bar.visibility = GONE
            params.height = value
            bar.layoutParams = params

            if (value == progressValue)
                animator.removeUpdateListener(listener!!)
        }
        animator.addUpdateListener(listener)

        animator.duration = 1500
        animator.start()
    }
}