package com.embibe.embibetvapp.ui.fragment.addUser

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.embibe.app.utils.Resource
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentSelectGoalExamBinding
import com.embibe.embibetvapp.model.UpdateProfileRequest
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.adduser.AddUserRequest
import com.embibe.embibetvapp.model.adduser.AddUserResponse
import com.embibe.embibetvapp.model.adduser.Exam
import com.embibe.embibetvapp.model.adduser.Goal
import com.embibe.embibetvapp.model.auth.login.LoginResponse
import com.embibe.embibetvapp.model.goals.GoalsExamsRes
import com.embibe.embibetvapp.model.updateProfile.Errors
import com.embibe.embibetvapp.model.updateProfile.UpdateProfileRes
import com.embibe.embibetvapp.ui.activity.AddUserActivity
import com.embibe.embibetvapp.ui.activity.UserSwitchActivity
import com.embibe.embibetvapp.ui.adapter.GoalAdapter
import com.embibe.embibetvapp.ui.adapter.PrimaryGoalExamAdapter
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Status
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper

class SelectGoalExamFragment : BaseAppFragment() {
    private lateinit var mCurrentView: View
    private lateinit var binding: FragmentSelectGoalExamBinding
    private lateinit var signInViewModel: SignInViewModel

    private lateinit var goalAdapter: GoalAdapter
    private lateinit var goalExamAdapter: PrimaryGoalExamAdapter

    private lateinit var primaryGoalCode: String
    private lateinit var primaryExamCode: String

    private var userType: String? = null
    private var userName: String? = null
    private var email: String? = null
    private var avatarPosition: String? = ""
    private var avatarName: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_select_goal_exam, container, false)
        signInViewModel =
            ViewModelProviders.of(activity as AddUserActivity).get(SignInViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCurrentView = view
        getGoalsExams()

    }

    private fun updateResult(view: View) {
        getBundleData()
        setGoalRecyclerView(view)
        setExamRecyclerView(view)
        registerDoneBtn()
        registerAddUserObserver()
    }

    private fun getBundleData() {
        userType = requireArguments().getString("user_type")
        userName = requireArguments().getString("user_name")
        email = requireArguments().getString("email", "")
        avatarPosition = requireArguments().getString("avatarPosition")
    }

    @SuppressLint("SetTextI18n")
    private fun setGoalRecyclerView(view: View) {

        goalAdapter = GoalAdapter(view.context)

        makeGridCenter(binding.rvPrimaryGoal)

        binding.rvPrimaryGoal.adapter = goalAdapter
        goalAdapter.setData(DataManager.instance.getPrimaryGoals())
        setDefaultFocusToSupportedGoal(DataManager.instance.getPrimaryGoals())

        goalAdapter.onItemClick = { goal ->
            if (goal.supported) {
                primaryGoalCode = goal.code
                goalExamAdapter.setData(goal.exams)
                setDefaultFocusToSupportedExam(goal.exams)
            }
        }

        goalAdapter.onFocusChange = { goal, hasFocus ->
            if (!goal.supported) {
                binding.textGoalNotSupported.visibility = View.VISIBLE
                binding.textGoalNotSupported.text =
                    "${resources.getString(R.string.text_goal_not_supported)} ${goal.name}"
            } else {
                binding.textGoalNotSupported.visibility = View.GONE
            }
            if (!hasFocus) {
                binding.textGoalNotSupported.visibility = View.GONE
            }
        }
    }

    private fun setDefaultFocusToSupportedGoal(primaryGoals: List<Goal>) {
        for (index in 0 until primaryGoals.size) {
            if (primaryGoals[index].default) {
                goalAdapter.setDefaultSelectedItem(index)
                primaryGoalCode = primaryGoals[index].code
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setExamRecyclerView(view: View) {

        goalExamAdapter = PrimaryGoalExamAdapter(view.context)

        makeGridCenter(binding.rvPrimaryExams)

        binding.rvPrimaryExams.adapter = goalExamAdapter
        goalExamAdapter.setData(DataManager.instance.getPrimaryGoals()[0].exams)
        setDefaultFocusToSupportedExam(DataManager.instance.getPrimaryGoals()[0].exams)

        goalExamAdapter.onItemClick = { exam ->
            primaryExamCode = exam.code
        }

        goalExamAdapter.onFocusChange = { exam, hasFocus ->
            if (!exam.supported) {
                binding.textExamNotSupported.visibility = View.VISIBLE
                binding.textExamNotSupported.text =
                    "${resources.getString(R.string.text_exam_not_supported)} ${exam.name}"
            } else {
                binding.textExamNotSupported.visibility = View.GONE
            }
            if (!hasFocus) {
                binding.textExamNotSupported.visibility = View.GONE
            }
        }
    }

    private fun setDefaultFocusToSupportedExam(exams: ArrayList<Exam>) {
        for (index in 0 until exams.size) {
            if (exams[index].default) {
                goalExamAdapter.setDefaultSelectedItem(index)
                primaryExamCode = exams[index].code
            }
        }
    }

    private fun registerDoneBtn() {
        binding.buttonDone.setOnClickListener {
            avatarName = avatarPosition!!

            // board corresponds to Goal and kClass corresponds to exam
            SegmentUtils.trackGradeBoardScreenSaveClick(
                AppConstants.TEMP_BOARD, AppConstants.TEMP_K_CLASS, avatarName, primaryGoalCode
            )

            if (primaryGoalCode != null && primaryExamCode != null) {
                callAddUserApi()
            }
        }
    }

    private fun callAddUserApi() {
        avatarName = avatarPosition!!
        var userID = UserData.getUserID()
        if (userID.isEmpty()) {
            userID = UserData.getLinkedUser()[0]!!.userId
        }
        signInViewModel.addUser(
            userID, AddUserRequest(
                userType!!, userName!!, "", "embibe1234", "sp", primaryGoalCode,
                primaryExamCode, AppConstants.TEMP_K_CLASS, AppConstants.TEMP_BOARD, "male",
                avatarName, ""
            )
        )
    }

    private fun registerAddUserObserver() {
        signInViewModel.addUserLiveData.observe(viewLifecycleOwner, Observer { resource ->
            if (resource != null) {
                when (resource.status) {
                    Status.LOADING -> showProgress()
                    Status.SUCCESS -> {
                        var response = resource.data as AddUserResponse
                        if (response.success && response.message.equals("user added", true)
                            || response.message.equals("user updated", true)
                        ) {
                            getConnectedProfile()
                        } else {
                            Utils.showToast(requireContext(), "Unable to add user")
                        }
                        hideProgress()
                    }
                    Status.ERROR -> {
                        hideProgress()
                        retryAddUser(resource)
                    }
                }
            }
        })
    }

    private fun retryAddUser(resource: Resource<AddUserResponse>) {
        if (Utils.isApiFailed(resource)) {
            Utils.showError(context, resource, object : BaseViewModel.ErrorCallBacks {
                override fun onRetry(msg: String) {
                    callAddUserApi()
                }

                override fun onDismiss() {
                }
            })
        } else {
            /*update error msg in ui*/
        }
    }


    private fun getConnectedProfile() {
        getLinkedProfiles()
    }

    fun updateProfile(model: UpdateProfileRequest) {
        signInViewModel.updateProfile(model, object : BaseViewModel.APICallBacks<UpdateProfileRes> {
            override fun onSuccess(model: UpdateProfileRes?) {
                hideProgress()
                if (model != null) {
                    if (model.success) {
                        profileUpdated(model)
                    } else {
                        failedToUpdateProfile(model.errors)
                    }
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            updateProfile(model)
                        }

                        override fun onDismiss() {

                        }
                    })

                } else {
                    showToast(error)
                }

            }
        })
    }

    private fun profileUpdated(model: UpdateProfileRes) {

    }

    private fun failedToUpdateProfile(errors: Errors?) {
        if (errors != null) {
            val email = errors.email
            val profile = errors.profile
            val mobile = errors.mobile
        }
    }


    private fun getLinkedProfiles() {
        showProgress()
        signInViewModel.getLinkedProfilesApi(object : BaseViewModel.APICallBacks<LoginResponse> {
            override fun onSuccess(model: LoginResponse?) {

                if (model != null && model.success) {
                    makeLog("Authentication Success")
                    updateSignInResponse(model)
                } else {
                    makeLog("getLinkedProfilesApi api failed")
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                makeLog("getLinkedProfilesApi api error $error")
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getLinkedProfiles()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }

        })
    }

    private fun updateSignInResponse(response: LoginResponse) {
        updateLoginPreferences(response)
        if (PreferenceHelper()[AppConstants.HOST_PAGE, ""].equals(AppConstants.PROFILE)) {
            (activity as AddUserActivity).finish()
        } else {
            startActivity(Intent(context, UserSwitchActivity::class.java))
            (activity as AddUserActivity).finish()
        }
    }

    private fun updateLoginPreferences(response: LoginResponse) {
        com.embibe.embibetvapp.model.UserData.updateLinkedUser(response)
    }


    fun getGoalsExams() {
        showProgress()
        signInViewModel.getGoalsExams(object : BaseViewModel.APICallBacks<GoalsExamsRes> {
            override fun onSuccess(model: GoalsExamsRes?) {
                hideProgress()
                if (model != null) {
                    DataManager.instance.setGoalsExamsList(
                        ((model.data ?: arrayListOf()) as ArrayList<Goal>)
                    )
                    updateResult(mCurrentView)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                retryGoalsExamsAPI(code, error)
            }
        })
    }

    private fun retryGoalsExamsAPI(code: Int, error: String) {
        if (Utils.isApiFailed(code)) {
            Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                override fun onRetry(msg: String) {
                    getGoalsExams()
                }

                override fun onDismiss() {

                }
            })

        } else {
            showToast(error)
        }
    }
}