package com.embibe.embibetvapp.ui.fragment.addUser

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.databinding.FragmentSelectAvatarBinding
import com.embibe.embibetvapp.ui.adapter.RvAdapterAvartar
import com.embibe.embibetvapp.ui.interfaces.ComponentClickListener
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.ContantUtils
import com.embibe.embibetvapp.utils.SegmentUtils

class SelectAvatarFragment : BaseAppFragment(), ComponentClickListener {
    private lateinit var binding: FragmentSelectAvatarBinding
    private lateinit var signInViewModel: SignInViewModel
    private lateinit var adapterAvatar: RvAdapterAvartar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signInViewModel =
            ViewModelProviders.of(activity!!).get(SignInViewModel::class.java)
        SegmentUtils.trackSelectAvatarScreenLoadStart()
        adapterAvatar = RvAdapterAvartar(context!!)
        defaultAvaterObserver()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_select_avatar, container, false)
        setAdapterOnRV()
        return binding.root
    }

    private fun setAdapterOnRV() {
        //val adapterAvatar = RvAdapterAvartar(context!!)
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.rvAvatar.layoutManager = layoutManager
        binding.rvAvatar.adapter = adapterAvatar

        adapterAvatar.onItemClick = { position ->
            signInViewModel.setAvatar(ContantUtils.mImgIds[position])
//            UserData.setCurrentUserAvatarPosition(position)
//            fragmentManager.popBackStack()
            activity?.onBackPressed()
        }
    }

    private fun defaultAvaterObserver() {
        signInViewModel.defaultAvatarLiveData.observe(requireActivity(), Observer { t ->
            if (t != null) adapterAvatar.defaultAvatarPosition(t)
        })
    }

    override fun onClicked(type: String, data: Any) {
//        SegmentUtils.trackSelectAvatarScreenLoadEnd()
//        SegmentUtils.trackAvatarIconClick()
//        var position = data as Int
//        signInViewModel.setAvatar(position)
//        fragmentManager!!.popBackStack()
    }
}