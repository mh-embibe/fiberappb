package com.embibe.embibetvapp.ui.adapter

import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.drawable.Animatable
import android.os.CountDownTimer
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.text.bold
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ItemRecommendedLearningBinding
import com.embibe.embibetvapp.databinding.ItemRecommendedLearningGifBinding
import com.embibe.embibetvapp.databinding.ItemRecommendedPracticeBinding
import com.embibe.embibetvapp.databinding.ItemTestForChapterBinding
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo
import kotlinx.android.synthetic.main.item_practice_for_chapter.view.*

class TestSummaryAdapter(private val mContext: Context) :
    RecyclerView.Adapter<TestSummaryAdapter.ItemViewHolder<*>>() {
    private lateinit var datatype: String
    var list: ArrayList<Content> = ArrayList()
    var labelsWithSequenceList: ArrayList<String> = ArrayList()
    val TAG = this.javaClass.name
    var onItemClick: ((Content) -> Unit)? = null
    var onItemDpadHit: ((Int) -> Unit)? = null
    private var previewTimer: CountDownTimer? = null

    companion object {
        private const val TYPE_CARD_RECOMMENDED_LEARNING = 1
        private const val TYPE_CARD_RECOMMENDED_PRACTICE = 2
        private const val TYPE_CARD_MORE_TEST = 0
        private const val TYPE_GIF = 4
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder<Content> {
        return when (viewType) {
            TYPE_CARD_RECOMMENDED_LEARNING -> {
                val binding: ItemRecommendedLearningBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_recommended_learning,
                    parent,
                    false
                )
                RecommendedLearningCardViewHolder(binding)
            }
            TYPE_CARD_RECOMMENDED_PRACTICE -> {
                val binding: ItemRecommendedPracticeBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_recommended_practice,
                    parent,
                    false
                )
                RecommendedPracticeCardViewHolder(binding)
            }
            TYPE_CARD_MORE_TEST -> {
                val binding: ItemTestForChapterBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_test_for_chapter,
                    parent,
                    false
                )
                MoreTestCardViewHolder(binding)
            }
            TYPE_GIF -> {
                val binding: ItemRecommendedLearningGifBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_recommended_learning_gif,
                    parent,
                    false
                )
                return GifCardViewHolder(binding)
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder<*>, position: Int) {
        val item = list[position]
        when (holder) {
            is MoreTestCardViewHolder -> holder.bind(item)
            is RecommendedLearningCardViewHolder -> holder.bind(item)
            is RecommendedPracticeCardViewHolder -> holder.bind(item)
            is GifCardViewHolder -> holder.bind(item)
            else -> (holder as RecommendedLearningCardViewHolder).bind(item)
        }

//        holder.itemView.setOnFocusChangeListener { view, hasFocus ->
//            if (hasFocus)
//                updateViewScaling(view, 1f, 1.06f)
//            else
//                updateViewScaling(view, 1.06f, 1f)
//        }

    }

    private fun updateViewScaling(view: View, from: Float, to: Float) {
        ObjectAnimator.ofFloat(view, View.SCALE_X, from, to).start()
        ObjectAnimator.ofFloat(view, View.SCALE_Y, from, to).start()
    }

    override fun getItemViewType(position: Int): Int {
        val value: Int
        var item: Content = list[position]

        value = when (datatype) {
            mContext.getString(R.string.more_tests) -> TYPE_CARD_MORE_TEST
            mContext.getString(R.string.recommended_learning) -> {
                return if (item.preview_url.isNullOrEmpty()) {
                    TYPE_CARD_RECOMMENDED_LEARNING
                } else {
                    TYPE_GIF
                }
            }
            mContext.getString(R.string.recommended_practice) -> TYPE_CARD_RECOMMENDED_PRACTICE
            else -> TYPE_CARD_MORE_TEST
        }
        return value
    }

    fun setData(
        data: ArrayList<Content>,
        type: String
    ) {
        labelsWithSequenceList.clear()
        list = data
        datatype = type
        var incr = 0
        list.forEach { content ->
            if (content.type.equals(AppConstants.TYPE_VIDEO, true)) {
                incr += 1
                labelsWithSequenceList.add("${incr}. ${content.title}")
            } else {
                incr = 0
                labelsWithSequenceList.add("")
            }
        }
        notifyDataSetChanged()
    }

    abstract class ItemViewHolder<Content>(binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        abstract fun bind(item: Content)
    }

    inner class RecommendedLearningCardViewHolder(var binding: ItemRecommendedLearningBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
            binding.txtWastedAttempts.text = labelsWithSequenceList[list.indexOf(item)]
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }
            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            onItemDpadHit?.invoke(adapterPosition)
                        }
                    }
                }

                false
            }
        }
    }

    inner class RecommendedPracticeCardViewHolder(var binding: ItemRecommendedPracticeBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.questionBooksTagTV.text = item.question_book_tag
            loadThumb(item, itemView.context, itemView.thumbPracticeForChapterIV)
            binding.detailItem = item
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }
            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            onItemDpadHit?.invoke(adapterPosition)
                        }
                    }
                }

                false
            }
        }
    }

    inner class MoreTestCardViewHolder(var binding: ItemTestForChapterBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
//            binding.detailItem = item
//            loadThumb(item, itemView.context, binding.ivImg)

            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)

            Glide.with(itemView.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(item.thumb)
                .placeholder(R.drawable.practice_placeholder)
                .error(R.drawable.practice_placeholder)
                .into(binding.ivImg)

            var time = Utils.convertIntoMinsTest(item.duration).split("\\s".toRegex())
            binding.testTime.text = setTextHTML(time[0], " " + time[1].toUpperCase())
            binding.testMarks.text = setTextHTML(item.total_marks.toString(), " Marks")
            binding.testQuestions.text = setTextHTML(item.questions, " Questions")
            binding.testQualityScore.text =
                setTextHTML(item.test_quality_score + "%", " Test Quality Score")
            binding.tvTestName.text = item.title
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }
            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            onItemDpadHit?.invoke(adapterPosition)
                        }
                    }
                }

                false
            }
//            itemView.setOnFocusChangeListener { v, hasFocus ->
//                if (hasFocus) {
//                    binding.ivBg.visibility = View.VISIBLE
//                    binding.btnTestFeedback.visibility = View.VISIBLE
//                } else {
//                    binding.ivBg.visibility = View.GONE
//                    binding.btnTestFeedback.visibility = View.GONE
//                }
//            }
        }
    }

    fun setTextHTML(boldValue: String, normalValue: String): Spanned {
        val sValue = SpannableStringBuilder()
            .bold { append(boldValue) }
            .append(normalValue)
        return sValue
    }

    private fun loadThumb(
        item: Content,
        context: Context,
        ivImg: ImageView
    ) {
        val requestOptions = RequestOptions().transform(RoundedCorners(10))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        if (item.thumb.isEmpty()) {
            Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(R.drawable.video_placeholder)
                .into(ivImg)
        } else {
            Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(item.thumb)
                .placeholder(R.drawable.video_placeholder)
                .into(ivImg)
        }
    }

    inner class GifCardViewHolder(var binding: ItemRecommendedLearningGifBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.layout.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    previewWithADelay()
                } else {
                    previewTimer?.cancel()
                    binding.ivImg.visibility = View.VISIBLE
                    binding.ivGifView.visibility = View.INVISIBLE
//                    updateViewScaling(binding.layout, 1.06f, 1f)
                }
            }
            binding.detailItem = item
            binding.txtWastedAttempts.text = labelsWithSequenceList[list.indexOf(item)]
            Glide.with(mContext).load(item.owner_info.copy_logo).into(binding.ivEmbibeLogo)
            if (item.type != "header") {
                if (item.category_thumb.isNotEmpty()) {
                    binding.ivCategory.visibility = View.VISIBLE
                    Glide.with(itemView.context)
                        .load(item.category_thumb)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(binding.ivCategory)
                } else {
                    binding.ivCategory.visibility = View.GONE
                }

                binding.textTime.text = Utils.convertIntoMins(item.length)
            }

            binding.tvSubjectName.visibility = View.GONE

            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            if (item.thumb == "thumb" || item.thumb == "") {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            } else {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(item.thumb)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            }
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
                var item: Content? = binding.detailItem
                if (item != null) {
                    SegmentUtils.trackEventMoreInfoMenuItemTileClick(item)
                }
            }



            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            onItemDpadHit?.invoke(adapterPosition)
                        }
                    }

                }

                false
            }
        }

        private fun previewWithADelay() {
            previewTimer = object : CountDownTimer(4000, 1000) {
                override fun onFinish() {
                    showPreview()
                }

                override fun onTick(millisUntilFinished: Long) {}
            }.start()
        }

        private fun showPreview() {
            binding.ivGifView.visibility = View.VISIBLE
            val controller = Fresco.newDraweeControllerBuilder()
            controller.autoPlayAnimations = true
            controller.setUri(list[adapterPosition].preview_url)
            controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                override fun onFinalImageSet(
                    id: String?,
                    imageInfo: ImageInfo?,
                    animatable: Animatable?
                ) {
                    val anim = animatable as AnimatedDrawable2
                    anim.setAnimationListener(object : AnimationListener {
                        override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                        override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                            binding.ivImg.visibility = View.INVISIBLE
                        }

                        override fun onAnimationFrame(
                            drawable: AnimatedDrawable2?,
                            frameNumber: Int
                        ) {
                        }

                        override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                            binding.ivImg.visibility = View.VISIBLE
                            binding.ivGifView.visibility = View.INVISIBLE
                        }

                        override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                    })
                }
            }
            binding.ivGifView.controller = controller.build()
        }
    }


}