package com.embibe.embibetvapp.ui.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.databinding.ActivityAchievePotentialBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.potential.Potential
import com.embibe.embibetvapp.ui.fragment.achieve.PotentialFragment
import com.embibe.embibetvapp.ui.fragment.achieve.PotentialPreUGFragment
import com.embibe.embibetvapp.ui.viewmodel.AchievePotentialViewModel
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper

class AchievePotentialActivity : BaseFragmentActivity() {
    private lateinit var achievePotentialViewModel: AchievePotentialViewModel
    private lateinit var binding: ActivityAchievePotentialBinding
    private var fragment = PotentialFragment()
    private var fragmentPotentialPreUG = PotentialPreUGFragment()
    var preferenceHelper = PreferenceHelper()

    private lateinit var testName: String
    private lateinit var testCode: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_achieve_potential)


        achievePotentialViewModel =
            ViewModelProviders.of(this).get(AchievePotentialViewModel::class.java)

        getTruePotentialK12Api()

    }

    private fun getData() {
        if (intent.hasExtra("testName"))
            testName = intent.getStringExtra("testName")!!
        if (intent.hasExtra("testCode"))
            testCode = intent.getStringExtra("testCode")!!

        val bundle = Bundle()
        bundle.putString("testCode", testCode)
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction().replace(R.id.fl_main_layout, fragment).commit()
        UserData.setCurrentUserAsAchiever()
    }

    /*load K12 from api*/
    fun getTruePotentialK12Api() {
        showProgress()
        achievePotentialViewModel.getTruePotentialK12(object :
            BaseViewModel.APICallBacks<List<Potential>> {

            override fun onSuccess(model: List<Potential>?) {
                hideProgress()
                if (model != null) {
                    if (model.isNotEmpty()) {
                        DataManager.instance.setKGPotentialQuestionList(model)
                        getData()
                    } else {
                        makeLog("getStepsToAchieveTruePotential Empty ")
                    }
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(this@AchievePotentialActivity, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getTruePotentialK12Api()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    //showToast(error)
                }
            }
        })
    }

    /*load getTruePotentialPreUG from api*/
    fun getTruePotentialPreUG() {
         showProgress()
        achievePotentialViewModel.getTruePotentialPreUG(object :
            BaseViewModel.APICallBacks<List<Potential>> {

            override fun onSuccess(model: List<Potential>?) {
                hideProgress()
                if (model != null) {

                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                 hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(this@AchievePotentialActivity, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getTruePotentialPreUG()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    //showToast(error)
                }
            }
        })
    }

    /*load getTruePotentialPrePG from api*/
    fun getTruePotentialPrePG() {
        showProgress()
        achievePotentialViewModel.getTruePotentialPrePG(object :
            BaseViewModel.APICallBacks<List<Potential>> {

            override fun onSuccess(model: List<Potential>?) {
                hideProgress()
                if (model != null) {

                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(this@AchievePotentialActivity, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getTruePotentialPrePG()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    //showToast(error)
                }
            }
        })
    }



}