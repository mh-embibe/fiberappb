package com.embibe.embibetvapp.ui.custom

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.ui.interfaces.PointsListener
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin


class ArcView : View {
    private var classTag = ArcView::class.java.simpleName
    private val paint = Paint()
    private var pointA: PointF? = null
    private var pointB: PointF? = null
    private var radius: Int = 0
    private var reverseArrow: Boolean = false
    private var isRelatedKeyConcept: Boolean = false
    private lateinit var pointsListener: PointsListener

    constructor(context: Context?) : super(context)
    constructor(
        context: Context?,
        @Nullable attrs: AttributeSet?
    ) : super(context, attrs)

    constructor(
        context: Context?,
        @Nullable attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr)

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        if (pointA != null && pointB != null) {
            setPaint()
            val density = resources.displayMetrics.density

            val path = Path()
            val x1 = pointA!!.x
            val y1 = pointA!!.y
            val x2 = pointB!!.x
            val y2 = pointB!!.y
            val curveRadius = radius
            val midX = x1 + (x2 - x1) / 2
            val midY = y1 + (y2 - y1) / 2
            val xDiff = midX - x1
            val yDiff = midY - y1
            val angle = atan2(
                yDiff.toDouble(),
                xDiff.toDouble()
            ) * (180 / Math.PI) - 90
            val angleRadians = Math.toRadians(angle)
            val pointX =
                (midX + curveRadius * cos(angleRadians)).toFloat()
            val pointY =
                (midY + curveRadius * sin(angleRadians)).toFloat()
            path.moveTo(x1, y1)
            path.cubicTo(x1, y1, pointX, pointY, x2, y2)
            val pathMeasure = PathMeasure(path, false)
            val pathLength = pathMeasure.length
            val coordinates = FloatArray(2)
            coordinates[0] = x1
            coordinates[1] = y1
            val arcPoints = arrayOfNulls<PointF>(20)
            for (i in arcPoints.indices) {
                pathMeasure.getPosTan(pathLength * i / arcPoints.size - 1, coordinates, null)
                arcPoints[i] = PointF(coordinates[0], coordinates[1])
            }
//            Log.d("ArcView", arcPoints.contentToString())
            canvas.drawPath(path, paint)
            if (!reverseArrow) {
                pointsListener.getPointOnCurve(arcPoints.asList().subList(6, 12).random())
                drawTriangle(
                    canvas,
                    paint,
                    arcPoints[16]!!.x.toInt(),
                    arcPoints[16]!!.y.toInt(),
                    10
                )
            } else {
                pointsListener.getPointOnCurve(arcPoints.asList().subList(10, 16).random())
                drawTriangle(canvas, paint, arcPoints[4]!!.x.toInt(), arcPoints[4]!!.y.toInt(), 10)
            }

//            for(i in arcPoints.indices) {
//                Log.d("ArcView", String.format("Point #%d = (%.0f,%.0f)", i + 1, arcPoints[i]!!.x, arcPoints[i]!!.y));
//                canvas.drawCircle(arcPoints[i]!!.x, arcPoints[i]!!.y, density * 4F, paint)
//            }
        }
        super.onDraw(canvas)
    }

    private fun drawTriangle(canvas: Canvas, paint: Paint?, x: Int, y: Int, width: Int) {
        val halfWidth = width / 2
        val path = Path()
//        path.moveTo(x.toFloat(), (y - halfWidth).toFloat()) // Top
        path.moveTo((x + halfWidth).toFloat(), (y + halfWidth).toFloat()) // Top
//        path.lineTo((x - halfWidth).toFloat(), (y + halfWidth).toFloat()) // Bottom left
        path.lineTo(x.toFloat(), (y - halfWidth).toFloat()) // Bottom left
//        path.lineTo((x + halfWidth).toFloat(), (y + halfWidth).toFloat()) // Bottom right
        path.lineTo((x - halfWidth).toFloat(), (y + halfWidth).toFloat()) // Bottom right
//        path.lineTo(x.toFloat(), (y - halfWidth).toFloat()) // Back to Top
        path.lineTo((x + halfWidth).toFloat(), (y + halfWidth).toFloat()) // Back to Top
        path.close()
        paint?.color = Color.WHITE
        paint?.style = Paint.Style.FILL
        canvas.drawPath(path, paint!!)
    }

    private fun setPaint() {
        val lineGradient =
            intArrayOf(
                ContextCompat.getColor(
                    context,
                    R.color.white_opc72
                ),
                ContextCompat.getColor(
                    context,
                    R.color.white_opc72
                )
            )
        paint.apply {
            strokeWidth = 2f
            style = Paint.Style.STROKE
            shader = LinearGradient(
                pointA!!.x,
                pointA!!.y,
                pointB!!.x,
                pointB!!.y,
                lineGradient,
                null,
                Shader.TileMode.MIRROR
            )
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val parentWidth = MeasureSpec.getSize(widthMeasureSpec)
        val parentHeight = MeasureSpec.getSize(heightMeasureSpec)
        setMeasuredDimension(parentWidth, parentHeight)
    }

    fun setPointA(point: PointF?) {
        pointA = point
    }

    fun setPointB(point: PointF?) {
        pointB = point
    }

    fun setCurveRadius(radius: Int) {
        this.radius = radius
    }

    fun draw() {
        invalidate()
        requestLayout()
    }

    fun reverseArrow(reverseGradient: Boolean) {
        this.reverseArrow = reverseGradient
    }

    fun setPointsListener(listener: PointsListener) {
        pointsListener = listener
    }
}