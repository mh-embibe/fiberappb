package com.embibe.embibetvapp.ui.interfaces

interface PlayerToControlListener {

    fun isPlaying(isPlaying: Boolean) // if isPlaying is true -> Video is in play state else video is in pause state

    fun isForwarded(isForwarded: Boolean)

    fun isRewinded(isRewinded: Boolean)

    fun onVideoReady(isReady: Boolean)

    fun onVideoEnd(isEnd: Boolean)

    fun getPlayerState(state: String)

    // player events
    fun videoLoadingStart(start_position: Long, bit_rate: String)

    fun videoLoadingEnd(bit_rate: String)

    fun videoBufferStart(
        current_position: Long, initial_resolution: String,
        change_resolution: String, current_network_state: String
    )

    fun videoSeek(seek_position: Long, start: Long, end: Long, seek_mode: String)

    fun videoPlayPauseClick(
        current_position: Long, video_player_mode: String,
        current_play_state: String, audio_bitrate: String, video_bitrate: String
    )

    fun videoBufferEnd(
        current_position: Long, change_resolution: String,
        current_network_state: String
    )

    fun videoVolume(
        current_position: Long, current_volume: Int, current_play_state: String,
        volume_change: String
    )

    fun videoPlaybackError(current_position: Long, player_error: String)

    fun videoExit(current_position: Long, current_play_state: String, current_network_state: String)

}
