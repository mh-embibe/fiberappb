package com.embibe.embibetvapp.ui.custom

import android.content.Context
import android.graphics.Canvas
import android.graphics.RectF
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.Scroller

class FocusBorderView(context: Context?) : View(context) {
    private var mTvRecyclerView: TvRecyclerView? = null
    private val mScroller: Scroller = Scroller(context)
    private var mScaleX: Float
    private var mScaleY: Float
    private var mIsDrawGetFocusAnim: Boolean
    private var mIsClicked: Boolean
    private var mLeftFocusBoundWidth: Int
    private var mTopFocusBoundWidth: Int
    private var mRightFocusBoundWidth: Int
    private var mBottomFocusBoundWidth: Int
    var tvRecyclerView: TvRecyclerView?
        get() = mTvRecyclerView
        set(tvRecyclerView: TvRecyclerView?) {
            if (mTvRecyclerView == null) {
                mTvRecyclerView = tvRecyclerView
            }
        }

    fun setSelectPadding(left: Int, top: Int, right: Int, bottom: Int) {
        mLeftFocusBoundWidth = left
        mTopFocusBoundWidth = top
        mRightFocusBoundWidth = right
        mBottomFocusBoundWidth = bottom
    }

    fun startFocusAnim() {
        if (mTvRecyclerView != null) {
            mTvRecyclerView!!.setLayerType(LAYER_TYPE_NONE, null)
            val v: View? =
                TvRecyclerView.getSelectedView(
                    mTvRecyclerView!!
                )
            if (v != null) {
                mIsDrawGetFocusAnim = true
                mScroller.abortAnimation()
                mScroller.startScroll(0, 0, 100, 100, 400)
                invalidate()
            }
        }
    }

    fun dismissGetFocus() {
        mIsDrawGetFocusAnim = false
    }

    fun dismissDraw() {
        mScroller.abortAnimation()
    }

    fun startClickAnim() {
        if (mTvRecyclerView != null) {
            mTvRecyclerView!!.setLayerType(LAYER_TYPE_NONE, null)
            var v: View? = null
            val indexChild: Int =
                TvRecyclerView.getSelectedPosition(
                    mTvRecyclerView!!
                )
            if (indexChild >= 0 && indexChild < mTvRecyclerView!!.adapter!!.itemCount) {
                v =
                    TvRecyclerView.getSelectedView(
                        mTvRecyclerView!!
                    )
            }
            if (v != null) {
                mIsClicked = true
                mScroller.abortAnimation()
                mScroller.startScroll(0, 0, 100, 100, 100)
                invalidate()
            }
        }
    }

    override fun computeScroll() {
        if (mScroller.computeScrollOffset()) {
            val scaleValue: Float = mTvRecyclerView!!.getSelectedScaleValue()
            if (mIsDrawGetFocusAnim) { // calculate scale when get focus animation
                mScaleX = (scaleValue - 1) * mScroller.currX / 100 + 1
                mScaleY = (scaleValue - 1) * mScroller.currY / 100 + 1
            } else if (mIsClicked) { // calculate scale when key down animation
                mScaleX = scaleValue - (scaleValue - 1) * mScroller.currX / 100
                mScaleY = scaleValue - (scaleValue - 1) * mScroller.currY / 100
            }
            invalidate()
        } else {
            if (mIsDrawGetFocusAnim) {
                mIsDrawGetFocusAnim = false
                if (mTvRecyclerView != null) {
                    mTvRecyclerView!!.setLayerType(mTvRecyclerView!!.mLayerType, null)
                    invalidate()
                }
            } else if (mIsClicked) {
                mIsClicked = false
                if (mTvRecyclerView != null) {
                    mTvRecyclerView!!.setLayerType(mTvRecyclerView!!.mLayerType, null)
                    invalidate()
                }
            }
        }
    }

    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        if (mTvRecyclerView != null && mTvRecyclerView!!.hasFocus()) {
            drawGetFocusOrClickScaleAnim(canvas)
            drawFocusMoveAnim(canvas)
            drawFocus(canvas)
        }
    }

    private fun drawGetFocusOrClickScaleAnim(canvas: Canvas) {
        if (mIsDrawGetFocusAnim || mIsClicked) {
            val itemView: View =
                mTvRecyclerView?.let {
                    TvRecyclerView.getSelectedView(
                        it
                    )
                } ?: return
            val itemWidth = itemView.width
            val itemHeight = itemView.height
            val location = IntArray(2)
            itemView.getLocationInWindow(location)
            val drawLocation = IntArray(2)
            getLocationInWindow(drawLocation)
            // draw focus image
            val drawableFocus: Drawable? = mTvRecyclerView!!.getDrawableFocus()
            if (drawableFocus != null) {
                val focusWidth = itemWidth + mLeftFocusBoundWidth + mRightFocusBoundWidth
                val focusHeight = itemHeight + mTopFocusBoundWidth + mBottomFocusBoundWidth
                canvas.save()
                canvas.translate(
                    location[0] - mLeftFocusBoundWidth.toFloat(),
                    location[1] - drawLocation[1] - mTopFocusBoundWidth.toFloat()
                )
                canvas.scale(mScaleX, mScaleY, itemWidth / 2.toFloat(), itemHeight / 2.toFloat())
                drawableFocus.setBounds(0, 0, focusWidth, focusHeight)
                drawableFocus.draw(canvas)
                canvas.restore()
            }
            // draw item view
            canvas.save()
            canvas.translate(location[0].toFloat(), location[1].toFloat())
            canvas.scale(mScaleX, mScaleY, itemWidth / 2.toFloat(), itemHeight / 2.toFloat())
            itemView.draw(canvas)
            canvas.restore()
        }
    }

    private fun drawFocusMoveAnim(canvas: Canvas) {
        if (mTvRecyclerView!!.mIsDrawFocusMoveAnim) {

            mScroller.abortAnimation()
            val curView: View? =
                TvRecyclerView.getSelectedView(
                    mTvRecyclerView!!
                )
            val nextView: View? = mTvRecyclerView!!.getNextFocusView()
            if (nextView != null && curView != null) {
                val locationDrawLayout = IntArray(2)
                getLocationInWindow(locationDrawLayout)
                val location = IntArray(2)
                nextView.getLocationInWindow(location)
                val nextLeft = location[0]
                val nextTop = location[1] - locationDrawLayout[1]
                val nextWidth = nextView.width
                val nextHeight = nextView.height
                curView.getLocationInWindow(location)
                val curLeft = location[0]
                val curTop = location[1] - locationDrawLayout[1]
                val curWidth = curView.width
                val curHeight = curView.height
                val animScale: Float = mTvRecyclerView!!.getFocusMoveAnimScale()
                val focusLeft = curLeft + (nextLeft - curLeft) * animScale
                val focusTop = curTop + (nextTop - curTop) * animScale
                val focusWidth = curWidth + (nextWidth - curWidth) * animScale
                val focusHeight = curHeight + (nextHeight - curHeight) * animScale
                // draw focus image
                val drawableFocus: Drawable? = mTvRecyclerView!!.getDrawableFocus()
                val scaleValue: Float = mTvRecyclerView!!.getSelectedScaleValue()
                if (drawableFocus != null) {
                    canvas.save()
                    canvas.translate(
                        focusLeft - (scaleValue - 1) / 2 * focusWidth,
                        focusTop - (scaleValue - 1) / 2 * focusHeight
                    )
                    canvas.scale(scaleValue, scaleValue, 0f, 0f)
                    drawableFocus.setBounds(
                        0 - mLeftFocusBoundWidth,
                        0 - mTopFocusBoundWidth,
                        (focusWidth + mRightFocusBoundWidth).toInt(),
                        (focusHeight + mBottomFocusBoundWidth).toInt()
                    )
                    drawableFocus.draw(canvas)
                    canvas.restore()
                }
                // draw next item view
                canvas.save()
                canvas.translate(
                    focusLeft - (scaleValue - 1) / 2 * focusWidth,
                    focusTop - (scaleValue - 1) / 2 * focusHeight
                )
                canvas.scale(
                    scaleValue * focusWidth / nextWidth,
                    scaleValue * focusHeight / nextHeight, 0f, 0f
                )
                canvas.saveLayerAlpha(
                    RectF(0F, 0F, width.toFloat(), height.toFloat()),
                    (0xFF * animScale).toInt(), Canvas.ALL_SAVE_FLAG
                )
                nextView.draw(canvas)
                canvas.restore()
                canvas.restore()
                // draw current item view
                canvas.save()
                canvas.translate(
                    focusLeft - (scaleValue - 1) / 2 * focusWidth,
                    focusTop - (scaleValue - 1) / 2 * focusHeight
                )
                canvas.scale(
                    scaleValue * focusWidth / curWidth,
                    scaleValue * focusHeight / curHeight, 0f, 0f
                )
                canvas.saveLayerAlpha(
                    RectF(0F, 0F, width.toFloat(), height.toFloat()),
                    (0xFF * (1 - animScale)).toInt(), Canvas.ALL_SAVE_FLAG
                )
                curView.draw(canvas)
                canvas.restore()
                canvas.restore()
            }
        }
    }

    private fun drawFocus(canvas: Canvas) {
        if (!mIsDrawGetFocusAnim && !mTvRecyclerView!!.mIsDrawFocusMoveAnim && !mIsClicked) {
            val itemView: View? =
                TvRecyclerView.getSelectedView(
                    mTvRecyclerView!!
                )
            if (itemView != null) {
                val itemLocation = IntArray(2)
                itemView.getLocationInWindow(itemLocation)

                val itemWidth = itemView.width
                val itemHeight = itemView.height
                val scaleValue: Float = mTvRecyclerView!!.getSelectedScaleValue()
                val itemPositionX =
                    itemLocation[0] - (scaleValue - 1) / 2 * itemWidth
                val itemPositionY =
                    itemLocation[1] - (scaleValue - 1) / 2 * itemHeight

                //draw focus image
                val drawableFocus: Drawable? = mTvRecyclerView!!.getDrawableFocus()
                val drawWidth = itemWidth + mLeftFocusBoundWidth + mRightFocusBoundWidth
                val drawHeight = itemHeight + mTopFocusBoundWidth + mBottomFocusBoundWidth
                val drawPositionX = itemPositionX - scaleValue * mLeftFocusBoundWidth
                val drawPositionY = itemPositionY - scaleValue * mTopFocusBoundWidth

                if (drawableFocus != null) {
                    canvas.save()
                    canvas.translate(drawPositionX, drawPositionY)
                    canvas.scale(scaleValue, scaleValue, 0f, 0f)
                    drawableFocus.setBounds(0, 0, drawWidth, drawHeight)
                    drawableFocus.draw(canvas)
                    canvas.restore()
                }
                // draw item view
                canvas.save()
                canvas.translate(itemPositionX, itemPositionY)
                canvas.scale(scaleValue, scaleValue, 0f, 0f)
                itemView.draw(canvas)
                canvas.restore()
            }
        }
    }

    companion object {
        private const val TAG = "TvRecyclerView.FB"
        fun getTvRecyclerView(focusBorderView: FocusBorderView): TvRecyclerView? {
            return focusBorderView.mTvRecyclerView
        }

        fun setTvRecyclerView(focusBorderView: FocusBorderView, tvRecyclerView: TvRecyclerView) {
            if (focusBorderView.mTvRecyclerView == null) {
                focusBorderView.mTvRecyclerView = tvRecyclerView
            }
        }
    }

    init {
        mIsDrawGetFocusAnim = false
        mIsClicked = false
        mLeftFocusBoundWidth = 0
        mTopFocusBoundWidth = 0
        mRightFocusBoundWidth = 0
        mBottomFocusBoundWidth = 0
        mScaleX = 0f
        mScaleY = 0f
    }
}
