package com.embibe.embibetvapp.ui.viewholders

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.videoplayer.model.JarModel

class JarViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.item_jar, parent, false)) {

    private var mTitleView: TextView? = null


    init {
        mTitleView = itemView.findViewById(R.id.title)
    }

    fun bind(context: Context, model: JarModel) {
        mTitleView?.text = model.title
        if (model.isHighlighted) {
            mTitleView?.setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
            mTitleView?.setTypeface(mTitleView?.typeface, Typeface.BOLD)
        } else {
            mTitleView?.setTextColor(ContextCompat.getColor(context, R.color.colorWhiteLight))
            mTitleView?.setTypeface(mTitleView?.typeface, Typeface.NORMAL)
        }
    }

}