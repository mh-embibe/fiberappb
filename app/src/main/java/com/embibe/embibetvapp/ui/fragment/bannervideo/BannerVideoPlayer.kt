package com.embibe.embibetvapp.ui.fragment.bannervideo

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.databinding.FragmentBgVideoPlayerBinding
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory


class BannerVideoPlayer : BaseAppFragment(), Player.EventListener {

    lateinit var binding: FragmentBgVideoPlayerBinding
    var player: SimpleExoPlayer? = null
    val TAG = "BannerVideoPlayer"
    var isPlayerInitialized = false
    var repeatVideo = false
    private var mUrl = ""

    private lateinit var onPlayerStateChangeListener: OnPlayerStateChangeListener

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_bg_video_player, container,
            false
        )
        return binding.root
    }

    override fun onStart() {
        super.onStart()
    }

    private fun initPlayer() {
        var playerView = binding.videoView
        var builder = DefaultTrackSelector.ParametersBuilder(requireContext())
        builder.setForceLowestBitrate(true)
        var rf = DefaultRenderersFactory(requireContext())
        var loadControl = DefaultLoadControl()
        var videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(DefaultBandwidthMeter())
        var trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
        var defaultTrackSelector = DefaultTrackSelector.ParametersBuilder().setForceLowestBitrate(
            true
        )
        trackSelector.setParameters(defaultTrackSelector)
        player = ExoPlayerFactory.newSimpleInstance(
            requireContext(),
            rf,
            trackSelector,
            loadControl
        )
        if (repeatVideo) player?.repeatMode = Player.REPEAT_MODE_ONE
        playerView.player = player
        isPlayerInitialized = true
        playerStateListener()
    }

    fun setUrl(url: String) {
        val mediaSource = buildMediaSource(Uri.parse(url))
        if (mUrl != url && mediaSource != null) {
            Log.e(TAG, "mediaSource not null")
            player?.prepare(mediaSource, false, true)
            player?.playWhenReady = true
        }
    }

    private fun buildMediaSource(uri: Uri): MediaSource? {

        val userAgent = "exoplayer-codelab"

        if (uri.lastPathSegment?.contains("mp3")!! || uri.lastPathSegment?.contains("mp4")!!) {
            Log.e(TAG, "Check")
            return ProgressiveMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
                .createMediaSource(uri)
        } else if (uri.lastPathSegment!!.contains("m3u8")) {
            return HlsMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
                .createMediaSource(uri)
        }
        return null
    }

    private fun releasePlayer() {
        if (player != null) {
            player?.release()
        }
    }

    override fun onStop() {
        super.onStop()
        releasePlayer()
    }


    override fun onPause() {
        super.onPause()
        pausePlayback()
    }

    fun pausePlayback() {
        player?.playWhenReady = !player?.playWhenReady!!
    }

    fun setPlayerStateChangeListener(onPlayerStateChangeListener: OnPlayerStateChangeListener) {
        this.onPlayerStateChangeListener = onPlayerStateChangeListener
    }

    private fun playerStateListener() {
        player?.addListener(object : Player.EventListener {

            override fun onPlayerStateChanged(
                playWhenReady: Boolean,
                playbackState: Int
            ) {
                onPlayerStateChangeListener.onPlayerStateChange(playbackState)
                if (playWhenReady && playbackState == Player.STATE_READY) { // Active playback.
                    Log.e(TAG, "video started")
                } else if (playbackState == Player.STATE_ENDED) { //The player finished playing all media
                    //Add your code here
                } else if (playbackState == Player.STATE_BUFFERING) { // Not playing because playback ended, the player is buffering, stopped or
                    Log.e(TAG, "buffering")
                } else if (playbackState == Player.STATE_IDLE) { // Paused by app.
                }
            }
        })
    }


    interface OnPlayerStateChangeListener {
        fun onPlayerStateChange(state: Int)
    }
}