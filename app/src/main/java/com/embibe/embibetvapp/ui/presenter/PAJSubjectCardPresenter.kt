/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.embibe.embibetvapp.ui.presenter

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.leanback.widget.BaseCardView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.model.achieve.home.Content

class PAJSubjectCardPresenter(context: Context) :
    AbstractCardPresenter<BaseCardView>(context) {

    override fun onCreateView(): BaseCardView {
        val cardView = BaseCardView(context, null, R.style.SideInfoCardStyle)
        cardView.addView(LayoutInflater.from(context).inflate(R.layout.card_subject, null))

        cardView.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            val viewAlpha = cardView.findViewById<View>(R.id.view_focus)
            if (hasFocus) {
//                 cardView.showVideoView(itemCurrent.preview_url)
                viewAlpha.alpha = 0.0f
            } else {
//                Utils.showToast(context, "Unfocused")
//                cardView.hideVideoView()
                viewAlpha.alpha = 0.2f
            }
        }
        cardView.isFocusable = true
        return cardView
    }

    override fun onBindViewHolder(card: Any, cardView: BaseCardView) {
        val card = card as Content
        val subjectIcon = cardView.findViewById<AppCompatImageView>(R.id.imgSubjectIcon)
        val tvSubjectName = cardView.findViewById<TextView>(R.id.tvSubjectName)
        val llMain = cardView.findViewById<ConstraintLayout>(R.id.llMain)
        val viewAlpha = cardView.findViewById<View>(R.id.view_focus)
        llMain.background = context.getDrawable(getBackgroundDrawable(getTitle(card)))
        tvSubjectName.text = getTitle(card)
        /* if (card.thumb != "") {
             subjectIcon.visibility = View.VISIBLE
             Glide.with(context).load(card.thumb)
                 .transition(DrawableTransitionOptions.withCrossFade())
                 .into(subjectIcon)
         }*/

        if (getTitle(card).toLowerCase() == "All Subjects".toLowerCase() || getTitle(card).toLowerCase() == "All Units".toLowerCase()) {
            subjectIcon.visibility = View.GONE
            tvSubjectName.gravity = Gravity.START
        }

//        llMain.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
//            if(hasFocus) {
//                viewAlpha.alpha = 1.0f
//            } else {
//                viewAlpha.alpha = 0.1f
//            }
//
//        }
//        llMain.setOnFocusChangeListener { v, hasFocus ->
//            if(hasFocus) {
//                Utils.showToast(context, "focused")
//                viewAlpha.alpha = 1.0f
//            } else {
//                viewAlpha.alpha = 0.1f
//                Utils.showToast(context, "unfocused")
//            }
//        }

    }

    private fun getTitle(card: Content): String {
        return if (card.title.isNotEmpty()) card.title else card.title
    }

    private fun getBackgroundDrawable(subject: String): Int {
        return when (subject.toLowerCase()) {
            "all subjects", "All Units".toLowerCase() -> return R.drawable.ic_bg_all_subjects
            "chemistry" -> return R.drawable.ic_bg_subject_one
            "biology" -> return R.drawable.ic_bg_subject_four
            "physics" -> return R.drawable.ic_bg_subject_three
            "maths", "mathematics" -> return R.drawable.ic_bg_subject_two
            "science" -> return R.drawable.ic_bg_subject_five
            else -> R.drawable.ic_bg_subject_one

        }
    }
}