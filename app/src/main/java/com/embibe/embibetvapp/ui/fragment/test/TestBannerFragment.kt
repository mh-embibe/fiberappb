package com.embibe.embibetvapp.ui.fragment.test

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.CardBannerTestBinding
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.newmodel.LearningMap
import com.embibe.embibetvapp.ui.activity.TestSummaryActivity
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils

class TestBannerFragment : BaseAppFragment() {

    private lateinit var binding: CardBannerTestBinding
    private lateinit var dPadKeysCallback: DPadKeysListener
    private lateinit var navigationMenuCallback: NavigationMenuCallback

    private val classTag = TestBannerFragment::class.java.toString()

    private var whichBtnFocused = ""
    private var canGainFocus = true

    lateinit var banner: BannerData

    companion object {
        const val BTN_TEST = "0"
        const val BTN_MORE_INFO = "1"
        const val BTN_GRADE = "2"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.card_banner_test, container, false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setInitialFocusToTest()
        btnTestListeners()
    }

    private fun setInitialFocusToTest() {
        binding.btnTest.postDelayed({ binding.btnTest.requestFocus() }, 100)
        whichBtnFocused = BTN_TEST
    }

    private fun btnTestListeners() {

        binding.btnTest.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackTestScreenHeroBannerMainBtnFocus()
                canGainFocus = true
                whichBtnFocused = BTN_TEST
            }
        }

        binding.btnTest.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!Utils.isKeyPressedTooFast(500))
                            dPadKeysCallback.isKeyPadDown(true, "Banner")
                    }

                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navigationMenuCallback.navMenuToggle(true)
                    }

                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
//                        if (binding.btnMoreInfo.visibility == View.VISIBLE) {
//                            binding.btnMoreInfo.postDelayed({
//                                binding.btnMoreInfo.requestFocus()
//                            }, 0)
//                        } else if (binding.btnGrade.visibility == View.VISIBLE) {
//                            binding.btnGrade.postDelayed({
//                                binding.btnGrade.requestFocus()
//                            }, 0)
//                        }
                    }

                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        startTestDetailActivity(v)
                        SegmentUtils.trackTestScreenHeroBannerMainBtnCLick()
                    }
                }
            }
            false
        }
    }

    private fun startTestDetailActivity(view: View) {
        if(banner.testList != null && banner.testList.isNotEmpty()) {
            Utils.loadTestSummaryAsync(banner.testList[0], context)
            var testIntent = Intent(view.context, TestSummaryActivity::class.java)
            banner.learning_map = LearningMap()
            testIntent.putExtra(AppConstants.CONTENT, banner.testList[0])
            startActivity(testIntent)
        }
    }


    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }

    fun setLastFocusedBtn() {
        canGainFocus = false
        binding.btnTest.postDelayed({
            binding.btnTest.requestFocus()
        }, 0)
    }

    fun setData(data: BannerData, position: Int) {
        banner = data
        binding.title.text = data.title
        binding.tvNTests.text = "${banner.testList.size} " + getString(R.string.test).toLowerCase()
        binding.tvSubjectName.text = data.subject
        binding.embiumsCount.text = getString(R.string.earn) + " ${banner.embiumCoins}"
        binding.textDescription.text = banner.description
        Utils.changeBannerTitleTextSize(binding.title)
        /* currentSubject = data.subject
         currentId = data.id
         currentTitle = data.title
         trackEventHomeBannerChange(data, position, com.embibejio.coreapp.constant.AppConstants.SLIDE_TYPE_PRACTICE)
         if (data.title_image_url == "") {
             binding.title.text = data.title
             binding.textTime.text = data.duration
             binding.textDescription.text = data.description
             binding.textTopic.text = data.subject
             binding.embiumsCount.text = data.embiumCoins
             binding.textTopic.visibility = View.VISIBLE
             binding.btnMoreInfo.visibility = View.GONE
             binding.textTime.visibility = View.INVISIBLE
             binding.textTime.visibility = View.VISIBLE
             binding.embiumsCount.visibility = View.VISIBLE
         } else {
             binding.textTopic.visibility = View.INVISIBLE
             binding.textTime.visibility = View.INVISIBLE
             binding.embiumsCount.visibility = View.INVISIBLE
             binding.btnMoreInfo.visibility = View.INVISIBLE
         }
*/
        SegmentUtils.trackTestScreenLoadEnd(data.id, data.title, data.subject, data.embiumCoins)
    }
}
