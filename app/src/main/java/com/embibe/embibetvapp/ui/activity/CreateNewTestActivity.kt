package com.embibe.embibetvapp.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.embibe.app.utils.Resource
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ActivityCreateNewTestBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.createtest.configuration.TestConfigurationRes
import com.embibe.embibetvapp.model.createtest.examconfig.ChapterDetails
import com.embibe.embibetvapp.model.createtest.examconfig.SubjectDetails
import com.embibe.embibetvapp.ui.adapter.RvSubjectAdapter
import com.embibe.embibetvapp.ui.viewmodel.CreateNewTestViewModel
import com.embibe.embibetvapp.utils.Status
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager

class CreateNewTestActivity : BaseFragmentActivity(),
    View.OnClickListener, RvSubjectAdapter.OnSelectedSubjectDetails {

    private var examConfigDetails: MutableList<ChapterDetails>? = null
    private lateinit var createNewTestViewModel: CreateNewTestViewModel
    private var subjectAdapter: RvSubjectAdapter? = null
    var selectedSubjects = ArrayList<SubjectDetails>()
    private lateinit var binding: ActivityCreateNewTestBinding

    companion object {
        var createNewTestActivity: Activity? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_new_test)
        createNewTestActivity = this
        initViews()
        getSubjects()
        createNewTestViewModel.examConfigLiveData.observe(
            this, androidx.lifecycle.Observer { resource ->
                if (resource != null) {
                    when (resource.status) {
                        Status.SUCCESS -> {
                            examConfigDetails = resource.data as MutableList<ChapterDetails>
                        }

                        Status.ERROR -> {
                            hideProgress()

                        }
                    }
                }
            })
        loadTestConfigurations(UserData.getEaxmSlugName().replace("exam--", "", true))

    }


    private fun initViews() {
        createNewTestViewModel = ViewModelProviders.of(this).get(CreateNewTestViewModel::class.java)
        binding.llBtnLearntOnEmbibe.setOnClickListener(this)
        binding.btnNext.setOnClickListener(this)
        binding.btnLearntOnEmbibe.setCheckMarkDrawable(R.drawable.bg_transparent_create_new_test)
    }

    private fun getSubjects() {
        createNewTestViewModel.getExamConfig()


        createNewTestViewModel.subjectsLiveData.observe(
            this, androidx.lifecycle.Observer { resource ->
                if (resource != null) {
                    when (resource.status) {
                        Status.LOADING -> {
                            showProgress()
                        }

                        Status.SUCCESS -> {
                            hideProgress()
                            setSubjectAdapter(resource)
                        }

                        Status.ERROR -> {
                            hideProgress()

                        }
                    }
                }
            })
    }

    private fun setSubjectAdapter(resource: Resource<Any>) {
        var response = resource.data as MutableList<SubjectDetails>
        subjectAdapter = RvSubjectAdapter(this, this)
        binding.rvSubjects.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rvSubjects.adapter = subjectAdapter
        subjectAdapter?.setData(response)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.llBtnLearntOnEmbibe -> {
                val value: Boolean = binding.btnLearntOnEmbibe.isSelected
                subjectAdapter?.notifyDataSetChanged()
                if (value) {
                    binding.btnLearntOnEmbibe.setCheckMarkDrawable(R.drawable.bg_transparent_create_new_test)
                    binding.btnLearntOnEmbibe.isSelected = false
                } else {
                    binding.btnLearntOnEmbibe.setCheckMarkDrawable(R.drawable.ic_tick)
                    binding.btnLearntOnEmbibe.isSelected = true
                }

            }
            R.id.btnNext -> {
                if (binding.btnLearntOnEmbibe.isSelected) {
                    selectedSubjects.clear()
                }
                if (selectedSubjects.isNotEmpty()) {
                    var intent = Intent(this, SelectChaptersActivity::class.java)
                    intent.putParcelableArrayListExtra("SubjectDetails", selectedSubjects)
                    intent.putParcelableArrayListExtra(
                        AppConstants.EXAM_CONFIG_DETAILS,
                        examConfigDetails as ArrayList<ChapterDetails>
                    )
                    startActivity(intent)
                } else {
                    Utils.showToast(
                        this,
                        this.resources.getString(R.string.please_select_atleast_one_subject)
                    )
                }

            }

        }
    }


    override fun selectedSubjectDetails(subjectSample: SubjectDetails) {
        binding.btnLearntOnEmbibe.isSelected = false
        binding.btnLearntOnEmbibe.setCheckMarkDrawable(R.drawable.bg_transparent_create_new_test)

        if (selectedSubjects.size == 0)
            selectedSubjects.add(subjectSample)

        for (details in selectedSubjects) {
            if (!details.subjectName!!.contains(subjectSample.subjectName.toString())) {
                selectedSubjects.add(subjectSample)
                break
            }
        }
    }

    override fun unSelectedSubjectDetails(subjectSample: SubjectDetails) {
        binding.btnLearntOnEmbibe.isSelected = false
        binding.btnLearntOnEmbibe.setCheckMarkDrawable(R.drawable.bg_transparent_create_new_test)

        for (i in selectedSubjects) {
            if (i.subjectName!!.contains(subjectSample.subjectName.toString())) {
                selectedSubjects.remove(subjectSample)
                break
            }

        }
    }

    private fun loadTestConfigurations(examName: String) {
        createNewTestViewModel.loadTestConfigurations(
            object : BaseViewModel.APICallBacks<TestConfigurationRes> {
                override fun onSuccess(model: TestConfigurationRes?) {
                    if (model != null) {
                        DataManager.instance.setTestConfigurations(model)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    /*ignore the error screens */
                }

            })
    }


}