package com.embibe.embibetvapp.ui.viewmodel

import android.annotation.SuppressLint
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.model.PAJ.PostPAJUpdate
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.achieve.RankComparison.RankComparison
import com.embibe.embibetvapp.model.achieve.completedJourney.TestSummary
import com.embibe.embibetvapp.model.completionSummary.Topic
import com.embibe.embibetvapp.model.home.HomeModel
import com.embibe.embibetvapp.model.home.SearchRequest
import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.embibe.embibetvapp.model.test.*
import com.embibe.embibetvapp.network.repo.HomeRepository
import com.embibe.embibetvapp.network.retrofit.RetrofitClient
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.exceptions.CustomException
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function4
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class TestViewModel : BaseViewModel() {

    private val repoHome = HomeRepository(RetrofitClient.homeApi!!)

    /*get Scores of testDetails */
    fun getTestDetails(bundleId: String, callBack: APICallBacks<TestDetailsRes>) {
        val model = TestDetailsRequest()
        try {
            model.childId = UserData.getChildId()
            model.testCode = bundleId
        } catch (e: Exception) {
            e.printStackTrace()
        }
        safeApi(apiDetails.getTestDetails(model), callBack)
    }

    /*get attempts for a test taken by the user */
    fun getAttempts(testCode: String, callBack: APICallBacks<TestAttemptsRes>) {
        val childId = UserData.getChildId()
        safeApi(apiDetails.getAttempts(testCode, childId), callBack)
    }

    /*get achieve attempts */
    fun getAchieveAttempts(testCodes: List<String>, callBack: APICallBacks<List<TestAttemptsRes>>) {
        val childId = UserData.getChildId()
        val testCodesStr = android.text.TextUtils.join(",", testCodes)
        safeApi(apiDetails.getAchieveAttempts(testCodesStr, childId), callBack)
    }

    /*get skills for a test taken by the user */
    fun getAchieveSkills(testCodes: List<String>, callBack: APICallBacks<List<TestSummary>>) {
        val childId = UserData.getChildId()
        val testCodesStr = android.text.TextUtils.join(",", testCodes)
        safeApi(apiDetails.getAchieveSkills(testCodesStr, childId), callBack)
    }

    /*get skills for a test taken by the user */
    fun getAchieveAchievePostPAJtestCode(pajId:String,bundleId:String, callBack: APICallBacks<CommonApiResponse>) {
        val model = PostPAJUpdate()
        model.PAJ_ID = pajId
        model.bundle_code = bundleId
        safeApi(apiDetails.getAchieveAchievePostPAJtestCode(model), callBack)
    }

    /*get getAchieveBehaviours for a test taken by the user */
    fun getAchieveBehaviours(
        testCodes: List<String>,
        callBack: APICallBacks<List<ImproveStrategy>>
    ) {
        val childId = UserData.getChildId()
        val testCodesStr = android.text.TextUtils.join(",", testCodes)
        safeApi(apiDetails.getAchieveBehaviours(testCodesStr, childId), callBack)
    }

    /*get summary for a test taken by the user */
    fun getQuestionSummary(testCode: String, callBack: APICallBacks<TestQuestionSummaryRes>) {
        val childId = UserData.getChildId()
        safeApi(apiDetails.getQuestionSummary(testCode, childId), callBack)
    }

    /*get summary for a test taken by the user */
    fun getTopicSummary(
        testCode: String,
        chapter_name: String,
        chapter_learning_map: String,
        callBack: APICallBacks<List<SubFeedbackDetailMenuModel>>
    ) {
        val childId = UserData.getChildId()
        safeApi(
            apiDetails.getTopicSummary(testCode, childId, chapter_name, chapter_learning_map),
            callBack
        )
    }


    //get Questions for a test taken by the user */
    fun getTestQuestions(testCode: String, callBack: APICallBacks<TestQuestionResponse>) {
        safeApi(apiDetails.getQuestions(testCode), callBack)
    }

    /*get skills for a test taken by the user */
    fun getSkills(testCode: String, callBack: APICallBacks<TestSkillsRes>) {
        val childId = UserData.getChildId()
        safeApi(apiDetails.getSkills(testCode, childId), callBack)
    }

    fun getAchieveData(testCode: String, callBack: APICallBacks<TestAchieveRes>) {
        val childId = UserData.getChildId()
        val userId = UserData.getUserID()
        val grade = UserData.getGrade()
        val examName = "10th%20cbse"
        safeApi(apiDetails.getAchieve(testCode, childId, userId, grade, examName), callBack)
    }

    /*get attempts for a test taken by the user */
    fun getTestStatus(
        testCode: String,
        callBack: APICallBacks<HashMap<String, HashMap<String, String>>>
    ) {
        safeApi(apiDetails.getTestStatus(testCode), callBack)
    }

    /*get attempts for a test taken by the user */
    fun getTestReadiness(
        testCode: String,
        callBack: APICallBacks<TestReadiness>
    ) {
        val req = TestReadinessReq()
        req.test_code = testCode
        req.user_id = UserData.getChildId()
        safeApi(apiDetails.testReadiness(req), callBack)
    }

    fun searchResults(
        query: String,
        data: Content?,
        callBack: APICallBacks<List<Content>>
    ) {
        val searchRequest = SearchRequest()
        searchRequest.user_id = Utils.getChildId()
        searchRequest.grade = UserData.getGrade()
        searchRequest.source = "Test Page Summary"
        searchRequest.query = query
        searchRequest.section = "more tests"
        searchRequest.test_code = data?.bundle_id
        searchRequest.xpath = data?.xpath
        searchRequest.formatReferenceId = data?.format_id
        searchRequest.size = 50

        safeApi(repoHome.searchMoreTest(searchRequest), callBack)
    }

    fun fetchTestAttempts(testCode: String): Single<Response<TestAttemptsRes>> {
        return apiDetails.getAttempts(testCode, UserData.getChildId())
    }

    fun fetchTestAchieveData(testCode: String): Single<Response<TestAchieveRes>> {
        val childId = UserData.getChildId()
        val userId = UserData.getUserID()
        val grade = UserData.getGrade()
        val examName = "10th%20cbse"
        return apiDetails.getAchieve(testCode, childId, userId, grade, examName)
    }

    fun fetchTestQuestionData(testCode: String): Single<Response<TestQuestionResponse>> {
        return apiDetails.getQuestions(testCode)
    }

    fun fetchQuestionSummary(testCode: String): Single<Response<TestQuestionSummaryRes>> {
        val childId = UserData.getChildId()
        return apiDetails.getQuestionSummary(testCode, childId)
    }

    fun getTestData(testCode: String, testCodes: List<String>, callback: APIMergeCallBacks<HomeModel>){
        if (UserData.getAchieveOfCurrentUser()){

        }else{
            getTestMergeCall(testCode,callback)
        }

    }

    @SuppressLint("CheckResult")
    fun <T> getTestMergeCall(testCode: String,
        callback: APIMergeCallBacks<T>
    ) {

        val childId = UserData.getChildId()
        val userId = UserData.getUserID()
        val grade = UserData.getGrade()
        val examName = UserData.getEaxmSlugName().replace("exam--", "", true)

        Single.zip(
            apiDetails.getQuestions(testCode).subscribeOn(Schedulers.newThread()),
            apiDetails.getAttempts(testCode, childId).subscribeOn(Schedulers.newThread()),
            apiDetails.getQuestionSummary(testCode, childId).subscribeOn(Schedulers.newThread()),
            apiDetails.getAchieve(testCode, childId, userId, grade, examName).subscribeOn(Schedulers.newThread()),
            Function4<Response<TestQuestionResponse>, Response<TestAttemptsRes>, Response<TestQuestionSummaryRes>,
                    Response<TestAchieveRes>, HomeModel> { mTestQuestionResponse,
                                                                                         mTestAttemptsRes,
                                                                                         mTestQuestionSummaryRes,
                                                                                         mTestAchieveRes->
                // here we combine the results to one.
                return@Function4 mergeTestRes(
                    mTestQuestionResponse,
                    mTestAttemptsRes,
                    mTestQuestionSummaryRes,
                    mTestAchieveRes
                )
            })
            .onErrorResumeNext {
                it.printStackTrace()
                throw CustomException(it.message.toString())
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback.onSuccess(it as T)
            }, {
                callback.onFailed(errorCode, getError(it), "")
            })
    }

    /*combine all mergeTestRes api calls results*/
    private fun mergeTestRes(
        mTestQuestionResponse: Response<TestQuestionResponse>,
        mTestAttemptsRes: Response<TestAttemptsRes>,
        mTestQuestionSummaryRes: Response<TestQuestionSummaryRes>,
        mTestAchieveRes: Response<TestAchieveRes>
    ): HomeModel {
        val mHomeModel = HomeModel()
        if (!mTestQuestionResponse.isSuccessful || mTestQuestionResponse.code() == NO_CONTENT_CODE) {
            mHomeModel.mTestQuestionResponse = null
        } else {
            mHomeModel.mTestQuestionResponse = mTestQuestionResponse.body() as TestQuestionResponse
        }

        if (!mTestAttemptsRes.isSuccessful || mTestAttemptsRes.code() == NO_CONTENT_CODE) {
            mHomeModel.mTestAttempts = null
        } else {
            mHomeModel.mTestAttempts = mTestAttemptsRes.body() as TestAttemptsRes
        }

        if (!mTestQuestionSummaryRes.isSuccessful || mTestQuestionSummaryRes.code() == NO_CONTENT_CODE) {
            mHomeModel.mTestQuestionSummaryRes = null
        } else {
            mHomeModel.mTestQuestionSummaryRes = mTestQuestionSummaryRes.body() as TestQuestionSummaryRes
        }

        if (!mTestAchieveRes.isSuccessful || mTestAchieveRes.code() == NO_CONTENT_CODE) {
            mHomeModel.mTestAchieveRes = null
        } else {
            mHomeModel.mTestAchieveRes = mTestAchieveRes.body() as TestAchieveRes
        }


        return mHomeModel
    }

    @SuppressLint("CheckResult")
    fun <T> getTestDiagnosticMergeCall(testCodes: List<String>,
                             callback: APIMergeCallBacks<T>
    ) {
        val childId = UserData.getChildId()
        val grade = UserData.getGrade()
        val exam = UserData.getExamCode()
        val testCode = android.text.TextUtils.join(",", testCodes)

        Single.zip(
            apiDetails.getAchieveAttempts(testCode, childId).subscribeOn(Schedulers.newThread()),
            apiDetails.getAchieveBehaviours(testCode, childId).subscribeOn(Schedulers.newThread()),
            apiDetails.getAchieveFeedbackTopicSummary(testCode, childId).subscribeOn(Schedulers.newThread()),
            apiDetails.getAchieveFeedbackRanksComparison(testCode, childId,grade,exam).subscribeOn(Schedulers.newThread()),
            Function4<Response<List<TestAttemptsRes>>,
                    Response<List<ImproveStrategy>>,
                    Response<List<Topic>>,
                    Response<List<RankComparison>>,
                    HomeModel> { mTestAttempts,
                                 mBehaviours,
                                 mTestTopicSummaryRes,
                                 mRankComparison->
                // here we combine the results to one.
                return@Function4 mergeDiagnosticTestRes(
                    mTestAttempts,
                    mBehaviours,
                    mTestTopicSummaryRes,
                    mRankComparison
                )
            })
            .onErrorResumeNext {
                it.printStackTrace()
                throw CustomException(it.message.toString())
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback.onSuccess(it as T)
            }, {
                callback.onFailed(errorCode, getError(it), "")
            })
    }

    /*combine all topic summary api calls results*/
    private fun mergeDiagnosticTestRes(
        mTestAttemptsRes: Response<List<TestAttemptsRes>>,
        mBehaviours: Response<List<ImproveStrategy>>,
        mTestTopicummaryRes: Response<List<Topic>>,
        mRankComparison: Response<List<RankComparison>>
    ): HomeModel {
        val mHomeModel = HomeModel()
        if (!mTestAttemptsRes.isSuccessful || mTestAttemptsRes.code() == NO_CONTENT_CODE) {
            mHomeModel.mTestAttemptsList = arrayListOf()
        } else {
            mHomeModel.mTestAttemptsList = mTestAttemptsRes.body() as List<TestAttemptsRes>
        }

        if (!mBehaviours.isSuccessful || mBehaviours.code() == NO_CONTENT_CODE) {
            mHomeModel.mBehaviours = arrayListOf()
        } else {
            mHomeModel.mBehaviours = mBehaviours.body() as List<ImproveStrategy>
        }
        if (!mTestTopicummaryRes.isSuccessful || mTestTopicummaryRes.code() == NO_CONTENT_CODE) {
            mHomeModel.mAchiveTopicummarylist = null
        } else {
            mHomeModel.mAchiveTopicummarylist = mTestTopicummaryRes.body() as List<Topic>
        }
        if (!mRankComparison.isSuccessful || mRankComparison.code() == NO_CONTENT_CODE) {
            mHomeModel.mRankComparison = arrayListOf()
        } else {
            mHomeModel.mRankComparison = mRankComparison.body() as List<RankComparison>
        }


        return mHomeModel
    }

    /*@SuppressLint("CheckResult")
    fun <T> getTestFeedbackData(
        testCode: String,
        callback: APIMergeCallBacks<T>
    ) {
        Single.zip(
            fetchTestAttempts(testCode).subscribeOn(
                Schedulers.newThread()
            ),
            fetchTestAchieveData(testCode).subscribeOn(
                Schedulers.newThread()
            ),
            fetchTestQuestionData(testCode).subscribeOn(Schedulers.newThread()),
            fetchQuestionSummary(testCode).subscribeOn(Schedulers.newThread()),
            Function4<Response<TestAttemptsRes>, Response<TestAchieveRes>, Response<TestQuestionResponse>,Response<TestQuestionSummaryRes>, TestFeedBackModel>
            { testAttemptRes: Response<TestAttemptsRes>, testAchieveRes: Response<TestAchieveRes>, testQuestionResponse: Response<TestQuestionResponse>,testQuestionSummary:Response<TestQuestionSummaryRes> ->

                return@Function4 mergeTestFeedBackDataModel(
                    testAttemptRes,
                    testAchieveRes,
                    testQuestionResponse,
                    testQuestionSummary
                )
            }
        ).onErrorResumeNext {
            it.printStackTrace()
            throw CustomException(it.message.toString())
        }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback.onSuccess(it as T)
            }, {
                callback.onFailed(errorCode, getError(it), "")
            })
    }

    private fun mergeTestFeedBackDataModel(
        testAttemptRes: Response<TestAttemptsRes>,
        testAchieveRes: Response<TestAchieveRes>,
        testQuestionResponse: Response<TestQuestionResponse>,
        testQuestionSummary: Response<TestQuestionSummaryRes>
    ): TestFeedBackModel {
        val testFeedBackModel = TestFeedBackModel()
        testFeedBackModel.testAttemptsRes = testAttemptRes.body()!!
        testFeedBackModel.testAchieveRes = testAchieveRes.body()!!
        testFeedBackModel.testQuestionResponse = testQuestionResponse.body()!!
        testFeedBackModel.testQuestionSummaryRes = testQuestionSummary.body()!!
        return testFeedBackModel
    }*/

}