package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.RowItemUserSwitchBinding
import com.embibe.embibetvapp.ui.interfaces.ComponentClickListener
import com.embibe.embibetvapp.utils.Utils
import com.embibejio.coreapp.model.LinkedProfile
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class UserSwitchAdapter(var context: Context, var listener: ComponentClickListener) :
    RecyclerView.Adapter<UserSwitchAdapter.VHUserSwitchAdapter>() {
    var coroutineScope = CoroutineScope(Dispatchers.Main)
    var currentUserId: String? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    lateinit var onItemClick: ((LinkedProfile) -> Unit)
    private lateinit var binding: RowItemUserSwitchBinding
    private var childList: List<LinkedProfile> = ArrayList()
    var onFocusChange: ((LinkedProfile, Boolean) -> Unit)? = null

    companion object {
        private const val TYPE_USER = 0
        private const val TYPE_ADD_USER = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHUserSwitchAdapter {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.row_item_user_switch, parent,
            false
        )
        return VHUserSwitchAdapter(binding)
    }

    override fun getItemViewType(position: Int): Int {
        var value: Int = 0
        var item = childList[position]
        if (item.userType != "addUser") {
            return TYPE_USER
        } else
            return TYPE_ADD_USER
    }

    override fun getItemCount(): Int {
        return if (childList.isNotEmpty()) childList.size else 0
    }

    fun updateChildData(childList: List<LinkedProfile>) {
        this.childList = childList
        notifyDataSetChanged()
    }


    override fun onBindViewHolder(holder: VHUserSwitchAdapter, position: Int) {
        holder.bind(childList[position], position)
    }

    //ViewHolder
    inner class VHUserSwitchAdapter(var binding: RowItemUserSwitchBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: LinkedProfile, position: Int) {

            if (item.userType.equals("addUser")) {
                binding.tvUsername.text = setFirstName(item.firstName)
                binding.ivItem.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_add
                    )
                )
                var layoutParams = binding.ivItem.layoutParams as ConstraintLayout.LayoutParams
                layoutParams.topMargin = 20
                layoutParams.height = 200
                layoutParams.width = 200
                binding.ivItem.layoutParams = layoutParams
                binding.ivProfileChange.visibility = View.GONE
            } else {
                binding.tvUsername.text = setFirstName(item.firstName)
                Utils.setAvatarImage(context, binding.ivItem, item.profilePic)
                binding.ivProfileChange.visibility = View.VISIBLE
                binding.ivProfileChange.setOnClickListener {
                    onItemClick.invoke(item)
                }
            }

            binding.ivItem.setOnFocusChangeListener { v, hasFocus ->
                onFocusChange?.invoke(item, hasFocus)
                when (hasFocus) {
                    true -> {
                        binding.ivItem.setBackgroundResource(R.drawable.layer_circle_glow_cyan)
                        binding.tvUsername.setTextColor(getColor(context, R.color.white))
                        if (item.userType != "addUser") {
//                            binding.ivProfileChange.visibility = View.VISIBLE
//                            binding.ivProfileChange.animate().translationY(0f).duration = 500
                        }
                    }
                    false -> {
                        binding.ivItem.setBackgroundResource(R.drawable.layer_circle_transparent)
                        binding.tvUsername.setTextColor(
                            getColor(
                                context,
                                R.color.user_text_unselected
                            )
                        )
                    }
                }
            }

            binding.ivItem.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (event.keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            Log.e("TAG", "Check")
                            coroutineScope.launch {
                                delay(10)
                                binding.ivProfileChange.requestFocus()
                            }
                        }
                    }
                }
                false
            }

            binding.ivProfileChange.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    binding.ivProfileChange.setBackgroundResource(R.drawable.ic_iconfinder_icon)
                else
                    binding.ivProfileChange.setBackgroundResource(R.drawable.ic_iconfiner_dull)
            }

            if (item.userId == currentUserId || position == 0) {
                binding.ivItem.requestFocus()
            }
            binding.ivItem.setOnClickListener {
                listener.onClicked("userswitch", item)
            }
        }

        private fun setFirstName(name: String?): String? {
            return if (name != null && name.length > 12 && name.contains(" ")) {
                name.substring(0, name.indexOf(" "))
            } else name
        }
    }

}