package com.embibe.embibetvapp.ui.fragment.signup

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.UnderlineSpan
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.leanback.app.BrowseSupportFragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants.ACTION_TYPE_ADD
import com.embibe.embibetvapp.constant.AppConstants.ACTION_TYPE_EDIT
import com.embibe.embibetvapp.constant.AppConstants.SUBSCRIBER_ID
import com.embibe.embibetvapp.constant.AppConstants.USER_TYPE_CHILD
import com.embibe.embibetvapp.constant.AppConstants.USER_TYPE_PARENT
import com.embibe.embibetvapp.constant.AppConstants.USER_TYPE_STUDENT
import com.embibe.embibetvapp.databinding.FragmentParentProfileBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.adduser.Goal
import com.embibe.embibetvapp.model.auth.login.LoginResponse
import com.embibe.embibetvapp.network.repo.DataRepo
import com.embibe.embibetvapp.ui.activity.*
import com.embibe.embibetvapp.ui.adapter.ParentProfileAdapter
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.SpaceItemDecoration
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.model.LinkedProfile
import com.embibejio.coreapp.preference.PreferenceHelper
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import com.embibe.embibetvapp.constant.AppConstants as AppConstants1

class ParentProfileFragment : BaseAppFragment(), BrowseSupportFragment.MainFragmentAdapterProvider,
    DPadKeysListener {

    private lateinit var binding: FragmentParentProfileBinding
    private lateinit var navigationMenuCallback: NavigationMenuCallback
    private lateinit var signInViewModel: SignInViewModel
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var subscriberId: String

    private lateinit var dataList: List<LinkedProfile?>
    private lateinit var mAdapter: ParentProfileAdapter

    private var mainFragmentAdapter = BrowseSupportFragment.MainFragmentAdapter(this)
    private var isTokenChanged: Boolean = false
    private var profile: LinkedProfile? = null
    private var isTncSelected = false

    var pref = PreferenceHelper()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_parent_profile, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        SegmentUtils.trackParentProfileScreenLoadStart()
        navigationMenuCallback.navMenuToggle(false)
        setUnderLinedTnC()
        initRecycler()
        tncListeners()
        editButtonListeners(view)
        editPersonalDetailsListeners(view)
        logoutButtonListeners()
        if (isTncSelected) {
            binding.textTnCBold.requestFocus()
        }
    }

    private fun setUnderLinedTnC() {
        val content = SpannableString("Terms and Conditions")
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        binding.textTnCBold.text = content
    }

    private fun tncListeners() {
        binding.textTnCBold.setOnClickListener {
            SegmentUtils.tracTermsConditionsClick()
            startActivity(Intent(activity, TermsAndConditionActivity::class.java))
        }
    }

    private fun editPersonalDetailsListeners(view: View) {
        binding.editPersonalDetails.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        binding.editPersonalDetails.clearFocus()
                        navigationMenuCallback.navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        binding.editPersonalDetails.clearFocus()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        startAddEditUserActivity(view, AppConstants1.ACTION_TYPE_EDIT)
                    }
                }
            }
            false
        }

        binding.editPersonalDetails.setOnClickListener {
            SegmentUtils.trackParentProfileScreenEditClick()
        }
    }

    private fun editButtonListeners(view: View) {
        binding.editGoalsBtn.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        binding.editGoalsBtn.clearFocus()
                        navigationMenuCallback.navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        if (binding.editPersonalDetails.visibility == View.GONE) {
                            binding.editGoalsBtn.clearFocus()
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        when (profile?.userType) {
                            USER_TYPE_CHILD, USER_TYPE_STUDENT -> {
                                isTokenChanged = true
                                startActivity(
                                    AddGoalsExamsActivity.getAddGoalsExamIntent(
                                        view.context, profile?.userId!!, profile?.embibe_token
                                    )
                                )
                                SegmentUtils.trackProfilePrimarySecLoadStart()
                            }

                            USER_TYPE_PARENT -> {
                                isTokenChanged = true

                                startActivity(
                                    EditUserActivity.getEditUserIntent(
                                        view.context,
                                        profile?.embibe_token,
                                        Gson().toJson(profile),
                                        USER_TYPE_PARENT
                                    )
                                )
                            }
                        }
                    }
                }
            }
            false
        }

        binding.editGoalsBtn.setOnClickListener {
            SegmentUtils.trackParentProfileScreenEditClick()
        }
    }

    private fun logoutButtonListeners() {
        binding.logoutBtn.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                        doLogout()
                    }

                    KeyEvent.KEYCODE_DPAD_UP -> {
                        if (binding.editPersonalDetails.visibility == View.GONE) {
                            binding.logoutBtn.clearFocus()
                        }
                    }
                }
            }

            false
        }
    }


    private fun gotoSignInActivity(subscribeId: String, goalList: List<Goal>) {
        hideProgress()
        PreferenceHelper().put(SUBSCRIBER_ID, subscribeId)
        DataManager.instance.setGoalsExamsList(goalList)
        startActivity(Intent(activity, SignInActivity::class.java))
        requireActivity().finish()
    }

    private fun doLogout() {
        DataRepo().clearBox()
        subscriberId = PreferenceHelper()[SUBSCRIBER_ID, ""].toString()
        val goalList = DataManager.instance.getGoalsExamsList()
        PreferenceHelper().clear()
        gotoSignInActivity(subscriberId, goalList)
    }

    private fun initRecycler() {
        val manager = GridLayoutManager(context, 1)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        manager.supportsPredictiveItemAnimations()
        binding.tvRecyclerViewProfile.layoutManager = manager
        val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space1)
        binding.tvRecyclerViewProfile.addItemDecoration(SpaceItemDecoration(itemSpace))
        val animator = DefaultItemAnimator()
        binding.tvRecyclerViewProfile.itemAnimator = animator
        mAdapter = ParentProfileAdapter(requireContext())
        mAdapter.setDPadKeysListener(this)
        binding.tvRecyclerViewProfile.adapter = mAdapter
        setAdapterListener()
        setDetails()
        doAsync { getProfileData(mAdapter) }
    }

    private fun setAdapterListener() {
        mAdapter.setOnItemStateListener(object : ParentProfileAdapter.OnItemStateListener {
            override fun onItemClick(view: View?, position: Int) {
                if (position == mAdapter.itemCount - 1 && mAdapter.itemCount < 7) {
                    startAddEditUserActivity(view!!, ACTION_TYPE_ADD)
                }
            }

            override fun onFocusChange(view: View?, position: Int) {
                if (mAdapter.getItemViewType(position) == 1) {
                    hideProfileDetails()
                } else {
                    try {
                        profile = dataList[position]
                        SegmentUtils.trackParentProfileScreenIconFocus(position.toString(), "", "")
                        showProfileDetails(view)
                    } catch (e: Exception) {

                    }
                }
            }
        })
    }

    private fun setDetails() {
        binding.tvRecyclerViewProfile.postDelayed({
            binding.tvRecyclerViewProfile.requestFocus()
            if (binding.tvRecyclerViewProfile.hasFocus() && mAdapter.itemCount > 1) {
                profile = dataList[0]
                setProfileData(profile!!)
            }
        }, 10)
    }

    private fun setProfileData(profile: LinkedProfile) {
        if (profile.userType == USER_TYPE_CHILD) {
            setChildDetails(profile)
        } else {
            setParentDetails(profile)
        }
    }

    private fun startAddEditUserActivity(view: View, actionType: String) {

        when (actionType) {
            ACTION_TYPE_ADD -> {
                startActivity(Intent(view.context, AddUserActivity::class.java))
            }

            ACTION_TYPE_EDIT -> {
                when (profile?.userType) {

                    USER_TYPE_CHILD, USER_TYPE_STUDENT -> {
                        isTokenChanged = true

                        startActivity(
                            EditUserActivity.getEditUserIntent(
                                view.context, profile?.embibe_token,
                                Gson().toJson(profile), USER_TYPE_CHILD
                            )
                        )
                    }

                    USER_TYPE_PARENT -> {
                        isTokenChanged = true

                        startActivity(
                            EditUserActivity.getEditUserIntent(
                                view.context, profile?.embibe_token,
                                Gson().toJson(profile), USER_TYPE_PARENT
                            )
                        )
                    }
                }
            }
        }

        PreferenceHelper().put(AppConstants1.HOST_PAGE, AppConstants1.PROFILE)
        SegmentUtils.trackParentProfileScreenAddIconClick()
    }

    private fun hideProfileDetails() {
        binding.clDetailProfile.visibility = View.GONE
        binding.textTnCBold.visibility = View.GONE
    }

    private fun showProfileDetails(view: View?) {
        binding.clDetailProfile.visibility = View.VISIBLE
        binding.textTnCBold.visibility = View.VISIBLE
        if (view != null && view.hasFocus()) {
            if (profile?.userType == USER_TYPE_PARENT) {
                setParentDetails(profile!!)
            } else if (profile?.userType == USER_TYPE_CHILD || profile?.userType.equals(
                    "student",
                    true
                )
            ) {
                setChildDetails(profile!!)
            }
        }
    }

    private fun getProfileData(adapter: ParentProfileAdapter) {
        dataList = UserData.getLinkedProfileList()!!
        val newDataList: MutableList<LinkedProfile> = dataList as MutableList<LinkedProfile>
        adapter.updateChildData(newDataList)
        adapter.notifyDataSetChanged()
    }

    private fun setParentDetails(profile: LinkedProfile) {
        UserData.setCurrentProfile(profile)

        binding.valueEmail.text =
            if (profile.email?.isEmpty()!!) getString(R.string.text_hyphen) else profile.email.toString()

        binding.valueMobile.text =
            if (profile.mobile == null || profile.mobile?.isEmpty()!!) getString(R.string.text_hyphen)
            else profile.mobile.toString()
        updateUIForParent()
    }

    private fun updateUIForParent() {
        binding.labelPrimaryGoal.visibility = View.GONE
        binding.valuePrimaryGoal.visibility = View.GONE
        binding.labelPrimaryGoalExam.visibility = View.GONE
        binding.valuePrimaryGoalExam.visibility = View.GONE
        binding.labelSecondaryGoal.visibility = View.GONE
        binding.valueSecondaryGoal.visibility = View.GONE
        binding.labelSecondaryGoalExam.visibility = View.GONE
        binding.valueSecondaryGoalExam.visibility = View.GONE
        binding.editPersonalDetails.visibility = View.GONE
        binding.labelMobile.visibility = View.VISIBLE
        binding.valueMobile.visibility = View.VISIBLE
        binding.editGoalsBtn.text = getString(R.string.edit)
    }

    private fun setChildDetails(profile: LinkedProfile) {
        UserData.setCurrentProfile(profile)

        val lstExamNames = ArrayList<String>()
        binding.valueEmail.text =
            if (profile.email?.isEmpty()!!) getString(R.string.text_hyphen) else profile.email.toString()
        binding.valueMobile.text =
            if (profile.mobile == null || profile.mobile?.isEmpty()!!) getString(R.string.text_hyphen) else profile.mobile.toString()

        val primaryGoal =
            profile.primary_goal_code?.let { DataManager.instance.getGoalNameByCode(it) }

        binding.valuePrimaryGoal.text =
            if (primaryGoal != "") primaryGoal else getString(R.string.text_hyphen)

        val primaryExam =
            profile.primary_goal_code?.let { goalCode ->
                profile.primary_exam_code?.let { examCode ->
                    DataManager.instance.getExamNameByCode(goalCode, examCode)
                }
            }

        binding.valuePrimaryGoalExam.text =
            if (primaryExam != "") primaryExam else getString(R.string.text_hyphen)

        val secondaryGoal =
            profile.secondary_exam_goal_id?.let { DataManager.instance.getGoalNameByCode(it) }

        binding.valueSecondaryGoal.text =
            if (secondaryGoal.isNullOrEmpty()) getString(R.string.text_hyphen) else secondaryGoal

        profile.secondary_exam_goal_id?.let { goalCode ->
            profile.secondary_exams?.let {
                val examCodes: List<String> = profile.secondary_exams!!.split(",")
                for (i in examCodes) {
                    val examNames = DataManager.instance.getExamNameByCode(goalCode, i.trim())
                    if (examNames != "")
                        lstExamNames.add(examNames)
                }
            }
        }

        binding.valueSecondaryGoalExam.text =
            if (lstExamNames.isNotEmpty()) TextUtils.join(
                ", ", lstExamNames
            ) else getString(R.string.text_hyphen)

        updateUIForChildren()
    }


    private fun updateUIForChildren() {
        binding.labelPrimaryGoal.visibility = View.VISIBLE
        binding.valuePrimaryGoal.visibility = View.VISIBLE
        binding.labelPrimaryGoalExam.visibility = View.VISIBLE
        binding.valuePrimaryGoalExam.visibility = View.VISIBLE
        binding.labelSecondaryGoal.visibility = View.VISIBLE
        binding.valueSecondaryGoal.visibility = View.VISIBLE
        binding.labelSecondaryGoalExam.visibility = View.VISIBLE
        binding.valueSecondaryGoalExam.visibility = View.VISIBLE
        binding.editPersonalDetails.visibility = View.VISIBLE
        binding.labelMobile.visibility = View.GONE
        binding.valueMobile.visibility = View.GONE
        binding.editGoalsBtn.text = getString(R.string.edit_goals)
    }

    private fun getColor(color: Int): Int {
        return context?.let { ContextCompat.getColor(it, color) }!!
    }

    override fun getMainFragmentAdapter(): BrowseSupportFragment.MainFragmentAdapter<ParentProfileFragment> {
        return mainFragmentAdapter
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {
        if (isDown) {
            when (from) {
                "ParentAdapter" -> {

                    binding.tvRecyclerViewProfile.postDelayed({
                        binding.tvRecyclerViewProfile.clearFocus()
                    }, 5)

                    if (binding.editPersonalDetails.visibility == View.VISIBLE) {

                        binding.editPersonalDetails.postDelayed({
                            binding.editPersonalDetails.requestFocus()

                        }, 5)
                    } else {

                        binding.editGoalsBtn.postDelayed({
                            binding.editGoalsBtn.requestFocus()
                        }, 5)
                    }
                }
            }
        }
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
        if (isLeft) {
            when (from) {
                "ParentAdapter" -> {
                    binding.tvRecyclerViewProfile.postDelayed({
                        binding.tvRecyclerViewProfile.clearFocus()
                    }, 10)
                    navigationMenuCallback.navMenuToggle(true)
                }
            }
        }
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {}
    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {}
    override fun isKeyPadUp(isUp: Boolean, from: String) {}

    fun restoreLastSelection() {
        binding.tvRecyclerViewProfile.postDelayed({
            binding.tvRecyclerViewProfile.clearFocus()
            binding.tvRecyclerViewProfile.requestFocus()
        }, 10)
    }

    private fun getLinkedProfiles() {
        showProgress()
        signInViewModel.getLinkedProfilesApi(object : BaseViewModel.APICallBacks<LoginResponse> {
            override fun onSuccess(model: LoginResponse?) {
                if (model != null && model.success) {
                    makeLog("Authentication Success")
                    updateSignInResponse(model)
                } else {
                    makeLog("getLinkedProfilesApi api failed")
                    hideProgress()
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                makeLog("getLinkedProfilesApi api error $error")
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getLinkedProfiles()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }
        })
    }

    private fun updateSignInResponse(response: LoginResponse) {
        doAsync {
            updateLoginPreferences(response)
            uiThread {
                getProfileData(mAdapter)
                setProfileData(profile!!)
                hideProgress()
            }
        }
    }

    private fun updateLoginPreferences(response: LoginResponse) {
        UserData.updateLinkedUser(response)
        profile = UserData.getLinkedProfileList()!!.single { s -> s?.userId == profile?.userId }
    }


    override fun onResume() {
        super.onResume()
        SegmentUtils.trackParentProfileScreenLoadEnd()
        if (isTokenChanged) {
            isTokenChanged = false
            val tempToken = PreferenceHelper()[AppConstants1.TEMP_EMBIBE_TOKEN, ""].toString()
            PreferenceHelper().put(AppConstants1.EMBIBETOKEN, tempToken)
            PreferenceHelper().put(AppConstants1.TEMP_EMBIBE_TOKEN, "")
            if (AppConstants1.ISPROFILEUPDATED) getLinkedProfiles()
        } else {
            mAdapter.setOnItemStateListener(null)
            getProfileData(mAdapter)
            setAdapterListener()
            binding.tvRecyclerViewProfile.postDelayed({
                binding.tvRecyclerViewProfile.requestFocus()
                if (binding.tvRecyclerViewProfile.hasFocus() && mAdapter.itemCount > 1) {
                    profile = dataList[0]
                    setProfileData(profile!!)
                }
            }, 3)
        }

    }
}