package com.embibe.embibetvapp.ui.viewmodel

import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.model.prerequisites.PrerequisitesResponse
import com.embibe.embibetvapp.network.repo.HomeRepository
import com.embibe.embibetvapp.network.retrofit.RetrofitClient

class VideosViewModel : BaseViewModel() {

    var homeRepo = HomeRepository(RetrofitClient.homeApi!!)

    fun getPrerequisites(
        conceptId: String,
        contentId: String,
        callBack: APICallBacks<PrerequisitesResponse>
    ) {
        safeApi(homeRepo.fetchPrerequisites(conceptId, contentId), callBack)
    }
}