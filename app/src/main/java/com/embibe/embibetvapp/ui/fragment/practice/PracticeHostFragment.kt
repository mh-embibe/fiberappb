package com.embibe.embibetvapp.ui.fragment.practice

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentHostPracticeBinding
import com.embibe.embibetvapp.jobScheduler.MyInternetTest
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.model.HeaderBannerData
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.activity.SwitchGoalExamActivity
import com.embibe.embibetvapp.ui.interfaces.*
import com.embibe.embibetvapp.ui.progressBar.LoadingManager
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.MainViewModel
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*

class PracticeHostFragment : BaseAppFragment(),
    DPadKeysListener, RowsToBannerListener,
    BannerDataListener, NavigationMenuCallback,
    BannerImageDataToHostListener, LoadingManager,
    MyInternetTest.NetworkSchedulerService.ConnectivityReceiverListener {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: FragmentHostPracticeBinding
    private lateinit var mainViewModel: MainViewModel

    private var practiceBannerFragment = PracticeBannerFragment()
    private var practiceRowsFragment = PracticeRowsFragment()
    private var practiceBannerImgFlipper = PracticeBannerImgFlipper()
    private var hostToNavigationListener: HostToNavigationListener? = null
    private var classTag = PracticeHostFragment::class.java.toString()
    private var internetCheckMLD = MutableLiveData<Boolean>()
    private var lastFocusedContent = "Banner"
    private var contentAvailable = true

    val pref = PreferenceHelper()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_host_practice, container, false)
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        return binding.root
    }

    private lateinit var listener: SharedPreferences.OnSharedPreferenceChangeListener


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        getData()
        hostToNavigationListener?.showNavigation(false)
        MyInternetTest.scheduleJob()
        setNetworkObserver()
        registerChangeExamButtonListeners()

    }

    private fun registerChangeExamButtonListeners() {
        binding.changeGradeExamButton.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        startActivity(Intent(context, SwitchGoalExamActivity::class.java))
                    }
                }
            }
            false
        }
    }

    private fun getData() {
        doAsync {
            val practiceData = homeViewModel.fetchSectionByPageName(AppConstants.PRACTICE)
            uiThread {
                if (practiceData.isNotEmpty()) {/*has data*/
                    DataManager.instance.setHomePractice(arrayListOf(), null)
                    updateAPIresult()
                } else {
                    /*No data*/
                    getApiCall(isAsync = false)
                }
            }
        }


    }

    private fun updateAPIresult() {
        setFragment(practiceBannerImgFlipper, binding.flBannerBgPractice.id)
        setFragment(practiceBannerFragment, binding.flBannerPractice.id)
        setFragment(practiceRowsFragment, binding.flRowPractice.id)
    }

    fun listenDBUpdates() {
        listener =
            SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
                if (key.equals(AppConstants.PRACTICE)) {
                    if (isVisible) {
                    }
                }
            }
        pref.preferences?.registerOnSharedPreferenceChangeListener(listener)
    }


    override fun onPause() {
        super.onPause()
        pref.preferences?.unregisterOnSharedPreferenceChangeListener(listener)
    }


    fun getApiCall(isAsync: Boolean) {
        if (!isAsync) {
            showProgress()
        }
        homeViewModel.homePracticeApi(object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
            override fun onSuccess(model: List<ResultsEntity>?) {
                if (model != null) {
                    if (!isAsync) {
                        DataManager.instance.setHomePractice(model as ArrayList<ResultsEntity>,
                            object : BaseViewModel.DataCallback<ArrayList<ResultsEntity>> {
                                override fun onSuccess(model: ArrayList<ResultsEntity>?) {
                                    if (model != null) {
                                        hideProgress()
                                        updateAPIresult()
                                    }
                                }

                            })

                    }
                }

            }

            override fun onFailed(code: Int, error: String, msg: String) {
                if (!isAsync) {
                    hideProgress()
                    if (Utils.isApiFailed(code)) {
                        Utils.showError(
                            context,
                            code,
                            object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: String) {
                                    getApiCall(isAsync)
                                }

                                override fun onDismiss() {

                                }
                            })
                    }
                }
            }
        })
    }

    private fun setNetworkObserver() {
        internetCheckMLD.observe(viewLifecycleOwner, Observer { value ->
            if (value)
                isInternetAvailable()
        })
    }

    private fun setFragment(fragment: Fragment, layoutResId: Int) {
        childFragmentManager.beginTransaction().replace(
            layoutResId,
            fragment
        ).commit()

    }

    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        when (childFragment) {
            is PracticeBannerFragment -> {
                childFragment.setDPadKeysListener(this)
                childFragment.setNavigationMenuListener(this)
            }
            is PracticeRowsFragment -> {
                childFragment.setNavigationMenuListener(this)
                childFragment.setRowsToBannerListener(this)
                childFragment.setDPadKeysListener(this)

            }
            is PracticeBannerImgFlipper -> {
                childFragment.bannerImageDataListener(this)
            }
        }
    }

    fun setHostToNavigationListener(callback: HostToNavigationListener) {
        this.hostToNavigationListener = callback
    }

    override fun onDetach() {
        super.onDetach()
        hostToNavigationListener = null
    }

    override fun onHeaderItemChange(data: HeaderBannerData) {
        Log.i(classTag, "data from header: $data")
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        Log.i(classTag, "Network: $isConnected")
        //if (!isConnected)
        /* Toast.makeText(App.context, "Please check your internet connection", Toast.LENGTH_SHORT)
             .show()*/
    }

    private fun isNetworkConnected(): Boolean {
        val cm =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null && cm.activeNetworkInfo.isConnected
    }

    private fun isInternetAvailable(): Boolean {
        return try {
            val command = "ping -c 1 google.com"
            if (Runtime.getRuntime().exec(command).waitFor() == 0) {
                internetCheckMLD.postValue(false)
            }
            return Runtime.getRuntime().exec(command).waitFor() == 0

        } catch (e: Exception) {
            Log.e(classTag, " isInternetAvailable ${e.message}")
            internetCheckMLD.postValue(true)
            false
        }
    }

    override fun onResume() {
        super.onResume()
        listenDBUpdates()
        MyInternetTest.scheduleJob()
        App.setConnectivityListener(this)
        Log.e(classTag, "onResume ")
    }

    override fun navMenuToggle(toShow: Boolean) {
        hostToNavigationListener?.showNavigation(toShow)
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {
        if (isDown) {
            when (from) {
                "Rows" -> {
                    slideBannerUp()
                    practiceBannerImgFlipper.stopImgFlipping()
                }
                "Banner" -> {
                    slideBannerUp()
                    practiceRowsFragment.setFirstRowFocus()
                    practiceBannerImgFlipper.stopImgFlipping()
                }
            }
        }
    }

    private fun slideBannerUp() {
        practiceRowsFragment.isBannerVisible(false)
        val bannerBGPracticeOA = ObjectAnimator.ofFloat(
            binding.flBannerBgPractice,
            View.TRANSLATION_Y, 0f, Values.BANNER_TRANSLATION_Y
        )
        val bannerPracticeOA = ObjectAnimator.ofFloat(
            binding.flBannerPractice,
            View.TRANSLATION_Y, 0f, Values.BANNER_TRANSLATION_Y
        )
        val rowsPracticeOA = ObjectAnimator.ofFloat(
            binding.flRowPractice,
            View.TRANSLATION_Y, 0f, Values.ROWS_TRANSLATION_Y
        )
        val animatorSet = AnimatorSet()
        animatorSet.apply {
            playTogether(bannerBGPracticeOA, bannerPracticeOA, rowsPracticeOA)
            duration = 500
            doOnEnd {
                binding.flBannerPractice.visibility = View.INVISIBLE
                binding.flBannerBgPractice.visibility = View.INVISIBLE
            }
            start()
        }

        binding.flRowPractice.setPadding(0, 50, 0, 0)

        lastFocusedContent = "Rows"
    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {
    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {
        if (isEnter) {
            when (from) {
                "Rows" -> {
                    if (cardType.toLowerCase(Locale.getDefault()) == "practice")
                        hostToNavigationListener?.switchToPractice()
                    else if (cardType.toLowerCase(Locale.getDefault()) == "learn") {
                        hostToNavigationListener?.switchToLearn()
                    }
                    //currently commented out because of undefined functionality requirement
//                    hostToNavigationListener?.switchToLearn()
                }
            }
        }
    }

    override fun rowsToBanner() {
        practiceRowsFragment.isBannerVisible(true)
        binding.flBannerPractice.requestFocus()
        practiceBannerFragment.setLastFocusedBtn()
        practiceBannerImgFlipper.startImgFlipping()

        val bannerBGPracticeOA = ObjectAnimator.ofFloat(
            binding.flBannerBgPractice,
            View.TRANSLATION_Y, Values.BANNER_TRANSLATION_Y, 0f
        )
        val bannerPracticeOA = ObjectAnimator.ofFloat(
            binding.flBannerPractice,
            View.TRANSLATION_Y, Values.BANNER_TRANSLATION_Y, 0f
        )
        val rowsPracticeOA = ObjectAnimator.ofFloat(
            binding.flRowPractice,
            View.TRANSLATION_Y, Values.ROWS_TRANSLATION_Y, 0f
        )
        val animatorSet = AnimatorSet()
        animatorSet.apply {
            playTogether(bannerBGPracticeOA, bannerPracticeOA, rowsPracticeOA)
            duration = 500
            doOnStart {
                binding.flBannerPractice.visibility = View.VISIBLE
                binding.flBannerBgPractice.visibility = View.VISIBLE
            }
            start()
        }

        binding.flRowPractice.setPadding(0, 0, 0, 0)
        lastFocusedContent = "Banner"
    }

    override fun notifyBanner(notifyMessage: String) {
        when (notifyMessage) {
            "Synced" -> {
                binding.flBannerPractice.postDelayed({
                    binding.flBannerPractice.requestFocus()
                }, 10)
            }
        }
    }

    override fun notifyHost(notifyMessage: String) {
        when (notifyMessage) {
            AppConstants.NO_CONTENT -> {
                contentAvailable = false
                toggleNoContentView(View.VISIBLE)
            }
            AppConstants.CONTENT_AVAILABLE -> {
                contentAvailable = true
                toggleNoContentView(View.GONE)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun toggleNoContentView(visibility: Int) {
        binding.noContentTv.visibility = visibility
        binding.changeGradeExamButton.visibility = visibility
        binding.flBannerBgPractice.visibility = View.GONE - visibility
        binding.flBannerPractice.visibility = View.GONE - visibility
        binding.flRowPractice.visibility = View.GONE - visibility

        if (visibility == View.VISIBLE) {
            binding.noContentTv.text = getString(R.string.text_goal_not_supported) + " " +
                    DataManager.instance.getExamNameByCode(
                        UserData.getGoalCode(),
                        UserData.getExamCode()
                    )
            binding.changeGradeExamButton.postDelayed({
                binding.changeGradeExamButton.requestFocus()
            }, 100)
        } else {
            binding.changeGradeExamButton.postDelayed({
                binding.changeGradeExamButton.clearFocus()
            }, 100)
        }
    }

    fun restoreLastSelection(fromNavigation: Boolean) {
        when (contentAvailable) {
            false -> {
                binding.changeGradeExamButton.postDelayed({
                    binding.changeGradeExamButton.requestFocus()
                }, 100)
            }
            true -> {
                if (lastFocusedContent.equals("Banner", true)) {
                    practiceBannerFragment.setLastFocusedBtn()
                } else {
                    binding.flRowPractice.requestFocus()
                    practiceRowsFragment.restoreLastSelection(fromNavigation)
                }
            }
        }
    }

    override fun setBannerImageData(data: BannerData, position: Int) {
        practiceBannerFragment.setData(data, position)
    }


    fun resumeBannerSliding() {
        practiceBannerImgFlipper.startImgFlipping()
    }

    fun pauseBannerSliding() {
        practiceBannerImgFlipper.stopImgFlipping()
    }

    object Values {
        const val BANNER_TRANSLATION_Y = -550f
        const val ROWS_TRANSLATION_Y = -600f
    }

}
