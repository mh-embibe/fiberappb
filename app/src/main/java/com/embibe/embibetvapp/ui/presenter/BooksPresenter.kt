package com.embibe.embibetvapp.ui.presenter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import androidx.core.content.res.ResourcesCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.utils.BookVideoCardView
import com.embibe.embibetvapp.utils.Utils

class BooksPresenter(context: Context) :
    AbstractCardPresenter<BookVideoCardView>(context) {
    override fun onCreateView(): BookVideoCardView {
        val cardView = BookVideoCardView(context, R.style.SideInfoCardStyle)
        cardView.isFocusable = true
        cardView.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            val viewAlpha = cardView.findViewById<View>(R.id.view_focus)
            if (hasFocus) {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_focused_alpha_value
                )
            } else {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_unfocused_alpha_value
                )
            }
        }
        return cardView
    }

    override fun onBindViewHolder(item: Any, cardView: BookVideoCardView) {
        val card = item as Content
        val imgThumbnailView: ImageView = cardView.findViewById(R.id.iv_thumbnail)

        val requestOptions = RequestOptions().transform(RoundedCorners(12))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(context).applyDefaultRequestOptions(requestOptions)
            .load(card.thumb)
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.books_placeholder)
            .into(imgThumbnailView)
    }

}