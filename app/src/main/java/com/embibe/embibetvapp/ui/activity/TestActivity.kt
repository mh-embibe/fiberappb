package com.embibe.embibetvapp.ui.activity

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.END_SESSION
import com.embibe.embibetvapp.constant.AppConstants.NO_MORE_QUESTIONS_DIALOG
import com.embibe.embibetvapp.constant.AppConstants.TEST_SESSION_ALERT_DIALOG
import com.embibe.embibetvapp.databinding.ActivityPracticeBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.achieve.completedJourney.TestSummary
import com.embibe.embibetvapp.model.home.HomeModel
import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.embibe.embibetvapp.model.test.TestQuestionResponse
import com.embibe.embibetvapp.model.test.TestSkillsRes
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.interfaces.CommonAndroidAPI
import com.embibe.embibetvapp.ui.interfaces.CommonAndroidAPI.AndroidAPIOnCallListener
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener
import com.embibe.embibetvapp.ui.interfaces.VoiceListener
import com.embibe.embibetvapp.ui.viewmodel.PracticeViewModel
import com.embibe.embibetvapp.ui.viewmodel.TestViewModel
import com.embibe.embibetvapp.utils.ConnectionManager
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_practice.*
import org.json.JSONException
import org.json.JSONObject

class TestActivity : BaseFragmentActivity(), AndroidAPIOnCallListener,
    ErrorScreensBtnClickListener, VoiceListener {

    private lateinit var binding: ActivityPracticeBinding
    private lateinit var practiceViewModel: PracticeViewModel
    private lateinit var testViewModel: TestViewModel
    private lateinit var testQuestionResponse: TestQuestionResponse
    private var mLastKeyDownTime: Long = 0

    private var isDiagnosticTest = false
    private var isDiagnosticTestCompleted = false
    private var previousTestCode = ""
    private var pref = PreferenceHelper()
    private var contentId: String? = ""
    private var practiceId = ""
    private var testName = ""
    var topicLearnPathName = ""
    var learnPathName = ""
    var learnPathFormatName = ""
    var formatId = ""
    lateinit var content: Content
    var conceptId = ""
    var query = ""
    var token = ""
    var title = ""
    var subject = ""
    var bundlePath = ""
    var bundleCode = ""
    var childId = 0L

    companion object {
        const val TEMPLATE_TEST = "test/index.html"
        val TAG = TestActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        voiceListener = this
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_practice)
        practiceViewModel = ViewModelProviders.of(this).get(PracticeViewModel::class.java)
        testViewModel = ViewModelProviders.of(this).get(TestViewModel::class.java)

        showProgress()

        //childId = pref[AppConstants.CURRENT_PROFILE_USER_ID, 0].toLong()
        token = pref[AppConstants.EMBIBETOKEN, ""]
        childId = UserData.getChildId().toLong()
        query = "token=$token&child_id=$childId&invokeModule=test"
        if (intent.hasExtra(AppConstants.IS_DIAGNOSTIC) &&
            intent.getBooleanExtra(AppConstants.IS_DIAGNOSTIC, false)
        ) {
            var childName = UserData.getName()
            query += "&type=diagnostic&user_name=$childName"
        }
        if (intent.hasExtra(AppConstants.TEST_XPATH)) {
            bundlePath = intent.getStringExtra(AppConstants.TEST_XPATH)
            query += "&bundlePath=$bundlePath"
        } else {
            makeLog("TEST_XPATH not received")
        }


        if (intent.hasExtra(AppConstants.TEST_NAME)) {
            testName = intent.getStringExtra(AppConstants.TEST_NAME)
        }
        if (intent.hasExtra(AppConstants.TEST_CODE)) {
            bundleCode = intent.getStringExtra(AppConstants.TEST_CODE)
            //query += "&bundleCode=$bundleCode"
        }
        if (intent.hasExtra(AppConstants.CONCEPT_ID)) {
            conceptId = intent.getStringExtra(AppConstants.CONCEPT_ID)
        }
        if (intent.hasExtra(AppConstants.CONTENT)) {
            content = getContent(intent)
        }

        if (intent.hasExtra(AppConstants.IS_DIAGNOSTIC_TEST_COMPLETED)) {
            isDiagnosticTestCompleted =
                intent.getBooleanExtra(AppConstants.IS_DIAGNOSTIC_TEST_COMPLETED, false)
        }
        if (intent.hasExtra(AppConstants.PREVIOUS_TEST_CODE)) {
            previousTestCode = intent.getStringExtra(AppConstants.PREVIOUS_TEST_CODE) ?: ""
        }

        isDiagnosticTest = intent.getBooleanExtra(AppConstants.IS_DIAGNOSTIC, false)

        setBackGround()
        loadWebView()

    }


    private fun loadWebView() {
        if (ConnectionManager.instance.hasNetworkAvailable()) {
            setTestWebView()

        } else {
            hideProgress()
            Utils.showError(this@TestActivity, ApiConstants.API_CODE_NO_NETWORK,
                object : BaseViewModel.ErrorCallBacks {
                    override fun onRetry(msg: String) {
                        loadWebView()
                    }

                    override fun onDismiss() {

                    }
                })
        }
    }

    private fun setBackGround() {
        Glide.with(this)
            .load(Utils.getSubjectBackground(subject))
            .placeholder(R.drawable.banner_placeholder)
            .error(R.drawable.banner_placeholder)
            .into(imageView)
    }

    private fun setTestWebView() {
        webView.webChromeClient = WebChromeClient()
        webView.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                Log.d("WEBVIEW", "Web view ready")
                // closeLoader()
                hideProgress()
            }
        }
        webView.settings.allowFileAccess = true
        webView.isLongClickable = false
        webView.settings.javaScriptEnabled = true
        webView.settings.cacheMode = WebSettings.LOAD_DEFAULT
        webView.settings.domStorageEnabled = true
        webView.setBackgroundColor(Color.argb(0, 0, 0, 0))
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
        webView.addJavascriptInterface(CommonAndroidAPI(this), CommonAndroidAPI.NAME)
        if (Build.VERSION.SDK_INT > 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        } else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }
        val base = filesDir.path.toString() + "/assets/"

        //val template =
        "file://$base$TEMPLATE_TEST?$query"
        val template = "http://52.172.137.100:8000?$query"
        //disableLongPress(webView)
        Log.i("Test url", template)
        webView.loadUrl(template)
    }

    fun disableLongPress(mWebView: WebView?) {
        if (mWebView != null) {
            mWebView.setOnLongClickListener(View.OnLongClickListener { v: View? -> true })
            mWebView.isLongClickable = false
            mWebView.isHapticFeedbackEnabled = false
        }
    }

    override val question: String?
        get() = ""

    override fun getQuestion(questionCode: String?): String? {
        return ""
    }

    override val concept: String?
        get() = ""
    override val hint: String?
        get() = ""

    override fun setAnswer(): String? {
        return ""
    }

    override fun getSections(): String? {
        return ""
    }

    override fun getAnswer(): String? {
        return ""
    }

    override fun getQuestionSet(sectionId: String?): String? {
        return ""
    }

    override fun getData(): String? {
        return ""
    }

    override fun getData(data: String) {

    }


    override fun getEvent(data: String) {
        try {
            val obj = JSONObject(data)
            val type = obj.getString(AppConstants.DETAILS_TYPE)
            when (type.toLowerCase()) {
                AppConstants.EVENT -> {
                    SegmentUtils.trackPracticeEvent(obj.getJSONObject(AppConstants.DATA))
                }
            }

        } catch (ex: JSONException) {
            Log.e(TAG, ex.toString())
        }
    }

    override fun sendToFeedback(testCode: String) {
        showProgress()
        if (isDiagnosticTestCompleted) {
            /* call update API*/
            getAchieveAchievePostPAJtestCode(intent.getStringExtra(AppConstants.PAJ_ID), testCode)
        } else {
            getSkills(testCode)
        }

    }

    private fun getAchieveAchievePostPAJtestCode(pajId: String, testCode: String) {
        showProgress()
        testViewModel.getAchieveAchievePostPAJtestCode(
            pajId,
            testCode,
            object : BaseViewModel.APICallBacks<CommonApiResponse> {
                override fun onSuccess(model: CommonApiResponse?) {
                    if (model != null) {
                        getAchieveSkills(listOf(bundleCode, testCode))
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLogI("$code - $error - $msg")
                    getAchieveSkills(listOf(bundleCode, testCode))
                }

            })
    }

    override fun showLoader() {

    }

    override fun closeLoader() {
        if (DataManager.isLoading)
            hideProgress()
    }

    override fun hideKeyboard() {
        closeKeyBoard(binding.root)
    }

    override fun focusUp() {
    }

    override fun focusDown() {
    }

    override fun focusRight() {
    }

    override fun focusLeft() {
    }

    //method used to call from Test Webview
    override fun initVoiceInput(value: String) {
        runOnUiThread {
            startRecognition()
        }
    }

    override fun getSelectedQuestion(qListJson: String) {
    }

    override fun setQuestion(singQuestionJson: String) {
    }

    override fun getSelectedVideo(videoData: String) {

    }

    override fun selectAnswer(value: String?) {

    }

    override fun deselectAnswer(value: String?) {

    }

    override fun inputAnswer(value: String?) {

    }

    override fun close() {
        webView.destroy()
        finish()
    }

    override val questionNumber: Int
        get() = 0
    override val sectionName: String?
        get() = ""
    override val chapterJson: String?
        get() = ""
    override val chapterName: String?
        get() = ""
    override val singQuestionJson: String?
        get() = TODO("Not yet implemented")
    override val questionListJson: String?
        get() = TODO("Not yet implemented")

    override fun showConcept(): Boolean? {
        return true
    }

    override fun screenUpdate(type: String) {
        when (type) {
            END_SESSION -> {
                /* practiceViewModel.updatedContentStatus(
                     bundleCode,
                     AppConstants.STATUS_CONTENT_TYPE_TEST,
                     0,
                     AppConstants.CONTENT_STATUS_INPROGRESS,
                     content.learning_map.topicLearnPathName,
                     content.learnpath_name,
                     content.learnpath_format_name,
                     content.learning_map.format_id,
                     conceptId,
                     if (::content.isInitialized) content.duration.toString() else "0"
                 )*/
                close()
            }
        }
    }

    override fun onBackPressed() {
        binding.webView.evaluateJavascript("CommonWebviewAPI.isKeyboardVisible()") { value ->
            println(value)
            if (value == "false")
                Utils.showAlertDialog(
                    TEST_SESSION_ALERT_DIALOG, supportFragmentManager,
                    this
                )
        }
    }

    private fun noMoreQuestionUI() {
        Utils.showAlertDialog(
            NO_MORE_QUESTIONS_DIALOG, supportFragmentManager,
            this
        )
    }


    private fun getSkills(testCode: String) {
        testViewModel.getSkills(testCode, object : BaseViewModel.APICallBacks<TestSkillsRes> {
            override fun onSuccess(testSkill: TestSkillsRes?) {

                if (testSkill != null && testSkill.questions > 0) {
                    testViewModel.getTestMergeCall(
                        testCode,
                        callback = object : BaseViewModel.APIMergeCallBacks<HomeModel> {
                            override fun onSuccess(model: HomeModel?) {
                                DataManager.instance.setTestSkills(Gson().toJson(testSkill))
                                DataManager.instance.setTestAttempts(Gson().toJson(model?.mTestAttempts))
                                DataManager.instance.setTestAchieve(Gson().toJson(model?.mTestAchieveRes))
                                DataManager.instance.setTestQuestionSummary(Gson().toJson(model?.mTestQuestionSummaryRes))
                                DataManager.instance.setTestQuestion(Gson().toJson(model?.mTestQuestionResponse))
                                moveToNext(testCode)
                            }

                            override fun onFailed(code: Int, error: String, msg: String) {
                                makeLog("onFailed : getTestMergeCall $")
                            }

                        })


                } else {
                    Handler().postDelayed({
                        getSkills(testCode)
                    }, 2000)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                makeLogI("$code - $error - $msg")
            }

        })
    }

    private fun moveToNext(testCode: String) {
        showProgress()
        val testFeedbackIntent: Intent
        if (isDiagnosticTestCompleted) {
            /*ImprovementActivity*/
            testFeedbackIntent = Intent(this, ImprovementActivity::class.java)
            testFeedbackIntent.putExtra(AppConstants.IS_DIAGNOSTIC, isDiagnosticTest)
        } else if (UserData.getAchieveOfCurrentUser() || isDiagnosticTest) {
            /*move to AchievePotentialActivity*/
            testFeedbackIntent = Intent(this, AchievePotentialActivity::class.java)
            testFeedbackIntent.putExtra(AppConstants.IS_DIAGNOSTIC, isDiagnosticTest)
            DataManager.instance.isDiagnosticTest = isDiagnosticTest
            hideProgress()
        } else {
            /*move to TestFeedbackActivity*/
            testFeedbackIntent = Intent(this, TestFeedbackActivity::class.java)
        }
        testFeedbackIntent.putExtra("testName", testName)
        testFeedbackIntent.putExtra("testCode", testCode)

        startActivity(testFeedbackIntent)
        this.finish()
    }


    override fun onVoiceInit() {

    }

    //method used to give callback from VoiceController
    override fun onVoiceSuccess(data: String) {
        updateSearchQuery(data)
    }

    override fun onVoiceFailed(msg: String) {

    }

    override fun onVoiceCancel(msg: String) {

    }

    private fun updateSearchQuery(searchQuery: String) {
        webView.evaluateJavascript(
            "CommonWebviewAPI.onFinishVoiceInput('" + searchQuery + "')",
            null
        )
    }


    /*achieve Skills*/
    private fun getAchieveSkills(testCodes: List<String>) {
        testViewModel.getAchieveSkills(
            testCodes,
            object : BaseViewModel.APICallBacks<List<TestSummary>> {
                override fun onSuccess(testSkill: List<TestSummary>?) {
                    if (testSkill != null) {
                        testViewModel.getTestDiagnosticMergeCall(
                            testCodes,
                            callback = object : BaseViewModel.APIMergeCallBacks<HomeModel> {
                                override fun onSuccess(model: HomeModel?) {

                                    DataManager.instance.setTestSkills(Gson().toJson(testSkill))
                                    DataManager.instance.setAchieveAttemptsList(Gson().toJson(model?.mTestAttemptsList))
                                    DataManager.instance.setBehaviours(Gson().toJson(model?.mBehaviours))
                                    DataManager.instance.setRankComparisons(Gson().toJson(model?.mRankComparison))
                                    DataManager.instance.setTopicSummary(Gson().toJson(model?.mAchiveTopicummarylist))

                                    moveToTestActivity(testCodes)
                                }

                                override fun onFailed(code: Int, error: String, msg: String) {

                                }

                            })
                    }


                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    hideProgress()
                    makeLogI("$code - $error - $msg")
                }

            })
    }

    private fun moveToTestActivity(testCode: List<String>) {
        hideProgress()
        val intent = Intent(this, ImprovementActivity::class.java)
        intent.putExtra("testName", testName)
        intent.putExtra("testCode", testCode[1])
        intent.putExtra(AppConstants.PREVIOUS_TEST_CODE, testCode[0])
        intent.putExtra(AppConstants.IS_DIAGNOSTIC_TEST_COMPLETED, isDiagnosticTestCompleted)
        startActivity(intent)
        this.finish()
    }


    override fun movePAJNextActivity(data: String) {

    }


}
