package com.embibe.embibetvapp.ui.adapter

import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.databinding.ItemTestsBinding
import com.embibe.embibetvapp.newmodel.Content

class TestsAdapter : RecyclerView.Adapter<TestsAdapter.TestsViewHolder>() {

    var list = ArrayList<Content>()
    var onItemClick: ((Content) -> Unit)? = null
    var onBookMarkClick: ((Content) -> Unit)? = null
    var onKeyPress: ((Int) -> Unit)? = null
    var onFocusChange: ((Content, Boolean) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestsViewHolder {
        val binding: ItemTestsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_tests, parent, false
        )
        return TestsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (list.size > 0) list.size else 0
    }

    override fun onBindViewHolder(holder: TestsViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun setData(list: ArrayList<Content>) {
        this.list = list
        notifyDataSetChanged()
    }

    fun updateBookMark(isBookMarked: Boolean) {
        list[0].isBookmarked = isBookMarked
        notifyDataSetChanged()
    }

    inner class TestsViewHolder(var binding: ItemTestsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Content) {
            binding.test = item

            setBackgrounds(binding, itemView)
            updateBookMarkUI(binding, itemView, item.isBookmarked)
            setFocusListeners(binding, itemView, item)
        }

        init {
            binding.testLayout.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {//only when key is pressed down
                    when (keyCode) {

                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                            onItemClick?.invoke(list[adapterPosition])
                        }
                    }
                }
                return@setOnKeyListener false
            }

            binding.bookMarkLayout.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {//only when key is pressed down
                    when (keyCode) {

                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                            onBookMarkClick?.invoke(list[adapterPosition])
                        }
                    }
                }
                return@setOnKeyListener false
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                Log.w("app-log", "setOnFocusChangeListener , $hasFocus ")
                onFocusChange?.invoke(list[adapterPosition], hasFocus)
            }
        }

        private fun setBackgrounds(
            binding: ItemTestsBinding,
            itemView: View
        ) {
            Handler().postDelayed({
                if (binding.testLayout.hasFocus()) {
                    binding.btnStartTest.background =
                        itemView.context.getDrawable(R.drawable.button_shape_rec_yellow)
                    binding.btnStartTest.setTextColor(itemView.resources.getColor(R.color.black))
                } else {
                    if (binding.btnStartTest.text.contains(App.context.getString(R.string.start_test))) {
                        binding.btnStartTest.background =
                            itemView.context.getDrawable(R.drawable.button_shape_rec_yellow_unselected)
                    } else {
                        binding.btnStartTest.background =
                            itemView.context.getDrawable(R.drawable.bg_card_unselected_grey)
                        binding.btnStartTest.setTextColor(itemView.resources.getColor(R.color.white))
                    }
                }
            }, 400)
        }

        private fun setFocusListeners(
            binding: ItemTestsBinding,
            itemView: View,
            item: Content
        ) {
            binding.testLayout.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    binding.btnStartTest.background =
                        itemView.context.getDrawable(R.drawable.button_shape_rec_yellow)
                    binding.btnStartTest.setTextColor(itemView.resources.getColor(R.color.black))

                } else {
                    if (binding.btnStartTest.text.contains(App.context.getString(R.string.start_test))) {
                        binding.btnStartTest.background =
                            itemView.context.getDrawable(R.drawable.button_shape_rec_yellow_unselected)
                    } else {
                        binding.btnStartTest.background =
                            itemView.context.getDrawable(R.drawable.bg_card_unselected_grey)
                        binding.btnStartTest.setTextColor(itemView.resources.getColor(R.color.white))
                    }
                }
            }

            binding.bookMarkLayout.setOnFocusChangeListener { v, hasFocus ->
                updateBookMarkUI(binding, itemView, item.isBookmarked)
            }
        }

        private fun updateBookMarkUI(
            binding: ItemTestsBinding,
            itemView: View,
            isBookMarked: Boolean
        ) {

            if (binding.bookMarkLayout.hasFocus()) {
                if (isBookMarked) {
                    binding.btnBookMark.setImageResource(R.drawable.ic_bookmark_filled_white)
                } else {
                    binding.btnBookMark.setImageResource(R.drawable.ic_bookmark_stroke_white)
                }
            } else {
                if (isBookMarked) {
                    binding.btnBookMark.setImageResource(R.drawable.ic_bookmark_filled_grey)
                } else {
                    binding.btnBookMark.setImageResource(R.drawable.ic_bookmark_stroke_grey)
                }
            }
        }
    }
}