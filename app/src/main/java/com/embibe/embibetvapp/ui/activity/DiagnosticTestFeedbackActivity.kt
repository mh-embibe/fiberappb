package com.embibe.embibetvapp.ui.activity

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.View.LAYER_TYPE_SOFTWARE
import android.view.ViewTreeObserver
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.base.TestFeedbackBaseFragmentActivity
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.ALL_ATTEMPTS
import com.embibe.embibetvapp.constant.AppConstants.ALL_SUBJECTS
import com.embibe.embibetvapp.databinding.ActivityDiagnosticTestFeedbackBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.achieve.achieveFeedback.AchieveFeedbackRes
import com.embibe.embibetvapp.model.achieve.achieveFeedback.Step
import com.embibe.embibetvapp.model.test.*
import com.embibe.embibetvapp.ui.adapter.DiagnosticTestFeedbackAdapter
import com.embibe.embibetvapp.ui.adapter.TestBarAdapter
import com.embibe.embibetvapp.ui.adapter.TestJarAdapter
import com.embibe.embibetvapp.ui.adapter.TopSkillAdapter
import com.embibe.embibetvapp.ui.interfaces.CommonAndroidAPI
import com.embibe.embibetvapp.ui.viewmodel.TestViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.layout_correctly_answered_info.view.*
import kotlinx.android.synthetic.main.layout_grade_info.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject
import java.io.InputStream
import java.lang.Math.abs
import java.util.*
import kotlin.collections.ArrayList

class DiagnosticTestFeedbackActivity : TestFeedbackBaseFragmentActivity(), TestJarAdapter.Listener,
    CommonAndroidAPI.AndroidAPIOnCallListener {

    private lateinit var binding: ActivityDiagnosticTestFeedbackBinding
    private lateinit var barAdapter: TestBarAdapter
    private lateinit var topSkillAdapter: TopSkillAdapter
    private lateinit var menuAdapter: DiagnosticTestFeedbackAdapter
    private lateinit var jarAdapter: TestJarAdapter
    private lateinit var coroutineScope: CoroutineScope

    private var testAttemptsRes: TestAttemptsRes? = null
    private var testSkillsRes: TestSkillsRes? = null
    private var testQuestionRes: TestQuestionResponse? = null
    private var testAchieveRes: TestAchieveRes? = null

    private var mySectionSummaryList = ArrayList<SectionSummary>()
    private var listTopSkill = ArrayList<SkillSet>()
    private var list = ArrayList<String>()

    //    private var correctlyAnsweredMotionLayout: MotionLayout? = null
    private var isFirstRowExpanded: Boolean = false
    private var layoutAQVisibility: Boolean = false
    private var lastKeyPress = 0L
    private var rowsHM = TreeMap<Int, View>()
    private var viewFocusList = ArrayList<View>()
    private var viewFocusCounter = 0
    private var progressLayoutWidth: Int = 0
    private var currentQWASubject = ""
    private var currentAttemptType = ALL_ATTEMPTS
    var testName: String = ""
    var bundleId: String = ""
    var height: Int = 0
    val TAG = this.javaClass.name
    private lateinit var testViewModel: TestViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SegmentUtils.trackTestFeedbackScreenLoadStart()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_diagnostic_test_feedback)
        coroutineScope = CoroutineScope(Dispatchers.Main)
//        correctlyAnsweredMotionLayout = binding.layoutChart.layoutCorrectlyAnsweredInfo.constraintLayout
        testViewModel = ViewModelProviders.of(this).get(TestViewModel::class.java)
        addViewsToHashMap()
        addFocusViewsToList()
        viewFocusList[viewFocusCounter].requestFocus()

        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)

        getAllData()
        setBackgrounds(requestOptions)

        setFirstRowListeners()
        setClickToWatchListener()
        setQWAKeyListener()
//        setQWAButtonListener()
        setAndAnimateProgress()
//        setQWAFocusListener()

        //getAchieveBehaviours(listOf("m1234","m4321"))
    }

    override fun onResume() {
        super.onResume()
        SegmentUtils.trackTestFeedbackScreenLoadEnd()
    }

    private fun setDropDownQWA() {
        currentQWASubject = getSubjectList()[0]
        binding.layoutQWA.tvDropDownSubject.text = currentQWASubject
        if (getSubjectList().size > 1) {
            binding.layoutQWA.tvDropDownSubject.setOnClickListener {
                SegmentUtils.trackTestFeedbackScreenSubjectWiseAnalyseDropdownClick()
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Choose Subject")
                val array = getSubjectList().toTypedArray()
                builder.setItems(
                    array
                ) { dialog, which ->
                    if (getSubjectList()[which].toLowerCase().contains(ALL_SUBJECTS)) {
                        setQuestionList(getQuestionListData(currentQWASubject))
                    } else {
                        currentQWASubject = getSubjectList()[which]
                        binding.layoutQWA.tvDropDownSubject.text = currentQWASubject
                        setQuestionList(getQuestionListData(currentQWASubject))
                    }
                }

                val dialog = builder.create()
                dialog.show()
            }
        }
    }

    private fun setQWAFocusListener() {
        binding.layoutQWA.btnUnAttempted.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (layoutAQVisibility) {
                            currentFocus?.clearFocus()
                            binding.layoutQWA.wvQWA.requestFocus()
                        } else {
                            currentFocus?.clearFocus()
                            binding.layoutQWA.layoutJar.rvJar.requestFocus()
                        }
                        return@setOnKeyListener true
                    }
                }
            }
            false
        }
        binding.layoutQWA.btnTooFastCorrectAttempts.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (layoutAQVisibility) {
                            currentFocus?.clearFocus()
                            binding.layoutQWA.wvQWA.requestFocus()
                        } else {
                            currentFocus?.clearFocus()
                            binding.layoutQWA.layoutJar.rvJar.requestFocus()
                        }
                        return@setOnKeyListener true
                    }

                }
            }
            false
        }
        binding.layoutQWA.wvQWA.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                Toast.makeText(this, "focused webview", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun getAllData() {

        with(DataManager.instance) {
            testAttemptsRes = Gson().fromJson(getTestAttempts(), TestAttemptsRes::class.java)
            testSkillsRes = Gson().fromJson(getTestSkills(), TestSkillsRes::class.java)
            testQuestionRes = Gson().fromJson(getTestQuestion(), TestQuestionResponse::class.java)
            //testQuestionRes.paper.sections.entries.
//            testAchieveRes = Gson().fromJson(getTestAchieve(), TestAchieveRes::class.java)
        }

        parseAttemptInToQuestion()

        if (intent.hasExtra("testName"))
            testName = intent.getStringExtra("testName")!!
        if (intent.hasExtra("TEST_CODE"))
            bundleId = intent.getStringExtra("TEST_CODE")!!
        readFromLocal()

        setTitle()
        setChart()
        setTopSkillRecycler()
        setAchieveRecycler()
        setAchieveMetrics()
//        setAchieveGif()
        setDropDownQWA()
        setQWAWebView()
        setJarRecycler()
    }

    private fun parseAttemptInToQuestion() {
        if (testQuestionRes != null) {
            for (key in testQuestionRes!!.paper.sections.keys) {
                val section = testQuestionRes!!.paper.sections[key]

                for (question in section!!.questions) {
                    val attempt = getAttemptByCode(question.code)
                    if (attempt != null) {
                        question.answerSelectedOption = attempt.answerSelectedOption
                        question.attemptTypeBadge = attempt.attemptTypeBadge
                        question.tags = attempt.tags!!
                        question.explanation = attempt.explanation
                        question.correctAnswerCsv = attempt.correctAnswerCsv
                        question.marks_obtained = attempt.marks!!
                        question.first_looked_at = attempt.tFirstLook!!
                        question.attempted_at = attempt.tFirstSave!!
                    }
                }
            }
        }
    }


    private fun getQuestionListData(
        subject: String,
        type: String = ALL_ATTEMPTS
    ): ArrayList<Question> {
        var qlist = ArrayList<Question>()
        currentAttemptType = type
        if (testQuestionRes != null) {
            for (key in testQuestionRes!!.paper.sections.keys) {
                val section = testQuestionRes!!.paper.sections[key]
                if (section!!.name == subject || subject == ALL_SUBJECTS) {
                    for (question in section.questions) {
                        if (question.attemptTypeBadge == type || type == ALL_ATTEMPTS) {
                            qlist.add(question)
                        }
                    }
                }
            }
        }
        return qlist
    }

    private fun moveToTestQWA(question: Question, pos: Int) {
        with(DataManager.instance) {
            setTestQuestion(Gson().toJson(testQuestionRes!!))
            setCurrentQuestion(Gson().toJson(question))
        }
        startActivity(
            TestQWAActivity.getQWAIntent(
                this, pos, currentQWASubject, currentAttemptType
            )
        )
    }

    private fun getSubjectList(): List<String> {
        var slist = ArrayList<String>()
        if (testQuestionRes != null) {
            for (key in testQuestionRes!!.paper.sections.keys) {
                val section = testQuestionRes!!.paper.sections[key]
                slist.add(section!!.name)
            }
            if (slist.size > 1) {
                slist.add(0, ALL_SUBJECTS)
            }
        }
        return slist
    }


    private fun getAttemptByCode(code: String): Attempt? {
        if (testAttemptsRes != null) {
            for (attempt in testAttemptsRes!!.attempts!!) {
                if (attempt.questionCode == code)
                    return attempt
            }
        }
        return null
    }


    private fun readFromLocal() {

        for (i in testAttemptsRes!!.scoreListInfo!!.sectionSummaryList!!.indices) {
            mySectionSummaryList.add(testAttemptsRes!!.scoreListInfo!!.sectionSummaryList!![0])
        }
        for (i in testSkillsRes!!.skillset!!.indices) {
            listTopSkill.add(testSkillsRes!!.skillset?.get(i)!!)
        }
        Log.e(TAG, "size: ${mySectionSummaryList.size}")
        Log.e(TAG, "Top Skill size: ${listTopSkill.size}")
    }

    private fun setTitle() {
        binding.tvTitle.text = getString(R.string.marks_and_overall_performance_on_and)
            .replace("*test_name*", testName)
            .replace(
                "*exam_name*", DataManager.instance.getExamNameByCode(
                    UserData.getGoalCode(), UserData.getExamCode()
                )
            )
    }

    private fun setBackgrounds(requestOptions: RequestOptions) {
        Glide.with(this).setDefaultRequestOptions(requestOptions)
            .load(R.drawable.video_banner)
            .transition(DrawableTransitionOptions.withCrossFade())
            .transform(RoundedCorners(20))
            .into(binding.layoutClickToWatchAndTopSkill.layoutClickToWatch.ivThumbnail)

        Glide.with(this).setDefaultRequestOptions(requestOptions)
            .load(R.drawable.bg_first_row_feedback)
            .transition(DrawableTransitionOptions.withCrossFade())
            .transform(RoundedCorners(20))
            .into(binding.layoutChart.ivBg)
    }

    private fun setChart() {
//        list.add("30")
//        list.add("45")
//        list.add("39")
//        list.add("-3")
//        list.add("-39 marks")
//        list.add("20 marks")
//        barAdapter = DiagnosticDummyBarAdapter()
//        barAdapter.setItems(list)
//        binding.layoutChart.layoutCorrectlyAnsweredInfo.rvMarksBar.adapter = barAdapter
//        binding.layoutChart.layoutCorrectlyAnsweredInfo2.rvMarksBar.adapter = barAdapter
//
//        setDataForChart()
        barAdapter = TestBarAdapter()
        barAdapter.setItems(mySectionSummaryList)
        binding.layoutChart.layoutCorrectlyAnsweredInfo.rvMarksBar.adapter = barAdapter

        setDataForChart()
    }

    private fun setDataForChart() {

        var scoreText = binding.layoutChart.layoutCorrectlyAnsweredInfo.tv_score
        var questionStr = getString(R.string.test_feedback_questions)
        var marksObtained = testSkillsRes?.correct_answer
        var totalMarks = testSkillsRes?.questions
//        val correctAnswer = testSkillsRes?.correct_answer
//        val totalQuestions = testSkillsRes?.questions
        var bothValueAsStr = "$marksObtained/$totalMarks"
        scoreText.text = Utils.getSpannable(
            this, "$bothValueAsStr $questionStr",
            R.color.colorGreen, 0, marksObtained.toString().length
        )

        binding.layoutChart.layoutGradeInfo.valueGrade.text = testAttemptsRes?.scoreListInfo?.grade
//        binding.layoutChart.layoutCorrectlyAnswered.tvScore.text = getScoreText("chart")
//        binding.layoutChart.layoutTestScore.tvScoreNominator.text =
//            if (testAttemptsRes?.scoreListInfo!!.myScore != 0.0)
//                testAttemptsRes?.scoreListInfo!!.myScore.roundToInt().toString()
//            else
//                testAttemptsRes?.scoreListInfo!!.myScore.roundToInt().toString()
//
//        binding.layoutChart.layoutTestScore.tvScoreDenominator.text =
//            if (testAttemptsRes?.scoreListInfo!!.maxScore != 0.0)
//                testAttemptsRes?.scoreListInfo!!.maxScore.roundToInt().toString()
//            else
//                testAttemptsRes?.scoreListInfo!!.maxScore.roundToInt().toString()
//
//        binding.layoutChart.layoutTestScore.tvScoreNominator.setTextColor(getColor(R.color.colorReddishPink))
    }

    private fun setTopSkillRecycler() {

        topSkillAdapter = TopSkillAdapter()
        topSkillAdapter.setItems(listTopSkill)
        binding.layoutClickToWatchAndTopSkill.layoutTopSkill.layoutCorrectlyAnswered.rvScoreBar.adapter =
            topSkillAdapter

        var scoreText =
            binding.layoutClickToWatchAndTopSkill.layoutTopSkill.layoutTestScore.tvScore
        var questionStr = getString(R.string.test_feedback_questions)
        var marksObtained = testSkillsRes?.correct_answer
        var totalMarks = testSkillsRes?.questions
//        val correctAnswer = testSkillsRes?.correct_answer
//        val totalQuestions = testSkillsRes?.questions
        var bothValueAsStr = "$marksObtained/$totalMarks"
        scoreText.text =
            Utils.getSpannable(
                this, "$bothValueAsStr $questionStr",
                R.color.colorGreen, 0, marksObtained.toString().length
            )

//        binding.layoutClickToWatchAndTopSkill.layoutTopSkill.layoutTestScore.tvCorrectlyAnswered.text =
//            getScoreText("top_skill")

//        binding.layoutClickToWatchAndTopSkill.layoutTopSkillPrevious.layoutTestScore.tvTopSkillMemory.text =
//            testSkillsRes!!.top_skill
    }

    private fun getScoreText(type: String): String {

        val questionStr = getString(R.string.test_feedback_questions)
        val correctAnswer = 30
        val totalQuestions = 100
        val bothValueAsStr = "$correctAnswer/$totalQuestions"

        return when (type) {
            "chart" -> {
                Utils.getSpannable(
                    this, "$bothValueAsStr $questionStr",
                    R.color.colorReddishPink, 0, correctAnswer.toString().length
                ).toString()
            }
            "top_skill" -> {
                Utils.getSpannable(
                    this, "$bothValueAsStr $questionStr",
                    R.color.colorReddishPink, 0, bothValueAsStr.toString().length
                ).toString()
            }

            else -> ""
        }
    }

    private fun setAchieveRecycler() {
        menuAdapter = DiagnosticTestFeedbackAdapter()
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

//        binding.layoutAchieve.recycler.adapter = menuAdapter
//        binding.layoutAchieve.recycler.layoutManager = layoutManager
        //menuAdapter.setData(getAchieveFeedback().steps as ArrayList<Step>)
    }

    /*load the AchieveFeedbackRes model data from the json response file*/
    private fun getAchieveFeedback(): AchieveFeedbackRes {
        return fromJson(achieveFeedbackResFromAsset() ?: "")
    }

    /*load the learnCoachNextContentRes model data from the json response file*/
    fun getLearnCoachNextContent(): AchieveFeedbackRes {
        return fromJson(learnCoachNextContentResFromAsset() ?: "")
    }

    private fun createDiagnosticAchieveData(): ArrayList<DiagnosticTestFeedbackModel> {
        return arrayListOf(
            DiagnosticTestFeedbackModel(
                "Step 1 (5 Hours)", "40 Questions, 9 Videos",
                "NA", "NA",
                "started", R.drawable.achieve_bg, arrayListOf(
                    DiagnosticTestFeedbackSubModel(
                        "Improve Your Behaviour", "3 hours",
                        R.drawable.practice_placeholder, "Coach me"
                    ),
                    DiagnosticTestFeedbackSubModel(
                        "Improve Your Knowledge", "2 hours",
                        R.drawable.practice_placeholder, "Learn"
                    )
                )
            ),
            DiagnosticTestFeedbackModel(
                "Step 1 (5 Hours)", "40 Questions, 9 Videos, 2 Behaviours",
                "NA", "NA",
                "locked", R.drawable.achieve_bg, arrayListOf(
                    DiagnosticTestFeedbackSubModel(
                        "Improve Your Behaviour", "3 hours",
                        R.drawable.practice_placeholder, "Coach me"
                    ),
                    DiagnosticTestFeedbackSubModel(
                        "Improve Your Knowledge", "2 hours",
                        R.drawable.practice_placeholder, "Learn"
                    )
                )
            ),
            DiagnosticTestFeedbackModel(
                "Step 1 (5 Hours)", "40 Questions, 9 Videos",
                "NA", "NA",
                "locked", R.drawable.achieve_bg, arrayListOf()
            ),
            DiagnosticTestFeedbackModel(
                "Step 1 (5 Hours)", "40 Questions, 9 Videos",
                "NA", "NA",
                "locked", R.drawable.start_test_bg_for_achieve, arrayListOf()
            )
        )
    }

    private fun setJarRecycler() {
        jarAdapter = TestJarAdapter(this)
        binding.layoutQWA.layoutJar.rvJar.adapter = jarAdapter
        jarAdapter.setData(createJarData())
    }

    private fun setAchieveMetrics() {
        binding.layoutAchieve.tvSphere1.text =
            if (testAchieveRes?.achieve?.current.isNullOrEmpty()
                || testAchieveRes?.achieve?.current!!.toLowerCase().contains("null")
            ) "NA" else testAchieveRes?.achieve?.current

        binding.layoutAchieve.tvSphere2.text =
            if (testAchieveRes?.achieve?.potential.isNullOrEmpty()
                || testAchieveRes?.achieve?.potential!!.toLowerCase().contains("null")
            ) "NA"
            else testAchieveRes?.achieve?.potential
    }

    private fun setQWAWebView() {
        binding.layoutQWA.wvQWA.webChromeClient = WebChromeClient()
        binding.layoutQWA.wvQWA.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                Log.d("WEBVIEW", "Web view ready")
                setQuestionList(getQuestionListData(currentQWASubject))
            }
        }
        binding.layoutQWA.wvQWA.addJavascriptInterface(
            CommonAndroidAPI(this),
            CommonAndroidAPI.NAME
        )
        binding.layoutQWA.wvQWA.settings.allowFileAccess = true
        binding.layoutQWA.wvQWA.isLongClickable = false
        binding.layoutQWA.wvQWA.settings.javaScriptEnabled = true
        binding.layoutQWA.wvQWA.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        binding.layoutQWA.wvQWA.settings.domStorageEnabled = true
        binding.layoutQWA.wvQWA.setBackgroundColor(Color.argb(0, 0, 0, 0))
        binding.layoutQWA.wvQWA.setLayerType(LAYER_TYPE_SOFTWARE, null)
        binding.layoutQWA.wvQWA.addJavascriptInterface(
            CommonAndroidAPI(this),
            CommonAndroidAPI.NAME
        )
        binding.layoutQWA.wvQWA.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        binding.layoutQWA.wvQWA.loadUrl("http://52.172.137.100:8000/?&invokeModule=question_list")

    }

    private fun setQuestionList(qlist: ArrayList<Question>) {
        var str = Gson().newBuilder().serializeNulls().create()
            .toJson(qlist)
        Log.i("json data", str)
        binding.layoutQWA.wvQWA.evaluateJavascript("commonWebviewAPI.setQuestionList($str)", null)
    }

    private fun addViewsToHashMap() {
        rowsHM[binding.layoutChart.constraintCharts.id] = binding.layoutChart.constraintCharts
        rowsHM[binding.layoutClickToWatchAndTopSkill.clClickTopWatchAndTopSkill.id] =
            binding.layoutClickToWatchAndTopSkill.clClickTopWatchAndTopSkill
        rowsHM[binding.clNestedQwaAndAchieve.id] = binding.clNestedQwaAndAchieve
    }

    private fun addFocusViewsToList() {
        viewFocusList.add(binding.layoutChart.constraintCharts)
        viewFocusList.add(binding.layoutClickToWatchAndTopSkill.clClickTopWatchAndTopSkill)
        viewFocusList.add(binding.layoutQWA.tvDropDownSubject)
    }

    private fun setFirstRowListeners() {
//        binding.layoutChart.constraintCharts.clickWithDebounce(1000) {
//            if (!isFirstRowExpanded)
//                expandFirstRow()
//            else
//                shrinkFirstRow()
//        }

        binding.layoutChart.constraintCharts.setOnKeyListener { v, keyCode, event ->
            when (event.action) {
                KeyEvent.ACTION_DOWN -> {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            return@setOnKeyListener keyPressLimiter {
                                smoothScrollRowsUp(binding.layoutChart.constraintCharts.id)
                            }
                        }
                        KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_DPAD_LEFT -> {
                            coroutineScope.launch {
                                delay(1)
                                binding.layoutChart.constraintCharts.requestFocus()
                            }

                        }
                    }
                }


            }
            false
        }
    }

    private fun setClickToWatchListener() {

//        binding.layoutClickToWatchAndTopSkill.layoutClickToWatch.ivThumbnail.setOnKeyListener { v, keyCode, event ->
//            if (event.action == KeyEvent.ACTION_DOWN) {
//                when (keyCode) {
//                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
//                        /*val intent = Intent(this,
//                            VideoPOCActivity::class.java)
//                        intent.putExtra("data","achiever")
//                        startActivity(intent)*/
//                    }
//                }
//            }
//
//            false
//        }

        binding.layoutClickToWatchAndTopSkill.layoutClickToWatch.constraintClickToWatch.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenClickToWatchFocus()
        }

        binding.layoutClickToWatchAndTopSkill.layoutClickToWatch.constraintClickToWatch.setOnKeyListener { v, keyCode, event ->
            when (event?.action) {
                KeyEvent.ACTION_DOWN -> {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            return@setOnKeyListener keyPressLimiter {
                                smoothScrollRowsUp(binding.layoutClickToWatchAndTopSkill.clClickTopWatchAndTopSkill.id)
                            }
                        }
                        KeyEvent.KEYCODE_DPAD_UP -> {
                            return@setOnKeyListener keyPressLimiter {
                                smoothScrollRowsDown(binding.layoutChart.constraintCharts.id)
                            }
                        }
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                            SegmentUtils.trackTestFeedbackScreenClickToWatchClick()
                        }
                    }
                }
            }
            false
        }
    }

    private fun setQWAKeyListener() {
        binding.layoutQWA.tvDropDownSubject.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenSubjectWiseAnalyseDropdownFocus()
        }

        binding.layoutQWA.tvDropDownSubject.setOnKeyListener { v, keyCode, event ->
            when (event?.action) {
                KeyEvent.ACTION_DOWN -> {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_UP -> {
                            return@setOnKeyListener keyPressLimiter {
                                smoothScrollRowsDown(binding.layoutClickToWatchAndTopSkill.clClickTopWatchAndTopSkill.id)
                            }
                        }
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                            SegmentUtils.trackTestFeedbackScreenSubjectWiseAnalyseDropdownClick()
                        }
                    }
                }
            }
            false
        }
    }

    private fun setQWAButtonListener() {
        binding.layoutQWA.btnPerfectAttempts.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.PERFECT_ATTEMPTS)
        }
        binding.layoutQWA.btnOverTimeCorrectAttempts.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.OVERTIME_CORRECT)
        }
        binding.layoutQWA.btnTooFastCorrectAttempts.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.TOO_FAST_CORRECT_ATTEMPTS)
        }
        binding.layoutQWA.btnInCorrectAttempts.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.INCORRECT_ATTEMPTS)
        }
        binding.layoutQWA.btnOverTimeInCorrectAttempts.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.OVERTIME_INCORRECT_ATTEMPTS)
        }
        binding.layoutQWA.btnWastedAttempts.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.WASTED_ATTEMPTS)
        }
        binding.layoutQWA.btnUnAttempted.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.UNATTEMPTED_ATTEMPTS)
        }

        //////

        binding.layoutQWA.btnPerfectAttempts.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.PERFECT_ATTEMPTS)
            setQuestionList(getQuestionListData(currentQWASubject, "Perfect"))
        }
        binding.layoutQWA.btnOverTimeCorrectAttempts.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.OVERTIME_CORRECT_ATTEMPTS)
            setQuestionList(getQuestionListData(currentQWASubject, "NonAttempt"))
        }
        binding.layoutQWA.btnTooFastCorrectAttempts.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.TOO_FAST_CORRECT_ATTEMPTS)
            moveToTestQWA(getQuestionListData(ALL_SUBJECTS, ALL_ATTEMPTS).get(0), 0)
        }
        binding.layoutQWA.btnInCorrectAttempts.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.INCORRECT_ATTEMPTS)
        }
        binding.layoutQWA.btnOverTimeInCorrectAttempts.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.OVERTIME_INCORRECT_ATTEMPTS)

        }
        binding.layoutQWA.btnWastedAttempts.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.WASTED_ATTEMPTS)
            setQuestionList(getQuestionListData(currentQWASubject))
        }
        binding.layoutQWA.btnUnAttempted.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.UNATTEMPTED_ATTEMPTS)
            setQuestionList(getQuestionListData(currentQWASubject, "NonAttempt"))
        }
    }

    private fun setAndAnimateProgress() {
        val goodScore = testAttemptsRes?.scoreListInfo?.goodScore!!
        val yourScore = testAttemptsRes?.scoreListInfo?.myScore!!
        val cutOffScore = testAttemptsRes?.scoreListInfo?.cutOffScore!!
        var totalScore = 100
        //to avoid values going of screen
        var extraBiasSpace = getHorizontalBiasOfBaseLine(totalScore)
        totalScore += extraBiasSpace
        totalScore += 20
        if (mySectionSummaryList.size >= 5)
            changeMotionLayoutConstraintSetToFit()

        animate(
            goodScore.toInt(),
            totalScore,
            binding.layoutChart.layoutProgressInfo.progressBarGoodScore,
            binding.layoutChart.layoutProgressInfo.tvGoodScore
        )
        moveVerticalBaseLineBiasBasedOnScore(yourScore, totalScore)
        moveCutOffLineBiasBasedOnScore(cutOffScore, totalScore)
        moveHorizontalCutOffMarkerBiasBasedOnScore(cutOffScore, totalScore)
        if (yourScore < 0) {
            animate(
                yourScore.toInt(),
                totalScore,
                binding.layoutChart.layoutProgressInfo.progressBarNegative,
                binding.layoutChart.layoutProgressInfo.tvNegativeScore
            )
        } else {
            animate(
                yourScore.toInt(),
                totalScore,
                binding.layoutChart.layoutProgressInfo.progressBarYourScore,
                binding.layoutChart.layoutProgressInfo.tvYourScore
            )
        }
    }


    private fun getHorizontalBiasOfBaseLine(totalScore: Int): Int {
//        var params =
//            binding.layoutChart.layoutCorrectlyAnswered.viewLine.layoutParams as ConstraintLayout.LayoutParams
//        return params.horizontalBias.times(totalScore).toInt()
        return 0
    }

    private fun moveVerticalBaseLineBiasBasedOnScore(yourScore: Double, totalScore: Int) {
        if (yourScore < 0) {
            val baseLineVertical = binding.layoutChart.layoutProgressInfo.viewLine
            val baseLineParams = baseLineVertical.layoutParams as ConstraintLayout.LayoutParams
            baseLineParams.horizontalBias =
                baseLineParams.horizontalBias + (abs(yourScore).toFloat() / totalScore)
            baseLineVertical.layoutParams = baseLineParams
            baseLineVertical.requestLayout()
        }
    }

    private fun moveCutOffLineBiasBasedOnScore(cutOffScore: Int, totalScore: Int) {
        // AnyScore/totalScore to get bias value
        //totalMarks 130, Negative Marks -12

        val calScore = (cutOffScore.toDouble().div(totalScore))
        Log.e(TAG, "cal Score $calScore")
        val cutOffLine = binding.layoutChart.layoutProgressInfo.ivCutOffMarker
        val cutOffLabel = binding.layoutChart.layoutProgressInfo.tvCutOffMarkerLabel
        val cutOffLineParams = cutOffLine.layoutParams as ConstraintLayout.LayoutParams
        val cutOffLineAnimator = ValueAnimator.ofFloat(0f, calScore.toFloat())

        cutOffLineAnimator.addUpdateListener { value ->
            cutOffLineParams.horizontalBias = (value.animatedValue as Float)
            cutOffLine.layoutParams = cutOffLineParams
        }

        val cutOffLabelAnimator = ValueAnimator.ofInt(0, cutOffScore)
        cutOffLabelAnimator.addUpdateListener { value ->
            cutOffLabel.text = (value.animatedValue as Int).toString()
        }

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(cutOffLabelAnimator, cutOffLineAnimator)
        animatorSet.duration = 1500
        animatorSet.start()
    }

    private fun moveHorizontalCutOffMarkerBiasBasedOnScore(
        cutOffScore: Int,
        totalScore: Int
    ) {
//        var endSet: ConstraintSet = correctlyAnsweredMotionLayout?.getConstraintSet(R.id.end)!!
//        endSet.setVerticalBias(R.id.iv_cut_off_marker, 0.2f)
//        endSet.applyTo(correctlyAnsweredMotionLayout)
//
//        val cutOffHorizontalTextView = binding.layoutChart.layoutCorrectlyAnswered.tvCutOffLabel
//        val cutOffStr = getString(R.string.cut_off)
//        val marksStr = getString(R.string.marks)
//        cutOffHorizontalTextView.text = "$cutOffStr $cutOffScore $marksStr"
    }

    private fun changeMotionLayoutConstraintSetToFit() {
//        val startSet: ConstraintSet = correctlyAnsweredMotionLayout?.getConstraintSet(R.id.start)!!
//        startSet.connect(R.id.tv_currently_answered, TOP, R.id.iv_text_score, TOP, 10)
//        startSet.connect(R.id.tv_currently_answered, START, R.id.iv_text_score, START)
//        startSet.connect(R.id.tv_currently_answered, END, R.id.iv_text_score, END)
//        startSet.connect(R.id.tv_currently_answered, BOTTOM, R.id.iv_text_score, BOTTOM)
//        startSet.connect(R.id.tv_score, START, R.id.tv_currently_answered, END, 5)
//        startSet.connect(R.id.tv_score, TOP, R.id.tv_currently_answered, TOP)
//        startSet.connect(R.id.tv_score, BOTTOM, R.id.tv_currently_answered, BOTTOM)
//        startSet.connect(R.id.tv_score, END, R.id.iv_text_score, END)
//        startSet.connect(R.id.rv_score_bar, START, constraintLayout.id, START)
//        startSet.connect(R.id.rv_score_bar, END, constraintLayout.id, END)
//        startSet.connect(R.id.rv_score_bar, TOP, constraintLayout.id, TOP)
//        startSet.connect(R.id.rv_score_bar, BOTTOM, constraintLayout.id, BOTTOM)
//        startSet.setVerticalBias(R.id.rv_score_bar, 0.7f)
//        startSet.setHorizontalBias(R.id.rv_score_bar, 0.3f)
//        startSet.setHorizontalBias(R.id.tv_currently_answered, 0.3f)
//        startSet.setVerticalBias(R.id.tv_currently_answered, 0.0f)
//        startSet.setHorizontalBias(R.id.tv_score, 0.0f)
//        startSet.applyTo(correctlyAnsweredMotionLayout)
//
//        val endSet: ConstraintSet = correctlyAnsweredMotionLayout?.getConstraintSet(R.id.end)!!
//        endSet.connect(R.id.tv_currently_answered, TOP, R.id.iv_text_score, TOP, 20)
//        endSet.connect(R.id.tv_currently_answered, START, R.id.iv_text_score, START)
//        endSet.connect(R.id.tv_currently_answered, END, R.id.iv_text_score, END)
//        endSet.connect(R.id.tv_currently_answered, BOTTOM, R.id.iv_text_score, BOTTOM)
//        endSet.connect(R.id.tv_score, START, R.id.tv_currently_answered, END, 5)
//        endSet.connect(R.id.tv_score, TOP, R.id.tv_currently_answered, TOP)
//        endSet.connect(R.id.tv_score, BOTTOM, R.id.tv_currently_answered, BOTTOM)
//        endSet.connect(R.id.tv_score, END, R.id.iv_text_score, END)
//        endSet.connect(R.id.rv_score_bar, START, constraintLayout.id, START)
//        endSet.connect(R.id.rv_score_bar, END, constraintLayout.id, END)
//        endSet.connect(R.id.rv_score_bar, TOP, constraintLayout.id, TOP)
//        endSet.connect(R.id.rv_score_bar, BOTTOM, constraintLayout.id, BOTTOM)
//        endSet.setVerticalBias(R.id.rv_score_bar, 0.7f)
//        endSet.setHorizontalBias(R.id.rv_score_bar, 0.3f)
//        endSet.setHorizontalBias(R.id.tv_currently_answered, 0.3f)
//        endSet.setVerticalBias(R.id.tv_currently_answered, 0.0f)
//        endSet.setHorizontalBias(R.id.tv_score, 0.0f)
//        endSet.applyTo(correctlyAnsweredMotionLayout)
    }

    private fun animate(progressValue: Int, totalScore: Int, progressBar: ImageView, tv: TextView) {
        animateBar(progressBar, progressValue, totalScore)
        tv.animateWithProgressBar(progressValue)
    }

    private fun TextView.animateWithProgressBar(progressValue: Int) {

        val animator = ObjectAnimator.ofInt(0, progressValue)
        animator.addUpdateListener { animationValue ->
            this.text = animationValue.animatedValue.toString()
        }
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(animator)
        animatorSet.duration = 2000
        animatorSet.start()
    }

    private fun animateBar(bar: ImageView, progressValue: Int, totalScore: Int) {
        val progressLayout = binding.layoutChart.layoutProgressInfo
        val vto = progressLayout.root.viewTreeObserver
        var newProgressValue = 0

        newProgressValue = kotlin.math.abs(progressValue)
        var listener: ViewTreeObserver.OnGlobalLayoutListener? = null

        listener = ViewTreeObserver.OnGlobalLayoutListener {
            progressLayoutWidth = progressLayout.root.width
            Log.e(TAG, "width $progressLayoutWidth")
            height = progressLayout.root.height

            val calValue = newProgressValue.toDouble() / totalScore * progressLayoutWidth
            val animator = ValueAnimator.ofInt(0, calValue.toInt())
            animator.addUpdateListener { valueAnimator ->
                val value = valueAnimator.animatedValue
                val params = bar.layoutParams as ConstraintLayout.LayoutParams
                if (value as Int > 0) {
                    bar.visibility = View.VISIBLE
                } else
                    bar.visibility = View.GONE
                params.width = value
                bar.layoutParams = params
            }

            animator.duration = 1500
            animator.start()

            binding.layoutChart.layoutProgressInfo.root.viewTreeObserver?.removeOnGlobalLayoutListener(
                listener!!
            )
        }
        vto?.addOnGlobalLayoutListener(listener)
    }

    private fun expandFirstRow() {
        animateCorrectlyAnsweredText()
        scaleRvBar()
        val params = binding.layoutChart.constraintCharts.layoutParams as LinearLayout.LayoutParams
        Log.e(TAG, "height: ${params.height}")
        val animator = ValueAnimator.ofInt(0, 200)
        animator.addUpdateListener { animatorValue ->
            val value = animatorValue.animatedValue
            val calVal = 320 + value as Int
            params.height = calVal
            Log.e(TAG, "height: ${params.height}")
            binding.layoutChart.constraintCharts.layoutParams = params

        }
        animator.duration = 1000
        animator.start()
        isFirstRowExpanded = true
//        binding.layoutChart.layoutTestScore.tvGradeLabel.visibility = View.VISIBLE
//        binding.layoutChart.layoutTestScore.tvGrade.visibility = View.VISIBLE
    }

    private fun shrinkFirstRow() {
        animateCorrectlyAnsweredText()
//        binding.layoutChart.layoutCorrectlyAnswered.rvScoreBar.scale()
//        binding.layoutChart.layoutTestScore.tvGrade.visibility = View.GONE
//        binding.layoutChart.layoutTestScore.tvGradeLabel.visibility = View.GONE

        val params =
            binding.layoutChart.constraintCharts.layoutParams as LinearLayout.LayoutParams
        Log.e(TAG, "height: ${params.height}")
        val animator = ValueAnimator.ofInt(0, 200)
        animator.addUpdateListener { animatorValue ->
            val value = animatorValue.animatedValue
            val calVal = 500 - value as Int
            params.height = calVal
            Log.e(TAG, "height: ${params.height}")
            binding.layoutChart.constraintCharts.layoutParams = params
        }
        animator.duration = 1000
        animator.start()
        isFirstRowExpanded = false
    }

    private fun scaleRvBar() {
////        val rvScoreBar = binding.layoutChart.layoutCorrectlyAnsweredInfo
//        if (mySectionSummaryList.size >= 5) {
//            rvScoreBar.scale(1f, 1.2f)
//        } else if (mySectionSummaryList.size >= 2 || mySectionSummaryList.size <= 4) {
//            rvScoreBar.scale(1.2f, 1.2f)
//        }
    }

    private fun RecyclerView.scale(x: Float = 1f, y: Float = 1f) {
        this.animate()?.scaleX(x)?.duration = 1000
        this.animate()?.scaleY(y)?.duration = 1000
    }

    private fun animateCorrectlyAnsweredText() {
//        val motionLayout = binding.layoutChart.layoutCorrectlyAnswered.constraintLayout
//        motionLayout.transitionToStart()
//        motionLayout.transitionToEnd()
    }

    private fun keyPressLimiter(threshHold: Long = 1500L, action: () -> Unit): Boolean {
        if (SystemClock.elapsedRealtime() - lastKeyPress < threshHold) {
            return true
        } else {
            action.invoke()
        }
        lastKeyPress = SystemClock.elapsedRealtime()
        return false
    }

    private fun smoothScrollRowsUp(viewTranslatingIndex: Int) {
        viewFocusCounter++
        rowsHM.forEach { (otherViewsIndex, _) ->
            if (otherViewsIndex != viewTranslatingIndex) {
                slideRow(
                    rowsHM[otherViewsIndex]!!,
                    -rowsHM[viewTranslatingIndex]?.translationY!!.toFloat()
                        .minus(rowsHM[viewTranslatingIndex]?.height!!.toFloat()),
                    false,
                    rowsHM[viewTranslatingIndex]?.translationY!!.toFloat()
                )
            } else
                slideRow(
                    rowsHM[viewTranslatingIndex]!!,
                    -rowsHM[viewTranslatingIndex]?.translationY!!.toFloat()
                        .minus(rowsHM[viewTranslatingIndex]?.height!!.toFloat()),
                    true,
                    rowsHM[viewTranslatingIndex]?.translationY!!.toFloat(),
                    1f, 0f
                )
        }
        coroutineScope.launch {
            delay(10)
            if (viewFocusCounter < viewFocusList.size) {
                viewFocusList[viewFocusCounter].requestFocus()
            }
        }
    }

    private fun smoothScrollRowsDown(viewTranslatingIndex: Int) {
        rowsHM.forEach { (otherViewsIndex, _) ->
            if (otherViewsIndex != viewTranslatingIndex) {
                slideRow(
                    rowsHM[otherViewsIndex]!!,
                    -rowsHM[viewTranslatingIndex]?.height!!.toFloat()
                        .plus(rowsHM[viewTranslatingIndex]!!.translationY),
                    false,
                    rowsHM[viewTranslatingIndex]!!.translationY
                )
            } else {
                slideRow(
                    rowsHM[viewTranslatingIndex]!!,
                    -rowsHM[viewTranslatingIndex]?.height!!.toFloat()
                        .plus(rowsHM[viewTranslatingIndex]!!.translationY),
                    true,
                    rowsHM[viewTranslatingIndex]!!.translationY,
                    0f,
                    1f
                )
            }
        }

        coroutineScope.launch {
            delay(10)
            if (viewFocusCounter > 0 && viewFocusCounter < viewFocusList.size) {
                viewFocusList[--viewFocusCounter].requestFocus()
            }
        }
    }

    private fun slideRow(
        view: View,
        viewHeight: Float = 0f,
        canFade: Boolean = false,
        updatedTransYValue: Float = 0f,
        alphaFrom: Float = 0f,
        alphaTo: Float = 1f
    ) {
        val transYObjectAnimator =
            ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, updatedTransYValue, -viewHeight)
        var alphaObjectAnimator: ObjectAnimator? = null
        if (canFade) {
            alphaObjectAnimator =
                ObjectAnimator.ofFloat(view, View.ALPHA, alphaFrom, alphaTo)
        }

        val animatorSet = AnimatorSet()
        if (alphaObjectAnimator != null)
            animatorSet.playTogether(transYObjectAnimator, alphaObjectAnimator)
        else
            animatorSet.playTogether(transYObjectAnimator)

        animatorSet.duration = RowAnimDelayValues.ANIM_DELAY
        animatorSet.start()
    }

    private fun View.clickWithDebounce(debounceTime: Long = 600L, action: () -> Unit) {
        this.setOnClickListener(object : View.OnClickListener {
            private var lastClickTime: Long = 0

            override fun onClick(v: View) {
                if (SystemClock.elapsedRealtime() - lastClickTime < debounceTime) return
                else action()

                lastClickTime = SystemClock.elapsedRealtime()
            }
        })
    }

//    private fun createAchieveData(): ArrayList<TestFeedbackMenuModel> {
//        return arrayListOf(
//            TestFeedbackMenuModel(
//                "Achieve Amazing Results with your \npersonalised achievement journey",
//                arrayListOf(), R.drawable.achieve_bg
//
//            ),
//            TestFeedbackMenuModel(
//                "Get Your Revision Lists", getRevisionList(), R.drawable.revision_list
//            ),
//            TestFeedbackMenuModel(
//                "Improve Your \nTest Taking Strategy",
//                getImprovingStrategyList(), R.drawable.test_taking_banner
//            )
//        )
//    }

//    private fun getRevisionList(): ArrayList<SubFeedbackMenuModel> {
//        val revisions = testAchieveRes?.revisions
//        var revisionList = ArrayList<SubFeedbackMenuModel>()
//        for (index in 0 until revisions?.size!!) {
//            revisionList.add(SubFeedbackMenuModel(revisions[index].display_name!!, arrayListOf()))
//        }
//
//        return revisionList
//    }
//
//    private fun getImprovingStrategyList(): ArrayList<SubFeedbackMenuModel> {
//        val improvingStrategy = testAchieveRes?.improveStrategy
//        val improveStrategyList = ArrayList<SubFeedbackMenuModel>()
//
//        if (improvingStrategy?.positiveBehaviour?.size != 0) {
//            val behaviourList = ArrayList<TestBehaviour>()
//            for (index in 0 until improvingStrategy?.positiveBehaviour?.size!!) {
//                behaviourList.add(improvingStrategy.positiveBehaviour!![index])
//            }
//            improveStrategyList.add(
//                SubFeedbackMenuModel(
//                    "Understand My Positive Behaviour",
//                    behaviourList
//                )
//            )
//        }
//
//        if (improvingStrategy.negativeBehaviour?.size != 0) {
//            val behaviourList = ArrayList<TestBehaviour>()
//            for (index in 0 until improvingStrategy.negativeBehaviour?.size!!) {
//                behaviourList.add(improvingStrategy.negativeBehaviour!![index])
//            }
//            improveStrategyList.add(
//                SubFeedbackMenuModel(
//                    "Understand My Negative Behaviour",
//                    behaviourList
//                )
//            )
//        }
//
//        return improveStrategyList
//    }

    private fun createJarData(): ArrayList<TestFeedbackJarModel> {
        return arrayListOf(
            TestFeedbackJarModel(20, R.drawable.jar_too_fast_correct),
            TestFeedbackJarModel(36, R.drawable.jar_perfect_attempt),
            TestFeedbackJarModel(22, R.drawable.jar_overtime_correct),
            TestFeedbackJarModel(15, R.drawable.jar_wasted_attempt),
            TestFeedbackJarModel(12, R.drawable.jar_incorrect_answer),
            TestFeedbackJarModel(13, R.drawable.jar_overtime_incorrect),
            TestFeedbackJarModel(0, R.drawable.jar_unattempted)
        )
    }

    private fun animateJar() {
        binding.layoutQWA.layoutJar.rvJar.visibility = View.GONE
        binding.layoutQWA.layoutAQ.qaMain.visibility = View.VISIBLE
    }

    override fun onJarSelected(item: TestFeedbackJarModel) {
        animateJar()
    }

    override fun focusUp() {
        super.focusUp()
//        Toast.makeText(this, "web triggered focusUp", Toast.LENGTH_SHORT).show()
        runOnUiThread {
//            currentFocus?.clearFocus()
            binding.layoutQWA.btnUnAttempted.postDelayed({
                binding.layoutQWA.btnUnAttempted.requestFocus()
            }, 10)
        }
    }

    override fun getSelectedQuestion(qListJson: String) {
        try {
            Log.i("selected Position", qListJson)
            var json = JSONObject(qListJson)
            var pos = json.getInt("pos")
            var qData = json.getJSONObject("data")
            moveToTestQWA(Gson().fromJson(qData.toString(), Question::class.java), pos)
        } catch (exception: JSONException) {
        }
        //var str = readJSONFromAsset()
//        binding.layoutQWA.wvQWA.evaluateJavascript("javascript:setQuestionList(\\\"$qListJson\\\")",null)
        // {"pos":1,"data":<Selected Question Obj>}


    }

    override fun setQuestion(singQuestionJson: String) {
        TODO("Not yet implemented")
    }

    object RowAnimDelayValues {
        const val ANIM_DELAY = 1000L
    }

//    private fun getAttemptsStatic(testCode: String) {
//        val testViewModel = ViewModelProviders.of(this).get(TestViewModel::class.java)
//        testViewModel.getAttempts(testCode, object : BaseViewModel.APICallBacks<TestAttemptsRes> {
//            override fun onSuccess(model: TestAttemptsRes?) {
//                if (model != null) {
//                    makeLogI("Attempts Res Ok")
//                    hideProgress()
//                    //sendAttemptsSkillsToFeedback(testSkill, model)
//                }
//
//            }
//
//            override fun onFailed(code: Int, error: String, msg: String) {
//                hideProgress()
//                makeLogI("Attempts Res failed $error")
//                makeLogI("$code - $error - $msg")
//            }
//
//        })
//    }
//
//    fun getQuestionList(secType: String, attemptType: String): List<Question> {
//        var questionList = ArrayList<Question>()
//        if (testQuestionRes != null) {
//            for (key in testQuestionRes!!.paper.sections.keys) {
//                var section = testQuestionRes!!.paper.sections.get(key)
//                if (secType == "all" || section!!.name == secType) {
//                    for (question in section!!.questions) {
//                        if (question.attemptTypeBadge == attemptType) {
//                            questionList.add(question)
//                        }
//                    }
//                }
//            }
//        }
//        return questionList
//    }
//
//    fun getQuestion(questionCode: String): Question? {
//
//        if (testQuestionRes != null) {
//            for (key in testQuestionRes!!.paper.sections.keys) {
//                var section = testQuestionRes!!.paper.sections.get(key)
//                for (question in section!!.questions) {
//                    if (question.code == questionCode) {
//                        return question
//                    }
//                }
//            }
//        }
//        return null
//    }
//
//    private fun getCalculatedBias(cutOffScore: Int): Float {
//        //need to write formula
//        return cutOffScore.toDouble().div(100).div(2).toFloat()
//    }
//
//    override fun onClick(view: View?) {
//        when (view) {
//            binding.layoutChart.constraintCharts -> {
//
//            }
//        }
//    }
//
//    private fun clipTextToTop() {
//        val startSet: ConstraintSet = correctlyAnsweredMotionLayout?.getConstraintSet(R.id.start)!!
//        startSet.connect(R.id.tv_currently_answered, TOP, R.id.iv_text_score, TOP)
//        correctlyAnsweredMotionLayout?.updateState(R.id.start, startSet)
//        animateCorrectlyAnsweredText()
//    }
//

    //TODO: need to delete this method
    fun readJSONFromAsset(): String? {
        var json: String? = null
        try {
            val inputStream: InputStream = assets.open("sample_data.json")
            json = inputStream.bufferedReader().use { it.readText() }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    inline fun <reified T> fromJson(json: String): T {
        return Gson().fromJson(json, object : TypeToken<T>() {}.type)
    }


    private fun achieveFeedbackResFromAsset(): String? {
        var json: String? = null
        try {
            val inputStream: InputStream = assets.open("achieve_feedback_response_latest.json")
            json = inputStream.bufferedReader().use { it.readText() }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    fun learnCoachNextContentResFromAsset(): String? {
        var json: String? = null
        try {
            val inputStream: InputStream = assets.open("learn_coach_next_content_response.json")
            json = inputStream.bufferedReader().use { it.readText() }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    /*AchieveBehaviours*/
    private fun getAchieveBehaviours(testCodes: List<String>) {
        showProgress()
        testViewModel.getAchieveBehaviours(
            testCodes,
            object : BaseViewModel.APICallBacks<List<ImproveStrategy>> {
                override fun onSuccess(model: List<ImproveStrategy>?) {
                    hideProgress()
                    if (model != null) {

                    }


                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    hideProgress()
                    makeLogI("$code - $error - $msg")
                }

            })
    }

    override fun movePAJNextActivity(data: String) {

    }


}
