package com.embibe.embibetvapp.ui.fragment.achieve

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.drawable.Animatable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.ProgressBar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentReadinessBinding
import com.embibe.embibetvapp.model.ModifiedResponse
import com.embibe.embibetvapp.model.SubjectReadiness
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.achieve.futureSuccess.FutureSuccessRes
import com.embibe.embibetvapp.model.achieve.readiness.Readiness
import com.embibe.embibetvapp.model.achieve.readiness.SubjectScore
import com.embibe.embibetvapp.ui.activity.DiagnosticTransitionActivity
import com.embibe.embibetvapp.ui.adapter.SubjectReadinessAdapter
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.viewmodel.AchieveViewModel
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo
import com.github.penfeizhou.animation.apng.APNGDrawable
import com.github.penfeizhou.animation.loader.ResourceStreamLoader
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ReadinessFragment : BaseAppFragment() {
    val classTag = ReadinessFragment::class.java.simpleName
    private var dPadKeysCallback: DPadKeysListener? = null

    private var listSubject: ArrayList<SubjectReadiness> = ArrayList()
    private val circularProgressDrawable = ArrayList<Int>()
    private val predictedProgressBars = ArrayList<ProgressBar>()
    private val projectedProgressBars = ArrayList<ProgressBar>()
    private var listFutureSuccess: List<FutureSuccessRes>? = null
    private var readinessData: Readiness? = null
    private lateinit var binding: FragmentReadinessBinding
    private lateinit var coroutineScope: CoroutineScope
    private lateinit var viewModel: AchieveViewModel
    private val userGrade = UserData.getGrade()
    private val userGoal = UserData.getGoal()
    private var isFutureSuccessActive = false

    private var subjectReadinessAdapter: SubjectReadinessAdapter? = null

    private var TAG = this.javaClass.name

    object AnimTranslate {
        const val DURATION = 700L
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coroutineScope = CoroutineScope(Dispatchers.Main)
        viewModel = ViewModelProvider(this).get(AchieveViewModel::class.java)
        listSubject.clear()
        readinessData = DataManager.instance.getReadinessData()
        listFutureSuccess = DataManager.instance.getFutureSuccessData()
    }

    private fun fillDrawables() {
        circularProgressDrawable.add(R.drawable.determinate_ciricular_blue_thicker)
        circularProgressDrawable.add(R.drawable.determinate_ciricular_red_thicker)
        circularProgressDrawable.add(R.drawable.determinate_ciricular_purple_thicker)
        circularProgressDrawable.add(R.drawable.determinate_ciricular_orange_thicker)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_readiness,
            container,
            false
        )
        loadDataToView()
        initView()
        return binding.root
    }

    private fun loadDataToView() {
        setReadinessLabels()
        if (readinessData?.predictedGrade != null && readinessData?.predictedGrade?.subjectScores?.isNotEmpty()!!) {
            fillDrawables()
            setPredictedProjectedProgressBars()
            setDataToBottomRv()
        } else {
            setNullDataToBottomRv()
        }
    }

    private fun setReadinessLabels() {
        val yourReadinessTranslation = getString(R.string.your_predicted_readiness_for)
        val classTranslation = getString(R.string.this_class)
        if (Utils.getCurrentLocale() == "en")
            binding.tvYourLifeSuccess.text =
                "$yourReadinessTranslation $classTranslation $userGrade"
        else if (Utils.getCurrentLocale() == "hi") {
            binding.tvYourLifeSuccess.text =
                "$userGrade $classTranslation $yourReadinessTranslation"
        }
        val readinessForSubjectsTranslation =
            getString(R.string.calculated_based_on_what_you_know_so_far)
        binding.tvClassStatusDesc.text = readinessForSubjectsTranslation
    }

    private fun setPredictedProjectedProgressBars() {
        predictedProgressBars.add(binding.predictedGrade.layoutReadiness.progressBarBlue)
        predictedProgressBars.add(binding.predictedGrade.layoutReadiness.progressBarRed)
        predictedProgressBars.add(binding.predictedGrade.layoutReadiness.progressBarPurple)
        predictedProgressBars.add(binding.predictedGrade.layoutReadiness.progressBarOrange)

        projectedProgressBars.add(binding.projectedGrade.layoutReadiness.progressBarBlue)
        projectedProgressBars.add(binding.projectedGrade.layoutReadiness.progressBarRed)
        projectedProgressBars.add(binding.projectedGrade.layoutReadiness.progressBarPurple)
        projectedProgressBars.add(binding.projectedGrade.layoutReadiness.progressBarOrange)
    }

    private fun setNullDataToBottomRv() {
        listSubject.clear()
        listSubject.add(
            SubjectReadiness(
                null,
                R.drawable.determinate_ciricular_orange,
                "",
                null
            )
        )
    }

    private fun setDataToBottomRv() {
        listSubject.clear()
        Log.i(classTag, readinessData?.predictedGrade.toString())

        val subjectScoresList =
            readinessData?.predictedGrade?.subjectScores as ArrayList<SubjectScore>
        subjectScoresList.forEachIndexed { index, subjectScore ->
            listSubject.add(
                SubjectReadiness(
                    subjectScore.score?.toInt(),
                    circularProgressDrawable[index % 4],
                    subjectScore.subject,
                    null
                )
            )
        }

    }

    private fun initView() {
        val assetLoader = ResourceStreamLoader(context, R.drawable.particles_a)
        val aPngDrawable = APNGDrawable(assetLoader)
        binding.ivParticles.setImageDrawable(aPngDrawable)
        binding.dyShadow.visibility = VISIBLE
        bottomRv()
        setListeners()
        setDataToUI()
        coroutineScope.launch {
            delay(500)
            binding.ivParticles.visibility = VISIBLE
        }
    }

    private fun bottomRv() {
        val rv = binding.rvSubjects
        rv.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        subjectReadinessAdapter = SubjectReadinessAdapter(requireContext())
        subjectReadinessAdapter!!.setType("progress")
        subjectReadinessAdapter!!.itemCount = listSubject.size
        subjectReadinessAdapter!!.setData(listSubject)
        rv.adapter = subjectReadinessAdapter
    }

    @SuppressLint("SetTextI18n")
    private fun setDataToUI() {
        //setting future success
        if (!listFutureSuccess.isNullOrEmpty()) {
            val futureSuccessList = listFutureSuccess as ArrayList<FutureSuccessRes>
            for (data in futureSuccessList) {
                if (data.isPrimary != null) {
                    if (data.isPrimary) {
                        setLikeDislike(data)
                        break
                    }
                }
            }
        }

        //setting readiness
        if (!readinessData?.predictedGrade?.grade.isNullOrEmpty()) {
            binding.predictedGrade.layoutReadiness.tvGrade.text =
                checkGrade(readinessData?.predictedGrade?.grade)
            binding.predictedGrade.tvLayoutA.text =
                getString(R.string.your_predicted_grade_for) + " " + readinessData?.exam

        } else {
            binding.predictedGrade.layoutReadiness.tvGrade.text = "?"
            if (Utils.getCurrentLocale() == "en") {
                binding.predictedGrade.tvLayoutA.text =
                    getString(R.string.your_predicted_grade_for) + " " + userGoal + " \n${getString(
                        R.string.this_class
                    )} " + userGrade
            } else if (Utils.getCurrentLocale() == "hi") {
                binding.predictedGrade.tvLayoutA.text =
                    getString(R.string.your_predicted_grade_for) + " " + userGoal + " " + userGrade + getString(
                        R.string.this_class
                    )
            }
        }


        if (!readinessData?.projectedGrade?.grade.isNullOrEmpty()) {
            binding.projectedGrade.layoutReadiness.tvGrade.text =
                checkGrade(readinessData?.projectedGrade?.grade)
            binding.projectedGrade.tvLayoutA.text =
                getString(R.string.your_potential_grade_for) + " " + readinessData?.exam
        } else {
            binding.projectedGrade.layoutReadiness.tvGrade.text = "?"
            if (Utils.getCurrentLocale() == "en") {
                binding.projectedGrade.tvLayoutA.text =
                    getString(R.string.your_potential_grade_for) + " " + userGoal + " \n${getString(
                        R.string.this_class
                    )} " + userGrade
            } else if (Utils.getCurrentLocale() == "hi") {
                binding.projectedGrade.tvLayoutA.text =
                    getString(R.string.your_potential_grade_for) + " " + userGoal + " " + userGrade + getString(
                        R.string.this_class
                    )
            }
        }


        try {
            val predictedScoreList = readinessData?.predictedGrade!!.subjectScores
            predictedScoreList?.forEachIndexed { index, predictedScore ->
                predictedProgressBars[index].bindProgress(predictedScore.score!!.toDouble())
            }

            val projectedScoreList = readinessData?.projectedGrade!!.subjectScores
            projectedScoreList?.forEachIndexed { index, projectedScore ->
                projectedProgressBars[index].bindProgress(projectedScore.score!!.toDouble())
            }

        } catch (e: Exception) {
            Log.e(TAG, "exception: ${e.message}")
        }
        binding.layoutTrophy.item = ModifiedResponse(
            AppConstants.TYPE_TROPHY, "",
            "", "", null, null, null
        )

        Utils.loadLottieAnimation(binding.layoutDiscoverYourself.lvTrophyGlow, R.raw.trophy_glow_2)
        Utils.loadLottieAnimation(binding.predictedGrade.lvCircularSphere, R.raw.circle_rays_two)
        Utils.loadLottieAnimation(binding.projectedGrade.lvCircularSphere, R.raw.circle_rays_two)
        binding.projectedGrade.clItemReadiness.scale()
        binding.predictedGrade.clItemReadiness.scale()
        binding.layoutDiscoverYourself.clDiscoverWhereYouStand.scale()
        setFocusToLastItem()

    }

    private fun getManipulatedStr(grade: String?): String {
        var strArray = grade!!.split(" ")
        var newStr = ""
        var count = 1
        for (strIndex in strArray.indices) {
            if (strArray[strIndex].isNotEmpty() && count == 1) {
                newStr = newStr + strArray[strIndex] + "\n"
                count = 0
            } else {
                newStr = newStr + " " + strArray[strIndex]
            }
        }
        return newStr
    }

    private fun setListeners() {

        binding.layoutTrophy.clTrophy.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (event.keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        dPadKeysCallback?.isKeyPadLeft(true, "ReadinessFragment")
                    }
                    KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                        val intent = Intent(context, DiagnosticTransitionActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
            false
        }

        binding.layoutDiscoverYourself.clDiscoverWhereYouStand.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (event.keyCode) {
                    KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                        isFutureSuccessActive = true
                        changeItems()
                        setFutureSuccess()
                        setFocusToLastItem()
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        dPadKeysCallback?.isKeyPadLeft(true, "ReadinessFragment")
                    }
                }
            }
            false
        }

        binding.tvQuestionsMark.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (event.keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        dPadKeysCallback?.isKeyPadLeft(true, "ReadinessFragment")
                    }
                }
            }
            false
        }
    }

    private fun checkGrade(grade: String?): String? {
        return if (grade!!.isEmpty())
            ""
        else
            grade
    }

    private fun ProgressBar.bindProgress(progress: Double) {
        if (progress != null && progress > 0) {
            ObjectAnimator.ofInt(this, "progress", progress.toInt())
                .setDuration(2000)
                .start()
        } else {
            ObjectAnimator.ofInt(this, "progress", 0)
                .setDuration(2000)
                .start()
        }
    }

    fun isFutureSuccessActive(): Boolean = isFutureSuccessActive

    private fun setLikeDislike(data: FutureSuccessRes) {
        val likeUrl = data.predictedGrade?.statusUrl

        val disLikeUrl = data.projectedGrade?.statusUrl

        if (data.exam.isNullOrEmpty()) {
            data.exam = ""
            Log.i(classTag, "data:1 ${data.exam}")
            binding.layoutLike.tvPotential.visibility = GONE
            binding.layoutDislike.tvPotential.visibility = GONE
            binding.layoutTrophy.tvPotential.text = ""
            binding.layoutTrophy.tvPotential.visibility = GONE
        } else {
            Log.i(classTag, "data2: ${data.exam}")
            binding.layoutLike.tvPotential.text =
                getString(R.string.your_potential_performance_in) + " " + data.exam
            binding.layoutDislike.tvPotential.text =
                getString(R.string.your_predicted_performance_in) + " " + data.exam
            binding.layoutTrophy.tvPotential.visibility = VISIBLE
            binding.layoutLike.tvPotential.visibility = VISIBLE
            binding.layoutDislike.tvPotential.visibility = VISIBLE
            binding.layoutTrophy.tvPotential.text = getString(R.string.unleash_your_ntrue_potential)
        }

        binding.layoutLike.item =
            ModifiedResponse(
                AppConstants.TYPE_LIKE_DISLIKE, "",
                "Your Potential Performance \nin ${data.exam}", "",
                disLikeUrl, null, null
            )

        binding.layoutDislike.item = ModifiedResponse(
            AppConstants.TYPE_LIKE_DISLIKE, "",
            "Your Predicted Performance \nin ${data.exam}",
            "", likeUrl, null, null
        )


    }

    private fun changeItems() {
        binding.tvYourLifeSuccess.text = getString(R.string.what_your_future_life_success_looks_like_right_now)
        binding.tvClassStatusDesc.text = ""
        switchVisibility(false)
        binding.layoutTrophy.clTrophy.scale()
        binding.layoutDislike.clLikeDislike.scale()
        binding.layoutLike.clLikeDislike.scale()
        binding.ivParticles.animate().translationX(-170f).duration = AnimTranslate.DURATION
        binding.layoutLike.clLikeDislike.animate().translationX(-180f).duration =
            AnimTranslate.DURATION
        binding.layoutDislike.clLikeDislike.animate().translationX(-155f).duration =
            AnimTranslate.DURATION
        binding.layoutTrophy.clTrophy.animate().translationX(-130f).duration =
            AnimTranslate.DURATION
        Utils.loadLottieAnimation(binding.layoutTrophy.lvTrophyGlow, R.raw.trophy_glow_2)
        Utils.loadLottieAnimation(binding.layoutTrophy.lvTrophy, R.raw.trophy_hover)
        Utils.loadLottieAnimation(binding.layoutLike.lvCircularSphere, R.raw.circle_rays_two)
        Utils.loadLottieAnimation(binding.layoutDislike.lvCircularSphere, R.raw.circle_rays_two)
    }

    private fun setFutureSuccess() {
        val futureSuccessList = listFutureSuccess as ArrayList<FutureSuccessRes>
        if (futureSuccessList.isNullOrEmpty()) {
            binding.tvStrengthenBasics.visibility = INVISIBLE
            //since no data setting manually to show 1 item [question mark]
            futureSuccessList.add(
                FutureSuccessRes(
                    null, null, null,
                    null
                )
            )
        } else {
            if (futureSuccessList.isNotEmpty()) {
                val exam = futureSuccessList[0].exam
                if (exam.isNullOrEmpty()) {
                    binding.tvStrengthenBasics.visibility = INVISIBLE
                } else {
                    binding.tvYourLifeSuccess.visibility = VISIBLE
                    binding.tvStrengthenBasics.visibility = VISIBLE
                    val strengthTranslation =
                        getString(R.string.today_system_aligns_life_success_with_entrance_exams_you_can_beat_the_system)
                    binding.tvStrengthenBasics.text = strengthTranslation
                }
            }
        }
        subjectReadinessAdapter!!.itemCount = futureSuccessList.size
        subjectReadinessAdapter!!.setType("futureSuccess")
        subjectReadinessAdapter!!.setFutureSuccessData(futureSuccessList)
    }

    private fun View.goneVisible(boolean: Boolean) {
        if (boolean)
            this.visibility = GONE
        else
            this.visibility = VISIBLE
    }

    private fun View.scale() {
        val scaleX = PropertyValuesHolder.ofFloat(SCALE_X, 1f)
        val scaleY = PropertyValuesHolder.ofFloat(SCALE_Y, 1f)
        val animator = ObjectAnimator.ofPropertyValuesHolder(this, scaleX, scaleY)
        animator.interpolator = DecelerateInterpolator()
        animator.duration = AnimTranslate.DURATION
        animator.start()
    }

    fun setFocusToLastItem() {
        coroutineScope.launch {
            delay(600)
            binding.layoutDiscoverYourself.clDiscoverWhereYouStand.requestFocus()
            binding.layoutTrophy.clTrophy.requestFocus()
        }
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }

    fun showReadiness() {
        switchVisibility(true)
        loadDataToView()
        initView()
        isFutureSuccessActive = false
    }

    private fun switchVisibility(isReadiness: Boolean) {
        if (isReadiness) {
            binding.ivParticles.apply {
                animate().apply {
                    translationX(25f)
                    start()
                }
            }
            binding.dyShadow.visibility = VISIBLE
        }else {
            binding.dyShadow.visibility = GONE
        }
        binding.tvStrengthenBasics.goneVisible(isReadiness)
        binding.layoutDiscoverYourself.clDiscoverWhereYouStand.goneVisible(!isReadiness)
        binding.predictedGrade.clItemReadiness.goneVisible(!isReadiness)
        binding.projectedGrade.clItemReadiness.goneVisible(!isReadiness)
        binding.layoutLike.clLikeDislike.goneVisible(isReadiness)
        binding.layoutDislike.clLikeDislike.goneVisible(isReadiness)
        binding.layoutTrophy.clTrophy.goneVisible(isReadiness)
        binding.tvQuestionsMark.goneVisible(true)
    }

    private fun playWebPAnimationWithFresco(
        view: SimpleDraweeView,
        resource: Int,
        setController: Boolean
    ) {
        val controller = Fresco.newDraweeControllerBuilder()
        if (setController) {
            controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                override fun onFinalImageSet(
                    id: String?,
                    imageInfo: ImageInfo?,
                    animatable: Animatable?
                ) {
                    val anim = animatable as AnimatedDrawable2
                    anim.setAnimationListener(object : AnimationListener {
                        override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                        override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                        }

                        override fun onAnimationFrame(
                            drawable: AnimatedDrawable2?,
                            frameNumber: Int
                        ) {
                        }

                        override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                        }

                        override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                    })
                }
            }
        }
        controller.autoPlayAnimations = true
        val res = "res:/" + resource
        controller.setUri(Uri.parse(res))
        view.controller = controller.build()
    }

}