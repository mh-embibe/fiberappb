package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.ui.custom.FocusRelativeLayout
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.utils.Utils
import com.embibejio.coreapp.model.LinkedProfile
import java.util.*
import kotlin.collections.ArrayList

class ParentProfileAdapter(var context: Context) :
    RecyclerView.Adapter<ParentProfileAdapter.BaseViewHolder<*>>() {

    private lateinit var dPadKeysCallback: DPadKeysListener
    private var mListener: OnItemStateListener? = null
    private var list: List<LinkedProfile> = ArrayList()

    companion object {
        private const val TYPE_USER = 0
        private const val TYPE_ADD_USER = 1
    }

    interface OnItemStateListener {
        fun onItemClick(view: View?, position: Int)
        fun onFocusChange(view: View?, position: Int)
    }

    fun setOnItemStateListener(listener: OnItemStateListener?) {
        mListener = listener
    }

    abstract class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(item: T)
    }

    inner class UserViewHolder internal constructor(itemView: View?) :
        BaseViewHolder<LinkedProfile>(itemView!!),
        View.OnFocusChangeListener {
        private var mFrameLayout: FocusRelativeLayout =
            itemView?.findViewById<View>(R.id.fl_parent_profile_layout) as FocusRelativeLayout
        var mImageView: ImageView = itemView?.findViewById<View>(R.id.iv_item) as ImageView
        var mTextView: TextView = itemView?.findViewById<View>(R.id.text_user) as TextView
        var mTextViewType: TextView = itemView?.findViewById<View>(R.id.text_user_type) as TextView
        override fun bind(item: LinkedProfile) {
            mImageView.setBackgroundResource(0)
            itemView.tag = TYPE_USER
            Utils.setAvatarImage(context, mImageView, item.profilePic)
            setUserNameAndType(item, mTextView, mTextViewType)
            mFrameLayout.onFocusChangeListener = this
        }

        override fun onFocusChange(v: View?, hasFocus: Boolean) {
            mFrameLayout.onFocusChangeListener = this
            if (hasFocus) {
                mTextView.setTextColor(context.getColor(R.color.white))
                mTextViewType.setTextColor(context.getColor(R.color.user_text_unselected))
                mImageView.setBackgroundResource(R.drawable.shape_circle_cyan)
            } else {
                mTextView.setTextColor(context.getColor(R.color.user_text_unselected))
                mTextViewType.setTextColor(context.getColor(R.color.user_text_unselected))
                mImageView.setBackgroundResource(R.drawable.shape_circle_cyan_unselected)
            }
            if (mListener != null) {
                mListener!!.onFocusChange(v, adapterPosition)
            }
        }
    }

    inner class AddViewHolder internal constructor(itemView: View?) :
        BaseViewHolder<Drawable>(itemView!!), View.OnClickListener, View.OnFocusChangeListener {
        private var mFrameLayout =
            itemView?.findViewById<View>(R.id.fl_parent_profile_layout) as FocusRelativeLayout
        var mImageView = itemView?.findViewById<View>(R.id.iv_item) as ImageView
        private var mTextView = itemView?.findViewById<View>(R.id.text_user) as TextView
        private var mTextViewType = itemView?.findViewById<View>(R.id.text_user_type) as TextView

        override fun bind(item: Drawable) {
            mImageView.setBackgroundResource(0)
            mImageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_add))
            itemView.tag = TYPE_ADD_USER
            mTextView.visibility = View.GONE
            mTextViewType.visibility = View.GONE
            mFrameLayout.onFocusChangeListener = this
            mFrameLayout.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            mFrameLayout.setOnClickListener(this)

            if (mListener != null) {
                mListener!!.onItemClick(v, adapterPosition)
            }
        }

        override fun onFocusChange(v: View?, hasFocus: Boolean) {
            mFrameLayout.onFocusChangeListener = this
            if (hasFocus) {
                mImageView.setBackgroundResource(R.drawable.shape_circle_cyan)
            } else {
                mImageView.setBackgroundResource(R.drawable.shape_circle_cyan_unselected)
            }
            if (mListener != null) {
                mListener!!.onFocusChange(v, adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return when (viewType) {
            TYPE_USER -> {
                val view = LayoutInflater.from(context)
                    .inflate(R.layout.item_recyclerview_parent_profile, parent, false)
                UserViewHolder(view)
            }
            TYPE_ADD_USER -> {
                val view = LayoutInflater.from(context)
                    .inflate(R.layout.item_rv_parent_profile_add_btn, parent, false)
                AddViewHolder(view)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun getItemCount(): Int {
        return when {
            list.size >= 6 -> 6
            list.size in 1..5 -> list.size + 1
            else -> 1
        }
    }

    fun updateChildData(dataList: List<LinkedProfile>) {
        this.list = dataList
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            list.size >= 6 -> TYPE_USER
            list.size in 1..5 -> {
                when (position) {
                    itemCount - 1 -> TYPE_ADD_USER
                    else -> TYPE_USER
                }
            }
            else -> {
                TYPE_ADD_USER
            }
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        holder.itemView.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        //open navigation
                        if (position == 0)
                            dPadKeysCallback.isKeyPadLeft(true, "ParentAdapter")
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        //shift focus to down buttons
                        if (holder.itemView.tag == TYPE_USER)
                            dPadKeysCallback.isKeyPadDown(true, "ParentAdapter")
                    }
                }
            }

            false
        }
        when (holder) {
            is UserViewHolder -> {
                val profile = list[position]
                holder.bind(profile)
            }
            is AddViewHolder -> {
                val profile: Drawable = ContextCompat.getDrawable(context, R.drawable.ic_add)!!
                holder.bind(profile)
            }
        }
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }


    private fun setUserNameAndType(
        item: LinkedProfile, mTextView: TextView, mTextViewType: TextView
    ) {
        if (item.userType == AppConstants.USER_TYPE_PARENT) {
            when {
                item.firstName.isNullOrEmpty() -> {
                    mTextView.text = AppConstants.PARENT
                }
                item.firstName!!.toLowerCase(Locale.getDefault()) == "null" -> mTextView.text =
                    AppConstants.PARENT
                else -> mTextView.text = setFirstName(item.firstName)
            }
            mTextViewType.text = context.getString(R.string.parent)
        } else {
            mTextView.visibility = View.VISIBLE
            when {
                item.firstName.isNullOrEmpty() -> {
                    mTextView.text = AppConstants.STUDENT
                }
                item.firstName!!.toLowerCase(Locale.getDefault()) == "null" -> mTextView.text =
                    AppConstants.STUDENT
                else -> mTextView.text = setFirstName(item.firstName)
            }
            mTextViewType.visibility = View.GONE

        }
    }

    private fun setFirstName(name: String?): String? {
        return if (name != null && name!!.length > 12 && name!!.contains(" ")) {
            name?.substring(0, name?.indexOf(" "))
        } else name
    }


}