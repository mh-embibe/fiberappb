package com.embibe.embibetvapp.ui.fragment.learn

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.TRANSLATION_Y
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentHostLearnBinding
import com.embibe.embibetvapp.jobScheduler.MyInternetTest
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.model.HeaderBannerData
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.ui.activity.SwitchGoalExamActivity
import com.embibe.embibetvapp.ui.interfaces.*
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.MainViewModel
import com.embibe.embibetvapp.utils.data.DataManager
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*

class LearnHostFragment : BaseAppFragment(),
    DPadKeysListener, RowsToBannerListener, BannerDataListener,
    NavigationMenuCallback, BannerImageDataToHostListener,
    MyInternetTest.NetworkSchedulerService.ConnectivityReceiverListener {

    private lateinit var binding: FragmentHostLearnBinding
    private lateinit var mainViewModel: MainViewModel

    private lateinit var quickDashboardFragment: QuickDashboardFragment
    private var learnBannerFragment = LearnBannerFragment()
    private var learnRowsFragment = LearnRowsFragment()

    // private var learnBannerImgFlipper = LearnBannerImgFlipper()
    private var hostToNavigationListener: HostToNavigationListener? = null
    private var classTag = LearnHostFragment::class.java.toString()
    private var internetCheckMLD = MutableLiveData<Boolean>()
    private var lastFocusedContent = "Banner"
    private var contentAvailable = true
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_host_learn, container, false)
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        fillBannerData()
//        setFragment(quickDashboardFragment, binding.dashboardFL.id)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        // setFragment(learnBannerImgFlipper, binding.flBannerBgLearn.id)
        setFragment(learnBannerFragment, binding.flBannerLearn.id)
        setFragment(learnRowsFragment, binding.flRowLearn.id)
        hostToNavigationListener?.showNavigation(false)
        MyInternetTest.scheduleJob()
        setNetworkObserver()
        registerChangeExamButtonListeners()

    }

    private fun registerChangeExamButtonListeners() {
        binding.changeGradeExamButton.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        startActivity(Intent(context, SwitchGoalExamActivity::class.java))
                    }
                }
            }
            false
        }
    }

    private fun setNetworkObserver() {
        internetCheckMLD.observe(viewLifecycleOwner, Observer { value ->
            if (value)
                isInternetAvailable()
        })
    }

    private fun setFragment(fragment: Fragment, layoutResId: Int) {
        childFragmentManager.beginTransaction().replace(
            layoutResId,
            fragment
        ).commit()
    }

    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        when (childFragment) {
            is LearnBannerFragment -> {
                childFragment.setDPadKeysListener(this)
                childFragment.setNavigationMenuListener(this)
                childFragment.bannerImageDataListener(this)
            }
            is LearnRowsFragment -> {
                childFragment.setNavigationMenuListener(this)
                childFragment.setRowsToBannerListener(this)
                childFragment.setDPadKeysListener(this)
            }
            is QuickDashboardFragment -> {
                childFragment.setDPadKeysListener(this)
                childFragment.setNavigationMenuListener(this)
            }
        }
    }

    fun setHostToNavigationListener(callback: HostToNavigationListener) {
        this.hostToNavigationListener = callback
    }

    override fun onDetach() {
        super.onDetach()
        hostToNavigationListener = null
    }

    override fun onHeaderItemChange(data: HeaderBannerData) {
        Log.i(classTag, "data from header: $data")
    }

    private fun slideBannerUp() {
        learnRowsFragment.isBannerVisible(false)
        val bannerBGLearnOA = ObjectAnimator.ofFloat(
            binding.flBannerBgLearn,
            TRANSLATION_Y,
            0f,
            Values.BANNER_TRANSLATION_Y
        )
        val bannerLearnOA = ObjectAnimator.ofFloat(
            binding.flBannerLearn,
            TRANSLATION_Y,
            0f,
            Values.BANNER_TRANSLATION_Y
        )
        val rowsLearnOA =
            ObjectAnimator.ofFloat(binding.flRowLearn, TRANSLATION_Y, 0f, Values.ROWS_TRANSLATION_Y)
        val animatorSet = AnimatorSet()
        animatorSet.apply {
            playTogether(bannerBGLearnOA, bannerLearnOA, rowsLearnOA)
            duration = 500
            doOnEnd {
                binding.flBannerLearn.visibility = View.INVISIBLE
                binding.flBannerBgLearn.visibility = View.INVISIBLE
            }
            start()
        }

        binding.flRowLearn.setPadding(0, 50, 0, 0)

        lastFocusedContent = "Rows"
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        Log.i(classTag, "Network: $isConnected")
        // if (!isConnected)
        /*Toast.makeText(App.context, "Please check your internet connection", Toast.LENGTH_SHORT)
            .show()*/
    }

    private fun isNetworkConnected(): Boolean {
        val cm =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null && cm.activeNetworkInfo.isConnected
    }

    private fun isInternetAvailable(): Boolean {
        return try {
            val command = "ping -c 1 google.com"
            if (Runtime.getRuntime().exec(command).waitFor() == 0) {
                internetCheckMLD.postValue(false)
            }
            return Runtime.getRuntime().exec(command).waitFor() == 0

        } catch (e: Exception) {
            Log.e(classTag, " isInternetAvailable ${e.message}")
            internetCheckMLD.postValue(true)
            false
        }
    }

    override fun onResume() {
        super.onResume()
        MyInternetTest.scheduleJob()
        App.setConnectivityListener(this)
        restoreLastSelection(false)
    }

    override fun navMenuToggle(toShow: Boolean) {
        hostToNavigationListener?.showNavigation(toShow)
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {
        if (isDown) {
            when (from) {
                "Rows" -> {
                    slideBannerUp()
//                    learnBannerImgFlipper.stopImgFlipping()
                }
                "Banner" -> {
                    slideBannerUp()
                    learnRowsFragment.setFirstRowFocus()
                    // learnBannerImgFlipper.stopImgFlipping()
                    // learnBannerImgFlipper.bannerVideoPlayer.pausePlayback()
                }
                "Dashboard" -> {
                    lastFocusedContent = "Banner"
                    hideQuickDashboard()
                }
            }
        }
    }


    private fun hideQuickDashboard() {
        val slideUp: Animation =
            AnimationUtils.loadAnimation(context, R.anim.slide_up)
        // learnBannerImgFlipper.startImgFlipping()
        binding.dashboardFL.startAnimation(slideUp)
        learnBannerFragment.setLastFocusedBtn()
        binding.learnCL.visibility = View.VISIBLE
        binding.learnCL.animate().translationY(0f).duration = 500
        binding.dashboardFL.postDelayed({
            binding.dashboardFL.visibility = View.GONE
        }, 10)
        try {
            childFragmentManager.beginTransaction()
                .remove(childFragmentManager.findFragmentById(binding.dashboardFL.id)!!).commit()
        } catch (e: Exception) {

        }
    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {
        if (isUp) {
            when (from) {
                "Banner" -> {
                    doAsync {
                        val quickLinksDataAvailable =
                            homeViewModel.fetchSectionByPageName(AppConstants.QUICK_LINKS)
                                .isNotEmpty()

                        uiThread {
                            if (quickLinksDataAvailable) {
                                showQuickDashBoard()
                                lastFocusedContent = "Dashboard"
                                //  learnBannerImgFlipper.bannerVideoPlayer.pausePlayback()

                            }
                        }
                    }
                }
            }
        }
    }

    private fun showQuickDashBoard() {
        val slideDown: Animation =
            AnimationUtils.loadAnimation(context, R.anim.slide_down)
        binding.dashboardFL.visibility = View.VISIBLE
        quickDashboardFragment = QuickDashboardFragment()
        setFragment(quickDashboardFragment, binding.dashboardFL.id)
        binding.dashboardFL.startAnimation(slideDown)
        binding.dashboardFL.setPadding(0, 50, 0, 0)
        binding.learnCL.animate().translationY(1200f).duration = 700
        binding.learnCL.postDelayed({
            binding.learnCL.visibility = View.GONE
        }, 10)
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {
    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {
        if (isEnter) {
            when (from) {
                "Rows" -> {
                    if (cardType.toLowerCase(Locale.getDefault()) == "practice")
                        hostToNavigationListener?.switchToPractice()
                    else if (cardType.toLowerCase(Locale.getDefault()) == "learn") {
                        hostToNavigationListener?.switchToLearn()
                    }
                }
            }
        }
    }

    override fun rowsToBanner() {
        learnRowsFragment.isBannerVisible(true)
        binding.flBannerLearn.requestFocus()
        learnBannerFragment.setLastFocusedBtn()
        //  learnBannerImgFlipper.bannerVideoPlayer.pausePlayback()

        val bannerBGLearnOA = ObjectAnimator.ofFloat(
            binding.flBannerBgLearn,
            TRANSLATION_Y,
            Values.BANNER_TRANSLATION_Y,
            0f
        )
        val bannerLearnOA = ObjectAnimator.ofFloat(
            binding.flBannerLearn,
            TRANSLATION_Y,
            Values.BANNER_TRANSLATION_Y,
            0f
        )
        val rowsLearnOA =
            ObjectAnimator.ofFloat(binding.flRowLearn, TRANSLATION_Y, Values.ROWS_TRANSLATION_Y, 0f)
        val animatorSet = AnimatorSet()
        animatorSet.apply {
            playTogether(bannerBGLearnOA, bannerLearnOA, rowsLearnOA)
            duration = 500
            doOnStart {
                binding.flBannerLearn.visibility = View.VISIBLE
                binding.flBannerBgLearn.visibility = View.VISIBLE
            }
            start()
        }

        binding.flRowLearn.setPadding(0, 0, 0, 0)
        lastFocusedContent = "Banner"
    }

    override fun notifyBanner(notifyMessage: String) {
        when (notifyMessage) {
            "Synced" -> {
                binding.flBannerLearn.postDelayed({
                    binding.flBannerLearn.requestFocus()
                    learnBannerFragment.setLastFocusedBtn()
                }, 10)
            }
        }
    }

    override fun notifyHost(notifyMessage: String) {
        when (notifyMessage) {
            AppConstants.NO_CONTENT -> {
                contentAvailable = false
                toggleNoContentView(View.VISIBLE)
            }
            AppConstants.CONTENT_AVAILABLE -> {
                contentAvailable = true
                toggleNoContentView(View.GONE)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun toggleNoContentView(visibility: Int) {
        binding.noContentTv.visibility = visibility
        binding.changeGradeExamButton.visibility = visibility
        binding.learnCL.visibility = View.GONE - visibility
        binding.dashboardFL.visibility = View.GONE - visibility

        if (visibility == View.VISIBLE) {
            binding.noContentTv.text = getString(R.string.text_goal_not_supported) + " " +
                    DataManager.instance.getExamNameByCode(
                        UserData.getGoalCode(),
                        UserData.getExamCode()
                    )
            binding.changeGradeExamButton.postDelayed({
                binding.changeGradeExamButton.requestFocus()
            }, 100)
        } else {
            binding.changeGradeExamButton.postDelayed({
                binding.changeGradeExamButton.clearFocus()
            }, 100)
        }
    }

    fun restoreLastSelection(fromNavigation: Boolean) {
        when (contentAvailable) {
            false -> {
                binding.changeGradeExamButton.postDelayed({
                    binding.changeGradeExamButton.requestFocus()
                }, 100)
            }
            true -> {
                when {
                    lastFocusedContent.equals("Banner", true) -> {
                        learnBannerFragment.setLastFocusedBtn()
                    }
                    lastFocusedContent.equals("Dashboard", true) -> {
                        binding.dashboardFL.requestFocus()
                        quickDashboardFragment.restoreLastSelection(fromNavigation)
                    }
                    else -> {
                        binding.flRowLearn.requestFocus()
                        learnRowsFragment.restoreLastSelection(fromNavigation)
                    }
                }
            }
        }
    }

    override fun setBannerImageData(data: BannerData, position: Int) {
        learnBannerFragment.setContentData(data)
    }

    fun pauseBannerSliding() {
        // learnBannerImgFlipper.bannerVideoPlayer.player?.release()
    }

    fun resumeBannerSliding() {
        // learnBannerImgFlipper.bannerVideoPlayer.pausePlayback()
    }

    object Values {
        const val BANNER_TRANSLATION_Y = -550f
        const val ROWS_TRANSLATION_Y = -600f
    }
}

