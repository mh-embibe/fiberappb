package com.embibe.embibetvapp.ui.fragment.learn

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import android.view.View.FOCUS_LEFT
import androidx.leanback.widget.*
import androidx.lifecycle.ViewModelProviders
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseRowsSupportFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.ui.interfaces.RowsToBannerListener
import com.embibe.embibetvapp.ui.presenter.CardPresenterSelector
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.*
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class LearnRowsFragment : BaseRowsSupportFragment(), DPadKeysListener {

    private var resultsData: ArrayList<ResultsEntity> = arrayListOf()
    private var mRowsAdapter: ArrayObjectAdapter = ArrayObjectAdapter(ShadowRowPresenterSelector())
    private lateinit var volleyRequest: RequestQueue
    private lateinit var navigationMenuCallback: NavigationMenuCallback
    private lateinit var rowsToBannerListener: RowsToBannerListener
    private lateinit var dPadKeysCallback: DPadKeysListener
    private val classTag = LearnRowsFragment::class.java.toString()
    private var continueLearningRowIndex = 0
    private var isContinueLearningRowFound = false
    private var continueLearningItemsSize = 0
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var listener: SharedPreferences.OnSharedPreferenceChangeListener
    private var previewUrlTimer: CountDownTimer? = null
    var pref = PreferenceHelper()
    var offset: Int = 0
    var initialize: Boolean = false
    private var itemFocusedPos = -1
    private var lastItemFocusPos = -1
    private var isBannerVisible = true

    init {
        initializeListeners()
    }

    private fun loadData(isUpdate: Boolean, isRefresh: Boolean) {
        if (!isUpdate) {
            // showProgress()
        }
        doAsync {
            resultsData =
                homeViewModel.fetchSectionByPageName(AppConstants.LEARN) as ArrayList<ResultsEntity>
            uiThread {
                makeLog("size of local Data : ${resultsData.size}")
                if (resultsData.isEmpty()) {
                    loadHomeDataFromApiAsync(false)
                } else {
                    rowsToBannerListener.notifyHost(AppConstants.CONTENT_AVAILABLE)
                    if (isUpdate) {
                        updateRows(resultsData)
                    } else {

                        //hideProgress()
                        createRows(resultsData)
                    }
                    if (isRefresh) {
                        loadHomeDataFromApiAsync(true)
                    }
                    rowsToBannerListener.notifyBanner("Synced")
                }


            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        volleyRequest = Volley.newRequestQueue(activity)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

        navigationMenuCallback.navMenuToggle(false)
        if (DataManager.instance.isHomeDataRemoved) {
            loadData(false, true)
            getQuickLinksApiAsync()
        } else {
            resultsData = DataManager.instance.getHome()
            createRows(resultsData)
            loadHomeDataFromApiAsync(true)
        }

    }

    override fun onResume() {
        super.onResume()
        /*check for content updated status*/
        try {
             restoreLastSelection(false)
            val preferences: SharedPreferences =
                App.context.getSharedPreferences(
                    "app_embibe",
                    Context.MODE_MULTI_PROCESS
                )
            if (Utils.isStatusUpdatedForVideosAndCoobos || preferences.getBoolean(
                    AppConstants.ISCOOBO_UPDATED,
                    false
                )
            ) {
                Utils.isStatusUpdatedForVideosAndCoobos = false
                preferences.edit().putBoolean(AppConstants.ISCOOBO_UPDATED, false).apply()
                /*refreshing home screen*/
                if (mRowsAdapter.size() > 0) {
                    val row = mRowsAdapter.get(1) as CardListRow
                    if (row.headerItem.name == "Continue Learning") {
                        isContinueLearningRowFound = true
                        continueLearningRowIndex = 1
                        val section = resultsData[continueLearningRowIndex]
                        paginateSection(section)
                    } else {
                        /*ContinueLearning Not found*/
                        isContinueLearningRowFound = false
                        makeLog("content-status refreshing home screen")
                        homeViewModel.homeApi(object :
                            BaseViewModel.APICallBacks<List<ResultsEntity>> {
                            override fun onSuccess(model: List<ResultsEntity>?) {
                                if (model != null && model.isNotEmpty()) {
                                    val continueSection = model[2]
                                    if (continueSection.sectionId == 300L) {
                                        DataManager.instance.insertCoobo(
                                            model as ArrayList<ResultsEntity>,
                                            object :
                                                BaseViewModel.DataCallback<ArrayList<ResultsEntity>> {
                                                override fun onSuccess(model: ArrayList<ResultsEntity>?) {
                                                    if (model != null) {
                                                        insertNewRowAsContinueLearning(
                                                            model,
                                                            continueSection
                                                        )
                                                    }
                                                }

                                            })

                                    }
                                }
                            }

                            override fun onFailed(code: Int, error: String, msg: String) {
                            }
                        })
                    }

                }

            }
            if (Utils.isContentStatusUpdatedForQuickLinks || preferences.getBoolean(
                    AppConstants.ISCOOBO_UPDATED_FOR_QUICKLINKS,
                    false
                ) || Utils.isLikeBookmarkUpdatedForQuickLinks
            ) {
                if (Utils.isLikeBookmarkUpdatedForQuickLinks) {
                    Utils.isLikeBookmarkUpdatedInLearn = true
                }
                if (preferences.getBoolean(
                        AppConstants.ISCOOBO_UPDATED_FOR_QUICKLINKS,
                        false
                    )
                ) {
                    Utils.isContentStatusUpdatedForQuickLinksInLearn = true
                    preferences.edit()
                        .putBoolean(AppConstants.ISCOOBO_UPDATED_FOR_QUICKLINKS, false).apply()
                }
                Utils.isContentStatusUpdatedForQuickLinks = false
                Utils.isLikeBookmarkUpdatedForQuickLinks = false

                /*refreshing quicklinks*/
                getQuickLinksApiAsync()
            }

            if (Utils.isSwitchGoalDone) {
                // Utils.isSwitchGoalDone = false
                loadHomeDataFromApiAsync(false)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            makeLog("content-status Exception ${e.printStackTrace()}")
        }

    }

    private fun paginateSection(section: ResultsEntity) {
        section.size = continueLearningItemsSize + paginationSize
        homeViewModel.homeSectionApi(
            section.content_section_type,
            0,
            section.size,
            object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
                override fun onSuccess(model: List<ResultsEntity>?) {
                    if (model != null && model.isNotEmpty()) {
                        val newSection = model[0]
                        if (resultsData.get(1).sectionId != 300L) {
                            resultsData.add(1, section)
                            val callback = object : BaseViewModel.DataCallback<String> {
                                override fun onSuccess(model: String?) {
                                    makeLog("Updated Saved to Local!")
                                }

                            }
                            dataRepo.removeListData(resultsData)
                            homeViewModel.saveResultToDb(
                                resultsData,
                                AppConstants.LEARN,
                                null,
                                callback
                            )
                            homeViewModel.saveResultToDb(
                                resultsData,
                                AppConstants.LEARN,
                                null,
                                callback
                            )
                        } else {
                            newSection.objId = section.objId
                            updateSectionToDB(null, AppConstants.LEARN, newSection)
                        }

                        updateContinueLearning(newSection)
                    } else {
                        removeSection()
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {


                }
            })
    }

    private fun updateContinueLearning(newSection: ResultsEntity) {
        /*update new Row as ContinueLearning*/
        val row = mRowsAdapter.get(continueLearningRowIndex) as CardListRow
        val rowItemsAdapter = row.adapter as ArrayObjectAdapter
        rowItemsAdapter.setItems(getContentItems(newSection), null)
        mRowsAdapter.replace(
            continueLearningRowIndex,
            CardListRow(HeaderItem(newSection.section_name), rowItemsAdapter)
        )
        saveContinueLearningIndex(newSection.section_name, continueLearningRowIndex)
    }

    private fun insertNewRowAsContinueLearning(
        newResults: ArrayList<ResultsEntity>,
        section: ResultsEntity
    ) {
        /*create new Row as ContinueLearning*/
        if (resultsData.get(1).sectionId != 300L) {

            dataRepo.removeListData(resultsData)
            val callback = object : BaseViewModel.DataCallback<String> {
                override fun onSuccess(model: String?) {
                    makeLog("Updated Saved to Local!")
                }

            }
            resultsData.add(1, section)
            homeViewModel.saveResultToDb(newResults, AppConstants.LEARN, null, callback)
        }

        mRowsAdapter.add(1, creatingNewRow(section))
        saveContinueLearningIndex(section.section_name, 1)

    }

    private fun removeSection() {
        if (resultsData[1].sectionId == 300L) {
            val row = mRowsAdapter.get(1) as CardListRow
            val rowItemsAdapter = row.adapter as ArrayObjectAdapter
            mRowsAdapter.remove(resultsData[1])
            mRowsAdapter.removeItems(1, 1)
            dataRepo.removeSection(resultsData[1])
        }

    }


    private fun getQuickLinksApiAsync() {
        homeViewModel.homeQuickLinksApi(object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
            override fun onSuccess(model: List<ResultsEntity>?) {
                if (model != null) {
                    doAsync {
                        homeViewModel.saveResultToDb(model, AppConstants.QUICK_LINKS)
                    }


                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {

            }
        })
    }

    fun loadHomeDataFromApiAsync(isAsync: Boolean) {
        if (!isAsync) {
            //showProgress()
        }
        homeViewModel.homeApi(object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
            override fun onSuccess(model: List<ResultsEntity>?) {
                if (!isAsync) {
                    //  hideProgress()
                }
                if (model != null) {
                    doAsync {
                        DataManager.instance.setHome(arrayListOf(), null)

                        if (model.size - 1 != mRowsAdapter.size()) {
                            makeLog("objects differs")
                            dataRepo.removeListData(resultsData)
                            homeViewModel.saveResultToDb(
                                model,
                                AppConstants.LEARN,
                                null,
                                object : BaseViewModel.DataCallback<String> {
                                    override fun onSuccess(s: String?) {
                                        loadData(false, true)
                                    }
                                })
                        } else {
                            makeLog("objects same")
                            val callback =
                                object : BaseViewModel.DataCallback<String> {
                                    override fun onSuccess(s: String?) {
                                        if (mRowsAdapter.size() == 0) {
                                            loadData(false, false)
                                        } else {
                                            val row =
                                                mRowsAdapter.get(1) as CardListRow
                                            if (model[2].sectionId != 300L && row.headerItem.name == "Continue Learning") {
                                                /*new api doesnt have CL, but local has CL*/
                                                removeSection()
                                                dataRepo.removeListData(resultsData)
                                                homeViewModel.saveResultToDb(
                                                    model,
                                                    AppConstants.LEARN,
                                                    null,
                                                    object :
                                                        BaseViewModel.DataCallback<String> {
                                                        override fun onSuccess(s: String?) {
                                                            loadData(false, true)
                                                        }
                                                    })
                                            } else {
                                                if (!isAsync) {
                                                    //hideProgress()
                                                    loadData(true, false)
                                                }
                                            }
                                        }

                                    }

                                }
                            homeViewModel.saveResultToDb(
                                model,
                                AppConstants.LEARN,
                                null,
                                callback
                            )
                        }

                    }

                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                if (!isAsync) {
                    if (code == 204) //no content success
                        rowsToBannerListener.notifyHost(AppConstants.NO_CONTENT)

                    //hideProgress()
                    if (Utils.isApiFailed(code)) {
                        Utils.showError(
                            context,
                            code,
                            object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: String) {
                                    loadHomeDataFromApiAsync(isAsync)
                                }

                                override fun onDismiss() {

                                }
                            })
                    } else {
                        showToast(error)
                    }
                }

            }
        })
    }

    private fun updateRows(resultsData: List<ResultsEntity>) {
        try {
            for ((index, video) in resultsData.withIndex()) {
                val row = mRowsAdapter.get(index) as CardListRow
                val rowItemsAdapter = row.adapter as ArrayObjectAdapter
                rowItemsAdapter.setItems(getContentItems(video), null)
                mRowsAdapter.replace(
                    index,
                    CardListRow(HeaderItem(video.section_name), rowItemsAdapter)
                )
                saveContinueLearningIndex(video.section_name, index)
            }
            restoreLastSelection(false)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun paginateApiAsync(
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter
    ) {
        val section = resultsData[indexOfRow]
        makeLog("updatedSection paginate ${section.content_section_type} ${section.content?.size} ${section.content?.size}")
        if (section.total > calculateOffset(section)) {
            section.size = paginationSize
            homeViewModel.homeSectionApi(
                section.content_section_type,
                calculateOffset(section),
                section.size,
                object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
                    override fun onSuccess(model: List<ResultsEntity>?) {
                        if (model != null && model.isNotEmpty()) {
                            updateSection(
                                model[0],
                                section,
                                indexOfRow,
                                lastIndexOfRow,
                                currentAdapter
                            )
                        }
                    }

                    override fun onFailed(code: Int, error: String, msg: String) {


                    }
                })

        }

    }

    private fun updateSection(
        resultsDataEntity: ResultsEntity,
        section: ResultsEntity,
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter
    ) {
        resultsDataEntity.content?.let { section.content?.addAll(it) }
        section.offset = resultsDataEntity.offset
        section.size = resultsDataEntity.size
        makeLog("updatedSection old section to DB Size : ${section.content?.size}")
        if (resultsDataEntity.sectionId == 300L) {
            continueLearningItemsSize = section.content?.size ?: 0
        }
        /*local DB*/
        updateSectionToDB(null, AppConstants.LEARN, section)
        /*UI*/
        bindSectionToUI(
            section,
            indexOfRow,
            lastIndexOfRow,
            currentAdapter,
            resultsDataEntity.content ?: arrayListOf()
        )

    }

    private fun updateSectionToDB(subject: String? = null, page: String, section: ResultsEntity) {

        section.subject = subject ?: "all"
        section.child_id = UserData.getChildId().toLong()
        section.page = page
        section.grade = UserData.getGrade()
        section.goal = UserData.getGoalCode()
        section.exam = UserData.getExamCode()
        resultsData

        dataRepo.modifySection(section, object : BaseViewModel.DataCallback<ResultsEntity> {
            override fun onSuccess(model: ResultsEntity?) {

            }
        })
    }

    private fun getUpdatedSection(
        sectionId: Long,
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter,
        newContentList: ArrayList<Content>
    ) {
        val updatedSection = homeViewModel.getSectionById(sectionId, AppConstants.LEARN)
        //bindSectionToUI(updatedSection, indexOfRow, lastIndexOfRow, currentAdapter,newContentList)
    }

    private fun bindSectionToUI(
        updatedSection: ResultsEntity?,
        indexOfRow: Int,
        lastIndexOfRow: Int,
        currentAdapter: ArrayObjectAdapter,
        newContentList: ArrayList<Content>
    ) {
        /*update the Section */
        currentAdapter.addAll(lastIndexOfRow, newContentList)
        makeLog("updatedSection?.content size :  ${updatedSection?.content?.size}")
    }

    private fun saveContinueLearningIndex(sectionName: String, index: Int) {
        if (sectionName == "Continue Learning")
            continueLearningRowIndex = index
    }


    private fun createRows(resultsData: List<ResultsEntity>) {
        if (mRowsAdapter.size() > 0) {
            mRowsAdapter.clear()
        }
        setBookDetails(resultsData.filter { it.sectionId == 6L })

        for ((index, video) in resultsData.withIndex()) {
            mRowsAdapter.add(creatingNewRow(video))
            saveContinueLearningIndex(video.section_name, index)
        }
    }

    private fun creatingNewRow(videos: ResultsEntity): Row {
        val presenterSelector = activity?.baseContext?.let { CardPresenterSelector(it) }
        presenterSelector?.setDPadKeysListener(this)
        var adapter: ArrayObjectAdapter? = null
        try {
            adapter = ArrayObjectAdapter(presenterSelector)
        } catch (e: Exception) {

        }
        for (video in getContentItems(videos)) {
            adapter?.add(video)
        }
        val headerItem = HeaderItem(videos.section_name)

        return CardListRow(headerItem, adapter!!)
    }

    inline fun <reified T : Any> Any.cast(): T {
        return this as T
    }

    private fun initializeListeners() {
        adapter = mRowsAdapter
        onItemViewClickedListener =
            OnItemViewClickedListener { itemViewHolder, item, rowViewHolder, row ->
                Utils.isSubjectFilterFromPractice = false
                Utils.subjectFilterBy = AppConstants.LEARN
                onClickItem(item as Content, row, mRowsAdapter, itemViewHolder)
            }


        onItemViewSelectedListener =
            OnItemViewSelectedListener { itemViewHolder, item, rowViewHolder, row ->
                var currentRowSize = ((row as CardListRow).adapter as ArrayObjectAdapter).size()
                val indexOfItem = (row.adapter as ArrayObjectAdapter).indexOf(item)
                val indexOfRow = mRowsAdapter.indexOf(row)
                if ((currentRowSize - (indexOfItem + 1)) == 5) {
                    updateRowItemForAdapter(
                        indexOfRow,
                        currentRowSize,
                        (row.adapter as ArrayObjectAdapter)
                    )
                }
                if (item is Content) {
                    SegmentUtils.trackEventHomeTileFocus(
                        item,
                        row.headerItem.name,
                        indexOfRow,
                        indexOfItem
                    )

                    val itemView = itemViewHolder.view
                    if (itemView is GifCardView) {
                        addDelayToPreviewUrls(item, itemView)
                    } else if (itemView is BookVideoCardView) {
                       /// addDelayToVideoUrls(indexOfRow, item, itemView)
                    }

                    saveLastSelected(indexOfItem, indexOfRow)
                    itemView.setOnKeyListener { v, keyCode, event ->
                        //checking only for down presses
                        if (event.action == KeyEvent.ACTION_DOWN) {
                            when (keyCode) {
                                KeyEvent.KEYCODE_DPAD_LEFT -> {
                                    previewUrlTimer?.cancel()
                                    if (itemView is GifCardView) {
                                        itemView.ivVideo.controller?.animatable?.stop()
                                    }
                                    if (indexOfItem == 0) {
                                        navigationMenuCallback.navMenuToggle(true)
                                    }
                                }
                                KeyEvent.KEYCODE_DPAD_RIGHT -> {
                                    previewUrlTimer?.cancel()
                                    if (itemView is GifCardView) {
                                        itemView.ivVideo.controller?.animatable?.stop()
                                    }
                                }
                                KeyEvent.KEYCODE_DPAD_UP -> {
                                    previewUrlTimer?.cancel()
                                    if (itemView is GifCardView) {
                                        itemView.ivVideo.controller?.animatable?.stop()
                                    }
                                    if (indexOfRow == 0 && !Utils.isKeyPressedTooFast(500)) {
                                        rowsToBannerListener.rowsToBanner()
                                    }
                                }
                                KeyEvent.KEYCODE_DPAD_DOWN -> {
                                    previewUrlTimer?.cancel()
                                    if (itemView is GifCardView) {
                                        itemView.ivVideo.controller?.animatable?.stop()
                                    }
                                    if (isBannerVisible)
                                        dPadKeysCallback.isKeyPadDown(true, "Rows")
                                    //for last row
                                    if (indexOfRow == mRowsAdapter.size() - 1) {
                                        itemView.postDelayed({
                                            itemView.requestFocus()
                                        }, 0)
                                    }
                                }
                            }
                        }
                        false
                    }
                }

            }

    }

    fun isBannerVisible(visible: Boolean) {
        isBannerVisible = visible
    }

    private fun addDelayToVideoUrls(indexOfRow: Int, item: Content, itemView: BookVideoCardView) {
        previewUrlTimer = object : CountDownTimer(4000, 1000) {
            override fun onFinish() {
                itemView.showVideoView(item.teaser_url)
            }

            override fun onTick(millisUntilFinished: Long) {}
        }
        previewUrlTimer?.start()
    }

    private fun addDelayToPreviewUrls(
        item: Content,
        itemView: GifCardView
    ) {
        previewUrlTimer = object : CountDownTimer(4000, 1000) {
            override fun onFinish() {
                if (item.preview_url.isNotEmpty())
                    itemView.showVideoView(item.preview_url)
            }

            override fun onTick(millisUntilFinished: Long) {}
        }
        previewUrlTimer?.start()
    }

    private fun updateRowItemForAdapter(
        indexOfRow: Int,
        lastIndexOfRow: Int,
        arrayObjectAdapter: ArrayObjectAdapter
    ) {
        makeLog("updatedSection paginate init ${resultsData.size}")
        if (resultsData.isNotEmpty()) {
            paginateApiAsync(indexOfRow, lastIndexOfRow, arrayObjectAdapter)

        }/*else if (lastIndexOfRow>0){
            loadData(true)
        }*/
    }

    private fun saveLastSelected(indexOfItem: Int, indexOfRow: Int) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        with(sharedPref?.edit()) {
            this?.putInt(getString(R.string.last_selected_learn_row_item), indexOfItem)
            this?.putInt(getString(R.string.last_selected_learn_row), indexOfRow)
            this?.apply()
        }
    }

    fun restoreLastSelection(fromNavigation: Boolean) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        var itemIndex = sharedPref.getInt(getString(R.string.last_selected_learn_row_item), 0)
        val rowIndex = sharedPref.getInt(getString(R.string.last_selected_learn_row), 0)


        if (fromNavigation) {//for 0th item and Navigation interaction case
            val rvh = getRowViewHolder(rowIndex)
            rvh?.selectedItemViewHolder?.view?.focusSearch(FOCUS_LEFT)?.requestFocus()
        } else {
            //For Continue Learning row, 0th item has to be focused
            if (rowIndex == continueLearningRowIndex)
                itemIndex = 0

            val rowsSize = mRowsAdapter.size()
            if (rowIndex < rowsSize) {
                val rowItemsSize = (mRowsAdapter.get(rowIndex) as CardListRow).adapter.size()
                if (itemIndex < rowItemsSize) {
                    setSelectedPosition(
                        rowIndex,
                        true,
                        object : ListRowPresenter.SelectItemViewHolderTask(itemIndex) {
                            override fun run(holder: Presenter.ViewHolder?) {
                                super.run(holder)//imp line
                                holder?.view?.postDelayed({
                                    holder.view.requestFocus()
                                    //hideProgress()
                                }, 10)
                            }
                        })
                }
            }
        }
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    fun setRowsToBannerListener(callback: RowsToBannerListener) {
        this.rowsToBannerListener = callback
    }

    fun setFirstRowFocus() {
        var rowViewHolder = mainFragmentRowsAdapter.findRowViewHolderByPosition(0)
        try {
            Handler().postDelayed({
                rowViewHolder.view.requestFocus()
                mainFragmentRowsAdapter.setSelectedPosition(0, true)
            }, 0)
            mainFragmentRowsAdapter.setSelectedPosition(1, false)
        } catch (e: Exception) {

        }
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {
        Log.i("Haseeb" ,"row down")
    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {
        Log.i("Haseeb" ,"row up")
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
        Log.i("Haseeb" ,"row left")
        navigationMenuCallback.navMenuToggle(true)
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {
        Log.i("Haseeb" ,"row right")
    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {
        Log.i("Haseeb" ,"row enter")
        dPadKeysCallback.isKeyPadEnter(true, "Rows", cardType)
    }


}