package com.embibe.embibetvapp.ui.fragment.errorAlertScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants.LEAVE_APP
import com.embibe.embibetvapp.databinding.FragmentAppCloseAlertMsgBinding
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener

class AppClosingAlertMsg : DialogFragment() {

    private lateinit var binding: FragmentAppCloseAlertMsgBinding
    private var msg: String? = null
    private var errorScreensBtnClickListener: ErrorScreensBtnClickListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, android.R.style.ThemeOverlay)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_app_close_alert_msg,
            container, false
        )
        binding.ivAlertMsg.setImageResource(R.drawable.ic_alert_msg)
        if (!msg.isNullOrEmpty())
            binding.tvMsg2.text = msg
        setCloseBtnClickListener()
        setLeaveAppBtbClickListener()

        return binding.root
    }

    private fun setLeaveAppBtbClickListener() {
        binding.btnLeaveApp.setOnClickListener {
            errorScreensBtnClickListener?.screenUpdate(LEAVE_APP)
        }
    }

    private fun setCloseBtnClickListener() {
        binding.btnCancel.setOnClickListener {
            dismiss()
        }
    }

    fun setListener(listener: ErrorScreensBtnClickListener) {
        errorScreensBtnClickListener = listener
    }

    fun setMsg(msg: String) {
        this.msg = msg
    }


}