/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.embibe.embibetvapp.ui.presenter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.leanback.widget.BaseCardView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.utils.Utils.getColorCode

/**
 * This Presenter will display a card consisting of an image on the left side of the card followed
 * by text on the right side. The image and text have equal width. The text will work like a info
 * box, thus it will be hidden if the parent row is inactive. This behavior is unique to this card
 * and requires a special focus handler.
 */
class BasicCardPresenter(context: Context, var type: String) :
    AbstractCardPresenter<BaseCardView>(context) {
    lateinit var cardView: BaseCardView

    @SuppressLint("InflateParams")
    override fun onCreateView(): BaseCardView {
        val cardView =
            BaseCardView(context, null, R.style.SideInfoCardStyle)
        cardView.addView(LayoutInflater.from(context).inflate(R.layout.card_video, null))
        cardView.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            val viewAlpha = cardView.findViewById<View>(R.id.view_focus)
            if (hasFocus) {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_focused_alpha_value
                )
            } else {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_unfocused_alpha_value
                )
            }
        }
        cardView.isFocusable = true
        return cardView
    }

    override fun onBindViewHolder(item: Any, cardView: BaseCardView) {
        val card = item as Content

        val tvSubjectName: TextView = cardView.findViewById(R.id.tvSubjectName)
        val imgThumbnailView: ImageView = cardView.findViewById(R.id.iv_thumbnail)
        val ivEmbibeLogo: ImageView = cardView.findViewById(R.id.ivEmbibeLogo)
        val ivCategory: ImageView = cardView.findViewById(R.id.ivCategory)
        val progress_bar: ProgressBar = cardView.findViewById(R.id.progress_bar)

        if (card.watched_duration > 0) {
            progress_bar.visibility = View.VISIBLE
            if (card.type.toLowerCase().equals(AppConstants.COOBO.toLowerCase())) {
                progress_bar.max = card.length / 50
            } else {
                progress_bar.max = card.length
            }
            progress_bar.progress = card.watched_duration
        }
        if (card.subject.isNotEmpty() && !type.equals(AppConstants.LEARN_CHAPTER)) {
            tvSubjectName.visibility = View.VISIBLE
            tvSubjectName.text = card.subject
            (tvSubjectName.background as GradientDrawable).setColor(
                context.resources.getColor(
                    getColorCode(
                        card.subject
                    )
                )
            )
        } else {
            tvSubjectName.visibility = View.GONE
        }

        if (card.thumb != "") {
            val requestOptions = RequestOptions().transform(RoundedCorners(8))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                .load(card.thumb)
                .transition(DrawableTransitionOptions.withCrossFade())
                .placeholder(R.drawable.video_placeholder)
                .into(imgThumbnailView)
        } else {
            Glide.with(context)
                .load(R.drawable.video_placeholder)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imgThumbnailView)
        }

        Glide.with(context).load(card.owner_info.copy_logo).into(ivEmbibeLogo)

        when (type) {
            AppConstants.COOBO -> {
                Glide.with(context)
                    .load(R.drawable.ic_coobo_category)
                    .into(ivCategory)
            }
            AppConstants.LEARN_CHAPTER -> {
                Glide.with(context)
                    .load(R.drawable.ic_multi_video)
                    .into(ivCategory)
            }
            else -> {
                Glide.with(context)
                    .load(card.category_thumb)
                    .into(ivCategory)
            }
        }
    }


}