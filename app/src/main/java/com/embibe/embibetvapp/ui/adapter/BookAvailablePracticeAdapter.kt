package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemPracticeDetailBookAvailableBinding
import com.embibe.embibetvapp.newmodel.Content

class BookAvailablePracticeAdapter(private val mContext: Context) :
    RecyclerView.Adapter<BookAvailablePracticeAdapter.BookViewHolder>() {

    var list = ArrayList<Content>()
    var onItemClick: ((Content) -> Unit)? = null
    var onItemFocused: ((Int) -> Unit?)? = null
    var onItemDpadHit: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val binding: ItemPracticeDetailBookAvailableBinding = DataBindingUtil.inflate(
            LayoutInflater.from(mContext),
            R.layout.item_practice_detail_book_available,
            parent,
            false
        )
        return BookViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (list.size > 0) list.size else 0
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun setData(list: ArrayList<Content>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class BookViewHolder(var binding: ItemPracticeDetailBookAvailableBinding) :
        DetailAdapter.ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)

            Glide.with(itemView.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(item.thumb)
                .placeholder(R.drawable.practice_placeholder)
                .error(R.drawable.practice_placeholder)
                .into(binding.ivThumbnail)

            binding.executePendingBindings()
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(adapterPosition)
            }

            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            onItemDpadHit?.invoke(adapterPosition)
                        }
                    }

                }

                false
            }
        }
    }


}



