package com.embibe.embibetvapp.ui.activity

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Intent
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.graphics.Color
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.View.*
import android.webkit.*
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.airbnb.lottie.LottieDrawable
import com.android.volley.RequestQueue
import com.android.volley.VolleyError
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.application.App.Companion.context
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.ANNOTATION_DETAILS
import com.embibe.embibetvapp.constant.AppConstants.ANNOTATION_MODEL
import com.embibe.embibetvapp.constant.AppConstants.CONTENT_TYPE
import com.embibe.embibetvapp.constant.AppConstants.CONTINUE_LEARNING
import com.embibe.embibetvapp.constant.AppConstants.IS_RECOMMENDATION_ENABLED
import com.embibe.embibetvapp.constant.AppConstants.RESUME
import com.embibe.embibetvapp.constant.AppConstants.START
import com.embibe.embibetvapp.constant.AppConstants.TOPIC_LEARN_PATH
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_AUTHORS
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CONCEPT_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_DESCRIPTION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SOURCE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SUBJECT
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TITLE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TOTAL_DURATION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_URL
import com.embibe.embibetvapp.databinding.ActivityTopicSummaryBinding
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkResponse
import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.embibe.embibetvapp.model.response.attemptquality.AttemptQualityResponse
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.fragment.learn.TopicSummaryFragment
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener
import com.embibe.embibetvapp.ui.viewmodel.DetailsViewModel
import com.embibe.embibetvapp.unity.UnityPlayerActivity
import com.embibe.embibetvapp.utils.ConnectionManager
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.VideoUtils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo
import kotlinx.android.synthetic.main.activity_detail.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class TopicSummaryActivity : BaseFragmentActivity(), ErrorScreensBtnClickListener {

    private lateinit var binding: ActivityTopicSummaryBinding
    private lateinit var detailsViewModel: DetailsViewModel
    private var isJarEnabled: Boolean = false


    private lateinit var listener: OnSharedPreferenceChangeListener
    private lateinit var volleyRequest: RequestQueue
    private lateinit var builder: AlertDialog.Builder
    private lateinit var dialog: AlertDialog
    private lateinit var data: Content
    private lateinit var topicSummaryFragment: TopicSummaryFragment
    private var lastWatchedData: Content? = null
    private var isBookMarked: Boolean = false
    private var isLiked: Boolean = false
    private var bookmarkRetryCount: Int = 0
    private var likeRetryCount: Int = 0
    private var pref = PreferenceHelper()
    private var isVideoResumed: Boolean = false
    private var relatedTopics: ArrayList<Content> = arrayListOf()
    private var testsForTopic: ArrayList<Content> = arrayListOf()
    private var practicesForTopic: ArrayList<Content> = arrayListOf()
    private var videosForTopic: ArrayList<ResultsEntity> = arrayListOf()
    private var prerequisitesForTopic: ArrayList<Content> = arrayListOf()
    private val REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124
    private var preview_url =
        "https://visualintelligence.blob.core.windows.net/video-summary/prod/4397012/summary.webp"
    private var thumb =
        "https://content-grail-production.s3.amazonaws.com/practice-temp-tiles/1p45wyBVwdAeFP29gv57nU28ymT793kC1.webp"
    private var jarType: String = ""
    private lateinit var attemptQualityRes: AttemptQualityResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_topic_summary)
        detailsViewModel = ViewModelProviders.of(this).get(DetailsViewModel::class.java)

        volleyRequest = Volley.newRequestQueue(this)
        listenForDataPrefsUpdate()
        getDetail()
        setInitialFocusToLearn()
        setOptionsListeners()
        setFocusListeners()
        permissionsCheck()
        bgTransformation()
        initViewSincerity()
        initViewAttemptQuality("")
        setAttemptQualityView("")
        setJarFocusListener()
        getAttemptQualityData()
        setWebViewTopicSummaryGraph(binding.wvPrimaryProgress)

    }

    override fun onResume() {
        super.onResume()
        if (::data.isInitialized) {
            getBookmarkStatus()
            getLikeStatus()
            SegmentUtils.trackEventScreenLoadEnd(
                isLiked,
                isBookMarked,
                content = data
            )
        }

    }

    private fun listenForDataPrefsUpdate() {

        listener = OnSharedPreferenceChangeListener { sharedPreferences, key ->
            when (key) {
                AppConstants.LAST_WATCHED_CONTENT + "_" + data.content_id -> {
                    getLastWatchedContentData()
                }
                AppConstants.ALL_VIDEOS_FOR_THIS_TOPIC + "_" + data.id -> {
                    toggleOptionsVisibility(binding.videosForThisTopic.tag.toString())
                }
                AppConstants.RELATED_TOPICS + "_" + data.id -> {
                    toggleOptionsVisibility(binding.relatedTopics.tag.toString())
                }
                AppConstants.PRE_REQUISITE_TOPICS + "_" + data.id -> {
                    toggleOptionsVisibility(binding.preRequisiteTopics.tag.toString())
                }
                AppConstants.TEST_ON_THIS_TOPIC + "_" + data.id -> {
                    toggleOptionsVisibility(binding.testOnThisTopic.tag.toString())
                }
                AppConstants.PRACTICE_ON_THIS_TOPIC + "_" + data.id -> {
                    toggleOptionsVisibility(binding.practiceOnThisTopic.tag.toString())
                }
            }
        }
        pref.preferences?.registerOnSharedPreferenceChangeListener(listener)
    }

    private fun getLastWatchedContentData() {
        hideProgress()
        val lastWatchedContent = DataManager.instance.getLastWatchedContent()
        val lastWatchedContents = lastWatchedContent?.content
        if (lastWatchedContents?.size != 0) {
            lastWatchedData = lastWatchedContents?.get(0)
            if (lastWatchedData?.url!!.isNotEmpty()) {
                if (lastWatchedData?.watched_duration != 0)
                    showContinueDialog()
                else
                    moveToPlayer()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::listener.isInitialized) {
            pref.preferences?.unregisterOnSharedPreferenceChangeListener(listener)
        }
    }

    private fun showHideLine() {
        //if none of the options are showing, hide the line
//        if (!binding.videosForThisTopic.isVisible
//            && !binding.relatedTopics.isVisible
//            && !binding.preRequisiteTopics.isVisible
//            && !binding.testOnThisTopic.isVisible
//            && !binding.practiceOnThisTopic.isVisible
//        ) {
//            binding.view.visibility = GONE
//        } else {
//            binding.view.visibility = VISIBLE
//        }
    }

    private fun getDetail() {
        data = getContent(intent)
        binding.hereIsWhereYouStandTitle.text =
            getString(R.string.here_is_where_you_stand_on) + " " + data.title
        val captureDescText =
            getString(R.string.capture_based_on_your_learning_practicing_and_testing_on) + " " + data.title
        binding.cardSincerityScore.tvDescription.text = captureDescText
        binding.cardAttemptScore.tvDescription.text = captureDescText
        SegmentUtils.trackEventScreenLoadStart(isLiked, isBookMarked, content = data)
        binding.content = data
        setDetails()
        if (data.bg_thumb.isEmpty())
            Utils.setBackgroundImage(
                Utils.getBackgroundUrl(data),
                binding.imgDescBackground,
                binding.imgDescBackgroundBlur
            )
        else
            Utils.setBackgroundImage(
                data.bg_thumb,
                binding.imgDescBackground,
                binding.imgDescBackgroundBlur
            )
    }

    private fun toggleOptionsVisibility(tag: String) {
        when (tag) {
            getString(R.string.all_videos_for_this_topic) -> {
                videosForTopic = DataManager.instance.getVideosForTopic()
                if (videosForTopic.isNotEmpty()) {
                    setVisibility(binding.videosForThisTopic, VISIBLE)
                } else {
                    setVisibility(binding.videosForThisTopic, GONE)
                }
            }
            getString(R.string.related_topics) -> {
                relatedTopics = DataManager.instance.getRelatedTopics()
                if (relatedTopics.isNotEmpty()) {
                    setVisibility(binding.relatedTopics, VISIBLE)
                } else {
                    setVisibility(binding.relatedTopics, GONE)
                }
            }
            getString(R.string.pre_requisite_topics) -> {
                prerequisitesForTopic = DataManager.instance.getPrerequisitesForTopic()
                if (prerequisitesForTopic.isNotEmpty()) {
                    setVisibility(binding.preRequisiteTopics, VISIBLE)
                } else {
                    setVisibility(binding.preRequisiteTopics, GONE)
                }
            }
            getString(R.string.test_on_this_topic) -> {
                testsForTopic = DataManager.instance.getTestsForTopic()
                if (testsForTopic.isNotEmpty()) {
                    setVisibility(binding.testOnThisTopic, VISIBLE)
                } else {
                    setVisibility(binding.testOnThisTopic, GONE)
                }
            }
            getString(R.string.practice_on_this_topic) -> {
                practicesForTopic = DataManager.instance.getPracticesForTopic()
                if (practicesForTopic.isNotEmpty()) {
                    setVisibility(binding.practiceOnThisTopic, VISIBLE)
                } else {
                    setVisibility(binding.practiceOnThisTopic, GONE)
                }
            }
        }
        showHideLine()
    }

    private fun setVisibility(view: Button, visibility: Int) {
        view.visibility = visibility
    }

    private fun setDetails() {
        if (data.title != "") {
            Log.d(TAG, data.title)
            /// JFR-5281
            var updateStr = Utils.getTruncatedString(data.title, 25)
            binding.title.text = updateStr

        } else binding.title.visibility = GONE

        if (data.subject != "") {
            binding.tvSubjectName.text = data.subject
        } else binding.tvSubjectName.visibility = GONE

        if (data.type != "topic")
            binding.textTime.text = Utils.convertIntoMins(data.length)
        else
            binding.textTime.text = Utils.convertIntoConcepts(data.concept_count)

        binding.embiumsCount.text = getString(R.string.earn) + " ${data.currency}"

        if (data.description != "") {
            binding.textDescription.text = data.description
        } else binding.textDescription.visibility = GONE

        if (data.watched_duration != 0) {
            btnLearn.text = getString(R.string.continue_learning)
        }
    }

    private fun getBookmarkStatus() {
        detailsViewModel.getBookmarkStatus(data.id,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                val data = dataList.filter {
                                    it.contentId.equals(data.id)
                                }
                                updateBookMarkUI(data[0].status)
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg ")

                }
            })
    }

    private fun getLikeStatus() {
        detailsViewModel.getLikeStatus(data.id,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                if (dataList.isNotEmpty()) {
                                    val data = dataList.filter {
                                        it.contentId.equals(data.id)
                                    }
                                    updateLikUI(data[0].status)
                                }
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg")
                }
            })
    }

    private fun setInitialFocusToLearn() {
        btnLearn.requestFocus()
        btnLearn.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_videos_for_chapter_black, 0, 0, 0
        )
    }

    private fun setOptionsListeners() {
        binding.btnLearn.setOnClickListener {
            showProgress()
            Utils.getLastWatchedContent(this, AppConstants.EMBIBE_SYLLABUS, data.content_id)
        }

        binding.videosForThisTopic.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
//                        learnSummaryFragment.requestFocus()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        val bundle = Bundle()
                        bundle.putString("type", getString(R.string.all_videos_for_this_topic))
                        bundle.putParcelableArrayList(
                            AppConstants.ALL_VIDEOS_FOR_THIS_TOPIC,
                            videosForTopic
                        )
                        showDetails(bundle)
                    }
                }
            }

            false
        }

        binding.relatedTopics.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
//                        learnSummaryFragment.requestFocus()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        val bundle = Bundle()
                        bundle.putString("type", getString(R.string.related_topics))
                        bundle.putParcelableArrayList(
                            AppConstants.RELATED_TOPICS,
                            relatedTopics
                        )
                        showDetails(bundle)
                    }
                }
            }

            false
        }

        binding.preRequisiteTopics.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
//                        learnSummaryFragment.requestFocus()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        val bundle = Bundle()
                        bundle.putString("type", getString(R.string.pre_requisite_topics))
                        bundle.putParcelableArrayList(
                            AppConstants.PRE_REQUISITE_TOPICS,
                            prerequisitesForTopic
                        )
                        showDetails(bundle)
                    }
                }
            }

            false
        }

        binding.testOnThisTopic.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
//                        learnSummaryFragment.requestFocus()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        val bundle = Bundle()
                        bundle.putString("type", getString(R.string.test_on_this_topic))
                        bundle.putParcelableArrayList(
                            AppConstants.TEST_ON_THIS_TOPIC,
                            testsForTopic
                        )
                        showDetails(bundle)
                    }
                }
            }

            false
        }

        binding.practiceOnThisTopic.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
//                        learnSummaryFragment.requestFocus()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        val bundle = Bundle()
                        bundle.putString("type", getString(R.string.practice_on_this_topic))
                        bundle.putParcelableArrayList(
                            AppConstants.PRACTICE_ON_THIS_TOPIC,
                            practicesForTopic
                        )
                        showDetails(bundle)
                    }
                }
            }

            false
        }

        binding.btnBookmark.setOnClickListener {
            Utils.isLikeBookmarkUpdatedForQuickLinks = true
            isBookMarked = !isBookMarked
            updateBookMarkUI(isBookMarked)
            SegmentUtils.trackEventMoreInfoBookmarkClick(isBookMarked, content = data)
            timerBookmark.cancel()
            timerBookmark = Timer()
            timerBookmark.schedule(object : TimerTask() {
                override fun run() {
                    updateBookmark()
                }
            }, interval)
        }

        binding.btnLike.setOnClickListener {
            isLiked = !isLiked
            updateLikUI(isLiked)
            SegmentUtils.trackEventMoreInfoLikeClick(isLiked, content = data)
            /*like api call*/
            timerLike.cancel()
            timerLike = Timer()
            timerLike.schedule(object : TimerTask() {
                override fun run() {
                    updateLike()
                }
            }, interval)
        }
    }

    private fun showContinueDialog() {
        dialog.show()
        //continueVideoDialog?.show(supportFragmentManager, "continue_video")
    }

    private fun hideDialog() {
        if (::dialog.isInitialized && dialog.isShowing)
            dialog.dismiss()
    }

    override fun onBackPressed() {
        if (isJarEnabled) {
            restoreJarFocus()
            binding.cardAttemptQuality.layout.visibility = View.VISIBLE
            binding.cardFour.layout.visibility = View.GONE
            isJarEnabled = false
            this.jarType = ""

        }
        hideDialog()
        super.onBackPressed()
    }

    private fun moveToPlayer() {
        if (!ConnectionManager.instance.hasNetworkAvailable()) {
            Utils.showError(this@TopicSummaryActivity, ApiConstants.API_CODE_NO_NETWORK,
                object : BaseViewModel.ErrorCallBacks {
                    override fun onRetry(msg: String) {
                        moveToPlayer()
                    }

                    override fun onDismiss() {

                    }
                })
            return
        }
        if (data.type.toLowerCase(Locale.getDefault()) == "Coobo".toLowerCase(Locale.getDefault())) {
            val i = Intent(context, UnityPlayerActivity::class.java)
            i.putExtra(VIDEO_CONCEPT_ID, lastWatchedData?.learning_map?.conceptId)
            i.putExtra(AppConstants.DURATION_LENGTH, lastWatchedData?.length)
            i.putExtra(AppConstants.SLIDE_COUNT, lastWatchedData?.watched_duration)
            i.putExtra(AppConstants.COOBO_URL, lastWatchedData?.url)
            i.putExtra(AppConstants.DETAILS_ID, lastWatchedData?.id)
            Utils.insertBundle(data, i)
            lastWatchedData?.learning_map.let()
            {
                i.putExtra(AppConstants.LEARNMAP_CODE, lastWatchedData?.learning_map?.lpcode)
            }
            i.putExtra(IS_RECOMMENDATION_ENABLED, true)
            startActivity(i)

        } else {
            val callback = object : BaseViewModel.APICallBackVolley {
                override fun <T> onSuccessCallBack(response: T) {
                    hideProgress()
                    response as JSONObject
                    startVideoPlayer(
                        ((response["files"] as JSONArray)[0] as JSONObject)["link"] as String,
                        AppConstants.PLAYER_TYPE_EXOPLAYER
                    )
                    /*hide ProgressBar here */
                }

                override fun onErrorCallBack(e: VolleyError) {
                    try {
                        makeLog("Error:  $e")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    /*hide ProgressBar here */
                    hideProgress()
                }
            }

            val type = Utils.getVideoTypeUsingUrl(lastWatchedData?.url!!)
            if (type == AppConstants.PLAYER_TYPE_EXOPLAYER) {
                showProgress()
                val videoId = VideoUtils.getVimeoVideoId(lastWatchedData?.url!!).toString()
                Utils.getVideoURL(this, videoId, lastWatchedData?.owner_info?.key, callback)
            } else {
                if (Utils.isYouTubeAppAvailable(this)) {
                    Utils.startYoutubeApp(this, lastWatchedData?.url ?: "")
                } else {
                    startVideoPlayer(lastWatchedData?.url!!, AppConstants.PLAYER_TYPE_YOUTUBE)
                }
            }
        }

    }

    private fun showDetails(bundle: Bundle) {
        switchViewsVisibility(INVISIBLE, VISIBLE)
        topicSummaryFragment =
            TopicSummaryFragment()
        topicSummaryFragment.arguments = bundle
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainerView, topicSummaryFragment)
            .commit()
    }

    private fun switchViewsVisibility(landingCLVisibility: Int, fragmentCVVisibility: Int) {
        if (binding.landingCL.visibility != landingCLVisibility)
            binding.landingCL.visibility = landingCLVisibility
        if (binding.fragmentContainerView.visibility != fragmentCVVisibility)
            binding.fragmentContainerView.visibility = fragmentCVVisibility
    }

    private fun bgTransformation() {
        Utils.setBlurBackgroundImage(
            binding.imgDescBackground
        )
    }

    private fun setFocusListeners() {
        binding.btnLearn.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
//                switchViewsVisibility(VISIBLE, INVISIBLE)
                btnLearn.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_videos_for_chapter_black, 0, 0, 0
                )
            } else {
                btnLearn.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_videos_for_chapter, 0, 0, 0
                )
            }
        }

        binding.btnBookmark.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackEventMoreInfoBookmarkFocus(isBookMarked, content = data)
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_black, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                    )
                }
            } else {
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_white, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                    )
                }
            }
        }

        binding.btnLike.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackEventMoreInfoLikeFocus(isLiked, content = data)
                if (isLiked) {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_filled_black, 0, 0, 0
                    )
                } else {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_stroke_black, 0, 0, 0
                    )
                }
            } else {
                if (isLiked) {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_filled_white, 0, 0, 0
                    )
                } else {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_stroke_white, 0, 0, 0
                    )
                }
            }
        }
        binding.cardAttemptScore.layout.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.cardAttemptScore.lvJar.visibility = View.VISIBLE
                binding.cardAttemptScore.ivPlay.visibility = View.INVISIBLE
                binding.cardAttemptScore.ivImg.visibility = View.INVISIBLE
                when (jarType) {
                    AppConstants.CORRECT -> binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_correct)
                    AppConstants.PERFECT -> binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_perfect_attempts)
                    AppConstants.OVERTIME_CORRECT -> binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_overtime_correct)
                    AppConstants.WASTED -> binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_wasted_attempt)
                    AppConstants.WRONG -> binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_wrong)
                    AppConstants.WASTED_IN_FOUR -> binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_wasted_attempt)
                    AppConstants.OVERTIME_WRONG -> binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_overtime_incorrect)
                    AppConstants.RECALL -> binding.cardAttemptScore.lvJar.setAnimation(R.raw.jar_recall)
                    AppConstants.ANALYTICAL_THINKING -> binding.cardAttemptScore.lvJar.setAnimation(R.raw.jar_analytical_tinking)
                    AppConstants.CONCEPTUAL_THINKING -> binding.cardAttemptScore.lvJar.setAnimation(R.raw.jar_conceptual_thinking)
                }
                binding.cardAttemptScore.lvJar.repeatCount = LottieDrawable.INFINITE
                binding.cardAttemptScore.lvJar.playAnimation()
            } else {
                binding.cardAttemptScore.lvJar.visibility = View.INVISIBLE
                binding.cardAttemptScore.ivPlay.visibility = View.VISIBLE
                binding.cardAttemptScore.ivImg.visibility = View.VISIBLE
            }
        }
    }

    private fun updateBookmark() {
        detailsViewModel.bookmark(data.id, data.type, isBookMarked,
            object : BaseViewModel.APICallBacks<CommonApiResponse> {
                override fun onSuccess(model: CommonApiResponse?) {
                    if (model != null && model.success) {
                        Utils.isLikeBookmarkUpdatedForQuickLinks = true
                        updateBookMarkUI(isBookMarked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (bookmarkRetryCount < retryAttempt) {
                        bookmarkRetryCount++
                        updateBookmark()
                        return
                    } else {
                        /*reset retry count*/
                        bookmarkRetryCount = 0
                        /*revert back to previous state of ui */
                        updateBookMarkUI(!isBookMarked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_BOOKMARK)
                    }
                }
            })
    }

    private fun updateLike() {
        detailsViewModel.like(data.id, data.type, isLiked,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!!) {
                        Utils.isLikeBookmarkUpdatedForQuickLinks = true
                        updateLikUI(isLiked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (likeRetryCount < retryAttempt) {
                        likeRetryCount++
                        updateLike()
                        return
                    } else {
                        /*reset retry count*/
                        likeRetryCount = 0
                        /*revert back to previous state of ui */
                        updateLikUI(!isLiked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_LIKE)
                    }
                }
            })
    }

    private fun updateBookMarkUI(bookmarked: Boolean) {
        isBookMarked = bookmarked
        data.isBookmarked = bookmarked

        if (binding.btnBookmark.hasFocus()) {
            if (isBookMarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        } else {
            if (isBookMarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        }
    }

    private fun updateLikUI(liked: Boolean) {
        isLiked = liked
        data.isLiked = liked

        if (binding.btnLike.hasFocus()) {
            if (isLiked) {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_filled_black, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.liked)
            } else {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_stroke_black, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.like)
            }
        } else {
            if (isLiked) {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_filled_white, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.liked)
            } else {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_stroke_white, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.like)
            }
        }
    }

    override fun screenUpdate(type: String) {
        when (type) {
            RESUME -> {

            }
            START -> {

            }
        }
    }

    private fun startVideoPlayer(url: String, source: String) {
        val intent = Intent(context, VideoPlayerActivity::class.java)
        var resumedDuration: Long? = null
        if (isVideoResumed) {
            if (DataManager.instance.getLastSeek(lastWatchedData?.id!!) != null)
                resumedDuration = DataManager.instance.getLastSeek(lastWatchedData?.id!!)
            if (resumedDuration != null)
                intent.putExtra(CONTINUE_LEARNING, resumedDuration.toString())
            else
                intent.putExtra(CONTINUE_LEARNING, lastWatchedData?.watched_duration.toString())
        } else
            intent.putExtra(CONTINUE_LEARNING, "0")
        if (lastWatchedData?.annotation_attributes != null && lastWatchedData?.annotation_attributes!!.size > 0) {
            intent.putExtra(ANNOTATION_DETAILS, true)
            intent.putParcelableArrayListExtra(
                ANNOTATION_MODEL,
                lastWatchedData?.annotation_attributes
            )
        }
        intent.putExtra(VIDEO_URL, url)
        intent.putExtra(VIDEO_ID, lastWatchedData?.id)
        intent.putExtra(VIDEO_CONCEPT_ID, lastWatchedData?.learning_map?.conceptId)
        intent.putExtra(CONTENT_TYPE, lastWatchedData?.type)
        intent.putExtra(VIDEO_TOTAL_DURATION, lastWatchedData?.length.toString())
        intent.putExtra(VIDEO_TITLE, lastWatchedData?.title)
        intent.putExtra(VIDEO_DESCRIPTION, lastWatchedData?.description)
        intent.putExtra(VIDEO_SUBJECT, lastWatchedData?.subject)
        intent.putExtra(VIDEO_CHAPTER, lastWatchedData?.learning_map?.chapter)
        intent.putExtra(VIDEO_AUTHORS, "")
        intent.putExtra(VIDEO_SOURCE, source)
        intent.putExtra(TOPIC_LEARN_PATH, lastWatchedData?.learning_map?.topicLearnPathName)
        intent.putExtra(IS_RECOMMENDATION_ENABLED, true)
        startActivity(intent)
    }

    private fun initViewSincerity() {
        val requestOptions = RequestOptions().transform(RoundedCorners(10))
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        Glide.with(context)
            .applyDefaultRequestOptions(requestOptions)
            .load(thumb)
            .placeholder(R.drawable.video_placeholder)
            .into(binding.cardSincerityScore.ivImg)

        binding.cardSincerityScore.layout.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.cardSincerityScore.cardView.visibility = VISIBLE
                binding.cardSincerityScore.ivGifView.visibility = VISIBLE
                val controller = Fresco.newDraweeControllerBuilder()
                controller.autoPlayAnimations = true
                controller.setUri(preview_url)
                controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                    override fun onFinalImageSet(
                        id: String?,
                        imageInfo: ImageInfo?,
                        animatable: Animatable?
                    ) {
                        val anim = animatable as AnimatedDrawable2
                        anim.setAnimationListener(object : AnimationListener {
                            override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                            override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                                binding.cardSincerityScore.ivImg.visibility = INVISIBLE
                            }

                            override fun onAnimationFrame(
                                drawable: AnimatedDrawable2?,
                                frameNumber: Int
                            ) {

                            }

                            override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                                binding.cardSincerityScore.ivImg.visibility = VISIBLE
                                binding.cardSincerityScore.ivGifView.visibility = INVISIBLE
                                binding.cardSincerityScore.cardView.visibility = View.INVISIBLE
                                binding.cardSincerityScore.ivPlay.visibility = VISIBLE
                            }

                            override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                        })
                    }
                }
                binding.cardSincerityScore.ivGifView.controller = controller.build()
            } else {
                binding.cardSincerityScore.ivImg.visibility = VISIBLE
                binding.cardSincerityScore.ivGifView.visibility = INVISIBLE
                binding.cardSincerityScore.cardView.visibility = INVISIBLE
                binding.cardSincerityScore.ivPlay.visibility = VISIBLE
            }
        }
    }


    private fun initViewAttemptQuality(jarType: String) {
        val requestOptions = RequestOptions().transform(RoundedCorners(10))
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        when (jarType) {
            AppConstants.CORRECT -> {
                Glide.with(context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_too_fast_correct)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardFour.ivImg)
            }
            AppConstants.PERFECT -> {
                Glide.with(context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_perfect_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardFour.ivImg)
            }
            AppConstants.OVERTIME_CORRECT -> {
                Glide.with(context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_overtime_correct)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardFour.ivImg)
            }
            AppConstants.WASTED -> {
                Glide.with(context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_wasted_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardFour.ivImg)
            }
            AppConstants.WASTED_IN_FOUR -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_wasted_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.WRONG -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_incorrect_answer)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.OVERTIME_WRONG -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_overtime_incorrect)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.CONCEPTUAL_THINKING -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_conceptual_thinking)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }

            AppConstants.RECALL -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_recall)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }

            AppConstants.ANALYTICAL_THINKING -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_analytical_thinking)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
        }

        when (jarType) {
            AppConstants.CORRECT -> binding.cardFour.lvJar.setAnimation(R.raw.jar_correct_attempt)
            AppConstants.PERFECT -> binding.cardFour.lvJar.setAnimation(R.raw.jar_perfect_attempt)
            AppConstants.OVERTIME_CORRECT -> binding.cardFour.lvJar.setAnimation(R.raw.jar_overtime_correct_attempt)
            AppConstants.WASTED -> binding.cardFour.lvJar.setAnimation(R.raw.jar_wasted_attempt)
            AppConstants.WRONG -> binding.cardFour.lvJar.setAnimation(R.raw.jar_wrong_attempt)
            AppConstants.OVERTIME_WRONG -> binding.cardFour.lvJar.setAnimation(R.raw.jar_overtime_incorrect_attempt)
        }
        binding.cardFour.lvJar.loop(true)
        binding.cardFour.lvJar.playAnimation()

        binding.cardAttemptScore.layout.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.cardAttemptScore.lvJar.visibility = VISIBLE
                binding.cardAttemptScore.ivPlay.visibility = INVISIBLE
                binding.cardAttemptScore.ivImg.visibility = INVISIBLE
                binding.cardAttemptScore.lvJar.setAnimation(R.raw.jar_correct)
                binding.cardAttemptScore.lvJar.repeatCount = LottieDrawable.INFINITE
                binding.cardAttemptScore.lvJar.playAnimation()
            } else {
                binding.cardAttemptScore.lvJar.visibility = INVISIBLE
                binding.cardAttemptScore.ivPlay.visibility = VISIBLE
                binding.cardAttemptScore.ivImg.visibility = VISIBLE
            }
        }
    }

    private fun setJarFocusListener() {
        binding.cardAttemptQuality.jarCorrectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.CORRECT)
            }
        }
        binding.cardAttemptQuality.jarPerfectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.PERFECT)
            }
        }
        binding.cardAttemptQuality.jarOvertimeCorrectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.OVERTIME_CORRECT)
            }
        }
        binding.cardAttemptQuality.jarWastedAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.WASTED)
            }
        }
        binding.cardAttemptQuality.jarWrongAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.WRONG)
            }
        }
        binding.cardAttemptQuality.jarOvertimeWrongAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.OVERTIME_WRONG)
            }
        }
        binding.cardAttemptQuality.jarRecall.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.RECALL)
            }
        }
        binding.cardAttemptQuality.jarAnalyticalThinking.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.ANALYTICAL_THINKING)
            }
        }
        binding.cardAttemptQuality.jarConceptualThinking.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.CONCEPTUAL_THINKING)
            }
        }
        binding.cardAttemptQuality.jarWastedAttemptNew.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setJarClickListener(AppConstants.WASTED_IN_FOUR)
            }
        }
    }

    private fun setJarClickListener(jarType: String) {
        when (jarType) {
            AppConstants.CORRECT -> binding.cardAttemptQuality.jarCorrectAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
            }
            AppConstants.PERFECT -> binding.cardAttemptQuality.jarPerfectAttempt.setOnClickListener {
                animateJar(jarType)

                // setAttemptQualityView(jarType)
            }
            AppConstants.OVERTIME_CORRECT -> binding.cardAttemptQuality.jarOvertimeCorrectAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)

            }
            AppConstants.WASTED -> binding.cardAttemptQuality.jarWastedAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)

            }
            AppConstants.WRONG -> binding.cardAttemptQuality.jarWrongAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)

            }
            AppConstants.OVERTIME_WRONG -> binding.cardAttemptQuality.jarOvertimeWrongAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)

            }
            AppConstants.RECALL -> binding.cardAttemptQuality.jarRecall.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)

            }
            AppConstants.ANALYTICAL_THINKING -> binding.cardAttemptQuality.jarAnalyticalThinking.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)

            }
            AppConstants.CONCEPTUAL_THINKING -> binding.cardAttemptQuality.jarConceptualThinking.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)

            }
            AppConstants.WASTED_IN_FOUR -> binding.cardAttemptQuality.jarWastedAttemptNew.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)

            }
        }

    }

    private fun animateJar(jarType: String) {
        when (jarType) {
            AppConstants.CORRECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -60f
            ).apply {
                duration = 1000
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(1)


                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 1000

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.CONCEPTUAL_THINKING -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -30f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(7)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.PERFECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -40f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(2)

                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500
                    }

                    override fun onAnimationCancel(animation: Animator) {}
                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.RECALL -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -80f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(8)

                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500
                    }

                    override fun onAnimationCancel(animation: Animator) {}
                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.OVERTIME_CORRECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -120f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(3)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.ANALYTICAL_THINKING -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -160f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(9)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.WASTED_IN_FOUR -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -220f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(10)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.WASTED -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -240f
            ).apply {
                duration = 1000
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(4)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 1000


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.WRONG -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -300f
            ).apply {
                duration = 1000
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(5)


                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 1000


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.OVERTIME_WRONG -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -360f
            ).apply {
                duration = 1000
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(6)


                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 1000


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
        }
    }


    private fun setAttemptQualityView(jarType: String) {
        initViewAttemptQuality(jarType)
        this.jarType = jarType
        binding.cardAttemptQuality.layout.visibility = View.GONE
        binding.cardFour.layout.visibility = View.VISIBLE
        binding.cardAttemptQuality.layout.setOnClickListener {
            binding.cardAttemptScore.layout.visibility = VISIBLE
            binding.cardAttemptScore.layout.postDelayed({
                binding.cardAttemptScore.layout.requestFocus()
                binding.cardAttemptQuality.layout.visibility = GONE
            }, 10)
        }
    }

    private fun viewAnimationImage(position: Int) {
        isJarEnabled = true
        binding.imgAttemptJarIcon.alpha = 1.0F
        binding.imgAttemptJarIcon.visibility = View.VISIBLE
        when (position) {
            1 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_too_fast_correct)
            }
            2 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_perfect_attempt)

            }
            3 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_correct)

            }
            4 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_wasted_attempt)

            }
            5 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_incorrect_answer)

            }
            6 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_incorrect)

            }
            8 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_recall)}
            7 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_conceptual_thinking)}
            9 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_analytical_thinking)}
            10 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_wasted_attempt)}
        }
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        if (position < 5)
            lp.setMargins((position - 1) * 60, 20, 0, 0)
        if (position == 5)
            lp.setMargins((position - 1) * 70, 20, 0, 0)
        if (position == 6)
            lp.setMargins((position - 1) * 80, 20, 0, 0)

        binding.imgAttemptJarIcon.layoutParams = lp
    }
    private fun restoreJarFocus() {
        when (jarType) {
            AppConstants.CORRECT -> binding.cardAttemptQuality.jarCorrectAttempt.requestFocus()
            AppConstants.PERFECT -> binding.cardAttemptQuality.jarPerfectAttempt.requestFocus()
            AppConstants.OVERTIME_CORRECT -> binding.cardAttemptQuality.jarOvertimeCorrectAttempt.requestFocus()
            AppConstants.WASTED -> binding.cardAttemptQuality.jarWastedAttempt.requestFocus()
            AppConstants.WRONG -> binding.cardAttemptQuality.jarWrongAttempt.requestFocus()
            AppConstants.OVERTIME_WRONG -> binding.cardAttemptQuality.jarOvertimeWrongAttempt.requestFocus()
            AppConstants.RECALL ->binding.cardAttemptQuality.jarRecall.requestFocus()
            AppConstants.ANALYTICAL_THINKING ->binding.cardAttemptQuality.jarAnalyticalThinking.requestFocus()
            AppConstants.CONCEPTUAL_THINKING ->binding.cardAttemptQuality.jarConceptualThinking.requestFocus()
            AppConstants.WASTED_IN_FOUR ->binding.cardAttemptQuality.jarWastedAttemptNew.requestFocus()
        }
    }

    private fun getAttemptQualityData() {
        detailsViewModel.getUserAttemptQualityData(
            data.learning_map.format_id,
            data.learnpath_format_name,
            data.learnpath_name,
            object : BaseViewModel.APICallBacks<AttemptQualityResponse> {
                override fun onSuccess(model: AttemptQualityResponse?) {
                    if (model != null) {
                        attemptQualityRes = model
                        setJarVisibility()
                    }
                    makeLog("")
                }

                override fun onFailed(code: Int, error: String, msg: String) {

                }
            })
    }

    private fun setJarVisibility() {
        if(::attemptQualityRes.isInitialized) {
            if (attemptQualityRes.data.conceptual_thinking != null) {
                binding.cardAttemptQuality.layoutFourJars.visibility = View.VISIBLE
                binding.cardAttemptQuality.layoutSixJars.visibility = View.GONE
            } else {
                binding.cardAttemptQuality.layoutFourJars.visibility = View.GONE
                binding.cardAttemptQuality.layoutSixJars.visibility = View.VISIBLE
            }
        }
    }

    private fun getAttemptPercentage(value: String): Int {
        Log.i("attempt data",value)
        if(value.isNotEmpty() || value.isNotBlank())
            return value.toDouble().toInt()
        return 0
    }

    private fun updateAttemptQualityData(jarType: String) {
        var jarName=jarType
        when(jarName)
        {
            AppConstants.RECALL->{jarName="Recall"}
            AppConstants.ANALYTICAL_THINKING->{jarName="Analytical Thinking"}
            AppConstants.CONCEPTUAL_THINKING->{jarName="Conceptual Thinking"}
            AppConstants.WRONG->{jarName="Wrong"}
            AppConstants.WASTED_IN_FOUR->{jarName="Wasted"}
        }
        if(::attemptQualityRes.isInitialized) {
            val percentage: Int = when (jarType) {
                AppConstants.CORRECT -> getAttemptPercentage(attemptQualityRes.data.overtime_correct!!.value) + getAttemptPercentage(
                    attemptQualityRes.data.perfect_attempt!!.value
                )
                AppConstants.PERFECT -> getAttemptPercentage(attemptQualityRes.data.perfect_attempt!!.value)
                AppConstants.OVERTIME_CORRECT -> getAttemptPercentage(attemptQualityRes.data.overtime_correct!!.value)
                AppConstants.WASTED -> getAttemptPercentage(attemptQualityRes.data.wasted_attempt!!.value)
                AppConstants.WRONG -> getAttemptPercentage(attemptQualityRes.data.wasted_attempt!!.value) + getAttemptPercentage(
                    attemptQualityRes.data.wasted_attempt!!.value
                )
                AppConstants.OVERTIME_WRONG -> getAttemptPercentage(attemptQualityRes.data.overtime_incorrect!!.value)
                AppConstants.RECALL -> getAttemptPercentage(attemptQualityRes.data.recall!!.value)
                AppConstants.ANALYTICAL_THINKING -> getAttemptPercentage(attemptQualityRes.data.analytical_thinking!!.value)
                AppConstants.CONCEPTUAL_THINKING -> getAttemptPercentage(attemptQualityRes.data.conceptual_thinking!!.value)
                AppConstants.WASTED_IN_FOUR -> getAttemptPercentage(attemptQualityRes.data.wasted_attempt!!.value)
                else -> 0
            }
            binding.cardAttemptScore.tvTopic.text = "$percentage% ${jarName}"
        }
        else
            binding.cardAttemptScore.tvTopic.text = "0% " + jarName
    }

    private fun setWebViewTopicSummaryGraph(webView: WebView) {
        val indexPath = "TestKGwidget/index.html"
        val template = "file:///android_asset/$indexPath"
        webView.webChromeClient = WebChromeClient()
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                Log.w("WEBVIEW", "Web view ready")
            }

            override fun onReceivedError(
                view: WebView,
                request: WebResourceRequest,
                error: WebResourceError
            ) {
                //isLoaded = false
                Log.w("WEBVIEW", "Web view failed to load")
                val errorMessage = "$error"
                makeLog(errorMessage)
                //setProgressDialogVisibility(false)
                super.onReceivedError(view, request, error)
            }
        }
        webView.settings.allowFileAccess = true
        webView.settings.allowFileAccessFromFileURLs = true
        webView.settings.allowUniversalAccessFromFileURLs = true
        webView.isLongClickable = false
        webView.settings.javaScriptEnabled = true
        webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        webView.settings.domStorageEnabled = true
        webView.setBackgroundColor(Color.argb(0, 0, 0, 0))
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        webView.loadUrl(template)
    }

    companion object {
        private const val TAG = "TopicSummaryActivity"
    }
}