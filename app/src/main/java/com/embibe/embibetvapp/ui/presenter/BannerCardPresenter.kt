package com.embibe.embibetvapp.ui.presenter

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.CountDownTimer
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.leanback.widget.BaseCardView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.utils.SegmentUtils
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory

class BannerCardPresenter(context: Context) : AbstractCardPresenter<BaseCardView>(context) {
    private lateinit var dPadKeysCallback: DPadKeysListener
    private lateinit var previewUrlTimer: CountDownTimer
    private lateinit var simpleExoPlayer: SimpleExoPlayer
    lateinit var imageView: ImageView
    lateinit var playerView: PlayerView
    lateinit var item: Content
    lateinit var title: TextView
    lateinit var textView: TextView
    lateinit var btnAchieveNow: Button

    private lateinit var mediaDataSourceFactory: DataSource.Factory
    override fun onCreateView(): BaseCardView {
        val cardView = BaseCardView(context, null)
        cardView.isFocusable = false
        cardView.addView(
            LayoutInflater.from(context).inflate(
                R.layout.card_banner, null
            )
        )
        return cardView
    }


    override fun onBindViewHolder(card: Any, cardView: BaseCardView) {
        item = card as Content
        btnAchieveNow = cardView.findViewById(R.id.btnAchieveNow)
        title = cardView.findViewById(R.id.title)
        textView = cardView.findViewById(R.id.textView)
        playerView = cardView.findViewById(R.id.playerView)
        imageView = cardView.findViewById(R.id.image)
/*        if (item.teaser_url.isEmpty()) {
            item.preview_url =
                "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/Practice.mp4"
        } else {
            item.preview_url = item.teaser_url
        }*/
        title.text = item.title
        textView.text = item.description

        when (item.category) {
            AppConstants.PRACTICE -> {
                item.preview_url =
                    "https://embibe-demo.s3-ap-southeast-1.amazonaws.com/trailerVideos/Practice.mp4"
                btnAchieveNow.text = "Practice Now"
                setDrawable(R.drawable.practice_banner)
            }
            AppConstants.TEST -> {
                item.preview_url =
                    "https://embibe-demo.s3-ap-southeast-1.amazonaws.com/trailerVideos/Test.mp4"
                btnAchieveNow.text = context.getString(R.string.start_test)
                setDrawable(R.drawable.test_banner)
            }
            AppConstants.LEARN -> {
                item.preview_url =
                    "https://embibe-demo.s3-ap-southeast-1.amazonaws.com/trailerVideos/Achieve.mp4"
                btnAchieveNow.text = context.getString(R.string.learn_now)
                setDrawable(R.drawable.learn_banner)
            }
            else -> {
                item.preview_url =
                    "https://embibe-demo.s3-ap-southeast-1.amazonaws.com/trailerVideos/Learn.mp4"
                btnAchieveNow.text = context.getString(R.string.achieve_now)
                setDrawable(R.drawable.achieve_banner)
            }
        }


        btnAchieveNow.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                        SegmentUtils.trackEventHomeBannerClick(item)
                        dPadKeysCallback.isKeyPadEnter(true, "BannerCardPresenter", item.category)
                        SegmentUtils.trackTestScreenAdBannerClick(item)
                    }

                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        dPadKeysCallback.isKeyPadLeft(true, "BannerCardPresenter")
                    }
                }
            }
            false
        }

        btnAchieveNow.setOnFocusChangeListener { view, b ->

            if (b) {
                addDelayToVideoUrls()
            } else {
                hideVideoView()
            }

            SegmentUtils.trackEventHomeBannerFocus(item)
            SegmentUtils.trackTestScreenAdBannerFocus(item)
        }

    }

    override fun onUnbindViewHolder(viewHolder: ViewHolder) {
        super.onUnbindViewHolder(viewHolder)
        if (::previewUrlTimer.isInitialized)
            previewUrlTimer.cancel()
        releasePlayer()
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }


    private fun addDelayToVideoUrls() {
        previewUrlTimer = object : CountDownTimer(2000, 1000) {
            override fun onFinish() {
                initializePlayer(playerView, item.preview_url)
                showVideoView()
            }

            override fun onTick(millisUntilFinished: Long) {}
        }
        previewUrlTimer.start()
    }


    private fun initializePlayer(playerView: PlayerView, previewURl: String) {

        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(context)
        mediaDataSourceFactory = DefaultDataSourceFactory(context, "mediaPlayerSample")
        val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory)
            .createMediaSource(Uri.parse(previewURl))
        simpleExoPlayer.prepare(mediaSource, false, false)
        simpleExoPlayer.playWhenReady = false
        playerView.setShutterBackgroundColor(Color.TRANSPARENT)
        playerView.player = simpleExoPlayer
        playerView.requestFocus()
        simpleExoPlayer.addListener(object : Player.DefaultEventListener() {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    Player.STATE_IDLE -> {
                    }
                    Player.STATE_BUFFERING -> {
                    }
                    Player.STATE_READY -> {
                        // showVideoView()
                    }
                    Player.STATE_ENDED -> {
                        hideVideoView()
                    }
                }
            }
        })
    }

    fun showVideoView() {
        imageView.visibility = View.GONE
        title.alpha = 0F
        textView.alpha = 0F
        btnAchieveNow.alpha = 0F
        simpleExoPlayer.playWhenReady = true
    }


    fun hideVideoView() {
        previewUrlTimer.cancel()
        imageView.visibility = View.VISIBLE
        title.alpha = 1F
        textView.alpha = 1F
        btnAchieveNow.alpha = 1F
        if (::simpleExoPlayer.isInitialized) {
            simpleExoPlayer.playWhenReady = false
            simpleExoPlayer.seekTo(0)
        }
    }


    private fun releasePlayer() {
        if (::simpleExoPlayer.isInitialized)
            simpleExoPlayer.release()
    }


    private fun setDrawable(drawableInt: Int) {
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(context).setDefaultRequestOptions(requestOptions)
            .load(drawableInt)
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.banner_placeholder)
            .transform(RoundedCorners(8))
            .into(imageView)
    }

}
