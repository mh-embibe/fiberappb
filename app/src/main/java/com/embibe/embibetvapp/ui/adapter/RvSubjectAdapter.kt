package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemSubjectCreateNewTestBinding
import com.embibe.embibetvapp.model.createtest.examconfig.SubjectDetails
import java.util.regex.Matcher
import java.util.regex.Pattern

class RvSubjectAdapter(var context: Context, var listner: OnSelectedSubjectDetails) :
    RecyclerView.Adapter<RvSubjectAdapter.ViewHolder>() {

    private var adapterList: MutableList<SubjectDetails>? = null

    val TAG = this.javaClass.name

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemSubjectCreateNewTestBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.item_subject_create_new_test,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(adapterList!![position])
    }

    override fun getItemCount(): Int = adapterList!!.size

    fun setData(list: MutableList<SubjectDetails>) {
        adapterList = list
        notifyDataSetChanged()
    }

    fun clearList() {
        adapterList!!.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(var binding: ItemSubjectCreateNewTestBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SubjectDetails) {

            binding.tvSubjectName.text = capitalize(item.subjectName!!)
            binding.imgSubjectIcon.setImageResource(item.subjectIcon!!)
            binding.llMain.setBackgroundResource(item.bgSubject!!)
            binding.flMain.setBackgroundResource(R.drawable.bg_create_new_test)
            binding.tvSubjectName.setCheckMarkDrawable(R.drawable.bg_transparent_create_new_test)
            binding.tvSubjectName.isChecked = false
            binding.flMain.setOnClickListener {
                val value: Boolean = binding.tvSubjectName.isChecked
                if (value) {
                    binding.tvSubjectName.setCheckMarkDrawable(R.drawable.bg_transparent_create_new_test)
                    binding.tvSubjectName.isChecked = false
                    listner.unSelectedSubjectDetails(item)
                } else {
                    binding.tvSubjectName.setCheckMarkDrawable(R.drawable.ic_tick)
                    binding.tvSubjectName.isChecked = true
                    listner.selectedSubjectDetails(item)
                }

            }
        }


    }

    interface OnSelectedSubjectDetails {
        fun selectedSubjectDetails(subjectName: SubjectDetails)
        fun unSelectedSubjectDetails(subjectName: SubjectDetails)

    }

    private fun capitalize(capString: String): String? {
        val capBuffer = StringBuffer()
        val capMatcher: Matcher =
            Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString)
        while (capMatcher.find()) {
            capMatcher.appendReplacement(
                capBuffer,
                capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase()
            )
        }
        return capMatcher.appendTail(capBuffer).toString()
    }


}