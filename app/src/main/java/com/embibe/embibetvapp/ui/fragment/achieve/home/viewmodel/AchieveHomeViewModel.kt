package com.embibe.embibetvapp.ui.fragment.achieve.home.viewmodel

import androidx.lifecycle.ViewModel
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.model.achieve.home.AchieveHome
import com.embibe.embibetvapp.utils.Utils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

open class AchieveHomeViewModel : ViewModel() {
    var achieveHomeResponse: ArrayList<AchieveHome>

    init {
        val jsonFileString =
            Utils.loadJSONFromAsset(App.activityContext, "achieve_home_response.json")
        achieveHomeResponse =
            Gson().fromJson(jsonFileString, object : TypeToken<ArrayList<AchieveHome>?>() {}.type)
    }
}