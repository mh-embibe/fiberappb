package com.embibe.embibetvapp.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.RelativeLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.embibe.app.utils.Resource
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ActivitySelectChaptersBinding
import com.embibe.embibetvapp.model.createtest.examconfig.ChapterDetails
import com.embibe.embibetvapp.model.createtest.examconfig.SubjectDetails
import com.embibe.embibetvapp.ui.adapter.RvChapterAdapter
import com.embibe.embibetvapp.ui.adapter.RvSelectedSubjectAdapter
import com.embibe.embibetvapp.ui.viewmodel.SelectChaptersViewModel
import com.embibe.embibetvapp.utils.Status
import com.embibe.embibetvapp.utils.Utils

class SelectChaptersActivity : BaseFragmentActivity(),
    RvSelectedSubjectAdapter.OnClickListner, RvChapterAdapter.OnClickListner, View.OnClickListener {
    private lateinit var binding: ActivitySelectChaptersBinding
    private var examConfigDetails: ArrayList<ChapterDetails>? = ArrayList()
    private var subjectPosition: Int? = 0
    private lateinit var selectChaptersViewModel: SelectChaptersViewModel
    private lateinit var chapterAdapter: RvChapterAdapter
    private lateinit var subjectAdapter: RvSelectedSubjectAdapter
    private var chapters: MutableList<ChapterDetails> = mutableListOf<ChapterDetails>()
    private var selectedSubjectDetails: ArrayList<SubjectDetails>? = ArrayList()
    private var subjectCode: String? = null

    companion object {
        var selectChaptersActivity: Activity? = null
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_chapters)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        selectChaptersActivity = this
        if (intent != null) {
            initViews()
        }
    }


    private fun initViews() {
        selectChaptersViewModel =
            ViewModelProviders.of(this).get(SelectChaptersViewModel::class.java)

        selectedSubjectDetails =
            intent?.getParcelableArrayListExtra<SubjectDetails>("SubjectDetails")!!
        examConfigDetails =
            intent?.getParcelableArrayListExtra<ChapterDetails>(AppConstants.EXAM_CONFIG_DETAILS)!!

        binding.btnNext.setOnClickListener(this)
        binding.btnBack.setOnClickListener(this)

        setSubjectDetails()
        setChapterDetails()

        selectChaptersViewModel.getSubjectDetails().observe(
            this, androidx.lifecycle.Observer { selectedSubjectDetails ->
                subjectAdapter.setData(selectedSubjectDetails!!)

            })
        selectChaptersViewModel.selectedChapterDetails().observe(
            this, androidx.lifecycle.Observer { selectedChapterDetails ->
                if (selectedChapterDetails.isNotEmpty()) {
                    var intent = Intent(this, SelectTestSettingsActivity::class.java)
                    intent.putParcelableArrayListExtra(
                        "SubjectDetails", selectedSubjectDetails

                    )
                    intent.putParcelableArrayListExtra(
                        "ChapterDetails",
                        ArrayList(selectedChapterDetails)
                    )
                    if (checkChapterWithSubject(selectedSubjectDetails!!, selectedChapterDetails))
                        startActivity(intent)
                    else
                        Utils.showToast(this, this.resources.getString(R.string.select_one_chapter))
                } else {
                    Utils.showToast(this, this.resources.getString(R.string.select_one_chapter))
                }

            })
    }

    private fun checkChapterWithSubject(
        selectedSubjectDetails: ArrayList<SubjectDetails>,
        selectedChapterDetails: MutableList<ChapterDetails>?
    ): Boolean {

        for (subjectDetails in selectedSubjectDetails) {
            var check = false
            for (chapterDetails in selectedChapterDetails!!) {
                if (chapterDetails.subjectName == subjectDetails.subjectName) {
                    check = true
                    break
                }
            }
            if (!check)
                return false
        }
        return true
    }

    private fun setSubjectDetails() {
        subjectAdapter = RvSelectedSubjectAdapter(this, this)
        binding.rvSelectedSubjects.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rvSelectedSubjects.adapter = subjectAdapter
        subjectAdapter.setData(selectedSubjectDetails!!)
        binding.rvSelectedSubjects.scrollToPosition(0)
        binding.rvSelectedSubjects.requestFocus()
    }

    private fun setChapterDetails() {
        selectChaptersViewModel.getChapterDetailsFromExamConfig(examConfigDetails)
    }

    private fun setChapterAdapter(resource: Resource<Any>) {

        for (i in selectedSubjectDetails!!.indices) {
            selectedSubjectDetails?.get(i)?.selectedPosition = i == subjectPosition
        }
        subjectAdapter.setData(selectedSubjectDetails!!)

        binding.rvChapters.scrollToPosition(0)
        binding.rvChapters.requestFocus()

        val response: List<ChapterDetails> = resource.data as List<ChapterDetails>
        chapters = response.toMutableList()
        chapterAdapter = RvChapterAdapter(this, this)
        binding.rvChapters.layoutManager =
            GridLayoutManager(this, 3)
        binding.rvChapters.adapter = chapterAdapter
        chapterAdapter.setData(response)
        showTransparentView(binding.rvSelectedSubjects, subjectPosition!!)
        binding.linSelectChapters.visibility = View.VISIBLE
        binding.rvChapters.visibility = View.VISIBLE

    }


    fun getChapterDetailsBasedOnSubjectCode(subjectCode: String?) {

        selectChaptersViewModel.getChapterDetailsBasedOnSubjectCode(subjectCode!!)
        selectChaptersViewModel.getChapterResponseDetails().observe(
            this, androidx.lifecycle.Observer { resource ->
                if (resource != null) {
                    when (resource.status) {
                        Status.LOADING -> {
                            showProgress()
                        }

                        Status.SUCCESS -> {
                            hideProgress()
                            setChapterAdapter(resource)
                        }

                        Status.ERROR -> {
                            hideProgress()

                        }
                    }
                }
            })


    }

    override fun onClickSubjectListner(position: Int, subjectCode: String?) {
        subjectPosition = position
        binding.rvChapters.visibility = View.GONE
        getChapterDetailsBasedOnSubjectCode(subjectCode)
    }


    override fun unselectedChapter(adapterPosition: Int, subjectCode: String?) {
        requestFocus(binding.rvChapters, adapterPosition)
        this.subjectCode = subjectCode

        if (adapterPosition == 0) {
            for (i in chapters) {
                i.selectedState = false
            }
        } else {
            chapters.get(adapterPosition).selectedState = false
            if (chapters.get(0).selectedState!!) {
                chapters.get(0).selectedState = false
            }
        }
        chapterAdapter.setData(chapters)
        selectChaptersViewModel.updateSelectedChaptersCount(subjectCode, selectedSubjectDetails)


    }

    override fun selectedChapter(
        adapterPosition: Int,
        subjectCode: String?,
        item: ChapterDetails
    ) {
        requestFocus(binding.rvChapters, adapterPosition)
        if (adapterPosition == 0) {
            for (i in chapters) {
                i.selectedState = true
            }
        } else {
            chapters.get(adapterPosition).selectedState = true
            if (chapters.get(0).selectedState == false) {
                if (chapters.filter { it.selectedState == true }.size + 1 == chapters.size) {
                    chapters.get(0).selectedState = true
                }
            }

        }
        chapterAdapter.setData(chapters)
        selectChaptersViewModel.updateSelectedChaptersCount(subjectCode, selectedSubjectDetails)

    }

    private fun requestFocus(
        rvChapters: RecyclerView,
        adapterPosition: Int
    ) {
        Handler().postDelayed(Runnable {
            rvChapters.findViewHolderForAdapterPosition(adapterPosition)?.itemView?.findViewById<RelativeLayout>(
                R.id.rl_main
            )?.requestFocus()

        }, 10)

    }

    private fun showTransparentView(rvSelectedSubjectAdapter: RecyclerView, position: Int) {
        Handler().postDelayed(Runnable {
            var view =
                rvSelectedSubjectAdapter.findViewHolderForAdapterPosition(position)?.itemView?.findViewById<RelativeLayout>(
                    R.id.fl_transparent
                )
            view?.visibility = View.VISIBLE
            view?.setBackgroundResource(R.drawable.bg_transparent_select_chapters)
        }, 10)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnNext -> {
                selectChaptersViewModel.getSelectedChapterDetails()
            }
            R.id.btnBack -> {
                finish()
            }
        }
    }


}