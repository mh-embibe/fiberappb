package com.embibe.embibetvapp.ui.interfaces

import android.view.View

interface OnItemStateListener {
    fun onItemClick(view: View?, position: Int)
}