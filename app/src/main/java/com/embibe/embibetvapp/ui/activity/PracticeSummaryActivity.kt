package com.embibe.embibetvapp.ui.activity

import android.Manifest
import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.TypedValue
import android.view.KeyEvent
import android.view.View
import android.webkit.*
import android.widget.LinearLayout
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.lottie.LottieDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.application.App.Companion.context
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.FORMAT_ID
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_FORMAT_NAME
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_NAME
import com.embibe.embibetvapp.constant.AppConstants.LM_NAME
import com.embibe.embibetvapp.constant.AppConstants.PRACTICE_MODE
import com.embibe.embibetvapp.constant.AppConstants.PracticeModes.Normal
import com.embibe.embibetvapp.constant.AppConstants.TOPIC_LEARN_PATH
import com.embibe.embibetvapp.databinding.ActivityPracticeDetailBinding
import com.embibe.embibetvapp.model.content.ContentStatusResponse
import com.embibe.embibetvapp.model.likeAndBookmark.BookmarkedQuestion
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkResponse
import com.embibe.embibetvapp.model.practise.conceptSequence.ConceptSeqRes
import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.embibe.embibetvapp.model.response.attemptquality.AttemptQualityResponse
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.ui.adapter.PracticeDetailMenuAdapter
import com.embibe.embibetvapp.ui.fragment.practice.FragmentPracticeDetail
import com.embibe.embibetvapp.ui.interfaces.*
import com.embibe.embibetvapp.ui.viewmodel.PractiseViewModel
import com.embibe.embibetvapp.ui.viewmodel.SearchViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo
import com.github.penfeizhou.animation.apng.APNGDrawable
import com.github.penfeizhou.animation.loader.ResourceStreamLoader
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.InputStream
import java.util.*

class PracticeSummaryActivity : BaseFragmentActivity(), DPadKeysListener,
    SearchDetailToTvKeysListener,
    TvKeysToSearchRes,
    SearchResultsListener {

    private var isPrimaryWebViewLoaded: Boolean = false
    private var isFullWebViewLoaded: Boolean = false
    private var mKGdata: String? = null
    private var isJarEnabled: Boolean = false
    private var practiceDetailMenuType: String? = null
    private lateinit var booksAvailable: ArrayList<Content>
    private lateinit var binding: ActivityPracticeDetailBinding
    private lateinit var practiseViewModel: PractiseViewModel
    private lateinit var data: Content
    private lateinit var topicsData: List<Content>
    private lateinit var recommendationLearning: List<Results>
    private var isBookMarked: Boolean = false
    private var isLiked: Boolean = false
    private var bookmarkRetryCount: Int = 0
    private var likeRetryCount: Int = 0
    private var practiceDetails = ArrayList<String>()
    private lateinit var searchViewModel: SearchViewModel
    private var TAG = this.javaClass.simpleName
    private var hostToNavigationListener: NavigationMenuCallback? = null
    private var searchQuery: String = ""
    private var searchResultSize: Int = 0
    private var speechRecognizer: SpeechRecognizer? = null
    private var speechIntent: Intent? = null
    private var countDownTimer: CountDownTimer? = null
    private val speechTimerDuration = 10000L // 10 secs
    private var elapsedSpeechTimer = 0L
    private var mLastKeyDownTime: Long = 0
    private var isSubSectionActive = false
    private lateinit var fragmentPracticeDetail: FragmentPracticeDetail

    val bundle = Bundle()
    var currentDetailFragment = "analysis"
    private lateinit var listener: SharedPreferences.OnSharedPreferenceChangeListener
    private var pref = PreferenceHelper()
    private lateinit var testsForChapter: ArrayList<Content>
    private var preview_url =
        "https://visualintelligence.blob.core.windows.net/video-summary/prod/4397012/summary.webp"
    private var thumb =
        "https://content-grail-production.s3.amazonaws.com/practice-temp-tiles/1p45wyBVwdAeFP29gv57nU28ymT793kC1.webp"
    private var thumb1 =
        "https://cg-production.s3.amazonaws.com/images/95af3bf6-4d80-4781-924c-591fbd7b7077.webp"

    private var gif_preview = "https://media.giphy.com/media/xT5LMvV7tQfpptiuzK/giphy.gif"
    private var isFisrt = true
    var wvFullViewProgressVisibility: Boolean = false
    var cardAttemptScoreVisibility: Boolean = false
    private var jarType: String = ""
    private lateinit var attemptQualityRes: AttemptQualityResponse
    private var searchInput: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SegmentUtils.trackPracticeSummaryScreenLoadStart()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_practice_detail)
        practiseViewModel = ViewModelProviders.of(this).get(PractiseViewModel::class.java)
        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)

        getDetail()
        setInitialFocusToPractice()
        setFocusListeners()
        setClickListeners()
        listenForDataPrefsUpdate()

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this)
        setupSpeechRecognitionListener()
        speechIntent = getRecognizerIntent()

        setSearchTextChangeListener()
        micSearchButtonListeners()
        permissionsCheck()
        bgTransformation()
        initViewSincerity()
        initViewAttemptQuality("")
        setJarFocusListener()
        setClickListener()
        setKeyListener()
        setPrimaryWebView(binding.wvPrimaryProgress)
        getConceptSequenceCoverage()
        getAttemptQualityData()
        setKeyboardFocusListener()
    }

    private fun setPrimaryWebView(webView: WebView) {
        val base = "file:///android_asset/"
        val indexPath = "KnowledgeGraph/index.html"
        val template = "file:///android_asset/$indexPath"
        webView.webChromeClient = WebChromeClient()
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                isPrimaryWebViewLoaded = true
                Log.w("WEBVIEW", "Web view ready")
                setDataToKGwidget(webView)
            }

            override fun onReceivedError(
                view: WebView,
                request: WebResourceRequest,
                error: WebResourceError
            ) {
                //isLoaded = false
                Log.w("WEBVIEW", "Web view failed to load")
                val errorMessage = "$error"
                makeLog(errorMessage)
                //setProgressDialogVisibility(false)
                super.onReceivedError(view, request, error)
            }
        }
        webView.settings.allowFileAccess = true
        webView.settings.allowFileAccessFromFileURLs = true
        webView.isLongClickable = false
        webView.settings.javaScriptEnabled = true
        webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        webView.settings.domStorageEnabled = true
        webView.setBackgroundColor(Color.argb(0, 0, 0, 0))
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        webView.loadUrl(template)
    }

    private fun setFullProgressWebView(webView: WebView) {
        val base = filesDir.path.toString() + "/assets/"
        val path = "KnowledgeGraph/index.html"
        val template = "file:///android_asset/$path"
        webView.webChromeClient = WebChromeClient()
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                isPrimaryWebViewLoaded = true
                Log.w("WEBVIEW", "Web view ready")
                setDataToKGwidget(webView)
            }
        }
        webView.settings.allowFileAccess = true
        webView.settings.allowFileAccessFromFileURLs = true
        webView.isLongClickable = false
        webView.settings.javaScriptEnabled = true
        webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        webView.settings.domStorageEnabled = true
        webView.setBackgroundColor(Color.argb(0, 0, 0, 0))
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
        /*webView.addJavascriptInterface(
            CommonAndroidAPI(this),
            CommonAndroidAPI.NAME
        )*/
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        webView.loadUrl(template)
    }

    private fun setDataToKGwidget(webView: WebView) {
        if (isPrimaryWebViewLoaded && mKGdata != null) {
            webView.evaluateJavascript("setDataFromApi($mKGdata)", null)
            isPrimaryWebViewLoaded = false
        }
        if (isFullWebViewLoaded && mKGdata != null) {
            webView.evaluateJavascript("setDataFromApi($mKGdata)", null)
            isFullWebViewLoaded = false
        }
    }

    private fun listenForDataPrefsUpdate() {

        listener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
            when (key) {
                AppConstants.BOOK_AVAILABLE_FOR_PRACTICE + "_" + data.id -> {
                    booksAvailable =
                        DataManager.instance.getBookAvailableForPractise()
                    if (booksAvailable.isNotEmpty())
                        practiceDetails.add(AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE)
                    updateUiForPracticeTopics()
                }
                AppConstants.RECOMMENDATION_LEARNING_FOR_PRACTICE + "_" + data.id -> {
                    recommendationLearning =
                        DataManager.instance.getRecommendationLearningForPractice()
                    if (recommendationLearning.isNotEmpty())
                        practiceDetails.add(AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING)
                    updateUiForPracticeTopics()
                }
                AppConstants.PRACTICE_TOPICS + "_" + data.id -> {
                    topicsData = DataManager.instance.getPracticeTopics()
                    if (topicsData.isNotEmpty())
                        practiceDetails.add(AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE)
                    updateUiForPracticeTopics()
                }
                AppConstants.TEST_ON_THIS_CHAPTER + "_" + data.id -> {
                    testsForChapter = DataManager.instance.getTestsForChapter()
                    if (testsForChapter.isNotEmpty())
                        practiceDetails.add(AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER)

                    updateUiForPracticeTopics()
                }


            }
        }
        pref.preferences?.registerOnSharedPreferenceChangeListener(listener)
        updateUiForPracticeTopics()
    }


    private fun updateUiForPracticeTopics() {
        if (practiceDetails.isNotEmpty()) {
            setDetailRecycler(practiceDetails)
            binding.rvPracticeDetails.visibility = View.VISIBLE
            binding.view1.visibility = View.VISIBLE
        } else {
            binding.rvPracticeDetails.visibility = View.GONE
            binding.view1.visibility = View.GONE
        }
    }

    private fun getDetail() {
        try {
            data = getContent(intent)
            binding.content = data
            binding.title.text = data.title
            binding.tvKnowledgeGraphTitle.text =
                getString(R.string.here_is_where_you_stand_on)
            val captureDescText =
                getString(R.string.capture_based_on_your_learning_practicing_and_testing_on)
            binding.cardSincerityScore.tvDescription.text = captureDescText
            binding.cardAttemptScore.tvDescription.text = captureDescText
            setTitleAndDescription()
            getStatus()
            if (data.bg_thumb.isEmpty())
                Utils.setBackgroundImage(
                    Utils.getBackgroundUrl(data),
                    binding.imgDescBackground,
                    binding.imgDescBackgroundBlur
                )
            else
                Utils.setBackgroundImage(
                    data.bg_thumb, binding.imgDescBackground, binding.imgDescBackgroundBlur
                )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        SegmentUtils.trackPracticeSummaryScreenLoadEnd()
        if (::data.isInitialized) {
            getBookmarkStatus()
            getLikeStatus()
        }
        if (speechRecognizer != null)
            setupSpeechRecognitionListener()


    }


    override fun onDestroy() {
        super.onDestroy()
        if (::listener.isInitialized) {
            pref.preferences?.unregisterOnSharedPreferenceChangeListener(listener)
        }
    }

    private fun setTitleAndDescription() {
        if (data.title != null && data.title != "") {
            Log.d(TAG, data.title)
            /// JFR-5281
//            var updateStr = Utils.getTruncatedString(data.title, 25)
            binding.title.text = data.title
        } else binding.title.visibility = View.GONE

        if (data.subject != null && data.subject != "") {
            binding.tvSubjectName.text = data.subject
        } else binding.tvSubjectName.visibility = View.GONE

        if (data.type != "chapter")
            binding.textTime.text = Utils.convertIntoMins(data.length)
        else
            binding.textTime.text = Utils.convertIntoConcepts(data.concept_count)

        binding.embiumsCount.text = getString(R.string.earn) + " ${data.currency}"

        if (data.description != null && data.description != "") {
            binding.textDescription.text = data.description
        } else binding.textDescription.visibility = View.GONE
    }

    private fun getStatus() {
        practiseViewModel.getContentStatus(data.id, data.type,
            object : BaseViewModel.APICallBacks<ContentStatusResponse> {
                override fun onSuccess(model: ContentStatusResponse?) {
                    if (model?.success!! && model.data != null) {
                        val contentStatusModel = model.data
                        if (contentStatusModel?.isWatched!!) {
                            val watchedDuration = contentStatusModel.watchedDuration
                            /*updated practice duration ui*/
                            updateCurrentStatus(watchedDuration, contentStatusModel.contentStatus)
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg")
                }
            })
    }

    private fun updateCurrentStatus(watchedDuration: Long, contentStatus: String) {
    }

    private fun getBookmarkStatus() {
        practiseViewModel.getBookmarkStatus(data.id,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                val data = dataList.filter {
                                    it.contentId == data.id
                                }
                                updateBookMarkUI(data[0].status)
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg")
                }
            })
    }

    private fun getLikeStatus() {
        practiseViewModel.getLikeStatus(data.id,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                if (dataList.isNotEmpty()) {
                                    val data = dataList.filter {
                                        it.contentId.equals(data.id)
                                    }
                                    updateLikUI(data[0].status)
                                }
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg")
                }
            })
    }

    private fun setInitialFocusToPractice() {
        binding.btnPractice.requestFocus()
    }

    private fun setClickListeners() {

        binding.btnPractice.setOnClickListener {
            SegmentUtils.trackPracticeSummaryScreenMainButtonClick("Start Practice")
            val intent = Intent(context, PracticeActivity::class.java)
            intent.putExtra("id", data.id)
            intent.putExtra("title", data.title)
            intent.putExtra("subject", data.subject)
            intent.putExtra(FORMAT_ID, data.learning_map.format_id)
            intent.putExtra(LM_NAME, data.learnmap_id)
            intent.putExtra(PRACTICE_MODE, Normal)
            intent.putExtra(TOPIC_LEARN_PATH, data.learning_map.topicLearnPathName)
            intent.putExtra(LEARN_PATH_NAME, data.learnpath_name)
            intent.putExtra(LEARN_PATH_FORMAT_NAME, data.learnpath_format_name)
            intent.putExtra(AppConstants.CONCEPT_ID, data.learning_map.conceptId)
            intent.putExtra(AppConstants.LENGTH, data.length)
            try {
                val parts = data.learning_map.lpcode.split("--")
                val lmCode = parts[parts.size - 1]
                val lmLevel = Utils.getLevelUsingCode(lmCode)
                val examCode = Utils.getExamLevelCode(parts)
                intent.putExtra("lm_level", lmLevel)
                intent.putExtra("lm_code", lmCode)
                intent.putExtra("exam_code", examCode)
                intent.putExtra(PRACTICE_MODE, Normal)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            startActivity(intent)
        }

        binding.btnBookmark.setOnClickListener {
            isBookMarked = !isBookMarked
            updateBookMarkUI(isBookMarked)
            Utils.isLikeBookmarkUpdatedForQuickLinks = true
            SegmentUtils.trackEventMoreInfoBookmarkClick(isBookMarked, content = data)
            SegmentUtils.trackPracticeSummaryScreenBookmarkButtonClick(isBookMarked)
            timerBookmark.cancel()
            timerBookmark = Timer()
            timerBookmark.schedule(object : TimerTask() {
                override fun run() {
                    updateBookmark()
                }
            }, interval)
        }

        binding.btnLike.setOnClickListener {
            isLiked = !isLiked
            updateLikUI(isLiked)
            SegmentUtils.trackEventMoreInfoLikeClick(isLiked, content = data)
            SegmentUtils.trackPracticeSummaryScreenLikeButtonClick(isLiked)
            timerLike.cancel()
            timerLike = Timer()
            timerLike.schedule(object : TimerTask() {
                override fun run() {
                    updateLike()
                }
            }, interval)
        }
    }

    private fun setFocusListeners() {

        binding.btnPractice.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                isSubSectionActive = false
                setKnowledgeGraphContainer()
                SegmentUtils.trackPracticeSummaryScreenMainButtonFocus("Start Practice")
            }
        }

        binding.btnBookmark.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                isSubSectionActive = false
                setKnowledgeGraphContainer()
                SegmentUtils.trackEventMoreInfoBookmarkFocus(isBookMarked, content = data)
                SegmentUtils.trackPracticeSummaryScreenBookmarkButtonFocus()
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_black, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                    )
                }
            } else {
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_white, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                    )
                }
            }
        }

        binding.btnLike.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                isSubSectionActive = false
                setKnowledgeGraphContainer()
                SegmentUtils.trackEventMoreInfoLikeFocus(isLiked, content = data)
                SegmentUtils.trackPracticeSummaryScreenLikekButtonFocus()
                if (isLiked) {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_filled_black, 0, 0, 0
                    )
                } else {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_stroke_black, 0, 0, 0
                    )
                }
            } else {
                if (isLiked) {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_filled_white, 0, 0, 0
                    )
                } else {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_stroke_white, 0, 0, 0
                    )
                }
            }
        }

        binding.btnCheckProgress.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackPracticeSummaryScreenKnowledgeGraphFocus(AppConstants.KnowledgeGraph)
        }


    }

    private fun updateBookmark() {
        practiseViewModel.bookmark(data.id, data.type, isBookMarked,
            object : BaseViewModel.APICallBacks<CommonApiResponse> {
                override fun onSuccess(model: CommonApiResponse?) {
                    if (model?.success!!) {
                        Utils.isLikeBookmarkUpdatedForQuickLinks = true
                        updateBookMarkUI(isBookMarked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (bookmarkRetryCount < retryAttempt) {
                        bookmarkRetryCount++
                        updateBookmark()
                        return
                    } else {
                        /*reset retry count*/
                        bookmarkRetryCount = 0
                        /*revert back to previous state of ui */
                        updateBookMarkUI(!isBookMarked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_BOOKMARK)
                    }
                }
            })
    }

    private fun updateLike() {
        practiseViewModel.like(data.id, data.type, isLiked,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!!) {
                        Utils.isLikeBookmarkUpdatedForQuickLinks = true
                        updateLikUI(isLiked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (likeRetryCount < retryAttempt) {
                        likeRetryCount++
                        updateLike()
                        return
                    } else {
                        /*reset retry count*/
                        likeRetryCount = 0
                        /*revert back to previous state of ui */
                        updateLikUI(!isLiked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_LIKE)
                    }
                }
            })
    }

    private fun updateBookMarkUI(bookmarked: Boolean) {
        isBookMarked = bookmarked
        data.isBookmarked = bookmarked

        if (binding.btnBookmark.hasFocus()) {
            if (bookmarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        } else {
            if (bookmarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        }
    }

    private fun updateLikUI(liked: Boolean) {
        isLiked = liked
        data.isLiked = liked

        if (binding.btnLike.hasFocus()) {
            if (liked) {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_filled_black, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.liked)
            } else {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_stroke_black, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.like)
            }
        } else {
            if (liked) {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_filled_white, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.liked)
            } else {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_stroke_white, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.like)
            }
        }
    }

    private fun bgTransformation() {
        Utils.setBlurBackgroundImage(
            binding.imgDescBackground
        )
    }

    private fun setKnowledgeGraphContainer() {
        binding.constraint1.visibility = View.GONE
        binding.fragmentContainerView.visibility = View.VISIBLE
        binding.fragmentProductDetailView.visibility = View.GONE
    }


    private fun setPracticeFragment(
        data: List<Content>,
        type: String
    ) {
        checkVisiblityBasedonPageDetail()
        val bundle = Bundle()
        bundle.putString(AppConstants.PRACTICE_DETAIL_TYPE, type)
        bundle.putParcelableArrayList(AppConstants.CONTENT, data as ArrayList<Content>)
        currentDetailFragment = type
        fragmentPracticeDetail = FragmentPracticeDetail()
        fragmentPracticeDetail.arguments = bundle
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.fragmentProductDetailView,
                fragmentPracticeDetail,
                AppConstants.TAG_PRACTICE_DETAL
            )
            .commit()
    }


    private fun setDetailRecycler(data: ArrayList<String>) {
        var detailAdapter =
            PracticeDetailMenuAdapter(
                this
            )
        var layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rvPracticeDetails.adapter = detailAdapter
        binding.rvPracticeDetails.layoutManager = layoutManager
        detailAdapter.setData(data)

        detailAdapter.onItemClick = {
            if (it == AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE) {
                isSubSectionActive = true
                SegmentUtils.trackPracticeSummaryScreenAvailableOptionClick(
                    AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE,
                    1,
                    ""
                )
                if (::topicsData.isInitialized) {
                    practiceDetailMenuType = AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE
                    setPracticeFragment(
                        topicsData,
                        AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE
                    )
                    setTextTitle(AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE)
                }
            }
            if (it == AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE) {
                isSubSectionActive = true
                SegmentUtils.trackPracticeSummaryScreenAvailableOptionClick(
                    AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE,
                    3,
                    ""
                )
                if (::booksAvailable.isInitialized) {
                    practiceDetailMenuType = AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE
                    setPracticeFragment(booksAvailable, AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE)
                    setTextTitle(AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE)

                }
            }
            if (it == AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING) {
                isSubSectionActive = true
                SegmentUtils.trackPracticeSummaryScreenAvailableOptionClick(
                    AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING,
                    2,
                    ""
                )
                if (::recommendationLearning.isInitialized) {
                    practiceDetailMenuType = AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING
                    setPracticeFragment(
                        recommendationLearning.get(0).content,
                        AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING
                    )
                    setTextTitle(AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING)

                }
            }
            if (it == AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER) {
                isSubSectionActive = true
                SegmentUtils.trackPracticeSummaryScreenAvailableOptionClick(
                    AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER,
                    4,
                    ""
                )
                if (::testsForChapter.isInitialized) {
                    practiceDetailMenuType = AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER
                    setPracticeFragment(
                        testsForChapter,
                        AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER
                    )
                    setTextTitle(AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER)

                }
            }
        }

        detailAdapter.onItemFocused = {
            if (it == AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE) {
                SegmentUtils.trackPracticeSummaryScreenAvailableOptionFocus(
                    AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE,
                    1,
                    ""
                )
            }
            if (it == AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE) {
                SegmentUtils.trackPracticeSummaryScreenAvailableOptionFocus(
                    AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE,
                    3,
                    ""
                )
            }
            if (it == AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING) {
                SegmentUtils.trackPracticeSummaryScreenAvailableOptionFocus(
                    AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING,
                    2,
                    ""
                )
            }
            if (it == AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER) {
                SegmentUtils.trackPracticeSummaryScreenAvailableOptionFocus(
                    AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER,
                    4,
                    ""
                )
            }
        }
    }

    private fun checkVisiblityBasedonPageDetail() {
        binding.fragmentContainerView.visibility = View.GONE
        binding.fragmentProductDetailView.visibility = View.VISIBLE
        binding.constraint1.visibility = View.VISIBLE
    }

    private fun setKeyboardFocusListener() {
        binding.textSearch.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackPracticeSummaryScreenKeyboardFocus()
        }
    }

    private fun setTextTitle(practiceDetailType: String) {
        binding.textSearch.visibility = View.VISIBLE
        binding.micSearch.visibility = View.VISIBLE
        binding.view2.visibility = View.VISIBLE
        when (practiceDetailType) {
            AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE -> {

                binding.textSearch.setText("")
                binding.tvTitle.text = resources.getString(R.string.learn_practice_from_books)
                binding.textSearch.hint = resources.getString(R.string.search_books)
            }
            AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING -> {
                binding.textSearch.setText("")
                binding.tvTitle.text =
                    resources.getString(R.string.recommended_learning_based_on_progress)
                binding.textSearch.hint = resources.getString(R.string.search)

            }
            AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE -> {

                binding.textSearch.setText("")
                binding.tvTitle.text = resources.getString(R.string.topic_to_practice)
                binding.textSearch.hint = resources.getString(R.string.search_practice)
            }
            AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER -> {
                binding.textSearch.visibility = View.GONE
                binding.micSearch.visibility = View.GONE
                binding.view2.visibility = View.GONE
                binding.textSearch.setText("")
                binding.tvTitle.text = resources.getString(R.string.test_to_chapter_title)
                binding.textSearch.hint = resources.getString(R.string.search_tests)
            }
        }

    }

    private fun setSearchTextChangeListener() {
        binding.textSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                searchInput = "Keyboard"
                if (!s.isNullOrEmpty()) {
                    getSuggestionsAndHitSearch(s.toString())
                    SegmentUtils.trackPracticeSummaryScreenKeyboardClick(
                        s.toString(),
                        s.length.toString(),
                        "D-Keyboard"
                    )
                    SegmentUtils.trackPracticeSummaryScreenSearchLoadStart(searchInput)
                } else {
                    displayData()
                }
                SegmentUtils.trackEventSearchResultLoadStart(
                    searchQuery,
                    searchQuery.length.toString()
                )

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })


    }

    private fun displayData() {
        binding.textNoResult.visibility = View.GONE
        binding.fragmentContainerView.visibility = View.VISIBLE

        if (practiceDetailMenuType == AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE) {
            if (::topicsData.isInitialized) {
                setPracticeFragment(
                    topicsData,
                    practiceDetailMenuType!!
                )
            }
        }
        if (practiceDetailMenuType == AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE) {
            if (::booksAvailable.isInitialized) {
                setPracticeFragment(
                    booksAvailable,
                    practiceDetailMenuType!!
                )

            }
        }
        if (practiceDetailMenuType == AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING) {
            if (::recommendationLearning.isInitialized) {
                setPracticeFragment(
                    recommendationLearning.get(0).content,
                    practiceDetailMenuType!!
                )

            }
        }
        if (practiceDetailMenuType == AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER) {
            if (::testsForChapter.isInitialized) {
                setPracticeFragment(
                    testsForChapter,
                    practiceDetailMenuType!!
                )

            }
        }
    }


    private fun micSearchButtonListeners() {

        binding.micSearch.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.micSearch.setImageResource(R.mipmap.ic_mic_selected)
            } else {
                binding.micSearch.setImageResource(R.mipmap.ic_mic_unselected)
            }
        }

        binding.micSearch.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (elapsedSpeechTimer != 0L) {
                            stopRecognition()
                            countDownTimer?.cancel()
                        }
                        //binding.customInputView.restoreSelection("Mic")
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_DPAD_UP -> {
                        binding.micSearch.postDelayed({
                            binding.micSearch.requestFocus()
                        }, 0)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        startRecognition()
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        focusToSearch(true)
                    }
                    KeyEvent.KEYCODE_VOICE_ASSIST -> {
//                        Toast.makeText(context, "Voice assist pressed", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            false
        }
    }

    override fun startRecognition() {
        if (Utils.hasPermission(this, Manifest.permission.RECORD_AUDIO)) {
            countDownTimer?.cancel()
            speechRecognizer?.startListening(speechIntent)
            if (speechRecognizer != null) setSpeechRecognitionTimer()
        } else {
            Toast.makeText(
                context,
                this.resources.getString(R.string.audio_permission_not),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun setSpeechRecognitionTimer() {
        elapsedSpeechTimer = 0L
        countDownTimer = object : CountDownTimer(speechTimerDuration, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                elapsedSpeechTimer = (speechTimerDuration - millisUntilFinished) / 1000
                Log.i(TAG, "$elapsedSpeechTimer")
            }

            override fun onFinish() {
                stopRecognition()
            }
        }
        countDownTimer?.start()
    }

    private fun setupSpeechRecognitionListener() {

        speechRecognizer?.setRecognitionListener(object : RecognitionListener {
            override fun onReadyForSpeech(params: Bundle?) {
                Log.v(TAG, "onReadyForSpeech")
                startMicSearchAnimation()
            }

            override fun onRmsChanged(rmsdB: Float) {
//                Log.v(TAG, "onRmsChanged $rmsdB")
            }

            override fun onBufferReceived(buffer: ByteArray?) {
//                Log.v(TAG, "onBufferReceived " + buffer?.size)
            }

            override fun onPartialResults(partialResults: Bundle?) {
                val results =
                    partialResults?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                Log.v(
                    TAG, "onPartialResults " + partialResults + " results "
                            + (results?.size ?: results)
                )
            }

            override fun onEvent(eventType: Int, params: Bundle?) {
            }

            override fun onBeginningOfSpeech() {
                Log.v(TAG, "onBeginningOfSpeech")
            }

            override fun onEndOfSpeech() {
                Log.v(TAG, "onEndOfSpeech")
                countDownTimer?.cancel()
            }

            override fun onError(error: Int) {
                when (error) {
                    SpeechRecognizer.ERROR_NETWORK_TIMEOUT -> Log.w(
                        TAG,
                        "recognizer network timeout"
                    )
                    SpeechRecognizer.ERROR_NETWORK -> Log.w(
                        TAG,
                        "recognizer network error"
                    )
                    SpeechRecognizer.ERROR_AUDIO -> {
                        Log.w(
                            TAG,
                            "recognizer audio error"
                        )
                    }
                    SpeechRecognizer.ERROR_SERVER -> Log.w(
                        TAG,
                        "recognizer server error"
                    )
                    SpeechRecognizer.ERROR_CLIENT -> Log.w(
                        TAG,
                        "recognizer client error"
                    )
                    SpeechRecognizer.ERROR_SPEECH_TIMEOUT -> Log.w(
                        TAG,
                        "recognizer speech timeout"
                    )
                    SpeechRecognizer.ERROR_NO_MATCH -> Log.w(
                        TAG,
                        "recognizer no match"
                    )
                    SpeechRecognizer.ERROR_RECOGNIZER_BUSY -> Log.w(
                        TAG,
                        "recognizer busy"
                    )
                    SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS -> Log.w(
                        TAG,
                        "recognizer insufficient permissions"
                    )
                    else -> Log.d(TAG, "recognizer other error")
                }

                countDownTimer?.cancel()
                stopRecognition()
            }

            override fun onResults(results: Bundle?) {
                val resultsList = results?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                Log.v(
                    TAG, "onResults " + resultsList + " results "
                            + (resultsList?.size ?: resultsList)
                )
                if (resultsList!!.isNotEmpty()) {
                    // binding.customInputView.setSearchText("")
                    val searchQuery = resultsList[0].toLowerCase(Locale.ENGLISH)
                    // performSearch(searchQuery)
                    SegmentUtils.trackPracticeSummaryScreenVoiceSearchClick(
                        searchQuery,
                        searchQuery.length.toString()
                    )
                    searchInput = "Voice"
                    SegmentUtils.trackPracticeSummaryScreenSearchLoadStart(searchInput)
                    getSuggestionsAndHitSearch(searchQuery)
                    binding.textSearch.setText(searchQuery)
                    finishMicSearchAnimation()
                    countDownTimer?.cancel()
                }
            }

        })
    }

    private fun stopRecognition() {
        finishMicSearchAnimation()
        speechRecognizer?.cancel()
    }

    fun startMicSearchAnimation() {
        binding.micSearchLav.visibility = View.VISIBLE
        binding.micSearch.setImageResource(0)
        binding.micSearchLav.playAnimation()
    }

    fun finishMicSearchAnimation() {
        binding.micSearchLav.cancelAnimation()
        binding.micSearchLav.visibility = View.INVISIBLE
        binding.micSearch.setImageResource(R.mipmap.ic_mic_selected)
    }


    private fun noSearchResults() {
        /*show the results not found UI here*/
        binding.textNoResult.visibility = View.VISIBLE
        binding.fragmentProductDetailView.visibility = View.GONE
    }

    private fun getSuggestionsAndHitSearch(searchQuery: String) {
        SegmentUtils.trackEventSearch(searchQuery)
        if (::data.isInitialized) {

            if (practiceDetailMenuType.equals(AppConstants.PRACTICE_DETAIL_BOOKMARKED_QUESTIONS)) {
                practiseViewModel.searchResults(searchQuery, data,
                    object : BaseViewModel.APICallBacks<List<BookmarkedQuestion>> {
                        override fun onSuccess(mBookmarkedQuestions: List<BookmarkedQuestion>?) {
                            if (!mBookmarkedQuestions.isNullOrEmpty()) {
                                /* show Bookmarked Question items UI here*/

                            } else {
                                noSearchResults()
                            }
                        }

                        override fun onFailed(code: Int, error: String, msg: String) {
                            makeLog("$code $error $msg")
                        }
                    })
            } else {

                practiseViewModel.searchResults(practiceDetailMenuType!!, searchQuery, data,
                    object : BaseViewModel.APICallBacks<List<Content>> {
                        override fun onSuccess(searchSummaryResults: List<Content>?) {

                            if (searchSummaryResults!!.isNotEmpty()) {
                                if (!isContentSummaryPresent(searchSummaryResults)) {
                                    noSearchResults()
                                } else {
                                    binding.textNoResult.visibility = View.GONE
                                    binding.fragmentContainerView.visibility = View.VISIBLE
                                    setPracticeFragment(
                                        searchSummaryResults,
                                        practiceDetailMenuType!!
                                    )

                                }
                            } else {
                                noSearchResults()
                            }
                        }

                        override fun onFailed(code: Int, error: String, msg: String) {
                            makeLog("$code $error $msg")
                        }
                    })
            }
        }
    }

    private fun isContentPresent(searchResults: List<Results>): Boolean {
        var isContentPresent = false
        for (index in searchResults.indices) {
            if (searchResults[index].content.isNotEmpty()) return true
        }
        return isContentPresent
    }

    private fun isContentSummaryPresent(searchResults: List<Content>): Boolean {
        var isContentPresent = false
        for (index in searchResults.indices) {
            if (searchResults.isNotEmpty()) return true
        }
        return isContentPresent
    }

    override fun forceFocusToKeys(setFocus: Boolean) {
        if (setFocus) {
            CoroutineScope(Dispatchers.Main).launch {
                delay(2)
                binding.micSearch.requestFocus()
            }
        }
    }

    override fun focusToSearch(setFocus: Boolean) {
        /*   if (setFocus && searchResultSize > 0) {
               searchDetailFragment?.setFocusToDetailRv()
           }*/
    }

    override fun searchResultsCount(count: Int) {
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.hostToNavigationListener = callback
    }

    override fun onPause() {
        releaseRecognizer(false)
        super.onPause()
    }

    override fun onStop() {
        countDownTimer?.cancel()
        releaseRecognizer(true)
        super.onStop()
    }


    private fun releaseRecognizer(isDestroyed: Boolean) {
        if (null != speechRecognizer) {
            speechRecognizer?.setRecognitionListener(null)
            if (isDestroyed)
                speechRecognizer?.destroy()
        }
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {

    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
        if (isLeft) {
            when (from) {
                "Keyboard", "SpaceBarButton" -> {
                    hostToNavigationListener?.navMenuToggle(isLeft)
                }
            }
        }
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {
        if (isRight) {
            when (from) {
                "BackSpaceButton" -> {
                    binding.micSearch.postDelayed({
                        binding.micSearch.requestFocus()
                    }, 0)
                }
            }
        }
    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {}

    private fun getRecognizerIntent(): Intent? {
        val recognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        recognizerIntent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "en-US")
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, true)

        return recognizerIntent
    }

    private fun initViewSincerity() {
        val requestOptions = RequestOptions().transform(RoundedCorners(5))
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        Glide.with(context)
            .applyDefaultRequestOptions(requestOptions)
            .load(thumb)
            .placeholder(R.drawable.video_placeholder)
            .into(binding.cardSincerityScore.ivImg)


        binding.cardSincerityScore.layout.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        currentFocus?.clearFocus()
                        binding.btnCheckProgress.requestFocus()
                        return@setOnKeyListener true
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        SegmentUtils.trackPracticeSummaryScreenSincerityScoreClick("IDK")
                    }
                }
            }

            false
        }

        binding.cardSincerityScore.layout.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackPracticeSummaryScreenSincerityScoreFocus("IDK")
                if (isFisrt) {
                    showSincerityScoreFlipAnimation()
                } else {
                    binding.cardSincerityScore.cardView.visibility = View.VISIBLE
                    binding.cardSincerityScore.ivGifView.visibility = View.VISIBLE
                    val controller = Fresco.newDraweeControllerBuilder()
                    controller.autoPlayAnimations = true
                    controller.setUri(preview_url)
                    controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                        override fun onFinalImageSet(
                            id: String?,
                            imageInfo: ImageInfo?,
                            animatable: Animatable?
                        ) {
                            val anim = animatable as AnimatedDrawable2
                            anim.setAnimationListener(object : AnimationListener {
                                override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                                override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                                    binding.cardSincerityScore.ivImg.visibility = View.INVISIBLE
                                }

                                override fun onAnimationFrame(
                                    drawable: AnimatedDrawable2?,
                                    frameNumber: Int
                                ) {

                                }

                                override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                                    binding.cardSincerityScore.ivImg.visibility = View.VISIBLE
                                    binding.cardSincerityScore.ivGifView.visibility = View.INVISIBLE
                                    binding.cardSincerityScore.cardView.visibility = View.INVISIBLE
                                    binding.cardSincerityScore.ivPlay.visibility = View.VISIBLE
                                }

                                override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                            })
                        }
                    }
                    binding.cardSincerityScore.ivGifView.controller = controller.build()
                }
            } else {
                binding.cardSincerityScore.ivImg.visibility = View.VISIBLE
                binding.cardSincerityScore.ivGifView.visibility = View.INVISIBLE
                binding.cardSincerityScore.cardView.visibility = View.INVISIBLE
                binding.cardSincerityScore.ivPlay.visibility = View.VISIBLE
            }
        }
    }


    private fun initViewAttemptQuality(jarType: String) {
        val requestOptions = RequestOptions().transform(RoundedCorners(10))
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        //binding.cardAttemptScore.tvTopic.text = "0% " + jarType
        when (jarType) {
            AppConstants.CORRECT -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_too_fast_correct)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.PERFECT -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_perfect_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.OVERTIME_CORRECT -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_overtime_correct)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.WASTED -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_wasted_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.WRONG -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_incorrect_answer)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.OVERTIME_WRONG -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_overtime_incorrect)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }

            AppConstants.WASTED_IN_FOUR -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_wasted_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.CONCEPTUAL_THINKING -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_conceptual_thinking)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }

            AppConstants.RECALL -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_recall)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }

            AppConstants.ANALYTICAL_THINKING -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_analytical_thinking)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
        }

        binding.cardAttemptScore.layout.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.cardAttemptScore.lvJar.visibility = View.VISIBLE
                binding.cardAttemptScore.ivPlay.visibility = View.INVISIBLE
                binding.cardAttemptScore.ivImg.visibility = View.INVISIBLE
                when (jarType) {
                    AppConstants.CORRECT -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_correct)
                        SegmentUtils.trackPracticeSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.PERFECT -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_perfect_attempts)
                        SegmentUtils.trackPracticeSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.OVERTIME_CORRECT -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_overtime_correct)
                        SegmentUtils.trackPracticeSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.WASTED -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_wasted_attempt)
                        SegmentUtils.trackPracticeSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.WRONG -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_wrong)
                        SegmentUtils.trackPracticeSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.OVERTIME_WRONG -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_overtime_incorrect)
                        SegmentUtils.trackPracticeSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.WASTED_IN_FOUR -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_wasted_attempt)
                        SegmentUtils.trackPracticeSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.RECALL -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.jar_recall)
                        SegmentUtils.trackPracticeSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.ANALYTICAL_THINKING -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.jar_analytical_tinking)
                        SegmentUtils.trackPracticeSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.CONCEPTUAL_THINKING -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.jar_conceptual_thinking)
                        SegmentUtils.trackPracticeSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                }
                binding.cardAttemptScore.lvJar.repeatCount = LottieDrawable.INFINITE
                binding.cardAttemptScore.lvJar.playAnimation()
            } else {
                binding.cardAttemptScore.lvJar.visibility = View.INVISIBLE
                binding.cardAttemptScore.ivPlay.visibility = View.VISIBLE
                binding.cardAttemptScore.ivImg.visibility = View.VISIBLE
            }
        }
    }

    private fun setCardAttemptScoreClickListener(jarType: String) {
        binding.cardAttemptScore.layout.setOnClickListener {
            SegmentUtils.trackPracticeSummaryScreenAttemptBucketDescriptionClick(jarType)
        }
    }

    private fun setClickListener() {
        binding.cardAttemptQuality.layout.setOnClickListener {
            binding.cardAttemptScore.layout.visibility = View.VISIBLE
            cardAttemptScoreVisibility = true
            binding.cardAttemptScore.layout.postDelayed({
                binding.cardAttemptScore.layout.requestFocus()
                binding.cardAttemptQuality.layout.visibility = View.GONE
            }, 10)
        }

        /*binding.cardFour.layout.setOnKeyListener { v, keyCode, event ->
            if (event.action==KeyEvent.ACTION_DOWN){
                when(keyCode){
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        binding.cardThree.layout.visibility = View.INVISIBLE
                        binding.cardFour.layout.visibility = View.VISIBLE
                        binding.cardFour.layout.requestFocus()
                    }
                }
            }
            false
        }*/

    }

    override fun onBackPressed() {
        if (isJarEnabled || wvFullViewProgressVisibility) {
            if (isJarEnabled) {
                restoreJarFocus()
                setAttemptQualityLayoutVisibility(true)
                binding.cardAttemptQuality.layout.visibility = View.VISIBLE
                binding.cardAttemptScore.layout.visibility = View.GONE
                this.jarType = ""
                isJarEnabled = false
            }
            if (wvFullViewProgressVisibility) {
                onwvFullViewProgressBackPress()
                setAttemptQualityLayoutVisibility(true)
                wvFullViewProgressVisibility = false
            }
        } else {
            super.onBackPressed()
        }
    }

    private fun setVisiblewvFullViewProgress() {
        wvFullViewProgressVisibility = true
        binding.cardAttemptQuality.layout.visibility = View.GONE
        binding.cardSincerityScore.layout.visibility = View.GONE
        binding.cardAttemptScore.layout.visibility = View.GONE
        cardAttemptScoreVisibility = false
    }


    private fun onwvFullViewProgressBackPress() {
        binding.cardSincerityScore.layout.visibility = View.VISIBLE
        if (cardAttemptScoreVisibility) {
            binding.cardAttemptScore.layout.visibility = View.VISIBLE
        } else
            binding.cardAttemptQuality.layout.visibility = View.VISIBLE
        val lp = ConstraintLayout.LayoutParams(820, 400)
        lp.startToStart = binding.practiceSummaryCL.id
        lp.topToBottom = binding.tvKnowledgeGraphTitle.id
        lp.topMargin = 6
        binding.primaryWebViewCL.apply {
            layoutParams = lp
        }
        binding.btnCheckProgress.apply {
            text = "Check Progress + Achievement"
        }
        binding.btnCheckProgress.postDelayed({
            binding.btnCheckProgress.requestFocus()
        }, 10)

        SegmentUtils.trackPracticeSummaryCheckProgressBackBtnClick()
    }


    private fun setKeyListener() {
        binding.wvPrimaryProgress.settings.layoutAlgorithm =
            WebSettings.LayoutAlgorithm.SINGLE_COLUMN

        binding.btnCheckProgress.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        return@setOnKeyListener true
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (!wvFullViewProgressVisibility) {
                            binding.btnLike.postDelayed({
                                binding.btnLike.requestFocus()
                            }, 10)
                        }
                        return@setOnKeyListener true
                    }
                    KeyEvent.KEYCODE_BACK -> {
                        TriggerBackCallBack()
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        binding.wvPrimaryProgress.evaluateJavascript(
                            "javascript:moveForward()",
                            null
                        )
                        return@setOnKeyListener true
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        SegmentUtils.trackPracticeSummaryScreenKnowledgeGraphClick(AppConstants.KnowledgeGraph)
                        if (!wvFullViewProgressVisibility) {
                            expandWebView()
                        } else {
                            TriggerBackCallBack()
                            onBackPressed()
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!wvFullViewProgressVisibility) {
                            binding.cardSincerityScore.layout.postDelayed({
                                binding.cardSincerityScore.layout.requestFocus()
                            }, 10)
                        } else {
                            binding.wvPrimaryProgress.evaluateJavascript(
                                "javascript:moveBackward()",
                                null
                            )
                        }
                        return@setOnKeyListener true
                    }
                }
            }

            false
        }
    }

    private fun TriggerBackCallBack() {
        binding.wvPrimaryProgress.evaluateJavascript(
            "javascript:handleOnBack()",
            null
        )
    }

    private fun expandWebView() {
        val lp = binding.primaryWebViewCL.layoutParams as ConstraintLayout.LayoutParams
        lp.height = 450.toDp(this)
        binding.primaryWebViewCL.apply {
            layoutParams = lp
        }
        binding.btnCheckProgress.apply {
            text = "Back"
        }
        setVisiblewvFullViewProgress()
        Handler().postDelayed({
            binding.wvPrimaryProgress.evaluateJavascript("javascript:handleOnViewProgress()", null)
        }, 1000)
        currentFocus?.clearFocus()
        binding.wvPrimaryProgress.isEnabled = true
        binding.btnCheckProgress.requestFocus()
        SegmentUtils.trackPracticeSummaryCheckProgreeBackBtnFocus()
    }

    private fun getConceptSequenceCoverage() {
        practiseViewModel.loadConceptsSequenceCoverage(
            data,
            object : BaseViewModel.APICallBacks<ConceptSeqRes> {
                override fun onSuccess(model: ConceptSeqRes?) {
                    if (model != null) {
                        val data = readJSONFromAsset("concept_sequence_response")
                        mKGdata = Gson().toJson(model)
                        makeLog("ConceptSequenceCoverage res : $mKGdata")
                        /* setPrimaryWebView(binding.wvPrimaryProgress,mKGdata)
                         setFullProgressWebView(binding.wvFullViewProgress,mKGdata)*/
                        setDataToKGwidget(binding.wvPrimaryProgress)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("Error in getConceptSequenceCoverage : $code $msg")
                }

            })
    }

    //************

    private fun setJarFocusListener() {
        binding.cardAttemptQuality.jarCorrectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketFocus(AppConstants.CORRECT, 1)
                setJarClickListener(AppConstants.CORRECT)
            }
        }
        binding.cardAttemptQuality.jarPerfectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketFocus(AppConstants.PERFECT, 2)
                setJarClickListener(AppConstants.PERFECT)
            }
        }
        binding.cardAttemptQuality.jarOvertimeCorrectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketFocus(
                    AppConstants.OVERTIME_CORRECT,
                    3
                )
                setJarClickListener(AppConstants.OVERTIME_CORRECT)
            }
        }
        binding.cardAttemptQuality.jarWastedAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketFocus(AppConstants.WASTED, 4)
                setJarClickListener(AppConstants.WASTED)
            }
        }
        binding.cardAttemptQuality.jarWrongAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketFocus(AppConstants.WRONG, 5)
                setJarClickListener(AppConstants.WRONG)
            }
        }
        binding.cardAttemptQuality.jarOvertimeWrongAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketFocus(
                    AppConstants.OVERTIME_WRONG,
                    6
                )
                setJarClickListener(AppConstants.OVERTIME_WRONG)
            }
        }
        binding.cardAttemptQuality.jarRecall.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketFocus(AppConstants.RECALL, 7)
                setJarClickListener(AppConstants.RECALL)
            }
        }
        binding.cardAttemptQuality.jarAnalyticalThinking.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketFocus(
                    AppConstants.ANALYTICAL_THINKING,
                    8
                )
                setJarClickListener(AppConstants.ANALYTICAL_THINKING)
            }
        }
        binding.cardAttemptQuality.jarConceptualThinking.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketFocus(
                    AppConstants.CONCEPTUAL_THINKING,
                    9
                )
                setJarClickListener(AppConstants.CONCEPTUAL_THINKING)
            }
        }
        binding.cardAttemptQuality.jarWastedAttemptNew.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketFocus(
                    AppConstants.WASTED_IN_FOUR,
                    10
                )
                setJarClickListener(AppConstants.WASTED_IN_FOUR)
            }
        }
    }

    private fun setJarClickListener(jarType: String) {
        when (jarType) {
            AppConstants.CORRECT -> binding.cardAttemptQuality.jarCorrectAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketClick(jarType, 1)
            }
            AppConstants.PERFECT -> binding.cardAttemptQuality.jarPerfectAttempt.setOnClickListener {
                animateJar(jarType)
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketClick(jarType, 2)
                // setAttemptQualityView(jarType)
            }
            AppConstants.OVERTIME_CORRECT -> binding.cardAttemptQuality.jarOvertimeCorrectAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketClick(jarType, 3)
            }
            AppConstants.WASTED -> binding.cardAttemptQuality.jarWastedAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketClick(jarType, 4)
            }
            AppConstants.WRONG -> binding.cardAttemptQuality.jarWrongAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketClick(jarType, 5)
            }
            AppConstants.OVERTIME_WRONG -> binding.cardAttemptQuality.jarOvertimeWrongAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketClick(jarType, 6)
            }
            AppConstants.RECALL -> binding.cardAttemptQuality.jarRecall.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketClick(jarType, 7)
            }
            AppConstants.ANALYTICAL_THINKING -> binding.cardAttemptQuality.jarAnalyticalThinking.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketClick(jarType, 8)
            }
            AppConstants.CONCEPTUAL_THINKING -> binding.cardAttemptQuality.jarConceptualThinking.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketClick(jarType, 9)
            }
            AppConstants.WASTED_IN_FOUR -> binding.cardAttemptQuality.jarWastedAttemptNew.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackPracticeSummaryScreenAttemptBucketClick(jarType, 10)
            }
        }

    }

    private fun animateJar(jarType: String) {
        updateAttemptQualityData(jarType)
        when (jarType) {
            AppConstants.CORRECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                10f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(1)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.CONCEPTUAL_THINKING -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -30f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(7)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.PERFECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -40f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(2)

                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500
                    }

                    override fun onAnimationCancel(animation: Animator) {}
                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.RECALL -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -80f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(8)

                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500
                    }

                    override fun onAnimationCancel(animation: Animator) {}
                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.OVERTIME_CORRECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -120f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(3)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.ANALYTICAL_THINKING -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -160f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(9)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.WASTED_IN_FOUR -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -220f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(10)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.WASTED -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -180f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(4)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.WRONG -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -260f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(5)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.OVERTIME_WRONG -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -320f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(6)

                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500
                    }

                    override fun onAnimationCancel(animation: Animator) {}
                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
        }
    }

    private fun setAttemptQualityView(jarType: String) {
        initViewAttemptQuality(jarType)
        this.jarType = jarType
        binding.cardAttemptScore.layout.visibility = View.VISIBLE
        binding.cardAttemptScore.layout.postDelayed({
            binding.cardAttemptScore.layout.requestFocus()
            binding.cardAttemptQuality.layout.visibility = View.GONE
        }, 10)
    }

    private fun viewAnimationImage(position: Int) {
        setAttemptQualityLayoutVisibility(false)
        isJarEnabled = true
        binding.imgAttemptJarIcon.alpha = 1.0F
        binding.imgAttemptJarIcon.visibility = View.VISIBLE
        when (position) {
            1 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_too_fast_correct)
            }
            2 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_perfect_attempt)

            }
            3 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_correct)

            }
            4 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_wasted_attempt)

            }
            5 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_incorrect_answer)

            }
            6 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_incorrect)

            }
            8 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_recall)
            }
            7 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_conceptual_thinking)
            }
            9 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_analytical_thinking)
            }
            10 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_wasted_attempt)
            }
        }
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        if (position < 5)
            lp.setMargins((position - 1) * 60, 20, 0, 0)
        if (position == 5)
            lp.setMargins((position - 1) * 70, 20, 0, 0)
        if (position == 6)
            lp.setMargins((position - 1) * 80, 20, 0, 0)

        binding.imgAttemptJarIcon.layoutParams = lp
    }

    private fun showSincerityScoreFlipAnimation() {
        isFisrt = false
        var resourceLoader = ResourceStreamLoader(context, R.drawable.flip_anim_apng)
        var apngDrawable = APNGDrawable(resourceLoader)
        binding.cardSincerityScore.ivImg.setImageDrawable(apngDrawable)

        val requestOptions = RequestOptions().transform(RoundedCorners(5))
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        Handler().postDelayed({
            Glide.with(App.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(gif_preview)
                .placeholder(R.drawable.video_placeholder)
                .into(binding.cardSincerityScore.ivImg)

            binding.cardSincerityScore.tvTopic.text = "Marathoner"
        }, 2000)
    }

    private fun readJSONFromAsset(jsonDataFileName: String): String? {
        var json: String? = null
        try {
            val inputStream: InputStream = assets.open("$jsonDataFileName.json")
            json = inputStream.bufferedReader().use { it.readText() }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    private fun setAttemptQualityLayoutVisibility(visibility: Boolean) {
        if (visibility) {
            binding.cardAttemptQuality.title.visibility = View.VISIBLE
            binding.cardAttemptQuality.jarCorrectAttempt.visibility = View.VISIBLE
            binding.cardAttemptQuality.jarOvertimeCorrectAttempt.visibility = View.VISIBLE
            binding.cardAttemptQuality.jarPerfectAttempt.visibility = View.VISIBLE
            binding.cardAttemptQuality.jarWrongAttempt.visibility = View.VISIBLE
            binding.cardAttemptQuality.jarWastedAttempt.visibility = View.VISIBLE
            binding.cardAttemptQuality.jarOvertimeWrongAttempt.visibility = View.VISIBLE
        } else {
            binding.cardAttemptQuality.title.visibility = View.GONE
            binding.cardAttemptQuality.jarCorrectAttempt.visibility = View.GONE
            binding.cardAttemptQuality.jarOvertimeCorrectAttempt.visibility = View.GONE
            binding.cardAttemptQuality.jarPerfectAttempt.visibility = View.GONE
            binding.cardAttemptQuality.jarWrongAttempt.visibility = View.GONE
            binding.cardAttemptQuality.jarWastedAttempt.visibility = View.GONE
            binding.cardAttemptQuality.jarOvertimeWrongAttempt.visibility = View.GONE
        }
    }

    private fun restoreJarFocus() {
        when (jarType) {
            AppConstants.CORRECT -> binding.cardAttemptQuality.jarCorrectAttempt.requestFocus()
            AppConstants.PERFECT -> binding.cardAttemptQuality.jarPerfectAttempt.requestFocus()
            AppConstants.OVERTIME_CORRECT -> binding.cardAttemptQuality.jarOvertimeCorrectAttempt.requestFocus()
            AppConstants.WASTED -> binding.cardAttemptQuality.jarWastedAttempt.requestFocus()
            AppConstants.WRONG -> binding.cardAttemptQuality.jarWrongAttempt.requestFocus()
            AppConstants.OVERTIME_WRONG -> binding.cardAttemptQuality.jarOvertimeWrongAttempt.requestFocus()
            AppConstants.RECALL -> binding.cardAttemptQuality.jarRecall.requestFocus()
            AppConstants.ANALYTICAL_THINKING -> binding.cardAttemptQuality.jarAnalyticalThinking.requestFocus()
            AppConstants.CONCEPTUAL_THINKING -> binding.cardAttemptQuality.jarConceptualThinking.requestFocus()
            AppConstants.WASTED_IN_FOUR -> binding.cardAttemptQuality.jarWastedAttemptNew.requestFocus()
        }
    }

    // Extension method to convert pixels to dp
    fun Int.toDp(context: Context): Int = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), context.resources.displayMetrics
    ).toInt()


    private fun getAttemptQualityData() {
        practiseViewModel.getUserAttemptQualityData(
            data.learning_map.format_id,
            data.learnpath_format_name,
            data.learnpath_name,
            object : BaseViewModel.APICallBacks<AttemptQualityResponse> {
                override fun onSuccess(model: AttemptQualityResponse?) {
                    if (model != null) {
                        attemptQualityRes = model
                        setJarVisibility()
                    }
                    makeLog("")
                }

                override fun onFailed(code: Int, error: String, msg: String) {

                }
            })
    }

    private fun setJarVisibility() {
        if (::attemptQualityRes.isInitialized) {
            if (attemptQualityRes.data.conceptual_thinking != null) {
                binding.cardAttemptQuality.layoutFourJars.visibility = View.VISIBLE
                binding.cardAttemptQuality.layoutSixJars.visibility = View.GONE
            } else {
                binding.cardAttemptQuality.layoutFourJars.visibility = View.GONE
                binding.cardAttemptQuality.layoutSixJars.visibility = View.VISIBLE
            }
        }
    }

    private fun getAttemptPercentage(value: String): Int {
        Log.i("attempt data", value)
        if (value.isNotEmpty() || value.isNotBlank())
            return value.toDouble().toInt()
        return 0
    }

    private fun updateAttemptQualityData(jarType: String) {
        var jarName = jarType
        when (jarName) {
            AppConstants.RECALL -> {
                jarName = "Recall"
            }
            AppConstants.ANALYTICAL_THINKING -> {
                jarName = "Analytical Thinking"
            }
            AppConstants.CONCEPTUAL_THINKING -> {
                jarName = "Conceptual Thinking"
            }
            AppConstants.WRONG -> {
                jarName = "Wrong"
            }
            AppConstants.WASTED_IN_FOUR -> {
                jarName = "Wasted"
            }
        }
        if (::attemptQualityRes.isInitialized) {
            val percentage: Int = when (jarType) {
                AppConstants.CORRECT -> getAttemptPercentage(attemptQualityRes.data.overtime_correct!!.value) + getAttemptPercentage(
                    attemptQualityRes.data.perfect_attempt!!.value
                )
                AppConstants.PERFECT -> getAttemptPercentage(attemptQualityRes.data.perfect_attempt!!.value)
                AppConstants.OVERTIME_CORRECT -> getAttemptPercentage(attemptQualityRes.data.overtime_correct!!.value)
                AppConstants.WASTED -> getAttemptPercentage(attemptQualityRes.data.wasted_attempt!!.value)
                AppConstants.WRONG -> getAttemptPercentage(attemptQualityRes.data.wasted_attempt!!.value) + getAttemptPercentage(
                    attemptQualityRes.data.wasted_attempt!!.value
                )
                AppConstants.OVERTIME_WRONG -> getAttemptPercentage(attemptQualityRes.data.overtime_incorrect!!.value)
                AppConstants.RECALL -> getAttemptPercentage(attemptQualityRes.data.recall!!.value)
                AppConstants.ANALYTICAL_THINKING -> getAttemptPercentage(attemptQualityRes.data.analytical_thinking!!.value)
                AppConstants.CONCEPTUAL_THINKING -> getAttemptPercentage(attemptQualityRes.data.conceptual_thinking!!.value)
                AppConstants.WASTED_IN_FOUR -> getAttemptPercentage(attemptQualityRes.data.wasted_attempt!!.value)
                else -> 0
            }
            binding.cardAttemptScore.tvTopic.text = "$percentage% ${jarName}"
        } else
            binding.cardAttemptScore.tvTopic.text = "0% " + jarName
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {

        if (event.keyCode == KeyEvent.KEYCODE_SEARCH) {
            if (!isSubSectionActive) {
                startActivity(Intent(this, SearchActivity::class.java))
            } else {
                startRecognition()
            }
            return true
        }
        val current = System.currentTimeMillis()
        var res = false
        if (current - mLastKeyDownTime < 300) {
            res = true
        } else {
            res = super.onKeyDown(keyCode, event)
            mLastKeyDownTime = current
        }
        return res
    }


    companion object {
        private const val TAG = "PracticeSummaryActivity"
    }
}