package com.embibe.embibetvapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import androidx.databinding.DataBindingUtil
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.databinding.ActivitySearchBinding
import com.embibe.embibetvapp.ui.fragment.search.SearchFragment2
import com.embibe.embibetvapp.utils.SegmentUtils

class SearchActivity : BaseFragmentActivity() {

    private lateinit var binding: ActivitySearchBinding
    private var mLastKeyDownTime: Long = 0
    private lateinit var searchFragment: SearchFragment2

    companion object {
        const val REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124
        val TAG = SearchActivity::javaClass.name
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        SegmentUtils.trackEventSearchLoadStart()
        permissionsCheck()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchFragment = SearchFragment2()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search)
        supportFragmentManager.beginTransaction().replace(R.id.search_FL, searchFragment)
            .commit()

        SegmentUtils.trackEventSearchLoadStart()
    }

    override fun onResume() {
        super.onResume()
        SegmentUtils.trackEventSearchLoadEnd()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        SegmentUtils.trackEventSearchBackBtnClick("Search page")
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {

        if (event.keyCode == KeyEvent.KEYCODE_SEARCH) {
            searchFragment.startRecognition()
            return true
        }
        val current = System.currentTimeMillis()
        var res = false
        if (current - mLastKeyDownTime < 300) {
            res = true
        } else {
            res = super.onKeyDown(keyCode, event)
            mLastKeyDownTime = current
        }
        return res
    }

}
