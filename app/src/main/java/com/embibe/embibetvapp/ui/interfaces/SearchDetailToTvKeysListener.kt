package com.embibe.embibetvapp.ui.interfaces

interface SearchDetailToTvKeysListener {
    fun forceFocusToKeys(setFocus: Boolean)
}