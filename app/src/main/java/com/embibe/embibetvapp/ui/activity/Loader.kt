package com.embibe.embibetvapp.ui.activity

import android.app.Activity
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.ui.custom.AnimationDrawableCallback
import com.embibe.embibetvapp.ui.progressBar.LoadingManager
import com.embibe.embibetvapp.utils.data.DataManager
import kotlinx.android.synthetic.main.progress_bar_ly.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class Loader : Activity() {

    companion object {
        var instance: Loader? = null
        var progressView: View? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.progress_bar_ly)
        LoadingManager.activity = this
        instance = this
        progressView = cp_view
        LoadingManager.setDPadListener(cp_view)

        val sequentialLoader = (sequential_loader_IV.background as AnimationDrawable)

        sequentialLoader.callback =
            object : AnimationDrawableCallback(sequentialLoader, sequential_loader_IV) {
                override fun onAnimationComplete() {

                }

                override fun onAnimationStart() {

                }

            }
        sequentialLoader.start()
    }

    override fun onPause() {
        super.onPause()
        hideActivity()
    }

    fun hideActivity() {
        try {
            instance!!.finish()
            this.overridePendingTransition(0, 0)
            DataManager.isLoading = false
            Log.w("hideActivity :", " finished")
        } catch (e: Exception) {
            try {
                doAsync {
                    uiThread {
                        instance!!.finish()
                        overridePendingTransition(0, 0)
                        DataManager.isLoading = false
                        Log.w("hideActivity :", " finished")
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        DataManager.isLoading = false
    }
}
