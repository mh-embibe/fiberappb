package com.embibe.embibetvapp.ui.fragment.test

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentHostTestBinding
import com.embibe.embibetvapp.jobScheduler.MyInternetTest
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.model.HeaderBannerData
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.activity.SwitchGoalExamActivity
import com.embibe.embibetvapp.ui.interfaces.*
import com.embibe.embibetvapp.ui.progressBar.LoadingManager
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.MainViewModel
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*

class TestHostFragment : BaseAppFragment(),
    DPadKeysListener, RowsToBannerListener, BannerDataListener,
    BannerImageDataToHostListener, NavigationMenuCallback, LoadingManager,
    MyInternetTest.NetworkSchedulerService.ConnectivityReceiverListener {

    private lateinit var binding: FragmentHostTestBinding
    private lateinit var mainViewModel: MainViewModel

    private var hostToNavigationListener: HostToNavigationListener? = null
    private var classTag = TestHostFragment::class.java.toString()
    private var internetCheckMLD = MutableLiveData<Boolean>()

    private var testBannerFragment = TestBannerFragment()
    private var testRowsFragment = TestRowsFragment()
    private var testBannerImgFlipper = TestBannerImgFlipper()
    private lateinit var homeViewModel: HomeViewModel
    private var lastFocusedContent = "Banner"
    private var contentAvailable = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_host_test, container, false)
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        hostToNavigationListener?.showNavigation(false)
        getData()
        MyInternetTest.scheduleJob()
        setNetworkObserver()
        registerChangeExamButtonListeners()

    }

    private fun registerChangeExamButtonListeners() {
        binding.changeGradeExamButton.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        startActivity(Intent(context, SwitchGoalExamActivity::class.java))
                    }
                }
            }
            false
        }
    }

    private fun setFragment(fragment: Fragment, layoutResId: Int) {
        childFragmentManager.beginTransaction().replace(layoutResId, fragment).commit()
    }

    private fun setNetworkObserver() {
        internetCheckMLD.observe(viewLifecycleOwner, Observer { value ->
            if (value)
                isInternetAvailable()
        })
    }

    private fun isNetworkConnected(): Boolean {
        val cm =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null && cm.activeNetworkInfo.isConnected
    }

    private fun isInternetAvailable(): Boolean {
        return try {
            val command = "ping -c 1 google.com"
            if (Runtime.getRuntime().exec(command).waitFor() == 0) {
                internetCheckMLD.postValue(false)
            }
            return Runtime.getRuntime().exec(command).waitFor() == 0

        } catch (e: Exception) {
            Log.e(classTag, " isInternetAvailable ${e.message}")
            internetCheckMLD.postValue(true)
            false
        }
    }

    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        when (childFragment) {
            is TestBannerFragment -> {
                childFragment.setDPadKeysListener(this)
                childFragment.setNavigationMenuListener(this)
            }
            is TestRowsFragment -> {
                childFragment.setNavigationMenuListener(this)
                childFragment.setRowsToBannerListener(this)
                childFragment.setDPadKeysListener(this)

            }
            is TestBannerImgFlipper -> {
                childFragment.bannerImageDataListener(this)
            }
        }
    }

    fun setHostToNavigationListener(callback: HostToNavigationListener) {
        this.hostToNavigationListener = callback
    }

    // DPadKeysListener functions
    override fun isKeyPadDown(isDown: Boolean, from: String) {
        if (isDown) {
            when (from) {
                "Rows" -> {
                    slideBannerUp()
                    testBannerImgFlipper.stopImgFlipping()
                }
                "Banner" -> {
                    slideBannerUp()
                    testRowsFragment.setFirstRowFocus()
                    testBannerImgFlipper.stopImgFlipping()
                }
            }
        }
    }

    private fun slideBannerUp() {
        testRowsFragment.isBannerVisible(false)
        val bannerBGTestOA = ObjectAnimator.ofFloat(
            binding.flBannerBgTest,
            View.TRANSLATION_Y, 0f, Values.BANNER_TRANSLATION_Y
        )
        val bannerTestOA = ObjectAnimator.ofFloat(
            binding.flBannerTest,
            View.TRANSLATION_Y, 0f, Values.BANNER_TRANSLATION_Y
        )
        val rowsTestOA = ObjectAnimator.ofFloat(
            binding.flRowTest,
            View.TRANSLATION_Y, 0f, Values.ROWS_TRANSLATION_Y
        )
        val animatorSet = AnimatorSet()
        animatorSet.apply {
            playTogether(bannerBGTestOA, bannerTestOA, rowsTestOA)
            duration = 500
            doOnEnd {
                binding.flBannerTest.visibility = View.INVISIBLE
                binding.flBannerBgTest.visibility = View.INVISIBLE
            }
            start()
        }

        binding.flRowTest.setPadding(0, 50, 0, 0)

        lastFocusedContent = "Rows"
    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {
    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {
        if (isEnter) {
            when (from) {
                "Rows" -> {
                    if (cardType.toLowerCase(Locale.getDefault()) == "practice")
                        hostToNavigationListener?.switchToPractice()
                    else if (cardType.toLowerCase(Locale.getDefault()) == "learn") {
                        hostToNavigationListener?.switchToLearn()
                    }
                    //currently commented out because of undefined functionality requirement
//                    hostToNavigationListener?.switchToLearn()
                }
            }
        }
    }

    // RowsToBannerListener function
    override fun rowsToBanner() {
        testRowsFragment.isBannerVisible(true)
        binding.flBannerTest.requestFocus()
        testBannerFragment.setLastFocusedBtn()
        testBannerImgFlipper.startImgFlipping()

        val bannerBGTestOA = ObjectAnimator.ofFloat(
            binding.flBannerBgTest,
            View.TRANSLATION_Y, Values.BANNER_TRANSLATION_Y, 0f
        )
        val bannerTestOA = ObjectAnimator.ofFloat(
            binding.flBannerTest,
            View.TRANSLATION_Y, Values.BANNER_TRANSLATION_Y, 0f
        )
        val rowsTestOA = ObjectAnimator.ofFloat(
            binding.flRowTest,
            View.TRANSLATION_Y, Values.ROWS_TRANSLATION_Y, 0f
        )
        val animatorSet = AnimatorSet()
        animatorSet.apply {
            playTogether(bannerBGTestOA, bannerTestOA, rowsTestOA)
            duration = 500
            doOnStart {
                binding.flBannerTest.visibility = View.VISIBLE
                binding.flBannerBgTest.visibility = View.VISIBLE
            }
            start()
        }

        binding.flRowTest.setPadding(0, 0, 0, 0)
        lastFocusedContent = "Banner"
    }

    override fun notifyBanner(notifyMessage: String) {
        when (notifyMessage) {
            "Synced" -> {
                //avoiding flickering due to sync event coming after focus has shifted to rows
                if (lastFocusedContent.equals("Banner", true)) {
                    Log.i(classTag, "flBannerTest VISIBLE")
                    binding.flBannerTest.postDelayed({
                        binding.flBannerTest.requestFocus()
                    }, 10)
                }
            }
        }
    }

    override fun notifyHost(notifyMessage: String) {
        when (notifyMessage) {
            AppConstants.NO_CONTENT -> {
                contentAvailable = false
                toggleNoContentView(View.VISIBLE)
            }
            AppConstants.CONTENT_AVAILABLE -> {
                contentAvailable = true
                toggleNoContentView(View.GONE)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun toggleNoContentView(visibility: Int) {
        binding.noContentTv.visibility = visibility
        binding.changeGradeExamButton.visibility = visibility
        binding.flBannerBgTest.visibility = View.GONE - visibility
        binding.flBannerTest.visibility = View.GONE - visibility
        binding.flRowTest.visibility = View.GONE - visibility

        if (visibility == View.VISIBLE) {
            binding.noContentTv.text = getString(R.string.text_goal_not_supported) + " " +
                    DataManager.instance.getExamNameByCode(
                        UserData.getGoalCode(),
                        UserData.getExamCode()
                    )
            binding.changeGradeExamButton.postDelayed({
                binding.changeGradeExamButton.requestFocus()
            }, 100)
        } else {
            binding.changeGradeExamButton.postDelayed({
                binding.changeGradeExamButton.clearFocus()
            }, 100)
        }
    }

    // BannerDataListener function
    override fun onHeaderItemChange(data: HeaderBannerData) {
    }

    // BannerImageDataToHostListener function
    override fun setBannerImageData(data: BannerData, position: Int) {
        testBannerFragment.setData(data, position)
    }

    // NavigationMenuCallback function
    override fun navMenuToggle(toShow: Boolean) {
        hostToNavigationListener?.showNavigation(toShow)
    }

    fun resumeBannerSliding() {
        testBannerImgFlipper.startImgFlipping()
    }

    fun pauseBannerSliding() {
        testBannerImgFlipper.stopImgFlipping()
    }

    // ConnectivityReceiverListener function
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        Log.i(classTag, "Network: $isConnected")
        //if (!isConnected)
        /* Toast.makeText(App.context, "Please check your internet connection", Toast.LENGTH_SHORT)
             .show()*/
    }

    override fun onResume() {
        super.onResume()
        MyInternetTest.scheduleJob()
        App.setConnectivityListener(this)
        Log.e(classTag, "onResume ")
    }

    override fun onDetach() {
        super.onDetach()
        hostToNavigationListener = null
    }

    fun restoreLastSelection(fromNavigation: Boolean) {
        when (contentAvailable) {
            false -> {
                binding.changeGradeExamButton.postDelayed({
                    binding.changeGradeExamButton.requestFocus()
                }, 100)
            }
            true -> {
                if (lastFocusedContent.equals("Banner", true)) {
                    testBannerFragment.setLastFocusedBtn()
                } else {
                    binding.flRowTest.requestFocus()
                    testRowsFragment.restoreLastSelection(fromNavigation)
                }
            }
        }
    }

    private fun getData() {
        doAsync {
            val testData = homeViewModel.fetchSectionByPageName(AppConstants.TEST)
            uiThread {
                if (testData.isNotEmpty()) {/*has data*/
                    DataManager.instance.setHomeTest(arrayListOf(), null)
                    updateAPIresult()
                } else {
                    /*No data*/
                    getApiCall(isAsync = false)
                }
            }
        }


    }

    private fun updateAPIresult() {
        setFragment(testBannerImgFlipper, binding.flBannerBgTest.id)
        setFragment(testBannerFragment, binding.flBannerTest.id)
        setFragment(testRowsFragment, binding.flRowTest.id)

    }

    fun getApiCall(isAsync: Boolean) {

        if (!isAsync) {
            showProgress()
        }
        homeViewModel.homeTest(object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
            override fun onSuccess(model: List<ResultsEntity>?) {
                if (model != null) {
                    if (!isAsync) {
                        DataManager.instance.setHomeTest(model as ArrayList<ResultsEntity>, object :
                            BaseViewModel.DataCallback<ArrayList<ResultsEntity>> {
                            override fun onSuccess(model: ArrayList<ResultsEntity>?) {
                                if (model != null) {
                                    hideProgress()
                                    updateAPIresult()
                                }
                            }

                        })

                    }
                }

            }

            override fun onFailed(code: Int, error: String, msg: String) {
                if (!isAsync) {
                    hideProgress()
                    if (Utils.isApiFailed(code)) {
                        Utils.showError(
                            context,
                            code,
                            object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: String) {
                                    getApiCall(isAsync)
                                }

                                override fun onDismiss() {

                                }
                            })
                    }
                }
            }
        })
    }

    object Values {
        const val BANNER_TRANSLATION_Y = -550f
        const val ROWS_TRANSLATION_Y = -650f
    }

}