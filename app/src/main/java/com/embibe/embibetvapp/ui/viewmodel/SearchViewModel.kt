package com.embibe.embibetvapp.ui.viewmodel

import android.annotation.SuppressLint
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.model.home.HomeModel
import com.embibe.embibetvapp.model.search.SearchResponse
import com.embibe.embibetvapp.network.repo.HomeRepository
import com.embibe.embibetvapp.network.retrofit.RetrofitClient
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

open class SearchViewModel : BaseViewModel() {

    private val repoHome = HomeRepository(RetrofitClient.homeApi!!)

    fun getSearchSuggestions(
        query: String,
        size: String,
        userId: String,
        grade: String,
        goal: String, examName: String,
        callback: APICallBacks<SearchResponse>
    ) {
        safeApi(
            repoHome.fetchSearchSuggestions(query, size, userId, grade, goal, examName),
            callback
        )
    }

    @SuppressLint("CheckResult")
    fun combineSearchResults(
        query: String,
        size: String,
        userId: String,
        grade: String,
        goal: String,
        examName: String,
        callback: APIMergeCallBack
    ) {
        Single.zip(
            repoHome.fetchSearchSuggestions(query, size, userId, grade, goal, examName),
            repoHome.fetchSearchResults(query, size, userId, grade, goal, examName),
            BiFunction<Response<SearchResponse>, Response<SearchResponse>, kotlin.Any> { mSuggestions, mResults ->
                // here we get both the results at a time.
                return@BiFunction filterBoth(mSuggestions, mResults)
            })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback.onSuccessCallBack(it)
            }, {
                callback.onErrorCallBack(it)
            })
    }

    /*combine both api results*/
    private fun filterBoth(
        mSuggestions: Response<SearchResponse>,
        mResults: Response<SearchResponse>
    ): Any {
        val mHomeModel = HomeModel()
        if (mSuggestions.code() == 200) {
            mHomeModel.quickLinks = (mSuggestions.body() as SearchResponse).data?.quicklinks
        }
        if (mResults.code() == 200) {
            mHomeModel.searchResults = (mResults.body() as SearchResponse).results
        }
        return mHomeModel

    }

}
