package com.embibe.embibetvapp.ui.fragment.achieve

import android.animation.ObjectAnimator
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentPotentialBinding
import com.embibe.embibetvapp.model.achieve.achieveFeedback.AchieveFeedbackRes
import com.embibe.embibetvapp.model.achieve.paj.AnswerData
import com.embibe.embibetvapp.model.achieve.paj.AvailableTimePerDay
import com.embibe.embibetvapp.model.achieve.paj.PAJCreateReq
import com.embibe.embibetvapp.model.achieve.updateTruePotential.TestDate
import com.embibe.embibetvapp.model.potential.Potential
import com.embibe.embibetvapp.ui.activity.TestFeedbackActivity
import com.embibe.embibetvapp.ui.adapter.RvAdapterGradeTarget
import com.embibe.embibetvapp.ui.adapter.RvAdapterTimePotential
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener
import com.embibe.embibetvapp.ui.viewmodel.AchieveViewModel
import com.embibe.embibetvapp.utils.FontHelper
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap

class PotentialFragment : BaseAppFragment(), ErrorScreensBtnClickListener {

    private lateinit var binding: FragmentPotentialBinding
    private var spendTime: String? = null
    private var grade: String? = null
    private var type: String? = null
    private var data: List<Potential>? = null
    private lateinit var achieveViewModel: AchieveViewModel

    private lateinit var testName: String
    private lateinit var testCode: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_potential, container, false)
        data = DataManager.instance.getKGPotentialQuestionList()
        achieveViewModel = ViewModelProviders.of(this).get(AchieveViewModel::class.java)
        setDisplayName()

        setAdapterOnRv()
        micSearchButtonListeners()
        whenYourExamListener()
        setShowImprovementListener()
        binding.lvShowMyImprovementPlan.setAnimation(R.raw.discover_where_you_stand_no_text)
        return binding.root
    }

    private fun setDisplayName() {
        binding.timeSpendHeader.text = data?.get(0)?.displayName
        binding.targetHeader.text = data?.get(1)?.displayName
        binding.daysExamHeader.text = data?.get(2)?.displayName
    }

    private fun setAdapterOnRv() {

        var timePotentialAdapter = RvAdapterTimePotential()
        binding.rvTimeSpend.adapter = timePotentialAdapter
        timePotentialAdapter.setData(data?.get(0)!!.options)

        var gradeTargetAdapter = RvAdapterGradeTarget()
        binding.rvTarget.adapter = gradeTargetAdapter

        timePotentialAdapter.onItemClick = { spendTime ->
            this.spendTime = spendTime
        }

        gradeTargetAdapter.onItemClick = { grade ->
            this.grade = grade
        }
    }


    private fun micSearchButtonListeners() {

        binding.micSearch.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.micSearch.setImageResource(R.mipmap.ic_mic_selected)
            } else {
                binding.micSearch.setImageResource(R.mipmap.ic_mic_unselected)
            }
        }

        binding.micSearch.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {

                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        binding.micSearch.postDelayed({
                            binding.micSearch.clearFocus()
                        }, 10)
                        binding.lvShowMyImprovementPlan.postDelayed({
                            binding.lvShowMyImprovementPlan.requestFocus()
                        }, 10)
                        toggleShadowVisibilityForDiscoverArrow(View.VISIBLE)
                    }

                }
            }
            false
        }
    }

    private fun whenYourExamListener() {
        binding.clDropDown.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                FontHelper().setFontFace(FontHelper.FontType.GILROY_SEMI_BOLD, binding.tvDateType)
            } else {
                FontHelper().setFontFace(FontHelper.FontType.GILROY_MEDIUM, binding.tvDateType)
            }
        }

        binding.clDropDown.setOnClickListener {
            activity?.supportFragmentManager?.let { it1 ->
                Utils.showAlertDialog(
                    AppConstants.SELECT_DAY_MONTH_YEAR,
                    it1, this
                )
            }
        }
    }

    private fun setShowImprovementListener() {
        binding.lvShowMyImprovementPlan.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.ivArrow.visibility = View.GONE
                toggleShadowVisibilityForDiscoverArrow(View.VISIBLE)
            } else {
                binding.ivArrow.visibility = View.VISIBLE
                toggleShadowVisibilityForDiscoverArrow(View.GONE)
            }
        }

        binding.lvShowMyImprovementPlan.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        /*startActivity(Intent(v.context, DiagnosticTestFeedbackActivity::class.java))
                        toggleShadowVisibilityForDiscoverArrow(View.GONE)*/
                        goToFeedBackActivity()
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT, KeyEvent.KEYCODE_DPAD_DOWN,
                    KeyEvent.KEYCODE_DPAD_UP -> {

                        toggleShadowVisibilityForDiscoverArrow(View.GONE)
                    }
                }
            }
            false
        }
    }

    private fun toggleShadowVisibilityForDiscoverArrow(visibility: Int) {
        if (visibility == View.VISIBLE)
            ObjectAnimator.ofFloat(binding.lvShowMyImprovementPlan, View.ALPHA, 0.7f, 1f).start()
        else
            ObjectAnimator.ofFloat(binding.lvShowMyImprovementPlan, View.ALPHA, 1f, 0.7f).start()

        binding.ivDiscoverShadow.visibility = visibility
    }

    override fun screenUpdate(type: String) {
        this.type = type
        binding.tvDateType.text = type
    }

    private fun goToFeedBackActivity() {
        var dateCount = binding.etDaysCount.text.toString().trim()
        when {
            spendTime == null -> {
                Utils.showToast(requireContext(), "Please select time spend")
            }
            grade == null -> {
                Utils.showToast(requireContext(), "Please select grade")
            }
            dateCount.isEmpty() -> {
                Utils.showToast(requireContext(), "Please enter valid $type count")
            }
            else -> {
                toggleShadowVisibilityForDiscoverArrow(View.GONE)
                val pajCreateReq = PAJCreateReq()
                pajCreateReq.bundleCode = requireArguments().getString("testCode").toString()
                pajCreateReq.type = "K12"
                val answerData = AnswerData()
                val testDate = TestDate()
                val availableTimePerDay = AvailableTimePerDay()
                availableTimePerDay.value = spendTime?.replace(" hours", "")
                testDate.value = dateCount
                pajCreateReq.answerData = answerData
                pajCreateReq.answerData!!.testDate = testDate
                pajCreateReq.answerData!!.availableTimePerDay = availableTimePerDay
                createPAJ(pajCreateReq)

            }
        }
    }

    /*Create PAJ api*/
    fun createPAJ(requestModel: PAJCreateReq) {
        showProgress()
        achieveViewModel.createPAJ(requestModel, object :
            BaseViewModel.APICallBacks<AchieveFeedbackRes> {

            override fun onSuccess(model: AchieveFeedbackRes?) {
                hideProgress()
                if (model != null) {
                    updateAchieveUserAsync(isAchieve = true)
                    DataManager.instance.setTestPAJ(Gson().toJson(model))
                    val intent = Intent(context, TestFeedbackActivity::class.java)
                    intent.putExtra(AppConstants.IS_DIAGNOSTIC,DataManager.instance.isDiagnosticTest)
                    startActivity(intent)
                    requireActivity().finish()
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            createPAJ(requestModel)
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }
        })
    }

    /*updateAchieveUser api*/
    fun updateAchieveUserAsync(isAchieve: Boolean) {
        achieveViewModel.updateAchieveUser(isAchieve, object :
            BaseViewModel.APICallBacks<LinkedTreeMap<Any, Any>> {

            override fun onSuccess(model: LinkedTreeMap<Any, Any>?) {
                //hideProgress()
                if (model != null) {

                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                //hideProgress()

            }
        })
    }
}