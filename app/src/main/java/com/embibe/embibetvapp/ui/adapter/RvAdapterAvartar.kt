package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.RowItemAvatarBinding
import com.embibe.embibetvapp.utils.ContantUtils
import com.embibe.embibetvapp.utils.Utils

class RvAdapterAvartar(var context: Context) : RecyclerView.Adapter<RvAdapterAvartar.VHAvatar>() {

    private lateinit var binding: RowItemAvatarBinding
    var onItemClick: ((Int) -> Unit)? = null
    private var defaultPosition: Int = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHAvatar {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_item_avatar,
            parent,
            false
        )
        return VHAvatar(binding)
    }

    override fun getItemCount(): Int {
        return 5
    }

    fun defaultAvatarPosition(defaultPosition: Int) {
        this.defaultPosition = defaultPosition
    }

    override fun onBindViewHolder(holder: VHAvatar, position: Int) {
        holder.bind(position)
    }

    inner class VHAvatar(var binding: RowItemAvatarBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(position: Int) {
            Utils.setAvatarImage(
                context,
                binding.ivAvatar,
                ContantUtils.getImgResourceId(position % 5)
            )

            binding.frlAvatar.setOnFocusChangeListener { _, hasFocus ->
                when (hasFocus) {
                    true -> binding.ivAvatar.setBackgroundResource(R.drawable.layer_circle_glow_cyan)
                    false -> binding.ivAvatar.setBackgroundResource(R.drawable.layer_circle_transparent)
                }
            }
//            if (position == 0) binding.frlAvatar.requestFocus()
//            binding.frlAvatar.setOnClickListener {
//                listener.onClicked("avatar", position)
//            }

            if (defaultPosition == position) binding.frlAvatar.requestFocus()
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(adapterPosition)
            }
        }
    }
}