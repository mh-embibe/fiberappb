package com.embibe.embibetvapp.ui.interfaces

interface TvKeysToSearchRes {
    fun focusToSearch(setFocus: Boolean)
}