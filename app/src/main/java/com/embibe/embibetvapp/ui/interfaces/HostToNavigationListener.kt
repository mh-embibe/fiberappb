package com.embibe.embibetvapp.ui.interfaces

/**
 * communication from right-side content to left-side navigation
 */

interface HostToNavigationListener {

    /**
     * DPAD_LEFT event on first item of rows will be notified using this function
     */
    fun showNavigation(toShow: Boolean)
    fun switchToPractice()
    fun switchToLearn()
    fun switchToTest()
}