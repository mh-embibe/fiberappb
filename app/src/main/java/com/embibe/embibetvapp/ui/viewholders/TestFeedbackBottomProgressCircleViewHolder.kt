package com.embibe.embibetvapp.ui.viewholders

import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemTestFeedbackCircleBinding

class TestFeedbackBottomProgressCircleViewHolder(val binding: ItemTestFeedbackCircleBinding) :
    BaseViewHolder<String>(binding) {
    override fun bind(item: String) {
        binding.tvLabel.text = item
        when (item) {
            "Your Score" -> {
                binding.ivCircle.setImageResource(R.drawable.circle_red_adobe_asset)
            }
            else -> binding.ivCircle.setImageResource(R.drawable.circle_orange_adobe_asset)
        }
    }
}