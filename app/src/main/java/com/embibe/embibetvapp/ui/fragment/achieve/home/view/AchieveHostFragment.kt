package com.embibe.embibetvapp.ui.fragment.achieve.home.view

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.TRANSLATION_Y
import android.view.ViewGroup
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.databinding.FragmentAchieveHomeBinding
import com.embibe.embibetvapp.ui.fragment.achieve.home.viewmodel.AchieveHomeViewModel
import com.embibe.embibetvapp.ui.fragment.learn.LearnHostFragment
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.HostToNavigationListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.ui.interfaces.RowsToBannerListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import java.util.*

class AchieveHostFragment : BaseAppFragment(), DPadKeysListener, RowsToBannerListener,
    NavigationMenuCallback {
    private lateinit var navigationMenuCallback: NavigationMenuCallback
    private lateinit var viewModel: AchieveHomeViewModel
    private lateinit var binding: FragmentAchieveHomeBinding
    private lateinit var coroutineScope: CoroutineScope
    private var TAG = this.javaClass.name
    private var achieveBannerFragment = AchieveBannerFragment()
    private var achieveRowsFragment = AchieveRowsFragment()
    private var hostToNavigationListener: HostToNavigationListener? = null
    private var lastFocusedContent = "Banner"
    private var contentAvailable = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coroutineScope = CoroutineScope(Dispatchers.Main)
        viewModel = ViewModelProvider(this).get(AchieveHomeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_achieve_home,
            container,
            false
        )
        navigationMenuCallback.navMenuToggle(false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadData()
    }

    private fun loadData() {
        addFragment(achieveBannerFragment, binding.rowHeroBannerAchieve.id)
        addFragment(achieveRowsFragment, binding.rowCarouselsAchieve.id)
    }

    private fun addFragment(fragment: Fragment, layoutResId: Int) {
        childFragmentManager.beginTransaction().replace(layoutResId, fragment).commit()
    }

    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        when (childFragment) {
            is AchieveBannerFragment -> {
                childFragment.setDPadKeysListener(this)
                childFragment.setNavigationMenuListener(this)
            }
            is AchieveRowsFragment -> {
                childFragment.setDPadKeysListener(this)
                childFragment.setNavigationMenuListener(this)
                childFragment.setRowsToBannerListener(this)
            }
        }
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {
        Log.i("Anil", "isKeyPadDown: " + isDown + "from : " + from)
        if (isDown) {
            when (from) {
                "Banner" -> {
                    slideBannerUp()
                    achieveRowsFragment.setFirstRowFocus()
                }
                "Dashboard" -> {
                    lastFocusedContent = "Banner"
                }
            }
        }
    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {
        Log.i("Anil", "isKeyPadDown: " + isUp + "from : " + from)
        if (isUp) {
            when (from) {
                "Banner" -> {
//                    doAsync {
//                        val quickLinksDataAvailable =
//                            homeViewModel.fetchSectionByPageName(AppConstants.QUICK_LINKS)
//                                .isNotEmpty()
//
//                        uiThread {
//                            if (quickLinksDataAvailable) {
//                                showQuickDashBoard()
//                                lastFocusedContent = "Dashboard"
//                                learnBannerImgFlipper.stopImgFlipping()
//                            }
//                        }
//                    }
                }
            }
        }
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
        Log.i("Anil", "isKeyPadLeft: " + isLeft + "from : " + from)
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {
        Log.i("Anil", "isKeyPadRight: " + isRight + "from : " + from)
    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {
        Log.i("Anil", "isKeyPadEnter: " + isEnter + "from : " + from)
        if (isEnter) {
            when (from) {
                "Rows" -> {
                    if (cardType.toLowerCase(Locale.getDefault()) == "practice")
                        hostToNavigationListener?.switchToPractice()
                    else if (cardType.toLowerCase(Locale.getDefault()) == "learn") {
                        hostToNavigationListener?.switchToLearn()
                    }
                }
            }
        }
    }

    private fun slideBannerUp() {
        val bannerAchieveOA =
            ObjectAnimator.ofFloat(
                binding.rowHeroBannerAchieve,
                View.TRANSLATION_Y, 0f, LearnHostFragment.Values.ROWS_TRANSLATION_Y
            )
        val rowsAchieveOA =
            ObjectAnimator.ofFloat(binding.rowCarouselsAchieve, TRANSLATION_Y, 0f, LearnHostFragment.Values.ROWS_TRANSLATION_Y)
        val animatorSet = AnimatorSet()

        animatorSet.apply {
            playTogether(bannerAchieveOA,rowsAchieveOA)
            duration = 500
            doOnEnd {
                binding.rowHeroBannerAchieve.visibility = View.INVISIBLE
            }
            start()
        }


        binding.rowCarouselsAchieve.setPadding(0, 50, 0, 0)

        lastFocusedContent = "Rows"
    }

    fun setHostToNavigationListener(callback: HostToNavigationListener) {
        this.hostToNavigationListener = callback
    }

    override fun onDetach() {
        super.onDetach()
        hostToNavigationListener = null
    }

    override fun rowsToBanner() {
        binding.rowHeroBannerAchieve.requestFocus()
        achieveBannerFragment.setFocusToLastItem()

        val bannerBGLearnOA = ObjectAnimator.ofFloat(
            binding.rowHeroBannerAchieve,
            TRANSLATION_Y,
            LearnHostFragment.Values.BANNER_TRANSLATION_Y,
            0f
        )
        val bannerLearnOA = ObjectAnimator.ofFloat(
            binding.rowHeroBannerAchieve,
            TRANSLATION_Y,
            LearnHostFragment.Values.BANNER_TRANSLATION_Y,
            0f
        )
        val rowsLearnOA =
            ObjectAnimator.ofFloat(
                binding.rowCarouselsAchieve,
                TRANSLATION_Y,
                LearnHostFragment.Values.ROWS_TRANSLATION_Y,
                0f
            )
        val animatorSet = AnimatorSet()
        animatorSet.apply {
            playTogether(bannerBGLearnOA, bannerLearnOA, rowsLearnOA)
            duration = 500
            doOnStart {
                binding.rowHeroBannerAchieve.visibility = View.VISIBLE
                binding.rowCarouselsAchieve.visibility = View.VISIBLE
            }
            start()
        }

        binding.rowCarouselsAchieve.setPadding(0, 0, 0, 0)
        lastFocusedContent = "Banner"

    }

    override fun notifyBanner(notifyMessage: String) {
    }

    override fun notifyHost(notifyMessage: String) {
    }

    override fun navMenuToggle(toShow: Boolean) {
        hostToNavigationListener?.showNavigation(toShow)
    }

    fun restoreLastSelection() {
        when (contentAvailable) {
            false -> {
                binding.rowHeroBannerAchieve.postDelayed({
                    binding.rowHeroBannerAchieve.requestFocus()
                }, 100)
            }
            true -> {
                when {
                    lastFocusedContent.equals("Banner", true) -> {
                        achieveBannerFragment.setFocusToLastItem()
                    }
                    else -> {
                        binding.rowCarouselsAchieve.requestFocus()
                        achieveRowsFragment.restoreLastSelection()
                    }
                }
            }
        }
    }



}