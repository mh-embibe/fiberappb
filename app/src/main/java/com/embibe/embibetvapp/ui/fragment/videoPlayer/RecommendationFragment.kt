package com.embibe.embibetvapp.ui.fragment.videoPlayer

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.BOOK_ID
import com.embibe.embibetvapp.constant.AppConstants.CONTENT_TYPE
import com.embibe.embibetvapp.constant.AppConstants.COOBO
import com.embibe.embibetvapp.constant.AppConstants.COOBO_URL
import com.embibe.embibetvapp.constant.AppConstants.DURATION_LENGTH
import com.embibe.embibetvapp.constant.AppConstants.FORMAT_ID
import com.embibe.embibetvapp.constant.AppConstants.IS_EMBIBE_SYLLABUS
import com.embibe.embibetvapp.constant.AppConstants.IS_RECOMMENDATION_ENABLED
import com.embibe.embibetvapp.constant.AppConstants.LEARNMAP_CODE
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_FORMAT_NAME
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_NAME
import com.embibe.embibetvapp.constant.AppConstants.PLAYER_TYPE_YOUTUBE
import com.embibe.embibetvapp.constant.AppConstants.SLIDE_COUNT
import com.embibe.embibetvapp.constant.AppConstants.SOURCE_NEXT_BOOK_TOC
import com.embibe.embibetvapp.constant.AppConstants.SOURCE_NEXT_EMBIBE_SYLLABUS
import com.embibe.embibetvapp.constant.AppConstants.SOURCE_NEXT_HOMEPAGE
import com.embibe.embibetvapp.constant.AppConstants.TOPIC_LEARN_PATH
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_AUTHORS
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CONCEPT_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_DESCRIPTION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SOURCE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SUBJECT
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TITLE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TOTAL_DURATION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_URL
import com.embibe.embibetvapp.databinding.FragmentRecommendationBinding
import com.embibe.embibetvapp.model.NextVideoRecommendationsRes
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.ui.activity.VideoPlayerActivity
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.unity.UnityPlayerActivity
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.google.gson.Gson

class RecommendationFragment : BaseAppFragment(), DPadKeysListener {
    private lateinit var homeViewModel: HomeViewModel
    private val TAG = this.javaClass.simpleName

    private lateinit var binding: FragmentRecommendationBinding
    private var recommendationRowsFragment =
        RecommendationRowsFragment()

    private var videoConceptId: String? = null
    private var videoUrl: String? = null
    private var videoTitle: String? = null
    private var videoDescription: String? = null
    private var videoSubject: String? = null
    private var videoChapter: String? = null
    private var videoAuthors: String? = null
    private var videoType: String? = null
    private var videoTotalDuration: String? = null
    private var topicLearnPath: String = ""
    private var videoId: String? = null
    private var contentType: String? = null
    private var nextVideoContent: Content? = null
    private var countDownTimer: CountDownTimer? = null
    private var countDownInSeconds: Int = 11
    private var duration: Int = 0
    private var slideCount: Int = 0
    private var book_id: String? = ""
    private var learningMap: String? = ""
    private var isEmbibeSyllabus: Boolean = false
    var ids = arrayListOf<String>()
    private var learnPathName: String = ""
    private var learnPathFormatName: String = ""
    private var format_id: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        loadData(arguments)
    }

    private fun loadData(videoBundle: Bundle?) {
        //video data of last played video
        videoUrl = videoBundle?.getString(VIDEO_URL)
        videoTitle = videoBundle?.getString(VIDEO_TITLE)
        videoDescription = videoBundle?.getString(VIDEO_DESCRIPTION)
        videoSubject = videoBundle?.getString(VIDEO_SUBJECT)
        videoChapter = videoBundle?.getString(VIDEO_CHAPTER)
        videoAuthors = videoBundle?.getString(VIDEO_AUTHORS)
        videoType = videoBundle?.getString(VIDEO_SOURCE)
        videoId = videoBundle?.getString(VIDEO_ID)
        learningMap = videoBundle?.getString(LEARNMAP_CODE)
        videoConceptId = videoBundle?.getString(VIDEO_CONCEPT_ID)
        contentType = videoBundle?.getString(CONTENT_TYPE)
        if (videoBundle?.containsKey(AppConstants.DURATION_LENGTH) != null)
            duration = videoBundle.getInt(AppConstants.DURATION_LENGTH)
        if (videoBundle?.containsKey(AppConstants.SLIDE_COUNT) != null)
            slideCount = videoBundle.getInt(AppConstants.SLIDE_COUNT)
        if (videoBundle?.containsKey(IS_EMBIBE_SYLLABUS) != null)
            isEmbibeSyllabus = videoBundle.getBoolean(IS_EMBIBE_SYLLABUS, false)
        if (videoBundle?.containsKey(BOOK_ID) != null)
            book_id = videoBundle.getString(BOOK_ID)
        if (videoBundle?.containsKey(VIDEO_TOTAL_DURATION) != null)
            videoTotalDuration = videoBundle.getString(VIDEO_TOTAL_DURATION)
        if (videoBundle?.containsKey(TOPIC_LEARN_PATH) != null)
            topicLearnPath = if (videoBundle.getString(TOPIC_LEARN_PATH).isNullOrEmpty()
            ) "" else videoBundle.getString(TOPIC_LEARN_PATH)!!


        if (videoBundle?.containsKey(LEARN_PATH_NAME) != null)
            learnPathName = videoBundle.getString(LEARN_PATH_NAME).toString()

        if (videoBundle?.containsKey(LEARN_PATH_FORMAT_NAME) != null)
            learnPathFormatName = videoBundle.getString(LEARN_PATH_FORMAT_NAME).toString()

        if (videoBundle?.containsKey(FORMAT_ID) != null)
            format_id = videoBundle.getString(FORMAT_ID).toString()


    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_recommendation, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        SegmentUtils.trackEventRecommendationStart()
        if (book_id == null || book_id.isNullOrEmpty()) {
            getNextVideoRecommendations(
                videoId!!, SOURCE_NEXT_HOMEPAGE, "",
                topicLearnPath, learnPathName, learnPathFormatName
            )
        } else {
            getNextVideoRecommendations(
                videoId!!,
                if (isEmbibeSyllabus) SOURCE_NEXT_EMBIBE_SYLLABUS else SOURCE_NEXT_BOOK_TOC,
                book_id, topicLearnPath, learnPathName, learnPathFormatName
            )
        }
        replayVideoListeners()
        nextVideoListeners()
        errorListeners()
    }

    private fun errorListeners() {
        binding.ivReplayError.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackEventErrorReplayVideoFocus()
                binding.ivReplay.setImageResource(R.drawable.ic_replay_focus_in)
            } else {
                binding.ivReplay.setImageResource(R.drawable.ic_replay_focus_out)
            }
        }

        binding.ivReplayError.setOnKeyListener { v, keyCode, event ->

            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        SegmentUtils.trackEventErrorReplayVideoClick()
                        replayVideo()
                    }
                }
            }

            false
        }
    }

    private fun nextVideoListeners() {
        binding.ivNextVideoScreen.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackEventRecommendationNextVideoTileFocus(nextVideoContent!!)
                binding.ivNextVideoScreen.setBackgroundResource(R.drawable.bg_home_card_background)
                binding.ivNextVideoPlay.visibility = VISIBLE
            } else {
                if (countDownInSeconds > 0) {
                    countDownTimer?.cancel()
                    SegmentUtils.trackEventPlayNextVideoTimerCanceled()
                    binding.tvPlayingNext.visibility = View.GONE
                    binding.tvPlayingNextCountdown.visibility = View.GONE
                    binding.tvPlayingNextCountdownDots.visibility = View.GONE
                }
                binding.ivNextVideoScreen.setBackgroundResource(0)
                binding.ivNextVideoPlay.visibility = GONE
            }
        }

        binding.ivNextVideoScreen.setOnKeyListener { v, keyCode, event ->

            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        binding.ivReplay.postDelayed({
                            binding.ivReplay.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        recommendationRowsFragment.restoreLastSelection()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        SegmentUtils.trackEventRecommendationNextVideoTileClick(nextVideoContent!!)
                        playNextVideo()
                    }
                }
            }

            false
        }
    }

    private fun replayVideoListeners() {
        binding.ivReplay.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackEventReplayVideoFocus()
                binding.ivReplay.setImageResource(R.drawable.ic_replay_focus_in)
            } else {
                binding.ivReplay.setImageResource(R.drawable.ic_replay_focus_out)
            }
        }

        binding.ivReplay.setOnKeyListener { v, keyCode, event ->

            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        binding.ivNextVideoScreen.postDelayed({
                            binding.ivNextVideoScreen.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        recommendationRowsFragment.restoreLastSelection()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        SegmentUtils.trackEventReplayVideoClick()
                        replayVideo()
                    }
                }
            }

            false
        }
    }

    private fun startVideoPlayer(
        videoUrl: String,
        videoId: String,
        videoConceptId: String,
        contentType: String,
        videoTitle: String,
        videoDescription: String,
        videoSubject: String,
        videoChapter: String,
        videoAuthors: String,
        videoType: String,
        videoTotalDuration: String,
        topicLearnPath: String
    ) {

        if (videoType == PLAYER_TYPE_YOUTUBE && Utils.isYouTubeAppAvailable(requireContext())) {
            Utils.startYoutubeApp(requireContext(), videoUrl)
        } else {
            val intent = Intent(App.context, VideoPlayerActivity::class.java)
            intent.putExtra(VIDEO_URL, videoUrl)
            intent.putExtra(VIDEO_ID, videoId)
            intent.putExtra(VIDEO_CONCEPT_ID, videoConceptId)
            intent.putExtra(CONTENT_TYPE, contentType)
            intent.putExtra(VIDEO_TITLE, videoTitle)
            intent.putExtra(VIDEO_TOTAL_DURATION, videoTotalDuration)
            intent.putExtra(VIDEO_DESCRIPTION, videoDescription)
            intent.putExtra(VIDEO_SUBJECT, videoSubject)
            intent.putExtra(VIDEO_CHAPTER, videoChapter)
            intent.putExtra(VIDEO_AUTHORS, videoAuthors)
            intent.putExtra(VIDEO_SOURCE, videoType)
            intent.putExtra(TOPIC_LEARN_PATH, topicLearnPath)
            intent.putExtra(LEARNMAP_CODE, learningMap)
            intent.putExtra(IS_RECOMMENDATION_ENABLED, true)
            intent.putExtra(IS_EMBIBE_SYLLABUS, isEmbibeSyllabus)
            intent.putExtra(BOOK_ID, book_id)
            intent.putExtra(AppConstants.FORMAT_ID, format_id)
            intent.putExtra(AppConstants.LEARN_PATH_NAME, learnPathName)
            intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, learnPathFormatName)
            startActivity(intent)
            //activity?.finish()
        }

    }

    private fun setRecommendationFragment(layoutId: Int, fragmentType: Fragment) {
        childFragmentManager.beginTransaction().replace(layoutId, fragmentType).commit()
    }

    private fun setNextVideoView(nextVideoItem: Content) {
        if (nextVideoItem.title.length <= 50)
            binding.tvNextVideoTitle.text = nextVideoItem.title
        else
            binding.tvNextVideoTitle.text = "${nextVideoItem.title.substring(0, 50)}..."

        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(requireContext()).setDefaultRequestOptions(requestOptions)
            .load(nextVideoItem.thumb)
            .placeholder(R.drawable.video_placeholder)
            .transform(RoundedCorners(8))
            .into(binding.ivNextVideoScreen)
    }

    private fun getNextVideoRecommendations(
        contentId: String?,
        source: String?,
        bookId: String?,
        topicLearnPathName: String,
        learnPathName: String,
        learnPathFormatName: String
    ) {
        showProgress()
        homeViewModel.getNextVideoRecommendations(
            contentId, source, bookId, topicLearnPathName, learnPathName, learnPathFormatName,
            object : BaseViewModel.APICallBacks<NextVideoRecommendationsRes> {
                override fun onSuccess(model: NextVideoRecommendationsRes?) {
                    hideProgress()
                    try {
                        if (model?.data != null) {
                            updateRecommendations(model.data.recommendations!!)
                            updateNextVideo(model.data.nextcontent!!)
                            SegmentUtils.trackEventRecommendationEnd()
                        } else {
                            updateNoRecommendations()
                        }
                    } catch (e: Exception) {
                        updateNoRecommendations()
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    hideProgress()
                    updateNoRecommendations()
                }
            })
    }

    private fun setRecommendationInitialSetup() {
        setRecommendationUI()
        binding.flRecommendationRows.postDelayed({
            binding.flRecommendationRows.requestFocus()
        }, 100)
    }

    private fun getAutoPlayVideos(conceptId: String, contentId: String, bookId: String) {
        showProgress()

        homeViewModel.getAutoPlayVideos(
            conceptId,
            contentId, DataManager.instance.nextVideoIds, bookId, isEmbibeSyllabus,
            object : BaseViewModel.APICallBacks<NextVideoRecommendationsRes> {
                override fun onSuccess(model: NextVideoRecommendationsRes?) {
                    hideProgress()
                    try {
                        if (model?.data != null) {
                            updateRecommendations(model.data.recommendations!!)
                            updateNextVideo(model.data.nextcontent!!)
                            SegmentUtils.trackEventRecommendationEnd()
                        } else {
                            updateNoAutoPlayVideos()
                        }
                    } catch (e: Exception) {
                        updateNoAutoPlayVideos()
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    hideProgress()
                    updateNoAutoPlayVideos()
                }
            })
    }

    private fun setTimer() {
        countDownTimer = object : CountDownTimer(10000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                countDownInSeconds = (millisUntilFinished / 1000).toInt()
                Log.i(TAG, countDownInSeconds.toString())
                if (countDownInSeconds > 0)
                    binding.tvPlayingNextCountdown.text = "${millisUntilFinished / 1000}"
            }

            override fun onFinish() {
                //play next video
                SegmentUtils.trackEventPlayNextVideoTimerCompleted()
                playNextVideo()
            }
        }
        countDownTimer?.start()
    }

    private fun setInitialFocus() {
        binding.flRecommendationRows.postDelayed({
            binding.flRecommendationRows.clearFocus()
        }, 100)
        binding.ivNextVideoScreen.postDelayed({
            binding.ivNextVideoScreen.requestFocus()
        }, 100)
    }

    private fun setRecommendationUI() {
        binding.flRecommendationRows.visibility = View.VISIBLE
        binding.ivReplay.visibility = View.VISIBLE
        binding.tvReplay.visibility = View.VISIBLE
        binding.ivNextVideoFrame.visibility = View.VISIBLE
        binding.tvNextVideoTitle.visibility = View.VISIBLE
        binding.tvPlayingNext.visibility = View.VISIBLE
        binding.tvPlayingNextCountdown.visibility = View.VISIBLE
        binding.tvPlayingNextCountdownDots.visibility = View.VISIBLE
        binding.ivReplayError.visibility = View.GONE
        binding.tvReplayError.visibility = View.GONE
    }

    private fun getStaticData() {
        /*static response */
        val json =
            Utils.inputStreamToString(App.context.resources.openRawResource(R.raw.next_vide_static))
        var nextvideos = Gson().fromJson(json, NextVideoRecommendationsRes::class.java)
        if (nextvideos?.data != null) {
            updateRecommendations(nextvideos.data?.recommendations!!)
            updateNextVideo(nextvideos.data?.nextcontent!!)
        } else {
            updateNoRecommendations()
        }
    }

    private fun updateNoRecommendations() {
        binding.flRecommendationRows.visibility = View.GONE
        binding.ivReplay.visibility = View.GONE
        binding.tvReplay.visibility = View.GONE
        binding.ivNextVideoFrame.visibility = View.GONE
        binding.ivNextVideoPlay.visibility = View.GONE
        binding.tvNextVideoTitle.visibility = View.GONE
        binding.tvPlayingNext.visibility = View.GONE
        binding.tvPlayingNextCountdown.visibility = View.GONE
        binding.tvPlayingNextCountdownDots.visibility = View.GONE
        binding.ivReplayError.visibility = View.VISIBLE
        binding.tvReplayError.visibility = View.VISIBLE
        binding.ivReplayError.setImageResource(R.drawable.ic_replay_cyan)
        binding.ivReplayError.postDelayed({
            binding.ivReplayError.requestFocus()
        }, 10)
        countDownTimer?.cancel()
        SegmentUtils.trackEventPlayNextVideoTimerCanceled()
    }

    private fun updateRecommendations(recommendations: List<Results>) {
        if (recommendations.isNotEmpty()) {
            setRecommendationInitialSetup()
            recommendationRowsFragment.setData(recommendations)
            setRecommendationFragment(binding.flRecommendationRows.id, recommendationRowsFragment)
        }
    }

    private fun updateNoAutoPlayVideos() {

    }

    private fun updateNextVideo(nextVideo: List<Results>) {
        if (nextVideo.isNotEmpty()) {
            setInitialFocus()
            setTimer()
            val nextVideoItem = nextVideo[0].content[0]
            nextVideoContent = nextVideoItem
            setNextVideoView(nextVideoItem)
        }
    }

    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)

        when (childFragment) {
            is RecommendationRowsFragment -> {
                recommendationRowsFragment.setDpadKeysListener(this)
            }
        }
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {
        if (isDown) {
            when (from) {
                "Rows" -> {
                    binding.ivReplay.isFocusable = true
                    binding.ivNextVideoScreen.isFocusable = true
                }
            }
        }
    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {
    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {

    }

    private fun playNextVideo() {
        if (isEmbibeSyllabus) {
            DataManager.instance.nextVideoIds.add(videoId ?: "")
        } else {
            DataManager.instance.nextVideoIds = arrayListOf()
        }
        recommendationRowsFragment.playVideo(nextVideoContent ?: return)
    }

    private fun replayVideo() {
        if (contentType?.toLowerCase().equals(COOBO.toLowerCase())) {
            var intent = Intent(activity, UnityPlayerActivity::class.java)
            intent.putExtra(VIDEO_CONCEPT_ID, videoConceptId)
            intent.putExtra(COOBO_URL, videoUrl)
            intent.putExtra(SLIDE_COUNT, slideCount)
            intent.putExtra(DURATION_LENGTH, duration)
            intent.putExtra(LEARNMAP_CODE, learningMap)
            intent.putExtra(AppConstants.IS_RECOMMENDATION_ENABLED, true)
            intent.putExtra(AppConstants.IS_EMBIBE_SYLLABUS, isEmbibeSyllabus)
            intent.putExtra(AppConstants.BOOK_ID, book_id)
            intent.putExtra(AppConstants.FORMAT_ID, format_id)
            intent.putExtra(AppConstants.LEARN_PATH_NAME, learnPathName)
            intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, learnPathFormatName)
            startActivity(intent)
            activity?.finish()
        } else
            startVideoPlayer(
                videoUrl ?: "",
                videoId ?: "",
                videoConceptId ?: "",
                contentType ?: "",
                videoTitle ?: "",
                videoDescription ?: "",
                videoSubject ?: "",
                videoChapter ?: "",
                videoAuthors ?: "",
                videoType ?: "",
                videoTotalDuration ?: "",
                topicLearnPath
            )
        activity?.finish()

    }

    override fun onDestroy() {
        super.onDestroy()
        countDownTimer?.cancel()
        SegmentUtils.trackEventPlayNextVideoTimerCanceled()
    }
}
