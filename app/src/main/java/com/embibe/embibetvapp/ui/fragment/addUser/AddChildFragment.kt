package com.embibe.embibetvapp.ui.fragment.addUser

import android.app.Activity
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.ui.adapter.SelectBoardAdapter
import com.embibe.embibetvapp.ui.custom.TvRecyclerView
import com.embibe.embibetvapp.utils.ContantUtils


class AddChildFragment : BaseAppFragment() {

    private lateinit var mTvRecyclerView: TvRecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_add_child, container, false)
        mTvRecyclerView = view.findViewById(R.id.select_board_rv)

        init()
        return view

    }

    private fun init() {
        val manager = GridLayoutManager(context, 1)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        manager.supportsPredictiveItemAnimations()
        mTvRecyclerView.layoutManager = manager

        val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space1)
        mTvRecyclerView.addItemDecoration(SpaceItemDecoration(itemSpace))
        val animator = DefaultItemAnimator()
        mTvRecyclerView.itemAnimator = animator
        val mAdapter = context?.let {
            SelectBoardAdapter(
                it
            )
        }
        mTvRecyclerView.adapter = mAdapter
        mAdapter!!.setOnItemStateListener(object : SelectBoardAdapter.OnItemStateListener {
            override fun onItemClick(view: View?, position: Int) {
                val returnIntent = Intent()
                returnIntent.putExtra(
                    "result", ContantUtils.testingdata[position]
                )
                activity!!.setResult(Activity.RESULT_OK, returnIntent)
                activity!!.finish()
            }
        })

    }

    inner class SpaceItemDecoration internal constructor(private val space: Int) :
        RecyclerView.ItemDecoration() {
        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            outRect.left = space
        }
    }

}