package com.embibe.embibetvapp.ui.custom

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText

class CustomEditText(context: Context?, attrs: AttributeSet) : AppCompatEditText(context, attrs) {
    override fun onCheckIsTextEditor(): Boolean {
        return true //for some reason False leads to cursor never blinking or being visible even if setCursorVisible(true) was called in code.
    }

    override fun isTextSelectable(): Boolean {
        return true
    }
}