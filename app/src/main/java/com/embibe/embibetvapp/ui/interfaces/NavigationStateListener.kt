package com.embibe.embibetvapp.ui.interfaces

interface NavigationStateListener {
    fun onStateChanged(expanded: Boolean, lastSelected: String)
}