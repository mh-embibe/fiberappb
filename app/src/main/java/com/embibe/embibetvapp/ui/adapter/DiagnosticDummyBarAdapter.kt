package com.embibe.embibetvapp.ui.adapter

import android.view.ViewGroup
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemBarBinding
import com.embibe.embibetvapp.ui.viewholders.BaseViewHolder
import com.embibe.embibetvapp.ui.viewholders.DiagnosticDummyBarViewHolder
import com.embibe.embibetvapp.utils.Utils

class DiagnosticDummyBarAdapter : BaseAdapter<String>() {

    override fun getLayoutId(position: Int, obj: String) = R.layout.item_bar

    override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<String> {
        val binding = Utils.binder<ItemBarBinding>(R.layout.item_bar, parent)
        return DiagnosticDummyBarViewHolder(binding, listItems)
    }
}