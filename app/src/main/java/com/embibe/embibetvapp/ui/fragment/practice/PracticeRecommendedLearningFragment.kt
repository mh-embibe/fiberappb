package com.embibe.embibetvapp.ui.fragment.practice

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentPracticeRecommendedLearningBinding
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.activity.BookDetailActivity
import com.embibe.embibetvapp.ui.activity.DetailActivity
import com.embibe.embibetvapp.ui.activity.PracticeActivity
import com.embibe.embibetvapp.ui.adapter.RecommendedLearningPracticeAdapter
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.Utils

class PracticeRecommendedLearningFragment(var data: List<Content>) : BaseAppFragment() {
    private var listData: ArrayList<Content> = ArrayList()
    private lateinit var binding: FragmentPracticeRecommendedLearningBinding
    private lateinit var homeViewModel: HomeViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_practice_recommended_learning,
                container,
                false
            )
        setAdapter()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

    }

    fun setAdapter() {
        listData = data as ArrayList<Content>

        var bookAvailableForAdapter = RecommendedLearningPracticeAdapter(requireActivity())
        binding.recyclerPractice.layoutManager =
            LinearLayoutManager(requireActivity())
        binding.recyclerPractice.adapter = bookAvailableForAdapter
        for (i in 0..10) {
            listData.add(listData.get(0))
        }
        bookAvailableForAdapter.setData(listData)
        bookAvailableForAdapter.onItemClick = { content ->
            startActivityByType(content)
        }

    }


    private fun startActivityByType(content: Content) {

        when ((content.type).toLowerCase()) {
            AppConstants.TYPE_VIDEO, AppConstants.TYPE_COOBO -> {
                startDetailActivity(content)
            }
            AppConstants.TYPE_BOOK -> {
                startBookDetailActivity(content)
            }
            AppConstants.TYPE_PRACTICE, AppConstants.TYPE_CHAPTER -> {
                startPracticeDetailActivity(content)
            }
        }
    }

    private fun startDetailActivity(content: Content) {
        Utils.loadConceptsAsync(content, context)
        val intent = Intent(App.context, DetailActivity::class.java)
        Utils.insertBundle(content, intent)
        startActivity(intent)
    }

    private fun startBookDetailActivity(content: Content) {
        var intent = Intent(App.context, BookDetailActivity::class.java)
        intent.putExtra(AppConstants.NAV_NAME_TYPE, AppConstants.NAV_TYPE_BOOK)
        Utils.insertBundle(content, intent)
        startActivity(intent)
    }

    private fun startPracticeDetailActivity(data: Content) {
        val intent = Intent(requireView().context, PracticeActivity::class.java)
        intent.putExtra("id", data.id)
        intent.putExtra("title", data.title)
        intent.putExtra("subject", data.subject)
        intent.putExtra(AppConstants.TOPIC_LEARN_PATH, data.learning_map.topicLearnPathName)
        intent.putExtra(AppConstants.FORMAT_ID, data.learning_map.format_id)
        intent.putExtra(AppConstants.LM_NAME, data.learnmap_id)
        intent.putExtra(AppConstants.PRACTICE_MODE, AppConstants.PracticeModes.Normal)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, data.learnpath_name)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, data.learnpath_format_name)
        try {
            val parts = data.learning_map.lpcode.split("--")
            val lmCode = parts[parts.size - 1]
            val lmLevel = Utils.getLevelUsingCode(lmCode)
            val examCode = Utils.getExamLevelCode(parts)
            intent.putExtra("lm_level", lmLevel)
            intent.putExtra("lm_code", lmCode)
            intent.putExtra("exam_code", examCode)
            intent.putExtra(AppConstants.PRACTICE_MODE, AppConstants.PracticeModes.Normal)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        startActivity(intent)
    }
}