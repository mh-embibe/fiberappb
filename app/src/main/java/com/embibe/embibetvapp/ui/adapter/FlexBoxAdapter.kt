package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.adduser.Goal
import com.embibe.embibetvapp.ui.activity.AddGoalsExamsActivity


class FlexboxAdapter(context: Context, arrayList: List<Goal>) :
    RecyclerView.Adapter<FlexboxAdapter.ViewHolder>() {


    val context: Context = context
    var arrayList: List<Goal> = arrayList
    private var selectedItem = 1
    var onItemClick: ((Goal) -> Unit)? = null
    var onFocusChange: ((Goal) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_goal, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        if (selectedItem == position) {
            holder.textGoalExam.background =
                context.getDrawable(R.drawable.shape_rec_white_border_with_radius)
            holder.textGoalExam.setTextColor(context.getColor(R.color.email_font_color))
        }
        bind(holder, arrayList[position], position)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textGoalExam: TextView

        init {
            textGoalExam = itemView.findViewById(R.id.textGoalExam)
        }
    }

    init {
        this.arrayList = arrayList
    }

    fun bind(
        holder: ViewHolder,
        item: Goal, adapterPosition: Int
    ) {
        /* goal = item
         executePendingBindings()*/

        holder.textGoalExam.background = context.getDrawable(R.drawable.bg_button_goals)
        holder.textGoalExam.setTextColor(context.getColor(R.color.white))


        if (item.supported) {
            holder.itemView.setOnClickListener {
                try {
                    holder.textGoalExam.postDelayed({
                        try {
                            onItemClick?.invoke(arrayList[adapterPosition])
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }, 500)

                    if (selectedItem != adapterPosition) {
                        val previousItem = selectedItem
                        selectedItem = adapterPosition
                        notifyItemChanged(previousItem)

                    } else
                        selectedItem = adapterPosition
                    notifyItemChanged(adapterPosition)
                } catch (e: Exception) {
                }
            }
        }

        holder.itemView.setOnFocusChangeListener { v, hasFocus ->
            onFocusChange?.invoke(arrayList[adapterPosition])
        }

    }

    fun updateSelection() {
        setDefaultFocusToSupportedGoal(arrayList)
        notifyDataSetChanged()
    }

    fun setDefaultFocusToSupportedGoal(primaryGoals: List<Goal>) {
        try {
            val goalCode = UserData.getGoalCode()
            for (index in primaryGoals.indices) {
                if (goalCode.equals(primaryGoals[index].code) /*primaryGoals[index].default*/) {
                    setDefaultSelectedItem(index)
                    AddGoalsExamsActivity.primaryGoalCode = primaryGoals[index].code
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setDefaultSelectedItem(position: Int) {
        selectedItem = position
        notifyDataSetChanged()
    }
}