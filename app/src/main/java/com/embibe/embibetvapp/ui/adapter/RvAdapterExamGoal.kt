package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.RowItemExamGoalBinding
import com.embibe.embibetvapp.model.adduser.Exam
import com.embibe.embibetvapp.ui.interfaces.ComponentClickListener
import com.embibe.embibetvapp.utils.SegmentUtils

class RvAdapterExamGoal(var context: Context, var listener: ComponentClickListener) :
    RecyclerView.Adapter<RvAdapterExamGoal.VHExamGoalAdapter>() {

    private lateinit var binding: RowItemExamGoalBinding
    private var goalsList: List<Exam> = ArrayList()
    private var lastClickedItem: Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHExamGoalAdapter {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_item_exam_goal,
            parent,
            false
        )
        return VHExamGoalAdapter(binding)
    }

    override fun getItemCount(): Int {
        return if (goalsList.size > 0) goalsList.size else 0
    }

    override fun onBindViewHolder(holder: VHExamGoalAdapter, position: Int) {
        holder.bind(goalsList.get(position), position)
    }

    fun updateData(goalsList: List<Exam>) {
        this.goalsList = goalsList
        notifyDataSetChanged()
    }

    //ViewHolder
    inner class VHExamGoalAdapter(var binding: RowItemExamGoalBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Exam, position: Int) {
            binding.btnExamGoal.text = item.name

            binding.btnExamGoal.setOnClickListener {
                lastClickedItem = position
                notifyDataSetChanged()
                SegmentUtils.trackAddGoalsScreenOptionsClick(item.name)
                listener.onClicked("", position)
            }
            binding.btnExamGoal.setOnFocusChangeListener { v, hasFocus ->
                when (hasFocus) {
                    true -> {
                        SegmentUtils.trackAddGoalsScreenOptionsFocus(item.name)
                    }
                }
            }

            if (lastClickedItem == position) {
                binding.btnExamGoal.background =
                    context.getDrawable(R.drawable.shape_rec_white_border_with_radius)
                binding.btnExamGoal.setTextColor(context.getColor(R.color.email_font_color))
                binding.btnExamGoal.requestFocus()
            } else {
                binding.btnExamGoal.background = context.getDrawable(R.drawable.bg_button_goals)
                binding.btnExamGoal.setTextColor(context.getColor(R.color.white))
            }
        }
    }
}