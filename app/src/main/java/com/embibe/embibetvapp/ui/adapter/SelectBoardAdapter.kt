package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.ui.custom.FocusRelativeLayout
import com.embibe.embibetvapp.utils.ContantUtils

class SelectBoardAdapter(var context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mListener: OnItemStateListener? = null

    interface OnItemStateListener {
        fun onItemClick(view: View?, position: Int)
    }

    fun setOnItemStateListener(listener: OnItemStateListener) {
        mListener = listener
    }

    inner class RecyclerViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var mFrameLayout: FocusRelativeLayout =
            itemView.findViewById<View>(R.id.fl_board_layout) as FocusRelativeLayout
        var mbutton: Button = itemView.findViewById<View>(R.id.board_button) as Button
        fun setListener() {
            mFrameLayout.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            mFrameLayout.setOnClickListener(this)

            if (mListener != null) {
                mListener!!.onItemClick(v, adapterPosition)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return RecyclerViewHolder(
            View.inflate(
                context,
                R.layout.item_recyclerview_board,
                null
            )
        )

    }

    override fun getItemCount(): Int {
        return 3
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as RecyclerViewHolder
        viewHolder.mbutton.text = ContantUtils.testingdata[position]
        viewHolder.setListener()
    }


}