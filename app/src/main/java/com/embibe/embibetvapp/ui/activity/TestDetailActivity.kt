package com.embibe.embibetvapp.ui.activity

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.BANNER_CONTENT
import com.embibe.embibetvapp.databinding.ActivityTestDetailBinding
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkDetailModel
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkResponse
import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.embibe.embibetvapp.model.test.TestDetailsRes
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.adapter.TestsAdapter
import com.embibe.embibetvapp.ui.fragment.test.TestDetailFragment
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.viewmodel.DetailsViewModel
import com.embibe.embibetvapp.ui.viewmodel.TestViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.getBackgroundUrl
import com.embibe.embibetvapp.utils.Utils.setBlurBackgroundImage
import com.embibe.embibetvapp.utils.Utils.setTestBlurBackgroundImage
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


class TestDetailActivity : BaseFragmentActivity(), DPadKeysListener {

    private lateinit var binding: ActivityTestDetailBinding
    private lateinit var testViewModel: TestViewModel
    private lateinit var detailsViewModel: DetailsViewModel
    private lateinit var testDetailFragment: TestDetailFragment
    private lateinit var testAdapter: TestsAdapter
    private lateinit var dPadKeysCallback: DPadKeysListener
    private lateinit var data: Content
    private lateinit var bannerData: BannerData

    private var isBundleData: Boolean = false
    private var pref = PreferenceHelper()
    private var isBookMarked: Boolean = false
    private var bookmarkRetryCount: Int = 0

    var query = ""
    var token = ""
    var title = ""
    var childId = 0L

    lateinit var currentContent: Content

    companion object {
        fun intentTestDetailActivity(context: Context): Intent {
            return Intent(context, TestDetailActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_test_detail)
        testViewModel = ViewModelProviders.of(this).get(TestViewModel::class.java)
        detailsViewModel = ViewModelProviders.of(this).get(DetailsViewModel::class.java)

        getData()
    }

    private fun getData() {
        if (intent.extras != null && intent.extras!!.containsKey(BANNER_CONTENT)) {
            bannerData = intent.getParcelableExtra(BANNER_CONTENT) as BannerData

            bindDataToUI(
                bannerData.title, bannerData.description, bannerData.embiumCoins,
                bannerData.testList.size.toString(), bannerData.subject, ""
            )

            isBundleData = true
            token = pref[AppConstants.EMBIBETOKEN, ""]
            childId = UserData.getChildId().toLong()
            query = "token=$token&child_id=$childId"

            getBookmarkStatus(
                (bannerData.testList as ArrayList<Content>)[0], AppConstants.MULTI_TYPE_TEST
            )
            currentContent = (bannerData.testList as ArrayList<Content>)[0]
            setBlurBackgroundImage(null)
        } else {
            data = getContent(intent)
            updateVisibility()
            if (data.sub_type != AppConstants.CUSTOM_TEST) {
                bindDataToUI(
                    data.title,
                    data.description,
                    data.currency.toString(),
                    "",
                    data.subject, ""
                )
            } else {
                bindDataToUI(
                    data.title,
                    data.description,
                    data.currency.toString(),
                    data.chapterSize,
                    data.subject, data.sub_type
                )
                binding.tvNTests.visibility = View.VISIBLE
                binding.btnBookmark.visibility = View.GONE

            }
            isBundleData = false
            getBookmarkStatus(data, AppConstants.SINGLE_TYPE_TEST)
            setTestListener()
            setBookmarkListener()
            currentContent = data
            setBackground()
        }
    }

    private fun bindDataToUI(
        title: String,
        description: String,
        embiumCoins: String,
        nTests: String,
        subject: String,
        type: String
    ) {
        binding.title.text = title
        binding.textDescription.text = description
        binding.embiumsCount.text = "${getString(R.string.earn)} $embiumCoins"
        if (type != AppConstants.CUSTOM_TEST) {
            binding.linSubjectName.visibility = View.GONE
            binding.tvNTests.text = "$nTests ${getString(R.string.test).toLowerCase()}"
            if (subject.isNotEmpty())
                displaySubjectName(subject)
            else
                binding.linSubjectName.visibility = View.GONE

        } else {
            binding.tvNTests.text = "$nTests"
            displaySubjectName(subject)

        }

    }

    private fun displaySubjectName(subject: String) {
        val subjectDetails: List<String> = subject.split(",")
        binding.linSubjectName.visibility = View.VISIBLE

        var textView = arrayOfNulls<TextView>(subjectDetails.size)
        val dim = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        dim.rightMargin = 6

        for (i in subjectDetails.indices) {
            textView[i] = TextView(this)
            textView[i]!!.layoutParams = dim
            textView[i]!!.text = capitalize(subjectDetails.get(i).toString())
            textView[i]!!.setBackgroundResource(R.drawable.test_grey_border)
            textView[i]!!.textSize = 12f
            val face = Typeface.createFromAsset(
                assets,
                "fonts/gilroy_semibold.ttf"
            )
            textView[i]!!.typeface = face
            binding.linSubjectName.addView(textView.get(i))
        }
    }

    private fun updateVisibility() {
        binding.tvNTests.visibility = View.GONE
        binding.btnTest.visibility = View.VISIBLE
        binding.btnBookmark.visibility = View.VISIBLE
    }

    private fun setTestListener() {
        binding.btnTest.setOnClickListener {
            callTestActivity(data)
        }
    }

    private fun setBookmarkListener() {
        binding.btnBookmark.setOnClickListener {
            Utils.isBookmarkUpdatedForTEST = true
            isBookMarked = !isBookMarked
            updateBookMarkUI(isBookMarked)
            SegmentUtils.trackEventMoreInfoBookmarkClick(isBookMarked, content = data)
            timerBookmark.cancel()
            timerBookmark = Timer()
            timerBookmark.schedule(object : TimerTask() {
                override fun run() {
                    updateBookmarkForData(AppConstants.ACTIVITY_TYPE_BOOKMARK, isBookMarked, data)
                }
            }, interval)
        }

        binding.btnBookmark.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackEventMoreInfoBookmarkFocus(isBookMarked, content = data)
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_black, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                    )
                }
            } else {
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_white, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                    )
                }
            }
        }
    }

    private fun callTestActivity(data: Content) {
        val testIntent = Intent(this, TestActivity::class.java)
        testIntent.putExtra(AppConstants.TEST_XPATH, data.xpath)
        testIntent.putExtra(AppConstants.TEST_CODE, data.bundle_id)
        testIntent.putExtra(AppConstants.CONCEPT_ID, data.learning_map.conceptId)
        testIntent.putExtra(AppConstants.TEST_NAME, data.title)
        //Utils.insertBundle(data, testIntent)
        DataManager.instance.content = data
        startActivity(testIntent)
    }

    private fun setBackground() {
        if (data.bg_thumb.isEmpty())
            setTestBlurBackgroundImage(getBackgroundUrl(data), binding.imgDescBackground)
        else
            setTestBlurBackgroundImage(data.bg_thumb, binding.imgDescBackground)
    }

    private fun getBookmarkStatus(content: Content, type: String) {
        detailsViewModel.getBookmarkStatus(content.bundle_id,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                val data = dataList.filter { bookmarkModel ->
                                    bookmarkModel.contentId == content.bundle_id
                                }

                                actionAsPerType(type, data)
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        } else {
                            isBookMarked = false
                            if (type == AppConstants.MULTI_TYPE_TEST) {
                                bannerData.testList[0].isBookmarked = false
                                setTestsRecycler(bannerData.testList as ArrayList<Content>)
                            }

                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg ")

                }
            })
    }

    private fun actionAsPerType(
        type: String,
        listBookmarkDetail: List<LikeBookmarkDetailModel>
    ) {
        when (type) {
            AppConstants.MULTI_TYPE_TEST -> {
                isBookMarked = listBookmarkDetail[0].status
                bannerData.testList[0].isBookmarked = listBookmarkDetail[0].status
                setTestsRecycler(bannerData.testList as ArrayList<Content>)
            }
            AppConstants.SINGLE_TYPE_TEST -> {
                updateBookMarkUI(listBookmarkDetail[0].status)
            }
        }
    }

    private fun updateBookMarkUI(bookmarked: Boolean) {
        isBookMarked = bookmarked
        data.isBookmarked = bookmarked

        if (binding.btnBookmark.hasFocus()) {
            if (isBookMarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        } else {
            if (isBookMarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        }
    }

    private fun setTestsRecycler(list: ArrayList<Content>) {
        testAdapter = TestsAdapter()
        currentContent = list[0]

        val layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.testsRecycler.visibility = View.VISIBLE
        binding.testsRecycler.adapter = testAdapter
        binding.testsRecycler.layoutManager = layoutManager
        testAdapter.setData(list)

        testAdapter.onItemClick = {
            callTestActivity(it)
        }

        testAdapter.onFocusChange = { content, hasFocus ->
            Log.w("app-log", "setOnFocusChange onFocusChange , $hasFocus ")
            if (hasFocus) {
                currentContent = content
                getTestDetails()
            }
        }

        testAdapter.onBookMarkClick = { content ->
            isBookMarked = !content.isBookmarked
            testAdapter.updateBookMark(isBookMarked)
//            SegmentUtils.trackEventMoreInfoBookmarkClick(isBookMarked, content = content)
            timerBookmark.cancel()
            timerBookmark = Timer()
            timerBookmark.schedule(object : TimerTask() {
                override fun run() {
                    updateBookmark(AppConstants.ACTIVITY_TYPE_BOOKMARK, isBookMarked, content)
//                    updateBookmarkForData(content, AppConstants.MULTI_TYPE_TEST)
                }
            }, interval)
        }
    }

    private fun updateBookmark(type: String, status: Boolean, content: Content) {
        testViewModel.likeBookmarkTest(content.bundle_id, content.type, type, status,
            object : BaseViewModel.APICallBacks<CommonApiResponse> {
                override fun onSuccess(model: CommonApiResponse?) {
                    if (model?.success!!) {
                        // update UI in adapter
                        testAdapter.updateBookMark(isBookMarked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (bookmarkRetryCount < retryAttempt) {
                        bookmarkRetryCount++
                        updateBookmark(type, status, content)
                        return
                    } else {
                        /*reset retry count*/
                        bookmarkRetryCount = 0
                        /*revert back to previous state of ui */
                        testAdapter.updateBookMark(!isBookMarked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_BOOKMARK)
                    }
                }
            })
    }

    private fun updateBookmarkForData(type: String, status: Boolean, content: Content) {
        testViewModel.likeBookmarkTest(
            content.bundle_id, content.type, type, status,
            object : BaseViewModel.APICallBacks<CommonApiResponse> {
                override fun onSuccess(model: CommonApiResponse?) {
                    if (model != null && model.success) {
                        Utils.isBookmarkUpdatedForTEST = true
                        updateBookMarkUI(isBookMarked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (bookmarkRetryCount < retryAttempt) {
                        bookmarkRetryCount++
                        updateBookmarkForData(type, status, content)
                        return
                    } else {
                        /*reset retry count*/
                        bookmarkRetryCount = 0
                        /*revert back to previous state of ui */
//                        updateBookMarkUI(!isBookMarked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_BOOKMARK)
                    }
                }
            })
    }

    override fun onResume() {
        super.onResume()
        getTestDetails()
    }

    private fun getTestDetails() {
        if (::currentContent.isInitialized) {
            testViewModel.getTestDetails(
                currentContent.bundle_id,
                object : BaseViewModel.APICallBacks<TestDetailsRes> {
                    override fun onSuccess(model: TestDetailsRes?) {
                        hideProgress()
                        if (model != null) {
                            updateScoreDetails(model, currentContent)
                        }
                    }

                    override fun onFailed(code: Int, error: String, msg: String) {
                    }
                })
        }
    }

    private fun updateScoreDetails(
        model: TestDetailsRes,
        content: Content
    ) {
//        val TQS = model.tqs
//        val topScore = model.topScore
//        val leastScore = model.leastScore
//        val avgScore = model.avgScore
//
//        testDetailFragment = TestDetailFragment()
//        if (isBundleData) {
//            testDetailFragment.setValue(
//                "",
//                bannerData.duration,
//                bannerData.testList[0].questions,
//                bannerData.testList[0].total_marks.toString(),
//                getRound(topScore),
//                getRound(avgScore),
//                getRound(leastScore), getRound(TQS)
//            )
//        } else {
//            testDetailFragment.setValue(
//                data.title,
//                Utils.convertIntoMinsTest(data.duration),
//                data.questions,
//                data.total_marks.toString(),
//                getRound(topScore),
//                getRound(avgScore),
//                getRound(leastScore), getRound(TQS)
//            )
//        }
//
//        val bundle = Bundle()
//        bundle.putParcelable(AppConstants.CONTENT, content)
//        testDetailFragment.arguments = bundle
//        supportFragmentManager.beginTransaction()
//            .replace(R.id.fragmentContainerView, testDetailFragment).commit()
//        setDPadKeysListener(this)
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {
    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {
    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }

    private fun capitalize(capString: String): String? {
        val capBuffer = StringBuffer()
        val capMatcher: Matcher =
            Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString)
        while (capMatcher.find()) {
            capMatcher.appendReplacement(
                capBuffer,
                capMatcher.group(1).toUpperCase(Locale.getDefault()) + capMatcher.group(2)
                    .toLowerCase(
                        Locale.getDefault()
                    )
            )
        }
        return capMatcher.appendTail(capBuffer).toString()
    }

}
