package com.embibe.embibetvapp.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import com.embibe.embibetvapp.R

class FocusRelativeLayout : RelativeLayout {
    private var scaleSmallAnimation: Animation? = null
    private var scaleBigAnimation: Animation? = null

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    private fun zoomIn() {
        if (scaleSmallAnimation == null) {
            scaleSmallAnimation = AnimationUtils.loadAnimation(
                context,
                R.anim.anim_scale_small
            )
        }
        startAnimation(scaleSmallAnimation)
    }

    private fun zoomOut() {
        if (scaleBigAnimation == null) {
            scaleBigAnimation = AnimationUtils.loadAnimation(
                context,
                R.anim.anim_scale_big
            )
        }
        startAnimation(scaleBigAnimation)
    }
}