package com.embibe.embibetvapp.ui.fragment.search

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.databinding.FragmentSearchDetailBinding
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.ui.adapter.ParentDetailAdapter
import com.embibe.embibetvapp.ui.fragment.search.SearchDetailFragment.Values.TAG
import com.embibe.embibetvapp.ui.interfaces.SearchDetailToTvKeysListener
import com.embibe.embibetvapp.ui.interfaces.SearchResultsListener
import com.embibe.embibetvapp.ui.viewmodel.SearchViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main

class SearchDetailFragment : BaseAppFragment() {

    private lateinit var binding: FragmentSearchDetailBinding
    private lateinit var searchDetailToTvKeysListener: SearchDetailToTvKeysListener
    private lateinit var searchResultsListener: SearchResultsListener
    private lateinit var searchViewModel: SearchViewModel
    private lateinit var parentDetailAdapter: ParentDetailAdapter

    private var arrayResults: ArrayList<Results> = ArrayList()
    private var hasResults: Boolean = false
    private var quickLinkToSearch = false

    object Values {
        const val DELAY = 2L
        val TAG = this.javaClass.name
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_search_detail, container, false)
        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)

        val bundle = this.arguments
        if (bundle != null) {
            arrayResults = bundle.getParcelableArrayList<Results>("results") as ArrayList<Results>
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setDetailRecycler(view, arrayResults)
    }

    private fun setDetailRecycler(view: View, result: ArrayList<Results>) {

        parentDetailAdapter = ParentDetailAdapter(view.context)
        binding.detailRecycler.layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        binding.detailRecycler.adapter = parentDetailAdapter
        parentDetailAdapter.setData(result)

        if (parentDetailAdapter.adapterList.size > 0) {
            if (quickLinkToSearch) {
                setFocusToDetailRv()
            }
        } else
            quickLinkToSearch = false

        if (::searchResultsListener.isInitialized)
            searchResultsListener.searchResultsCount(result.size)

        parentDetailAdapter.onDapUpHit = { focusFlag ->
            searchDetailToTvKeysListener.forceFocusToKeys(focusFlag)
        }

        parentDetailAdapter.onDpadDownHit = { bool ->
            if (bool) {
                /*Since focus from first RV to second Rv was not happening
                           for the first time , so secondItemView would be initialized with
                           second Rv and when DpadDown is hit from first Rv focus would shift
                           from First Rv to Second Rv in a separate coroutine thread
                           *
                           * */
                if (result.size > 0) {
                    var scrollPosition = 0
                    var scrollFlag = false

                    for (resultIndex in 0 until result.size) {
                        if (resultIndex != 0) {
                            if (result[resultIndex].content.isNotEmpty()) {
                                scrollPosition = resultIndex
                                scrollFlag = true
                                break
                            }
                        }
                    }

                    if (scrollFlag) {
                        binding.detailRecycler.scrollToPosition(scrollPosition)
                        GlobalScope.launch(Dispatchers.Main) {
                            delay(Values.DELAY)
                            try {
                                var viewHolder =
                                    binding.detailRecycler
                                        .findViewHolderForAdapterPosition(scrollPosition)
                                viewHolder!!.itemView.requestFocus()
                            } catch (e: Exception) {
                                Log.e(TAG, "${e.message}")
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SearchDetailToTvKeysListener) {
            searchDetailToTvKeysListener = context
        }
    }

    fun clearAdapter() {
        if (::parentDetailAdapter.isInitialized)
            parentDetailAdapter.clearList()
    }

    fun setFocusToDetailRv() {
        CoroutineScope(Main).launch {
            delay(2)
            binding.detailRecycler.requestFocus()
        }
    }

    fun setFlagToFocusSearch() {
        quickLinkToSearch = true
    }

    fun setSearchResultsListener(callback: SearchResultsListener) {
        this.searchResultsListener = callback
    }

    fun setSearchDetailToTvListener(callback: SearchDetailToTvKeysListener) {
        searchDetailToTvKeysListener = callback
    }
}
