package com.embibe.embibetvapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import com.embibe.app.utils.Resource
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.createtest.configuration.TestConfigurationRes
import com.embibe.embibetvapp.model.createtest.examconfig.request.ExamConfigRequest
import com.embibe.embibetvapp.model.createtest.examconfig.response.ChapterImportance
import com.embibe.embibetvapp.model.createtest.examconfig.response.TestDetails
import com.embibe.embibetvapp.network.repo.CreateTestRepository
import com.embibe.embibetvapp.utils.Status

class CreateNewTestViewModel
    : BaseViewModel() {
    val subjectsLiveData: MutableLiveData<Resource<Any>> = MutableLiveData()
    var repo = CreateTestRepository()
    val examConfigLiveData: MutableLiveData<Resource<Any>> = MutableLiveData()

    fun getExamConfig() {
        var examConfigRequest = ExamConfigRequest(
            UserData.getExamName().toLowerCase(),
            AppConstants.NAMESPACE,
            AppConstants.VERSION,
            AppConstants.SUBJECT
        )

        safeApi(
            repo.getExamConfigDetails(examConfigRequest),
            object : BaseViewModel.APICallBacks<TestDetails> {
                override fun onSuccess(model: TestDetails?) {
                    var subjectDetails = repo.getSubjectDetailsFromResponse(model!!)
                    getSubjectDetailsConfig()

                    subjectsLiveData.value =
                        Resource(Status.SUCCESS, subjectDetails)
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    subjectsLiveData.value =
                        Resource(
                            Status.ERROR, null,
                            msg
                        )
                }
            })
    }

    fun getSubjectDetailsConfig() {
        var examConfigRequest = ExamConfigRequest(
            UserData.getExamName().toLowerCase(),
            AppConstants.NAMESPACE,
            AppConstants.VERSION,
            AppConstants.CHAPTER
        )

        safeApi(
            repo.getExamConfigDetailsForChapter(examConfigRequest),
            object : BaseViewModel.APICallBacks<ChapterImportance> {
                override fun onSuccess(model: ChapterImportance?) {
                    var subjectDetails = repo.getChapterDetails(model!!)
                    examConfigLiveData.value =
                        Resource(Status.SUCCESS, subjectDetails)

                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    examConfigLiveData.value =
                        Resource(
                            Status.ERROR, null,
                            msg
                        )
                }
            })
    }

    fun loadTestConfigurations(
        callback: BaseViewModel.APICallBacks<TestConfigurationRes>
    ) {
        safeApi(repo.getTestConfigurations(), callback)
    }


}