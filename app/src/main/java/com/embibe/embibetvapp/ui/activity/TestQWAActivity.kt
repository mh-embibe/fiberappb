package com.embibe.embibetvapp.ui.activity

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.ALL_ATTEMPTS
import com.embibe.embibetvapp.constant.AppConstants.ALL_SUBJECTS
import com.embibe.embibetvapp.constant.AppConstants.QWA_ATTEMPT_TYPE.*
import com.embibe.embibetvapp.constant.AppConstants.QWA_Modes.detail
import com.embibe.embibetvapp.constant.AppConstants.QWA_Modes.list
import com.embibe.embibetvapp.databinding.ActivityTestQwsBinding
import com.embibe.embibetvapp.model.test.Question
import com.embibe.embibetvapp.model.test.TestAttemptsRes
import com.embibe.embibetvapp.model.test.TestQuestionResponse
import com.embibe.embibetvapp.model.test_feedback_scatter_chart.XYModel
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.interfaces.CommonAndroidAPI
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.getDrawableByAttemptType
import com.embibe.embibetvapp.utils.Utils.getIconByAttemptType
import com.embibe.embibetvapp.utils.Utils.getIconByAttemptTypeNew
import com.embibe.embibetvapp.utils.VideoUtils
import com.embibe.embibetvapp.utils.data.DataManager
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class TestQWAActivity : ScatterGraphTestFeedBackActivity(),
    CommonAndroidAPI.AndroidAPIOnCallListener {

    private lateinit var binding: ActivityTestQwsBinding

    private lateinit var testAttemptsRes: TestAttemptsRes
    private lateinit var testQuestionRes: TestQuestionResponse
    private lateinit var testQuestion: Question
    private lateinit var attemptType: String
    private var itemHighlight: XYModel? = null

    private var mode = detail
    private var attemptCount = 0
    private var pos = 0
    private var currentQWASubject = ""
    private var currentQuestionList = ArrayList<Question>()
    val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)

    companion object {
        val TAG = this.javaClass.name
        var xAxisLabelSize = 0.0

        fun getQWAIntent(
            context: Context,
            pos: Int, currentSubject: String, attemptType: String
        ): Intent {
            return Intent(context, TestQWAActivity::class.java)
                .putExtra("pos", pos)
                .putExtra("currentSubject", currentSubject)
                .putExtra("attemptType", attemptType)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_test_qws)
        with(DataManager.instance) {
            testAttemptsRes = Gson().fromJson(getTestAttempts(), TestAttemptsRes::class.java)
            testQuestionRes =
                Gson().fromJson(getTestQuestion(), TestQuestionResponse::class.java)
            testQuestion =
                Gson().fromJson(getCurrentQuestion(), Question::class.java)
            pos = intent.getIntExtra("pos", 0)
        }
        attemptType = intent.getStringExtra("attemptType")
        currentQWASubject = intent.getStringExtra("currentSubject")

        binding.constraintTitle.requestFocus()

        updateTitleComponent()
        updateClickToWatchComponent()
        setPreviousFocusListener()
        setNextFocusListener()
        currentQuestionList = getQuestionListData(currentQWASubject, attemptType)
        initScatter(
            testAttemptsRes,
            attemptType,
            getIconByAttemptTypeNew(attemptType),
            currentQWASubject,
            currentQuestionList,
            getQListData(currentQWASubject, ALL_ATTEMPTS)
        )

        updateLayoutBasedOnAttemptCount()
        setNoAttempt(attemptType)
        setQWAButtonListener()
        setWebView()
        setMode()
        setCurrentQuestion(pos, false)
        setFocusAttempts()
    }

    private fun setFocusAttempts() {
        binding.tvDropDownSubject.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (mode == list) {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWA.postDelayed({
                                    binding.layoutPractice.wvQWA.requestFocus()
                                }, 10)
                            }
                        } else {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWADetail.postDelayed({
                                    binding.layoutPractice.wvQWADetail.requestFocus()
                                }, 10)
                            }
                        }
                        return@setOnKeyListener true
                    }
                }
            }
            false
        }
        binding.btnPerfectAttempts.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (mode == list) {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWA.postDelayed({
                                    binding.layoutPractice.wvQWA.requestFocus()
                                }, 10)
                            }
                        } else {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWADetail.postDelayed({
                                    binding.layoutPractice.wvQWADetail.requestFocus()
                                }, 10)
                            }
                        }
                        return@setOnKeyListener true
                    }
                }
            }
            false
        }
        binding.btnInCorrectAttempts.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (mode == list) {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWA.postDelayed({
                                    binding.layoutPractice.wvQWA.requestFocus()
                                }, 10)
                            }
                        } else {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWADetail.postDelayed({
                                    binding.layoutPractice.wvQWADetail.requestFocus()
                                }, 10)
                            }
                        }
                        return@setOnKeyListener true
                    }
                }
            }
            false
        }
        binding.btnOverTimeCorrectAttempts.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (mode == list) {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWA.postDelayed({
                                    binding.layoutPractice.wvQWA.requestFocus()
                                }, 10)
                            }
                        } else {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWADetail.postDelayed({
                                    binding.layoutPractice.wvQWADetail.requestFocus()
                                }, 10)
                            }
                        }
                        return@setOnKeyListener true
                    }
                }
            }
            false
        }
        binding.btnTooFastCorrectAttempts.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (mode == list) {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWA.postDelayed({
                                    binding.layoutPractice.wvQWA.requestFocus()
                                }, 10)
                            }
                        } else {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWADetail.postDelayed({
                                    binding.layoutPractice.wvQWADetail.requestFocus()
                                }, 10)
                            }
                        }
                        return@setOnKeyListener true
                    }
                }
            }
            false
        }
        binding.btnOverTimeInCorrectAttempts.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (mode == list) {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWA.postDelayed({
                                    binding.layoutPractice.wvQWA.requestFocus()
                                }, 10)
                            }
                        } else {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWADetail.postDelayed({
                                    binding.layoutPractice.wvQWADetail.requestFocus()
                                }, 10)
                            }
                        }
                        return@setOnKeyListener true
                    }
                }
            }
            false
        }
        binding.btnWastedAttempts.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (mode == list) {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWA.postDelayed({
                                    binding.layoutPractice.wvQWA.requestFocus()
                                }, 10)
                            }
                        } else {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWADetail.postDelayed({
                                    binding.layoutPractice.wvQWADetail.requestFocus()
                                }, 10)
                            }
                        }
                        return@setOnKeyListener true
                    }
                }
            }
            false
        }
        binding.btnUnAttempted.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (mode == list) {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWA.postDelayed({
                                    binding.layoutPractice.wvQWA.requestFocus()
                                }, 10)
                            }
                        } else {
                            runOnUiThread {
                                currentFocus?.clearFocus()
                                binding.layoutPractice.wvQWADetail.postDelayed({
                                    binding.layoutPractice.wvQWADetail.requestFocus()
                                }, 10)
                            }
                        }
                        return@setOnKeyListener true
                    }
                }
            }
            false
        }
    }

    private fun setMode() {
        runOnUiThread {
            if (mode == detail) {
                binding.layoutPractice.wvQWADetail.visibility = View.VISIBLE
                binding.layoutPractice.clNavigation.visibility = View.VISIBLE
                binding.layoutPractice.wvQWA.visibility = View.GONE
                binding.layoutPractice.arrowNext.setOnClickListener {
                    if (pos < currentQuestionList.size - 1) {
                        pos += 1
                        setCurrentQuestion(pos, true)
                    } else {
                        pos = 0
                        setCurrentQuestion(pos, true)
                    }
                }
                binding.layoutPractice.arrowPrevious.setOnClickListener {
                    if (pos > 0) {
                        pos -= 1
                        setCurrentQuestion(pos, true)
                    } else {
                        pos = currentQuestionList.size - 1
                        setCurrentQuestion(pos, true)
                    }
                }

            } else {
                binding.layoutPractice.wvQWADetail.visibility = View.GONE
                binding.layoutPractice.clNavigation.visibility = View.GONE
                binding.layoutPractice.wvQWA.visibility = View.VISIBLE
            }
        }


    }

    private fun setCurrentQuestion(pos: Int, loadQuestion: Boolean) {
        testQuestion = currentQuestionList[pos]
        if (loadQuestion)
            setQuestion()
        binding.layoutPractice.tvNumber.text = resources.getString(R.string._3_23)
            .replace("*current*", (pos + 1).toString())
            .replace("*total*", currentQuestionList.size.toString())
        binding.layoutPractice.imgPractice.setImageResource(getIconByAttemptTypeNew(attemptType))
    }

    private fun setWebView() {
        setQWADetailWebView()
        setQWAListWebView()
        setDropDownQWA()
    }


    private fun updateTitleComponent() {
        binding.tvType.text = getString(R.string.showing_xx_wasted_attempts_for_selected_subject)
            .replace(
                "*attempt_count*",
                getQuestionListData(currentQWASubject, attemptType).size.toString()
            )
            .replace("*attempts*", Utils.getAttemptFromKey(attemptType))
            .replace(
                "*subject*",
                currentQWASubject
            )

//        Glide.with(this).setDefaultRequestOptions(requestOptions)
//            .load(getDrawableByAttemptType(attemptType))
//            .transition(DrawableTransitionOptions.withCrossFade())
//            .transform(BlurTransformation(App.context))
//            .transform(RoundedCorners(20))
//            .into(binding.textBg)


        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(this).setDefaultRequestOptions(requestOptions)
            .load(getDrawableByAttemptType(attemptType))
            .transition(DrawableTransitionOptions.withCrossFade())
            .transform(RoundedCorners(16))
            .into(binding.ivBg)

//        binding.textBg.setBlur(20)
        binding.tvDropDownSubject.text =
            testAttemptsRes.scoreListInfo!!.sectionSummaryList!![0].name.toString()
    }

    private fun getAttemptCount(testAttemptsRes: TestAttemptsRes): Int {
        var count = 0

        for (index in testAttemptsRes.attempts!!.indices) {
            if (testAttemptsRes.attempts!![index].attemptTypeBadge == attemptType) {
                count++
            }
        }

        attemptCount = count
        return count
    }

    @SuppressLint("SetTextI18n")
    private fun updateClickToWatchComponent() {

        binding.layoutClickToWatch.textClickToWatch.text = getString(R.string.click_to_understand)
            .replace("*attempts*", attemptType.toLowerCase(Locale.getDefault()))

        binding.layoutClickToWatch.textEmbiums.text =
            "${getString(R.string.earn)} ${testAttemptsRes.actionReplay?.embium}"

        Glide.with(this).setDefaultRequestOptions(requestOptions)
            .load(R.drawable.video_attempts)
            .transition(DrawableTransitionOptions.withCrossFade())
            .transform(RoundedCorners(20))
            .into(binding.layoutClickToWatch.ivThumbnail)
    }

    private fun updateLayoutBasedOnAttemptCount() {
        attemptCount = getAttemptCount(testAttemptsRes)
        if (attemptCount != 0) {
            binding.layoutPractice.layoutQWPractice.visibility = View.VISIBLE
            binding.layoutNoAttempts.constraintNoAttempt.visibility = View.GONE
            binding.scatterChartContainer.visibility = View.VISIBLE
        } else {
            binding.layoutNoAttempts.constraintNoAttempt.visibility = View.VISIBLE
            binding.layoutPractice.layoutQWPractice.visibility = View.GONE
            binding.scatterChartContainer.visibility = View.GONE
        }

    }

    private fun setNoAttempt(attemptType: String) {
        binding.layoutNoAttempts.imgAttempt.setImageResource(getIconByAttemptType(attemptType))
        binding.layoutNoAttempts.textAttempt.text = getString(R.string.you_do_not_have_any_n)
            .replace("*attempts*", attemptType)
    }

    private fun setQuestionFocus() {
        if (itemHighlight != null) {
            deHighlightPlotView(itemHighlight!!.view, itemHighlight!!)
        }
        itemHighlight = getItem(testQuestion.code)
        if (itemHighlight != null)
            highlightPlotView(itemHighlight!!.view, itemHighlight!!)
        //highlightLightFocusView(itemHighlight!!)
        //itemHighlight!!.view.requestFocus()

    }

    private fun setQWAButtonListener() {

        binding.btnPerfectAttempts.setOnClickListener {
            setQuestionListWithType(Perfect.name)
        }
        binding.btnOverTimeCorrectAttempts.setOnClickListener {
            setQuestionListWithType(OvertimeCorrect.name)
        }
        binding.btnTooFastCorrectAttempts.setOnClickListener {
            setQuestionListWithType(TooFastCorrect.name)
        }
        binding.btnInCorrectAttempts.setOnClickListener {
            setQuestionListWithType(Incorrect.name)
        }
        binding.btnOverTimeInCorrectAttempts.setOnClickListener {
            setQuestionListWithType(OvertimeIncorrect.name)
        }
        binding.btnWastedAttempts.setOnClickListener {
            setQuestionListWithType(Wasted.name)
        }
        binding.btnUnAttempted.setOnClickListener {
            setQuestionListWithType(NonAttempt.name)
        }
    }

    private fun setQuestionListWithType(type: String) {
        mode = list
        attemptType = type
        setMode()
        val questionList = getQuestionListData(currentQWASubject, type)
        if (questionList.size > 0) {
            setQuestionList(questionList)
        } else
            setNoQuestionState()
        initScatter(
            testAttemptsRes,
            type,
            getIconByAttemptType(type),
            currentQWASubject,
            getQuestionListData(currentQWASubject, type),
            getQListData(currentQWASubject, ALL_ATTEMPTS)
        )
        updateTitleComponent()
    }

    private fun getQuestionListData(
        subject: String,
        type: String
    ): ArrayList<Question> {
        val qlist = ArrayList<Question>()
        for (key in testQuestionRes.paper.sections.keys) {
            val section = testQuestionRes.paper.sections[key]
            if (section!!.name == subject || subject == ALL_SUBJECTS) {
                for (question in section.questions) {
                    if (question.attemptTypeBadge == type) {
                        qlist.add(question)
                    }
                }
            }
        }
        return qlist
    }

    private fun getQListData(
        subject: String,
        type: String
    ): ArrayList<Question> {
        val qlist = ArrayList<Question>()
        for (key in testQuestionRes.paper.sections.keys) {
            val section = testQuestionRes.paper.sections[key]
            if (section!!.name == subject || subject == ALL_SUBJECTS) {
                for (question in section.questions) {
                    if (question.attemptTypeBadge == type || type == ALL_ATTEMPTS) {
                        qlist.add(question)
                    }
                }
            }
        }
        return qlist
    }


    private fun setPreviousFocusListener() {
        binding.layoutPractice.arrowPrevious.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.layoutPractice.arrowPrevious.background =
                    getDrawable(R.drawable.ic_selected_left)
            } else {
                binding.layoutPractice.arrowPrevious.background =
                    getDrawable(R.drawable.ic_unselected_left)
            }
        }
    }

    private fun setNextFocusListener() {
        binding.layoutPractice.arrowNext.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.layoutPractice.arrowNext.background =
                    getDrawable(R.drawable.ic_selected_right)
            } else {
                binding.layoutPractice.arrowNext.background =
                    getDrawable(R.drawable.ic_unselected_right)
            }
        }
    }

    private fun setQWAListWebView() {
        binding.layoutPractice.wvQWA.webChromeClient = WebChromeClient()
        binding.layoutPractice.wvQWA.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                Log.d("WEBVIEW", "Web view ready")

            }
        }
        binding.layoutPractice.wvQWA.settings.allowFileAccess = true
        binding.layoutPractice.wvQWA.isLongClickable = false
        binding.layoutPractice.wvQWA.settings.javaScriptEnabled = true
        binding.layoutPractice.wvQWA.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        binding.layoutPractice.wvQWA.settings.domStorageEnabled = true
        binding.layoutPractice.wvQWA.setBackgroundColor(Color.argb(0, 0, 0, 0))
        binding.layoutPractice.wvQWA.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        binding.layoutPractice.wvQWA.addJavascriptInterface(
            CommonAndroidAPI(this),
            CommonAndroidAPI.NAME
        )
        binding.layoutPractice.wvQWA.setLayerType(View.LAYER_TYPE_HARDWARE, null)

        binding.layoutPractice.wvQWA.loadUrl("http://52.172.137.100:8000/?&invokeModule=question_list")
        binding.layoutPractice.wvQWA.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        Log.d("KEYCODE_DPAD_RIGHT", "KEYCODE_DPAD_RIGHT")
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        Log.d("KEYCODE_DPAD_UP", "KEYCODE_DPAD_UP")
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        Log.d("KEYCODE_DPAD_LEFT", "KEYCODE_DPAD_LEFT")
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        Log.d("KEYCODE_DPAD_DOWN", "KEYCODE_DPAD_DOWN")
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {

                    }
                }
            }

            false
        }
    }

    private fun setQWADetailWebView() {
        binding.layoutPractice.wvQWADetail.webChromeClient = WebChromeClient()
        binding.layoutPractice.wvQWADetail.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                setQuestion(false)
            }
        }
        binding.layoutPractice.wvQWADetail.settings.allowFileAccess = true
        binding.layoutPractice.wvQWADetail.isLongClickable = false
        binding.layoutPractice.wvQWADetail.settings.javaScriptEnabled = true
        binding.layoutPractice.wvQWADetail.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        binding.layoutPractice.wvQWADetail.settings.domStorageEnabled = true
        binding.layoutPractice.wvQWADetail.setBackgroundColor(Color.argb(0, 0, 0, 0))
        binding.layoutPractice.wvQWADetail.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        binding.layoutPractice.wvQWADetail.addJavascriptInterface(
            CommonAndroidAPI(this),
            CommonAndroidAPI.NAME
        )
        binding.layoutPractice.wvQWADetail.setLayerType(View.LAYER_TYPE_HARDWARE, null)

        binding.layoutPractice.wvQWADetail.loadUrl("http://52.172.137.100:8000/?&invokeModule=test_feedback")
        binding.layoutPractice.wvQWADetail.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        Log.d("KEYCODE_DPAD_RIGHT", "KEYCODE_DPAD_RIGHT")
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        Log.d("KEYCODE_DPAD_UP", "KEYCODE_DPAD_UP")
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        Log.d("KEYCODE_DPAD_LEFT", "KEYCODE_DPAD_LEFT")
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        Log.d("KEYCODE_DPAD_DOWN", "KEYCODE_DPAD_DOWN")
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {

                    }
                }
            }

            false
        }
    }

    private fun setQuestion(bool: Boolean = true) {
        runOnUiThread {
            binding.layoutPractice.tvNumber.text = resources.getString(R.string._3_23)
                .replace("*current*", (pos + 1).toString())
                .replace("*total*", currentQuestionList.size.toString())
            var data = Gson().toJson(testQuestion)
            if (bool) {
                currentFocus?.clearFocus()
                binding.layoutPractice.wvQWADetail.postDelayed({
                    binding.layoutPractice.wvQWADetail.requestFocus()
                }, 10)

            }
            binding.layoutPractice.wvQWADetail.evaluateJavascript(
                "commonWebviewAPI.setQuestion($data)",
                null
            )
            setQuestionFocus()
        }
    }

    override fun focusUp() {
        super.focusUp()
        if (mode == list) {
            runOnUiThread {
                currentFocus?.clearFocus()
                binding.layoutPractice.arrowPrevious.postDelayed({
                    binding.layoutPractice.arrowPrevious.requestFocus()
                }, 10)
            }
        } else {
            runOnUiThread {
                currentFocus?.clearFocus()
                binding.tvDropDownSubject.postDelayed({
                    binding.tvDropDownSubject.requestFocus()
                }, 10)
            }
        }
    }

    override fun focusLeft() {
        super.focusLeft()
        if (mode == list) {
            runOnUiThread {
                currentFocus?.clearFocus()
                binding.tvDropDownSubject.postDelayed({
                    binding.tvDropDownSubject.requestFocus()
                }, 10)
            }
        } else {
            runOnUiThread {
                currentFocus?.clearFocus()
                binding.tvDropDownSubject.postDelayed({
                    binding.tvDropDownSubject.requestFocus()
                }, 10)
            }
        }

    }

    override fun getSelectedQuestion(qListJson: String) {
        runOnUiThread {
            try {
                Log.i("selected Position", qListJson)
                val json = JSONObject(qListJson)
                var pos = json.getInt("pos")
                val qData = json.getJSONObject("data")
                testQuestion = Gson().fromJson(qData.toString(), Question::class.java)
                this.pos = pos
                mode = detail
                setMode()
                setQuestion()
                binding.layoutPractice.clNavigation.visibility = VISIBLE
                binding.layoutPractice.noQuestionPlaceholder.visibility = GONE
                binding.layoutPractice.wvQWA.visibility = View.GONE
                binding.layoutPractice.wvQWADetail.visibility = View.VISIBLE
            } catch (exception: JSONException) {
            }
        }
    }

    override fun setQuestion(singQuestionJson: String) {

    }

    override val singQuestionJson: String?
        get() = ""
    override val questionListJson: String?
        get() = ""

    private fun setDropDownQWA() {
        currentQWASubject = intent.getStringExtra("currentSubject")!!
        binding.tvDropDownSubject.text = currentQWASubject
        if (getSubjectList().size > 1) {
            binding.tvDropDownSubject.setOnClickListener {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Choose Subject")
                val array = getSubjectList().toTypedArray()
                builder.setItems(
                    array
                ) { dialog, which ->
                    if (getSubjectList()[which].toLowerCase(Locale.getDefault())
                            .contains(ALL_SUBJECTS)
                    ) {
                        currentQWASubject = getSubjectList()[which]
                        binding.tvDropDownSubject.text = currentQWASubject
                        // setQuestionList(getQuestionListData(currentQWASubject))
                    } else {
                        currentQWASubject = getSubjectList()[which]
                        binding.tvDropDownSubject.text = currentQWASubject
                        // setQuestionList(getQuestionListData(currentQWASubject))
                    }
                }

                val dialog = builder.create()
                dialog.show()
            }
        }
    }

    private fun setNoQuestionState() {
        binding.layoutPractice.clNavigation.visibility = GONE
        binding.layoutPractice.noQuestionPlaceholder.visibility = VISIBLE
        binding.layoutPractice.wvQWA.visibility = View.GONE
        binding.layoutPractice.wvQWADetail.visibility = View.GONE
        binding.layoutPractice.imgAttemptBadge.setImageResource(Utils.getAttemptBadge(attemptType))
        binding.layoutPractice.tvAttemptBadge.text =
            resources.getString(R.string.you_do_not_have_any_attempttype_attempts)
                .replace("*attemptType*", attemptType)
    }

    private fun setQuestionList(qlist: ArrayList<Question>) {
        currentQuestionList = qlist
        val data = Gson().newBuilder().serializeNulls().create()
            .toJson(qlist)
        Log.i("json data", data)
        binding.layoutPractice.clNavigation.visibility = GONE
        binding.layoutPractice.wvQWA.visibility = View.VISIBLE
        binding.layoutPractice.wvQWADetail.visibility = View.GONE
        binding.layoutPractice.noQuestionPlaceholder.visibility = GONE
        binding.layoutPractice.wvQWA.evaluateJavascript(
            "commonWebviewAPI.setQuestionList($data)",
            null
        )
    }

    private fun getSubjectList(): List<String> {
        val sliest = ArrayList<String>()
        for (key in testQuestionRes.paper.sections.keys) {
            val section = testQuestionRes.paper.sections[key]
            sliest.add(section!!.name)
        }
        if (sliest.size > 1) {
            sliest.add(0, ALL_SUBJECTS)
        }
        return sliest
    }

    override fun getSelectedVideo(videoData: String) {
        Log.i("video data", videoData)
        var obj = JSONObject(videoData)
        var data = obj.getJSONObject("data")
        val gson = Gson()
        val videoContent: Content =
            gson.fromJson(data.toString(), Content::class.java)
        var duration = ""
        var url = ""
        var source = ""
        var id = ""
        var title = ""
        var conceptId = ""
        doAsync {
            val gson = Gson()
            val videoContent: Content =
                gson.fromJson(data.toString(), Content::class.java)
            id = videoContent.id
            conceptId = videoContent.learning_map.conceptId
            url = videoContent.url
            title = videoContent.title
            source = Utils.getVideoTypeUsingUrl(url)
            duration = videoContent.duration.toString()
            val formatId =videoContent.format_id
            val learnPathName =videoContent.learnpath_name
            val learnPathFormatName =videoContent.learnpath_format_name
            uiThread {
                hideProgress()
                callVideo(
                    id,
                    AppConstants.VIDEO,
                    conceptId,
                    url,
                    title,
                    "",
                    source.toLowerCase(),
                    "",
                    "",
                    "",
                    duration,
                    "",
                    learnPathName,
                    learnPathFormatName,
                    formatId, videoContent
                )
            }
        }


    }

    private fun callVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        duration: String,
        topicLearningPath: String,
        learnPathName: String,
        learnPathFormatName: String,
        formatId: String,
        content: Content
    ) {
        if (source == AppConstants.PLAYER_TYPE_YOUTUBE) {
            if (Utils.isYouTubeAppAvailable(this)) {
                Utils.startYoutubeApp(this, url)
            } else {
                startVideo(
                    id,
                    type,
                    conceptId,
                    url,
                    title,
                    description,
                    source,
                    subject,
                    chapter,
                    authors,
                    duration,
                    topicLearningPath,
                    learnPathName, learnPathFormatName, formatId
                )
            }
        } else {
            val callback = object : BaseViewModel.APICallBackVolley {
                override fun <T> onSuccessCallBack(response: T) {
                    hideProgress()
                    response as JSONObject
                    val playableUrl = Utils.getVimeoHD((response["files"] as JSONArray))
                    startVideo(
                        id,
                        type,
                        conceptId,
                        playableUrl,
                        title,
                        description,
                        AppConstants.PLAYER_TYPE_EXOPLAYER,
                        subject,
                        chapter,
                        authors,
                        duration,
                        topicLearningPath,
                        learnPathName, learnPathFormatName, formatId
                    )
                    /*hide ProgressBar here */

                }

                override fun onErrorCallBack(e: VolleyError) {
                    try {
                        makeLog("Error:  $e")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    /*hide ProgressBar here */
                    hideProgress()
                }
            }
            showProgress()
            val videoId = VideoUtils.getVimeoVideoId(url).toString()
            Utils.getVideoURL(this, videoId, content.owner_info.key, callback)
        }
    }

    fun startVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        totalDuration: String,
        topicLearningPath: String,
        learnPathName: String,
        learnPathFormatName: String,
        formatId: String
    ) {
        val intent = Intent(App.context, VideoPlayerActivity::class.java)

        intent.putExtra(AppConstants.VIDEO_ID, id)
        intent.putExtra(AppConstants.CONTENT_TYPE, type)
        intent.putExtra(AppConstants.VIDEO_CONCEPT_ID, conceptId)
        intent.putExtra(AppConstants.VIDEO_URL, url)
        intent.putExtra(AppConstants.VIDEO_TITLE, title)
        intent.putExtra(AppConstants.VIDEO_DESCRIPTION, description)
        intent.putExtra(AppConstants.VIDEO_SUBJECT, subject)
        intent.putExtra(AppConstants.VIDEO_CHAPTER, chapter)
        intent.putExtra(AppConstants.VIDEO_TOTAL_DURATION, totalDuration)
        intent.putExtra(AppConstants.VIDEO_AUTHORS, authors)
        intent.putExtra(AppConstants.SOURCE_REFERENCE, AppConstants.BOOK)
        intent.putExtra(AppConstants.TOPIC_LEARN_PATH, topicLearningPath)
        intent.putExtra(AppConstants.VIDEO_SOURCE, source)
        intent.putExtra(AppConstants.FORMAT_ID, formatId)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, learnPathName)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, learnPathFormatName)
        startActivity(intent)
    }
}
