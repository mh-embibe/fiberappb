package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemPotentialPerformanceBinding
import com.embibe.embibetvapp.model.Performance

class PotentialAdapter(var context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var binding: ItemPotentialPerformanceBinding
    var list = ArrayList<Performance>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_potential_performance,
            parent,
            false
        )
        return RecyclerViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (list.size > 0) list.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as RecyclerViewHolder
        viewHolder.bind(list[position])
    }

    fun setData(list: ArrayList<Performance>) {
        this.list = list
    }


    inner class RecyclerViewHolder internal constructor(var binding: ItemPotentialPerformanceBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Performance) {
            Glide.with(context).load(item.image).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.ivPotential)
            binding.tvPotential.text = item.message
            var animView = binding.lvCircularSphere
            animView.setAnimation(R.raw.trophy_glow)
            animView.playAnimation()
        }
    }
}