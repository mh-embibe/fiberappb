package com.embibe.embibetvapp.ui.presenter

import android.content.Context
import androidx.leanback.widget.Presenter
import androidx.leanback.widget.PresenterSelector
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener

/**
 * This PresenterSelector will decide what Presenter to use depending on a given card's type.
 */

class CardPresenterSelector(private val mContext: Context) : PresenterSelector(), DPadKeysListener {

    private val presenters = HashMap<String, Presenter>()
    private lateinit var dPadKeysCallback: DPadKeysListener

    override fun getPresenter(item: Any?): Presenter? {
        when (item) {
            is Content -> {
                var presenter = presenters[item.type]
                if (presenter == null) {
                    presenter = when (item.type.toLowerCase()) {
                        AppConstants.SUBJECT -> SubjectCardPresenter(mContext)
                        AppConstants.BANNER, AppConstants.AD_BANNER -> {
                            val bannerCardPresenter = BannerCardPresenter(mContext)
                            bannerCardPresenter.setDPadKeysListener(this)
                            return bannerCardPresenter
                        }
                        AppConstants.BOOK -> BooksPresenter(mContext)
                        AppConstants.STORY -> StoryWorldPresenter(mContext)
                        AppConstants.LEARNING_VIDEOS ->
                            BasicCardPresenter(mContext, AppConstants.LEARNING_VIDEOS)
                        AppConstants.PRACTICE, AppConstants.CHAPTER ->
                            PracticeCardPresenter(mContext)
                        AppConstants.LEARN ->
                            BasicCardPresenter(mContext, AppConstants.LEARN)
                        AppConstants.LEARN_CHAPTER ->
                            BasicCardPresenter(mContext, AppConstants.LEARN_CHAPTER)
                        AppConstants.TEST ->
                            TestCardPresenter(mContext, AppConstants.TEST)
                        AppConstants.CREATE_TEST ->
                            CreateOwnTestCardPresenter(mContext, AppConstants.TEST)
                        AppConstants.VIDEO -> {
                            if (item.watched_duration == 0)
                                GifBasicCardPresenter(mContext, AppConstants.VIDEO)
                            else
                                BasicCardPresenter(mContext, AppConstants.TEST)
                        }
                        AppConstants.COOBO -> BasicCardPresenter(mContext, "")
                        else ->
                            BasicCardPresenter(mContext, "")
                    }
                }
                presenters[item.type] = presenter
                return presenter
            }

            is com.embibe.embibetvapp.model.achieve.home.Content -> {
                var presenter = presenters[item.type]
                if (presenter == null) {
                    presenter = when (item.type.toLowerCase()) {
                        AppConstants.SUBJECT -> PAJSubjectCardPresenter(mContext)
                        AppConstants.PAJ -> ActiveAchievmentCardPresenter(mContext)
                        AppConstants.TEST -> TestPAJCardPresenter(mContext)
                        else -> ActiveAchievmentCardPresenter(mContext)
                    }

                }
                presenters[item.type] = presenter
                return presenter
            }
            else -> return null
        }
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {}

    override fun isKeyPadUp(isUp: Boolean, from: String) {}

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
        when (from) {
            "BannerCardPresenter" -> {
                dPadKeysCallback.isKeyPadLeft(true, "CardPresenterSelector")
            }
        }
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {}

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {
        if (isEnter) {
            when (from) {
                "BannerCardPresenter" -> {
                    dPadKeysCallback.isKeyPadEnter(true, "CardPresenterSelector", cardType)
                }
            }
        }
    }
}






