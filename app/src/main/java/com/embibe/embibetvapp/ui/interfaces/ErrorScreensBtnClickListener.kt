package com.embibe.embibetvapp.ui.interfaces

interface ErrorScreensBtnClickListener {
    fun screenUpdate(type: String)
}