package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemSubjectSelectTestSettingsBinding
import com.embibe.embibetvapp.model.createtest.examconfig.SubjectDetails
import java.util.regex.Matcher
import java.util.regex.Pattern

class RvSubjectsTestAdapter(var context: Context) :
    RecyclerView.Adapter<RvSubjectsTestAdapter.ViewHolder>() {

    private var adapterList: MutableList<SubjectDetails>? = null

    val TAG = this.javaClass.name

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemSubjectSelectTestSettingsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.item_subject_select_test_settings,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(adapterList!![position], holder)
    }

    override fun getItemCount(): Int = adapterList!!.size

    fun setData(list: MutableList<SubjectDetails>) {
        adapterList = list
        notifyDataSetChanged()
    }

    fun clearList() {
        adapterList!!.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(var binding: ItemSubjectSelectTestSettingsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: SubjectDetails,
            holder: ViewHolder
        ) {
            holder.setIsRecyclable(false);

            binding.tvSubjectName.text = capitalize(item.subjectName!!)
            binding.chapterCount.text = item.count.toString()
            binding.imgSubjectIcon.setImageResource(item.subjectIcon!!)

        }


    }

    private fun capitalize(capString: String): String? {
        val capBuffer = StringBuffer()
        val capMatcher: Matcher =
            Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString)
        while (capMatcher.find()) {
            capMatcher.appendReplacement(
                capBuffer,
                capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase()
            )
        }
        return capMatcher.appendTail(capBuffer).toString()
    }

}
