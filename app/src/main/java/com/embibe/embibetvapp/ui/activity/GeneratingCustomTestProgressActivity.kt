package com.embibe.embibetvapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ActivityGeneratingCustomTestProgressBinding
import com.embibe.embibetvapp.model.createtest.examconfig.ChapterDetails
import com.embibe.embibetvapp.model.createtest.generateowntest.response.GenerateOwnTestResponse
import com.embibe.embibetvapp.model.createtest.progress.response.ProgressResponse
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.viewmodel.GeneratingCustomTestProgressViewModel
import com.embibe.embibetvapp.utils.Utils
import java.util.*


class GeneratingCustomTestProgressActivity :
    BaseFragmentActivity() {
    private var incorrectMark: String? = null
    private var correctMark: String? = null
    private var chapterDetails: ArrayList<ChapterDetails>? = null
    private var testName: String? = ""
    private var createdAt: String = ""
    private var difficultyLevel: String? = null
    private var duration: Int? = null
    private val INTERVAL = 20000
    var mHandler: Handler = Handler()
    private var createTest: GenerateOwnTestResponse? = null
    var progressStatus: Float = 0.0F
    private lateinit var binding: ActivityGeneratingCustomTestProgressBinding
    private lateinit var generateProgressViewModel: GeneratingCustomTestProgressViewModel
    var isRunning = false

    var mHandlerTask: Runnable = object : Runnable {
        override fun run() {
            testProgressApi(
                testName,
                createdAt,
                difficultyLevel,
                duration,
                chapterDetails,
                correctMark!!,
                incorrectMark!!
            )
            mHandler.postDelayed(this, INTERVAL.toLong())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_generating_custom_test_progress)
        createTest = intent.getParcelableExtra<GenerateOwnTestResponse>(AppConstants.CREATE_TEST)
        generateProgressViewModel =
            ViewModelProviders.of(this).get(GeneratingCustomTestProgressViewModel::class.java)
        testName = intent.getStringExtra(AppConstants.TEST_NAME) as String
        createdAt = intent.getStringExtra(AppConstants.CREATED_AT) as String
        difficultyLevel = intent.getStringExtra(AppConstants.TEST_DIFFICULTY_LEVEL) as String
        duration = intent.getIntExtra(AppConstants.TEST_DURATION, 0)
        chapterDetails = intent.getParcelableArrayListExtra<ChapterDetails>("ChapterDetails")
        correctMark = intent.getStringExtra(AppConstants.TEST_CORRECT_MARK) as String
        incorrectMark = intent.getStringExtra(AppConstants.TEST_INCORRECT_MARK) as String


        testProgressApi(
            testName,
            createdAt,
            difficultyLevel,
            duration,
            chapterDetails,
            correctMark!!,
            incorrectMark!!
        )
    }

    private fun testProgressApi(
        testName: String?,
        createdAt: String,
        difficultyLevel: String?,
        duration: Int?,
        chapterDetails: ArrayList<ChapterDetails>?,
        correctMark: String,
        incorrectMark: String
    ) {

        generateProgressViewModel.testProgress(
            createTest!!,
            testName!!,
            createdAt,
            difficultyLevel!!,
            duration!!, chapterDetails,
            object : BaseViewModel.APICallBacks<ProgressResponse> {
                override fun onSuccess(model: ProgressResponse?) {
                    hideProgress()
                    if (model != null && model.progress!! < 100) {
                        progressStatus = model.progress!!.toFloat()
                        binding.crpv.percent = progressStatus
                        binding.tvPercent.text = progressStatus.toInt().toString() + "%"
                        if (!isRunning) {
                            startRepeatingTask()
                        }

                    } else {
                        stopRepeatingTask()
                        progressStatus = model?.progress!!.toFloat()
                        binding.crpv.percent = progressStatus
                        binding.tvPercent.text = progressStatus.toInt().toString() + "%"
                        moveToTestDetails(getContentDetails(model))
                        CreateNewTestActivity.createNewTestActivity?.finish()
                        SelectChaptersActivity.selectChaptersActivity?.finish()
                        finish()
                    }

                }

                override fun onFailed(code: Int, error: kotlin.String, msg: kotlin.String) {
                    hideProgress()
                    if (Utils.isApiFailed(code)) {
                        Utils.showError(
                            this@GeneratingCustomTestProgressActivity,
                            code,
                            object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: kotlin.String) {
                                    testProgressApi(
                                        testName,
                                        createdAt,
                                        difficultyLevel,
                                        duration,
                                        chapterDetails,
                                        correctMark,
                                        incorrectMark
                                    )
                                }

                                override fun onDismiss() {}
                            })
                    } else {
                        Utils.showToast(this@GeneratingCustomTestProgressActivity, error)
                    }
                }

            })
    }

    private fun getContentDetails(model: ProgressResponse): Content {
        var content = Content()
        content.bundle_id = model.fiberAtgData?.code!!
        content.title = model.fiberAtgData?.name!!
        content.description = model.fiberAtgData?.description!!
        content.currency = 0
        content.type = "Test"
        content.sub_type = model.fiberAtgData?.testType!!
        content.questions = model.fiberAtgData?.questionCount.toString()
        content.duration = model.fiberAtgData?.duration!!.toInt()
        content.total_marks = model.fiberAtgData?.maxMarks!!.toInt()
        content.chapterSize = (model.fiberAtgData?.chapterCount?.toString() + " Chapters")
        content.xpath = model.fiberAtgData?.path.toString()
        content.subject = TextUtils.join(", ", model.fiberAtgData?.subjectName!!)
        return content

    }


    fun moveToTestDetails(content: Content) {
        Utils.isTestGenerated = true
        val intent = Intent(this, TestDetailActivity::class.java)
        Utils.insertBundle(content, intent)
        startActivity(intent)

    }

    fun startRepeatingTask() {
        mHandlerTask.run()
        isRunning = true
    }

    fun stopRepeatingTask() {
        mHandler.removeCallbacks(mHandlerTask)
        isRunning = false
    }

}