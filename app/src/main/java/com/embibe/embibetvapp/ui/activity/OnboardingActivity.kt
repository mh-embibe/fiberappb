package com.embibe.embibetvapp.ui.activity

import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.FragmentActivity
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.ui.fragment.onboarding.OnboardingFragment

class OnboardingActivity : FragmentActivity() {
    /**
     * Called when the activity is first created.
     */
    private var mLastKeyDownTime: Long = 0

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.onboarding)
        supportFragmentManager.beginTransaction()
            .add(R.id.onboarding_fragment, OnboardingFragment()).commit()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        val current = System.currentTimeMillis()
        var res = false
        if (current - mLastKeyDownTime < 400) {
            res = true
        } else {
            res = super.onKeyDown(keyCode, event)
            mLastKeyDownTime = current
        }
        return res
    }
}