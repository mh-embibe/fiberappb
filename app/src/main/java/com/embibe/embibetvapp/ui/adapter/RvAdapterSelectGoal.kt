package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.RowItemGoalBinding
import com.embibe.embibetvapp.model.adduser.Exam

class RvAdapterSelectGoal(var context: Context) :
    RecyclerView.Adapter<RvAdapterSelectGoal.VHSelectGoal>() {

    private lateinit var binding: RowItemGoalBinding
    private var goalList = ArrayList<String>()
    private var goalsList: List<Exam> = ArrayList()
    private var lastClicked: Int = 9
    var mutableLiveData: MutableLiveData<Int> = MutableLiveData()
    private var mListener: OnItemStateListener? = null


    interface OnItemStateListener {
        fun onItemClick(view: View?, position: Int)

    }

    fun setOnItemStateListener(listener: OnItemStateListener) {
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHSelectGoal {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_item_goal,
            parent,
            false
        )
        return VHSelectGoal(binding.root)
    }

    override fun getItemCount(): Int {
        return if (goalsList.size > 0) goalsList.size else 0
    }

    override fun onBindViewHolder(holder: VHSelectGoal, position: Int) {
        holder.goalButton.text = goalsList[position].name

        holder.clGoal.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                holder.goalButton.background =
                    ContextCompat.getDrawable(context, R.drawable.bg_card_selected_yellow)
                holder.goalButton.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.grey_1000_b
                    )
                )
                mutableLiveData.value = position
            } else {
                holder.goalButton.background =
                    ContextCompat.getDrawable(context, R.drawable.bg_card_unselector_offwhite)
                holder.goalButton.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.grey_1000_w
                    )
                )
            }
        }

        holder.setListener()
    }

    fun getFocusedPosition() = mutableLiveData


    fun updateData(goalsList: List<Exam>) {
        this.goalsList = goalsList
        notifyDataSetChanged()
    }

    //
    inner class VHSelectGoal internal constructor(itemView: View) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        val clGoal: ConstraintLayout = itemView.findViewById<View>(R.id.cl_goal) as ConstraintLayout
        val goalButton: Button = itemView.findViewById<View>(R.id.btn_goal) as Button

        fun setListener() {
            clGoal.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            clGoal.setOnClickListener(this)

            if (mListener != null) {
                mListener!!.onItemClick(v, adapterPosition)
            }
        }
    }
}