package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemExamsBinding
import com.embibe.embibetvapp.model.UserData.getSecondaryExamCode
import com.embibe.embibetvapp.model.adduser.Exam
import com.embibe.embibetvapp.ui.activity.AddGoalsExamsActivity
import com.embibe.embibetvapp.utils.SegmentUtils

class SecondaryGoalExamAdapter(var context: Context) :
    RecyclerView.Adapter<SecondaryGoalExamAdapter.ExamViewHolder>() {

    //    var list = ContantUtils.testingSecondaryGoalsExamsData()
    private lateinit var list: ArrayList<Exam>
    var onItemClick: ((Exam) -> Unit)? = null
    var onFocusChange: ((Exam) -> Unit)? = null
    var selectedItem = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExamViewHolder {

        val binding: ItemExamsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_exams, parent, false
        )
        return ExamViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ExamViewHolder, position: Int) {


        holder.bind(list[position], position)
        var examCodes = getSecondaryExamCode()
        if (!examCodes.isNullOrEmpty()) {
            val lstExamCodes: List<String> = examCodes.split(",")
            for (exam in lstExamCodes) {
                if (exam.trim() == list[position].code) {
                    AddGoalsExamsActivity.secondaryGoalExams.add(exam.trim())
                    holder.binding.textGoalExam.background =
                        context.getDrawable(R.drawable.bg_goals_slected_selector)
                    holder.binding.textGoalExam.setTextColor(context.getColor(R.color.email_font_color))

                }
            }
        }
    }

    fun setData(data: ArrayList<Exam>) {
        list = data
        notifyDataSetChanged()
    }

    inner class ExamViewHolder(var binding: ItemExamsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: Exam,
            position: Int
        ) {
            binding.exam = item
            binding.executePendingBindings()
            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (adapterPosition != -1) {
                    onFocusChange?.invoke(this@SecondaryGoalExamAdapter.list[adapterPosition])
                    SegmentUtils.trackProfileExamsSecondaryGoalValueFocus(this@SecondaryGoalExamAdapter.list[adapterPosition].name)
                }
            }
            itemView.setOnClickListener {
                SegmentUtils.trackProfileExamsSecondaryGoalValueClick(list[adapterPosition].name)
                if (!AddGoalsExamsActivity.secondaryGoalExams.contains(item.code)) {
                    AddGoalsExamsActivity.secondaryGoalExams.add(item.code)
                    binding.textGoalExam.background = context.getDrawable(R.drawable.goal_selected)
                    binding.textGoalExam.background =
                        context.getDrawable(R.drawable.bg_goals_slected_selector)
                    binding.textGoalExam.setTextColor(context.getColor(R.color.email_font_color))
                } else {
                    AddGoalsExamsActivity.secondaryGoalExams.remove(item.code)
                    binding.textGoalExam.background =
                        context.getDrawable(R.drawable.bg_button_goals)
                    binding.textGoalExam.setTextColor(context.getColor(R.color.white))
                }
                onItemClick?.invoke(list[adapterPosition])
            }
        }


    }

}
