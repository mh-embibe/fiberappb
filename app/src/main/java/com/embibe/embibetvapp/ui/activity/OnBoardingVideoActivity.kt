package com.embibe.embibetvapp.ui.activity

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.embibe.embibetvapp.BuildConfig
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.databinding.ActivityOnboardingVideoBinding
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibejio.coreapp.preference.PreferenceHelper
import java.util.*


class OnBoardingVideoActivity : BaseFragmentActivity() {

    private lateinit var binding: ActivityOnboardingVideoBinding
    private var isVideoFinished: Boolean = false
    private var timer: Timer? = null

    var pref = PreferenceHelper()

    companion object {
        const val POOLING_INTERVAL_MS = 1000L
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_onboarding_video)
        initCode()
        removeRowsAndItemsPrefs()
    }

    private fun removeRowsAndItemsPrefs() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        val editor = sharedPref.edit()
        editor.remove(getString(R.string.last_selected_practice_row_item))
        editor.remove(getString(R.string.last_selected_practice_row))
        editor.remove(getString(R.string.last_selected_learn_row_item))
        editor.remove(getString(R.string.last_selected_learn_row))
        editor.remove(getString(R.string.last_selected_test_row))
        editor.remove(getString(R.string.last_selected_test_row_item))
        editor.remove(getString(R.string.last_selected_dashboard_row_item))
        editor.remove(getString(R.string.last_selected_dashboard_row))
        editor.apply()
    }

    private fun initCode() {
        SegmentUtils.trackSplashLoadStart()
        //configureVideoView()
        isVideoFinished = true
        startNextActivity()
    }

    private fun startNextActivity() {
        if (isVideoFinished) {
            isVideoFinished = false
            SegmentUtils.trackSplashLoadEnd()
            startActivity(Intent(this@OnBoardingVideoActivity, MainActivity::class.java))
            this.finish()
        }
    }


    private fun configureVideoView() {
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        val uriPath =
            "android.resource://" + BuildConfig.APPLICATION_ID + "/" + R.raw.embibe_animation_touch
        binding.videoView.setVideoPath(uriPath)
        binding.videoView.setOnInfoListener(object : MediaPlayer.OnInfoListener {
            override fun onInfo(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    // video started; hide the placeholder.
                    binding.placeholder.visibility = View.GONE
                    return true
                }
                return false
            }
        })
        binding.videoView.start()
        initVideoTimer(8000)
    }

    private fun initVideoTimer(stopAtMsec: Int) {
        cancelTimer()
        timer = Timer()
        timer!!.schedule(object : TimerTask() {
            override fun run() {
                binding.videoView.post {
                    if (binding.videoView.currentPosition >= stopAtMsec) {
                        binding.videoView.pause()
                        cancelTimer()
                        isVideoFinished = true
                        //player!!.pause()
                        startNextActivity()
                    }

                }
            }
        }, 0, POOLING_INTERVAL_MS)
    }

    private fun cancelTimer() {
        timer?.cancel()
        timer = null
    }

}
