package com.embibe.embibetvapp.ui.activity


import android.net.Uri
import android.os.Bundle
import com.embibe.embibetvapp.BuildConfig
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.model.test.TestQuestionResponse

class YouTubePlayerActivity : BaseFragmentActivity() {
    var url = ""
    private lateinit var testQuestionResponse: TestQuestionResponse
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (intent.hasExtra(AppConstants.VIDEO_URL)) {
            url = intent.getStringExtra(AppConstants.VIDEO_URL)?:url
        }
        launchYouTube(url)
    }

    private fun launchYouTube(videoUrl: String) {
        val YOUTUBE_PACKAGE_NAME = "com.jio.yt"
        val uri = Uri.parse(videoUrl)
        val id = uri.getQueryParameter("v")
        val url = "https://www.youtube.com/watch?v=$id"
        val pm = packageManager
        val launchIntent = pm.getLaunchIntentForPackage(YOUTUBE_PACKAGE_NAME)
        makeLog("youtube url : $url")
        if (launchIntent != null) {
           /* PreferenceManager.getInstance(ContextUtils.getApplicationContext())
                .setVideoSectionLaunch(true);*/
            launchIntent.putExtra("jio-embibe", BuildConfig.APPLICATION_ID)
            launchIntent.data = Uri.parse(url)
            startActivity(launchIntent)
            finish()
        }
    }
}