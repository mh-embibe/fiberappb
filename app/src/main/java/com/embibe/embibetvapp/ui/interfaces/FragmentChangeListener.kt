package com.embibe.embibetvapp.ui.interfaces

interface FragmentChangeListener {
    fun switchFragment(fragmentName: String)
}