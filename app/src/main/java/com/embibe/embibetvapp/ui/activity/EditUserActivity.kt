package com.embibe.embibetvapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.ui.fragment.addUser.EditUserFragment
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibejio.coreapp.preference.PreferenceHelper

class EditUserActivity : BaseFragmentActivity() {

    companion object {
        fun getEditUserIntent(
            context: Context,
            embibeToken: String?,
            userData: String,
            userType: String
        ): Intent {
            return Intent(context, EditUserActivity::class.java)
                .putExtra(AppConstants.TEMP_EMBIBE_TOKEN, embibeToken)
                .putExtra(AppConstants.USER_DATA, userData)
                .putExtra(AppConstants.USER_TYPE, userType)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user)

        SegmentUtils.trackAddUserScreenLoadStart()

        val currentToken: String = PreferenceHelper()[AppConstants.EMBIBETOKEN, ""].toString()
        PreferenceHelper().put(AppConstants.TEMP_EMBIBE_TOKEN, currentToken)
        val token = intent.getStringExtra(AppConstants.TEMP_EMBIBE_TOKEN)
        PreferenceHelper().put(AppConstants.EMBIBETOKEN, token)

        setEditUserFragment()
    }

    private fun setEditUserFragment() {
        val editUserFragment = EditUserFragment()
        val bundle = Bundle()
        bundle.putString(AppConstants.USER_TYPE, intent.getStringExtra(AppConstants.USER_TYPE))
        bundle.putString(AppConstants.USER_DATA, intent.getStringExtra(AppConstants.USER_DATA))
        editUserFragment.arguments = bundle
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_main_layout, editUserFragment).commit()
    }

    fun setFragment(fragment: Fragment, name: String) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_main_layout, fragment).addToBackStack(name).commit()
        SegmentUtils.trackAddUserScreenLoadEnd()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        supportFragmentManager.popBackStack()
    }
}