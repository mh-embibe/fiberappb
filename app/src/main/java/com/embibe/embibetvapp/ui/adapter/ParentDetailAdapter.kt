package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ItemSearchTestBinding
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.ui.activity.BookDetailActivity
import com.embibe.embibetvapp.ui.activity.DetailActivity
import com.embibe.embibetvapp.ui.activity.PracticeSummaryActivity
import com.embibe.embibetvapp.ui.activity.TestDetailActivity
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.insertBundle
import com.embibe.embibetvapp.utils.Utils.loadConceptsAsync
import com.embibe.embibetvapp.utils.Utils.removeUnderScoreAndCapitalize
import com.google.gson.Gson

class ParentDetailAdapter(var context: Context) :
    RecyclerView.Adapter<ParentDetailAdapter.ViewHolder>() {

    private var firstTimeFocused = false

    var adapterList: ArrayList<Results> = ArrayList()
    var onDpadDownHit: ((Boolean) -> Unit)? = null
    var onDapUpHit: ((Boolean) -> Unit)? = null

    companion object {
        val TAG = this.javaClass.name
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemSearchTestBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.item_search_test,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(adapterList[position])
    }

    override fun getItemCount(): Int = adapterList.size

    fun setData(list: ArrayList<Results>) {
        adapterList = list
        notifyDataSetChanged()
    }

    fun clearList() {
        adapterList.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(var binding: ItemSearchTestBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Results) {

            val adapterOrientation = getRecyclerOrientation(item.section_name)

            if (item.content.isNotEmpty()) {
                binding.tvRvHeader.text = removeUnderScoreAndCapitalize(item.section_name)
                binding.tvRvHeader.visibility = View.VISIBLE
                setSearchDetailAdapter(binding, itemView.context, item, adapterOrientation)
            } else {
                binding.tvRvHeader.visibility = View.GONE
            }
        }
    }

    private fun setSearchDetailAdapter(
        binding: ItemSearchTestBinding,
        context: Context,
        item: Results,
        adapterOrientation: Int
    ) {
        val searchDetailAdapter = SearchDetailAdapter(context)
        val arrayContent = item.content as ArrayList<Content>
        binding.rvItem.layoutManager = LinearLayoutManager(context, adapterOrientation, false)
        binding.rvItem.adapter = searchDetailAdapter
        searchDetailAdapter.setData(arrayContent)

        /*
         * The below logic is for showing the RvHeader of first item recyclerView when first
         * item of searchDetailAdapter is focused
         */
        if (adapterOrientation == LinearLayoutManager.VERTICAL) {
            searchDetailAdapter.onItemFocused = { focusedPos ->
                if (focusedPos == 0 && firstTimeFocused) {
                    binding.tvRvHeader.setPadding(
                        0, SettingsPanel.TOP_PADDING,
                        0, 0
                    )
                } else {
                    binding.tvRvHeader.setPadding(0, 0, 0, 0)
                    firstTimeFocused = true
                }
            }
        } else {
            searchDetailAdapter.onItemFocused = { focusedPosition ->
                if (focusedPosition == 0) {
                    firstTimeFocused = true
                }
            }
        }

        searchDetailAdapter.onItemClick = { content ->
            startActivityByType(content, context)
        }

        searchDetailAdapter.onItemDpadUpHit = { position ->
            if (position == 0) {
                onDapUpHit?.invoke(true)
            }
        }

        searchDetailAdapter.onItemDpadHit = { dpadDownHitPos ->
            if (dpadDownHitPos == arrayContent.size - 1) {
                try {
                    onDpadDownHit?.invoke(true)
                } catch (e: Exception) {
                    Log.e(TAG, "exception: ${e.message}")
                }
            } else {
                onDpadDownHit?.invoke(false)
            }
        }
    }

    private fun getRecyclerOrientation(sectionName: String): Int {
        return if (sectionName.toLowerCase().contains("book") || sectionName.toLowerCase().contains(
                "chapters"
            ) || sectionName.toLowerCase().contains("test")
        ) {
            LinearLayoutManager.HORIZONTAL
        } else LinearLayoutManager.VERTICAL
    }

    private fun startActivityByType(content: Content, context: Context) {

        when ((content.type).toLowerCase()) {
            AppConstants.TYPE_VIDEO, AppConstants.TYPE_COOBO -> {
                startDetailActivity(context, content)
            }
            AppConstants.TYPE_BOOK -> {
                startBookDetailActivity(context, content)
            }
            AppConstants.TYPE_PRACTICE, AppConstants.TYPE_CHAPTER -> {
                startPracticeDetailActivity(context, content)
            }
            AppConstants.TYPE_TEST -> {
                startTestDetailActivity(context, content)
            }
        }
    }

    private fun startDetailActivity(
        context: Context,
        content: Content
    ) {
        loadConceptsAsync(content, context)
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("fromSearch", true)
        intent.putExtra("value", Gson().toJson(content))
        insertBundle(content, intent)

        context.startActivity(intent)
    }

    private fun startBookDetailActivity(context: Context, content: Content) {
        val intent = Intent(context, BookDetailActivity::class.java)
        intent.putExtra(AppConstants.NAV_NAME_TYPE, AppConstants.NAV_TYPE_BOOK)
        insertBundle(content, intent)
        context.startActivity(intent)
    }

    private fun startPracticeDetailActivity(context: Context, content: Content) {
        Utils.loadPracticeSummaryInfo(content, context)

        val intent = Intent(context, PracticeSummaryActivity::class.java)
        insertBundle(content, intent)
        context.startActivity(intent)
    }

    private fun startTestDetailActivity(context: Context, content: Content) {
        val intent = Intent(context, TestDetailActivity::class.java)
        insertBundle(content, intent)
        context.startActivity(intent)
    }

    object SettingsPanel {
        const val TOP_PADDING = 80
    }
}