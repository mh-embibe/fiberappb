package com.embibe.embibetvapp.ui.adapter

import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ItemSubTestMenuBinding
import com.embibe.embibetvapp.model.test.SubFeedbackMenuModel
import com.embibe.embibetvapp.ui.activity.TestCWAActivity
import com.embibe.embibetvapp.utils.SegmentUtils

class TestFeedbackSubMenuAdapter(private val type: String) :
    RecyclerView.Adapter<TestFeedbackSubMenuAdapter.TestsViewHolder>() {

    var list = ArrayList<SubFeedbackMenuModel>()
    var onItemClick: ((SubFeedbackMenuModel) -> Unit)? = null
    private lateinit var subSubMenuAdapter: TestFeedbackSubSubMenuAdapter
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestsViewHolder {
        val binding: ItemSubTestMenuBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_sub_test_menu, parent, false
        )
        return TestsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (list.size > 0) list.size else 0
    }

    override fun onBindViewHolder(holder: TestsViewHolder, position: Int) {
        holder.bind(list[position])

        if (position == selectedPosition) {
            if (list[position].subItems.size > 0) {
                holder.binding.imgAdd.setImageDrawable(holder.itemView.resources.getDrawable(R.drawable.minus_bold))
                holder.binding.recyclerSubSubItem.visibility = View.VISIBLE
            }
        }
    }

    fun setData(list: ArrayList<SubFeedbackMenuModel>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class TestsViewHolder(var binding: ItemSubTestMenuBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SubFeedbackMenuModel) {
            binding.title.text = item.title


//            binding.ivBg.setImageResource(getBackground(item.title))

            val requestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            Glide.with(itemView.context).setDefaultRequestOptions(requestOptions)
                .load(getBackground(item.title))
                .transition(DrawableTransitionOptions.withCrossFade())
                .transform(RoundedCorners(16))
                .into(binding.ivBg)

            if (type == "revision_lists") {
                binding.imgAdd.visibility = View.GONE
                binding.imgArrow.visibility = View.VISIBLE
            } else if (type == "test_taking_strategy") {
                binding.imgArrow.visibility = View.GONE
                binding.imgAdd.visibility = View.VISIBLE
            }

            binding.recyclerSubSubItem.visibility = View.GONE

            if (adapterPosition == list.size - 1) {
                binding.verticalViewBottom.visibility = View.INVISIBLE
            } else {
                binding.verticalViewBottom.visibility = View.VISIBLE
            }

            if (list[adapterPosition].subItems.size > 0) {
                binding.imgAdd.visibility = View.VISIBLE
                binding.imgAdd.setImageDrawable(itemView.resources.getDrawable(R.drawable.plus_bold))
                setRecycler(binding, itemView, item)
            } else {
                binding.imgAdd.visibility = View.GONE
            }
        }

        private fun getBackground(title: String): Int {
            return when (title) {
                AppConstants.TYPE_CHAPTERS_YOU_GOT_RIGHT -> R.drawable.chapters_right
                AppConstants.TYPE_CHAPTERS_YOU_GOT_WRONG -> R.drawable.chapters_wrong
                AppConstants.TYPE_CHAPTERS_YOU_DID_NOT_ATTEMPT -> R.drawable.chapters_not_attempted
                AppConstants.TYPE_STRONG_CHAPTERS_YOU_GOT_WRONG -> R.drawable.strong_chapters_wrong
                AppConstants.TYPE_STRONG_CHAPTERS_YOU_DID_NOT_ATTEMPT -> R.drawable.strong_chapters_not_attempted
                else -> R.drawable.practice_placeholder
            }
        }

        init {
            binding.subImgLayout.setOnFocusChangeListener { v, hasFocus ->
                when(hasFocus) {
                    true -> {
                        if(type == "revision_lists") {
                            SegmentUtils.trackTestFeedbackScreenRevisionListFocus()
                        } else if(type == "test_taking_strategy") {
                            SegmentUtils.trackTestFeedbackScreenImproveTestTakingStrategyValueFocus(
                                list[adapterPosition].title,adapterPosition+1)
                        }
                    }
                }
            }
            binding.subImgLayout.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {

                            if (type == "revision_lists") {
                                // intent next activity
                                v.context.startActivity(
                                    TestCWAActivity.getActivityIntent(
                                        v.context, list[adapterPosition].title, adapterPosition
                                    )
                                )
                                SegmentUtils.trackTestFeedbackScreenRevisionListClick()

                            } else if (type == "test_taking_strategy") {
                                SegmentUtils.trackTestFeedbackScreenImproveTestTakingStrategyValueClick(
                                    list[adapterPosition].title,adapterPosition+1)
                                if (selectedPosition != adapterPosition) {
                                    val previousPosition = selectedPosition
                                    notifyItemChanged(previousPosition)

                                    selectedPosition = adapterPosition
                                    notifyItemChanged(adapterPosition)
                                } else {
                                    // make recycler visible and previous item back to original position
                                    toggleRecyclerVisibility(binding, itemView, adapterPosition)
                                }
                            }

                        }
                    }
                }

                false
            }
        }
    }

    private fun toggleRecyclerVisibility(
        binding: ItemSubTestMenuBinding,
        itemView: View,
        adapterPosition: Int
    ) {
        if (list[adapterPosition].subItems.size > 0) {
            if (binding.recyclerSubSubItem.visibility == View.GONE) {

                binding.recyclerSubSubItem.visibility = View.VISIBLE
                binding.imgAdd.setImageDrawable(itemView.resources.getDrawable(R.drawable.minus_bold))
            } else if (binding.recyclerSubSubItem.visibility == View.VISIBLE) {

                binding.recyclerSubSubItem.visibility = View.GONE
                binding.imgAdd.setImageDrawable(itemView.resources.getDrawable(R.drawable.plus_bold))
            }
        }
    }

    private fun setRecycler(
        binding: ItemSubTestMenuBinding,
        itemView: View,
        item: SubFeedbackMenuModel
    ) {
        subSubMenuAdapter = TestFeedbackSubSubMenuAdapter()
        val layoutManager =
            LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerSubSubItem.adapter = subSubMenuAdapter
        binding.recyclerSubSubItem.layoutManager = layoutManager
        subSubMenuAdapter.setData(item.subItems)

    }
}