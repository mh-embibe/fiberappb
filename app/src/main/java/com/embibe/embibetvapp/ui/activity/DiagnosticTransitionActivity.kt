package com.embibe.embibetvapp.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ActivityDiagnosticTransitionBinding
import com.embibe.embibetvapp.ui.fragment.achieve.DiagnosticTransitionFragment

class DiagnosticTransitionActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDiagnosticTransitionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_diagnostic_transition)
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.anim_fade_in, R.anim.anim_fade_out)
        transaction.replace(
            R.id.diagnostic_TL,
            DiagnosticTransitionFragment(this)
        ).commit()

    }


}