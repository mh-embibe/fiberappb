package com.embibe.embibetvapp.ui.adapter

import android.view.ViewGroup
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemCutOffScoreBinding
import com.embibe.embibetvapp.databinding.ItemTestFeedbackCircleBinding
import com.embibe.embibetvapp.ui.viewholders.BaseViewHolder
import com.embibe.embibetvapp.ui.viewholders.TestFeedbackBottomProgressCircleViewHolder
import com.embibe.embibetvapp.ui.viewholders.TestFeedbackCutOffViewHolder
import com.embibe.embibetvapp.utils.Utils

private const val TYPE_CIRCLE = 0
private const val TYPE_DASED_LINE = 1

class TestFeedbackBottomCircleAdapter : BaseAdapter<String>() {

    var layout = R.layout.item_test_feedback_circle

    override fun getLayoutId(position: Int, obj: String) = layout

    override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<String> {
        return when (viewType) {
            TYPE_CIRCLE -> {
                val binding = Utils.binder<ItemTestFeedbackCircleBinding>(layout, parent)
                TestFeedbackBottomProgressCircleViewHolder(binding)
            }
            else -> {
                val binding =
                    Utils.binder<ItemCutOffScoreBinding>(R.layout.item_cut_off_score, parent)
                TestFeedbackCutOffViewHolder(binding)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            listItems.size - 1 -> TYPE_DASED_LINE
            else -> TYPE_CIRCLE
        }
    }

}