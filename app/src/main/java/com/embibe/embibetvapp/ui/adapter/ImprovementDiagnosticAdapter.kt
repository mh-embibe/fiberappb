package com.embibe.embibetvapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemDiagnosticImprovementBinding
import com.embibe.embibetvapp.model.diagnostic.ReadninessData

class ImprovementDiagnosticAdapter :
    RecyclerView.Adapter<ImprovementDiagnosticAdapter.TestViewHolder>() {

    var list: List<ReadninessData> = ArrayList()
    var onItemClick: ((ReadninessData) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestViewHolder {

        val binding: ItemDiagnosticImprovementBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_diagnostic_improvement,
                parent, false
            )
        return TestViewHolder(binding)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: TestViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun setData(data: List<ReadninessData>) {
        list = data
        notifyDataSetChanged()
    }

    inner class TestViewHolder(var binding: ItemDiagnosticImprovementBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ReadninessData) {
            if (item.bundle_id.isNotEmpty()) {
                binding.testArrow.visibility = View.VISIBLE
                binding.tvTestDetail.visibility = View.VISIBLE
                binding.tvTestTitle.visibility = View.VISIBLE
                binding.tvAd.visibility = View.GONE
                binding.test = item
                Glide.with(binding.root.context).load(R.drawable.achieve_bg)
                    .into(binding.imgBackground)
                itemView.setOnClickListener {
                    onItemClick?.invoke(list[adapterPosition])
                }
            } else {
                Glide.with(binding.root.context).load(item.image_url)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .transform(RoundedCorners(6))
                    .into(binding.imgBackground)
                binding.testArrow.visibility = View.GONE
                binding.tvTestDetail.visibility = View.GONE
                binding.tvTestTitle.visibility = View.GONE
                binding.tvAd.visibility = View.VISIBLE
                binding.tvAd.text = item.text
            }

        }

    }
}