package com.embibe.embibetvapp.ui.fragment.learn

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.VolleyError
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentLearnSummaryBinding
import com.embibe.embibetvapp.model.chapter.chapterLearningObjects.ChapterLearningObject
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.activity.*
import com.embibe.embibetvapp.ui.adapter.LearnAllVideoAdapter
import com.embibe.embibetvapp.ui.adapter.LearnSummaryAdapter
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.unity.UnityPlayerActivity
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.insertBundle
import com.embibe.embibetvapp.utils.Utils.loadLearnSummaryAsync
import com.embibe.embibetvapp.utils.Utils.loadTopicSummaryAsync
import com.embibe.embibetvapp.utils.VideoUtils
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


class LearnSummaryFragment : BaseAppFragment() {

    private var data: Content? = null
    private lateinit var homeViewModel: HomeViewModel
    private var results: List<Content>? = null
    private var resultsVideos: List<ResultsEntity>? = null
    private var resultsChapterVideoSections: List<ChapterLearningObject>? = null
    private lateinit var type: String
    private lateinit var binding: FragmentLearnSummaryBinding
    private lateinit var learnSummaryAdapter: LearnSummaryAdapter
    private var TAG = this.javaClass.simpleName
    private var searchQuery: String = ""
    private var searchResultSize: Int = 0
    private var speechRecognizer: SpeechRecognizer? = null
    private var speechIntent: Intent? = null
    private var countDownTimer: CountDownTimer? = null
    private val speechTimerDuration = 10000L // 10 secs
    private var elapsedSpeechTimer = 0L
    private var searchInput: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(activity)
        setupSpeechRecognitionListener()
        speechIntent = getRecognizerIntent()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_learn_summary, container, false)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        type = this.requireArguments().getString("type").toString()
        data = this.requireArguments().getParcelable<Content>(AppConstants.CONTENT)
        SegmentUtils.trackLearnSummaryScreenAvailableOptionContentLoadStart(type)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getData(view)
        micSearchButtonListeners()
        setSearchTextChangeListener(view)
        setKeyboardFocusListener()
    }

    private fun setKeyboardFocusListener() {
        binding.textSearch.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    openSoftKeyboard(context, binding.textSearch)
                    SegmentUtils.trackLearnSummaryScreenKeyboardFocus()
                }
                false -> {
                    binding.textSearch.clearFocus()
                    Utils.hideKeyboardFrom(
                        context,
                        binding.textSearch.rootView
                    )

                }

            }
        }


    }

    private fun openSoftKeyboard(context: Context?, view: View) {
        view.requestFocus()
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)

    }

    private fun setSearchTextChangeListener(view: View) {
        binding.textSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                searchInput = "Keyboard"
                if (!s.isNullOrEmpty()) {
                    getSuggestionsAndHitSearch(s.toString(), "10", view)
                    SegmentUtils.trackEventSearchResultLoadStart(
                        searchQuery,
                        searchQuery.length.toString()
                    )
                    SegmentUtils.trackLearnSummaryScreenSearchLoadStart(searchInput)
                    SegmentUtils.trackLearnSummaryScreenKeyboardClick(
                        s.toString(),
                        s.length.toString(),
                        "D-KeyPad"
                    )
                } else {
                    getData(view)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })


    }

    private fun getSuggestionsAndHitSearch(
        searchQuery: String,
        size: String,
        view: View
    ) {
        SegmentUtils.trackEventSearch(searchQuery)

        homeViewModel.searchResults(
            type, searchQuery, data, this,
            object : BaseViewModel.APICallBacks<List<Content>> {
                override fun onSuccess(results: List<Content>?) {
                    if (results!!.isNotEmpty()) {
                        if (results.isEmpty()) {
                            noSearchResults()
                        } else {
                            binding.textNoResult.visibility = View.GONE
                            binding.detailRecycler.visibility = View.VISIBLE
                            setDetailRecycler(view, results as ArrayList<Content>)
                        }
                    } else {
                        noSearchResults()
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg")
                }
            })
    }

    private fun noSearchResults() {
        /*show the results not found UI here*/
        binding.textNoResult.visibility = View.VISIBLE
        binding.detailRecycler.visibility = View.GONE
    }

    private fun micSearchButtonListeners() {

        binding.micSearch.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (elapsedSpeechTimer != 0L) {
                            stopRecognition()
                            countDownTimer?.cancel()
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_DPAD_UP -> {
                        binding.micSearch.postDelayed({
                            binding.micSearch.requestFocus()
                        }, 0)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        startRecognition()
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        toggleFocusForTheView(binding.detailRecycler, true)
                    }
                    KeyEvent.KEYCODE_VOICE_ASSIST -> {
                    }
                }
            }

            false
        }
    }

    private fun toggleFocusForTheView(view: View, askFocus: Boolean) {

        view.postDelayed({
            when (askFocus) {
                true -> {
                    view.requestFocus()
                }
                false -> {
                    view.clearFocus()
                }
            }
        }, 10)
    }

    fun startRecognition() {
        if (Utils.hasPermission(requireContext(), Manifest.permission.RECORD_AUDIO)) {
            countDownTimer?.cancel()
            speechRecognizer?.startListening(speechIntent)
            if (speechRecognizer != null) setSpeechRecognitionTimer()
        } else {
            Toast.makeText(
                context,
                requireContext().resources.getString(R.string.audio_permission_not),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun setSpeechRecognitionTimer() {
        elapsedSpeechTimer = 0L
        countDownTimer = object : CountDownTimer(speechTimerDuration, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                elapsedSpeechTimer = (speechTimerDuration - millisUntilFinished) / 1000
                Log.i(TAG, "$elapsedSpeechTimer")
            }

            override fun onFinish() {
                stopRecognition()
            }
        }
        countDownTimer?.start()
    }

    private fun stopRecognition() {
        finishMicSearchAnimation()
        speechRecognizer?.cancel()
    }

    private fun finishMicSearchAnimation() {
        binding.micSearchLav.cancelAnimation()
        binding.micSearchLav.visibility = View.INVISIBLE
        binding.micSearch.setBackgroundResource(R.drawable.bg_voice_search)
    }

    fun startMicSearchAnimation() {
        binding.micSearchLav.visibility = View.VISIBLE
        binding.micSearch.setBackgroundResource(0)
        binding.micSearchLav.playAnimation()
    }

    private fun setupSpeechRecognitionListener() {

        speechRecognizer?.setRecognitionListener(object : RecognitionListener {
            override fun onReadyForSpeech(params: Bundle?) {
                Log.v(TAG, "onReadyForSpeech")
                startMicSearchAnimation()
            }

            override fun onRmsChanged(rmsdB: Float) {
//                Log.v(TAG, "onRmsChanged $rmsdB")
            }

            override fun onBufferReceived(buffer: ByteArray?) {
//                Log.v(TAG, "onBufferReceived " + buffer?.size)
            }

            override fun onPartialResults(partialResults: Bundle?) {
                val results =
                    partialResults?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                Log.v(
                    TAG, "onPartialResults " + partialResults + " results "
                            + (results?.size ?: results)
                )
            }

            override fun onEvent(eventType: Int, params: Bundle?) {
            }

            override fun onBeginningOfSpeech() {
                Log.v(TAG, "onBeginningOfSpeech")
            }

            override fun onEndOfSpeech() {
                Log.v(TAG, "onEndOfSpeech")
                countDownTimer?.cancel()
            }

            override fun onError(error: Int) {
                when (error) {
                    SpeechRecognizer.ERROR_NETWORK_TIMEOUT -> Log.w(
                        TAG,
                        "recognizer network timeout"
                    )
                    SpeechRecognizer.ERROR_NETWORK -> Log.w(
                        TAG,
                        "recognizer network error"
                    )
                    SpeechRecognizer.ERROR_AUDIO -> {
                        Log.w(
                            TAG,
                            "recognizer audio error"
                        )
                    }
                    SpeechRecognizer.ERROR_SERVER -> Log.w(
                        TAG,
                        "recognizer server error"
                    )
                    SpeechRecognizer.ERROR_CLIENT -> Log.w(
                        TAG,
                        "recognizer client error"
                    )
                    SpeechRecognizer.ERROR_SPEECH_TIMEOUT -> Log.w(
                        TAG,
                        "recognizer speech timeout"
                    )
                    SpeechRecognizer.ERROR_NO_MATCH -> Log.w(
                        TAG,
                        "recognizer no match"
                    )
                    SpeechRecognizer.ERROR_RECOGNIZER_BUSY -> Log.w(
                        TAG,
                        "recognizer busy"
                    )
                    SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS -> Log.w(
                        TAG,
                        "recognizer insufficient permissions"
                    )
                    else -> Log.d(TAG, "recognizer other error")
                }

                countDownTimer?.cancel()
                stopRecognition()
            }

            override fun onResults(results: Bundle?) {
                val resultsList = results?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                Log.v(
                    TAG, "onResults " + resultsList + " results "
                            + (resultsList?.size ?: resultsList)
                )
                if (resultsList!!.isNotEmpty()) {
                    binding.textSearch.text.clear()
                    val searchQuery = resultsList[0].toLowerCase(Locale.ENGLISH)
                    performSearch(searchQuery)

                    finishMicSearchAnimation()
                    countDownTimer?.cancel()
                }
            }

        })
    }

    private fun getRecognizerIntent(): Intent? {
        val recognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        recognizerIntent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "en-US")
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, true)

        return recognizerIntent
    }

    override fun onPause() {
        releaseRecognizer(false)
        super.onPause()
    }

    override fun onStop() {
        countDownTimer?.cancel()
        releaseRecognizer(true)
        super.onStop()
    }

    private fun releaseRecognizer(isDestroyed: Boolean) {
        if (null != speechRecognizer) {
            speechRecognizer?.setRecognitionListener(null)
            if (isDestroyed)
                speechRecognizer?.destroy()
        }
    }

    private fun performSearch(searchQuery: String) {
        //bind search text to textSearch text view
        binding.textSearch.setText(searchQuery)
        SegmentUtils.trackLearnSummaryScreenVoiceSearchClick(
            searchQuery,
            searchQuery.length.toString()
        )
        searchInput = "Voice"
        SegmentUtils.trackLearnSummaryScreenSearchLoadStart(searchInput)
        /*api call for load suggestions only*/
//        getSearchSuggestions(searchQuery, "10")
        /*api call for load suggestions with search results*/
//        getSuggestionsAndHitSearch(searchQuery, "10")
    }

    private fun getData(view: View) {
        binding.textSearch.visibility = View.VISIBLE
        binding.micSearch.visibility = View.VISIBLE
        binding.view.visibility = View.VISIBLE
        binding.textNoResult.visibility = View.GONE
        binding.detailRecycler.visibility = View.VISIBLE
        when (type) {
            getString(R.string.all_videos_for_this_chapter) -> {
                binding.textSearch.hint = getString(R.string.search)
                binding.tvDetailName.text = type
                resultsChapterVideoSections =
                    requireArguments().getParcelableArrayList(AppConstants.ALL_VIDEOS_FOR_THIS_CHAPTER)
                        ?: arrayListOf()

            }
            getString(R.string.test_on_this_chapter) -> {
                binding.textSearch.hint = getString(R.string.search_tests)
                binding.textSearch.visibility = View.GONE
                binding.micSearch.visibility = View.GONE
                binding.view.visibility = View.GONE

                binding.tvDetailName.text = getString(R.string.tests_covering_this_chapter)
                results =
                    requireArguments().getParcelableArrayList(AppConstants.TEST_ON_THIS_CHAPTER)
                        ?: arrayListOf()
            }
            getString(R.string.practice_on_this_chapter) -> {
                binding.textSearch.hint = getString(R.string.search_practices)
                binding.tvDetailName.text = type
                results =
                    requireArguments().getParcelableArrayList(AppConstants.PRACTICE_ON_THIS_CHAPTER)
                        ?: arrayListOf()
            }
            getString(R.string.topics_in_this_chapter) -> {
                binding.textSearch.hint = getString(R.string.search)
                binding.tvDetailName.text = type
                results =
                    requireArguments().getParcelableArrayList(AppConstants.TOPICS_IN_THIS_CHAPTER)
                        ?: arrayListOf()
            }
            getString(R.string.pre_requisite_topics) -> {
                binding.textSearch.hint = getString(R.string.search)
                binding.tvDetailName.text = getString(R.string.pre_requisites_for_this_chapter)
                results =
                    requireArguments().getParcelableArrayList(AppConstants.PRE_REQUISITE_TOPICS)
                        ?: arrayListOf()
            }
        }

        if (type == getString(R.string.all_videos_for_this_chapter)) {
            val allVideo = createAllVideo(resultsChapterVideoSections!!)
            setDetailAllVideoRecycler(view, allVideo)
        } else {
            val data = createData(results, resultsVideos)
            setDetailRecycler(view, data)
        }
    }

    private fun setDetailAllVideoRecycler(view: View, data: ArrayList<Content>) {
        SegmentUtils.trackLearnSummaryScreenSearchLoadEnd(searchInput)
        val learnAllVideoAdapter = LearnAllVideoAdapter(view.context)
        val layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        binding.detailRecycler.adapter = learnAllVideoAdapter
        binding.detailRecycler.layoutManager = layoutManager
        learnAllVideoAdapter.setData(data)
        learnAllVideoAdapter.onItemClick = { content ->
            startActivityByType(content)
        }
    }

    private fun createAllVideo(sections: List<ChapterLearningObject>): ArrayList<Content> {
        var chapterSections = sections
        val allVideoContent: ArrayList<Content> = arrayListOf()
        for (headerSection in chapterSections) {
            val currentHeaderSection = Content()
            currentHeaderSection.section_name = headerSection.sectionName.toString()
            currentHeaderSection.type = AppConstants.TYPE_HEADER
            currentHeaderSection.currency = headerSection.currency
            allVideoContent.add(currentHeaderSection)
            for (subSection in headerSection.sections!!) {
                if (!subSection.subSectionName.isNullOrEmpty()) {
                    val currentSubHeaderSection = Content()
                    currentSubHeaderSection.section_name = subSection.subSectionName.toString()
                    currentSubHeaderSection.type = AppConstants.TYPE_SUB_HEADER
                    currentHeaderSection.currency = subSection.currency
                    allVideoContent.add(currentSubHeaderSection)
                    allVideoContent.addAll(subSection.content!!)
                } else {
                    allVideoContent.addAll(subSection.content!!)
                }
            }
        }
        return allVideoContent
    }

    override fun onResume() {
        super.onResume()
        SegmentUtils.trackLearnSummaryScreenAvailableOptionContentLoadEnd(type)
        if (speechRecognizer != null)
            setupSpeechRecognitionListener()
    }

    private fun setDetailRecycler(view: View, data: ArrayList<Content>) {
        SegmentUtils.trackLearnSummaryScreenSearchLoadEnd(searchInput)
        learnSummaryAdapter = LearnSummaryAdapter(view.context)
        val layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        binding.detailRecycler.adapter = learnSummaryAdapter
        binding.detailRecycler.layoutManager = layoutManager
        learnSummaryAdapter.setData(data, type)

        learnSummaryAdapter.onItemClick = { content ->
            startActivityByType(content)
        }
    }

    private fun startActivityByType(content: Content) {

        when ((content.type).toLowerCase(Locale.getDefault())) {
            AppConstants.TYPE_VIDEO -> {
                startVideoActivity(content)
                // startDetailActivity(content)
            }
            AppConstants.TYPE_COOBO -> {
                callCoobo(content)
            }
            AppConstants.TYPE_BOOK -> {
                startBookDetailActivity(content)
            }
            AppConstants.TYPE_PRACTICE, AppConstants.TYPE_CHAPTER -> {
                startPracticeDetailActivity(content)
            }
            AppConstants.NAV_TYPE_SYLLABUS -> {
                startLearnSummaryActivity(content)
            }
            AppConstants.TYPE_TOPIC -> {
                startTopicSummaryActivity(content)
            }
            AppConstants.TEST -> {
                startTestScreen(content)
                // startTopicSummaryActivity(content)
            }
        }
    }

    private fun startTestScreen(data: Content) {
        val testIntent = Intent(requireContext(), TestActivity::class.java)
        testIntent.putExtra(AppConstants.TEST_XPATH, data.xpath)
        testIntent.putExtra(AppConstants.TEST_CODE, data.bundle_id)
        testIntent.putExtra(AppConstants.TEST_NAME, data.title)
        testIntent.putExtra(AppConstants.CONCEPT_ID, data.learning_map.conceptId)
        testIntent.putExtra(AppConstants.IS_DIAGNOSTIC, false)
        startActivity(testIntent)
    }

    private fun startLearnSummaryActivity(item: Content) {
        loadLearnSummaryAsync(item, context)
        val intent = Intent(context, LearnSummaryActivity::class.java)
        insertBundle(item, intent)
        startActivity(intent)
    }

    private fun startTopicSummaryActivity(item: Content) {
        loadTopicSummaryAsync(item, context)
        val intent = Intent(context, TopicSummaryActivity::class.java)
        insertBundle(item, intent)
        startActivity(intent)
    }

    /* private fun startDetailActivity(content: Content) {
         loadConceptsAsync(content, context)
         val intent = Intent(context, DetailActivity::class.java)
         insertBundle(content, intent)
         startActivity(intent)
     }
 */
    private fun startBookDetailActivity(content: Content) {
        val intent = Intent(context, BookDetailActivity::class.java)
        intent.putExtra(AppConstants.NAV_NAME_TYPE, AppConstants.NAV_TYPE_BOOK)
        insertBundle(content, intent)
        startActivity(intent)
    }

    private fun startPracticeDetailActivity(data: Content) {
/*

        Utils.loadPracticeSummaryInfo(content, context)
        val intent = Intent(context, PracticeDetailActivity::class.java)
        insertBundle(content, intent)
        startActivity(intent)
*/
        val intent = Intent(requireView().context, PracticeActivity::class.java)
        intent.putExtra("id", data.id)
        intent.putExtra("title", data.title)
        intent.putExtra("subject", data.subject)
        intent.putExtra(AppConstants.TOPIC_LEARN_PATH, data.learning_map.topicLearnPathName)
        intent.putExtra(AppConstants.CONCEPT_ID, data.learning_map.conceptId)
        intent.putExtra(AppConstants.LENGTH, data.length)
        intent.putExtra(AppConstants.FORMAT_ID, data.learning_map.format_id)
        intent.putExtra(AppConstants.LM_NAME, data.learnmap_id)
        intent.putExtra(AppConstants.PRACTICE_MODE, AppConstants.PracticeModes.Normal)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, data.learnpath_name)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, data.learnpath_format_name)
        try {
            val parts = data.learning_map.lpcode.split("--")
            val lmCode = parts[parts.size - 1]
            val lmLevel = Utils.getLevelUsingCode(lmCode)
            val examCode = Utils.getExamLevelCode(parts)
            intent.putExtra("lm_level", lmLevel)
            intent.putExtra("lm_code", lmCode)
            intent.putExtra("exam_code", examCode)
            intent.putExtra(AppConstants.PRACTICE_MODE, AppConstants.PracticeModes.Normal)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        startActivity(intent)
    }

    private fun createData(
        results: List<Content>?,
        resultsVideos: List<ResultsEntity>?
    ): ArrayList<Content> {
        val dataList: ArrayList<Content> = ArrayList()

        if (type == getString(R.string.all_videos_for_this_chapter)) {
            makeLog("results size - ${resultsVideos?.size}")
            resultsVideos?.forEach { resultEntity ->
                resultEntity.content?.forEach { content ->
                    dataList.add(content)
                }
            }
        } else {
            makeLog("results size - ${results?.size}")
            results?.forEach { content ->
                dataList.add(content)
            }
        }
        return dataList
    }

    fun requestFocus() {
        binding.detailRecycler.postDelayed({
            binding.detailRecycler.requestFocus()
        }, 10)
    }


    private fun startVideoActivity(content: Content) {
        doAsync {
            val url = content.url
            val type = Utils.getVideoTypeUsingUrl(content.url)
            val title = content.title
            val id = content.id
            var conceptId = ""
            var subject = ""
            var chapter = ""
            var totalDuration = ""
            var topicLearnPath = ""
            conceptId = content.learning_map.conceptId
            chapter = content.learning_map.chapter
            subject = content.learning_map.subject
            topicLearnPath = content.learning_map.topicLearnPathName
            totalDuration = content.length.toString()
            uiThread {
                hideProgress()
                callVideo(
                    id,
                    AppConstants.VIDEO,
                    conceptId,
                    url,
                    title,
                    "",
                    type.toLowerCase(),
                    subject,
                    chapter,
                    "",
                    totalDuration,
                    topicLearnPath, content
                )
            }
        }
    }


    private fun callVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        videoTotalDuration: String,
        topicLearnpath: String, content: Content
    ) {
        if (source == AppConstants.PLAYER_TYPE_YOUTUBE) {
            if (Utils.isYouTubeAppAvailable(requireContext())) {
                Utils.startYoutubeApp(requireContext(), url)
            } else {
                startVideo(
                    id,
                    type,
                    conceptId,
                    url,
                    title,
                    description,
                    source,
                    subject,
                    chapter,
                    authors,
                    videoTotalDuration,
                    topicLearnpath
                )
            }
        } else {
            val callback = object : BaseViewModel.APICallBackVolley {
                override fun <T> onSuccessCallBack(response: T) {
                    hideProgress()
                    response as JSONObject
                    val playableUrl = Utils.getVimeoHD((response["files"] as JSONArray))
                    startVideo(
                        id,
                        type,
                        conceptId,
                        playableUrl,
                        title,
                        description,
                        AppConstants.PLAYER_TYPE_EXOPLAYER,
                        subject,
                        chapter,
                        authors,
                        videoTotalDuration,
                        topicLearnpath
                    )
                    /*hide ProgressBar here */

                }

                override fun onErrorCallBack(e: VolleyError) {
                    try {
                        makeLog("Error:  $e")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    /*hide ProgressBar here */
                    hideProgress()
                }
            }
            showProgress()
            val videoId = VideoUtils.getVimeoVideoId(url).toString()
            Utils.getVideoURL(requireActivity(), videoId, content.owner_info.key, callback)
        }

    }


    fun startVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        videoTotalDuration: String,
        topicLearnpath: String
    ) {
        val intent = Intent(App.context, VideoPlayerActivity::class.java)

        intent.putExtra(AppConstants.VIDEO_ID, id)
        intent.putExtra(AppConstants.CONTENT_TYPE, type)
        intent.putExtra(AppConstants.VIDEO_CONCEPT_ID, conceptId)
        intent.putExtra(AppConstants.VIDEO_URL, url)
        intent.putExtra(AppConstants.VIDEO_TITLE, title)
        intent.putExtra(AppConstants.VIDEO_DESCRIPTION, description)
        intent.putExtra(AppConstants.VIDEO_SUBJECT, subject)
        intent.putExtra(AppConstants.VIDEO_CHAPTER, chapter)
        intent.putExtra(AppConstants.VIDEO_AUTHORS, authors)
        intent.putExtra(AppConstants.SOURCE_REFERENCE, AppConstants.PRACTICE)
        intent.putExtra(AppConstants.VIDEO_SOURCE, source)
        intent.putExtra(AppConstants.TOPIC_LEARN_PATH, topicLearnpath)
        intent.putExtra(AppConstants.IS_RECOMMENDATION_ENABLED, true)
        intent.putExtra(AppConstants.VIDEO_TOTAL_DURATION, videoTotalDuration)
        intent.putExtra(AppConstants.FORMAT_ID, data?.learning_map?.format_id)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, data?.learnpath_name)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, data?.learnpath_format_name)
        startActivity(intent)
    }


    private fun callCoobo(data: Content) {
        val i = Intent(requireContext(), UnityPlayerActivity::class.java)
        i.putExtra(AppConstants.COOBO_URL, data.url)
        i.putExtra(AppConstants.DETAILS_ID, data.id)
        i.putExtra(AppConstants.DURATION_LENGTH, data.length)
        i.putExtra(AppConstants.IS_RECOMMENDATION_ENABLED, true)
        insertBundle(data, i)
        startActivity(i)
    }
}
