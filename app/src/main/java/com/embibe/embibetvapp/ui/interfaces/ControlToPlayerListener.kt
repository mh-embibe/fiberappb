package com.embibe.embibetvapp.ui.interfaces

interface ControlToPlayerListener {
    fun playPause()
    fun rewind10Seconds()
    fun forward10Seconds()
    fun rewindByPercentage(seconds: Int)
    fun forwardByPercentage(seconds: Int)
    fun bookMark()
    fun bookMarkFocus()
    fun like()
    fun likeFocus()
    fun getCurrentDuration(): Long
    fun getVideoTotalDuration(): Long
    fun annotationPoint(duration: Long)

}
