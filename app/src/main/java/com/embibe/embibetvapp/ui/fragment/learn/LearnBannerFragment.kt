package com.embibe.embibetvapp.ui.fragment.learn


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.LEARN_BANNER_TEASER_VIDEO_WATCHED
import com.embibe.embibetvapp.databinding.CardBannerLearnBinding
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.ui.activity.SwitchGoalExamActivity
import com.embibe.embibetvapp.ui.activity.VideoPlayerActivity
import com.embibe.embibetvapp.ui.interfaces.BannerImageDataToHostListener
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.VideoUtils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.properties.Delegates


class LearnBannerFragment : BaseAppFragment() {

    lateinit var binding: CardBannerLearnBinding
    private lateinit var dPadKeysCallback: DPadKeysListener
    private lateinit var navigationMenuCallback: NavigationMenuCallback
    val classTag = LearnBannerFragment::class.java.toString()
    private var whichBtnFocused = ""
    private var BTN_LEARN = "0"
    private var BTN_GRADE = "2"
    private var canGainFocus = true
    private lateinit var currentID: String
    private lateinit var currentBannerData: BannerData
    val TAG = this.javaClass.name
    private lateinit var homeViewModel: HomeViewModel
    private var bannerDataSize by Delegates.notNull<Int>()
    var imageViewList = ArrayList<ImageView>()
    private lateinit var bannerImageDataToHostListener: BannerImageDataToHostListener
    lateinit var player: SimpleExoPlayer
    var repeatVideo = false
    var isPlayerInitialized: Boolean = false
    var videoPlaying = false
    lateinit var pref: PreferenceHelper
    lateinit var previewURl: String
    var mediaSource: MediaSource? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.card_banner_learn, container, false)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        pref = PreferenceHelper()
        doAsync {
            val learnBannerData = if (DataManager.instance.getHeroBannerData().data.isNotEmpty()) {
                DataManager.instance.getHeroBannerData().data
            } else {
                homeViewModel.fetchBannerSectionByPageName(AppConstants.LEARN) ?: arrayListOf()
            }
            uiThread {
                bannerDataSize = learnBannerData.size
                //initViewFlipper(learnBannerData as ArrayList<BannerData>)
            }
        }
        setViewAccToType()
        whichBtnFocused = BTN_LEARN
        binding.btnLearn.postDelayed({ binding.btnLearn.requestFocus() }, 100)
        btnLearnListeners()
        btnGradeListeners()
        return binding.root
    }


    override fun onStart() {
        super.onStart()
        initPlayer()
    }

    private fun setViewAccToType() {
        binding.btnLearn.visibility = View.VISIBLE
        binding.btnGrade.nextFocusLeftId = R.id.btnMoreInfo
    }

    private fun btnGradeListeners() {

        binding.btnGrade.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                if (canGainFocus) {
                    whichBtnFocused = BTN_GRADE
                    //update focused view
                    binding.btnGrade.setBackgroundResource(R.drawable.ic_banner_grade_selected)
                } else {
                    canGainFocus = true
                }
            } else {
                //update non focused view
                binding.btnGrade.setBackgroundResource(R.drawable.ic_banner_grade)
            }
        }

        binding.btnGrade.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!Utils.isKeyPressedTooFast(500))
                            dPadKeysCallback.isKeyPadDown(true, "Banner")
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        if (!videoPlaying)
                            startActivity(Intent(context, SwitchGoalExamActivity::class.java))
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (binding.btnLearn.visibility == View.VISIBLE) {
                            binding.btnLearn.postDelayed({
                                binding.btnLearn.requestFocus()
                            }, 0)
                        }
                    }
                }
            }
            false
        }
    }


    private fun btnLearnListeners() {

        binding.btnLearn.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                trackEventHomeMainBtnFocus(
                    bannerData,
                    com.embibejio.coreapp.constant.AppConstants.SLIDE_TYPE_LEARN
                )
                canGainFocus = true
                binding.btnLearn.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_play_circle_filled_black_24dp, 0, 0, 0
                )
                whichBtnFocused = BTN_LEARN
            } else {
                binding.btnLearn.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_play_circle_filled_white_24dp, 0, 0, 0
                )
            }
        }

        binding.btnLearn.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!Utils.isKeyPressedTooFast(500)) {
                            dPadKeysCallback.isKeyPadDown(true, "Banner")
                            pausePlayer()
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (!videoPlaying)
                            navigationMenuCallback.navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        trackEventHomeMainBtnClick(
                            bannerData,
                            com.embibejio.coreapp.constant.AppConstants.SLIDE_TYPE_LEARN
                        )
                        if (!videoPlaying) {
                            if (currentBannerData.type.toLowerCase(Locale.getDefault()) == AppConstants.VIDEO) {
                                callVideo(
                                    currentBannerData.id,
                                    AppConstants.VIDEO,
                                    "",
                                    currentBannerData.videoUrl,
                                    currentBannerData.title,
                                    currentBannerData.description,
                                    Utils.getVideoTypeUsingUrl(currentBannerData.videoUrl),
                                    "",
                                    "",
                                    "", "0", ""
                                )
                            }
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (binding.btnGrade.visibility == View.VISIBLE) {
                            binding.btnGrade.postDelayed({
                                binding.btnGrade.requestFocus()
                            }, 0)
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        if (!videoPlaying)
                            if (!Utils.isKeyPressedTooFast(1000)) {
                                dPadKeysCallback.isKeyPadUp(true, "Banner")
                            }
                    }
                }
            }
            false
        }
    }

    private fun callVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        duration: String,
        topicLearningPath: String
    ) {
        if (source == AppConstants.PLAYER_TYPE_YOUTUBE) {
            if (Utils.isYouTubeAppAvailable(requireContext())) {
                Utils.startYoutubeApp(requireContext(), url)
            } else {
                startVideo(
                    id,
                    type,
                    conceptId,
                    url,
                    title,
                    description,
                    source,
                    subject,
                    chapter,
                    authors,
                    duration,
                    topicLearningPath
                )
            }
        } else {
            val callback = object : BaseViewModel.APICallBackVolley {
                override fun <T> onSuccessCallBack(response: T) {
                    hideProgress()
                    response as JSONObject
                    val playableUrl = Utils.getVimeoHD((response["files"] as JSONArray))
                    startVideo(
                        id,
                        type,
                        conceptId,
                        playableUrl,
                        title,
                        description,
                        AppConstants.PLAYER_TYPE_EXOPLAYER,
                        subject,
                        chapter,
                        authors,
                        duration,
                        topicLearningPath
                    )
                    /*hide ProgressBar here */

                }

                override fun onErrorCallBack(e: VolleyError) {
                    try {
                        makeLog("Error:  $e")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    /*hide ProgressBar here */
                    hideProgress()
                }
            }
            showProgress()
            val videoId = VideoUtils.getVimeoVideoId(url).toString()
            Utils.getVideoURL(App.context, videoId, bannerData?.owner_info?.key, callback)
        }
    }

    override fun onResume() {
        super.onResume()
        Log.e("resume", "resume2")

        doAsync {
            val learnBannerData = if (DataManager.instance.getHeroBannerData().data.isNotEmpty()) {
                DataManager.instance.getHeroBannerData().data
            } else {
                homeViewModel.fetchBannerSectionByPageName(AppConstants.LEARN) ?: arrayListOf()
            }
            uiThread {
                initViewFlipper(learnBannerData as ArrayList<BannerData>)
                try {
                    val examName = DataManager.instance.getExamNameByCode(
                        UserData.getGoalCode(),
                        UserData.getExamCode()
                    )
                    binding.btnGrade.apply {
                        textSize = if (examName.length > 25)
                            11f
                        else
                            12f
                    }
                    binding.btnGrade.text = examName
                } catch (e: Exception) {

                }

            }
        }
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }

    fun setLastFocusedBtn() {
        canGainFocus = false
        binding.btnLearn.postDelayed({
            binding.btnLearn.requestFocus()
        }, 0)
        if (videoPlaying) {
            startPlayer()
        }
    }

    fun setContentData(data: BannerData) {
        currentID = data.id
        currentBannerData = data
        trackEventHomeBannerChange(
            data,
            0,
            com.embibejio.coreapp.constant.AppConstants.SLIDE_TYPE_COOBO
        )

        if (data.title_image_url == "") {
            binding.title.text = data.title
            showView(binding.title)
            binding.titleImage.visibility = View.GONE
            CoroutineScope(Main).launch {
                delay(2)
                binding.textTopic.text = data.subject
                binding.textTime.text = data.duration
                if (isAdded) {
                    binding.embiumsCount.text = getString(R.string.earn) + " 50"
                }
                showView(binding.textTopic)
                showView(binding.textTime)
                showView(binding.embiumsCount)
            }

        } else {
            binding.title.visibility = View.INVISIBLE
            setImage(data.title_image_url)
            showView(binding.titleImage)
            binding.textTopic.visibility = View.INVISIBLE
            binding.textTime.visibility = View.INVISIBLE
            binding.embiumsCount.visibility = View.INVISIBLE

            GlobalScope.launch(Main) {
                delay(2)
                binding.btnLearn.requestFocus()
            }
        }
        binding.textDescription.text = data.description
        showView(binding.textDescription)
        binding.btnLearn.text = data.buttonText
        Utils.changeBannerTitleTextSize(binding.title)
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    fun bannerImageDataListener(callback: BannerImageDataToHostListener) {
        this.bannerImageDataToHostListener = callback
    }


    private fun showView(view: View) {
        try {
            val animation: Animation =
                AnimationUtils.loadAnimation(requireContext(), R.anim.anim_fade_in)
            animation.duration = AnimSettings.FADE_IN_DURATION
            animation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation?) {}
                override fun onAnimationRepeat(animation: Animation?) {}
                override fun onAnimationEnd(animation: Animation?) {
                    view.visibility = View.VISIBLE
                }
            })
            view.startAnimation(animation)
        } catch (e: Exception) {

        }

    }


    object AnimSettings {
        const val FADE_IN_DURATION = 800L
        const val FADE_OUT_DURATION = 10L
    }

    private fun setImage(imgUrl: String) {
        Glide.with(requireContext())
            .load(Uri.parse(imgUrl))
            .into(binding.titleImage)
    }


    fun startVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        totalDuration: String,
        topicLearningPath: String
    ) {
        val intent = Intent(App.context, VideoPlayerActivity::class.java)

        intent.putExtra(AppConstants.VIDEO_ID, id)
        intent.putExtra(AppConstants.CONTENT_TYPE, type)
        intent.putExtra(AppConstants.VIDEO_CONCEPT_ID, conceptId)
        intent.putExtra(AppConstants.VIDEO_URL, url)
        intent.putExtra(AppConstants.VIDEO_TITLE, title)
        intent.putExtra(AppConstants.VIDEO_DESCRIPTION, description)
        intent.putExtra(AppConstants.VIDEO_SUBJECT, subject)
        intent.putExtra(AppConstants.VIDEO_CHAPTER, chapter)
        intent.putExtra(AppConstants.VIDEO_TOTAL_DURATION, totalDuration)
        intent.putExtra(AppConstants.VIDEO_AUTHORS, authors)
        intent.putExtra(AppConstants.SOURCE_REFERENCE, AppConstants.BOOK)
        intent.putExtra(AppConstants.TOPIC_LEARN_PATH, topicLearningPath)
        intent.putExtra(AppConstants.VIDEO_SOURCE, source)

        startActivity(intent)
    }


    private fun initViewFlipper(data: ArrayList<BannerData>) {

        for (i in 0 until 1) {
            context?.let {
                val imageView = ImageView(it)
                imageView.apply {
                    scaleType = ImageView.ScaleType.CENTER_CROP
                }
                imageViewList.add(imageView)
            }
            try {
                addImageView(data[i].imgUrl)
                setContentData(data[i])
                if (!pref[LEARN_BANNER_TEASER_VIDEO_WATCHED, false]) {
                    CoroutineScope(Main).launch {
                        delay(2000L)
                        try {
                            // temporary video
                            val videoId = VideoUtils.getVimeoVideoId(data[i].teaser_url).toString()
                            Utils.getVideoURL(
                                requireContext(),
                                videoId,
                                data[i].owner_info.teaser_key,
                                object :
                                    BaseViewModel.APICallBackVolley {
                                    override fun <T> onSuccessCallBack(it: T) {
                                        val response = it as JSONObject
                                        try {
                                            previewURl =
                                                Utils.getVimeoHD((response["files"] as JSONArray))
                                            android.util.Log.d("Success", "$it")
                                            showVideoView()
                                        } catch (e: Exception) {

                                        }

                                    }

                                    override fun onErrorCallBack(error: VolleyError) {
                                        error.printStackTrace()
                                    }
                                })
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            } catch (e: Exception) {
            }
        }
    }


    private fun addImageView(imgUrl: String) {
        val uri = Uri.parse(imgUrl)
        setImage(binding.vfBanner, uri)
    }

    private fun setImage(imageView: ImageView, uri: Uri) {
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        context?.let {
            Glide.with(it).setDefaultRequestOptions(requestOptions).load(uri)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView)
        }
    }


    fun showVideoView() {
        setUrl()
        hideContentView()
    }

    private fun initPlayer() {
        var playerView = binding.videoView
        var builder = DefaultTrackSelector.ParametersBuilder(requireContext())
        builder.setForceLowestBitrate(true)
        var rf = DefaultRenderersFactory(requireContext())
        var loadControl = DefaultLoadControl()
        var videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(DefaultBandwidthMeter())
        var trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
        var defaultTrackSelector = DefaultTrackSelector.ParametersBuilder().setForceLowestBitrate(
            true
        )
        trackSelector.setParameters(defaultTrackSelector)
        player = ExoPlayerFactory.newSimpleInstance(
            requireContext(),
            rf,
            trackSelector,
            loadControl
        )
        if (repeatVideo) player.repeatMode = Player.REPEAT_MODE_ONE
        playerView.player = player
        isPlayerInitialized = true
        playerStateListener()
    }

    private fun playerStateListener() {
        player.addListener(object : Player.EventListener {

            override fun onPlayerStateChanged(
                playWhenReady: Boolean,
                playbackState: Int
            ) {
                if (playWhenReady && playbackState == Player.STATE_READY) {
                    // Active playback.
                    //hideImageView&ShowVideoView
                    videoPlaying = true
                    hideContentView()
                } else if (playbackState == Player.STATE_ENDED) {
                    //The player finished playing all media
                    //ShowImageView&HideVideoView
                    videoPlaying = false
                    showContentView()
                    //setPreferenceForLearnBannerVideo
                    pref.put(LEARN_BANNER_TEASER_VIDEO_WATCHED, true)
                }
            }
        })
    }

    private fun hideContentView() {
        binding.title.visibility = View.GONE
        binding.textDescription.visibility = View.GONE
        binding.textTopic.visibility = View.GONE
        binding.embiumsCount.visibility = View.GONE
        binding.vfBanner.visibility = View.GONE
        binding.cardView.visibility = View.VISIBLE
        binding.videoView.visibility = View.VISIBLE
    }

    private fun showContentView() {
        binding.title.visibility = View.VISIBLE
        binding.textDescription.visibility = View.VISIBLE
        binding.textTopic.visibility = View.VISIBLE
        binding.embiumsCount.visibility = View.VISIBLE
        binding.vfBanner.visibility = View.VISIBLE
        binding.cardView.visibility = View.INVISIBLE
        binding.videoView.visibility = View.GONE
    }


    private fun setUrl() {
        mediaSource = buildMediaSource(Uri.parse(previewURl))
        if (mediaSource != null) {
            Log.e(TAG, "mediaSource not null")
            player.prepare(mediaSource!!, false, true)
            player.playWhenReady = true
        }
    }

    private fun buildMediaSource(uri: Uri): MediaSource? {
        val userAgent = "exoplayer-codelab"
        if (uri.lastPathSegment?.contains("mp3")!! || uri.lastPathSegment?.contains("mp4")!!) {
            android.util.Log.e(TAG, "Check")
            return ProgressiveMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
                .createMediaSource(uri)
        } else if (uri.lastPathSegment!!.contains("m3u8")) {
            return HlsMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
                .createMediaSource(uri)
        }
        return null
    }

    private fun releasePlayer() {
        player.release()
    }

    override fun onStop() {
        super.onStop()
        releasePlayer()
    }


    override fun onPause() {
        super.onPause()
        pausePlayer()
    }

    private fun pausePlayer() {
        binding.videoView.visibility = View.GONE
        player.release()
    }

    private fun startPlayer() {
        binding.videoView.visibility = View.VISIBLE
        initPlayer()
        setUrl()
    }
}
