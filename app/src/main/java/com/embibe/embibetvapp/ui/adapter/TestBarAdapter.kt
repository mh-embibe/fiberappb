package com.embibe.embibetvapp.ui.adapter

import android.view.ViewGroup
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemBarBinding
import com.embibe.embibetvapp.model.test.SectionSummary
import com.embibe.embibetvapp.ui.viewholders.BaseViewHolder
import com.embibe.embibetvapp.ui.viewholders.TestBarViewHolder
import com.embibe.embibetvapp.utils.Utils

class TestBarAdapter : BaseAdapter<SectionSummary>() {

    override fun getLayoutId(position: Int, obj: SectionSummary) = R.layout.item_bar

    override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<SectionSummary> {
        val binding = Utils.binder<ItemBarBinding>(R.layout.item_bar, parent)
        return TestBarViewHolder(binding, listItems)
    }
}