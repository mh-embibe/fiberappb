package com.embibe.embibetvapp.ui.activity

import android.graphics.DashPathEffect
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RectShape
import android.os.Bundle
import android.os.Handler
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.doOnLayout
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.TestFeedbackBaseFragmentActivity
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.model.test.Attempt
import com.embibe.embibetvapp.model.test.Question
import com.embibe.embibetvapp.model.test.TestAttemptsRes
import com.embibe.embibetvapp.model.test_feedback_scatter_chart.XYModel
import com.embibe.embibetvapp.utils.Utils
import java.io.InputStream
import java.util.*


open class ScatterGraphTestFeedBackActivity : TestFeedbackBaseFragmentActivity() {

    private var itemHasFoucs: Boolean = false
    private var startedAtInMilliSec: Long? = 0L
    private var isPlotted: Boolean = false
    private lateinit var drawable: Drawable
    private  var width: Int = 0
    private var height: Int = 0
    private lateinit var drawableTriangular: Drawable
    private  var widthTriangular: Int = 0
    private var heightTriangular: Int = 0
    private lateinit var data: ArrayList<XYModel>
    private lateinit var focusLightView: ImageView
    private lateinit var plottedHexagons: ArrayList<XYModel>
    private lateinit var testAttemptsRes: TestAttemptsRes
    private lateinit var durationTextView: TextView
    private lateinit var difficultyLevelTextView: TextView
    private lateinit var xDurationLP: FrameLayout.LayoutParams
    private lateinit var durationView: View
    private lateinit var difficultyLevelView: View
    private lateinit var line: View
    private lateinit var subjectDataRed: ArrayList<XYModel>
    private lateinit var subjectDataGreen: ArrayList<XYModel>
    private lateinit var plot: FrameLayout
    private lateinit var plotLine: FrameLayout

    //    private lateinit var pointer: ImageView
    private lateinit var xLabelsView: FrameLayout
    private lateinit var yLabelsView: FrameLayout
    private lateinit var hexagonLP: FrameLayout.LayoutParams
    private lateinit var textViewAttempt: TextView
    private lateinit var imageViewAttempt: ImageView
    private lateinit var textViewPercentage: TextView
    private lateinit var labelYAxis: TextView

    private lateinit var textViewYLabel: TextView
    private lateinit var graph: FrameLayout

    private var isLinesDrawn: Boolean = false
    private var scale: Float = 0.0f

    private var xAxisLabelInterval = 1F
    private var xAxisLabelSize = 10F
    private var xLabelCount = 10f

    private var yAxisLabelInterval = 2F
    private var yAxisLabelSize = 8F
    private var yLabelCount = 4f

    private var xAxisTotalWidth: Int = 0
    private var yAxisTotalHeight: Int = 0

    private var plotAreaTotalWidth: Int = 0
    private var plotAreaTotalHeight: Int = 0

    private var drawingXPosition = 0F
    private var drawingYPosition = 0F

    private var xPixelCount = 0F
    private var yPixelCount = 0F

    private var yPixelPlotAreaCount = 0F

    private val animDuration: Long = 100
    private val animDurationScaleIn: Long = 100//350
    private val animDurationScaleOut: Long = 10//550

    private var dpWidthInPx: Int = 0
    private var dpHeightInPx: Int = 0


    private var lineLp: FrameLayout.LayoutParams =
        FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, 1)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        scale = resources.displayMetrics.density
        dpWidthInPx = (13 * scale).toInt()
        dpHeightInPx = (13 * scale).toInt()
        hexagonLP = FrameLayout.LayoutParams(dpWidthInPx, dpHeightInPx)

        drawable = resources.getDrawable(R.drawable.hexagon_green_tiny)
        width = drawable.intrinsicWidth
        height = drawable.intrinsicHeight

        drawableTriangular = resources.getDrawable(R.drawable.triangular_glow_png)
        widthTriangular = drawable.intrinsicWidth
        heightTriangular = drawable.intrinsicHeight

        /* setContentView(R.layout.activity_test_qws)//setContentView(R.layout.activity_scatter_graph_test_feedback)
         val attemptType = "TooFastCorrect"

         testAttemptsRes = Utils.fromJson(readJSONFromAsset("test_feedback_attempts").toString())
         initScatter(testAttemptsRes,attemptType, Utils.getIconByAttemptType(attemptType))*/
    }

    private fun readJSONFromAsset(jsonDataFileName: String): String? {
        var json: String? = null
        try {
            val inputStream: InputStream = assets.open("$jsonDataFileName.json")
            json = inputStream.bufferedReader().use { it.readText() }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    fun initScatter(
        testAttemptsRes: TestAttemptsRes,
        attemptType: String,
        iconByAttemptType: Int,
        subject: String,
        questionList: ArrayList<Question>,
        allList: ArrayList<Question>
    ) {
        clearPlots()
        this.testAttemptsRes = testAttemptsRes
        data = arrayListOf()
        data = createScatterData(testAttemptsRes, attemptType)
        makeLog("set plot input Data : ${data.size}, attemptType : ${attemptType}")
        setPlotDescription(attemptType, iconByAttemptType)
        plot = findViewById<View>(R.id.plotArea) as FrameLayout
        plotLine = findViewById<View>(R.id.plotAreaLine) as FrameLayout
        //pointer = findViewById<View>(R.id.pointer) as ImageView
        xLabelsView = findViewById<View>(R.id.x_labels) as FrameLayout
        yLabelsView = findViewById<View>(R.id.y_labels) as FrameLayout
        calculateAttemptPercent(testAttemptsRes, attemptType, subject, questionList, allList)
        if (!isPlotted) {
            xAxisLabelSize = getMaxXValue(testAttemptsRes.attempts)
            xAxisLabelInterval = calculateInterval(xAxisLabelSize)

            xLabelCount = xAxisLabelSize / xAxisLabelInterval
            yAxisLabelSize = getMaxYValue(testAttemptsRes)
            yAxisLabelInterval = calculateYInterval(yAxisLabelSize)
            yAxisLabelSize +=yAxisLabelInterval
            yLabelCount = yAxisLabelSize / yAxisLabelInterval
            drawXYAxis()
        }else{
            setPoints(data)
        }
            
    }

    private fun calculateInterval(xAxisLabelSize: Float): Float {

        return when (xAxisLabelSize){
            in 1f..20f -> 1f
            in 20f..50f -> 2f
            in 50f..150f -> 10f
            in 150f..200f -> 20f
            in 200f..250f -> 30f
            in 250f..350f -> 40f
            else -> 1f
        }
    }
    private fun calculateYInterval(yAxisLabelSize: Float): Float {

        return when (yAxisLabelSize){
            in 1f..10f -> 2f
            in 10f..20f -> 4f
            in 20f..30f -> 6f
            in 30f..40f -> 8f
            in 40f..50f -> 10f
            else -> 2f
        }
    }

    private fun getMaxXValue(data: List<Attempt>?): Float {
        var xMax = xAxisLabelSize
        var minute =0f
        for (item in data!!){
            minute = getAttemptedTimeInMinute(item)
            if (xMax < minute) {
                xMax = minute
            }
        }
        xMax += xMax / 10
        return xMax
    }

    private fun getMaxYValue(data: TestAttemptsRes): Float {
        var yMax = yAxisLabelSize
        for (item in data.attempts!!) {
            if (yMax < item.blendedDifficulty!!) {
                yMax = item.blendedDifficulty!!.toFloat()
            }
        }
        return yMax
    }


    private fun calculateAttemptPercent(
        testAttemptsRes: TestAttemptsRes,
        attemptType: String,
        subject: String,
        questionList: ArrayList<Question>,
        allList: ArrayList<Question>
    ) {
        var percentage: Int = 0
        if (testAttemptsRes != null && testAttemptsRes.attempts != null && testAttemptsRes.attempts!!.size > 0) {
            var attemptSize =
                testAttemptsRes.attempts!!.filter {
                    it.attemptTypeBadge == attemptType && (subject == AppConstants.ALL_SUBJECTS || isQuestionAvailable(
                        it.questionCode!!,
                        questionList
                    ))
                }.size
            var totalSize =
                if (subject == AppConstants.ALL_SUBJECTS) testAttemptsRes.attempts!!.size else testAttemptsRes.attempts!!.filter {
                    isQuestionAvailable(
                        it.questionCode!!,
                        allList
                    )
                }.size
            percentage =
                if (attemptSize == 0) 0 else ((attemptSize.toDouble() / totalSize.toDouble()) * 100).toInt()
        }
        textViewPercentage.text = "$percentage%"
    }

    private fun isQuestionAvailable(qcode:String,questionList: ArrayList<Question>):Boolean {
        var pos=questionList.filter {it.code==qcode}.size
        return if(pos>0) true else false
    }

    private fun createScatterData(
        testAttemptsRes: TestAttemptsRes,
        attemptType: String
    ): ArrayList<XYModel> {
        val dataList = arrayListOf<XYModel>()
        val scatterDataGreen: ArrayList<XYModel> = arrayListOf()
        val scatterDataRed: ArrayList<XYModel> = arrayListOf()

        val list = testAttemptsRes.attempts?.filter { it.attemptTypeBadge.equals(attemptType) }
        startedAtInMilliSec = testAttemptsRes.startedAtInMilliSec?.div(1000L)
        for (index in list!!.indices) {
            when (list!![index].attemptTypeBadge) {
                AppConstants.QWA_ATTEMPT_TYPE.Perfect.name, AppConstants.QWA_ATTEMPT_TYPE.OvertimeCorrect.name, AppConstants.QWA_ATTEMPT_TYPE.TooFastCorrect.name -> {
                    scatterDataGreen.add(getXYModel(list!![index], 2))
                }
                AppConstants.QWA_ATTEMPT_TYPE.Wasted.name, AppConstants.QWA_ATTEMPT_TYPE.OvertimeIncorrect.name, AppConstants.QWA_ATTEMPT_TYPE.Incorrect.name, AppConstants.QWA_ATTEMPT_TYPE.NonAttempt.name -> {
                    scatterDataRed.add(getXYModel(list!![index], 1))
                }
            }
        }
        dataList.addAll(scatterDataGreen)
        dataList.addAll(scatterDataRed)
        return dataList
    }

    private fun getXYModel(attempt: Attempt, type: Int): XYModel {
        val model = XYModel()

        model.x = getAttemptedTimeInMinute(attempt)
        model.y = attempt.blendedDifficulty!!.toFloat()
        model.isHighlight = true
        model.type = type
        model.questionCode = attempt.questionCode!!


        return model
    }

    private fun getXMinute(lastSave: Int): Float {
        val x = lastSave - startedAtInMilliSec!!
        val minutes = x/60F
        return minutes
    }

    private fun getAttemptedTimeInMinute(attempt: Attempt): Float {
        var lastSave = attempt.tLastSave?:0
        if (lastSave==0){
            lastSave = attempt.tFirstSave?:0
        }
        if (lastSave==0){
            lastSave = attempt.tLastLook?:0
        }
        if (lastSave==0){
            lastSave = attempt.tFirstLook?:0
        }
        return getXMinute(lastSave)
    }

    private fun getAllData(): ArrayList<XYModel> {

        val dataList = arrayListOf<XYModel>()
        dataList.addAll(getPointsDataGreen())
        //dataList.addAll(subjectDataRed)
        return dataList
    }

    private fun getPointsDataRed(): ArrayList<XYModel> {
        var list = arrayListOf<XYModel>()
        var model = XYModel()

        list = arrayListOf<XYModel>()

        /*for (i in 1..22) {
            model = XYModel()
            model.x = Random.nextDouble(3.0, 10.0)
            model.y = Random.nextInt(1, 7)
            model.type = 1
            model.isHighlight = true
            model.alpha = 0.3F
            model.questionCode = i.toString()
            list.add(model)
        }*/

        list.sortBy { it.x }
        return list
    }

    private fun getPointsDataGreen(): ArrayList<XYModel> {
        val list = arrayListOf<XYModel>()
        var model = XYModel()
        /*for (i in 1..10) {
            model = XYModel()
            model.x = Random.nextDouble(2.0, 8.0)
            model.y = Random.nextInt(1, 7)
            model.type = 2
            model.isHighlight = true
            model.alpha = 0.6F
            model.questionCode = i.toString()
            list.add(model)
        }*/
        list.sortBy { it.x }
        return list
    }

    private fun setPlotDescription(attemptType: String, iconByAttemptType: Int) {
        textViewAttempt = findViewById<TextView>(R.id.txt_attempts) as TextView
        imageViewAttempt = findViewById<ImageView>(R.id.image_attempts) as ImageView
        textViewPercentage = findViewById<TextView>(R.id.text_percentage) as TextView

        textViewAttempt.text = Utils.getAttemptFromKey(attemptType)
        imageViewAttempt.setImageResource(iconByAttemptType)
    }

    private fun drawXYAxis() {
        (xLabelsView as View).doOnLayout {
            xAxisTotalWidth = it.width
            initXAxis()
        }

        (yLabelsView as View).doOnLayout {
            yAxisTotalHeight = it.height
            initYAxis()
            drawLines()
            if (isLinesDrawn) {
                isLinesDrawn = false
                doPlotting()
            }
        }

        plot.doOnLayout {
            plotAreaTotalHeight = it.height
            plotAreaTotalWidth = it.width
            drawLines()
            if (isLinesDrawn) {
                isLinesDrawn = false
                doPlotting()
            }
        }

    }

    private fun doPlotting() {
        if (!isPlotted) {
            setPoints(data)
        }
    }

    private fun initXAxis() {
        val xLabelCount = xAxisLabelSize / xAxisLabelInterval
        makeLog("x max f xLabelCount $xLabelCount xAxisLabelSize: $xAxisLabelSize xAxisLabelInterval:$xAxisLabelInterval")
        val list = generateItems(xAxisLabelInterval, xAxisLabelSize)
        createTextViews(0, xLabelsView, xLabelCount, list, xAxisTotalWidth, 0)
    }

    private fun initYAxis() {
        yLabelsView.removeAllViews()
        val list = generateItems(yAxisLabelInterval, yAxisLabelSize)
        createTextViews(1, yLabelsView, yLabelCount, list, 0, yAxisTotalHeight)
    }

    private fun generateItems(interval: Float, size: Float): IntArray {
        val num = IntArray((size / interval).toInt())
        val total = size
        for (i in 0 until num.size - 1) {
            if (i != 0) {
                num[i] = num[i - 1] + interval.toInt()
            } else {
                num[i] = interval.toInt()
            }
        }
//        num[num.size - 1] = total.toInt()
        Arrays.sort(num)
        return num
    }

    private fun drawLines() {
        /*draw lines*/
        plotLine.removeAllViews()

        var id = Integer.MAX_VALUE

        if (yAxisTotalHeight > 0 && plotAreaTotalHeight > 0) {
            yPixelPlotAreaCount = yAxisTotalHeight / yLabelCount
            for (i in 1..yLabelCount.toInt()) {
                line = View(this)
                line.id = id
                id--
                plotLineViewOnPlottingArea(plotLine, line, 0F, getPlotYLinePoint(i))
            }
            isLinesDrawn = true
        }
    }


    private fun plotLineViewOnPlottingArea(rl: FrameLayout, line: View, x: Float, y: Float) {

        line.x = x
        line.y = y
        line.layoutParams = lineLp
        //line.alpha = 0.5F
        line.background = createDashedLined()
        //line.setBackgroundColor(ContextCompat.getColor(this, R.color.colorGreenGridLine))
        rl.addView(line)
    }

    private fun createDashedLined(): Drawable? {
        val sd = ShapeDrawable(RectShape())
        val fgPaintSel: Paint = sd.paint
        fgPaintSel.color = ContextCompat.getColor(this, R.color.colorGreenGridLine)
        fgPaintSel.style = Paint.Style.STROKE
        fgPaintSel.pathEffect = DashPathEffect(floatArrayOf(4f, 8f), 0F)
        return sd
    }

    private fun createTextViews(
        type: Int,
        rl: FrameLayout,
        labelCount: Float,
        str: IntArray, width: Int, height: Int
    ) {
        when (type) {
            0 -> {
                xPixelCount = width / labelCount
                drawingXPosition = xPixelCount
                setZero(rl, type)
                for (i in 1 until str.size) {
                    durationView =
                        LayoutInflater.from(this).inflate(R.layout.x_axis_duration_layout, null)
                    drawDurationTextView(rl, durationView, str[i].toString(), false)
                    if (i == str.size) {
                        drawingXPosition -= (scale * xPixelCount/2).toInt()
                    }
                    setTextLabelPosition(durationView, drawingXPosition, 0F, type)
                    makeLog("drawingXPosition :  $drawingXPosition")
                    drawingXPosition += xPixelCount
                }
            }
            1 -> {
                yPixelCount = height / labelCount
                yPixelCount += 1
                drawingYPosition = height - yPixelCount
                for (i in 1 until str.size) {
                    difficultyLevelView =
                        LayoutInflater.from(this).inflate(R.layout.difficulty_level_layout, null)
                    drawYCustomTextLabels(rl, difficultyLevelView, str[i].toString(), false)
                    setTextLabelPosition(difficultyLevelView, 0F, drawingYPosition, type)
                    drawingYPosition -= yPixelCount
                }
            }
        }
    }

    private fun setZero(rl: FrameLayout, type: Int) {
        when (type) {
            0 -> {
                val view =
                    LayoutInflater.from(this).inflate(R.layout.duration_layout_zero_small, null)
                drawDurationTextView(rl, view, "0", true)
                setTextLabelPosition(view, 0F, 0F, type)

            }
            1 -> {
                /* val view = LayoutInflater.from(this).inflate(R.layout.difficulty_level_layout_zero, null)
                 drawYCustomTextLabels(rl, view, "D.L", true)
                 setTextLabelPosition(view, 0, yAxisTotalHeight-20,0)*/
            }
        }

    }

    private fun setTextLabelPosition(view: View, x: Float, y: Float, type: Int) {
        xDurationLP = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.WRAP_CONTENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )
        when (type) {
            0 -> xDurationLP.setMargins(x.toInt(), y.toInt(), 0, 0)
            1 -> xDurationLP.setMargins(x.toInt(), y.toInt(), 0, 0)
        }

        view.layoutParams = xDurationLP
    }

    private fun drawYCustomTextLabels(rl: FrameLayout, view: View, text: String, isZero: Boolean) {

        difficultyLevelTextView = view.findViewById<TextView>(R.id.tvDifficultyLevelLabel)
        difficultyLevelTextView.setPadding(16, 0, 0, 0)
        difficultyLevelTextView.text = text
        difficultyLevelTextView.gravity = Gravity.END
        difficultyLevelTextView.setTextColor(
            ContextCompat.getColor(
                this,
                R.color.colorGreenGridLine
            )
        )
        difficultyLevelTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 7F)
        rl.addView(view)
    }

    private fun drawDurationTextView(rl: FrameLayout, view: View, text: String, isZero: Boolean) {
        if (isZero) {
            durationTextView = view.findViewById<TextView>(R.id.tvDurationLabel_zero)
            durationTextView.setPadding(0, 6, 0, 0)
        } else {
            durationTextView = view.findViewById<TextView>(R.id.tvDurationLabel)
            durationTextView.setPadding(0, 6, 0, 0)
        }
        durationTextView.text = text
        durationTextView.gravity = Gravity.CENTER_VERTICAL
        durationTextView.setTextColor(ContextCompat.getColor(this, R.color.colorGreenGridLine))
        durationTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 6F)
        rl.addView(view)
    }

    private fun setPoints(data: ArrayList<XYModel>) {
        plottedHexagons = arrayListOf<XYModel>()
        val xSinglePixel = (plotAreaTotalWidth / xLabelCount) / xAxisLabelInterval
        val yPixelCount = (plotAreaTotalHeight / yLabelCount)
        val ySinglePixel = yPixelCount / yAxisLabelInterval

        var imagePoint = ImageView(this)
        var autoIncrement = 1
        for (xYModel in data) {
            imagePoint = ImageView(this)
            imagePoint.id = autoIncrement
            xYModel.id = autoIncrement
            xYModel.view = imagePoint
            ++autoIncrement
            createPoint(xYModel, imagePoint, xSinglePixel, ySinglePixel)
        }
        isPlotted = true
    }

    private fun createPoint(
        xYModel: XYModel, imagePoint: ImageView, xSinglePixel: Float, ySinglePixel: Float
    ) {
        makeLog("xYModel x:${xYModel.x} ,  y:${xYModel.y}")
        imagePoint.isFocusable = true
        plottedHexagons.add(xYModel)
        plotPointOnPlottingArea(plot, imagePoint, xYModel.type)
        val x = xYModel.x
        val widthCenter = width/2
        var heightCenter = height/2
        heightCenter += 5
        imagePoint.x = (xSinglePixel * x) - widthCenter
        val calY = (ySinglePixel * xYModel.y)
        imagePoint.y = (yAxisTotalHeight - calY) - heightCenter
        /*if (xYModel.type==2){
            xYModel.alpha = 0.7f
            imagePoint.alpha = 0.7f
        }else{
            xYModel.alpha = 0.3f
            imagePoint.alpha = 0.3f
        }*/
        setListenersOnPoint(imagePoint)
    }


    private fun plotPointOnPlottingArea(rl: FrameLayout, iv: ImageView, type: Int) {
        val scale = resources.displayMetrics.density
        val dpWidthInPx = (40 * scale).toInt()
        val dpHeightInPx = (40 * scale).toInt()
        val layoutParams = FrameLayout.LayoutParams(dpWidthInPx, dpHeightInPx)
        iv.layoutParams = layoutParams
        setImageRes(iv, type)

        rl.addView(iv)
    }

    private fun setImageRes(itemView: View, type: Int) {

        (itemView as ImageView).setImageResource(
            when (type) {
                1 -> R.drawable.hexagon_red
                2 -> R.drawable.hexagon_green
                else -> R.drawable.hexagon_red
            }
        )
    }

    private fun setListenersOnPoint(imagePoint: ImageView) {

        imagePoint.setOnClickListener {
            val item = getItem(it.id)
            makeLog("clicked item : ${it.id} item : ${item.type}")
        }

        imagePoint.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                itemHasFoucs = true
                v.nextFocusRightId = v.id + 1
                v.nextFocusLeftId = v.id - 1
                val item = getItem(v.id)
                highlightPlotView(v,item)
                /*highlightXView(item.x)
                highlightYView(item.y)*/
            } else {
                itemHasFoucs = false
                val item = getItem(v.id)
                deHighlightPlotView(v,item)
                Handler().postDelayed({


                }, 100)

            }
        }
    }

    fun highlightLightFocusView(item: XYModel) {
        removeLightFocusView()
        focusLightView = ImageView(this)
        focusLightView.setImageResource(R.drawable.triangular_glow_png)
        val scale = resources.displayMetrics.density
        val dpWidthInPx = (40 * scale).toInt()
        val dpHeightInPx = yAxisTotalHeight - item.view.y
        val layoutParams = FrameLayout.LayoutParams(dpWidthInPx, dpHeightInPx.toInt())
        val y = item.view.y.toInt()
        val x = item.view.x.toInt()
        layoutParams.setMargins(x, y, 0, 0)
        focusLightView.layoutParams = layoutParams
        plot.addView(focusLightView)
    }

    fun removeLightFocusView() {
        try {
            if (::focusLightView.isInitialized)
                plot.removeView(focusLightView)
        } catch (e: Exception) {

        }
    }

    fun highlightPlotView(v:View, item: XYModel) {
        reduceAlphaToAll()
        v.alpha = 1f
        v.scaleX = 1.6F
        v.scaleY = 1.6F
        setImageResOnFocus(v, item.type)
        highlightLightFocusView(item)
    }

    private fun resetAlpha() {
        if (::plottedHexagons.isInitialized && !plottedHexagons.isNullOrEmpty()) {
            for (item in plottedHexagons) {
                val itemView = plot.getChildAt(item.id)
                if (itemView != null) {
                    itemView.scaleX = 1F
                    itemView.scaleY = 1F
                    setImageRes(itemView, item.type)
                    if (item.type == 2) {
                        itemView.alpha = 1f
                    } else {
                        itemView.alpha = 1f//item.alpha
                    }
                }
            }
        }
    }

    private fun reduceAlphaToAll() {
        if (::plottedHexagons.isInitialized && !plottedHexagons.isNullOrEmpty()) {
            for (item in plottedHexagons) {
                val itemView = plot.getChildAt(item.id)
                if (itemView != null) {
                    itemView.scaleX = 1F
                    itemView.scaleY = 1F
                    setImageRes(itemView, item.type)
                    if (item.type == 2) {
                        itemView.alpha = 0.7f
                    } else {
                        itemView.alpha = 0.3f
                    }
                }
            }
        }
    }

    private fun setPointerConstraints(
        imagePoint: View, item: XYModel
    ) {
//        pointer.visibility = View.VISIBLE
//
////        pointer.x = imagePoint.translationX
////        pointer.y = imagePoint.translationY
//
//        val scale = resources.displayMetrics.density
//        val dpWidthInPx = (item.view.width * scale).toInt()
//        val dpHeightInPx = yAxisTotalHeight - item.view.y
//        val layoutParams = ConstraintLayout.LayoutParams(dpWidthInPx, dpHeightInPx.toInt())
//        val y = item.view.y
//        layoutParams.setMargins(item.view.x.toInt()-(item.view.width/3 * scale).toInt(), y.toInt(), 0, 0)
//        pointer.layoutParams = layoutParams

//        if (imagePoint.id != null) {
//            val constraintSet = ConstraintSet()
//            constraintSet.clone(plot)
//            constraintSet.connect(pointer.id, START, imagePoint.id, START, 0)
//            constraintSet.connect(pointer.id, END, imagePoint.id, END, 0)
//            constraintSet.applyTo(plot)
//        }
    }

    fun deHighlightPlotView(v:View,xyModel: XYModel) {

        v.scaleX = 1F
        v.scaleY = 1F
        if (itemHasFoucs) {
            if (xyModel.type == 2) {
                v.alpha = 0.7f
            } else {
                v.alpha = 0.3f
            }
        }else{
            removeLightFocusView()
            resetAlpha()
        }
        setImageRes(v, xyModel.type)
    }

    private fun setImageResOnFocus(imagePoint: View, type: Int) {

        (imagePoint as ImageView).setImageResource(
            when (type) {
                1 -> R.drawable.hexagon_red_tiny_glow_png
                2 -> R.drawable.hexagon_green_tiny_glow_png
                else -> R.drawable.hexagon_red_tiny_glow_png
            }
        )
    }

    private fun highlightXView(value: Float) {
        try {
            if (xLabelsView.childCount > 0) {
                val childAt = (value / xAxisLabelInterval).toInt()
                for (i in 1..xLabelsView.childCount) {
                    val itemView = xLabelsView.getChildAt(i)
                    if (itemView != null) {
                        if (i == childAt) {
                            itemView.alpha = 1f
                            val durationTextView =
                                itemView.findViewById<TextView>(R.id.tvDurationLabel)
                            durationTextView.setTextColor(
                                ContextCompat.getColor(this, R.color.colorWhite)
                            )
                            durationTextView.setTypeface(durationTextView.typeface, Typeface.BOLD)
                            val lineView = itemView.findViewById<View>(R.id.x_line)
                            lineView.setBackgroundColor(
                                ContextCompat.getColor(this, R.color.colorWhite)
                            )
                        } else {
                            itemView.alpha = 1f
                            val durationTextView =
                                itemView.findViewById<TextView>(R.id.tvDurationLabel)
                            durationTextView.setTextColor(
                                ContextCompat.getColor(this, R.color.colorGreenGridLine)
                            )
                            durationTextView.setTypeface(durationTextView.typeface, Typeface.NORMAL)
                            val lineView = itemView.findViewById<View>(R.id.x_line)
                            lineView.setBackgroundColor(
                                ContextCompat.getColor(this, R.color.colorGreenGridLine)
                            )
                        }

                    } else {
                        makeLog("itemView is null ${xLabelsView.childCount}")
                    }
                }
            }
        }
        catch (e:Exception){}
    }

    private fun highlightYView(value: Float) {
        if (yLabelsView.childCount > 0) {
            val childAt = (value / yAxisLabelInterval) - 1
            for (i in 1..yLabelsView.childCount) {
                val itemView = yLabelsView.getChildAt(i)
                if (itemView != null) {
                    if (i == childAt.toInt()) {
                        itemView.alpha = 1f
                        val durationTextView =
                            itemView.findViewById<TextView>(R.id.tvDifficultyLevelLabel)
                        durationTextView.setTextColor(
                            ContextCompat.getColor(this, R.color.colorWhite)
                        )
                        durationTextView.setTypeface(durationTextView.typeface, Typeface.BOLD)
                    } else {
                        itemView.alpha = 1f
                        val durationTextView =
                            itemView.findViewById<TextView>(R.id.tvDifficultyLevelLabel)
                        durationTextView.setTextColor(
                            ContextCompat.getColor(this, R.color.colorGreenGridLine)
                        )
                        durationTextView.setTypeface(durationTextView.typeface, Typeface.NORMAL)
                    }

                } else {
                    makeLog("itemView is null ${yLabelsView.childCount}")
                }
            }
        }
    }

    interface CompleteListener {
        fun onComplete(view: View)
    }

    private fun getItem(id: Int): XYModel {
        val items = plottedHexagons.filter { it.view.id == id }
        return items[0]
    }

    fun getItem(code: String): XYModel? {
        return if (::plottedHexagons.isInitialized) {
            val items = plottedHexagons.filter { it.questionCode == code }
            if (items.isNotEmpty()) items[0] else null
        }else null
    }

    private fun getPlotYLinePoint(value: Int): Float {
        return plotAreaTotalHeight - (yPixelPlotAreaCount * value)
    }

    private fun alphaAnim(view: View, alpha: Float) {
        view.alpha = alpha
    }

    fun clearPlots() {
        if (::plottedHexagons.isInitialized && !plottedHexagons.isNullOrEmpty()) {
            plot.removeAllViews()
        }
        if (::focusLightView.isInitialized && ::plot.isInitialized) {
            plot.removeView(focusLightView)
        }
    }

//    private fun scaleInAnimate(view: View, callback: CompleteListener) {
//        runOnUiThread {
//            view.animate().withStartAction {
//            }.withEndAction {
//                callback.onComplete(view)
//                scaleOutAnimate(view)
//                //view.alpha = 0.5F
//            }.scaleX(1.6F).scaleY(1.6F).duration = animDurationScaleIn
//        }
//    }

//    private fun scaleOutAnimate(view: View/*,callback:CompleteListener*/) {
//        runOnUiThread {
//            view.animate().withStartAction {
//            }.withEndAction {
//                //view.alpha = view.alpha
//                //callback.onComplete(view)
//            }.scaleX(1F).scaleY(1F).duration = animDurationScaleOut
//        }
//    }


//    private fun doScaleInOutAnimation(data: ArrayList<XYModel>) {
//        data.shuffle()
//        var position = 0
//        var xYModel = data[position]
//        val callback = object : CompleteListener {
//            override fun onComplete(view: View) {
//                ++position
//                if (data.size > position) {
//                    xYModel = data[position]
//                    //view.visibility = VISIBLE
//                    scaleInAnimate(xYModel.view, this)
//                }
//            }
//
//        }
//        scaleInAnimate(xYModel.view, callback)
//
//    }

//    private fun setPosition(view: TextView, x: Int?, y: Int?) {
//        val lp: FrameLayout.LayoutParams =
//            FrameLayout.LayoutParams(
//                FrameLayout.LayoutParams.WRAP_CONTENT,
//                FrameLayout.LayoutParams.WRAP_CONTENT
//            )
//        lp.setMargins(x!!, y!!, 0, 0)
//        view.layoutParams = lp
//    }
//
//    private fun setPosition(view: ImageView, x: Int?, y: Int?) {
//        val lp: FrameLayout.LayoutParams =
//            FrameLayout.LayoutParams(20, 20)
//        lp.setMargins(x!!, y!!, 0, 0)
//        view.layoutParams = lp
//
//    }
//
//    private fun setLinePosition(view: ImageView, x: Int?, y: Int?) {
//        lineLp = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, 1)
//        lineLp.setMargins(x!!, y!!, 0, 0)
//        view.layoutParams = lineLp
//    }

//    fun drawTextView(rl: FrameLayout, tv: TextView, text: String, type: Int) {
//        tv.text = text
//        tv.gravity = Gravity.CENTER_HORIZONTAL
//
//        tv.setTextColor(ContextCompat.getColor(this, R.color.colorGreenGridLine))
//        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10F)
//        when (type) {
//            0 -> {
//                tv.setPadding(0, 6, 0, 2)
//            }
//            1 -> {
//                tv.setPadding(10, 0, 0, 0)
//            }
//        }
//        rl.addView(tv)
//    }
}