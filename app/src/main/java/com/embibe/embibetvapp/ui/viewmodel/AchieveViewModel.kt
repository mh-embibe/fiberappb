package com.embibe.embibetvapp.ui.viewmodel

import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.UserData.getGoal
import com.embibe.embibetvapp.model.UserData.getGrade
import com.embibe.embibetvapp.model.achieve.AchieveListBase
import com.embibe.embibetvapp.model.achieve.Crunching.CrunchingCount
import com.embibe.embibetvapp.model.achieve.PercentageConnectionsRes
import com.embibe.embibetvapp.model.achieve.achieveFeedback.AchieveFeedbackRes
import com.embibe.embibetvapp.model.achieve.futureSuccess.FutureSuccessRes
import com.embibe.embibetvapp.model.achieve.keyconcepts.KeyConceptModel
import com.embibe.embibetvapp.model.achieve.paj.PAJCreateReq
import com.embibe.embibetvapp.model.achieve.readiness.Readiness
import com.embibe.embibetvapp.model.achieve.successStories.SuccessStory
import com.embibe.embibetvapp.model.diagnostic.TestDiagnosticRes
import com.embibe.embibetvapp.model.potential.Potential
import com.embibe.embibetvapp.network.repo.AchieveRepository
import com.embibe.embibetvapp.network.retrofit.RetrofitClient
import com.embibe.embibetvapp.utils.Utils.getChildId
import com.embibe.embibetvapp.utils.data.DataManager
import com.google.gson.internal.LinkedTreeMap

open class AchieveViewModel : BaseViewModel() {

    val achieveApiPreprod = AchieveRepository(RetrofitClient.achieveApiPreprod!!)

    fun getPercentageConnectionsApi(callback: APICallBacks<PercentageConnectionsRes>) {
        var examName = DataManager.instance.getExamNameByCode(
            UserData.getGoalCode(),
            UserData.getExamCode()
        )
        safeApi(
            achieveApiPreprod.fetchPercentageConnections(
                getChildId(),
                getGrade(),
                examName,
                getGoal()
            ), callback
        )
    }

    fun getKeyRelationsApi(
        from_goal: String,
        to_goal: String,
        from_exam: String,
        to_exam: String,
        level: String,
        count: String,
        callback: APICallBacks<ArrayList<KeyConceptModel>>
    ) {
        safeApi(
            achieveApiPreprod.fetchKeyRelations(
                getChildId(),
                from_goal,
                to_goal,
                from_exam,
                to_exam,
                level,
                count
            ), callback
        )
    }

    fun getReadinessApi(examCode: String, callback: APICallBacks<Readiness>) {
        safeApi(
            achieveApiPreprod.fetchReadiness(
                getChildId(),
                examCode,
                getGrade().toString()
            ), callback
        )
    }

    fun getFutureSuccessApi(callback: APICallBacks<List<FutureSuccessRes>>) {
        safeApi(achieveApiPreprod.fetchFutureSuccess(getChildId()), callback)
    }

    fun getSuccessStories(callback: APICallBacks<List<SuccessStory>>) {
        safeApi(
            achieveApiPreprod.fetchSuccessStoriess(
                getChildId(),
                getGrade().toString()
            ), callback
        )
    }

    fun getEducationQualification(callback: APICallBacks<AchieveListBase>) {
        safeApi(
            achieveApiPreprod.fetchEducationQualification(), callback
        )
    }

    fun getJobOptions(callback: APICallBacks<AchieveListBase>) {
        safeApi(
            achieveApiPreprod.fetchJobOptions(), callback
        )
    }

    fun getColleges(callback: APICallBacks<AchieveListBase>) {
        safeApi(
            achieveApiPreprod.fetchColleges(), callback
        )
    }

    fun getStates(callback: APICallBacks<AchieveListBase>) {
        safeApi(
            achieveApiPreprod.fetchStates(), callback
        )
    }

    fun getTruePotentialPrePG(callback: APICallBacks<List<Potential>>) {
        safeApi(
            achieveApiPreprod.fetchTruePotentialPrePG(), callback
        )
    }

    fun getTruePotentialPreUG(callback: APICallBacks<List<Potential>>) {
        safeApi(
            achieveApiPreprod.fetchTruePotentialPreUG(), callback
        )
    }

    fun getTruePotentialK12(callback: APICallBacks<List<Potential>>) {
        safeApi(
            achieveApiPreprod.fetchTruePotentialK12(), callback
        )
    }

    fun getCrunchingCount(callback: APICallBacks<CrunchingCount>) {
        safeApi(
            achieveApiPreprod.fetchCrunchingCount(), callback
        )
    }

    fun getDiagnosticTest(exam: String, goal: String, callback: APICallBacks<TestDiagnosticRes>) {
        safeApi(
            achieveApiPreprod.fetchDiagnosticTest(exam, goal), callback
        )
    }

    fun updateAchieveUser(status:Boolean,callback: APICallBacks<LinkedTreeMap<Any,Any>>) {
        safeApi(
            achieveApiPreprod.updateAchieveUserProfile(status), callback
        )
    }

    fun getAchieveFeedbackRes(bundle_code:String,paj_id:String,callback: APICallBacks<AchieveFeedbackRes>) {
        safeApi(
            achieveApiPreprod.fetchAchieveFeedbackRes(bundle_code,paj_id), callback
        )
    }

    fun createPAJ(model: PAJCreateReq, callback: APICallBacks<AchieveFeedbackRes>) {
        safeApi(
            achieveApiPreprod.createPAJ(model), callback
        )
    }
}
