package com.embibe.embibetvapp.ui.fragment.achieve

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.databinding.FragmentPotentialPreUgBinding
import com.embibe.embibetvapp.ui.adapter.*

class PotentialPreUGFragment : BaseAppFragment() {

    private lateinit var binding: FragmentPotentialPreUgBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_potential_pre_ug, container, false)
        setAdapterOnRv()
        micSearchButtonListeners()
        setBgOnInstitution()
        binding.lvDiscoverWhereYouStand.setAnimation(R.raw.discover_where_you_stand_no_text)
        return binding.root
    }

    private fun setAdapterOnRv() {
        var timePotentialAdapter = RvAdapterTimePotential()
        binding.rvTimeSpend.adapter = timePotentialAdapter

        var classAdapter = RvAdapterClass()
        binding.rvClass.adapter = classAdapter

        var genderAdapter = RvAdapterGender()
        binding.rvGender.adapter = genderAdapter

        var categoryAdapter = RvAdapterCategory()
        binding.rvCategory.adapter = categoryAdapter

        var diffAbledAdapter = RvAdapterDiffAdbled()
        binding.rvDiffAbled.adapter = diffAbledAdapter
    }

    private fun micSearchButtonListeners() {

        binding.layoutDreamCollege.micSearch.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.layoutDreamCollege.micSearch.setImageResource(R.mipmap.ic_mic_selected)
            } else {
                binding.layoutDreamCollege.micSearch.setImageResource(R.mipmap.ic_mic_unselected)
            }
        }

        binding.layoutDreamCollege.micSearch.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {

                        //binding.customInputView.restoreSelection("Mic")
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_DPAD_UP -> {
                        binding.layoutDreamCollege.micSearch.postDelayed({
                            binding.layoutDreamCollege.micSearch.requestFocus()
                        }, 0)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {

                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {

                    }
                    KeyEvent.KEYCODE_VOICE_ASSIST -> {
//                        Toast.makeText(context, "Voice assist pressed", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            false
        }
    }

    private fun setBgOnInstitution() {
        binding.layoutDreamCollege.clNational.setBackgroundResource(R.drawable.shape_rec_green_corner_rad7)

        binding.layoutDreamCollege.clNational.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                nationalCollegeClickListener()
            }
        }
        binding.layoutDreamCollege.clState.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stateCollegeClickListener()
            }
        }
    }

    private fun nationalCollegeClickListener() {
        binding.layoutDreamCollege.clNational.setOnClickListener {
            binding.layoutDreamCollege.clNational.setBackgroundResource(R.drawable.shape_rec_green_corner_rad7)
            binding.layoutDreamCollege.clState.setBackgroundResource(R.drawable.bg_carosel_acheive_potential_unselect)
        }
    }

    private fun stateCollegeClickListener() {
        binding.layoutDreamCollege.clNational.setOnClickListener {
            binding.layoutDreamCollege.clNational.setBackgroundResource(R.drawable.bg_carosel_acheive_potential_unselect)
            binding.layoutDreamCollege.clState.setBackgroundResource(R.drawable.shape_rec_green_corner_rad7)
        }
    }
}