package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemExamsBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.adduser.Exam
import com.embibe.embibetvapp.ui.activity.AddGoalsExamsActivity
import com.embibe.embibetvapp.utils.SegmentUtils

class PrimaryGoalExamAdapter(var context: Context) :
    RecyclerView.Adapter<PrimaryGoalExamAdapter.ExamViewHolder>() {

    private lateinit var list: ArrayList<Exam>
    var onItemClick: ((Exam) -> Unit)? = null
    var onFocusChange: ((Exam, Boolean) -> Unit)? = null
    var selectedItem = -1
    private var currentPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExamViewHolder {

        val binding: ItemExamsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_exams, parent, false
        )
        return ExamViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ExamViewHolder, position: Int) {
        currentPosition = position
        holder.bind(list[position])

        if (selectedItem == position) {
            holder.binding.textGoalExam.setBackgroundResource(R.drawable.bg_goals_slected_selector)
            holder.binding.textGoalExam.setTextColor(context.getColor(R.color.email_font_color))
        }
    }

    fun setData(data: ArrayList<Exam>) {
        list = data
        setDefaultFocusToSupportedGoalExam(list)
        notifyDataSetChanged()
    }

    private fun setDefaultFocusToSupportedGoalExam(examsList: ArrayList<Exam>) {
        try {
            val examCode = UserData.getCurrentProfile()!!.primary_exam_code
            for (index in examsList.indices) {
                if (examCode == examsList[index].code /*primaryGoals[index].default*/) {
                    setDefaultSelectedItem(index)
                    AddGoalsExamsActivity.primaryGoalExamCode = this.list[index].code
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setDefaultSelectedItem(position: Int) {
        selectedItem = position
        notifyDataSetChanged()
    }

    inner class ExamViewHolder(var binding: ItemExamsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Exam) {
            binding.exam = item
            binding.executePendingBindings()

            binding.textGoalExam.background = context.getDrawable(R.drawable.bg_button_goals)
            binding.textGoalExam.setTextColor(context.getColor(R.color.white))


            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
                currentPosition = adapterPosition
                if (item.supported) {
                    SegmentUtils.trackProfileExamPrimaryGoalValueCLick(
                        list[adapterPosition].name,
                        adapterPosition,
                        ""
                    )
                    onItemClick?.invoke(list[adapterPosition])

                    if (selectedItem != adapterPosition) {
                        val previousItem = selectedItem
                        selectedItem = adapterPosition

                        notifyItemChanged(previousItem)
                    } else
                        selectedItem = -1
                    notifyItemChanged(adapterPosition)
                }

            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (adapterPosition != -1) {
                    onFocusChange?.invoke(list[adapterPosition], hasFocus)
                    SegmentUtils.trackProfileExamPrimaryGoalValueFocus(
                        list[adapterPosition].name,
                        adapterPosition
                    )
                }
            }

        }
    }
}
