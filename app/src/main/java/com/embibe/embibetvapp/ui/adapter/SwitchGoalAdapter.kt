package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemGoalsBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.adduser.Goal
import com.embibe.embibetvapp.utils.SegmentUtils


class SwitchGoalAdapter(var context: Context) :
    RecyclerView.Adapter<SwitchGoalAdapter.GoalViewHolder>() {

    private var goalsList: List<Goal> = arrayListOf()
    var onItemClick: ((Goal) -> Unit)? = null
    var onFocusChange: ((Goal, Boolean) -> Unit)? = null
    var selectedItem = -1
    private var currentPosition = -1
    private var currentGoalCode = UserData.getGoalCode()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GoalViewHolder {
        val binding: ItemGoalsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_goals, parent, false
        )
        return GoalViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return goalsList.size
    }

    override fun onBindViewHolder(holder: GoalViewHolder, position: Int) {
        currentPosition = holder.adapterPosition
        holder.bind(goalsList[position])

        when (selectedItem) {
            position -> {
                holder.binding.textGoalExam.background =
                    context.getDrawable(R.drawable.shape_rec_white_border_with_radius)
                holder.binding.textGoalExam.setTextColor(context.getColor(R.color.email_font_color))
            }
        }

    }

    fun setData(data: List<Goal>) {
        selectedItem = -1
        goalsList = data
        //for default selection
        goalsList.forEachIndexed { index, goal ->
            when (currentGoalCode) {
                goal.code -> selectedItem = index
            }
        }
        notifyDataSetChanged()
    }


    inner class GoalViewHolder(var binding: ItemGoalsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Goal) {
            binding.goal = item
            binding.executePendingBindings()

            binding.textGoalExam.background = context.getDrawable(R.drawable.bg_button_goals)
            binding.textGoalExam.setTextColor(context.getColor(R.color.white))


            itemView.setOnFocusChangeListener { v, hasFocus ->
                onFocusChange?.invoke(goalsList[adapterPosition], hasFocus)
                SegmentUtils.trackProfilePrimaryGoalValueFocus(
                    goalsList[adapterPosition].name,
                    adapterPosition
                )
            }
            itemView.setOnClickListener {
                onItemClick?.invoke(goalsList[adapterPosition])
                SegmentUtils.trackProfilePrimaryGoalValueClick(
                    goalsList[adapterPosition].name,
                    adapterPosition
                )

                when {
                    selectedItem != adapterPosition -> {
                        val previousItem = selectedItem
                        selectedItem = adapterPosition
                        notifyItemChanged(previousItem)
                    }
                    else -> selectedItem = -1
                }
                notifyItemChanged(adapterPosition)
            }

        }

    }
}
