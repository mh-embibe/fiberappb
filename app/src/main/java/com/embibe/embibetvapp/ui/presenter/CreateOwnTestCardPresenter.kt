package com.embibe.embibetvapp.ui.presenter

import android.content.Context
import android.view.LayoutInflater
import androidx.leanback.widget.BaseCardView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.newmodel.Content

class CreateOwnTestCardPresenter(context: Context, var type: String) :
    AbstractCardPresenter<BaseCardView>(context) {
    override fun onCreateView(): BaseCardView {
        val cardView = BaseCardView(context, null, R.style.SideInfoCardStyle)
        cardView.isFocusable = true
        cardView.addView(
            LayoutInflater.from(context).inflate(
                R.layout.card_create_own_test,
                null
            )
        )
        return cardView
    }

    override fun onBindViewHolder(item: Any, cardView: BaseCardView) {
        val card = item as Content

    }

}