package com.embibe.embibetvapp.ui.activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.ui.fragment.addUser.AddChildFragment

class AddChildActivity : BaseFragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_child)
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_main_layout, AddChildFragment()).commit()
    }

    private fun setFragment(fragment: Fragment, name: String) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_main_layout, fragment)
            .addToBackStack(name)
            .commit()
    }
}


