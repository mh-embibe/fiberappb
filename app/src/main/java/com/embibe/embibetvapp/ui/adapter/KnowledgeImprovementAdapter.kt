package com.embibe.embibetvapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemKnowledgeImprovementBinding
import com.embibe.embibetvapp.model.completionSummary.Topic

class KnowledgeImprovementAdapter :
    RecyclerView.Adapter<KnowledgeImprovementAdapter.TestsViewHolder>() {

    var list = ArrayList<Topic>()
    var onItemClick: ((Topic) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestsViewHolder {
        val binding: ItemKnowledgeImprovementBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_knowledge_improvement, parent, false
        )
        return TestsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (list.size > 0) list.size else 0
    }

    override fun onBindViewHolder(holder: TestsViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun setData(list: ArrayList<Topic>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class TestsViewHolder(var binding: ItemKnowledgeImprovementBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Topic) {
//            binding.title.text = item.name
//            binding.textDescription.text = item.coverage
//            binding.previousPercentage.text = item.previous
//            binding.currentPercentage.text = item.current
            binding.executePendingBindings()

            if (adapterPosition == list.size - 1) {
                binding.verticalViewBottom.visibility = INVISIBLE
            }

            setImage(binding, item, itemView)
        }
    }

//    private fun setDescription(description: String): SpannableString {
//        val spannableString = SpannableString(description)
//
//        val indexOfFrom = description.lastIndexOf("from")
//        val indexOfTo = description.indexOf("to")
//        val boldStyle = StyleSpan(Typeface.BOLD)
//        spannableString.setSpan(
//            boldStyle, indexOfFrom , indexOfTo, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
//        )
//        spannableString.setSpan(
//            boldStyle, indexOfTo + 2, description.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
//        )
//        return spannableString
//    }

    private fun setImage(
        binding: ItemKnowledgeImprovementBinding,
        item: Topic,
        itemView: View
    ) {
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        if (item.thumb == null || item.thumb == "thumb" || item.thumb == "") {
            Glide.with(itemView.context).setDefaultRequestOptions(requestOptions)
                .load(R.drawable.practice_placeholder)
                .transition(DrawableTransitionOptions.withCrossFade())
                .transform(RoundedCorners(16))
                .into(binding.ivBg)
        } else {
            Glide.with(itemView.context).setDefaultRequestOptions(requestOptions)
                .load(item.thumb)
                .placeholder(R.drawable.practice_placeholder)
                .transition(DrawableTransitionOptions.withCrossFade())
                .transform(RoundedCorners(16))
                .into(binding.ivBg)
        }
    }
}