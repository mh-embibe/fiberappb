package com.embibe.embibetvapp.ui.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.databinding.LearnFromSyallbusBinding

class SyllabusSelectionActivity : BaseFragmentActivity() {

    private lateinit var binding: LearnFromSyallbusBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.learn_from_syallbus)
    }
}
