package com.embibe.embibetvapp.ui.adapter

import android.content.Intent
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ItemSubTestDetailMenuBinding
import com.embibe.embibetvapp.model.test.SubFeedbackDetailMenuModel
import com.embibe.embibetvapp.ui.activity.TopicSummaryActivity
import com.embibe.embibetvapp.utils.data.DataManager

class TestFeedbackDetailSubMenuAdapter :
    RecyclerView.Adapter<TestFeedbackDetailSubMenuAdapter.TestsViewHolder>() {

    var list = ArrayList<SubFeedbackDetailMenuModel>()
    var onItemClick: ((SubFeedbackDetailMenuModel) -> Unit)? = null
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestsViewHolder {
        val binding: ItemSubTestDetailMenuBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_sub_test_detail_menu, parent, false
        )
        return TestsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (list.size > 0) list.size else 0
    }

    override fun onBindViewHolder(holder: TestsViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun setData(list: ArrayList<SubFeedbackDetailMenuModel>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class TestsViewHolder(var binding: ItemSubTestDetailMenuBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SubFeedbackDetailMenuModel) {
            binding.title.text = item.title.capitalize()
            binding.textAccuracy.text = "${item.accuracy} % "
            binding.textTopicMastery.text = "${item.topic_mastery} % "

            if (adapterPosition == list.size - 1) {
                binding.verticalViewBottom.visibility = View.INVISIBLE
            } else {
                binding.verticalViewBottom.visibility = View.VISIBLE
            }

            setLearnPracticeButtons(binding, item)
            setFocusListeners(binding, itemView)
        }

        init {
            binding.subImgLayout.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                            val content = DataManager.instance.content
                            val intent = Intent(
                                binding.subImgLayout.context,
                                TopicSummaryActivity::class.java
                            )
                            com.embibe.embibetvapp.utils.Utils.insertBundle(content, intent)
                            binding.subImgLayout.context.startActivity(intent)
                        }
                    }
                }

                false
            }
        }

        private fun setLearnPracticeButtons(
            binding: ItemSubTestDetailMenuBinding,
            item: SubFeedbackDetailMenuModel
        ) {
            when (item.type) {
                AppConstants.PRACTICE -> {
                    binding.btnPractice.visibility = View.VISIBLE
                    binding.btnLearn.visibility = View.GONE
                }

                AppConstants.LEARN -> {
                    binding.btnLearn.visibility = View.VISIBLE
                    binding.btnPractice.visibility = View.GONE
                }
            }
        }

        private fun setFocusListeners(
            binding: ItemSubTestDetailMenuBinding,
            itemView: View
        ) {
            binding.subImgLayout.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    if (binding.btnPractice.visibility == View.VISIBLE) {
                        binding.btnPractice.background =
                            itemView.context.getDrawable(R.drawable.bg_card_selected_yellow)
                        binding.btnPractice.setTextColor(itemView.resources.getColor(R.color.black))
                    } else if (binding.btnLearn.visibility == View.VISIBLE) {

                        binding.btnLearn.background =
                            itemView.context.getDrawable(R.drawable.bg_card_selected_yellow)
                        binding.btnLearn.setTextColor(itemView.resources.getColor(R.color.black))
                        binding.btnLearn.setCompoundDrawablesWithIntrinsicBounds(
                            R.drawable.ic_play_circle_filled_black_24dp, 0, 0, 0
                        )
                    }
                } else {
                    if (binding.btnPractice.visibility == View.VISIBLE) {
                        binding.btnPractice.background =
                            itemView.context.getDrawable(R.drawable.bg_card_unselected_grey)
                        binding.btnPractice.setTextColor(itemView.resources.getColor(R.color.white))
                    } else if (binding.btnLearn.visibility == View.VISIBLE) {

                        binding.btnLearn.background =
                            itemView.context.getDrawable(R.drawable.bg_card_unselected_grey)
                        binding.btnLearn.setTextColor(itemView.resources.getColor(R.color.white))
                        binding.btnLearn.setCompoundDrawablesWithIntrinsicBounds(
                            R.drawable.ic_play_circle_filled_white_24dp, 0, 0, 0
                        )
                    }

                }
            }
        }
    }
}