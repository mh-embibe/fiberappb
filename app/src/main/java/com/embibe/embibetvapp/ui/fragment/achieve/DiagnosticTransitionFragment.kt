package com.embibe.embibetvapp.ui.fragment.achieve

import android.media.MediaPlayer
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.BuildConfig
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.databinding.FragmentDiagnosticTransitionBinding
import com.embibe.embibetvapp.model.achieve.Crunching.CrunchingCount
import com.embibe.embibetvapp.ui.activity.DiagnosticTransitionActivity
import com.embibe.embibetvapp.ui.viewmodel.AchieveViewModel
import com.embibe.embibetvapp.utils.Utils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Suppress("DEPRECATION")
class DiagnosticTransitionFragment(var context: DiagnosticTransitionActivity) : BaseAppFragment() {
    private lateinit var binding: FragmentDiagnosticTransitionBinding
    private lateinit var achieveViewModel: AchieveViewModel
    private var variables = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_diagnostic_transition,
                container,
                false
            )
        achieveViewModel = ViewModelProviders.of(this).get(AchieveViewModel::class.java)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureVideoView()
        getCrunchingCount()
    }

    private fun configureVideoView() {
        val metrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(metrics)
        val uriPath =
            "android.resource://" + BuildConfig.APPLICATION_ID + "/" + R.raw.transition_screen_video
        binding.transitionVideo.setVideoPath(uriPath)
        binding.transitionVideo.setOnInfoListener(object : MediaPlayer.OnInfoListener {
            override fun onInfo(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    loadNextScreen()
                    binding.tvGen.visibility = View.VISIBLE
                    binding.placeholder.visibility = View.GONE
                    return true
                }
                return false
            }
        })
        binding.transitionVideo.start()
    }


    /*load CrunchingCount from api*/
    fun getCrunchingCount() {
        showProgress()
        achieveViewModel.getCrunchingCount(object :
            BaseViewModel.APICallBacks<CrunchingCount> {

            override fun onSuccess(model: CrunchingCount?) {
                hideProgress()
                if (model != null) {
                    variables = model.count ?: ""
                    configureVideoView()
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getCrunchingCount()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    Utils.showToast(context, error)
                }
            }
        })
    }

    private fun loadNextScreen() {
        CoroutineScope(Dispatchers.Main).launch {
            delay(7000)
            binding.tvGen.visibility = View.GONE
            val transaction: FragmentTransaction = context.supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(R.anim.anim_fade_in, R.anim.anim_fade_out)
            transaction.replace(
                R.id.diagnostic_TL,
                DiagnosticCrunchingVariableFragment(context, variables)
            ).commit()
        }
    }

}