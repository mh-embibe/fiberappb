package com.embibe.embibetvapp.ui.custom

import android.graphics.PointF
import android.graphics.Rect
import android.util.Log
import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.OrientationHelper
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.ui.custom.TvRecyclerView.Companion.DEBUG
import java.util.*

abstract class ModuleLayoutManager @JvmOverloads constructor(
    private val mNumRowOrColumn: Int, private var mOrientation: Int,
    private var mOriItemWidth: Int = BASE_ITEM_DEFAULT_SIZE,
    private var mOriItemHeight: Int = BASE_ITEM_DEFAULT_SIZE
) : RecyclerView.LayoutManager(),
    RecyclerView.SmoothScroller.ScrollVectorProvider {

    private val mItemsRect: SparseArray<Rect> = SparseArray()
    private var mHorizontalOffset = 0
    private var mVerticalOffset = 0
    private var mTotalSize = 0

    // re-used variable to acquire decor insets from RecyclerView
    private val mDecorInsets = Rect()

    /**
     * reset default item row or column.
     * Avoid the width and height of all items in each row or column
     * more than the width and height of the parent view.
     */
    private fun resetItemRowColumnSize() {
        if (mOrientation == HORIZONTAL) {
            mOriItemHeight = ((height - (mNumRowOrColumn - 1) * rowSpacing)
                    / mNumRowOrColumn)
        } else {
            mOriItemWidth = ((width - (mNumRowOrColumn - 1) * columnSpacing)
                    / mNumRowOrColumn)
        }
        if (DEBUG) {
            Log.d(
                TAG,
                "resetItemRowColumnSize: OriItemHeight=" + mOriItemHeight
                        + "=OriItemWidth=" + mOriItemWidth
            )
        }
    }

    override fun generateDefaultLayoutParams(): RecyclerView.LayoutParams {
        return RecyclerView.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onLayoutChildren(recycler: RecyclerView.Recycler, state: RecyclerView.State) {
        layoutChildren(recycler, state)
    }

    private fun layoutChildren(recycler: RecyclerView.Recycler, state: RecyclerView.State) {
        if (itemCount === 0) {
            detachAndScrapAttachedViews(recycler)
            return
        }
        if (childCount === 0 && state.isPreLayout) {
            return
        }
        detachAndScrapAttachedViews(recycler)
        mHorizontalOffset = 0
        mVerticalOffset = 0
        mItemsRect.clear()
        resetItemRowColumnSize()
        fill(recycler, state)
    }

    private fun fill(recycler: RecyclerView.Recycler, state: RecyclerView.State) {
        val itemsCount: Int = state.itemCount
        val displayRect = displayRect
        for (i in 0 until itemsCount) {
            val child: View = recycler.getViewForPosition(i)
            val itemRect = calculateViewSizeByPosition(child, i)
            if (!Rect.intersects(displayRect, itemRect)) {
                recycler.recycleView(child)
                break
            }
            addView(child)
            //calculate width includes margin
            layoutDecoratedWithMargins(
                child,
                itemRect.left,
                itemRect.top,
                itemRect.right,
                itemRect.bottom
            )
            mTotalSize = if (mOrientation == HORIZONTAL) {
                itemRect.right
            } else {
                itemRect.bottom
            }
            // Save the current Bound field data for the item view
            var frame = mItemsRect[i]
            if (frame == null) {
                frame = Rect()
            }
            frame.set(itemRect)
            mItemsRect.put(i, frame)
            if (DEBUG) {
                Log.d(
                    TAG,
                    "fill: pos=$i=frame=$frame"
                )
            }
        }
    }

    private fun calculateViewSizeByPosition(
        child: View,
        position: Int
    ): Rect {
        require(position < itemCount) {
            ("position outside of itemCount position is "
                    + position + " itemCount is " + itemCount)
        }
        val leftOffset: Int
        val topOffset: Int
        val childFrame = Rect()
        calculateItemDecorationsForChild(child, mDecorInsets)
        measureChild(child, getItemWidth(position), getItemHeight(position))
        val itemStartPos = getItemStartIndex(position)
        val childHorizontalSpace = getDecoratedMeasurementHorizontal(child)
        val childVerticalSpace = getDecoratedMeasurementVertical(child)
        val lastPos: Int
        val topPos: Int
        if (mOrientation == HORIZONTAL) {
            lastPos = itemStartPos / mNumRowOrColumn
            topPos = itemStartPos % mNumRowOrColumn
        } else {
            lastPos = itemStartPos % mNumRowOrColumn
            topPos = itemStartPos / mNumRowOrColumn
        }
        leftOffset = if (lastPos == 0) {
            -mDecorInsets.left
        } else {
            ((mOriItemWidth + getChildHorizontalPadding(child)) * lastPos
                    - mDecorInsets.left)
        }
        topOffset = if (topPos == 0) {
            -mDecorInsets.top
        } else {
            ((mOriItemHeight + getChildVerticalPadding(child)) * topPos
                    - mDecorInsets.top)
        }
        childFrame.left = leftOffset
        childFrame.top = topOffset
        childFrame.right = leftOffset + childHorizontalSpace
        childFrame.bottom = topOffset + childVerticalSpace
        return childFrame
    }

    private val displayRect: Rect
        get() {
            return if (mOrientation == HORIZONTAL) {
                Rect(
                    mHorizontalOffset - paddingLeft, 0,
                    mHorizontalOffset + horizontalSpace + paddingRight, verticalSpace
                )
            } else {
                Rect(
                    0, mVerticalOffset - paddingTop,
                    horizontalSpace, mVerticalOffset + verticalSpace + paddingBottom
                )
            }
        }

    private fun findLastViewLayoutPosition(): Int {
        var lastPos: Int = childCount
        if (lastPos > 0) {
            lastPos = getChildAt(lastPos - 1)?.let { getPosition(it) }!!
        }
        return lastPos
    }

    private fun recycleAndFillItems(
        recycler: RecyclerView.Recycler,
        state: RecyclerView.State,
        dt: Int
    ) {
        if (state.isPreLayout) {
            return
        }
        recycleByScrollState(recycler, dt)
        if (dt >= 0) {
            val beginPos = findLastViewLayoutPosition() + 1
            fillRequireItems(recycler, beginPos)
        } else {
            val endPos = findFirstVisibleItemPosition() + mNumRowOrColumn
            for (i in endPos downTo 0) {
                val frame = mItemsRect[i]
                if (Rect.intersects(displayRect, frame)) {
                    if (findViewByPosition(i) != null) {
                        continue
                    }
                    val scrap: View = recycler.getViewForPosition(i)
                    addView(scrap, 0)
                    measureChild(scrap, getItemWidth(i), getItemHeight(i))
                    if (mOrientation == HORIZONTAL) {
                        layoutDecoratedWithMargins(
                            scrap,
                            frame.left - mHorizontalOffset,
                            frame.top,
                            frame.right - mHorizontalOffset,
                            frame.bottom
                        )
                    } else {
                        layoutDecoratedWithMargins(
                            scrap,
                            frame.left,
                            frame.top - mVerticalOffset,
                            frame.right,
                            frame.bottom - mVerticalOffset
                        )
                    }
                }
            }
        }
    }

    private fun fillRequireItems(recycler: RecyclerView.Recycler, beginPos: Int) {
        val itemCount: Int = itemCount
        val displayRect = displayRect
        var rectCount: Int
        // Re-display the subview that needs to appear on the screen
        for (i in beginPos until itemCount) {
            rectCount = mItemsRect.size()
            var frame = mItemsRect[i]
            if (i < rectCount && frame != null) {
                if (Rect.intersects(displayRect, frame)) {
                    val scrap: View = recycler.getViewForPosition(i)
                    addView(scrap)
                    measureChild(scrap, getItemWidth(i), getItemHeight(i))
                    if (mOrientation == HORIZONTAL) {
                        layoutDecoratedWithMargins(
                            scrap,
                            frame.left - mHorizontalOffset,
                            frame.top,
                            frame.right - mHorizontalOffset,
                            frame.bottom
                        )
                    } else {
                        layoutDecoratedWithMargins(
                            scrap,
                            frame.left,
                            frame.top - mVerticalOffset,
                            frame.right,
                            frame.bottom - mVerticalOffset
                        )
                    }
                }
            } else if (rectCount < itemCount) {
                val child: View = recycler.getViewForPosition(i)
                val itemRect = calculateViewSizeByPosition(child, i)
                if (!Rect.intersects(displayRect, itemRect)) {
                    recycler.recycleView(child)
                    return
                }
                addView(child)
                mTotalSize = if (mOrientation == HORIZONTAL) {
                    layoutDecoratedWithMargins(
                        child,
                        itemRect.left - mHorizontalOffset,
                        itemRect.top,
                        itemRect.right - mHorizontalOffset,
                        itemRect.bottom
                    )
                    itemRect.right
                } else {
                    layoutDecoratedWithMargins(
                        child,
                        itemRect.left,
                        itemRect.top - mVerticalOffset,
                        itemRect.right,
                        itemRect.bottom - mVerticalOffset
                    )
                    itemRect.bottom
                }
                if (frame == null) {
                    frame = Rect()
                }
                frame.set(itemRect)
                mItemsRect.put(i, frame)
                if (DEBUG) {
                    Log.d(
                        TAG,
                        "fillRequireItems: new pos=$i=frame=$frame"
                    )
                }
            }
        }
    }

    private fun recycleByScrollState(recycler: RecyclerView.Recycler, dt: Int): Boolean {
        val childCount: Int = childCount
        val recycleIndexList = ArrayList<Int>()
        val displayRect = displayRect
        if (dt >= 0) {
            for (i in 0 until childCount) {
                val child: View? = getChildAt(i)
                val pos: Int? = child?.let { getPosition(it) }
                val frame = mItemsRect[pos!!]
                if (!Rect.intersects(displayRect, frame)) {
                    recycleIndexList.add(i)
                }
            }
        } else {
            for (i in childCount - 1 downTo 0) {
                val child: View? = getChildAt(i)
                val pos: Int? = child?.let { getPosition(it) }
                val frame = mItemsRect[pos!!]
                if (!Rect.intersects(displayRect, frame)) {
                    recycleIndexList.add(i)
                }
            }
        }
        if (recycleIndexList.size > 0) {
            recycleChildren(recycler, dt, recycleIndexList)
            return true
        }
        return false
    }

    /**
     * Recycles children between given index.
     *
     * @param dt direction
     * @param recycleIndexList   save need recycle index
     */
    private fun recycleChildren(
        recycler: RecyclerView.Recycler, dt: Int,
        recycleIndexList: ArrayList<Int>
    ) {
        val size = recycleIndexList.size
        if (DEBUG) {
            Log.d(
                TAG,
                "recycleChildren: recycler item size=$size"
            )
        }
        if (dt < 0) {
            for (i in 0 until size) {
                val pos = recycleIndexList[i]
                removeAndRecycleViewAt(pos, recycler)
            }
        } else {
            for (i in size - 1 downTo 0) {
                val pos = recycleIndexList[i]
                removeAndRecycleViewAt(pos, recycler)
            }
        }
    }

    override fun measureChild(child: View, childWidth: Int, childHeight: Int) {
        val lp: RecyclerView.LayoutParams = child.layoutParams as RecyclerView.LayoutParams
        calculateItemDecorationsForChild(child, mDecorInsets)
        val widthSpec: Int = getChildMeasureSpec(
            width, widthMode,
            paddingLeft + paddingRight +
                    lp.leftMargin + lp.rightMargin, childWidth,
            canScrollHorizontally()
        )
        val heightSpec: Int = getChildMeasureSpec(
            height, heightMode,
            paddingTop + paddingBottom +
                    lp.topMargin + lp.bottomMargin, childHeight,
            canScrollVertically()
        )
        child.measure(widthSpec, heightSpec)
    }

    override fun canScrollHorizontally(): Boolean {
        return mOrientation == HORIZONTAL
    }

    override fun scrollHorizontallyBy(
        dx: Int,
        recycler: RecyclerView.Recycler,
        state: RecyclerView.State
    ): Int {
        if (dx == 0 || childCount === 0) {
            return 0
        }
        var realOffset = dx
        val maxScrollSpace = mTotalSize - horizontalSpace
        if (mHorizontalOffset + dx < 0) {
            if (Math.abs(dx) > mHorizontalOffset) {
                realOffset = -mHorizontalOffset
            } else {
                realOffset -= mHorizontalOffset
            }
        } else if (mItemsRect.size() >= itemCount &&
            mHorizontalOffset + dx > maxScrollSpace
        ) {
            realOffset = maxScrollSpace - mHorizontalOffset
        }
        mHorizontalOffset += realOffset
        offsetChildrenHorizontal(-realOffset)
        if (mHorizontalOffset != 0) {
            recycleAndFillItems(recycler, state, dx)
        }
        if (DEBUG) {
            Log.d(
                TAG,
                "scrollHorizontallyBy: HorizontalOffset=" + mHorizontalOffset
                        + "=maxScrollSpace=" + maxScrollSpace
            )
        }
        return realOffset
    }

    override fun canScrollVertically(): Boolean {
        return mOrientation == VERTICAL
    }

    override fun scrollVerticallyBy(
        dy: Int,
        recycler: RecyclerView.Recycler,
        state: RecyclerView.State
    ): Int {
        if (dy == 0 || childCount === 0) {
            return 0
        }
        var realOffset = dy
        val maxScrollSpace = mTotalSize - verticalSpace
        if (mVerticalOffset + dy < 0) {
            if (Math.abs(dy) > mVerticalOffset) {
                realOffset = -mVerticalOffset
            } else {
                realOffset -= mVerticalOffset
            }
        } else if (mItemsRect.size() >= itemCount &&
            mVerticalOffset + dy > maxScrollSpace
        ) {
            realOffset = maxScrollSpace - mVerticalOffset
        }
        mVerticalOffset += realOffset
        offsetChildrenVertical(-realOffset)
        if (mVerticalOffset != 0) {
            recycleAndFillItems(recycler, state, dy)
        }
        if (DEBUG) {
            Log.d(
                TAG,
                "scrollVerticallyBy: VerticalOffset=" + mVerticalOffset
                        + "=maxScrollSpace=" + maxScrollSpace
            )
        }
        return realOffset
    }

    var orientation: Int
        get() = mOrientation
        set(orientation) {
            require(!(orientation != HORIZONTAL && orientation != VERTICAL)) { "invalid orientation." }
            assertNotInLayoutOrScroll(null)
            if (orientation == mOrientation) {
                return
            }
            mOrientation = orientation
            requestLayout()
        }

    private fun getItemWidth(position: Int): Int {
        val itemColumnSize = getItemColumnSize(position)
        return (itemColumnSize * mOriItemWidth
                + (itemColumnSize - 1) * (mDecorInsets.left + mDecorInsets.right))
    }

    private fun getItemHeight(position: Int): Int {
        val itemRowSize = getItemRowSize(position)
        return (itemRowSize * mOriItemHeight
                + (itemRowSize - 1) * (mDecorInsets.bottom + mDecorInsets.top))
    }

    private fun getDecoratedMeasurementHorizontal(child: View): Int {
        val params: ViewGroup.MarginLayoutParams = child.layoutParams as RecyclerView.LayoutParams
        return (getDecoratedMeasuredWidth(child) + params.leftMargin
                + params.rightMargin)
    }

    private fun getChildHorizontalPadding(child: View): Int {
        val params: ViewGroup.MarginLayoutParams = child.layoutParams as RecyclerView.LayoutParams
        return (getDecoratedMeasuredWidth(child) + params.leftMargin
                + params.rightMargin) - child.measuredWidth
    }

    private fun getChildVerticalPadding(child: View): Int {
        val params: ViewGroup.MarginLayoutParams = child.layoutParams as RecyclerView.LayoutParams
        return (getDecoratedMeasuredHeight(child) + params.topMargin
                + params.bottomMargin) - child.measuredHeight
    }

    private fun getDecoratedMeasurementVertical(view: View): Int {
        val params: ViewGroup.MarginLayoutParams = view.layoutParams as RecyclerView.LayoutParams
        return (getDecoratedMeasuredHeight(view) + params.topMargin
                + params.bottomMargin)
    }

    private val verticalSpace: Int
        get() {
            val space: Int = height - paddingTop - paddingBottom
            return if (space <= 0) minimumHeight else space
        }

    private val horizontalSpace: Int
        get() {
            val space: Int = width - paddingLeft - paddingRight
            return if (space <= 0) minimumWidth else space
        }

    /**
     * Returns the adapter position of the first visible view. This position does not include
     * adapter changes that were dispatched after the last layout pass.
     * @return the first visible item position or -1
     */
    fun findFirstVisibleItemPosition(): Int {
        val child = findOneVisibleChild(0, childCount, false, true)
        return child?.let { getPosition(it) } ?: -1
    }

    /**
     * Returns the adapter position of the last visible view. This position does not include
     * adapter changes that were dispatched after the last layout pass.
     * @return the last visible item position or -1
     */
    fun findLastVisibleItemPosition(): Int {
        val child = findOneVisibleChild(childCount - 1, -1, false, true)
        return child?.let { getPosition(it) } ?: -1
    }

    override fun computeScrollVectorForPosition(targetPosition: Int): PointF? {
        val direction = calculateScrollDirectionForPosition(targetPosition)
        val outVector = PointF()
        if (direction == 0) {
            return null
        }
        if (mOrientation == HORIZONTAL) {
            outVector.x = direction.toFloat()
            outVector.y = 0f
        } else {
            outVector.x = 0f
            outVector.y = direction.toFloat()
        }
        return outVector
    }

    private fun findOneVisibleChild(
        fromIndex: Int, toIndex: Int, completelyVisible: Boolean,
        acceptPartiallyVisible: Boolean
    ): View? {
        val start: Int = paddingLeft
        val end: Int = height - paddingBottom
        val next = if (toIndex > fromIndex) 1 else -1
        var partiallyVisible: View? = null
        var i = fromIndex
        while (i != toIndex) {
            val child: View? = getChildAt(i)
            val childStart = child?.let { getDecoratedStart(it) }
            val childEnd = child?.let { getDecoratedEnd(it) }
            if (childEnd != null) {
                if (childStart != null) {
                    if (childStart < end && childEnd > start) {
                        if (completelyVisible) {
                            if (childStart >= start && childEnd <= end) {
                                return child
                            } else if (acceptPartiallyVisible && partiallyVisible == null) {
                                partiallyVisible = child
                            }
                        } else {
                            return child
                        }
                    }
                }
            }
            i += next
        }
        return partiallyVisible
    }

    private fun getDecoratedStart(view: View): Int {
        val params: RecyclerView.LayoutParams = view.layoutParams as RecyclerView.LayoutParams
        return getDecoratedTop(view) - params.topMargin
    }

    private fun getDecoratedEnd(view: View): Int {
        val params: RecyclerView.LayoutParams = view.layoutParams as RecyclerView.LayoutParams
        return getDecoratedBottom(view) + params.bottomMargin
    }

    private val firstChildPosition: Int?
        get() {
            val childCount: Int = childCount
            return if (childCount == 0) 0 else getChildAt(0)?.let { getPosition(it) }
        }

    private fun calculateScrollDirectionForPosition(position: Int): Int {
        if (childCount === 0) {
            return LAYOUT_START
        }
        val firstChildPos = firstChildPosition
        return if (position < firstChildPos!!) LAYOUT_START else LAYOUT_END
    }

    protected abstract fun getItemStartIndex(position: Int): Int
    protected abstract fun getItemRowSize(position: Int): Int
    protected abstract fun getItemColumnSize(position: Int): Int
    protected abstract val columnSpacing: Int
    protected abstract val rowSpacing: Int

    companion object {
        private const val TAG = "TvRecyclerView_ML"
        private const val BASE_ITEM_DEFAULT_SIZE = 380
        private const val HORIZONTAL: Int = OrientationHelper.HORIZONTAL
        private const val VERTICAL: Int = OrientationHelper.VERTICAL
        private const val LAYOUT_START = -1
        private const val LAYOUT_END = 1
    }

}