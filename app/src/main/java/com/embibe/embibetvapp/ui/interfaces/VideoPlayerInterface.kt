package com.embibe.embibetvapp.ui.interfaces

interface VideoPlayerInterface {
    fun backPressed()
    val androidAPIOnCallListener: CommonAndroidAPI.AndroidAPIOnCallListener
}