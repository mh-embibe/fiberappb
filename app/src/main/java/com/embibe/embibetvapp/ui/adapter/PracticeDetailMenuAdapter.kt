package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ItemPracticeDetailMenuBinding

class PracticeDetailMenuAdapter(private val mContext: Context) :
    RecyclerView.Adapter<PracticeDetailMenuAdapter.ItemViewHolder<*>>() {
    var list: ArrayList<String> = ArrayList()
    val TAG = this.javaClass.name
    var onItemClick: ((String) -> Unit)? = null
    var onItemFocused: ((String) -> Unit?)? = null
    var onItemDpadHit: ((Int) -> Unit)? = null
    var selectedItem: Int = -1
    var previousItem: Int = -1

    companion object {
        private const val TYPE_PRACTICE = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder<String> {
        return when (viewType) {
            TYPE_PRACTICE -> {
                val binding: ItemPracticeDetailMenuBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_practice_detail_menu,
                    parent,
                    false
                )
                PracticeViewHolder(binding)
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder<*>, position: Int) {
        val item = list[position]
        when (holder) {
            is PracticeViewHolder -> holder.bind(item)
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return TYPE_PRACTICE
    }

    fun setData(data: ArrayList<String>) {
        list = getOrderedPracticeSummaryList(data) as ArrayList<String>
        Log.e(TAG, "${list.size}")
        notifyDataSetChanged()
    }


    abstract class ItemViewHolder<String>(binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        abstract fun bind(item: String)
    }

    inner class PracticeViewHolder(var binding: ItemPracticeDetailMenuBinding) :
        ItemViewHolder<String>(binding) {
        override fun bind(item: String) {

            binding.detailItem = item
            when (item) {
                AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE!! -> {
                    binding.itemPracticeTopic.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_practice_topic_for_practice,
                        0,
                        0,
                        0
                    );

                }
                AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE -> {
                    binding.itemPracticeTopic.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_practice_books_available,
                        0,
                        0,
                        0
                    );

                }
                AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING!! -> {
                    binding.itemPracticeTopic.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_practice_recommended_learning,
                        0,
                        0,
                        0
                    );


                }
                AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER!! -> {
                    binding.itemPracticeTopic.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_practice_tests_on_this_chapter,
                        0,
                        0,
                        0
                    );

                }
            }

            if (selectedItem == adapterPosition) {
                itemView.requestFocus()
                itemView.setBackgroundResource(R.drawable.summary_screen_menu_selected)
            }
            if (previousItem == adapterPosition) itemView.setBackgroundResource(R.drawable.summary_menu_selector)

        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
                if (selectedItem != adapterPosition) {
                    previousItem = selectedItem
                    selectedItem = adapterPosition
                    notifyItemChanged(previousItem)
                    notifyItemChanged(selectedItem)
                } else {
                    selectedItem = -1
                    notifyItemChanged(adapterPosition)
                }
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(list[adapterPosition])
            }

        }
    }

    private fun getOrderedPracticeSummaryList(practiceDetails: java.util.ArrayList<String>): MutableList<String> {
        val updatedOrderedList: MutableList<String> = getExpectedOrderPracticeList()
        for (item in getExpectedOrderPracticeList()) {
            if (item !in practiceDetails) {
                updatedOrderedList.remove(item)
            }
        }
        return updatedOrderedList
    }

    private fun getExpectedOrderPracticeList(): MutableList<String> {
        val list: MutableList<String> = ArrayList()
        list.add(AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE)
        list.add(AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING)
        list.add(AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE)
        list.add(AppConstants.PRACTICE_DETAIL_TEST_ON_THIS_CHAPTER)
        return list
    }
}