package com.embibe.embibetvapp.ui.fragment.errorAlertScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants.RESUME
import com.embibe.embibetvapp.constant.AppConstants.START
import com.embibe.embibetvapp.databinding.LayoutContinueVideoBinding
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener

class ContinueVideoDialog : DialogFragment(), View.OnClickListener {

    private var errorScreensBtnClickListener: ErrorScreensBtnClickListener? = null
    private lateinit var binding: LayoutContinueVideoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, android.R.style.ThemeOverlay)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.layout_continue_video, container, false
        )

        binding.btnStart.setOnClickListener(this)
        binding.btnResume.setOnClickListener(this)
        return binding.root
    }

    fun setListener(listener: ErrorScreensBtnClickListener) {
        errorScreensBtnClickListener = listener
    }

    override fun onResume() {
        super.onResume()
        binding.btnResume.requestFocus()
    }


    override fun onClick(view: View?) {
        when (view) {
            binding.btnStart -> {
                errorScreensBtnClickListener?.screenUpdate(START)
                dismiss()
            }
            binding.btnResume -> {
                errorScreensBtnClickListener?.screenUpdate(RESUME)
                dismiss()
            }
        }
    }


}