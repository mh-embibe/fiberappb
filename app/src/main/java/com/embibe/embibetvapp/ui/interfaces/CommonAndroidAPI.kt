package com.embibe.embibetvapp.ui.interfaces

import android.webkit.JavascriptInterface

/**
 * Created by Sriram
 */
class CommonAndroidAPI(private val listener: AndroidAPIOnCallListener?) {
    @get:JavascriptInterface
    val question: String?
        get() = listener?.question

    @JavascriptInterface
    fun getQuestion(questionCode: String?): String? {
        return listener?.getQuestion(questionCode)
    }

    @JavascriptInterface
    fun focusUp() {
        listener?.focusUp()
    }

    @JavascriptInterface
    fun focusDown() {
        listener?.focusDown()
    }

    @JavascriptInterface
    fun getPAJNextActivity(msg: String) {
        listener?.movePAJNextActivity(msg)
    }

    @JavascriptInterface
    fun focusRight() {
        listener?.focusRight()
    }

    @JavascriptInterface
    fun focusLeft() {
        listener?.focusLeft()
    }


    @JavascriptInterface
    fun setFocusUp() {
        //   listener?.setFocusUp()
    }

    @JavascriptInterface
    fun getData(): String {
        return listener?.getData()!!
    }

    @JavascriptInterface
    fun sendToFeedback(testCode: String) {
        return listener?.sendToFeedback(testCode)!!
    }

    @JavascriptInterface
    fun getData(data: String) {
        listener?.getData(data.toString())!!
    }

    @JavascriptInterface
    fun getEvent(data: String) {
        listener?.getEvent(data.toString())!!
    }

    @JavascriptInterface
    fun showLoader() {
        listener?.showLoader()
    }

    @JavascriptInterface
    fun closeLoader() {
        listener?.closeLoader()
    }

    @get:JavascriptInterface
    val concept: String?
        get() = listener?.concept

    @get:JavascriptInterface
    val hint: String?
        get() = listener?.hint

    @JavascriptInterface
    fun getSections(): String? {
        return listener?.getSections()
    }

    @JavascriptInterface
    fun getQuestionSet(sectionId: String?): String? {
        return listener?.getQuestionSet(sectionId)
    }

    @JavascriptInterface
    fun getAnswer(): String? {
        return listener?.getAnswer()
    }

    @JavascriptInterface
    fun setAnswer(): String? {
        return listener?.getAnswer()
    }


    @JavascriptInterface
    fun selectAnswer(value: String?) {
        listener?.selectAnswer(value)
    }

    @JavascriptInterface
    fun deselectAnswer(value: String?) {
        listener?.deselectAnswer(value)
    }

    @JavascriptInterface
    fun inputAnswer(value: String?) {
        listener?.inputAnswer(value)
    }

    @JavascriptInterface
    fun setQuestionList(qListJson: String?) {
        if (qListJson != null) {
            listener?.getSelectedQuestion(qListJson)
        }
    }

    @JavascriptInterface
    fun getSelectedQuestion(qListJson: String?) {
        if (qListJson != null) {
            listener?.getSelectedQuestion(qListJson)
        }
    }

    @JavascriptInterface
    fun setQuestion(singQuestionJson: String?) {
        if (singQuestionJson != null) {
            listener?.setQuestion(singQuestionJson)
        }
    }

    @JavascriptInterface
    fun getSelectedVideo(videoData: String?) {
        if (videoData != null) {
            listener?.getSelectedVideo(videoData)
        }
    }

    @get:JavascriptInterface
    val questionNumber: Int
        get() = listener?.questionNumber ?: -1

    @get:JavascriptInterface
    val sectionName: String?
        get() = listener?.sectionName

    @get:JavascriptInterface
    val chapterJson: String?
        get() = listener?.chapterJson

    @get:JavascriptInterface
    val chapterName: String?
        get() = listener?.chapterName

    @JavascriptInterface
    fun showConcept(): Boolean {
        return true

    }

    @JavascriptInterface
    fun close() {
        listener?.close()

    }

    @JavascriptInterface
    fun hideKeyboard() {
        listener?.hideKeyboard()

    }

    @JavascriptInterface
    fun initVoiceInput(value: String) {
        listener?.initVoiceInput(value)

    }

    @get:JavascriptInterface
    val testFeedbackSingleJson: String?
        get() = listener?.singQuestionJson

    @get:JavascriptInterface
    val testFeedbackMultipleJson: String?
        get() = listener?.questionListJson

    interface AndroidAPIOnCallListener {
        // Getter
        val question: String?
        fun getQuestion(questionCode: String?): String?
        val concept: String?
        val hint: String?
        fun setAnswer(): String?
        fun getSections(): String?
        fun getAnswer(): String?
        fun getQuestionSet(sectionId: String?): String?
        fun getData(): String?
        fun getData(data: String)
        fun getEvent(data: String)
        fun sendToFeedback(testCode: String)
        fun showLoader()
        fun closeLoader()
        fun hideKeyboard()
        fun focusUp()
        fun focusDown()
        fun focusRight()
        fun focusLeft()
        fun movePAJNextActivity(msg: String)
        fun initVoiceInput(value: String)
        fun getSelectedQuestion(qListJson: String)
        fun setQuestion(singQuestionJson: String)
        fun getSelectedVideo(videoData: String)
        // Events
        fun selectAnswer(value: String?)

        fun deselectAnswer(value: String?)
        fun inputAnswer(value: String?)

        // Close Function
        fun close()

        // fun setFocusUp()

        val questionNumber: Int
        val sectionName: String?
        val chapterJson: String?
        val chapterName: String?
        val singQuestionJson: String?
        val questionListJson: String?

        fun showConcept(): Boolean?
    }

    companion object {
        const val NAME = "CommonAndroidAPI"
    }

}