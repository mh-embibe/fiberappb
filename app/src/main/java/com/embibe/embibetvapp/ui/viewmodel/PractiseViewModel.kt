package com.embibe.embibetvapp.ui.viewmodel

import android.annotation.SuppressLint
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.model.DEAnalysis.*
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.home.HomeModel
import com.embibe.embibetvapp.model.home.SearchRequest
import com.embibe.embibetvapp.model.home.TestForChapterRequest
import com.embibe.embibetvapp.model.likeAndBookmark.BookmarkedQuestion
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkResponse
import com.embibe.embibetvapp.model.practise.details.PracticeTopicsRequest
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.exceptions.CustomException
import com.embibejio.coreapp.preference.PreferenceHelper
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function3
import io.reactivex.functions.Function4
import io.reactivex.functions.Function5
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class PractiseViewModel : BaseViewModel() {

    @SuppressLint("CheckResult")
    fun <T> getDEAnalysisAsync(model: DEAnalysisRequest, callback: APIMergeCallBacks<T>) {
        Single.zip(
            apiDetails.getOverallAccuracy(model),
            apiDetails.getConceptsCoverage(model),
            apiDetails.getBehaviourMeter(model),
            Function3<Response<DEOverAllAccuracy>,
                    Response<DEConceptCoverage>,
                    Response<DEBehaviorMeter>,
                    DEAnalysisModel> { mOverallAccuracy, mConceptsCoverage, BehaviourMeter ->
                return@Function3 mergeRes(mOverallAccuracy, mConceptsCoverage, BehaviourMeter)
            })
            .subscribeOn(Schedulers.io())
            .onErrorResumeNext {
                throw CustomException("server Connection failed")
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback.onSuccess(it as T)
            }, {
                callback.onFailed(0, getError(it), "")
            })
    }

    private fun mergeRes(
        mOverallAccuracy: Response<DEOverAllAccuracy>,
        mConceptsCoverage: Response<DEConceptCoverage>,
        behaviourMeter: Response<DEBehaviorMeter>
    ): DEAnalysisModel {
        val model = DEAnalysisModel()
        model.accuracyModel = if (mOverallAccuracy.isSuccessful) {
            (mOverallAccuracy.body() as DEOverAllAccuracy)
        } else null
        model.conceptCoverageModel = if (mConceptsCoverage.isSuccessful) {
            (mConceptsCoverage.body() as DEConceptCoverage)
        } else null
        model.behaviourMerterModel = if (behaviourMeter.isSuccessful) {
            (behaviourMeter.body() as DEBehaviorMeter)
        } else null
        return model
    }


    /*get Practice topics*/
    fun getPracticeTopics(model: PracticeTopicsRequest, callBack: APICallBacks<List<Content>>) {
        safeApi(practiceSummary.getPracticeTopics(model), callBack)
    }


    @SuppressLint("CheckResult")
    fun  getPracticeSummaryAsync(
        content: Content,
        callback: APIMergeCallBacks<HomeModel>
    ) {
        val req = PracticeTopicsRequest()
        req.learnmapId = content.learnmap_id
        req.subject = content.subject


        val testForChapterRequest = TestForChapterRequest()
        testForChapterRequest.board = UserData.getBoard()
        testForChapterRequest.child_id = Utils.getChildId()
        testForChapterRequest.exam = UserData.getEaxmSlugName().replace("exam--", "", true)
        testForChapterRequest.goal = UserData.getGoal()
        testForChapterRequest.grade = UserData.getGrade()
        if (content.learning_map.chapter.isEmpty()) {
            testForChapterRequest.chapter_name = replaceSpaceWithHypen(content.title.toLowerCase())
        } else {
            testForChapterRequest.chapter_name = replaceSpaceWithHypen(content.learning_map.chapter)
        }
        Single.zip(
            apiDetails.getPracticeTopics(req).subscribeOn(Schedulers.newThread()),
            apiDetails.getRecommendationLearningForPractices(
                content.learnmap_id,
                AppConstants.VIDEO_FOR_CHAPTER
            )
                .subscribeOn(Schedulers.newThread()),
            apiDetails.getTestsForChapter(testForChapterRequest)
                .subscribeOn(Schedulers.newThread()),
            apiDetails.getBooksForPractice(
                Utils.getChildId(),
                "Practice Page Summary",
                "books",
                content.learnpath_name
            ).subscribeOn(Schedulers.newThread()),
           /* apiDetails.getBookmarkedContents(
                "Question",
                content.learnpath_name
            ).subscribeOn(Schedulers.newThread()),*/

            Function4<Response<List<Content>>,
                    Response<List<Results>>,
                    Response<List<Content>>, Response<List<Content>>,/* Response<List<BookmarkedQuestion>>,*/
                    HomeModel> { mPracticeTopics, mRecommendedLearning, mTestOnThisChapter, mBooksAvailable/*, mBookmarkedContents */->
                // here we combine the results to one.
                return@Function4 filterAll(
                    mPracticeTopics,
                    mRecommendedLearning,
                    mTestOnThisChapter, mBooksAvailable/*, mBookmarkedContents*/
                )
            })
            .onErrorResumeNext {
                throw CustomException("server Connection failed")
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback.onSuccess(it)
            }, {
                callback.onFailed(errorCode, getError(it), "")
            })

    }

    private fun replaceSpaceWithHypen(chapterName: String): String {
        if (chapterName.contains(" ")) {
            return chapterName.replace(" ", "-")
        } else return chapterName
    }

    //*combine all api results
    private fun filterAll(
        topicsForChapterRes: Response<List<Content>>,
        recommendationLearning: Response<List<Results>>,
        mTestOnThisChapter: Response<List<Content>>,
        mBooksAvailable: Response<List<Content>>/*,
        mBookmarkedContents: Response<List<BookmarkedQuestion>>*/
    ): HomeModel {
        val mHomeModel = HomeModel()
        if (!mBooksAvailable.isSuccessful || mBooksAvailable.code() == NO_CONTENT_CODE) {
            mHomeModel.booksAvailable = arrayListOf()
        } else {
            mHomeModel.booksAvailable =
                (mBooksAvailable.body() as List<Content>) as ArrayList<Content>
        }

        if (!topicsForChapterRes.isSuccessful || topicsForChapterRes.code() == NO_CONTENT_CODE) {
            mHomeModel.practiceTopicsForChapter = arrayListOf()
        } else {
            mHomeModel.practiceTopicsForChapter = (topicsForChapterRes.body() as ArrayList<Content>)
        }
        if (!recommendationLearning.isSuccessful || recommendationLearning.code() == NO_CONTENT_CODE) {
            mHomeModel.recommendationLearning = arrayListOf()
        } else {
            mHomeModel.recommendationLearning =
                (recommendationLearning.body() as ArrayList<Results>)
        }
        if (!mTestOnThisChapter.isSuccessful || mTestOnThisChapter.code() == NO_CONTENT_CODE) {
            mHomeModel.testsForChapter = arrayListOf()
        } else {
            mHomeModel.testsForChapter =
                (mTestOnThisChapter.body() as List<Content>) as ArrayList<Content>
        }

        /*if (!mBookmarkedContents.isSuccessful || mBookmarkedContents.code() == NO_CONTENT_CODE) {
            mHomeModel.bookmarkedQuestions = arrayListOf()
        } else {
            mHomeModel.bookmarkedQuestions = mBookmarkedContents.body() as ArrayList<BookmarkedQuestion>
        }*/
        var pref = PreferenceHelper()
        return mHomeModel
    }

    fun searchResults(
        dataType: String,
        query: String,
        data: Content,
        callBack: APICallBacks<List<Content>>
    ) {
        val searchRequest = SearchRequest()
        searchRequest.user_id = Utils.getChildId()
        searchRequest.learn_path = data.learnpath_name
        searchRequest.source = "Practice Page Summary"
        searchRequest.query = query
        when {
            AppConstants.PRACTICE_DETAIL_RECOMMENDED_LEARNING.equals(dataType) -> {
                searchRequest.section = "recommended learning"
                safeApi(apiDetails.searchForRecommendedLearning(searchRequest), callBack)
            }
            AppConstants.PRACTICE_DETAIL_TOPICS_FOR_PRACTICE.equals(dataType) -> {
                searchRequest.section = "topics"
                safeApi(apiDetails.searchForTopics(searchRequest), callBack)
            }
            AppConstants.PRACTICE_DETAIL_BOOK_AVAILABLE.equals(dataType) -> {
                searchRequest.section = "books"

                safeApi(
                    apiDetails.searchForBooks(
                        query,
                        Utils.getChildId(),
                        "Practice Page Summary",
                        "books",
                        data.learnmap_id.substringAfterLast('/')
                    ), callBack
                )
            }
        }
    }

    fun searchResults(
        query: String,
        data: Content?,
        callBack: APICallBacks<List<BookmarkedQuestion>>
    ) {
        val searchRequest = SearchRequest()
        searchRequest.user_id = Utils.getChildId()
        searchRequest.query = query
        searchRequest.source = "Practice Page Summary"
        searchRequest.section = "bookmarked questions"
        searchRequest.learn_path = data?.learnpath_name

        safeApi(apiDetails.searchBookmarkedQuestions(searchRequest), callBack)
    }


}