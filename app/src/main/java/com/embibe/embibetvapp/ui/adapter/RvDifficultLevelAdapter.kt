package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemSelectTestActivityBinding
import java.util.regex.Matcher
import java.util.regex.Pattern

class RvDifficultLevelAdapter(var context: Context) :
    RecyclerView.Adapter<RvDifficultLevelAdapter.ViewHolder>() {

    private var adapterList: MutableList<String>? = null

    val TAG = this.javaClass.name

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemSelectTestActivityBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.item_select_test_activity,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(
            adapterList!![position.rem(adapterList!!.size)],
            holder,
            position.rem(adapterList!!.size)
        )

    }

    override fun getItemCount(): Int = if (adapterList!!.size === 0) 0 else Int.MAX_VALUE

    fun setData(list: MutableList<String>) {
        adapterList = list
        notifyDataSetChanged()
    }

    fun clearList() {
        adapterList!!.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(var binding: ItemSelectTestActivityBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: String,
            holder: ViewHolder,
            position: Int
        ) {
            binding.title.text = capitalize(item)


        }


    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private fun capitalize(capString: String): String? {
        val capBuffer = StringBuffer()
        val capMatcher: Matcher =
            Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString)
        while (capMatcher.find()) {
            capMatcher.appendReplacement(
                capBuffer,
                capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase()
            )
        }
        return capMatcher.appendTail(capBuffer).toString()
    }


}