package com.embibe.embibetvapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.utils.FontHelper
import com.embibe.embibetvapp.utils.SegmentUtils

class SimpleRVAdapter(private val inputString: Array<String>) :
    RecyclerView.Adapter<SimpleRVAdapter.SimpleViewHolder>() {

    var onItemClick: ((String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_quick_link, parent,
                false
            )
        return SimpleViewHolder(v)
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        holder.txtName?.text = inputString[position]
        /* if (position == 0) {
             holder.itemView.nextFocusUpId = R.id.rv_keylist
         }
         if (position == inputString.size - 1) {
             holder.itemView.nextFocusDownId = R.id.rv_quick_links
         }
         holder.itemView.nextFocusLeftId = R.id.rv_quick_links*/
    }

    override fun getItemCount(): Int {
        return inputString.size
    }

    inner class SimpleViewHolder(itemView: View?) :
        RecyclerView.ViewHolder(itemView!!) {
        val txtName = itemView?.findViewById<TextView>(R.id.text_quick_links)

        init {
            txtName?.setOnClickListener {
                onItemClick?.invoke(inputString[adapterPosition])
                SegmentUtils.trackEventSearchQuickLinkClick(
                    adapterPosition.toString(),
                    inputString[adapterPosition]
                )
            }
            txtName?.setOnFocusChangeListener { v, hasFocus ->
                when (hasFocus) {
                    true -> {
                        FontHelper().setFontFace(FontHelper.FontType.GILROY_BOLD, txtName)
                        SegmentUtils.trackEventSearchQuickLinkFocus(
                            adapterPosition.toString(),
                            inputString[adapterPosition]
                        )
                    }
                    false -> {
                        FontHelper().setFontFace(FontHelper.FontType.GILROY_REGULAR, txtName)
                    }

                }
            }
        }
    }
}
