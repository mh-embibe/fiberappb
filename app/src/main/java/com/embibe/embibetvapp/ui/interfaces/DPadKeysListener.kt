package com.embibe.embibetvapp.ui.interfaces

interface DPadKeysListener {

    fun isKeyPadDown(isDown: Boolean, from: String)
    fun isKeyPadUp(isUp: Boolean, from: String)
    fun isKeyPadLeft(isLeft: Boolean, from: String)
    fun isKeyPadRight(isRight: Boolean, from: String)
    fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String)
}