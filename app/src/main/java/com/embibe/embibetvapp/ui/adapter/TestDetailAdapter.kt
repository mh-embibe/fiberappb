package com.embibe.embibetvapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemTestDetailBinding
import com.embibe.embibetvapp.model.test.TestDetailModel

class TestDetailAdapter : RecyclerView.Adapter<TestDetailAdapter.TestsViewHolder>() {

    var list = ArrayList<TestDetailModel>()
    var onItemClick: ((TestDetailModel) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestsViewHolder {
        val binding: ItemTestDetailBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_test_detail, parent, false
        )
        return TestsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (list.size > 0) list.size else 0
    }

    override fun onBindViewHolder(holder: TestsViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun setData(list: ArrayList<TestDetailModel>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class TestsViewHolder(var binding: ItemTestDetailBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: TestDetailModel) {
            binding.testDetail = item
            Glide.with(itemView.context).load(item.image).into(binding.img)
        }
    }
}