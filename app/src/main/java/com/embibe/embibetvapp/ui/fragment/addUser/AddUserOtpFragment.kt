package com.embibe.embibetvapp.ui.fragment.addUser

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentSigninOtpBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.adduser.AddUserRequest
import com.embibe.embibetvapp.model.adduser.AddUserResponse
import com.embibe.embibetvapp.model.auth.SignupVerifyOtpRequest
import com.embibe.embibetvapp.model.auth.SignupVerifyOtpResponse
import com.embibe.embibetvapp.model.auth.login.LoginResponse
import com.embibe.embibetvapp.model.auth.signup.SignupSendOtpRequest
import com.embibe.embibetvapp.model.auth.signup.SignupSendOtpResponse
import com.embibe.embibetvapp.ui.activity.AddUserActivity
import com.embibe.embibetvapp.ui.activity.UserSwitchActivity
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Status
import com.embibe.embibetvapp.utils.Utils
import com.embibejio.coreapp.preference.PreferenceHelper
import org.jetbrains.anko.doAsync

class AddUserOtpFragment : BaseAppFragment() {

    private lateinit var binding: FragmentSigninOtpBinding
    private lateinit var signInViewModel: SignInViewModel
    private lateinit var addUserRequest: AddUserRequest
    private var isTwoVerificationRequired: Boolean = false

    private var mobileOrEmail: String? = null
    private var email: String? = null
    private var mobile: String? = null
    private var userType: String? = null
    private var isFirst: Boolean = false
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_signin_otp, container, false)

        addUserRequest = requireArguments().getSerializable("addUserModel") as AddUserRequest
        isTwoVerificationRequired = requireArguments().getBoolean("isTwoVerificationRequired")
        mobileOrEmail = requireArguments().getString("mobileOrEmail", "")
        userType = requireArguments().getString("userType", "")
        isFirst = requireArguments().getBoolean("isFirst")

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        clearAllOTP()
        setTextWatcher()
        clickListeners(view)

        when (Utils.getInputType(mobileOrEmail!!)) {
            1 -> {
                binding.tv2.text =
                    resources.getString(R.string.enter_otp_sent_mobile_number) + "\n" + mobileOrEmail
                mobile = mobileOrEmail
            }
            2 -> {
                binding.tv2.text =
                    resources.getString(R.string.enter_otp_sent_email_id) + "\n" + mobileOrEmail
                email = mobileOrEmail
            }
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        callSendOtpApi(requireView())
        registerAddUserObserver()
    }

    private fun clearAllOTP() {
        binding.etOtp1.text.clear()
        binding.etOtp2.text.clear()
        binding.etOtp3.text.clear()
        binding.etOtp4.text.clear()
        binding.etOtp5.text.clear()
        binding.etOtp6.text.clear()
        binding.etOtp1.requestFocus()
        Handler().postDelayed({
            openSoftKeyboard(context, binding.etOtp1)
        }, 500)
    }

    private fun callSendOtpApi(view: View) {
        val model = SignupSendOtpRequest(
            mobile, email, "embibe_fiber_app", "600", "2"
        )

        showProgress()
        signInViewModel.sendOtp(model, object : BaseViewModel.APICallBacks<SignupSendOtpResponse> {
            override fun onSuccess(model: SignupSendOtpResponse?) {
                hideProgress()
                if (model?.result.equals("OTP sent.")) {
                    updateResult(view)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                retrySendOTPApi(code, error, view)
            }
        })
    }

    private fun retrySendOTPApi(code: Int, error: String, view: View) {
        if (Utils.isApiFailed(code)) {
            Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                override fun onRetry(msg: String) {
                    callSendOtpApi(view)
                }

                override fun onDismiss() {
                }
            })
        } else {
            /*update error msg in ui*/
        }
    }

    private fun updateResult(view: View) {
        /*OTP sent successfully!*/
        Toast.makeText(
            view.context,
            view.context.resources.getString(R.string.otp_sent_successfully),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun clickListeners(view: View) {
        binding.btnResendOtp.setOnClickListener {
            SegmentUtils.trackResendOtpBtnClick()
            clearAllOTP()
            callSendOtpApi(view)
        }

        binding.btnVerify.setOnClickListener {
            SegmentUtils.trackLoginVerifyOtpBtnClick()
            if (getEnterOtpText().length < 6) {
                Utils.showToast(
                    requireContext(),
                    requireActivity().getString(R.string.enter_corrent_otp)
                )
            } else {
                /* call verify otp api here */
                verifyOTP(getEnterOtpText(), mobileOrEmail ?: "")
            }
        }

        binding.btnClearOtp.setOnClickListener {
            SegmentUtils.tracClearOtpClick()
            binding.etOtp1.text.clear()
            binding.etOtp2.text.clear()
            binding.etOtp3.text.clear()
            binding.etOtp4.text.clear()
            binding.etOtp5.text.clear()
            binding.etOtp6.text.clear()
            binding.etOtp1.requestFocus()
        }
    }

    private fun verifyOTP(otp: String, id: String) {
        val model = SignupVerifyOtpRequest(otp, id)

        showProgress()
        signInViewModel.verifyOtp(
            model, object : BaseViewModel.APICallBacks<SignupVerifyOtpResponse> {
                override fun onSuccess(model: SignupVerifyOtpResponse?) {
                    hideProgress()
                    if (model?.result.equals("Success")) {
                        verificationSuccess()
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    hideProgress()
                    verificationFailed(msg)
                }
            })
    }

    private fun verificationSuccess() {
        Toast.makeText(
            requireView().context,
            requireView().context.resources.getString(R.string.otp_verified_successfully),
            Toast.LENGTH_SHORT
        ).show()
        if (isTwoVerificationRequired) {
            callAddOTPFragment(addUserRequest, addUserRequest.mobile!!, false)
        } else {
            if (userType.equals("child", true) || userType.equals("student", true)) {
                if (isFirst) setFragment(SelectGoalExamFragment())
                isFirst = false
            } else {
                callAddUserApi()
            }
        }
    }

    private fun verificationFailed(msg: String) {
        /* Failed*/
        Toast.makeText(
            requireView().context,
            requireView().context.resources.getString(R.string.otp_verification_failed),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun callAddOTPFragment(
        model: AddUserRequest,
        valueToVerify: String,
        isTwoVerificationRequired: Boolean
    ) {

        (activity as AddUserActivity).supportFragmentManager.beginTransaction().remove(this)
            .commit()

        val bundle = Bundle()
        bundle.putSerializable("addUserModel", model)
        bundle.putSerializable("isTwoVerificationRequired", isTwoVerificationRequired)
        bundle.putString("mobileOrEmail", valueToVerify)

        val fragment = AddUserOtpFragment()
        fragment.arguments = bundle
        (activity as AddUserActivity).setFragment(fragment, "add_user_otp_fragment_mobile")
    }

    private fun callAddUserApi() {
        signInViewModel.addUser(UserData.getUserID(), addUserRequest)
    }

    private fun registerAddUserObserver() {
        signInViewModel.addUserLiveData.observe(viewLifecycleOwner, Observer { resource ->
            if (resource != null) {
                when (resource.status) {
                    Status.LOADING -> showProgress()
                    Status.SUCCESS -> {
                        var response = resource.data as AddUserResponse
                        if (response.success
                            && (response.message.equals("user added", true)
                                    || response.message.equals("user updated", true))
                        ) {
                            getConnectedProfile()
                        } else {
                            Utils.showToast(
                                requireContext(),
                                getString(R.string.unable_to_add_user)
                            )
                        }
                        hideProgress()
                    }
                    Status.ERROR -> {
                        hideProgress()
                        Utils.showToast(requireContext(), getString(R.string.add_user_limit_exceed))

                    }
                }
            }
        })
    }

    private fun getConnectedProfile() {
        getLinkedProfiles()
    }

    private fun updateSignInResponse(response: LoginResponse) {
        doAsync {
            UserData.updateLinkedUser(response)
            if (PreferenceHelper()[AppConstants.HOST_PAGE, ""] == AppConstants.PROFILE) {
                (activity as AddUserActivity).finish()
            } else {
                startActivity(Intent(context, UserSwitchActivity::class.java))
                (activity as AddUserActivity).finish()
            }
        }
    }

    private fun getLinkedProfiles() {
        showProgress()
        signInViewModel.getLinkedProfilesApi(object : BaseViewModel.APICallBacks<LoginResponse> {
            override fun onSuccess(model: LoginResponse?) {

                if (model != null && model.success) {
                    makeLog("Authentication Success")
                    updateSignInResponse(model)
                } else {
                    makeLog("getLinkedProfilesApi api failed")
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                makeLog("getLinkedProfilesApi api error $error")
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getLinkedProfiles()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }

        })

    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if (event?.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    childFragmentManager.popBackStackImmediate(
                        "add_user_otp_fragment_email",
                        POP_BACK_STACK_INCLUSIVE
                    )
                }
            }
        }
        return false
    }

    private fun setFragment(fragment: Fragment) {
        val bundle = Bundle()
        bundle.putString("user_type", userType)
        bundle.putString("user_name", addUserRequest.first_name)
        bundle.putString("email", email)
        bundle.putString("avatarPosition", addUserRequest.profile_pic)
        fragment.arguments = bundle
        (activity as AddUserActivity).setFragment(fragment, "select_goal_exam_fragment")
    }

    private fun setTextWatcher() {
        binding.etOtp1.addTextChangedListener(GenericTextWatcher(binding.etOtp1))
        binding.etOtp2.addTextChangedListener(GenericTextWatcher(binding.etOtp2))
        binding.etOtp3.addTextChangedListener(GenericTextWatcher(binding.etOtp3))
        binding.etOtp4.addTextChangedListener(GenericTextWatcher(binding.etOtp4))
        binding.etOtp5.addTextChangedListener(GenericTextWatcher(binding.etOtp5))
        binding.etOtp6.addTextChangedListener(GenericTextWatcher(binding.etOtp6))
    }

    private fun getEnterOtpText(): String {
        return binding.etOtp1.text.toString() + binding.etOtp2.text.toString() + binding.etOtp3.text.toString() + binding.etOtp4.text.toString() + binding.etOtp5.text.toString() + binding.etOtp6.text.toString()
    }

    private fun setBackgroundNormal() {
        binding.etOtp1.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp2.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp3.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp4.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp5.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp6.background = requireContext().getDrawable(R.drawable.shape_rec_white)
    }

    inner class GenericTextWatcher(var view: View) : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            if (binding.tvWrongMessage.visibility == View.VISIBLE)
                binding.tvWrongMessage.visibility = View.GONE
            setBackgroundNormal()
            val text = editable.toString()
            when (view.id) {
                R.id.et_otp_1 -> if (text.length == 1) binding.etOtp2.requestFocus()
                R.id.et_otp_2 -> if (text.length == 1) binding.etOtp3.requestFocus() else if (text.isEmpty()) binding.etOtp1.requestFocus()
                R.id.et_otp_3 -> if (text.length == 1) binding.etOtp4.requestFocus() else if (text.isEmpty()) binding.etOtp2.requestFocus()
                R.id.et_otp_4 -> if (text.length == 1) binding.etOtp5.requestFocus() else if (text.isEmpty()) binding.etOtp3.requestFocus()
                R.id.et_otp_5 -> if (text.length == 1) binding.etOtp6.requestFocus() else if (text.isEmpty()) binding.etOtp4.requestFocus()
                R.id.et_otp_6 -> {
                    if (text.isEmpty()) binding.etOtp5.requestFocus()
                    if (text.length == 1) {
                        binding.btnVerify.requestFocus()
                        Utils.hideKeyboardFrom(context, binding.etOtp3.rootView)
                    }
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }

    fun openSoftKeyboard(context: Context?, view: View) {
        view.requestFocus()
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_FORCED)

    }


}