package com.embibe.embibetvapp.ui.activity

import android.os.Bundle
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity

class TermsAndConditionActivity : BaseFragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_condition)
    }
}