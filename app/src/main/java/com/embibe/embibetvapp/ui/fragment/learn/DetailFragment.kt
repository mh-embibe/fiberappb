package com.embibe.embibetvapp.ui.fragment.learn

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentDetailBinding
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.ui.activity.BookDetailActivity
import com.embibe.embibetvapp.ui.activity.DetailActivity
import com.embibe.embibetvapp.ui.activity.PracticeSummaryActivity
import com.embibe.embibetvapp.ui.adapter.DetailAdapter
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.insertBundle
import com.embibe.embibetvapp.utils.Utils.loadConceptsAsync


class DetailFragment : BaseAppFragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var data: java.util.ArrayList<Content>
    private lateinit var results: List<Results>
    private lateinit var type: String
    private lateinit var title: String
    private lateinit var binding: FragmentDetailBinding
    private lateinit var detailAdapter: DetailAdapter
    private var nItems: Int = 0

    private var dataList: ArrayList<Content> = ArrayList()
    private var moreConcepts: List<Results> = arrayListOf()
    private var relatedConcepts: List<Results> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        type = this.requireArguments().getString("type").toString()
        title = this.requireArguments().getString("title").toString()
        moreConcepts = this.requireArguments().getParcelableArrayList(AppConstants.MORE_TOPICS)
            ?: arrayListOf()
        relatedConcepts =
            this.requireArguments().getParcelableArrayList(AppConstants.RELATED_CONCEPTS)
                ?: arrayListOf()
        makeLog("Concepts moreConcepts - ${moreConcepts.size}")
        makeLog("Concepts relatedConcepts received ${relatedConcepts.size}")
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getData(view)
//        binding.tvDetailName.text = "Concept: $title"
    }

    private fun getData(view: View) {
        results = if (type == AppConstants.RELATED_CONCEPTS) {
            relatedConcepts
        } else {
            moreConcepts
        }
        data = createData(results, dataList)
//        binding.textNConcepts.text = if (nItems <= 1) {
//            "$nItems item"
//        } else {
//            "$nItems items"
//        }
        setDetailRecycler(view, data)

        SegmentUtils.trackEventMoreInfoMainMenuResultLoadStart(title, type, data.size)
    }

    override fun onResume() {
        super.onResume()
        SegmentUtils.trackEventMoreInfoMainMenuResultLoadEnd(title, type, data.size)
    }

    private fun setDetailRecycler(view: View, data: ArrayList<Content>) {
        detailAdapter = DetailAdapter(view.context)
        var layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        binding.detailRecycler.adapter = detailAdapter
        binding.detailRecycler.layoutManager = layoutManager
        detailAdapter.setData(data)

        detailAdapter.onItemClick = { content ->
            startActivityByType(content)
        }
    }

    private fun startActivityByType(content: Content) {

        when ((content.type).toLowerCase()) {
            AppConstants.TYPE_VIDEO, AppConstants.TYPE_COOBO -> {
                startDetailActivity(content)
            }
            AppConstants.TYPE_BOOK -> {
                startBookDetailActivity(content)
            }
            AppConstants.TYPE_PRACTICE, AppConstants.TYPE_CHAPTER -> {
                startPracticeDetailActivity(content)
            }
        }
    }

    private fun startDetailActivity(content: Content) {
        loadConceptsAsync(content, context)
        val intent = Intent(App.context, DetailActivity::class.java)
        insertBundle(content, intent)
        startActivity(intent)
    }

    private fun startBookDetailActivity(content: Content) {
        var intent = Intent(App.context, BookDetailActivity::class.java)
        intent.putExtra(AppConstants.NAV_NAME_TYPE, AppConstants.NAV_TYPE_BOOK)
        insertBundle(content, intent)
        startActivity(intent)
    }

    private fun startPracticeDetailActivity(content: Content) {
        Utils.loadPracticeSummaryInfo(content, context)
        val intent = Intent(App.context, PracticeSummaryActivity::class.java)
        insertBundle(content, intent)
        startActivity(intent)
    }


    private fun createData(
        results: List<Results>,
        dataList: java.util.ArrayList<Content>
    ): ArrayList<Content> {

        for (data in results) {
            var content = Content()
            content.type = "header"
            content.section_name = data.section_name
            dataList.add(content)

            for (index in data.content.indices) {
                if (data.content[index].category.isNullOrEmpty()) { /* hard code the category while api res null*/
                    data.content[index].category = ""
                    dataList.add(data.content[index])
                } else {
                    dataList.add(data.content[index])
                }
                nItems++

            }
        }
        return dataList
    }
}
