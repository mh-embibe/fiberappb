package com.embibe.embibetvapp.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.RowItemClassBinding
import com.embibe.embibetvapp.utils.ContantUtils

class RvAdapterClass() : RecyclerView.Adapter<RvAdapterClass.VHClass>() {

    lateinit var binding: RowItemClassBinding
    private var selectedItem = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHClass {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_item_class,
            parent,
            false
        )
        return VHClass(binding)
    }

    override fun getItemCount(): Int {
        return ContantUtils.classData.size
    }

    override fun onBindViewHolder(holder: VHClass, position: Int) {
        holder.bind(position)
    }

    //
    inner class VHClass(var binding: RowItemClassBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.tvClass.text = ContantUtils.classData[position]

            binding.clItem.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    binding.tvClass.setTextColor(Color.parseColor("#000000"))
                } else {
                    binding.tvClass.setTextColor(Color.parseColor("#ffffff"))
                }
            }

            if (selectedItem == adapterPosition) {
                binding.clItem.setBackgroundResource(R.drawable.bg_acheive_potential_selected)
                binding.tvClass.setTextColor(Color.parseColor("#000000"))
            } else {
                binding.clItem.setBackgroundResource(R.drawable.bg_acheive_potential_selector)
                binding.tvClass.setTextColor(Color.parseColor("#ffffff"))
            }
            itemView.setOnClickListener {
                if (selectedItem != adapterPosition) {
                    selectedItem = adapterPosition
                    notifyDataSetChanged()
                } else {
                    selectedItem = -1
                    notifyDataSetChanged()
                }
            }
        }
    }
}