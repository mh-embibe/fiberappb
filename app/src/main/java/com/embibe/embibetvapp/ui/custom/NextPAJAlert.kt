package com.embibe.embibetvapp.ui.custom

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.FragmentNextPAJAlertBinding

class NextPAJAlert : DialogFragment() {

    private lateinit var binding: FragmentNextPAJAlertBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, android.R.style.ThemeOverlay)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_next_p_a_j_alert, container, false)
        return binding.root
    }
}