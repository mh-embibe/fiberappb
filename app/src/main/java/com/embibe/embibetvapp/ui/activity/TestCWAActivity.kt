package com.embibe.embibetvapp.ui.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.databinding.ActivityChapterAnlaysisFeedbackBinding
import com.embibe.embibetvapp.databinding.ItemTestDetailMenuBinding
import com.embibe.embibetvapp.model.test.ChapterListData
import com.embibe.embibetvapp.model.test.SubFeedbackDetailMenuModel
import com.embibe.embibetvapp.model.test.TestAchieveRes
import com.embibe.embibetvapp.ui.adapter.TestFeedbackDetailMenuAdapter
import com.embibe.embibetvapp.ui.viewmodel.TestViewModel
import com.embibe.embibetvapp.utils.BlurTransformation
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.google.gson.Gson

class TestCWAActivity : BaseFragmentActivity() {

    private lateinit var binding: ActivityChapterAnlaysisFeedbackBinding
    private lateinit var menuAdapter: TestFeedbackDetailMenuAdapter
    private lateinit var title: String
    private var position: Int = 0
    private var testAchieveRes: TestAchieveRes? = null
    private lateinit var testViewModel: TestViewModel

    companion object {
        fun getActivityIntent(context: Context, title: String, position: Int): Intent {
            return Intent(context, TestCWAActivity::class.java)
                .putExtra("title", title)
                .putExtra("position", position)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chapter_anlaysis_feedback)
        testViewModel = ViewModelProviders.of(this).get(TestViewModel::class.java)
        SegmentUtils.trackTestFeedbackScreenChapterWiseAnalysisLoadStart()
        title = intent.getStringExtra("title")
        position = intent.getIntExtra("position", 0)

        with(DataManager.instance) {
            testAchieveRes = Gson().fromJson(getTestAchieve(), TestAchieveRes::class.java)
        }

        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)

        binding.tvDropDownSubject.requestFocus()
        binding.tvType.text = title

        updateTitleComponent(requestOptions)
        updateClickToWatchComponent(requestOptions)
        setRecycler(testAchieveRes?.revisions!![position].chapter_list_data)

        binding.arrowBack.setOnClickListener {
            finish()
        }
        SegmentUtils.trackTestFeedbackScreenChapterWiseAnalysisLoadEnd()
    }

    private fun updateTitleComponent(requestOptions: RequestOptions) {
        binding.tvType.text = getString(R.string.showing)
            .replace("*title*", title)

        Glide.with(this).setDefaultRequestOptions(requestOptions)
            .load(Utils.getDrawableByTitle(title))
            .transition(DrawableTransitionOptions.withCrossFade())
            .transform(BlurTransformation(App.context))
            .into(binding.textBg)

        binding.textBg.setBlur(20)
    }

    @SuppressLint("SetTextI18n")
    private fun updateClickToWatchComponent(requestOptions: RequestOptions) {

        binding.layoutClickToWatch.textClickToWatch.text =
            getString(R.string.click_to_understand_how_this_is_calculated)

//        binding.layoutClickToWatch.textEmbiums.text =
//            "${getString(R.string.earn)} ${testAttemptsRes.actionReplay?.embium}"

        Glide.with(this).setDefaultRequestOptions(requestOptions)
            .load(R.drawable.video_chapter_wise)
            .transition(DrawableTransitionOptions.withCrossFade())
            .transform(RoundedCorners(20))
            .into(binding.layoutClickToWatch.ivThumbnail)
    }

    private fun setRecycler(chapterListData: List<ChapterListData>?) {
        menuAdapter = TestFeedbackDetailMenuAdapter()
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.layoutAchieve.recycler.adapter = menuAdapter
        binding.layoutAchieve.recycler.layoutManager = layoutManager
        menuAdapter.setData(createChapterData(chapterListData as ArrayList<ChapterListData>))

        menuAdapter.onItemClick = { chapterListData, binding, view ->
            // call topic summary API here using name and lmformat
            getTopicSummary(
                binding,
                view,
                DataManager.instance.testCode,
                chapterListData.chapter_name!!,
                chapterListData.lmformat!!
            )
        }
    }

    private fun createChapterData(arrayList: ArrayList<ChapterListData>): ArrayList<ChapterListData> {
        return arrayList
    }


    /*get Topic Summary*/
    private fun getTopicSummary(
        binding: ItemTestDetailMenuBinding,
        view: View,
        testCode: String,
        chapter_name: String,
        chapter_learning_map: String
    ) {
        testViewModel.getTopicSummary(
            testCode, chapter_name, chapter_learning_map,
            object : BaseViewModel.APICallBacks<List<SubFeedbackDetailMenuModel>> {
                override fun onSuccess(model: List<SubFeedbackDetailMenuModel>?) {
                    if (model != null) {
                        menuAdapter.setRecycler(
                            binding,
                            view,
                            model as ArrayList<SubFeedbackDetailMenuModel>
                        )
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLogI("$code - $error - $msg")
                }

            })
    }
}