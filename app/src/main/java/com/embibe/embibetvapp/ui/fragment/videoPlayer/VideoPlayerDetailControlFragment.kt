package com.embibe.embibetvapp.ui.fragment.videoPlayer

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentVideoPlayerControlBinding
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.ui.activity.VideoPlayerActivity
import com.embibe.embibetvapp.ui.interfaces.ControlToPlayerListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.utils.Utils.getConvertedTime

class VideoPlayerDetailControlFragment : Fragment(), NavigationMenuCallback {

    private lateinit var runnable: Runnable
    private lateinit var binding: FragmentVideoPlayerControlBinding
    private lateinit var controlToPlayerListener: ControlToPlayerListener
    private lateinit var prerequisiteFragment: PrerequisiteFragment
    private lateinit var videoTitle: String
    private lateinit var videoDescription: String
    private lateinit var videoSubject: String
    private lateinit var videoChapter: String
    private lateinit var videoAuthors: String

    private var hideControllerHandler = Handler()
    private var isVideoReady: Boolean = false
    private var isVideoBuffering: Boolean = true
    var isControllerShown: Boolean = false
    var isPreRequisitePresent: Boolean = false
    var currentDurationHandler = Handler()
    var currentDuration = 0L
    var totalDuration = 0L

    private var hideControllerRunnable = Runnable {
        hideDetail()
        hidePreRequisite()
        hideController()
    }

    companion object {
        const val TIME_TO_WAIT = 4000
        const val SEEK_BAR_RUNNABLE_WAIT_TIME = 2000
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_video_player_control,
            container,
            false
        )

        val bundle = this.arguments
        if (bundle != null) {
            videoTitle = bundle.getString(AppConstants.VIDEO_TITLE, "")
            videoDescription = bundle.getString(AppConstants.VIDEO_DESCRIPTION, "")
            videoSubject = bundle.getString(AppConstants.VIDEO_SUBJECT, "")
            videoChapter = bundle.getString(AppConstants.VIDEO_CHAPTER, "")
            videoAuthors = bundle.getString(AppConstants.VIDEO_AUTHORS, "")
        }

        setTitleAndDescription()
        //uncomment to show the
        //setAchieveProgressValue(58)
        prerequisiteFragment =
            PrerequisiteFragment()


        return binding.root
    }

    private fun setAchieveProgressValue(progressValue: Int) {
//        binding.clDetail.visibility = View.GONE
//        binding.pajComponent.constraintClickToWatch.visibility = View.VISIBLE
//        binding.pajComponent.pajProgress.progress = progressValue
//        binding.pajComponent.progressPercentage.text = "$progressValue%"
//        binding.pajComponent.txtPaj.text = "Personalised Achievement Journey"
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showDetail()
        showController()

        setMainLayoutKeyListener()
        setPlayPauseKeyListener()
        setRewindKeyListener()
        setForwardKeyListener()
        setBookmarkKeyListener()
        setLikeKeyListener()
        setSeekerChangeListener()
        setFocusInView()
        setFragment(prerequisiteFragment, binding.preRequisiteView.id)
    }

    private fun setFocusInView() {
        binding.btnBookmark.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    controlToPlayerListener.bookMarkFocus()

                }
            }
        }
        binding.btnLike.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    controlToPlayerListener.likeFocus()
                }
            }
        }

    }

    @SuppressLint("SetTextI18n")
    private fun setTitleAndDescription() {
        if (videoTitle != "") {
            binding.videoTitle.text = videoTitle
        } else binding.videoTitle.visibility = View.GONE

        if (videoDescription != "") {
            binding.videoDescription.text = videoDescription
        } else binding.videoDescription.visibility = View.GONE

        if (videoSubject != "" && videoChapter != "" && videoAuthors != "") {
            binding.videoSubDescription.text =
                "${resources.getString(R.string.video_subject)} $videoSubject ${
                    resources.getString(
                        R.string.video_hash
                    )
                } ${resources.getString(R.string.video_chapter)} $videoChapter ${
                    resources.getString(
                        R.string.video_hash
                    )
                } ${resources.getString(R.string.video_author)} $videoAuthors"

        } else if (videoSubject != "" && videoChapter != "" && videoAuthors == "") {
            binding.videoSubDescription.text =
                "${resources.getString(R.string.video_subject)} $videoSubject ${
                    resources.getString(
                        R.string.video_hash
                    )
                } ${resources.getString(R.string.video_chapter)} $videoChapter"
        } else if (videoSubject != "" && videoAuthors != "" && videoChapter == "") {
            binding.videoSubDescription.text =
                "${resources.getString(R.string.video_subject)} $videoSubject ${
                    resources.getString(
                        R.string.video_hash
                    )
                } ${resources.getString(R.string.video_author)} $videoAuthors"
        } else if (videoChapter != "" && videoAuthors != "" && videoSubject == "") {
            binding.videoSubDescription.text =
                "${resources.getString(R.string.video_chapter)} $videoChapter ${
                    resources.getString(
                        R.string.video_hash
                    )
                } ${resources.getString(R.string.video_author)} $videoAuthors"
        } else if (videoSubject != "" && videoChapter == "" && videoAuthors == "") {
            binding.videoSubDescription.text =
                "${resources.getString(R.string.video_subject)} $videoSubject"
        } else if (videoChapter != "" && videoSubject == "" && videoAuthors == "") {
            binding.videoSubDescription.text =
                "${resources.getString(R.string.video_chapter)} $videoChapter"
        } else if (videoAuthors != "" && videoChapter == "" && videoSubject == "") {
            binding.videoSubDescription.text =
                "${resources.getString(R.string.video_author)} $videoAuthors"
        } else {
            binding.videoSubDescription.visibility = View.GONE
        }
    }

    fun setVideoTotalDuration() {
        totalDuration = controlToPlayerListener.getVideoTotalDuration()
        binding.seekbar.max = totalDuration.toInt()

        runnable = object : Runnable {
            override fun run() {
                currentDuration = controlToPlayerListener.getCurrentDuration()
                updateSeekBar(currentDuration)
                if (activity == null) {
                    currentDurationHandler.removeCallbacks(runnable)
                } else {
                    if ((activity as VideoPlayerActivity).annotationEnabled) {
                        if ((activity as VideoPlayerActivity).checkAnnotation(currentDuration)
                        ) {
                            currentDurationHandler.removeCallbacks(this)
                            controlToPlayerListener.annotationPoint(currentDuration)
                            Log.d("tag", "setVideoTotalDuration$currentDuration")
                        }
                    }

                    if (currentDuration == totalDuration) {
                        currentDurationHandler.removeCallbacks(this)
                    } else {
                        currentDurationHandler.postDelayed(this, 1000)
                    }
                }


            }
        }
        requireActivity().runOnUiThread(runnable)
    }

    @SuppressLint("SetTextI18n")
    private fun updateSeekBar(currentProgress: Long) {
//        Log.i("VIDEO PLAYER", "$currentProgress")
        binding.seekbar.progress = currentProgress.toInt()
        binding.textTime.text =
            "${getConvertedTime(currentProgress)} / ${getConvertedTime(totalDuration)}"
    }

    override fun onStop() {
        closeHandler()
        super.onStop()
    }

    fun closeHandler() {
        if (::runnable.isInitialized) {
            currentDurationHandler.removeCallbacks(runnable)
        }
    }

    private fun showDetail() {
        binding.playerDetailsView.visibility = View.VISIBLE
        binding.playerDetailsView.animate()
            .translationY(0f)
            .alpha(1.0f)
            .setDuration(500)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    binding.playerDetailsView.clearAnimation()
                    binding.layoutMain.setBackgroundColor(activity!!.resources.getColor(R.color.player_background))
                }
            })
    }

    private fun hideDetail() {
        binding.playerDetailsView.animate()
            .translationY(-binding.playerDetailsView.height.toFloat())
            .alpha(0.0f)
            .setDuration(500)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    try {
                        binding.playerDetailsView.clearAnimation()
                        binding.playerDetailsView.visibility = View.GONE
                        binding.layoutMain.setBackgroundColor(activity!!.resources.getColor(R.color.transparent))
                    } catch (e: Exception) {

                    }
                }
            })
    }

    private fun showController() {
        binding.playerControlView.visibility = View.VISIBLE
        binding.playerControlView.animate()
            .translationY(0f)
            .alpha(1.0f)
            .setDuration(500)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    Handler().postDelayed({ binding.focusedBtn.clearFocus() }, 10)
                    Handler().postDelayed({ binding.btnPlayPause.requestFocus() }, 10)
                    binding.playerControlView.clearAnimation()
                    isControllerShown = true
                }
            })

        // start timer to hide detail and controller after 4 seconds
        if (isVideoReady) {
            startTimer()
        }
    }

    private fun hideController() {
        binding.playerControlView.animate()
            .translationY(binding.playerControlView.height.toFloat())
            .alpha(0.0f)
            .setDuration(500)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    binding.playerControlView.clearAnimation()
                    Handler().postDelayed({ binding.btnPlayPause.clearFocus() }, 10)
                    Handler().postDelayed({ binding.focusedBtn.requestFocus() }, 10)
                    binding.playerControlView.visibility = View.GONE
                    isControllerShown = false
                }
            })
    }

    private fun showPreRequisite() {
        binding.preRequisiteView.visibility = View.VISIBLE
        binding.preRequisiteView.animate()
            .translationY(0f)
            .alpha(1.0f)
            .setDuration(200)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    binding.preRequisiteView.clearAnimation()
                }
            })
    }

    private fun hidePreRequisite() {
        binding.preRequisiteView.animate()
            .translationY(binding.preRequisiteView.height.toFloat())
            .alpha(0.0f)
            .setDuration(200)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    binding.preRequisiteView.clearAnimation()
                    binding.preRequisiteView.visibility = View.GONE
                }
            })
    }

    private fun startTimer() {
        hideControllerHandler.postDelayed(hideControllerRunnable, TIME_TO_WAIT.toLong())
    }

    fun stopTimer() {
        hideControllerHandler.removeCallbacks(hideControllerRunnable)
    }

    fun restartTimer() {
        hideControllerHandler.removeCallbacks(hideControllerRunnable)
        hideControllerHandler.postDelayed(hideControllerRunnable, TIME_TO_WAIT.toLong())
    }

    private fun setMainLayoutKeyListener() {
        binding.focusedBtn.setOnKeyListener { view, keyCode, keyEvent ->
            when (keyCode) {
                KeyEvent.KEYCODE_DPAD_UP, KeyEvent.KEYCODE_DPAD_DOWN, KeyEvent.KEYCODE_DPAD_LEFT,
                KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {

                    Handler().postDelayed({ binding.focusedBtn.clearFocus() }, 10)
                    Handler().postDelayed({ binding.btnPlayPause.requestFocus() }, 10)
                    showDetail()
                    showController()
                }
            }
            false
        }
    }

    private fun setSeekerChangeListener() {
        binding.seekbar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    restartTimer()
                    updateSeekBar(progress.toLong())
                    if (currentDuration < progress) {
                        controlToPlayerListener.forwardByPercentage(progress)
                    } else {
                        controlToPlayerListener.rewindByPercentage(progress)
                    }
                }
            }
        })

        binding.seekbar.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_DPAD_LEFT -> {
                        /**
                         * As soon as the right,left key is released(after dragging),
                         * removing SeekBar runnable(Thread) and asking it to execute after a delay
                         * In the mean time, SeekBar is updated from the callback
                         * from onProgressChanged() of OnSeekBarChangeListener
                         */
                        if (isVideoReady) {
                            currentDurationHandler.removeCallbacks(runnable)
                            currentDurationHandler.postDelayed(
                                runnable,
                                SEEK_BAR_RUNNABLE_WAIT_TIME.toLong()
                            )
                        }
                    }
                }
            }

            false
        }
    }

    private fun setPlayPauseKeyListener() {

        binding.btnPlayPause.setOnKeyListener { view, keyCode, keyEvent ->
            if (keyEvent.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (isPreRequisitePresent) {
                            navMenuToggle(true)
                        } else removeFocusDownFromPlay()
                    }
                }
            } else {
                when (keyCode) {
                    KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                        controlToPlayerListener.playPause()
                    }
                }
            }
            false
        }
    }

    private fun setForwardKeyListener() {
        binding.btnForward.setOnKeyListener { view, keyCode, keyEvent ->
            when (keyCode) {
                KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                    controlToPlayerListener.forward10Seconds()
                }
            }
            false
        }
    }

    private fun setRewindKeyListener() {
        binding.btnRewind.setOnKeyListener { v, keyCode, event ->
            when (keyCode) {
                KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                    controlToPlayerListener.rewind10Seconds()
                }
            }
            false
        }
    }

    private fun setBookmarkKeyListener() {
        binding.btnBookmark.setOnClickListener {
            controlToPlayerListener.bookMark()
        }
    }

    private fun setLikeKeyListener() {
        binding.btnLike.setOnClickListener {
            controlToPlayerListener.like()
        }
    }

    fun updatePlayPauseUI(isPlaying: Boolean) {
        if (isPlaying) {
            binding.btnPlayPause.background =
                requireContext().getDrawable(R.drawable.player_pause_state_selector)
        } else {
            binding.btnPlayPause.background =
                requireContext().getDrawable(R.drawable.player_play_state_selector)
        }
    }

    fun updateBookmarkUI(isBookmarked: Boolean) {
        if (isBookmarked) {
            binding.btnBookmark.background =
                requireContext().getDrawable(R.drawable.player_bookmark_state_selector_filled)
        } else binding.btnBookmark.background =
            requireContext().getDrawable(R.drawable.player_bookmark_state_selector_stroke)
    }

    fun updateLikeUI(isLiked: Boolean) {
        if (isLiked) {
            binding.btnLike.background =
                requireContext().getDrawable(R.drawable.player_favorite_state_selector_filled)
        } else binding.btnLike.background =
            requireContext().getDrawable(R.drawable.player_favorite_state_selector_stroke)
    }

    fun onVideoReady(isReady: Boolean) {
        isVideoReady = isReady

        if (isReady) {
            hideDetail()
            hideController()
        }
    }

    fun onVideoEnd() {
        Handler().postDelayed({ binding.focusedBtn.clearFocus() }, 10)
    }

    fun onVideoBuffering(isBuffering: Boolean) {
        isVideoBuffering = isBuffering
    }

    private fun removeFocusDownFromPlay() {
        binding.btnPlayPause.nextFocusDownId = binding.btnPlayPause.id
        binding.preRequisiteView.visibility = View.GONE
    }

    private fun setFragment(fragment: Fragment, layoutResId: Int) {
        fragment.view?.clearFocus()
        childFragmentManager.beginTransaction().replace(
            layoutResId,
            fragment
        ).commit()
    }

    fun setData(data: ArrayList<Results>) {
        prerequisiteFragment.setData(data)
    }

    fun setControllerToPlayerListener(callback: ControlToPlayerListener) {
        this.controlToPlayerListener = callback
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)
        when (fragment) {
            is PrerequisiteFragment -> {
                fragment.setNavigationMenuListener(this)
            }
        }
    }

    override fun navMenuToggle(toShow: Boolean) {
        if (toShow) {
            binding.btnPlayPause.postDelayed({ binding.btnPlayPause.clearFocus() }, 10)
            showPreRequisite()
        } else {
            hidePreRequisite()
            binding.btnPlayPause.postDelayed({ binding.btnPlayPause.requestFocus() }, 10)
        }
    }


}
