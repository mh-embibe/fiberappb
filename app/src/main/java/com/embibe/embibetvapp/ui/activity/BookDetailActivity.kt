package com.embibe.embibetvapp.ui.activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.android.volley.VolleyError
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App.Companion.context
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.BOOK
import com.embibe.embibetvapp.constant.AppConstants.BOOK_ID
import com.embibe.embibetvapp.constant.AppConstants.CFU_PRACTICE
import com.embibe.embibetvapp.constant.AppConstants.CONTENT_TYPE
import com.embibe.embibetvapp.constant.AppConstants.END_SESSION
import com.embibe.embibetvapp.constant.AppConstants.FORMAT_ID
import com.embibe.embibetvapp.constant.AppConstants.IS_EMBIBE_SYLLABUS
import com.embibe.embibetvapp.constant.AppConstants.IS_RECOMMENDATION_ENABLED
import com.embibe.embibetvapp.constant.AppConstants.LEARN_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_FORMAT_NAME
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_NAME
import com.embibe.embibetvapp.constant.AppConstants.LM_NAME
import com.embibe.embibetvapp.constant.AppConstants.LPCODE
import com.embibe.embibetvapp.constant.AppConstants.NAME
import com.embibe.embibetvapp.constant.AppConstants.NAV_TYPE_SYLLABUS
import com.embibe.embibetvapp.constant.AppConstants.NORMAL_PRACTICE
import com.embibe.embibetvapp.constant.AppConstants.PRACTICE
import com.embibe.embibetvapp.constant.AppConstants.PRACTICE_MODE
import com.embibe.embibetvapp.constant.AppConstants.PracticeModes
import com.embibe.embibetvapp.constant.AppConstants.PracticeModes.*
import com.embibe.embibetvapp.constant.AppConstants.SOURCE_REFERENCE
import com.embibe.embibetvapp.constant.AppConstants.TOPIC_LEARN_PATH
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_AUTHORS
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CONCEPT_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_DESCRIPTION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SOURCE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SUBJECT
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TITLE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TOTAL_DURATION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_URL
import com.embibe.embibetvapp.constant.AppConstants._ID
import com.embibe.embibetvapp.databinding.ActivityBookDetailWebBinding
import com.embibe.embibetvapp.model.home.BooksResponse.BookDetail
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.interfaces.CommonAndroidAPI
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener
import com.embibe.embibetvapp.ui.viewmodel.BookViewModel
import com.embibe.embibetvapp.unity.UnityPlayerActivity
import com.embibe.embibetvapp.utils.ConnectionManager
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.VideoUtils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_practice.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class BookDetailActivity : BaseFragmentActivity(), CommonAndroidAPI.AndroidAPIOnCallListener,
    ErrorScreensBtnClickListener {

    private lateinit var binding: ActivityBookDetailWebBinding
    private lateinit var bookViewModel: BookViewModel

    private lateinit var content: Content
    private lateinit var book: BookDetail
    private var navType: String = AppConstants.NAV_TYPE_BOOK
    private var language = "en"
    private var query = ""

    companion object {
        const val TEMPLATE_BOOK = "book/index.html"
        const val TEMPLATE_SYLLABUS = "syllabus/index.html"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_book_detail_web)
        bookViewModel = ViewModelProviders.of(this).get(BookViewModel::class.java)
        //  nav_type = intent.getStringExtra(AppConstants.NAV_NAME_TYPE)?:AppConstants.NAV_TYPE_BOOK
        //showProgress()
        content = getContent(intent)
        navType = getNavType(content)
        setBook()
        loadWebView()

    }

    private fun getNavType(content: Content): String {
        return if (content.type.equals(LEARN_CHAPTER)) NAV_TYPE_SYLLABUS else AppConstants.NAV_TYPE_BOOK
    }

    private fun loadWebView() {
        if (ConnectionManager.instance.hasNetworkAvailable()) {
            setBookWebView()

        } else {
            hideProgress()
            Utils.showError(this@BookDetailActivity, ApiConstants.API_CODE_NO_NETWORK,
                object : BaseViewModel.ErrorCallBacks {
                    override fun onRetry(msg: String) {
                        loadWebView()
                    }

                    override fun onDismiss() {
                        finish()
                    }
                })
        }
    }

    private fun setBook() {
        book = BookDetail()
        book.bookId = content.id
        if (navType == NAV_TYPE_SYLLABUS) {
            book.bookId = content.learnmap_id
            book.lpcode = content.learning_map.lpcode
        }
        book.name = content.title
        book.description = content.description
        book.embiums = content.currency.toDouble()
        book.duration = Utils.getDuration(content.length)
        book.rating = 4.0
        book.subject = content.subject
        book.subtitle = content.title
        book.thumbnailUrl = content.bg_thumb
        book.navType = navType
        book.authors = content.authors.joinToString(separator = ",")
        book.response = content
        book.bg_url = Utils.getSubjectBackground(book.subject)
    }

    private fun setBookWebView() {
        webView.webChromeClient = WebChromeClient()
        webView.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                hideProgress()
            }
        }
        webView.settings.allowFileAccess = true
        webView.isLongClickable = false
        webView.settings.javaScriptEnabled = true
        webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        webView.setBackgroundColor(Color.argb(0, 0, 0, 0))
        webView.addJavascriptInterface(CommonAndroidAPI(this), CommonAndroidAPI.NAME)
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        val base = filesDir.path.toString() + "/assets/"
        language = Utils.getCurrentLocale()

        if (language != null)
            query = "?language=$language"
        var template = ""
        if (navType == NAV_TYPE_SYLLABUS)
            template = "file:///android_asset/$TEMPLATE_SYLLABUS$query"
        else
            template = "file:///android_asset/$TEMPLATE_BOOK$query"
        // template = "https://finder-fiber.netlify.com/"
        //template = "http://52.172.137.100:61613/"
        //disableLongPress(webView)
        webView.loadUrl(template)
    }


    private fun callPractice(
        id: String,
        title: String,
        mode: PracticeModes,
        formatId: String,
        lmname: String,
        learnPathName: String,
        learnPathFormatName: String
    ) {
        callPractice(id, title, null, mode, formatId, lmname, learnPathName, learnPathFormatName)
    }

    private fun callPractice(
        id: String,
        title: String,
        lpcode: String?,
        mode: PracticeModes,
        formatId: String,
        lmname: String,
        learnPathName: String,
        learnPathFormatName: String
    ) {
        val intent = Intent(context, PracticeActivity::class.java)
        intent.putExtra("id", id)
        intent.putExtra("title", title)
        intent.putExtra("subject", book.subject)
        intent.putExtra(FORMAT_ID, formatId)
        intent.putExtra(LM_NAME, lmname)
        intent.putExtra(PRACTICE_MODE, mode)
        intent.putExtra(AppConstants.CONCEPT_ID, content.learning_map.conceptId)
        intent.putExtra(AppConstants.LENGTH, content.length)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, learnPathName)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, learnPathFormatName)
        if (lpcode != null) {
            val parts = lpcode.split("--")
            val lmCode = parts[parts.size - 1]
            val lmLevel = Utils.getLevelUsingCode(lmCode)
            val examCode = Utils.getExamLevelCode(parts)
            intent.putExtra("lm_level", lmLevel)
            intent.putExtra("lm_code", lmCode)
            intent.putExtra("exam_code", examCode)
        }

        startActivity(intent)
    }

    fun startVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        totalDuration: String,
        topicLearningPath: String,
        learnPathName: String,
        learnPathFormatName: String,
        formatId: String
    ) {
        val intent = Intent(context, VideoPlayerActivity::class.java)

        intent.putExtra(VIDEO_ID, id)
        intent.putExtra(CONTENT_TYPE, type)
        intent.putExtra(VIDEO_CONCEPT_ID, conceptId)
        intent.putExtra(VIDEO_URL, url)
        intent.putExtra(VIDEO_TITLE, title)
        intent.putExtra(VIDEO_DESCRIPTION, description)
        intent.putExtra(VIDEO_SUBJECT, subject)
        intent.putExtra(VIDEO_CHAPTER, chapter)
        intent.putExtra(VIDEO_TOTAL_DURATION, totalDuration)
        intent.putExtra(VIDEO_AUTHORS, authors)
        intent.putExtra(SOURCE_REFERENCE, BOOK)
        intent.putExtra(TOPIC_LEARN_PATH, topicLearningPath)
        intent.putExtra(VIDEO_SOURCE, source)
        intent.putExtra(FORMAT_ID, formatId)
        intent.putExtra(LEARN_PATH_NAME, learnPathName)
        intent.putExtra(LEARN_PATH_FORMAT_NAME, learnPathFormatName)
        if (navType == NAV_TYPE_SYLLABUS || navType == BOOK)
            intent.putExtra(IS_RECOMMENDATION_ENABLED, true)
        intent.putExtra(IS_EMBIBE_SYLLABUS, true)
        if (navType == BOOK) {
            intent.putExtra(BOOK_ID, content.id)
        }

        startActivity(intent)
    }

    private fun callVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        duration: String,
        topicLearningPath: String,
        learnPathName: String,
        learnPathFormatName: String,
        formatId: String,
        content: Content
    ) {
        if (source == AppConstants.PLAYER_TYPE_YOUTUBE) {
            if (Utils.isYouTubeAppAvailable(this)) {
                Utils.startYoutubeApp(this, url)
            } else {
                startVideo(
                    id,
                    type,
                    conceptId,
                    url,
                    title,
                    description,
                    source,
                    subject,
                    chapter,
                    authors,
                    duration,
                    topicLearningPath,
                    learnPathName, learnPathFormatName, formatId
                )
            }
        } else {
            val callback = object : BaseViewModel.APICallBackVolley {
                override fun <T> onSuccessCallBack(response: T) {
                    hideProgress()
                    response as JSONObject
                    val playableUrl = Utils.getVimeoHD((response["files"] as JSONArray))
                    startVideo(
                        id,
                        type,
                        conceptId,
                        playableUrl,
                        title,
                        description,
                        AppConstants.PLAYER_TYPE_EXOPLAYER,
                        subject,
                        chapter,
                        authors,
                        duration,
                        topicLearningPath,
                        learnPathName, learnPathFormatName, formatId
                    )
                    /*hide ProgressBar here */

                }

                override fun onErrorCallBack(e: VolleyError) {
                    try {
                        makeLog("Error:  $e")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    /*hide ProgressBar here */
                    hideProgress()
                }
            }
            showProgress()
            val videoId = VideoUtils.getVimeoVideoId(url).toString()
            Utils.getVideoURL(this, videoId, content.owner_info.key, callback)
        }
    }

    private fun callCoobo(url: String, id: String, length: Int) {
        val i = Intent(this, UnityPlayerActivity::class.java)
        i.putExtra(AppConstants.COOBO_URL, url)
        i.putExtra(AppConstants.DETAILS_ID, id)
        i.putExtra(AppConstants.DURATION_LENGTH, length)
        if (navType == NAV_TYPE_SYLLABUS || navType == AppConstants.BOOK)
            intent.putExtra(IS_RECOMMENDATION_ENABLED, true)
        startActivity(i)
    }

    override val question: String?
        get() = ""

    override fun getQuestion(questionCode: String?): String? {
        return ""
    }

    override val concept: String?
        get() = ""
    override val hint: String?
        get() = ""

    override fun setAnswer(): String? {
        return ""
    }

    override fun getSections(): String? {
        return ""
    }

    override fun getAnswer(): String? {
        return ""
    }

    override fun getQuestionSet(sectionId: String?): String? {
        return ""
    }

    override fun getData(): String? {
        Log.i("Book data res", Gson().toJson(book))
        return Gson().toJson(book)
    }

    override fun getData(data: String) {
        try {
            Log.i("Book data", data)
            val obj = JSONObject(data)
            makeLog("BookTOC Json $obj")
            val type = obj.getString(AppConstants.DETAILS_TYPE)
            var mode = BookPractice

            when (type.toLowerCase()) {
                PRACTICE, CFU_PRACTICE, NORMAL_PRACTICE -> {
                    if (type == NORMAL_PRACTICE)
                        mode = Normal
                    else if (type != PRACTICE)
                        mode = CFUPractice
                    val formatId =
                        if (obj.has(FORMAT_ID)) obj.getString(FORMAT_ID) else ""
                    val lmName =
                        if (obj.has(LM_NAME)) obj.getString(LM_NAME) else ""
                    val id =
                        if (obj.has(_ID)) obj.getString(_ID) else ""
                    val lpcode =
                        if (obj.has(LPCODE)) obj.getString(LPCODE) else ""
                    val name =
                        if (obj.has(NAME)) obj.getString(NAME) else ""
                    val learnPathName =
                        if (obj.has(LEARN_PATH_NAME)) obj.getString(LEARN_PATH_NAME) else ""
                    val learnPathFormatName =
                        if (obj.has(LEARN_PATH_FORMAT_NAME)) obj.getString(LEARN_PATH_FORMAT_NAME) else ""
                    if (mode == Normal) {
                        callPractice(
                            id,
                            name,
                            lpcode,
                            mode,
                            id,
                            lmName,
                            learnPathName,
                            learnPathFormatName
                        )
                    } else {
                        if (navType.toLowerCase() == BOOK.toLowerCase())
                            callPractice(
                                id,
                                name,
                                mode,
                                formatId,
                                lmName,
                                learnPathName,
                                learnPathFormatName
                            )
                        else
                            callPractice(
                                id,
                                name,
                                lpcode,
                                mode,
                                formatId,
                                lmName,
                                learnPathName,
                                learnPathFormatName
                            )
                    }
                }

                AppConstants.VIDEO -> {
                    var duration = ""
                    var url = ""
                    var source = ""
                    var id = ""
                    var title = ""
                    var conceptId = ""
                    doAsync {
                        val gson = Gson()
                        val videoContent: Content =
                            gson.fromJson(obj.toString(), Content::class.java)
                        id = videoContent.id
                        conceptId = videoContent.learning_map.conceptId
                        url = videoContent.url
                        title = videoContent.title
                        source = Utils.getVideoTypeUsingUrl(url)
                        duration = videoContent.duration.toString()
                        val formatId =
                            if (obj.has(FORMAT_ID)) obj.getString(FORMAT_ID) else ""
                        val learnPathName =
                            if (obj.has(LEARN_PATH_NAME)) obj.getString(LEARN_PATH_NAME) else ""
                        val learnPathFormatName =
                            if (obj.has(LEARN_PATH_FORMAT_NAME)) obj.getString(
                                LEARN_PATH_FORMAT_NAME
                            ) else ""
                        uiThread {
                            hideProgress()
                            callVideo(
                                id,
                                AppConstants.VIDEO,
                                conceptId,
                                url,
                                title,
                                "",
                                source.toLowerCase(),
                                "",
                                "",
                                "",
                                duration,
                                "",
                                learnPathName,
                                learnPathFormatName,
                                formatId, videoContent
                            )
                        }
                    }

                }

                AppConstants.COOBO -> {
                    if (navType == NAV_TYPE_SYLLABUS) {
                        val cooboContent = Gson().fromJson(obj.toString(), Content::class.java)
                        callCoobo(cooboContent.url, cooboContent.id, cooboContent.duration)
                    } else {
                        var content = obj.getJSONObject("content")
                        val url = content.getString("coobo_uuid")
                        var id = ""
                        var length = 0
                        if (obj.has("id"))
                            id = obj.getString("id")
                        if (content.has("slide_count"))
                            length = content.getInt("slide_count")
                        callCoobo(url, id, length)
                    }
                }
            }

        } catch (ex: JSONException) {
        }
    }

    override fun getEvent(data: String) {
        try {
            val obj = JSONObject(data)
            val type = obj.getString(AppConstants.DETAILS_TYPE)
            when (type.toLowerCase()) {
                AppConstants.EVENT -> {
                    SegmentUtils.trackBookDetailsEvent(obj.getJSONObject(AppConstants.DATA))
                }
            }

        } catch (ex: JSONException) {
        }
    }

    override fun sendToFeedback(testCode: String) {
        TODO("Not yet implemented")
    }

    override fun showLoader() {

    }

    override fun closeLoader() {

    }

    override fun hideKeyboard() {
        closeKeyBoard(binding.root)
    }

    override fun focusUp() {
        TODO("Not yet implemented")
    }

    override fun focusDown() {
        TODO("Not yet implemented")
    }

    override fun focusRight() {
        TODO("Not yet implemented")
    }

    override fun focusLeft() {
        TODO("Not yet implemented")
    }

    override fun initVoiceInput(value: String) {
        TODO("Not yet implemented")
    }

    override fun getSelectedQuestion(qListJson: String) {
        TODO("Not yet implemented")
    }

    override fun setQuestion(singQuestionJson: String) {
        TODO("Not yet implemented")
    }

    override fun getSelectedVideo(videoData: String) {

    }

    override fun selectAnswer(value: String?) {

    }

    override fun deselectAnswer(value: String?) {

    }

    override fun inputAnswer(value: String?) {

    }

    override fun close() {
        webView.destroy()
        finish()
    }

    override val questionNumber: Int
        get() = 0
    override val sectionName: String?
        get() = ""
    override val chapterJson: String?
        get() = ""
    override val chapterName: String?
        get() = ""
    override val singQuestionJson: String?
        get() = TODO("Not yet implemented")
    override val questionListJson: String?
        get() = TODO("Not yet implemented")

    override fun showConcept(): Boolean? {
        return true
    }

    override fun screenUpdate(type: String) {
        when (type) {
            END_SESSION -> {
                close()
            }
        }
    }

    /* override fun onBackPressed() {
         Utils.showAlertDialog(
             PRACTICE_SESSION_ALERT_DIALOG, supportFragmentManager,
             this, null,
             resources.getString(R.string.are_you_sure_you_want_to_end_your_book_session))
     }*/

    override fun movePAJNextActivity(data: String) {

    }

}