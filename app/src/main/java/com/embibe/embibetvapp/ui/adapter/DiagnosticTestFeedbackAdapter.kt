package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.text.SpannableString
import android.text.Spanned
import android.text.style.RelativeSizeSpan
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ItemStepDiagnosticFeedbackBinding
import com.embibe.embibetvapp.model.achieve.achieveFeedback.Activity
import com.embibe.embibetvapp.model.achieve.achieveFeedback.Step
import kotlin.math.roundToInt

class DiagnosticTestFeedbackAdapter :
    RecyclerView.Adapter<DiagnosticTestFeedbackAdapter.TestsViewHolder>() {

    var list = ArrayList<Step>()
    var onItemClick: ((Step) -> Unit)? = null
    private lateinit var subMenuAdapter: DiagnosticTestFeedbackSubAdapter
    var selectedPosition = -1
    var onItemLeftClick: ((Boolean) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestsViewHolder {
        val binding: ItemStepDiagnosticFeedbackBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_step__diagnostic_feedback,
            parent, false
        )
        return TestsViewHolder(binding)
    }

    override fun getItemCount() = if (list.size > 0) list.size else 0

    override fun onBindViewHolder(holder: TestsViewHolder, position: Int) {
        holder.bind(list[position])

        if (position == selectedPosition) {
            if (list[position].activities!!.isNotEmpty()) {
                holder.binding.imgExpand.setImageDrawable(holder.itemView.resources.getDrawable(R.drawable.ic_expand_less_24px))
                holder.binding.recyclerSubItem.visibility = View.VISIBLE
            }
        }

//        if (position != 3) {
        when (list[position].status) {
            AppConstants.DIAGNOSTIC_TEST_STATUS_OPEN -> {
                if (list[position].progress != 100) {
                    updateUIForOpenStep(holder.binding, list[position], holder.itemView.context)
                } else {
                    updateUIForCompletedStep(
                        holder.binding, list[position], holder.itemView.context
                    )
                }
            }
            AppConstants.DIAGNOSTIC_TEST_STATUS_LOCKED -> {
                updateUIForLockedStep(holder.binding, list[position])
            }
        }
//        } else {
//            when (list[position].status) {
//                AppConstants.DIAGNOSTIC_TEST_STATUS_OPEN -> {
//                    updateStartTestUIForOpenState(holder.binding, list[position])
//                }
//                AppConstants.DIAGNOSTIC_TEST_STATUS_LOCKED -> {
//                    updateStartTestUIForLockedState(holder.binding, list[position])
//                }
//            }
//        }
    }

    private fun updateUIForOpenStep(
        binding: ItemStepDiagnosticFeedbackBinding, step: Step, context: Context
    ) {
        setAchieveBanner(binding, step, context)
        setTitle(binding, step)
        setDescription(binding, step)
        setPredictedGrade(binding, step)
        hideCurrentPotentialConstraint(binding)
        hideStartTestUI(binding)
        hideOverlay(binding)
        updateResumeProgress(binding, step)
    }

    private fun updateUIForCompletedStep(
        binding: ItemStepDiagnosticFeedbackBinding, step: Step, context: Context
    ) {
        setAchieveBanner(binding, step, context)
        setTitle(binding, step)
        setDescriptionCompleted(binding, step)
        hidePredictedGrade(binding)
        setCurrentPotentialConstraint(binding, step)
        hideStartTestUI(binding)
        hideOverlay(binding)
        hideSeekBarProgress(binding)
    }

    private fun updateUIForLockedStep(binding: ItemStepDiagnosticFeedbackBinding, step: Step) {
        showOverlay(binding)
        setAchieveBannerOverlay(binding)
        hideCurrentPotentialConstraint(binding)
        hideStartTestUI(binding)
        hideSeekBarProgress(binding)
    }

//    private fun updateStartTestUIForOpenState(
//        binding: ItemStepDiagnosticFeedbackBinding,
//        step: Step
//    ) {
//        hideTitleDescriptionPredictedGrade(binding)
//        hideCurrentPotentialConstraint(binding)
//        setStartTestBanner(binding)
//        showStartTestUI(binding)
//        hideSeekBarProgress(binding)
//    }
//
//    private fun updateStartTestUIForLockedState(
//        binding: ItemStepDiagnosticFeedbackBinding,
//        step: Step
//    ) {
//        showOverlay(binding)
//        setStartTestOverlay(binding)
//        hideCurrentPotentialConstraint(binding)
//        hideStartTestUI(binding)
//        hideSeekBarProgress(binding)
//    }

    private fun setAchieveBanner(
        binding: ItemStepDiagnosticFeedbackBinding, step: Step, context: Context
    ) {
//        binding.ivBg.setImageResource(R.drawable.achieve_bg)
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)

        Glide.with(context).setDefaultRequestOptions(requestOptions)
            .load(step.thumbUrl)
            .transition(DrawableTransitionOptions.withCrossFade())
            .transform(RoundedCorners(16))
            .into(binding.ivBg)
    }

    private fun setTitle(binding: ItemStepDiagnosticFeedbackBinding, step: Step) {
        binding.stepNo.visibility = View.VISIBLE
        val title = processTitle(step.title)

        val string =
            if (step.duration!!.toInt() <= 1) SpannableString(title + " (" + step.duration + " Hour) ")
            else SpannableString(title + " (" + step.duration + " Hours) ")
        string.setSpan(RelativeSizeSpan(1.5f), 0, 6, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        binding.stepNo.text = string
    }

    private fun processTitle(title: String?): String {
        val splitArray = title!!.split("_")
        return if (splitArray != null && splitArray[0] != null && splitArray[1] != null) {
            splitArray[0].substring(0, 1).toUpperCase() + splitArray[0].substring(1)
                .toLowerCase() + " " + splitArray[1]
        } else title
    }

    private fun setDescription(binding: ItemStepDiagnosticFeedbackBinding, step: Step) {
        binding.stepDescription.visibility = View.VISIBLE
        binding.stepDescriptionCompleted.visibility = View.GONE
        binding.stepDescription.text =
            getStepDescription(
                step.questions!!,
                step.videos!!,
                step.behaviours!!,
                step.status!!,
                binding
            )
    }

    private fun setDescriptionCompleted(binding: ItemStepDiagnosticFeedbackBinding, step: Step) {
        binding.stepDescription.visibility = View.GONE
        binding.stepDescriptionCompleted.visibility = View.VISIBLE
        binding.stepDescriptionCompleted.text =
            "Completed | Effort Rating ${step.effortRating?.roundToInt().toString()}%"
    }

    private fun setPredictedGrade(binding: ItemStepDiagnosticFeedbackBinding, step: Step) {
        binding.ivGradeImprovement.visibility = View.VISIBLE
        binding.tvSphere.visibility = View.VISIBLE
        binding.textGradeImprovement.visibility = View.VISIBLE
        binding.tvSphere.text = step.predictedGrade
    }

    private fun hideCurrentPotentialConstraint(binding: ItemStepDiagnosticFeedbackBinding) {
        binding.constraintAchieve.visibility = View.GONE
    }

    private fun hideStartTestUI(binding: ItemStepDiagnosticFeedbackBinding) {
        binding.textCertifyAchievement.visibility = View.GONE
        binding.btnStartTest.visibility = View.GONE
    }

    private fun hideOverlay(binding: ItemStepDiagnosticFeedbackBinding) {
        binding.lockedOverlay.visibility = View.GONE
        binding.ivLock.visibility = View.GONE
    }

    private fun hidePredictedGrade(binding: ItemStepDiagnosticFeedbackBinding) {
        binding.ivGradeImprovement.visibility = View.INVISIBLE
        binding.tvSphere.visibility = View.INVISIBLE
        binding.textGradeImprovement.visibility = View.INVISIBLE
    }

    private fun setCurrentPotentialConstraint(
        binding: ItemStepDiagnosticFeedbackBinding,
        step: Step
    ) {
        binding.constraintAchieve.visibility = View.VISIBLE
        binding.tvSphere1.text = step.predictedGrade
        binding.tvSphere2.text = step.currentGrade
    }

    private fun showOverlay(binding: ItemStepDiagnosticFeedbackBinding) {
        binding.lockedOverlay.visibility = View.VISIBLE
        binding.ivLock.visibility = View.VISIBLE
    }

    private fun setAchieveBannerOverlay(binding: ItemStepDiagnosticFeedbackBinding) {
        binding.lockedOverlay.setImageResource(R.drawable.three_step_overlay)
    }

//    private fun hideTitleDescriptionPredictedGrade(binding: ItemStepDiagnosticFeedbackBinding) {
//        binding.stepNo.visibility = View.GONE
//        binding.stepDescription.visibility = View.GONE
//        binding.stepDescriptionCompleted.visibility = View.GONE
//        binding.imgExpand.visibility = View.GONE
//        hidePredictedGrade(binding)
//    }
//
//    private fun showStartTestUI(binding: ItemStepDiagnosticFeedbackBinding) {
//        binding.textCertifyAchievement.visibility = View.VISIBLE
//        binding.btnStartTest.visibility = View.VISIBLE
//    }
//
//    private fun setStartTestBanner(binding: ItemStepDiagnosticFeedbackBinding) {
//        binding.ivBg.setImageResource(R.drawable.start_test_bg_for_achieve)
//    }
//
//    private fun setStartTestOverlay(binding: ItemStepDiagnosticFeedbackBinding) {
//        binding.lockedOverlay.setImageResource(R.drawable.start_test_overlay)
//    }

    private fun updateResumeProgress(binding: ItemStepDiagnosticFeedbackBinding, step: Step) {
        if (step.progress!! > 0) {
            binding.btnResume.visibility = View.VISIBLE
            binding.seekBar.visibility = View.VISIBLE
            binding.seekBarOverlay.visibility = View.VISIBLE
            binding.seekBar.progress = step.progress

            val requestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)

//            Glide.with().setDefaultRequestOptions(requestOptions)
//                .transform(RoundedCorners(16))
//                .into(binding.seekBarOverlay)
        } else {
            binding.btnResume.visibility = View.GONE
            binding.seekBar.visibility = View.GONE
            binding.seekBarOverlay.visibility = View.GONE
        }
    }

    private fun hideSeekBarProgress(binding: ItemStepDiagnosticFeedbackBinding) {
        binding.btnResume.visibility = View.GONE
        binding.seekBar.visibility = View.GONE
        binding.seekBarOverlay.visibility = View.GONE
    }

    fun setData(list: ArrayList<Step>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class TestsViewHolder(var binding: ItemStepDiagnosticFeedbackBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Step) {

            binding.recyclerSubItem.visibility = View.GONE

            if (list[adapterPosition].activities!!.isNotEmpty()) {
//                binding.imgAdd.visibility = View.VISIBLE
////                binding.imgAdd.setImageDrawable(itemView.resources.getDrawable(R.drawable.ic_add_white_24dp))
                setRecycler(binding, itemView, item, adapterPosition)
            }
        }

        init {
            binding.imgLayout.setOnKeyListener { v, keyCode, event ->

                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {

                            if (list[adapterPosition].status == AppConstants.DIAGNOSTIC_TEST_STATUS_OPEN) {
                                if (selectedPosition != adapterPosition) {
                                    val previousPosition = selectedPosition
                                    notifyItemChanged(previousPosition)

                                    selectedPosition = adapterPosition
                                    notifyItemChanged(adapterPosition)
                                } else {
                                    // make recycler visible and previous item back to original position
                                    toggleRecyclerVisibility(binding, itemView, adapterPosition)
                                }
                            }
                        }

                        KeyEvent.KEYCODE_DPAD_LEFT -> {
                            onItemLeftClick?.invoke(true)
                        }
                    }
                }

                false
            }

        }
    }

    private fun getStepDescription(
        questions: String,
        videos: String,
        behaviour: String,
        status: String,
        binding: ItemStepDiagnosticFeedbackBinding
    ): String {

        when (status) {
            AppConstants.DIAGNOSTIC_TEST_STATUS_OPEN -> {
                var questionString = ""
                var videoString = ""
                var behaviourString = ""

                if (questions != "" && questions != "0") {
                    questionString =
                        if (questions == "1") "$questions Question" else "$questions Questions"
                }

                if (videos != "" && videos != "0") {
                    videoString =
                        if (videos == "1") "$videos Video" else "$videos Videos"
                }

                if (behaviour != "" && behaviour != "0") {
                    behaviourString =
                        if (behaviour == "1") "$behaviour Behaviour" else "$behaviour Behaviours"
                }

                return if (questionString != "" && videoString != "" && behaviourString != "") {
                    "$questionString, $videoString, $behaviourString"
                } else if (questionString != "" && videoString != "" && behaviourString == "") {
                    "$questionString, $videoString"
                } else if (questionString != "" && videoString == "" && behaviourString != "") {
                    "$questionString, $behaviourString"
                } else if (questionString == "" && videoString != "" && behaviourString != "") {
                    "$videoString, $behaviourString"
                } else if (questionString != "" && videoString == "" && behaviourString == "") {
                    "$questionString"
                } else if (questionString == "" && videoString != "" && behaviourString == "") {
                    "$videoString"
                } else if (questionString == "" && videoString == "" && behaviourString != "") {
                    "$behaviourString"
                } else ""
            }
            else -> return ""
        }
    }

    private fun toggleRecyclerVisibility(
        binding: ItemStepDiagnosticFeedbackBinding,
        itemView: View,
        adapterPosition: Int
    ) {
        if (list[adapterPosition].activities!!.isNotEmpty()) {
            if (binding.recyclerSubItem.visibility == View.GONE) {
                binding.recyclerSubItem.visibility = View.VISIBLE
                binding.imgExpand.setImageDrawable(itemView.resources.getDrawable(R.drawable.ic_expand_less_24px))
            } else if (binding.recyclerSubItem.visibility == View.VISIBLE) {
                binding.recyclerSubItem.visibility = View.GONE
                binding.imgExpand.setImageDrawable(itemView.resources.getDrawable(R.drawable.ic_expand_more_24px))
            }
        }
    }

    private fun setRecycler(
        binding: ItemStepDiagnosticFeedbackBinding,
        itemView: View,
        item: Step,
        pos: Int
    ) {
        subMenuAdapter = DiagnosticTestFeedbackSubAdapter()
        val layoutManager =
            LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerSubItem.adapter = subMenuAdapter
        binding.recyclerSubItem.layoutManager = layoutManager
        subMenuAdapter.setData(item, item.activities as ArrayList<Activity>, pos)
    }
}