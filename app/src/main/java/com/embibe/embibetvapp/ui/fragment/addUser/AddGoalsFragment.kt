package com.embibe.embibetvapp.ui.fragment.addUser

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.embibe.app.utils.Resource
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentAddGoalsBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.adduser.AddUserRequest
import com.embibe.embibetvapp.model.adduser.AddUserResponse
import com.embibe.embibetvapp.model.adduser.Exam
import com.embibe.embibetvapp.model.adduser.Goal
import com.embibe.embibetvapp.model.auth.login.LoginResponse
import com.embibe.embibetvapp.model.goals.GoalsExamsRes
import com.embibe.embibetvapp.ui.activity.UserSwitchActivity
import com.embibe.embibetvapp.ui.adapter.GoalAdapter
import com.embibe.embibetvapp.ui.adapter.PrimaryGoalExamAdapter
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.ContantUtils
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Status
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import org.jetbrains.anko.doAsync

class AddGoalsFragment : BaseAppFragment() {

    private lateinit var mCurrentView: View
    private lateinit var binding: FragmentAddGoalsBinding
    private lateinit var signInViewModel: SignInViewModel
    private lateinit var homeViewModel: HomeViewModel

    private lateinit var goalAdapter: GoalAdapter
    private lateinit var goalExamAdapter: PrimaryGoalExamAdapter

    private lateinit var primaryGoalCode: String
    private lateinit var primaryExamCode: String

    private var avatarUrl: String = ContantUtils.mImgIds[0]

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_goals, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCurrentView = view
        getGoalsExams()

    }

    fun getGoalsExams() {
        showProgress()
        signInViewModel.getGoalsExams(object : BaseViewModel.APICallBacks<GoalsExamsRes> {
            override fun onSuccess(model: GoalsExamsRes?) {
                hideProgress()
                if (model != null) {
                    DataManager.instance.setGoalsExamsList(
                        ((model.data ?: arrayListOf()))
                    )
                    updateResult(mCurrentView)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getGoalsExams()
                        }

                        override fun onDismiss() {

                        }
                    })

                } else {
                    showToast(error)
                }

            }
        })
    }

    private fun updateResult(view: View) {
        setGoalRecyclerView(view)
        setExamRecyclerView(view)
        registerDoneBtn()
        registerAddUserObserver()
    }

    @SuppressLint("SetTextI18n")
    private fun setGoalRecyclerView(view: View) {

        goalAdapter = GoalAdapter(view.context)

        makeGridCenter(binding.rvPrimaryGoal)

        binding.rvPrimaryGoal.adapter = goalAdapter
        goalAdapter.setData(DataManager.instance.getPrimaryGoals())
        setDefaultFocusToSupportedGoal(DataManager.instance.getPrimaryGoals())
        goalAdapter.onItemClick = { goal ->
            if (goal.supported) {
                primaryGoalCode = goal.code
                goalExamAdapter.setData(goal.exams)
                setDefaultFocusToSupportedExam(goal.exams)
            }
            if (!goal.supported) {
                binding.textGoalNotSupported.visibility = View.VISIBLE
                binding.textGoalNotSupported.text =
                    "${resources.getString(R.string.text_goal_not_supported)} ${goal.name}"
            } else {
                binding.textGoalNotSupported.visibility = View.INVISIBLE
            }
        }

        goalAdapter.onFocusChange = { goal, hasFocus ->
            if (!hasFocus) {
                binding.textGoalNotSupported.visibility = View.INVISIBLE
            }
        }
    }


    private fun setDefaultFocusToSupportedGoal(primaryGoals: List<Goal>) {
        for (index in primaryGoals.indices) {
            if (primaryGoals[index].default) {
                goalAdapter.setDefaultSelectedItem(index)
                primaryGoalCode = primaryGoals[index].code
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setExamRecyclerView(view: View) {

        goalExamAdapter = PrimaryGoalExamAdapter(view.context)

        makeGridCenter(binding.rvPrimaryExams)

        binding.rvPrimaryExams.adapter = goalExamAdapter
        goalExamAdapter.setData(DataManager.instance.getPrimaryGoals()[0].exams)
        setDefaultFocusToSupportedExam(DataManager.instance.getPrimaryGoals()[0].exams)

        goalExamAdapter.onItemClick = { exam ->
            primaryExamCode = exam.code
            if (!exam.supported) {
                binding.textExamNotSupported.visibility = View.VISIBLE
                binding.textExamNotSupported.text =
                    "${resources.getString(R.string.text_exam_not_supported)} ${exam.name}"
            } else {
                binding.textExamNotSupported.visibility = View.GONE
            }
        }

        goalExamAdapter.onFocusChange = { exam, hasFocus ->
            if (!hasFocus) {
                binding.textExamNotSupported.visibility = View.GONE
            }
        }
    }

    private fun setDefaultFocusToSupportedExam(exams: ArrayList<Exam>) {
        for (index in 0 until exams.size) {
            if (exams[index].default) {
                goalExamAdapter.setDefaultSelectedItem(index)
                primaryExamCode = exams[index].code
            }
        }
    }

    private fun registerDoneBtn() {
        binding.buttonDone.setOnClickListener {
            // board corresponds to Goal and kClass corresponds to exam
            SegmentUtils.trackSigninGoalSelectionDoneClick(
                AppConstants.TEMP_BOARD,
                AppConstants.TEMP_K_CLASS
            )
            if (goalAdapter.selectedItem == -1) {
                showToast(resources.getString(R.string.please_select_goal))
                return@setOnClickListener
            } else if (goalExamAdapter.selectedItem == -1) {
                showToast(resources.getString(R.string.please_select_exam))
                return@setOnClickListener
            } else {
                doAddUser()
            }
        }
    }

    private fun doAddUser() {
        val parentLinkedProfile = UserData.getLinkedUser()[0]!!
        var childEmailID: String = parentLinkedProfile.email!!
        childEmailID = if (childEmailID.isBlank()) {
            "Student1@embibe.com"
        } else {
            parentLinkedProfile.email!!
        }
        showProgress()
        signInViewModel.addUser(
            parentLinkedProfile.userId,
            AddUserRequest(
                AppConstants.USER_TYPE_CHILD, "Student 1", "c1$childEmailID",
                childEmailID.substring(childEmailID.indexOf("@") + 1),
                "sp", primaryGoalCode, primaryExamCode,
                AppConstants.TEMP_K_CLASS, AppConstants.TEMP_BOARD,
                "male", avatarUrl, ""
            )
        )
    }

    private fun registerAddUserObserver() {
        signInViewModel.addUserLiveData.observe(viewLifecycleOwner, Observer { resource ->
            if (resource != null) {
                when (resource.status) {
                    Status.LOADING -> showProgress()
                    Status.SUCCESS -> {
                        val response = resource.data as AddUserResponse
                        if (response.success
                            && response.message.equals("user added", true)
                            || response.message.equals("user updated", true)
                        ) {
                            getConnectedProfile()
                        } else {
                            Utils.showToast(requireContext(), "Unable to add user")
                        }
                        hideProgress()
                    }
                    Status.ERROR -> {
                        hideProgress()
                        retryAddUser(resource)
                    }
                }
            }
        })
    }

    private fun retryAddUser(resource: Resource<AddUserResponse>) {
        if (Utils.isApiFailed(resource)) {
            Utils.showError(context, resource, object : BaseViewModel.ErrorCallBacks {
                override fun onRetry(msg: String) {
                    doAddUser()
                }

                override fun onDismiss() {
                }
            })
        }
    }

    private fun getConnectedProfile() {
        getLinkedProfiles()
    }

    private fun getLinkedProfiles() {
        showProgress()
        signInViewModel.getLinkedProfilesApi(object : BaseViewModel.APICallBacks<LoginResponse> {
            override fun onSuccess(model: LoginResponse?) {
                if (model != null && model.success) {
                    makeLog("Authentication Success")
                    updateSignInResponse(model)
                } else {
                    makeLog("getLinkedProfilesApi api failed")
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                makeLog("getLinkedProfilesApi api error $error")
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getLinkedProfiles()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }

        })

    }

    private fun updateSignInResponse(response: LoginResponse) {
        doAsync {
            val profileList = response.linkedProfiles
            UserData.updateLinkedUser(response)
            if (profileList != null && profileList.isNotEmpty()) {
                gotoUserSwitchActivity()
            }
        }
    }

    private fun gotoUserSwitchActivity() {
        val intent = Intent(context, UserSwitchActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        requireActivity().finish()
    }


}