package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.graphics.drawable.Animatable
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.*
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo

class DetailAdapter(private val mContext: Context) :
    RecyclerView.Adapter<DetailAdapter.ItemViewHolder<*>>() {
    var list: ArrayList<Content> = ArrayList()
    val TAG = this.javaClass.name
    var onItemClick: ((Content) -> Unit)? = null
    var onItemFocused: ((Int) -> Unit?)? = null
    var onItemDpadHit: ((Int) -> Unit)? = null

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_CARD = 1
        private const val TYPE_BOOK = 2
        private const val TYPE_PRACTICE = 3
        private const val TYPE_GIF = 4
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder<Content> {
        return when (viewType) {
            TYPE_HEADER -> {
                val binding: ItemDetailHeaderBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_detail_header,
                    parent,
                    false
                )
                HeaderViewHolder(binding)
            }
            TYPE_CARD -> {
                val binding: ItemDetailCardBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_detail_card,
                    parent,
                    false
                )
                CardViewHolder(binding)
            }
            TYPE_GIF -> {
                val binding: ItemDetailCardGifBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_detail_card_gif,
                    parent,
                    false
                )
                GifCardViewHolder(binding)
            }
            TYPE_BOOK -> {
                val binding: ItemDetailBookBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_detail_book,
                    parent,
                    false
                )
                BookViewHolder(binding)
            }
            TYPE_PRACTICE -> {
                val binding: ItemDetailPracticeBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(mContext),
                    R.layout.item_detail_practice,
                    parent,
                    false
                )
                PracticeViewHolder(binding)
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder<*>, position: Int) {
        val item = list[position]
        when (holder) {
            is HeaderViewHolder -> holder.bind(item)
            is CardViewHolder -> holder.bind(item)
            is BookViewHolder -> holder.bind(item)
            is PracticeViewHolder -> holder.bind(item)
            is GifCardViewHolder -> holder.bind(item)
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemViewType(position: Int): Int {
        var value: Int = 0
        var item: Content = list[position]
        Log.e(TAG, item.type)
        value = when (item.type.toLowerCase()) {
            AppConstants.TYPE_HEADER -> TYPE_HEADER
            AppConstants.TYPE_CARD, AppConstants.TYPE_COOBO -> TYPE_CARD
            AppConstants.TYPE_VIDEO -> {
                return if (item.preview_url.isNullOrEmpty()) {
                    TYPE_CARD
                } else {
                    TYPE_GIF
                }
            }
            AppConstants.TYPE_BOOK -> TYPE_BOOK
            AppConstants.TYPE_PRACTICE, AppConstants.TYPE_CHAPTER -> TYPE_PRACTICE
            else -> TYPE_PRACTICE
        }
        return value
    }

    fun setData(data: ArrayList<Content>) {
        list = data
        Log.e(TAG, "${list.size}")
        notifyDataSetChanged()
    }

    abstract class ItemViewHolder<Content>(binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        abstract fun bind(item: Content)
    }

    inner class HeaderViewHolder(var binding: ItemDetailHeaderBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {

            if (list.isNotEmpty()) {
                binding.contentModel = item
                binding.tvHeader.visibility = View.VISIBLE
            } else
                binding.tvHeader.visibility = View.GONE
        }

        init {
            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(adapterPosition)
            }
        }
    }

    inner class CardViewHolder(var binding: ItemDetailCardBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
            Glide.with(mContext).load(item.owner_info.copy_logo).into(binding.ivEmbibeLogo)

            if (item.type != "header") {
                if (item.category_thumb.isNotEmpty()) {
                    binding.ivCategory.visibility = View.VISIBLE
                    Glide.with(itemView.context)
                        .load(item.category_thumb)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(binding.ivCategory)
                } else {
                    binding.ivCategory.visibility = View.GONE
                }

                binding.textTime.text = Utils.convertIntoMins(item.length)
            }

            binding.tvSubjectName.visibility = View.GONE

            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            if (item.thumb == null || item.thumb == "thumb" || item.thumb == "") {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            } else {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(item.thumb)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            }
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
                var item: Content? = binding.detailItem
                if (item != null) {
                    SegmentUtils.trackEventMoreInfoMenuItemTileClick(item)
                }
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(adapterPosition)
                var item: Content? = binding.detailItem
                if (item != null) {
                    SegmentUtils.trackEventMoreInfoMenuItemTileFocus(item)
/*
                    SegmentUtils.trackEventSearchTileCaroselFocus()
*/
                }

            }

            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            onItemDpadHit?.invoke(adapterPosition)
                        }
                    }

                }

                false
            }
        }
    }

    inner class GifCardViewHolder(var binding: ItemDetailCardGifBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.layout.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    binding.ivGifView.visibility = View.VISIBLE
                    val controller = Fresco.newDraweeControllerBuilder()
                    controller.autoPlayAnimations = true
                    controller.setUri(item.preview_url)
                    controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                        override fun onFinalImageSet(
                            id: String?,
                            imageInfo: ImageInfo?,
                            animatable: Animatable?
                        ) {
                            val anim = animatable as AnimatedDrawable2
                            anim.setAnimationListener(object : AnimationListener {
                                override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                                override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                                    binding.ivImg.visibility = View.INVISIBLE
                                }

                                override fun onAnimationFrame(
                                    drawable: AnimatedDrawable2?,
                                    frameNumber: Int
                                ) {

                                }

                                override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                                    binding.ivImg.visibility = View.VISIBLE
                                    binding.ivGifView.visibility = View.INVISIBLE
                                }

                                override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                            })
                        }
                    }
                    binding.ivGifView.controller = controller.build()

                } else {
                    binding.ivGifView.controller?.animatable?.stop()
                    binding.ivImg.visibility = View.VISIBLE
                    binding.ivGifView.visibility = View.INVISIBLE
                }
            }
            binding.detailItem = item
            Glide.with(mContext).load(item.owner_info.copy_logo).into(binding.ivEmbibeLogo)
            if (item.type != "header") {
                if (item.category_thumb.isNotEmpty()) {
                    binding.ivCategory.visibility = View.VISIBLE
                    Glide.with(itemView.context)
                        .load(item.category_thumb)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(binding.ivCategory)
                } else {
                    binding.ivCategory.visibility = View.GONE
                }

                binding.textTime.text = Utils.convertIntoMins(item.length)
            }

            binding.tvSubjectName.visibility = View.GONE

            val requestOptions = RequestOptions().transform(RoundedCorners(10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            if (item.thumb == "thumb" || item.thumb == "") {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            } else {
                Glide.with(itemView.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(item.thumb)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.ivImg)
            }
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
                var item: Content? = binding.detailItem
                if (item != null) {
                    SegmentUtils.trackEventMoreInfoMenuItemTileClick(item)
                }
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(adapterPosition)
                var item: Content? = binding.detailItem
                if (item != null) {
                    SegmentUtils.trackEventMoreInfoMenuItemTileFocus(item)
/*
                    SegmentUtils.trackEventSearchTileCaroselFocus()
*/
                }

            }

            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            onItemDpadHit?.invoke(adapterPosition)
                        }
                    }

                }

                false
            }
        }
    }

    inner class BookViewHolder(var binding: ItemDetailBookBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(adapterPosition)
            }
        }
    }

    inner class PracticeViewHolder(var binding: ItemDetailPracticeBinding) :
        ItemViewHolder<Content>(binding) {
        override fun bind(item: Content) {
            binding.detailItem = item
        }

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    onItemFocused?.invoke(adapterPosition)
            }
        }
    }
}