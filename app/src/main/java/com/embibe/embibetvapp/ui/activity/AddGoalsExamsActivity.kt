package com.embibe.embibetvapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.ArraySet
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.model.adduser.Goal
import com.embibe.embibetvapp.model.goals.GoalsExamsRes
import com.embibe.embibetvapp.ui.fragment.editUser.PrimaryGoalsFragment
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.showToast
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper

class AddGoalsExamsActivity : BaseFragmentActivity() {

    private val primaryGoalsFragment = PrimaryGoalsFragment()

    private lateinit var signInViewModel: SignInViewModel

    companion object {
        fun getAddGoalsExamIntent(context: Context, userId: String, embibeToken: String?): Intent {
            return Intent(
                context,
                AddGoalsExamsActivity::class.java
            ).putExtra(AppConstants.TEMP_EMBIBE_TOKEN, embibeToken)
                .putExtra(AppConstants.USER_ID, userId)
        }

        var primaryGoal: String = ""
        var primaryGoalCode: String = ""
        var primaryGoalExamCode: String = ""
        var secondaryGoalCode: String = ""
        var secondaryGoal: String = ""
        var secondaryGoalExams = ArraySet<String>()
        var userId: String = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_goals_exams)
        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)

        val currentToken: String = PreferenceHelper()[AppConstants.EMBIBETOKEN, ""].toString()
        PreferenceHelper().put(AppConstants.TEMP_EMBIBE_TOKEN, currentToken)
        val token = intent.getStringExtra(AppConstants.TEMP_EMBIBE_TOKEN)
        userId = intent.getStringExtra(AppConstants.USER_ID)!!
        PreferenceHelper().put(AppConstants.EMBIBETOKEN, token)
        getGoalsExams()
        SegmentUtils.trackProfilePrimarySecLoadEnd()
    }


    fun getGoalsExams() {
        showProgress()
        signInViewModel.getGoalsExams(object : BaseViewModel.APICallBacks<GoalsExamsRes> {
            override fun onSuccess(model: GoalsExamsRes?) {
                hideProgress()
                if (model != null) {
                    DataManager.instance.setGoalsExamsList(
                        ((model.data ?: arrayListOf()) as ArrayList<Goal>)
                    )
                    setPrimaryGoal(primaryGoalsFragment)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(
                        this@AddGoalsExamsActivity,
                        code,
                        object : BaseViewModel.ErrorCallBacks {
                            override fun onRetry(msg: String) {
                                getGoalsExams()
                            }

                            override fun onDismiss() {

                            }
                        })

                } else {
                    showToast(this@AddGoalsExamsActivity, error)
                }

            }
        })
    }

    fun setSecondaryGoalExam(fragment: Fragment, name: String) {
        setBackStackFragment(fragment, name)
        SegmentUtils.trackAddUserScreenLoadEnd()
    }

    fun setPrimaryGoal(fragment: Fragment) {
        setNonBackStackFragment(fragment)
    }

    fun setSecondaryGoal(fragment: Fragment) {
        while (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
        }
        setNonBackStackFragment(fragment)
    }

    fun setPrimaryGoalExam(fragment: Fragment, tag: String) {
        setBackStackFragment(fragment, tag)
    }

    private fun setNonBackStackFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.fl_main_layout, fragment)
            .commit()
    }

    private fun setBackStackFragment(fragment: Fragment, name: String) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_main_layout, fragment).addToBackStack(name).commit()
    }

}