/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.embibe.embibetvapp.ui.presenter

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.leanback.widget.BaseCardView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.newmodel.Content
import java.util.*

class ContinueLearningPresenter(context: Context) :
    AbstractCardPresenter<BaseCardView>(context) {
    override fun onCreateView(): BaseCardView {
        val cardView = BaseCardView(context, null, R.style.SideInfoCardStyle)
        cardView.addView(
            LayoutInflater.from(context).inflate(
                R.layout.card_continue_learning,
                null
            )
        )
        cardView.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            val viewAlpha = cardView.findViewById<View>(R.id.view_focus)
            if (hasFocus) {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_focused_alpha_value
                )
            } else {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_unfocused_alpha_value
                )
            }
        }
        cardView.isFocusable = true

        return cardView
    }

    override fun onBindViewHolder(item: Any, cardView: BaseCardView) {
        val intArray = intArrayOf(25, 60, 85)

        val idx: Int = Random().nextInt(intArray.size)
        val card = item as Content
        val imgThumbnailView: ImageView = cardView.findViewById(R.id.iv_thumbnail)
        val embibeLogo: ImageView = cardView.findViewById(R.id.iv_embibe_logo)
        val tvVideoName: TextView = cardView.findViewById(R.id.tvVideoName)
        val progressBar: ProgressBar = cardView.findViewById(R.id.progress_bar)
        val ivCategory: ImageView = cardView.findViewById(R.id.ivCategory)

        progressBar.progress = intArray[idx]
        tvVideoName.text = card.subject
        Glide.with(context).load(card.owner_info.copy_logo).into(embibeLogo)
        (tvVideoName.background as GradientDrawable).setColor(
            context.resources.getColor(getColorCode(card.subject))
        )
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(context).setDefaultRequestOptions(requestOptions)
            .load(card.thumb)
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.video_placeholder)
            .transform(RoundedCorners(8))
            .into(imgThumbnailView)
    }

    private fun getColorCode(subject: String?): Int {
        return when (subject?.toLowerCase(Locale.ENGLISH)) {
            "maths", "mathematics" -> return R.color.sub_maths
            "chemistry" -> return R.color.sub_chemistry
            "biology" -> return R.color.sub_biology
            "physics" -> return R.color.sub_physics
            else -> R.color.sub_all
        }
    }

}