package com.embibe.embibetvapp.ui.viewmodel

import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.model.createtest.examconfig.ChapterDetails
import com.embibe.embibetvapp.model.createtest.examconfig.SubjectDetails
import com.embibe.embibetvapp.model.createtest.generateowntest.response.GenerateOwnTestResponse
import com.embibe.embibetvapp.network.repo.CreateTestRepository
import java.util.*


class SelectTestSettingsViewModel
    : BaseViewModel() {
    var repo = CreateTestRepository()


    fun generateTest(
        testName: String,
        createdAt: String,
        subjectDetails: ArrayList<SubjectDetails>?,
        chapterDetails: ArrayList<ChapterDetails>?,
        difficultyLevel: String,
        duration: Int, correctMark: String,
        incorrectMark: String,

        param: APICallBacks<GenerateOwnTestResponse>
    ) {

        var requestParamsGeneratingTest =
            repo.generateTestRepositoryRequest(
                subjectDetails,
                chapterDetails,
                testName,
                createdAt,
                difficultyLevel,
                duration, correctMark, incorrectMark
            )
        safeApi(repo.generateTest(requestParamsGeneratingTest), param)
    }


}