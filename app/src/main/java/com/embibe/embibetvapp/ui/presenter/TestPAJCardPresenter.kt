package com.embibe.embibetvapp.ui.presenter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import androidx.leanback.widget.BaseCardView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.model.achieve.home.Content

class TestPAJCardPresenter(context: Context) :
    AbstractCardPresenter<BaseCardView>(context) {
    override fun onCreateView(): BaseCardView {
        val cardView =
            BaseCardView(context, null, R.style.SideInfoCardStyle)
        cardView.isFocusable = true
        cardView.addView(
            LayoutInflater.from(context).inflate(
                R.layout.card_test_achievment,
                null
            )
        )
        return cardView
    }

    @SuppressLint("StringFormatMatches")
    override fun onBindViewHolder(item: Any, cardView: BaseCardView) {
        val card = item as Content

        val imgThumbnailView: ImageView = cardView.findViewById(R.id.iv_thumbnail)
        val tvTitle: TextView = cardView.findViewById(R.id.tvTitle)
        tvTitle.text = card.title
        val requestOptions = RequestOptions().transform(RoundedCorners(8))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(context).applyDefaultRequestOptions(requestOptions)
            .load(R.drawable.diagnostic_test_bg)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(imgThumbnailView)

    }

}