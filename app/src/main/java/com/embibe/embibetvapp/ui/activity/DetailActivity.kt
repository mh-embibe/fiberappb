package com.embibe.embibetvapp.ui.activity

import android.content.Intent
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.android.volley.RequestQueue
import com.android.volley.VolleyError
import com.android.volley.toolbox.Volley
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App.Companion.context
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.ANNOTATION_DETAILS
import com.embibe.embibetvapp.constant.AppConstants.ANNOTATION_MODEL
import com.embibe.embibetvapp.constant.AppConstants.CONTENT_TYPE
import com.embibe.embibetvapp.constant.AppConstants.CONTINUE_LEARNING
import com.embibe.embibetvapp.constant.AppConstants.FORMAT_ID
import com.embibe.embibetvapp.constant.AppConstants.IS_RECOMMENDATION_ENABLED
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_FORMAT_NAME
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_NAME
import com.embibe.embibetvapp.constant.AppConstants.RESUME
import com.embibe.embibetvapp.constant.AppConstants.START
import com.embibe.embibetvapp.constant.AppConstants.TOPIC_LEARN_PATH
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_AUTHORS
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CONCEPT_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_DESCRIPTION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SOURCE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SUBJECT
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TITLE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TOTAL_DURATION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_URL
import com.embibe.embibetvapp.databinding.ActivityDetailBinding
import com.embibe.embibetvapp.model.achieve.achieveFeedback.AchieveFeedbackRes
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkResponse
import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.ui.fragment.errorAlertScreen.ContinueVideoDialog
import com.embibe.embibetvapp.ui.fragment.learn.DetailFragment
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener
import com.embibe.embibetvapp.ui.viewmodel.DetailsViewModel
import com.embibe.embibetvapp.unity.UnityPlayerActivity
import com.embibe.embibetvapp.utils.ConnectionManager
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.VideoUtils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import kotlinx.android.synthetic.main.activity_detail.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class DetailActivity : BaseFragmentActivity(), ErrorScreensBtnClickListener {

    private lateinit var binding: ActivityDetailBinding
    private lateinit var detailsViewModel: DetailsViewModel

    private lateinit var listener: OnSharedPreferenceChangeListener
    private lateinit var volleyRequest: RequestQueue
    private lateinit var data: Content

    private lateinit var continueVideoDialog: ContinueVideoDialog
    private var isRefreshed: Boolean = false
    private var isAlreadyLoaded: Boolean = false
    private var isBookMarked: Boolean = false
    private var isLiked: Boolean = false
    private var bookmarkRetryCount: Int = 0
    private var likeRetryCount: Int = 0
    private var pref = PreferenceHelper()
    private var isVideoResumed: Boolean = false
    private var moreTopics: ArrayList<Results> = arrayListOf()
    private var relatedConcepts: ArrayList<Results> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        detailsViewModel = ViewModelProviders.of(this).get(DetailsViewModel::class.java)

        volleyRequest = Volley.newRequestQueue(this)
        hideButtons()
        listenForRelatedMoreTopics()
        getDetail()
        setInitialFocusToLearn()
        setClickListeners()
        setFocusListeners()
        setDialog()
    }

    private fun getAchieveFeedback(): AchieveFeedbackRes {
        return Utils.fromJson(
            Utils.readJSONFromAsset("achieve_feedback_response_latest", this) ?: ""
        )
    }

    private fun setDialog() {
        continueVideoDialog = ContinueVideoDialog()
        continueVideoDialog.setListener(object : ErrorScreensBtnClickListener {
            override fun screenUpdate(type: String) {
                when (type) {
                    RESUME -> {
                        isVideoResumed = true
                        moveToPlayer()
                    }
                    START -> {
                        isVideoResumed = false
                        moveToPlayer()
                    }
                }
            }

        })
    }

    override fun onResume() {
        super.onResume()
        if (::data.isInitialized) {
            getBookmarkStatus()
            getLikeStatus()
            SegmentUtils.trackEventScreenLoadEnd(
                isLiked,
                isBookMarked,
                content = data
            )
        }

    }

    private fun listenForRelatedMoreTopics() {

        listener = OnSharedPreferenceChangeListener { sharedPreferences, key ->
            if (key == AppConstants.RELATED_CONCEPTS + "_" + data.id) {
                updateUiForRelatedConcepts()
            } else if (key == AppConstants.MORE_TOPICS + "_" + data.id) {
                updateUiForMoreTopics()
            }
        }
        pref.preferences?.registerOnSharedPreferenceChangeListener(listener)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::listener.isInitialized) {
            pref.preferences?.unregisterOnSharedPreferenceChangeListener(listener)
        }
    }

    private fun showHideLine() {
        if (!binding.btnMoreTopics.isVisible && !binding.btnRelatedConcepts.isVisible) {
            binding.view.visibility = View.GONE
        } else {
            binding.view.visibility = View.VISIBLE
        }
    }

    private fun hideButtons() {
        binding.btnMoreTopics.visibility = View.GONE
        binding.btnRelatedConcepts.visibility = View.GONE
        binding.view.visibility = View.GONE
    }

    private fun getDetail() {
        data = getContent(intent)

        SegmentUtils.trackEventScreenLoadStart(isLiked, isBookMarked, content = data)
        binding.content = data
        setTitleAndDescription()
        if (data.bg_thumb.isEmpty())
            Utils.setBackgroundImage(
                Utils.getBackgroundUrl(data),
                binding.imgDescBackground,
                binding.imgDescBackgroundBlur
            )
        else
            Utils.setBackgroundImage(
                data.bg_thumb,
                binding.imgDescBackground,
                binding.imgDescBackgroundBlur
            )
    }

    private fun updateUiForRelatedConcepts() {
        relatedConcepts = DataManager.instance.getRelatedConcepts()
        if (relatedConcepts.isNotEmpty()) {
            binding.btnRelatedConcepts.visibility = View.VISIBLE
        } else {
            binding.btnRelatedConcepts.visibility = View.GONE
        }
        showHideLine()
    }

    private fun updateUiForMoreTopics() {
        moreTopics = DataManager.instance.getMoreTopics()

        if (moreTopics.isNotEmpty()) {
            binding.btnMoreTopics.visibility = View.VISIBLE
        } else {
            binding.btnMoreTopics.visibility = View.GONE
        }
        showHideLine()
    }

    private fun setTitleAndDescription() {
        if (data.title != "") {
            Log.d(TAG, data.title)
//            var updateStr = Utils.getTruncatedString(data.title, 25)
            binding.title.text = data.title

        } else binding.title.visibility = View.GONE

        if (data.subject != "") {
            binding.tvSubjectName.text = data.subject
        } else binding.tvSubjectName.visibility = View.GONE

        if (data.type != "chapter")
            binding.textTime.text = Utils.convertIntoMins(data.length)
        else
            binding.textTime.text = Utils.convertIntoConcepts(data.concept_count)

        binding.embiumsCount.text = getString(R.string.earn) + " ${data.currency}"

        if (data.description != "") {
            binding.textDescription.text = data.description
        } else binding.textDescription.visibility = View.GONE

        if (data.watched_duration != 0) {
            btnLearn.text = getString(R.string.continue_learning)
        }
    }

    private fun getBookmarkStatus() {
        detailsViewModel.getBookmarkStatus(data.id,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                val data = dataList.filter {
                                    it.contentId.equals(data.id)
                                }
                                updateBookMarkUI(data[0].status)
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("Bookmark Error : $code $error $msg ")

                }
            })
    }

    private fun getLikeStatus() {
        detailsViewModel.getLikeStatus(data.id,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                if (dataList.isNotEmpty()) {
                                    val data = dataList.filter {
                                        it.contentId.equals(data.id)
                                    }
                                    if (data.isNotEmpty())
                                        updateLikUI(data[0].status)
                                }
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("Like Error $code $error $msg")
                }
            })
    }

    private fun setInitialFocusToLearn() {
        btnLearn.requestFocus()
        btnLearn.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_play_circle_filled_black_24dp, 0, 0, 0
        )
    }

    private fun setClickListeners() {
        binding.btnLearn.setOnClickListener {

            if (data.watched_duration != 0)
                showContinueDialog()
            else
                moveToPlayer()

        }

        binding.btnMoreTopics.setOnClickListener {
            bgTransformation(isAlreadyLoaded)
            val bundle = Bundle()
            bundle.putString("type", AppConstants.MORE_TOPICS)
            bundle.putString("title", data.title)
            bundle.putParcelableArrayList(AppConstants.MORE_TOPICS, moreTopics)
            SegmentUtils.trackEventMoreInfoMainButtonClick(data, AppConstants.MORE_TOPICS)
            showDetails(bundle)
        }

        binding.btnRelatedConcepts.setOnClickListener {
            bgTransformation(isAlreadyLoaded)
            val bundle = Bundle()
            bundle.putString("type", AppConstants.RELATED_CONCEPTS)
            bundle.putString("title", data.title)
            bundle.putParcelableArrayList(AppConstants.RELATED_CONCEPTS, relatedConcepts)
            showDetails(bundle)
        }

        binding.btnBookmark.setOnClickListener {
            Utils.isLikeBookmarkUpdatedForQuickLinks = true
            isBookMarked = !isBookMarked
            updateBookMarkUI(isBookMarked)
            SegmentUtils.trackEventMoreInfoBookmarkClick(isBookMarked, content = data)
            timerBookmark.cancel()
            timerBookmark = Timer()
            timerBookmark.schedule(object : TimerTask() {
                override fun run() {
                    updateBookmark()
                }
            }, interval)
        }

        binding.btnLike.setOnClickListener {
            isLiked = !isLiked
            updateLikUI(isLiked)
            SegmentUtils.trackEventMoreInfoLikeClick(isLiked, content = data)
            /*like api call*/
            timerLike.cancel()
            timerLike = Timer()
            timerLike.schedule(object : TimerTask() {
                override fun run() {
                    updateLike()
                }
            }, interval)
        }
    }

    private fun showContinueDialog() {
        continueVideoDialog.show(supportFragmentManager, "continue_video")
    }


    private fun moveToPlayer() {
        if (!ConnectionManager.instance.hasNetworkAvailable()) {
            Utils.showError(this@DetailActivity, ApiConstants.API_CODE_NO_NETWORK,
                object : BaseViewModel.ErrorCallBacks {
                    override fun onRetry(msg: String) {
                        moveToPlayer()
                    }

                    override fun onDismiss() {
                        finish()
                    }
                })
            return
        }
        if (data.type.toLowerCase(Locale.getDefault()) == "Coobo".toLowerCase(Locale.getDefault())) {
            val i = Intent(context, UnityPlayerActivity::class.java)
            i.putExtra(VIDEO_CONCEPT_ID, data.learning_map.conceptId)
            i.putExtra(AppConstants.DURATION_LENGTH, data.length)
            i.putExtra(AppConstants.SLIDE_COUNT, data.watched_duration)
            i.putExtra(AppConstants.COOBO_URL, data.url)
            i.putExtra(AppConstants.DETAILS_ID, data.id)
            data.learning_map.let()
            {
                i.putExtra(AppConstants.LEARNMAP_CODE, data.learning_map.lpcode)
            }
            i.putExtra(IS_RECOMMENDATION_ENABLED, true)
            startActivity(i)

        } else {
            val callback = object : BaseViewModel.APICallBackVolley {
                override fun <T> onSuccessCallBack(response: T) {
                    hideProgress()
                    response as JSONObject
                    startVideoPlayer(
                        Utils.getVimeoHD((response["files"] as JSONArray)),
                        AppConstants.PLAYER_TYPE_EXOPLAYER
                    )
                    /*hide ProgressBar here */
                }

                override fun onErrorCallBack(e: VolleyError) {
                    try {
                        makeLog("Error:  $e")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    /*hide ProgressBar here */
                    hideProgress()
                }
            }

            val type = Utils.getVideoTypeUsingUrl(data.url)
            if (type == AppConstants.PLAYER_TYPE_EXOPLAYER) {
                showProgress()
                val videoId = VideoUtils.getVimeoVideoId(data.url).toString()
                Utils.getVideoURL(this, videoId, data.owner_info.key, callback)
            } else {
                if (Utils.isYouTubeAppAvailable(this)) {
                    Utils.startYoutubeApp(this, data.url)
                } else {
                    startVideoPlayer(data.url, AppConstants.PLAYER_TYPE_YOUTUBE)
                }
            }
        }

    }

    private fun showDetails(bundle: Bundle) {
        val fragDetail =
            DetailFragment()
        fragDetail.arguments = bundle
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainerView, fragDetail)
            .commit()
    }

    private fun bgTransformation(isAlreadyLoaded: Boolean) {
        if (!isAlreadyLoaded) {
            this.isAlreadyLoaded = true
            if (data.bg_thumb.isEmpty())
                Utils.setBlurBackgroundImage(
                    binding.imgDescBackground
                ) else
                Utils.setBlurBackgroundImage(
                    binding.imgDescBackground
                )
        }
    }

    private fun setFocusListeners() {
        binding.btnLearn.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                btnLearn.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_play_circle_filled_black_24dp, 0, 0, 0
                )
            } else {
                btnLearn.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_play_circle_filled_white_24dp, 0, 0, 0
                )
            }
        }

        binding.btnBookmark.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackEventMoreInfoBookmarkFocus(isBookMarked, content = data)
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_black, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                    )
                }
            } else {
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_white, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                    )
                }
            }
        }

        binding.btnLike.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackEventMoreInfoLikeFocus(isLiked, content = data)
                if (isLiked) {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_filled_black, 0, 0, 0
                    )
                } else {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_stroke_black, 0, 0, 0
                    )
                }
            } else {
                if (isLiked) {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_filled_white, 0, 0, 0
                    )
                } else {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_stroke_white, 0, 0, 0
                    )
                }
            }
        }

        binding.btnMoreTopics.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.btnMoreTopics.typeface =
                    Typeface.createFromAsset(assets, "fonts/gilroy_semibold.ttf")
                SegmentUtils.trackEventMoreInfoMainButtonFocus(content = data)
//                binding.btnMoreConcepts.background = resources.getDrawable(R.drawable.bg_detail_concepts_selected)
            } else {
                binding.btnMoreTopics.typeface =
                    Typeface.createFromAsset(assets, "fonts/gilroy_regular.ttf")
//                binding.btnMoreConcepts.background = resources.getDrawable(R.drawable.bg_detail_concepts_unselected)
            }
        }

        binding.btnRelatedConcepts.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.btnRelatedConcepts.typeface =
                    Typeface.createFromAsset(assets, "fonts/gilroy_semibold.ttf")
            } else {
                binding.btnRelatedConcepts.typeface =
                    Typeface.createFromAsset(assets, "fonts/gilroy_regular.ttf")
            }
        }
    }

    private fun updateBookmark() {
        detailsViewModel.bookmark(data.id, data.type, isBookMarked,
            object : BaseViewModel.APICallBacks<CommonApiResponse> {
                override fun onSuccess(model: CommonApiResponse?) {
                    if (model != null && model.success) {
                        Utils.isLikeBookmarkUpdatedForQuickLinks = true
                        updateBookMarkUI(isBookMarked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (bookmarkRetryCount < retryAttempt) {
                        bookmarkRetryCount++
                        updateBookmark()
                        return
                    } else {
                        /*reset retry count*/
                        bookmarkRetryCount = 0
                        /*revert back to previous state of ui */
                        updateBookMarkUI(!isBookMarked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_BOOKMARK)
                    }
                }
            })
    }

    private fun updateLike() {
        detailsViewModel.like(data.id, data.type, isLiked,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!!) {
                        Utils.isLikeBookmarkUpdatedForQuickLinks = true
                        updateLikUI(isLiked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (likeRetryCount < retryAttempt) {
                        likeRetryCount++
                        updateLike()
                        return
                    } else {
                        /*reset retry count*/
                        likeRetryCount = 0
                        /*revert back to previous state of ui */
                        updateLikUI(!isLiked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_LIKE)
                    }
                }
            })
    }

    private fun updateBookMarkUI(bookmarked: Boolean) {
        isBookMarked = bookmarked
        data.isBookmarked = bookmarked

        if (binding.btnBookmark.hasFocus()) {
            if (isBookMarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        } else {
            if (isBookMarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        }
    }

    private fun updateLikUI(liked: Boolean) {
        isLiked = liked
        data.isLiked = liked

        if (binding.btnLike.hasFocus()) {
            if (isLiked) {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_filled_black, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.liked)
            } else {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_stroke_black, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.like)
            }
        } else {
            if (isLiked) {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_filled_white, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.liked)
            } else {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_stroke_white, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.like)
            }
        }
    }

    override fun screenUpdate(type: String) {
        when (type) {
            RESUME -> {

            }
            START -> {

            }
        }
    }

    private fun startVideoPlayer(url: String, source: String) {
        val intent = Intent(context, VideoPlayerActivity::class.java)
        var resumedDuration: Long? = null
        if (isVideoResumed) {
            if (DataManager.instance.getLastSeek(data.id) != null)
                resumedDuration = DataManager.instance.getLastSeek(data.id)
            if (resumedDuration != null)
                intent.putExtra(CONTINUE_LEARNING, resumedDuration.toString())
            else
                intent.putExtra(CONTINUE_LEARNING, data.watched_duration.toString())
        } else
            intent.putExtra(CONTINUE_LEARNING, "0")
        if (data.annotation_attributes != null && data.annotation_attributes.size > 0) {
            intent.putExtra(ANNOTATION_DETAILS, true)
            intent.putParcelableArrayListExtra(
                ANNOTATION_MODEL,
                data.annotation_attributes
            )
        }
        intent.putExtra(VIDEO_URL, url)
        intent.putExtra(VIDEO_ID, data.id)
        intent.putExtra(VIDEO_CONCEPT_ID, data.learning_map.conceptId)
        intent.putExtra(CONTENT_TYPE, data.type)
        intent.putExtra(VIDEO_TOTAL_DURATION, 216.toString())
        intent.putExtra(VIDEO_TITLE, data.title)
        intent.putExtra(VIDEO_DESCRIPTION, data.description)
        intent.putExtra(VIDEO_SUBJECT, data.subject)
        intent.putExtra(VIDEO_CHAPTER, data.learning_map.chapter)
        intent.putExtra(VIDEO_AUTHORS, "")
        intent.putExtra(VIDEO_SOURCE, source)
        intent.putExtra(TOPIC_LEARN_PATH, data.learning_map.topicLearnPathName)
        intent.putExtra(LEARN_PATH_NAME, data.learnpath_name)
        intent.putExtra(LEARN_PATH_FORMAT_NAME, data.learnpath_format_name)
        intent.putExtra(FORMAT_ID, data.learning_map.format_id)
        intent.putExtra(IS_RECOMMENDATION_ENABLED, true)
        startActivity(intent)
    }


    companion object {
        private const val TAG = "DetailActivity"
    }
}