package com.embibe.embibetvapp.ui.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.databinding.ActivityUserSwitchBinding
import com.embibe.embibetvapp.ui.fragment.addUser.UserSwitchFragment

class UserSwitchActivity : BaseFragmentActivity(), UserSwitchFragment.CallBackListener {

    private lateinit var binding: ActivityUserSwitchBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_switch)
        val fragment = UserSwitchFragment()
        fragment.activityContext = this
        supportFragmentManager.beginTransaction().replace(R.id.fl_main_layout, fragment).commit()
    }

    override fun onCallBack() {
        finish()
    }
}