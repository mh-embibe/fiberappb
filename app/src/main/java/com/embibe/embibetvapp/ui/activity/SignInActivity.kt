package com.embibe.embibetvapp.ui.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ActivitySignInBinding
import com.embibe.embibetvapp.ui.fragment.signIn.SignInWithJioFragment
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils


class SignInActivity : BaseFragmentActivity(), ErrorScreensBtnClickListener {

    private lateinit var binding: ActivitySignInBinding
    val fragment = SignInWithJioFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in)
        supportFragmentManager.beginTransaction().replace(R.id.fl_main_layout, fragment).commit()
    }

    fun setFragment(fragment: Fragment, name: String) {
        supportFragmentManager.beginTransaction().replace(R.id.fl_main_layout, fragment)
            .addToBackStack(name).commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.fragments[0] is SignInWithJioFragment) {
            Utils.showAlertDialog(AppConstants.APP_CLOSE_DIALOG, supportFragmentManager, this)
        } else {
            super.onBackPressed()
        }
        SegmentUtils.trackTermsAndConditionBackPress()
    }

    override fun screenUpdate(type: String) {
        when (type) {
            AppConstants.LEAVE_APP -> {
                finish()
                SegmentUtils.trackEventHomeEnd()
            }
        }
    }

}
