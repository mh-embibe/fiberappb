package com.embibe.embibetvapp.ui.fragment.videoPlayer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.leanback.widget.*
import androidx.lifecycle.ViewModelProviders
import com.android.volley.VolleyError
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseRowsSupportFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.CONTENT_TYPE
import com.embibe.embibetvapp.constant.AppConstants.IS_NEXVIDEO
import com.embibe.embibetvapp.constant.AppConstants.IS_RECOMMENDATION_ENABLED
import com.embibe.embibetvapp.constant.AppConstants.PRACTICE_MODE
import com.embibe.embibetvapp.constant.AppConstants.PracticeModes.*
import com.embibe.embibetvapp.constant.AppConstants.TOPIC_LEARN_PATH
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_AUTHORS
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CONCEPT_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_DESCRIPTION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SOURCE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SUBJECT
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TITLE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TOTAL_DURATION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_URL
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.ui.activity.PracticeActivity
import com.embibe.embibetvapp.ui.activity.SyllabusSelectionActivity
import com.embibe.embibetvapp.ui.activity.TestDetailActivity
import com.embibe.embibetvapp.ui.activity.VideoPlayerActivity
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.presenter.CardPresenterSelector
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.unity.UnityPlayerActivity
import com.embibe.embibetvapp.utils.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class RecommendationRowsFragment : BaseRowsSupportFragment() {
    private var mRowsAdapter: ArrayObjectAdapter = ArrayObjectAdapter(ShadowRowPresenterSelector())
    val TAG = this.javaClass.name
    private var results = ArrayList<Results>()
    private lateinit var dPadKeysListener: DPadKeysListener
    private lateinit var homeViewModel: HomeViewModel

    init {
        initAdapter()
        initListeners()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createRows(results)
    }

    private fun createRows(recommendations: List<Results>) {
        for (recommendation in recommendations) {
            mRowsAdapter.add(creatingNewRow(recommendation))
        }
    }

    private fun creatingNewRow(results: Results): Row {
        val presenter = activity?.baseContext?.let {
            CardPresenterSelector(it)
        }
        val adapter = ArrayObjectAdapter(presenter)
        for (content in results.content) {
            adapter.add(content)
        }

        val headerItem = HeaderItem(Utils.removeUnderScoreAndCapitalize(results.section_name))
        return CardListRow(headerItem, adapter)
    }

    private fun initAdapter() {
        adapter = mRowsAdapter
    }

    private fun initListeners() {
        onItemViewClickedListener =
            OnItemViewClickedListener { itemViewHolder, item, rowViewHolder, row ->

                if (item is Content) {
                    SegmentUtils.trackEventRecommendationTileClick(item)

                    when (item.type.toLowerCase()) {
                        AppConstants.COOBO -> {
                            callCoobo(item)
                        }

                        AppConstants.VIDEO -> {
                            playVideo(item)
                        }
                        AppConstants.PRACTICE, AppConstants.CHAPTER -> {
                            if (item.id != "") {
                                Utils.getTopicsForPracticeAsync(requireContext(), item)
                                val intent = Intent(App.context, PracticeActivity::class.java)
                                intent.putExtra("id", item.id)
                                intent.putExtra("title", item.title)
                                intent.putExtra("subject", item.subject)
                                intent.putExtra(AppConstants.FORMAT_ID, item.learning_map.format_id)
                                intent.putExtra(
                                    AppConstants.CONCEPT_ID,
                                    item.learning_map.conceptId
                                )
                                intent.putExtra(AppConstants.LENGTH, item.length)
                                intent.putExtra(AppConstants.LM_NAME, item.learnmap_id)
                                intent.putExtra(
                                    PRACTICE_MODE,
                                    if (item.learning_map.mode == BookPractice.typeName) Normal.typeName else CFUPractice.typeName
                                )
                                intent.putExtra(
                                    TOPIC_LEARN_PATH,
                                    item.learning_map.topicLearnPathName
                                )
                                if (item.type == AppConstants.CHAPTER) {
                                    try {
                                        val parts = item.learning_map.lpcode.split("--")
                                        val lmCode = parts[parts.size - 1]
                                        val lmLevel = Utils.getLevelUsingCode(lmCode)
                                        val examCode = Utils.getExamLevelCode(parts)
                                        intent.putExtra("lm_level", lmLevel)
                                        intent.putExtra("lm_code", lmCode)
                                        intent.putExtra("exam_code", examCode)
                                        intent.putExtra(
                                            PRACTICE_MODE,
                                            if (item.learning_map.mode == BookPractice.typeName) BookPractice.typeName else Normal.typeName
                                        )
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                                intent.putExtra(AppConstants.LEARN_PATH_NAME, item.learnpath_name)
                                intent.putExtra(
                                    AppConstants.LEARN_PATH_FORMAT_NAME,
                                    item.learnpath_format_name
                                )
                                startActivity(intent)
                            }
                        }
                        AppConstants.LEARN -> {
                            val intent = Intent(context, SyllabusSelectionActivity::class.java)
                            Utils.insertBundle(item, intent)
                            startActivity(intent)
                        }
                        AppConstants.TEST -> {
                            val intent = Intent(context, TestDetailActivity::class.java)
                            Utils.insertBundle(item, intent)
                            startActivity(intent)
                        }
                    }
                }
            }

        onItemViewSelectedListener =
            OnItemViewSelectedListener { itemViewHolder, item, rowViewHolder, row ->

                if (item is Content) {

                    val indexOfRow = mRowsAdapter.indexOf(row)
                    val indexOfItem =
                        ((row as CardListRow).adapter as ArrayObjectAdapter).indexOf(item)
                    val itemView = itemViewHolder.view
                    val rowView = rowViewHolder.view

                    if (itemView.isFocusable && !itemView.isFocused) itemView.requestFocus()

                    saveLastSelected(indexOfItem, indexOfRow)
                    SegmentUtils.trackEventRecommendationTileFocus(item)

                    itemView.setOnKeyListener { v, keyCode, event ->
                        //checking only for down presses
                        if (event.action == KeyEvent.ACTION_DOWN) {
                            when (keyCode) {
                                KeyEvent.KEYCODE_DPAD_DOWN -> {
                                    //for last row
                                    if (indexOfRow == mRowsAdapter.size() - 1) {
                                        dPadKeysListener.isKeyPadDown(true, "Rows")
                                    }
                                }
                            }
                        }
                        false
                    }
                }
            }
    }

    private fun callCoobo(data: Content) {
        val i = Intent(App.context, UnityPlayerActivity::class.java)
        i.putExtra(VIDEO_CONCEPT_ID, data.learning_map.conceptId)
        i.putExtra(AppConstants.DURATION_LENGTH, data.length)
        i.putExtra(AppConstants.SLIDE_COUNT, data.watched_duration)
        i.putExtra(AppConstants.COOBO_URL, data.url)
        i.putExtra(AppConstants.DETAILS_ID, data.id)
        data.learning_map.let()
        {
            i.putExtra(AppConstants.LEARNMAP_CODE, data.learning_map.lpcode)
        }
        i.putExtra(IS_RECOMMENDATION_ENABLED, true)
        Utils.insertBundle(data, i)
        startActivity(i)
        activity?.finish()
    }

    fun playVideo(videoContent: Content) {
        val callback = object : BaseViewModel.APICallBackVolley {
            override fun <T> onSuccessCallBack(response: T) {
                hideProgress()
                response as JSONObject
                startVideoPlayer(
                    Utils.getVimeoHD((response["files"] as JSONArray)),
                    videoContent.id,
                    videoContent.learning_map.conceptId, videoContent.type,
                    videoContent.title, videoContent.description,
                    videoContent.subject,
                    videoContent.learning_map.chapter,
                    "", AppConstants.PLAYER_TYPE_EXOPLAYER,
                    videoContent.length.toString(),
                    videoContent.learning_map.topicLearnPathName,
                    videoContent
                )
                /*hide ProgressBar here */
            }

            override fun onErrorCallBack(e: VolleyError) {
                try {
                    makeLog("Error:  $e")
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                /*hide ProgressBar here */
                hideProgress()
            }
        }
        val source = Utils.getVideoTypeUsingUrl(videoContent.url)
        when {
            source == AppConstants.PLAYER_TYPE_EXOPLAYER -> {
                showProgress()
                val videoId = VideoUtils.getVimeoVideoId(videoContent.url).toString()
                Utils.getVideoURL(requireActivity(), videoId, videoContent.owner_info.key, callback)
            }
            videoContent.type.toLowerCase(Locale.getDefault()) == "Coobo".toLowerCase(Locale.getDefault()) -> {
                val i = Intent(App.context, UnityPlayerActivity::class.java)
                i.putExtra(AppConstants.COOBO_URL, videoContent.url)
                i.putExtra(AppConstants.DETAILS_ID, videoContent.id)
                i.putExtra(AppConstants.DURATION_LENGTH, videoContent.length)
                i.putExtra(AppConstants.SLIDE_COUNT, videoContent.watched_duration)
                i.putExtra(AppConstants.IS_RECOMMENDATION_ENABLED, true)
                i.putExtra(AppConstants.LEARNMAP_CODE, videoContent.learning_map)
                Utils.insertBundle(videoContent, i)
                startActivity(i)
                activity?.finish()
            }
            else -> {
                if (Utils.isYouTubeAppAvailable(requireContext())) {
                    Utils.startYoutubeApp(requireContext(), videoContent.url)
                } else {
                    startVideoPlayer(
                        videoContent.url, videoContent.id,
                        videoContent.learning_map.conceptId, videoContent.type,
                        videoContent.title, videoContent.description,
                        videoContent.subject,
                        videoContent.learning_map.chapter,
                        "", AppConstants.PLAYER_TYPE_YOUTUBE,
                        videoContent.length.toString(),
                        videoContent.learning_map.topicLearnPathName,
                        videoContent
                    )
                }
            }
        }

    }

    private fun saveLastSelected(indexOfItem: Int, indexOfRow: Int) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        with(sharedPref?.edit()) {
            this?.putInt(getString(R.string.last_selected_rec_row_item), indexOfItem)
            this?.putInt(getString(R.string.last_selected_rec_row), indexOfRow)
            this?.apply()
        }
    }

    fun restoreLastSelection() {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val itemIndex = sharedPref.getInt(getString(R.string.last_selected_rec_row_item), 0)
        val rowIndex = sharedPref.getInt(getString(R.string.last_selected_rec_row), 0)

        setSelectedPosition(
            rowIndex,
            true,
            object : ListRowPresenter.SelectItemViewHolderTask(itemIndex) {
                override fun run(holder: Presenter.ViewHolder?) {
                    super.run(holder)
                    holder?.view?.postDelayed({
                        holder.view.requestFocus()
                    }, 10)
                }
            })
    }

    fun setData(recommendations: List<Results>) {
        results.clear()
        results.addAll(recommendations)
    }

    fun setDpadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysListener = callback
    }

    private fun startVideoPlayer(
        videoUrl: String,
        videoId: String,
        videoConceptId: String,
        contentType: String,
        videoTitle: String,
        videoDescription: String,
        videoSubject: String,
        videoChapter: String,
        videoAuthors: String,
        videoType: String,
        totalDuration: String,
        topicLearnPath: String,
        data: Content
    ) {
        val intent = Intent(App.context, VideoPlayerActivity::class.java)
        intent.putExtra(VIDEO_URL, videoUrl)
        intent.putExtra(VIDEO_ID, videoId)
        intent.putExtra(VIDEO_CONCEPT_ID, videoConceptId)
        intent.putExtra(CONTENT_TYPE, contentType)
        intent.putExtra(VIDEO_TITLE, videoTitle)
        intent.putExtra(VIDEO_DESCRIPTION, videoDescription)
        intent.putExtra(VIDEO_SUBJECT, videoSubject)
        intent.putExtra(VIDEO_TOTAL_DURATION, totalDuration)
        intent.putExtra(VIDEO_CHAPTER, videoChapter)
        intent.putExtra(VIDEO_AUTHORS, videoAuthors)
        intent.putExtra(VIDEO_SOURCE, videoType)
        intent.putExtra(TOPIC_LEARN_PATH, topicLearnPath)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, data.learnpath_name)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, data.learnpath_format_name)
        intent.putExtra(AppConstants.FORMAT_ID, data.learning_map.format_id)
        intent.putExtra(IS_RECOMMENDATION_ENABLED, true)
        intent.putExtra(IS_NEXVIDEO, true)
        startActivity(intent)
        activity?.finish()
    }

}