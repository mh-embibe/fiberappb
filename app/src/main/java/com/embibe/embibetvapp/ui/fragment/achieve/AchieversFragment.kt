package com.embibe.embibetvapp.ui.fragment.achieve

import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.databinding.FragmentEmbibeAchieversBinding
import com.embibe.embibetvapp.model.Achiever
import com.embibe.embibetvapp.model.achieve.successStories.SuccessStory
import com.embibe.embibetvapp.ui.adapter.AchieverAdapter
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.viewmodel.AchieveViewModel
import com.embibe.embibetvapp.utils.Utils
import kotlinx.android.synthetic.main.item_embibe_achievers.view.*

class AchieversFragment : BaseAppFragment() {

    private val classTag = AchieversFragment::class.java.simpleName
    private lateinit var binding: FragmentEmbibeAchieversBinding
    private var list: ArrayList<Achiever> = ArrayList()
    private var dPadKeysCallback: DPadKeysListener? = null
    private lateinit var achieveViewModel: AchieveViewModel
    private var flipperTimer: CountDownTimer? = null
    private val cardFlippingInterval = 10000L
    private val cardFlipStartDelay = 10000L
    var achieverAdapter: AchieverAdapter? = null
    var dupList = ArrayList<Achiever>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_embibe_achievers, container,
            false
        )
        achieveViewModel = ViewModelProviders.of(this).get(AchieveViewModel::class.java)

        getSuccessStoriesApi()
        return binding.root
    }

    private fun initRv() {
        achieverAdapter = AchieverAdapter(requireContext())
        achieverAdapter?.setData(list)
        val rv = binding.rvAchiever
        rv.layoutManager = GridLayoutManager(requireContext(), 5)
        rv.adapter = achieverAdapter
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }

    /*load SuccessStory list from api*/
    fun getSuccessStoriesApi() {
        //       showProgress()
        achieveViewModel.getSuccessStories(object :
            BaseViewModel.APICallBacks<List<SuccessStory>> {

            override fun onSuccess(model: List<SuccessStory>?) {
                //      hideProgress()
                if (model != null) {
                    updateFutureSuccessResult(model)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                //    hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getSuccessStoriesApi()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }
        })
    }

    private fun updateFutureSuccessResult(listSuccessStories: List<SuccessStory>) {
        loadList(listSuccessStories)
        initRv()
    }

    fun startCardFlip() {
        if (list.size > 10) {
            binding.rvAchiever.postDelayed({
                startCardFlipping()
            }, cardFlipStartDelay)
        }
    }

    fun stopCardFlip() {
        Log.i(classTag, "stopping flipper timer")
        flipperTimer?.cancel()
        flipperTimer = null
    }

    private fun startCardFlipping() {
        flipperTimer = object : CountDownTimer(cardFlippingInterval, cardFlippingInterval) {
            override fun onFinish() {
                flipSuccessStoryCards()
                startCardFlipping()//calling again like a cycle
            }

            override fun onTick(millisUntilFinished: Long) {
//                Log.i(classTag, "flipper timer running")
                if (flipperTimer == null) return
            }
        }.start()

    }

    private fun flipSuccessStoryCards() {
        val itemToBeFlipped1 = list.random()
        val itemToBeFlipped2 = list.random()
        val indexOfItemToBeFlipped1 = list.indexOf(itemToBeFlipped1)
        val indexOfItemToBeFlipped2 = list.indexOf(itemToBeFlipped2)
//        Log.i(classTag, "item 1 = $itemToBeFlipped1 item 2 = $itemToBeFlipped2")
        if (indexOfItemToBeFlipped1 == indexOfItemToBeFlipped2) return
        var randomPos = (0..9).random()
        var updatePos = randomPos
        var item1 = binding.rvAchiever.findViewHolderForAdapterPosition(randomPos)?.itemView
        var item2 =
            binding.rvAchiever.findViewHolderForAdapterPosition(indexOfItemToBeFlipped2)?.itemView
        var flipFlag = false
        var itemVH: AchieverAdapter.ViewHolder? = null
        for (itemPosition in 0..9) {
            try {
                if (dupList[itemPosition].title != itemToBeFlipped1.title) {
                    flipFlag = true
                } else {
                    flipFlag = false
                    break
                }
            } catch (e: Exception) {
                Log.e("flip", "${e.message}")
            }
        }
        if (flipFlag) {
            flipAndSetData(item1, itemVH, itemToBeFlipped1, updatePos)
        }
    }

    private fun flipAndSetData(
        item1: View?,
        itemVH: AchieverAdapter.ViewHolder?,
        itemToBeFlipped1: Achiever,
        updatePos: Int
    ) {
        dupList[updatePos] = itemToBeFlipped1
        item1?.tv_embibe_achiever_back?.text = itemToBeFlipped1.title
        item1?.iv_embibe_achiever_back?.let {
            Glide.with(this).load(R.drawable.embibe_achievers_gif)
                .into(it)
        }
        item1?.apply {
            flipView.setFlipDuration(1000)
            flipView.flipTheView()
        }
    }

    private fun loadList(listSuccessStories: List<SuccessStory>) {
        for (successStory in listSuccessStories)
            list.add(Achiever(successStory.backgroundUrl, successStory.content, false))
        setDupList()
    }

    private fun setDupList() {
        for (index in 0..9) {
            if (index < list.size) {
                dupList.add(list[index])
            }
        }
    }

    override fun onStop() {
        flipperTimer?.cancel()
        flipperTimer = null
        super.onStop()
    }
}