package com.embibe.embibetvapp.ui.activity

import android.animation.Animator
import android.animation.ObjectAnimator
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RectShape
import android.os.Bundle
import android.os.Handler
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.doOnLayout
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.videoplayer.model.HexagonalModel
import kotlinx.android.synthetic.main.activity_scatter_graph.*
import kotlinx.android.synthetic.main.scatter_graph.*
import java.util.*
import kotlin.random.Random


open class ScatterGraphActivity : BaseFragmentActivity() {

    private val typeX: Int = 0
    private val typeY: Int = 1
    private val defaultX: Float = 0F
    private val defaultY: Float = 0F
    private lateinit var plottedHexagons: ArrayList<HexagonalModel>
    private lateinit var textViewYLabel: TextView
    private lateinit var durationTextView: TextView
    private lateinit var difficultyLevelTextView: TextView
    private lateinit var xDurationLP: FrameLayout.LayoutParams
    private lateinit var duration_view: View
    private var listdurationViews: ArrayList<View> = arrayListOf()
    private var listdificultyLevelViews: ArrayList<View> = arrayListOf()
    private lateinit var difficultyLevelView: View
    private lateinit var line: View
    private var listLineViews: ArrayList<View> = arrayListOf()
    var scale: Float = 0.0f
    private lateinit var subjectDataRed: java.util.ArrayList<HexagonalModel>
    private lateinit var subjectDataGreen: java.util.ArrayList<HexagonalModel>
    private lateinit var subjectDataViolet: java.util.ArrayList<HexagonalModel>
    lateinit var plot: FrameLayout
    lateinit var xLabelsView: FrameLayout
    lateinit var yLabelsView: FrameLayout
    lateinit var graph: FrameLayout
    private val xAxisLabelInterval = 10F
    private val xAxisLabelSize = 180F
    private val xLabelCount = xAxisLabelSize / xAxisLabelInterval

    private val yAxisLabelInterval = 1F
    private val yAxisLabelLabelSize = 10F
    private val yLabelCount = yAxisLabelLabelSize / yAxisLabelInterval


    private var xAxisTotalWidth: Int = 0//
    private var yAxisTotalHeight: Int = 0//

    private var plotAreaTotalWidth: Int = 0//
    private var plotAreaTotalHeight: Int = 0//


    var drawingXPosition = 0F
    var drawingYPosition = 0F

    var xPixelCount = 0F
    var yPixelCount = 0F
    var yPixelPlotAreaCount = 0F
    private val animDuration: Long = 10
    private val animDurationScaleIn: Long = 100//350
    private val animDurationScaleOut: Long = 10//550
    private var dpWidthInPx: Int = 0
    private var dpHeightInPx: Int = 0
    private lateinit var hexagonLP: FrameLayout.LayoutParams
    var lineLp: FrameLayout.LayoutParams =
        FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, 1)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scale = resources.displayMetrics.density
        dpWidthInPx = (13 * scale).toInt()
        dpHeightInPx = (13 * scale).toInt()
        hexagonLP = FrameLayout.LayoutParams(dpWidthInPx, dpHeightInPx)
        subjectDataRed = getPointsDataRed()
        subjectDataGreen = getPointsDataGreen()
        //initScatter()
    }

    /*initialize the char UI */
    private fun initScatter() {
        setContentView(R.layout.activity_scatter_graph)
        plot = findViewById<View>(R.id.plotArea) as FrameLayout
        xLabelsView = findViewById<View>(R.id.x_labels) as FrameLayout
        yLabelsView = findViewById<View>(R.id.y_labels) as FrameLayout
        startScatterChart()
        back.setOnClickListener {
            plotPoints(getAllData())
            Handler().postDelayed(Runnable {
                kotlin.run {
                    moveHexagons()
                    Handler().postDelayed(Runnable {
                        kotlin.run {
                            reverseMoveHexagons()
                        }
                    }, 5000)
                }
            }, 5000)


        }
    }

    /*move hexagonal forward in X axis*/
    fun moveHexagons() {
        for (item in plottedHexagons) {
            item.view.translateX(defaultX, item.moveToX, 1300, object : CompleteListener {
                override fun onComplete(view: View) {

                }
            })
        }
    }

    /*move hexagonal reverse in X axis*/
    fun reverseMoveHexagons() {
        for (item in plottedHexagons) {
            item.view.translateXReverse(item.moveToX, defaultX, 1300, object : CompleteListener {
                override fun onComplete(view: View) {

                }
            })
        }
    }


    /*init the chart UI*/
    fun startScatterChart() {

        (xLabelsView as View).doOnLayout {
            xAxisTotalWidth = it.width
            initXAxis()
        }

        (y_axis as View).doOnLayout {
            yAxisTotalHeight = it.height
        }
        plot.doOnLayout {
            plotAreaTotalHeight = it.height
            plotAreaTotalWidth = it.width
        }

    }


    /*draw the Y axis*/
    private fun initYAxis() {
        val list = generateItems(yAxisLabelInterval, yAxisLabelLabelSize)
        createTextViews(1, yLabelsView, yLabelCount, list, 0, yAxisTotalHeight)
        animateYAxis()
        var position = 0
        animateItemView(listdificultyLevelViews[position], typeY, object : CompleteListener {
            override fun onComplete(view: View) {
                ++position
                if (position < listdificultyLevelViews.size) {
                    animateItemView(listdificultyLevelViews[position], typeY, this)
                } else {
                    /*title text animation*/
                    y_title.setLetterDuration(100)
                    y_title.text = getString(R.string.difficulty_level)
                    Handler().postDelayed(Runnable {
                        kotlin.run {
                            drawLines()
                        }
                    }, 1000)

                }
            }

        })


    }

    /*draw the X axis*/
    private fun initXAxis() {
        y_axis.visibility = GONE
        val xLabelCount = xAxisLabelSize / xAxisLabelInterval
        val list = generateItems(xAxisLabelInterval, xAxisLabelSize)
        createTextViews(0, xLabelsView, xLabelCount, list, xAxisTotalWidth, 0)

        animateXAxis()

        var position = 0
        animateItemView(listdurationViews[position], typeX, object : CompleteListener {
            override fun onComplete(view: View) {
                ++position
                if (position < listdurationViews.size) {
                    animateItemView(listdurationViews[position], typeX, this)
                } else {
                    /*title text animation*/
                    x_title.setLetterDuration(100)
                    x_title.text = getString(R.string.time_in_minutes)

                    /*start Y axix animation here */
                    Handler().postDelayed(Runnable {
                        kotlin.run {
                            initYAxis()
                        }
                    }, 1000)
                }
            }

        })
    }

    fun animateXAxis() {
        /*x_axis line animation*/
        x_axis.translateX(xAxisTotalWidth.toFloat(), 3000, object : CompleteListener {
            override fun onComplete(view: View) {
            }
        })
    }

    fun animateYAxis() {
        /*x_axis line animation*/
        y_axis.translateY(yAxisTotalHeight.toFloat(), 1200, object : CompleteListener {
            override fun onComplete(view: View) {
            }
        })
    }

    private fun createTextViews(
        type: Int,
        rl: FrameLayout,
        labelCount: Float,
        str: IntArray,
        width: Int,
        height: Int
    ) {
        when (type) {
            0 -> {
                xPixelCount = width / labelCount
                drawingXPosition = xPixelCount
                setZero(rl, type)
                for (i in str.indices) {
                    duration_view =
                        LayoutInflater.from(this).inflate(R.layout.duration_layout, null)
                    drawDurationTextView(rl, duration_view, str[i].toString(), false)
                    if (i == str.size - 1) {
                        drawingXPosition -= (scale * 14).toInt()
                    }
                    setTextLabelPosition(duration_view, drawingXPosition, defaultY, type)
                    makeLog("drawingXPosition :  $drawingXPosition")
                    drawingXPosition += xPixelCount
                    listdurationViews.add(duration_view)
                    duration_view.findViewById<View>(R.id.tvDurationLabel).visibility = INVISIBLE
                    duration_view.findViewById<View>(R.id.lineVertical).visibility = INVISIBLE
                }

            }
            1 -> {
                yPixelCount = height / labelCount
                yPixelCount += 1
                drawingYPosition = height - yPixelCount
                setZero(rl, type)
                for (i in str.indices) {
                    difficultyLevelView =
                        LayoutInflater.from(this).inflate(R.layout.difficulty_level_layout, null)
                    drawYCustomTextLabels(rl, difficultyLevelView, str[i].toString(), false)
                    if (i == str.size - 1) {
                        drawingYPosition += 12
                    }
                    setTextLabelPosition(difficultyLevelView, defaultX, drawingYPosition, type)
                    drawingYPosition -= yPixelCount
                    listdificultyLevelViews.add(difficultyLevelView)
                    difficultyLevelView.visibility = INVISIBLE
                }

            }
        }


    }

    fun animateItemView(view: View, type: Int, callback: CompleteListener) {
        view.alpha = 0f
        view.visibility = View.VISIBLE

        view.animate()
            .alpha(1f)
            .setDuration(150)
            .setListener(object : Animator.AnimatorListener {

                override fun onAnimationRepeat(animation: Animator?) {
                }

                override fun onAnimationEnd(animation: Animator?) {
                    if (type == 0) {
                        view.findViewById<View>(R.id.lineVertical).visibility = VISIBLE
                        Handler().postDelayed(Runnable {
                            kotlin.run {
                                view.findViewById<View>(R.id.tvDurationLabel).visibility = VISIBLE
                            }
                        }, 1000)
                    }


                    callback.onComplete(view)
                }

                override fun onAnimationCancel(animation: Animator?) {
                }

                override fun onAnimationStart(animation: Animator?) {
                }

            })
    }

    inline fun View.fadeIn(durationMillis: Long = 250) {
        this.startAnimation(AlphaAnimation(0F, 1F).apply {
            duration = durationMillis
            fillAfter = true
        })
    }

    inline fun View.fadeOut(durationMillis: Long = 250) {
        this.startAnimation(AlphaAnimation(1F, 0F).apply {
            duration = durationMillis
            fillAfter = true
        })
    }


    fun drawYCustomTextLabels(rl: FrameLayout, view: View, text: String, isZero: Boolean) {

        difficultyLevelTextView = view.findViewById<TextView>(R.id.tvDifficultyLevelLabel)
        difficultyLevelTextView.setPadding(16, 0, 0, 0)
        difficultyLevelTextView.text = text
        difficultyLevelTextView.gravity = Gravity.END
        difficultyLevelTextView.setTextColor(
            ContextCompat.getColor(
                this,
                R.color.colorGreenGridLine
            )
        )
        difficultyLevelTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 7F)
        rl.addView(view)
    }


    fun drawDurationTextView(rl: FrameLayout, view: View, text: String, isZero: Boolean) {
        if (isZero) {
            durationTextView = view.findViewById<TextView>(R.id.tvDurationLabel_zero)
            durationTextView.setPadding(0, 6, 0, 0)
        } else {
            durationTextView = view.findViewById<TextView>(R.id.tvDurationLabel)
            durationTextView.setPadding(0, 6, 0, 0)
        }
        durationTextView.text = text
        durationTextView.gravity = Gravity.CENTER_VERTICAL
        durationTextView.setTextColor(ContextCompat.getColor(this, R.color.colorGreenGridLine))
        durationTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8F)
        rl.addView(view)
    }

    public fun drawLines() {
        /*draw lines*/
        if (yAxisTotalHeight > 0 && plotAreaTotalHeight > 0) {
            yPixelPlotAreaCount = yAxisTotalHeight / yLabelCount
            for (i in 1..yLabelCount.toInt()) {
                line = View(this)
                listLineViews.add(line)
                plotLineViewOnPlottingArea(plot, line, defaultX, getPlotYLinePoint(i))
                //line.slideUp()

            }
            if (listLineViews.isNotEmpty()) {
                var position = 0
                listLineViews[position].translateX(
                    xAxisTotalWidth.toFloat(),
                    250,
                    object : CompleteListener {
                        override fun onComplete(view: View) {
                            ++position
                            if (listLineViews.size > position) {
                                listLineViews[position].translateX(
                                    xAxisTotalWidth.toFloat(),
                                    250,
                                    this
                                )
                            }
                        }
                    })
            }


        }

    }

    fun plotViewOnPlottingArea(rl: FrameLayout, iv: ImageView, type: Int) {
        iv.layoutParams = hexagonLP
        setImageRes(iv, type)
        rl.addView(iv)
    }

    private fun plotLineViewOnPlottingArea(rl: FrameLayout, line: View, x: Float, y: Float) {

        line.x = x
        line.y = y
        line.layoutParams = lineLp
        line.alpha = 0.6F
        line.background = createDashedLined()
        line.visibility = INVISIBLE
        //line.setBackgroundColor(ContextCompat.getColor(this, R.color.colorGreenGridLine))
        rl.addView(line)
    }


    public fun plotPoints(data: ArrayList<HexagonalModel>) {
        plottedHexagons = arrayListOf<HexagonalModel>()
        val xSinglePixel = (plotAreaTotalWidth / xLabelCount) / xAxisLabelInterval
        val yPixelCount = (plotAreaTotalHeight / yLabelCount)
        val ySinglePixel = yPixelCount / yAxisLabelInterval

        var iv = ImageView(this)
        for (xYModel in data) {
            iv = ImageView(this)

            plotViewOnPlottingArea(plot, iv, xYModel.type)
            iv.x = (xSinglePixel * xYModel.x).toFloat()
            iv.y = (yAxisTotalHeight - (ySinglePixel * xYModel.y)) - 0F

            //translateImages(iv,iv.x,iv.y)
            xYModel.view = iv
            plottedHexagons.add(xYModel)
            if (xYModel.isHighlight) {
                alphaAnim(iv, xYModel.alpha)
            }
            alphaAnimate(xYModel, 2000)
        }
        //iv.visibility = INVISIBLE
        //doAnimation(data)


    }

    fun View.translateXReverse(
        fromX: Float,
        toX: Float,
        duration: Long,
        callback: CompleteListener
    ) {
        visibility = View.VISIBLE
        val animate = TranslateAnimation(fromX, toX, 0f, 0f)
        animate.duration = duration
        animate.fillAfter = true
        animate.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                callback.onComplete(this@translateXReverse)
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })
        this.startAnimation(animate)
    }

    fun View.translateX(fromX: Float, toX: Float, duration: Long, callback: CompleteListener) {
        visibility = View.VISIBLE
        val animate = TranslateAnimation(fromX, toX, 0f, 0f)
        animate.duration = duration
        animate.fillAfter = true
        animate.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                callback.onComplete(this@translateX)
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })
        this.startAnimation(animate)
    }

    fun View.translateX(fromX: Float, duration: Long, callback: CompleteListener) {
        visibility = View.VISIBLE
        val animate = TranslateAnimation(-fromX, 0f, 0f, 0f)
        animate.duration = duration
        animate.fillAfter = true
        animate.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                callback.onComplete(this@translateX)
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })
        this.startAnimation(animate)
    }

    fun View.translateY(fromY: Float, duration: Long, callback: CompleteListener) {
        visibility = View.VISIBLE
        val animate = TranslateAnimation(0f, 0f, fromY, 0f)
        animate.duration = duration
        animate.fillAfter = true
        animate.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                callback.onComplete(this@translateY)
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })
        this.startAnimation(animate)
    }

    private fun translateView(view: View, xValue: Float = 1F, callback: CompleteListener) {
        //view.animate().translationX(xValue).translationY(yValue).setDuration(animDuration).start()

        val anim: ObjectAnimator = ObjectAnimator.ofFloat(view, "x", xValue)
        val anim1 = ObjectAnimator.ofFloat(view, "translationX", 0f, xValue)
        anim.addUpdateListener { animation ->
            val animatedValue = animation.animatedValue as Float
            view.visibility = VISIBLE
            callback.onComplete(view)
        }
        anim.duration = 3000 // duration 5 seconds
        anim.start()
    }


    private fun getPlotYLinePoint(value: Int): Float {
        return plotAreaTotalHeight - (yPixelPlotAreaCount * value)
    }

    private fun generateItems(interval: Float, size: Float): IntArray {
        val nums = IntArray((size / interval).toInt())
        var total = size
        for (i in 0 until nums.size - 1) {
            if (i != 0) {
                nums[i] = nums[i - 1] + interval.toInt()
            } else {
                nums[i] = interval.toInt()
            }
        }
        nums[nums.size - 1] = total.toInt()
        Arrays.sort(nums)
        return nums
    }


    private fun setTextLabelPosition(view: View, x: Float, y: Float, type: Int) {
        xDurationLP = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.WRAP_CONTENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )
        when (type) {
            0 -> {
                xDurationLP.setMargins(x.toInt(), y.toInt(), 0, 0)
            }
            1 -> {
                xDurationLP.setMargins(x.toInt(), y.toInt(), 0, 0)
            }
        }

        view.layoutParams = xDurationLP
    }


    private fun setZero(rl: FrameLayout, type: Int) {
        when (type) {
            0 -> {
                val view = LayoutInflater.from(this).inflate(R.layout.duration_layout_zero, null)
                drawDurationTextView(rl, view, "0", true)
                setTextLabelPosition(view, defaultX, defaultY, type)

            }
            1 -> {
                /* val view = LayoutInflater.from(this).inflate(R.layout.difficulty_level_layout_zero, null)
                 drawYCustomTextLabels(rl, view, "D.L", true)
                 setTextLabelPosition(view, 0, yAxisTotalHeight-20,0)*/
            }
        }

    }


    private fun createDashedLined(): Drawable? {
        val sd = ShapeDrawable(RectShape())
        val fgPaintSel: Paint = sd.paint
        fgPaintSel.color = ContextCompat.getColor(this, R.color.colorGreenGridLine)
        fgPaintSel.style = Paint.Style.STROKE
        fgPaintSel.pathEffect = DashPathEffect(floatArrayOf(4f, 8f), 0F)
        return sd
    }

    private fun setImageRes(itemView: View, type: Int) {
        (itemView as ImageView).setImageResource(
            when (type) {
                1 -> {
                    R.drawable.hexagon_red_tiny
                }
                2 -> {
                    R.drawable.hexagon_green_tiny
                }

                else -> {
                    R.drawable.hexagon_red_tiny
                }
            }
        )
    }

    private fun getPlotXPoint(value: Int): Float? {
        return value * xPixelCount
    }

    private fun getPlotYPoint(value: Int): Float? {
        yPixelPlotAreaCount = yAxisTotalHeight / yLabelCount
        if (value == 0) {
            return plotAreaTotalHeight - 18F
        } else {
            return plotAreaTotalHeight - (yPixelPlotAreaCount * value) - 15
        }
    }

    private fun setPosition(view: TextView, x: Int?, y: Int?) {
        val lp: FrameLayout.LayoutParams =
            FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
            )
        lp.setMargins(x!!, y!!, 0, 0)
        view.layoutParams = lp
    }

    private fun setPosition(view: ImageView, x: Int?, y: Int?) {
        val lp: FrameLayout.LayoutParams =
            FrameLayout.LayoutParams(20, 20)
        lp.setMargins(x!!, y!!, 0, 0)
        view.layoutParams = lp

    }

    private fun setLinePosition(view: ImageView, x: Int?, y: Int?) {
        lineLp = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, 1)
        lineLp.setMargins(x!!, y!!, 0, 0)
        view.layoutParams = lineLp
    }


    fun clearPlots() {
        for (item in plottedHexagons) {
            plot.removeView(item.view)
        }
    }

    interface CompleteListener {
        fun onComplete(view: View)
    }


    private fun scaleAnimate(view: View, callback: CompleteListener) {
        runOnUiThread {
            view.animate().withStartAction {
            }.withEndAction {
                //scaleInAnimate(view)
                callback.onComplete(view)
            }.scaleX(1.1F).scaleY(1.1F).duration = animDuration
        }

    }

    private fun scaleInAnimate(view: View, callback: CompleteListener) {
        runOnUiThread {
            view.animate().withStartAction {
            }.withEndAction {
                callback.onComplete(view)
                scaleOutAnimate(view)
                //view.alpha = 0.5F
            }.scaleX(1.6F).scaleY(1.6F).duration = animDurationScaleIn
        }
    }

    private fun scaleOutAnimate(view: View/*,callback:CompleteListener*/) {
        runOnUiThread {
            view.animate().withStartAction {
            }.withEndAction {
                //view.alpha = view.alpha
                //callback.onComplete(view)
            }.scaleX(1F).scaleY(1F).duration = animDurationScaleOut
        }
    }

    private fun alphaAnimate(model: HexagonalModel, duration: Long/*,callback:CompleteListener*/) {
        runOnUiThread {
            model.view.alpha = 0f
            model.view.animate().alpha(model.alpha).withStartAction {
            }.withEndAction {
                //view.alpha = view.alpha
                //callback.onComplete(view)
            }.duration = duration
        }
    }

    private fun doAnimation(data: ArrayList<HexagonalModel>) {

        var position = 0
        var xYModel = data[position]
        val callback = object : CompleteListener {
            override fun onComplete(view: View) {
                ++position
                if (data.size > position) {
                    xYModel = data[position]
                    view.visibility = VISIBLE
                    scaleAnimate(xYModel.view, this)
                }

            }

        }
        scaleAnimate(xYModel.view, callback)

        //doScaleInOutAnimation(data)
    }

    private fun doScaleInOutAnimation(data: ArrayList<HexagonalModel>) {
        data.shuffle()
        var position = 0
        var xYModel = data[position]
        val callback = object : CompleteListener {
            override fun onComplete(view: View) {
                ++position
                if (data.size > position) {
                    xYModel = data[position]
                    //view.visibility = VISIBLE
                    scaleInAnimate(xYModel.view, this)
                }
            }

        }
        scaleInAnimate(xYModel.view, callback)

    }


    private fun alphaAnim(view: View, alpha: Float) {
        view.alpha = alpha
    }


    fun getAllData(): ArrayList<HexagonalModel> {
        val dataList = arrayListOf<HexagonalModel>()
        dataList.addAll(subjectDataRed)
        dataList.addAll(subjectDataGreen)
        dataList.sortBy { it.x }
        return dataList
    }

    private fun getPointsDataRed(): ArrayList<HexagonalModel> {
        var list = arrayListOf<HexagonalModel>()
        var model = HexagonalModel()
        list = arrayListOf<HexagonalModel>()

        for (i in 1..3) {
            model = HexagonalModel()
            model.x = Random.nextInt(10, 160).toFloat()
            model.y = Random.nextInt(2, 8).toFloat()
            model.type = 1
            model.alpha = 0.6F
            model.isHighlight = false
            model.moveToX = model.x + Random.nextInt(20, 120).toFloat()
            model.moveToY = model.y.toFloat()
            list.add(model)
        }
        /*list.addAll(getPointsDataBlue())
        list.addAll(getPointsDataViolet())*/
        list.sortBy { it.x }
        return list
    }

    fun getPointsDataGreen(): ArrayList<HexagonalModel> {
        var list = arrayListOf<HexagonalModel>()
        var model = HexagonalModel()
        for (i in 1..3) {
            model = HexagonalModel()
            model.x = Random.nextInt(5, 170).toFloat()
            model.y = Random.nextInt(2, 8).toFloat()
            model.type = 2

            model.isHighlight = true
            model.alpha = 0.2F
            model.moveToX = model.x + Random.nextInt(10, 50).toFloat()
            model.moveToY = model.y
            list.add(model)
        }
        list.sortBy { it.x }
        return list
    }

}