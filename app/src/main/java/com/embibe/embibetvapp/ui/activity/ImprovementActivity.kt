package com.embibe.embibetvapp.ui.activity

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.text.SpannableString
import android.view.KeyEvent
import android.view.View
import android.view.ViewTreeObserver
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.databinding.ActivityImprovementBinding
import com.embibe.embibetvapp.model.achieve.RankComparison.RankComparison
import com.embibe.embibetvapp.model.achieve.completedJourney.TestSummary
import com.embibe.embibetvapp.model.completionSummary.Topic
import com.embibe.embibetvapp.model.test.*
import com.embibe.embibetvapp.ui.adapter.BehaviourImprovementAdapter
import com.embibe.embibetvapp.ui.adapter.KnowledgeImprovementAdapter
import com.embibe.embibetvapp.ui.adapter.TestBarAdapter
import com.embibe.embibetvapp.ui.adapter.TopSkillAdapter
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.facebook.common.util.UriUtil
import com.facebook.drawee.backends.pipeline.Fresco
import kotlinx.android.synthetic.main.layout_correctly_answered_info.view.*
import kotlinx.android.synthetic.main.layout_grade_info.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class ImprovementActivity : BaseFragmentActivity() {

    private lateinit var binding: ActivityImprovementBinding
    private lateinit var coroutineScope: CoroutineScope

    private lateinit var barAdapterOne: TestBarAdapter
    private lateinit var barAdapterTwo: TestBarAdapter

    private lateinit var topSkillAdapterOne: TopSkillAdapter
    private lateinit var topSkillAdapterTwo: TopSkillAdapter

    private lateinit var behaviourImprovementAdapter: BehaviourImprovementAdapter
    private lateinit var knowledgeImprovementAdapter: KnowledgeImprovementAdapter

    private var rowsHM = TreeMap<Int, View>()
    private var viewFocusList = ArrayList<View>()
    private var viewFocusCounter = 0
    private var lastKeyPress = 0L
    private var progressLayoutWidth: Int = 0
    private var height: Int = 0

    private lateinit var behavioursModel: List<ImproveStrategy>
    private lateinit var rankComparisonModel: List<RankComparison>
    private lateinit var skillsModel: List<TestSummary>
    private lateinit var topicSummaryModel: List<Topic>
    private lateinit var attemptsModel: List<TestAttemptsRes>

    private var mySectionSummaryListOne = ArrayList<SectionSummary>()
    private var listTopSkillOne = ArrayList<SkillSet>()

    private var mySectionSummaryListTwo = ArrayList<SectionSummary>()
    private var listTopSkillTwo = ArrayList<SkillSet>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_improvement)
        coroutineScope = CoroutineScope(Dispatchers.Main)
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)

        getAPIsData()

        addViewsToHashMap()
        addFocusViewsToList()
        viewFocusList[viewFocusCounter].requestFocus()
        setBackgrounds(requestOptions)

        setChart()
        setAndAnimateProgressOne()
        setAndAnimateProgressTwo()

        setTopSkillRecycler()

        setAchieveMetrics()
        animateParticles()
        setBehaviourImprovementRecycler()
        setKnowledgeImprovementRecycler()

        setFirstRowListeners()
        setClickToWatchListener()
        setQWAKeyListener()
        setBehaviourImprovementListener()
        setKnowledgeImprovementListener()
    }

    private fun getAPIsData() {
        /*api data*/
        behavioursModel = Utils.fromJson(DataManager.instance.getBehaviours())
        rankComparisonModel = Utils.fromJson(DataManager.instance.getAchieveRankComparisons())
        skillsModel = Utils.fromJson(DataManager.instance.getAchieveSkills())
        topicSummaryModel = Utils.fromJson(DataManager.instance.getAchieveTopicSummaryList())
        attemptsModel = Utils.fromJson(DataManager.instance.getAchieveAttemptsList())
    }

    private fun setBackgrounds(requestOptions: RequestOptions) {
        Glide.with(this).setDefaultRequestOptions(requestOptions)
            .load(R.drawable.bg_first_row_feedback)
            .transition(DrawableTransitionOptions.withCrossFade())
            .transform(RoundedCorners(20))
            .into(binding.layoutTitleCharts.layoutChart.ivBg)
    }

    private fun addViewsToHashMap() {
        rowsHM[binding.layoutTitleCharts.constraintTitleCharts.id] =
            binding.layoutTitleCharts.constraintTitleCharts
        rowsHM[binding.layoutTopSkill.constraintTopSkill.id] =
            binding.layoutTopSkill.constraintTopSkill
        rowsHM[binding.clNestedQwaAndAchieve.id] = binding.clNestedQwaAndAchieve
    }

    private fun addFocusViewsToList() {
        viewFocusList.add(binding.layoutTitleCharts.layoutChart.constraintCharts)
        viewFocusList.add(binding.layoutTopSkill.constraintTopSkill)
        viewFocusList.add(binding.layoutQWA.tvDropDownSubject)
    }

    private fun setFirstRowListeners() {
        binding.layoutTitleCharts.layoutChart.constraintCharts.setOnKeyListener { v, keyCode, event ->
            when (event.action) {
                KeyEvent.ACTION_DOWN -> {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            return@setOnKeyListener keyPressLimiter {
                                smoothScrollRowsUp(binding.layoutTitleCharts.constraintTitleCharts.id)
                            }
                        }
                    }
                }
            }
            false
        }
    }

    private fun setClickToWatchListener() {
        binding.layoutTopSkill.constraintTopSkill.setOnKeyListener { v, keyCode, event ->
            when (event?.action) {
                KeyEvent.ACTION_DOWN -> {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            return@setOnKeyListener keyPressLimiter {
                                smoothScrollRowsUp(binding.layoutTopSkill.constraintTopSkill.id)
                            }
                        }
                        KeyEvent.KEYCODE_DPAD_UP -> {
                            return@setOnKeyListener keyPressLimiter {
                                smoothScrollRowsDown(binding.layoutTitleCharts.constraintTitleCharts.id)
                            }
                        }
                    }
                }
            }
            false
        }
    }

    private fun setQWAKeyListener() {
        binding.layoutQWA.tvDropDownSubject.setOnKeyListener { v, keyCode, event ->
            when (event?.action) {
                KeyEvent.ACTION_DOWN -> {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_UP ->
                            return@setOnKeyListener keyPressLimiter {
                                smoothScrollRowsDown(binding.layoutTopSkill.constraintTopSkill.id)
                            }
                    }
                }
            }
            false
        }
    }

    private fun smoothScrollRowsUp(viewTranslatingIndex: Int) {
        viewFocusCounter++
        rowsHM.forEach { (otherViewsIndex, _) ->
            if (otherViewsIndex != viewTranslatingIndex) {
                slideRow(
                    rowsHM[otherViewsIndex]!!,
                    -rowsHM[viewTranslatingIndex]?.translationY!!.toFloat()
                        .minus(rowsHM[viewTranslatingIndex]?.height!!.toFloat()),
                    false,
                    rowsHM[viewTranslatingIndex]?.translationY!!.toFloat()
                )
            } else
                slideRow(
                    rowsHM[viewTranslatingIndex]!!,
                    -rowsHM[viewTranslatingIndex]?.translationY!!.toFloat()
                        .minus(rowsHM[viewTranslatingIndex]?.height!!.toFloat()),
                    true,
                    rowsHM[viewTranslatingIndex]?.translationY!!.toFloat(),
                    1f, 0f
                )
        }
        coroutineScope.launch {
            delay(10)
            if (viewFocusCounter < viewFocusList.size) {
                viewFocusList[viewFocusCounter].requestFocus()
            }
        }
    }

    private fun smoothScrollRowsDown(viewTranslatingIndex: Int) {
        rowsHM.forEach { (otherViewsIndex, _) ->
            if (otherViewsIndex != viewTranslatingIndex) {
                slideRow(
                    rowsHM[otherViewsIndex]!!,
                    -rowsHM[viewTranslatingIndex]?.height!!.toFloat()
                        .plus(rowsHM[viewTranslatingIndex]!!.translationY),
                    false,
                    rowsHM[viewTranslatingIndex]!!.translationY
                )
            } else {
                slideRow(
                    rowsHM[viewTranslatingIndex]!!,
                    -rowsHM[viewTranslatingIndex]?.height!!.toFloat()
                        .plus(rowsHM[viewTranslatingIndex]!!.translationY),
                    true,
                    rowsHM[viewTranslatingIndex]!!.translationY,
                    0f,
                    1f
                )
            }
        }

        coroutineScope.launch {
            delay(10)
            if (viewFocusCounter > 0 && viewFocusCounter < viewFocusList.size) {
                viewFocusList[--viewFocusCounter].requestFocus()
            }
        }
    }

    private fun slideRow(
        view: View,
        viewHeight: Float = 0f,
        canFade: Boolean = false,
        updatedTransYValue: Float = 0f,
        alphaFrom: Float = 0f,
        alphaTo: Float = 1f
    ) {
        val transYObjectAnimator =
            ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, updatedTransYValue, -viewHeight)
        var alphaObjectAnimator: ObjectAnimator? = null
        if (canFade) {
            alphaObjectAnimator =
                ObjectAnimator.ofFloat(view, View.ALPHA, alphaFrom, alphaTo)
        }

        val animatorSet = AnimatorSet()
        if (alphaObjectAnimator != null)
            animatorSet.playTogether(transYObjectAnimator, alphaObjectAnimator)
        else
            animatorSet.playTogether(transYObjectAnimator)

        animatorSet.duration = TestFeedbackActivity.RowAnimDelayValues.ANIM_DELAY
        animatorSet.start()
    }

    private fun keyPressLimiter(threshHold: Long = 1500L, action: () -> Unit): Boolean {
        if (SystemClock.elapsedRealtime() - lastKeyPress < threshHold) {
            return true
        } else {
            action.invoke()
        }
        lastKeyPress = SystemClock.elapsedRealtime()
        return false
    }

    private fun setChart() {
//
        // 1 is previous test and 0 is current test
        for (i in attemptsModel[1].scoreListInfo!!.sectionSummaryList!!.indices) {
            mySectionSummaryListOne.add(attemptsModel[1].scoreListInfo!!.sectionSummaryList!![i])
        }

        for (i in attemptsModel[0].scoreListInfo!!.sectionSummaryList!!.indices) {
            mySectionSummaryListTwo.add(attemptsModel[0].scoreListInfo!!.sectionSummaryList!![i])
        }

        barAdapterOne = TestBarAdapter()
        barAdapterTwo = TestBarAdapter()

        barAdapterOne.setItems(mySectionSummaryListOne)
        barAdapterTwo.setItems(mySectionSummaryListTwo)

        binding.layoutTitleCharts.layoutChart.layoutCorrectlyAnsweredInfo.rvMarksBar.adapter =
            barAdapterOne
        binding.layoutTitleCharts.layoutChart.layoutCorrectlyAnsweredInfo2.rvMarksBar.adapter =
            barAdapterTwo

        setDataForChart()
    }

    private fun setDataForChart() {
        binding.layoutTitleCharts.layoutChart.layoutGradeInfo.tvTest.text = "Test 1"
        binding.layoutTitleCharts.layoutChart.layoutGradeInfo.valueGrade.text =
            attemptsModel[1].scoreListInfo!!.grade
        binding.layoutTitleCharts.layoutChart.layoutGradeInfo.valueGrade.setTextColor(getColor(R.color.grade_previous))
        binding.layoutTitleCharts.layoutChart.layoutCorrectlyAnsweredInfo.constraintCorrectlyAnswered.tv_score.text =
            setCorrectlyAnsweredQuestions(
                skillsModel[1].skills?.correct_answer ?: 0,
                skillsModel[1].skills?.questions ?: 0,
                R.color.colorGreen
            )

        binding.layoutTitleCharts.layoutChart.layoutGradeInfo2.tvTest.text = "Test 2"
        binding.layoutTitleCharts.layoutChart.layoutGradeInfo2.valueGrade.text =
            attemptsModel[0].scoreListInfo!!.grade
        binding.layoutTitleCharts.layoutChart.layoutGradeInfo2.valueGrade.setTextColor(getColor(R.color.grade_current))
        binding.layoutTitleCharts.layoutChart.layoutCorrectlyAnsweredInfo2.constraintCorrectlyAnswered.tv_score.text =
            setCorrectlyAnsweredQuestions(
                skillsModel[0].skills?.correct_answer ?: 0,
                skillsModel[0].skills?.questions ?: 0,
                R.color.grade_current
            )
    }

    private fun setTopSkillRecycler() {

        for (i in skillsModel[1].skills?.skillset!!.indices) {
            listTopSkillOne.add(skillsModel[1].skills?.skillset?.get(i)!!)
        }

        for (i in skillsModel[0].skills?.skillset!!.indices) {
            listTopSkillTwo.add(skillsModel[0].skills?.skillset?.get(i)!!)
        }

        topSkillAdapterOne = TopSkillAdapter()
        topSkillAdapterTwo = TopSkillAdapter()

        topSkillAdapterOne.setItems(listTopSkillOne)
        topSkillAdapterTwo.setItems(listTopSkillTwo)

        binding.layoutTopSkill.layoutCorrectlyAnsweredPrevious.rvScoreBar.adapter =
            topSkillAdapterOne
        binding.layoutTopSkill.layoutCorrectlyAnsweredAfter.rvScoreBar.adapter = topSkillAdapterTwo

        setDataForTopSkill()
    }

    private fun setDataForTopSkill() {
        binding.layoutTopSkill.layoutTestScore.tvTopSkill.text = getString(R.string.test_1_skill)
        binding.layoutTopSkill.layoutTestScoreAfter.tvTopSkill.text =
            getString(R.string.test_2_skill)

        binding.layoutTopSkill.layoutTestScore.valueTopSkill.text = skillsModel[1].skills?.top_skill
        binding.layoutTopSkill.layoutTestScoreAfter.valueTopSkill.text =
            skillsModel[0].skills?.top_skill

        binding.layoutTopSkill.layoutTestScore.valueTopSkill.setTextColor(getColor(R.color.white))
        binding.layoutTopSkill.layoutTestScoreAfter.valueTopSkill.setTextColor(getColor(R.color.grade_current))

        binding.layoutTopSkill.layoutTestScore.tvScore.text =
            setCorrectlyAnsweredQuestions(
                skillsModel[1].skills?.correct_answer ?: 0,
                skillsModel[1].skills?.questions ?: 0,
                R.color.colorGreen
            )
        binding.layoutTopSkill.layoutTestScoreAfter.tvScore.text =
            setCorrectlyAnsweredQuestions(
                skillsModel[0].skills?.correct_answer ?: 0,
                skillsModel[0].skills?.questions ?: 0,
                R.color.grade_current
            )

        binding.layoutTopSkill.layoutCorrectlyAnsweredPrevious.tvSkillWiseAccuracy.text =
            getString(R.string.test_skill_wise_accuracy_1)
        binding.layoutTopSkill.layoutCorrectlyAnsweredAfter.tvSkillWiseAccuracy.text =
            getString(R.string.test_skill_wise_accuracy_2)
    }

    private fun setCorrectlyAnsweredQuestions(
        correctAnswer: Int, questions: Int, color: Int
    ): SpannableString {
        val questionStr = getString(R.string.test_feedback_questions)
        val bothValueAsStr = "$correctAnswer/$questions"
        return Utils.getSpannable(
            this, "$bothValueAsStr $questionStr",
            color, 0, correctAnswer.toString().length
        )
    }

    private fun setAndAnimateProgressOne() {
        val goodScore = attemptsModel[0].scoreListInfo?.goodScore!!
        val yourScore = attemptsModel[0].scoreListInfo?.myScore!!
        val cutOffScore = attemptsModel[0].scoreListInfo?.cutOffScore!!
        var totalScore = 100
        //to avoid values going of screen
        var extraBiasSpace = getHorizontalBiasOfBaseLine(totalScore)
        totalScore += extraBiasSpace
        totalScore += 20
//        if (mySectionSummaryList.size >= 5)
//            changeMotionLayoutConstraintSetToFit()

        animate(
            goodScore.toInt(),
            totalScore,
            binding.layoutTitleCharts.layoutChart.layoutProgressInfo.progressBarGoodScore,
            binding.layoutTitleCharts.layoutChart.layoutProgressInfo.tvGoodScore
        )
//        removeMarginTopFromGoodScoreProgressBar(goodScore)
        moveVerticalBaseLineBiasBasedOnScore(yourScore, totalScore)
        moveCutOffLineBiasBasedOnScore(cutOffScore, totalScore)
//        moveHorizontalCutOffMarkerBiasBasedOnScore(cutOffScore, totalScore)
        if (yourScore < 0) {
            animate(
                yourScore.toInt(),
                totalScore,
                binding.layoutTitleCharts.layoutChart.layoutProgressInfo.progressBarNegative,
                binding.layoutTitleCharts.layoutChart.layoutProgressInfo.tvNegativeScore
            )
        } else {
            animate(
                yourScore.toInt(),
                totalScore,
                binding.layoutTitleCharts.layoutChart.layoutProgressInfo.progressBarYourScore,
                binding.layoutTitleCharts.layoutChart.layoutProgressInfo.tvYourScore
            )
        }
    }

    private fun getHorizontalBiasOfBaseLine(totalScore: Int): Int {
        var params =
            binding.layoutTitleCharts.layoutChart.layoutProgressInfo.viewLine.layoutParams as ConstraintLayout.LayoutParams
        return params.horizontalBias.times(totalScore).toInt()
    }

    private fun moveVerticalBaseLineBiasBasedOnScore(yourScore: Double, totalScore: Int) {
        if (yourScore < 0) {
            val baseLineVertical = binding.layoutTitleCharts.layoutChart.layoutProgressInfo.viewLine
            val baseLineParams = baseLineVertical.layoutParams as ConstraintLayout.LayoutParams
            baseLineParams.horizontalBias =
                baseLineParams.horizontalBias + (Math.abs(yourScore).toFloat() / totalScore)
            baseLineVertical.layoutParams = baseLineParams
            baseLineVertical.requestLayout()
        }
    }

    private fun moveCutOffLineBiasBasedOnScore(cutOffScore: Int, totalScore: Int) {
        // AnyScore/totalScore to get bias value
        //totalMarks 130, Negative Marks -12

        val calScore = (cutOffScore.toDouble().div(totalScore))
        val cutOffLine = binding.layoutTitleCharts.layoutChart.layoutProgressInfo.ivCutOffMarker
        val cutOffLabel =
            binding.layoutTitleCharts.layoutChart.layoutProgressInfo.tvCutOffMarkerLabel
        val cutOffLineParams = cutOffLine.layoutParams as ConstraintLayout.LayoutParams
        val cutOffLineAnimator = ValueAnimator.ofFloat(0f, calScore.toFloat())

        cutOffLineAnimator.addUpdateListener { value ->
            cutOffLineParams.horizontalBias = (value.animatedValue as Float)
            cutOffLine.layoutParams = cutOffLineParams
        }

        val cutOffLabelAnimator = ValueAnimator.ofInt(0, cutOffScore)
        cutOffLabelAnimator.addUpdateListener { value ->
            cutOffLabel.text = (value.animatedValue as Int).toString()
        }

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(cutOffLabelAnimator, cutOffLineAnimator)
        animatorSet.duration = 1500
        animatorSet.start()
    }

//    private fun moveHorizontalCutOffMarkerBiasBasedOnScore(
//        cutOffScore: Int,
//        totalScore: Int
//    ) {
//        var endSet: ConstraintSet = correctlyAnsweredMotionLayout?.getConstraintSet(R.id.end)!!
//        endSet.setVerticalBias(R.id.iv_cut_off_marker, calculateCutOffBias(cutOffScore))
//        endSet.applyTo(correctlyAnsweredMotionLayout)
////        animateBar(binding.layoutChart.layoutCorrectlyAnswered.ivCutOffHolder, cutOffScore)
//
//        val cutOffHorizontalTextView = binding.layoutChart.layoutProgressInfo.tvCutOffLabel
//        val cutOffStr = getString(R.string.cut_off)
//        val marksStr = getString(R.string.marks)
//        cutOffHorizontalTextView.text = "$cutOffStr $cutOffScore $marksStr"
//    }

    private fun calculateCutOffBias(cutOffScore: Int): Float {
        if (cutOffScore >= 10 && cutOffScore <= 20) {
            return 0.28f
        } else if (cutOffScore >= 20 && cutOffScore <= 40) {
            return 0.22f
        } else if (cutOffScore >= 40 && cutOffScore <= 60) {
            return 0.17f
        } else if (cutOffScore >= 60 && cutOffScore <= 80) {
            return 0.08f
        } else
            return 0.01f
    }

    private fun setAndAnimateProgressTwo() {
        updateUIForGoldenProgress()
        val goodScore = attemptsModel[1].scoreListInfo?.goodScore!!
        val yourScore = attemptsModel[1].scoreListInfo?.myScore!!
        val cutOffScore = attemptsModel[1].scoreListInfo?.cutOffScore!!
        var totalScore = 100
        //to avoid values going of screen
        var extraBiasSpace = getHorizontalBiasOfBaseLineTwo(totalScore)
        totalScore += extraBiasSpace
        totalScore += 20
//        if (mySectionSummaryList.size >= 5)
//            changeMotionLayoutConstraintSetToFit()

        animate(
            goodScore.toInt(),
            totalScore,
            binding.layoutTitleCharts.layoutChart.layoutProgressInfo2.progressBarGoodScore,
            binding.layoutTitleCharts.layoutChart.layoutProgressInfo2.tvGoodScore
        )
//        removeMarginTopFromGoodScoreProgressBar(goodScore)
        moveVerticalBaseLineBiasBasedOnScoreTwo(yourScore, totalScore)
        moveCutOffLineBiasBasedOnScoreTwo(cutOffScore, totalScore)
//        moveHorizontalCutOffMarkerBiasBasedOnScore(cutOffScore, totalScore)
        if (yourScore < 0) {
            animate(
                yourScore.toInt(),
                totalScore,
                binding.layoutTitleCharts.layoutChart.layoutProgressInfo2.progressBarNegative,
                binding.layoutTitleCharts.layoutChart.layoutProgressInfo2.tvNegativeScore
            )
        } else {
            animate(
                yourScore.toInt(),
                totalScore,
                binding.layoutTitleCharts.layoutChart.layoutProgressInfo2.progressBarYourScore,
                binding.layoutTitleCharts.layoutChart.layoutProgressInfo2.tvYourScore
            )
        }
    }

    private fun updateUIForGoldenProgress() {
        binding.layoutTitleCharts.layoutChart.layoutProgressInfo2.ivCircleYellow
            .setBackgroundResource(R.drawable.ic_circle_golden)
        binding.layoutTitleCharts.layoutChart.layoutProgressInfo2.progressBarYourScore
            .setBackgroundResource(R.drawable.test_feedback_progress_golden_check)
    }

    private fun getHorizontalBiasOfBaseLineTwo(totalScore: Int): Int {
        var params =
            binding.layoutTitleCharts.layoutChart.layoutProgressInfo2.viewLine.layoutParams as ConstraintLayout.LayoutParams
        return params.horizontalBias.times(totalScore).toInt()
    }

    private fun moveVerticalBaseLineBiasBasedOnScoreTwo(yourScore: Double, totalScore: Int) {
        if (yourScore < 0) {
            val baseLineVertical =
                binding.layoutTitleCharts.layoutChart.layoutProgressInfo2.viewLine
            val baseLineParams = baseLineVertical.layoutParams as ConstraintLayout.LayoutParams
            baseLineParams.horizontalBias =
                baseLineParams.horizontalBias + (Math.abs(yourScore).toFloat() / totalScore)
            baseLineVertical.layoutParams = baseLineParams
            baseLineVertical.requestLayout()
        }
    }

    private fun moveCutOffLineBiasBasedOnScoreTwo(cutOffScore: Int, totalScore: Int) {
        // AnyScore/totalScore to get bias value
        //totalMarks 130, Negative Marks -12

        val calScore = (cutOffScore.toDouble().div(totalScore))
        val cutOffLine = binding.layoutTitleCharts.layoutChart.layoutProgressInfo2.ivCutOffMarker
        val cutOffLabel =
            binding.layoutTitleCharts.layoutChart.layoutProgressInfo2.tvCutOffMarkerLabel
        val cutOffLineParams = cutOffLine.layoutParams as ConstraintLayout.LayoutParams
        val cutOffLineAnimator = ValueAnimator.ofFloat(0f, calScore.toFloat())

        cutOffLineAnimator.addUpdateListener { value ->
            cutOffLineParams.horizontalBias = (value.animatedValue as Float)
            cutOffLine.layoutParams = cutOffLineParams
        }

        val cutOffLabelAnimator = ValueAnimator.ofInt(0, cutOffScore)
        cutOffLabelAnimator.addUpdateListener { value ->
            cutOffLabel.text = (value.animatedValue as Int).toString()
        }

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(cutOffLabelAnimator, cutOffLineAnimator)
        animatorSet.duration = 1500
        animatorSet.start()
    }

    private fun animate(progressValue: Int, totalScore: Int, progressBar: ImageView, tv: TextView) {
        animateBar(progressBar, progressValue, totalScore)
        tv.animateWithProgressBar(progressValue)
    }

    private fun TextView.animateWithProgressBar(progressValue: Int) {

        val animator = ObjectAnimator.ofInt(0, progressValue)
        animator.addUpdateListener { animationValue ->
            this.text = animationValue.animatedValue.toString()
        }
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(animator)
        animatorSet.duration = 2000
        animatorSet.start()
    }

    private fun animateBar(bar: ImageView, progressValue: Int, totalScore: Int) {
        val progressLayout = binding.layoutTitleCharts.layoutChart.layoutProgressInfo
        val vto = progressLayout.root.viewTreeObserver
        var newProgressValue = 0

        newProgressValue = kotlin.math.abs(progressValue)
        var listener: ViewTreeObserver.OnGlobalLayoutListener? = null

        listener = ViewTreeObserver.OnGlobalLayoutListener {
            progressLayoutWidth = progressLayout.root.width
            height = progressLayout.root.height

            val calValue = newProgressValue.toDouble() / totalScore * progressLayoutWidth
            val animator = ValueAnimator.ofInt(0, calValue.toInt())
            animator.addUpdateListener { valueAnimator ->
                val value = valueAnimator.animatedValue
                val params = bar.layoutParams as ConstraintLayout.LayoutParams
                if (value as Int > 0) {
                    bar.visibility = View.VISIBLE
                } else
                    bar.visibility = View.GONE
                params.width = value
                bar.layoutParams = params
            }

            animator.duration = 1500
            animator.start()

            binding.layoutTitleCharts.layoutChart.layoutProgressInfo.root.viewTreeObserver?.removeOnGlobalLayoutListener(
                listener!!
            )
        }
        vto?.addOnGlobalLayoutListener(listener)
    }

    private fun animateParticles() {
        val controller = Fresco.newDraweeControllerBuilder()
        controller.autoPlayAnimations = true
        val uri: Uri = UriUtil.getUriForResourceId(R.drawable.achieve_sphere_animation)
        controller.setUri(uri)
        binding.layoutAchieve.ivParticles.controller = controller.build()
    }

    private fun setAchieveMetrics() {
        binding.layoutAchieve.tvSphere1.text =
            if (rankComparisonModel[1].rank.isNullOrEmpty()
                || rankComparisonModel[1].rank!!.toLowerCase().contains("null")
            ) "NA" else rankComparisonModel[1].rank

        binding.layoutAchieve.tvSphere2.text =
            if (rankComparisonModel[0].rank.isNullOrEmpty()
                || rankComparisonModel[0].rank!!.toLowerCase().contains("null")
            ) "NA"
            else rankComparisonModel[0].rank
    }

    private fun setBehaviourImprovementRecycler() {
        behaviourImprovementAdapter = BehaviourImprovementAdapter()
        binding.layoutAchieve.recyclerBehaviour.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.layoutAchieve.recyclerBehaviour.adapter = behaviourImprovementAdapter

        behaviourImprovementAdapter.setData(
            arrayListOf(
                TestBehaviourImprovement(
                    behavioursModel[1].negativeBehaviour!![0].behavior_name!!,
                    behavioursModel[1].negativeBehaviour!![1].behavior_name!!,
                    ""
                ),
                TestBehaviourImprovement(
                    behavioursModel[0].negativeBehaviour!![0].behavior_name!!,
                    behavioursModel[0].negativeBehaviour!![1].behavior_name!!,
                    ""
                )
            )
        )
    }

    private fun setKnowledgeImprovementRecycler() {
        knowledgeImprovementAdapter = KnowledgeImprovementAdapter()
        binding.layoutAchieve.recyclerKnowledge.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.layoutAchieve.recyclerKnowledge.adapter = knowledgeImprovementAdapter

        knowledgeImprovementAdapter.setData(topicSummaryModel as ArrayList<Topic>)
//        knowledgeImprovementAdapter.setData(
//            arrayListOf(
//                TestKnowledgeImprovement(
//                    "Importance of pH in everyday life", "25%", "47%"
//                ),
//                TestKnowledgeImprovement(
//                    "Preparation and uses of Sodium Hydroxide", "32%", "57%"
//                ),
//                TestKnowledgeImprovement(
//                    "Renewable sources of energy", "25%", "47%"
//                )
//            )
//        )
    }

    fun createKnowledgeImprovementData() {
        topicSummaryModel
    }

    private fun setBehaviourImprovementListener() {
        binding.layoutAchieve.imgLayout.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        toggleBehaviourRecyclerVisibility()
                    }
                }
            }

            false
        }
    }

    private fun setKnowledgeImprovementListener() {
        binding.layoutAchieve.imgLayoutKnowledge.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        toggleKnowledgeRecyclerVisibility()
                    }
                }
            }

            false
        }
    }

    private fun toggleBehaviourRecyclerVisibility() {
        if (binding.layoutAchieve.recyclerBehaviour.visibility == View.GONE) {

            binding.layoutAchieve.recyclerBehaviour.visibility = View.VISIBLE
            binding.layoutAchieve.imgExpand.setImageDrawable(getDrawable(R.drawable.ic_expand_less_24px))
        } else if (binding.layoutAchieve.recyclerBehaviour.visibility == View.VISIBLE) {

            binding.layoutAchieve.recyclerBehaviour.visibility = View.GONE
            binding.layoutAchieve.imgExpand.setImageDrawable(getDrawable(R.drawable.ic_expand_more_24px))
        }

        if (binding.layoutAchieve.recyclerKnowledge.visibility == View.VISIBLE) {
            binding.layoutAchieve.recyclerKnowledge.visibility = View.GONE
            binding.layoutAchieve.imgExpandKnowledge.setImageDrawable(getDrawable(R.drawable.ic_expand_more_24px))
        }
    }

    private fun toggleKnowledgeRecyclerVisibility() {
        if (binding.layoutAchieve.recyclerKnowledge.visibility == View.GONE) {

            binding.layoutAchieve.recyclerKnowledge.visibility = View.VISIBLE
            binding.layoutAchieve.imgExpandKnowledge.setImageDrawable(getDrawable(R.drawable.ic_expand_less_24px))
        } else if (binding.layoutAchieve.recyclerKnowledge.visibility == View.VISIBLE) {

            binding.layoutAchieve.recyclerKnowledge.visibility = View.GONE
            binding.layoutAchieve.imgExpandKnowledge.setImageDrawable(getDrawable(R.drawable.ic_expand_more_24px))
        }

        if (binding.layoutAchieve.recyclerBehaviour.visibility == View.VISIBLE) {
            binding.layoutAchieve.recyclerBehaviour.visibility = View.GONE
            binding.layoutAchieve.imgExpand.setImageDrawable(getDrawable(R.drawable.ic_expand_more_24px))
        }
    }
}