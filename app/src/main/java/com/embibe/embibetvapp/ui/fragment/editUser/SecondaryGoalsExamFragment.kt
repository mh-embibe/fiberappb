package com.embibe.embibetvapp.ui.fragment.editUser

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentSecondaryGoalsExamBinding
import com.embibe.embibetvapp.model.UpdateProfileRequest
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.updateProfile.Errors
import com.embibe.embibetvapp.model.updateProfile.UpdateProfileRes
import com.embibe.embibetvapp.ui.activity.AddGoalsExamsActivity
import com.embibe.embibetvapp.ui.adapter.SecondaryGoalExamAdapter
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent

class SecondaryGoalsExamFragment : BaseAppFragment() {

    private lateinit var binding: FragmentSecondaryGoalsExamBinding
    private lateinit var signInViewModel: SignInViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_secondary_goals_exam, container, false
        )
        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AddGoalsExamsActivity.secondaryGoalExams.clear()
        setExamRecyclerView(view)
        setBackClickListener()
        setDoneClickListener()
    }

    private fun setExamRecyclerView(view: View) {

        val goalExamAdapter = SecondaryGoalExamAdapter(view.context)
        makeGridCenter()

        goalExamAdapter.setData(DataManager.instance.getPrimaryExamGoals(AddGoalsExamsActivity.secondaryGoalCode))
        binding.rvSecondaryGoalExam.adapter = goalExamAdapter

        goalExamAdapter.onItemClick = { exam ->
        }
        /*goalExamAdapter.onFocusChange = { goal ->
            if (!goal.supported) {
                binding.textGoalNotSupported.visibility = View.VISIBLE
                binding.textGoalNotSupported.text =
                    "${resources.getString(R.string.text_goal_not_supported)} ${goal.name}"
            } else {
                binding.textGoalNotSupported.visibility = View.GONE
            }
        }*/
    }

    fun makeGridCenter() {
        val layoutManager = FlexboxLayoutManager(context)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.CENTER
        layoutManager.alignItems = AlignItems.CENTER
        binding.rvSecondaryGoalExam.layoutManager = layoutManager

    }

    private fun setBackClickListener() {
        binding.btnBack.setOnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        }
        binding.btnBack.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    SegmentUtils.trackProfileExamsSecondaryGoalDoneFocus(
                        AddGoalsExamsActivity.secondaryGoalExams
                    )
                }
            }
        }
    }

    private fun setDoneClickListener() {
        binding.btnDone.setOnClickListener {
            // update data to api
            SegmentUtils.trackProfileExamsSecondaryGoalDoneClick(
                AddGoalsExamsActivity.secondaryGoalExams,
                AddGoalsExamsActivity.secondaryGoal
            )
            updateGoalExams()

        }
        binding.btnDone.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    SegmentUtils.trackProfileExamsSecondaryGoalDoneFocus(
                        AddGoalsExamsActivity.secondaryGoalExams
                    )
                }
            }
        }
    }

    private fun isValidExams(): Boolean {
        return AddGoalsExamsActivity.secondaryGoalExams.isNotEmpty()
    }

    fun updateGoalExams() {
        val model = UpdateProfileRequest()
        model.secondaryGoal = AddGoalsExamsActivity.secondaryGoalCode
        model.secondaryExamsPreparingFor = AddGoalsExamsActivity.secondaryGoalExams.joinToString()

        makeLog(
            "results : ${AddGoalsExamsActivity.primaryGoalCode} " +
                    " ${AddGoalsExamsActivity.primaryGoalExamCode}  " +
                    " ${AddGoalsExamsActivity.secondaryGoalCode} " +
                    " ${AddGoalsExamsActivity.secondaryGoalExams.joinToString()} "
        )

        updateProfile(model)
    }

    fun updateProfile(model: UpdateProfileRequest) {
        showProgress()
        signInViewModel.updateProfile(model, object : BaseViewModel.APICallBacks<UpdateProfileRes> {
            override fun onSuccess(model: UpdateProfileRes?) {
                hideProgress()
                if (model != null) {
                    if (model.success) {
                        profileUpdated(model)
                    } else {
                        failedToUpdateProfile(model.errors)
                    }
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(activity, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            updateProfile(model)
                        }

                        override fun onDismiss() {

                        }
                    })

                } else {
                    Utils.showToast(activity!!, error)
                }

            }
        })
    }

    private fun profileUpdated(model: UpdateProfileRes) {
        UserData.updateUserData(model, AddGoalsExamsActivity.userId)
        AppConstants.ISPROFILEUPDATED = true
        (activity as AddGoalsExamsActivity).finish()
    }

    private fun failedToUpdateProfile(errors: Errors?) {
        if (errors != null) {
            val email = errors.email
            val profile = errors.profile
            val mobile = errors.mobile

            if (profile.equals("Nothing to change")) {
                (activity as AddGoalsExamsActivity).finish()
            } else if (mobile?.contains("has already been taken")!!) {
                showToast(getString(R.string.edit_profile_mobile_exist))
                (activity as AddGoalsExamsActivity).finish()
            }
        }
    }


}