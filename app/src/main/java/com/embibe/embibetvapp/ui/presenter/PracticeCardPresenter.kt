package com.embibe.embibetvapp.ui.presenter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import androidx.core.content.res.ResourcesCompat
import androidx.leanback.widget.BaseCardView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.fragment.practice.PracticeRowsFragment

class PracticeCardPresenter(context: Context) :
    AbstractCardPresenter<BaseCardView>(context) {
    private var cardView: BaseCardView? = null
    override fun onCreateView(): BaseCardView {
        val cardView =
            BaseCardView(context, null, R.style.SideInfoCardStyle)
        cardView?.addView(
            LayoutInflater.from(context).inflate(
                R.layout.card_practice,
                null
            )
        )

        val viewAlpha = cardView?.findViewById<View>(R.id.view_focus)

        cardView!!.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_focused_alpha_value
                )
                if (PracticeRowsFragment.rowFocusedPosition > 0)
                    cardView!!.setPadding(50, 0, 70, 0)
                else
                    cardView.setPadding(20, 0, 60, 0)

            } else {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_unfocused_alpha_value
                )
                cardView!!.setPadding(0, 0, 0, 0)
            }
        }

        cardView?.isFocusable = true

        return cardView!!
    }

    override fun onBindViewHolder(item: Any, cardView: BaseCardView) {

        var card = item as Content
        val ivThumbnail: ImageView = cardView.findViewById(R.id.iv_thumbnail)
        val ivCategory: ImageView = cardView.findViewById(R.id.ivCategory)

/*        val width = context.resources
            .getDimension(R.dimen.video_card_width).toInt()
        val height = context.resources
            .getDimension(R.dimen.video_card_image_height).toInt()*/
        val requestOptions = RequestOptions().transform(RoundedCorners(8))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(context)
            .applyDefaultRequestOptions(requestOptions)
            .load(card.thumb)
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.practice_placeholder)
            .into(ivThumbnail)

        Glide.with(context)
            .load(R.drawable.ic_practice_symbol)
            .into(ivCategory)

    }

}