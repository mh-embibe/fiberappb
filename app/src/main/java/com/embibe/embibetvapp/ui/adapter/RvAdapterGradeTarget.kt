package com.embibe.embibetvapp.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.RowItemGradeTargetBinding
import com.embibe.embibetvapp.utils.ContantUtils
import com.embibe.embibetvapp.utils.FontHelper

class RvAdapterGradeTarget() : RecyclerView.Adapter<RvAdapterGradeTarget.VHGradeTarget>() {

    private lateinit var binding: RowItemGradeTargetBinding
    private var selectedItem = -1
    var onItemClick: ((String) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHGradeTarget {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_item_grade_target,
            parent,
            false
        )
        return VHGradeTarget(binding)
    }

    override fun getItemCount(): Int {
        return ContantUtils.targetGradeData.size
    }

    override fun onBindViewHolder(holder: VHGradeTarget, position: Int) {
        holder.bind(position)
    }

    //ViewHolder
    inner class VHGradeTarget(var binding: RowItemGradeTargetBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.tvGrade.text = ContantUtils.targetGradeData[position]
            binding.tvPoint.text = ContantUtils.targetPointData[position]
            binding.tvMarksRange.text = ContantUtils.targetMarksData[position]
            if (selectedItem == adapterPosition) binding.clItem.requestFocus()

            if (selectedItem == adapterPosition) {
                binding.clItem.setBackgroundResource(R.drawable.bg_achieve_potential_selected_selector)
                binding.tvGrade.setTextColor(Color.parseColor("#000000"))
                binding.tvPoint.setTextColor(Color.parseColor("#000000"))
                binding.tvMarksRange.setTextColor(Color.parseColor("#000000"))
            } else {
                binding.clItem.setBackgroundResource(R.drawable.bg_acheive_potential_selector)
                binding.tvGrade.setTextColor(Color.parseColor("#ffffff"))
                binding.tvPoint.setTextColor(Color.parseColor("#ffffff"))
                binding.tvMarksRange.setTextColor(Color.parseColor("#ffffff"))
            }
            itemView.setOnClickListener {
                onItemClick?.invoke(ContantUtils.targetGradeData[position])

                if (selectedItem != adapterPosition) {
                    selectedItem = adapterPosition
                    notifyDataSetChanged()
                } else {
                    selectedItem = -1
                    notifyDataSetChanged()
                }
            }
        }
    }
}