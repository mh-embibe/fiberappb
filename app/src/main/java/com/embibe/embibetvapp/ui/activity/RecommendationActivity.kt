package com.embibe.embibetvapp.ui.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.BOOK_ID
import com.embibe.embibetvapp.constant.AppConstants.CONTENT_TYPE
import com.embibe.embibetvapp.constant.AppConstants.DURATION_LENGTH
import com.embibe.embibetvapp.constant.AppConstants.FORMAT_ID
import com.embibe.embibetvapp.constant.AppConstants.IS_EMBIBE_SYLLABUS
import com.embibe.embibetvapp.constant.AppConstants.LEARNMAP_CODE
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_FORMAT_NAME
import com.embibe.embibetvapp.constant.AppConstants.LEARN_PATH_NAME
import com.embibe.embibetvapp.constant.AppConstants.SLIDE_COUNT
import com.embibe.embibetvapp.constant.AppConstants.TOPIC_LEARN_PATH
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_AUTHORS
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CONCEPT_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_DESCRIPTION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SOURCE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SUBJECT
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TITLE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TOTAL_DURATION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_URL
import com.embibe.embibetvapp.databinding.ActivityRecommendationBinding
import com.embibe.embibetvapp.ui.fragment.videoPlayer.RecommendationFragment

class RecommendationActivity : FragmentActivity() {

    private var book_id: String? = null
    private lateinit var binding: ActivityRecommendationBinding
    private var videoTotalDuration: String? = null
    private var videoConceptId: String? = null
    private var videoUrl: String? = null
    private var videoTitle: String? = null
    private var videoDescription: String? = null
    private var videoSubject: String? = null
    private var videoChapter: String? = null
    private var videoAuthors: String? = null
    private var videoType: String? = null
    private var videoId: String? = null
    private var contentType: String? = null
    private var duration: Int = 0
    private var slideCount: Int = 0
    private var learningMap: String = ""
    private var isEmbibeSyllabus: Boolean = false
    private var topicLearnPath: String? = ""
    private var learnPathName: String = ""
    private var learnPathFormatName: String = ""
    private var format_id: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_recommendation)
        loadData()
        val fragment = RecommendationFragment()
        fragment.arguments = buildBundle()
        supportFragmentManager.beginTransaction().replace(binding.flRecommendation.id, fragment)
            .commit()
    }

    private fun buildBundle(): Bundle {
        val bundle = Bundle()
        bundle.putString(VIDEO_URL, videoUrl)
        bundle.putString(VIDEO_ID, videoId)
        bundle.putString(VIDEO_CONCEPT_ID, videoConceptId)
        bundle.putString(CONTENT_TYPE, contentType)
        bundle.putString(VIDEO_TITLE, videoTitle)
        bundle.putString(VIDEO_DESCRIPTION, videoDescription)
        bundle.putString(VIDEO_SUBJECT, videoSubject)
        bundle.putString(VIDEO_CHAPTER, videoChapter)
        bundle.putString(VIDEO_AUTHORS, videoAuthors)
        bundle.putString(VIDEO_SOURCE, videoType)
        bundle.putString(VIDEO_TOTAL_DURATION, videoType)
        bundle.putInt(DURATION_LENGTH, duration)
        bundle.putInt(SLIDE_COUNT, slideCount)
        bundle.putString(LEARNMAP_CODE, learningMap)
        bundle.putBoolean(IS_EMBIBE_SYLLABUS, isEmbibeSyllabus)
        bundle.putString(BOOK_ID, book_id)
        bundle.putString(TOPIC_LEARN_PATH, topicLearnPath)
        bundle.putString(FORMAT_ID, format_id)
        bundle.putString(LEARN_PATH_NAME, learnPathName)
        bundle.putString(LEARN_PATH_FORMAT_NAME, learnPathFormatName)
        return bundle
    }

    private fun loadData() {
        videoUrl = if (intent.hasExtra(AppConstants.COOBO_URL)) {
            intent.getStringExtra(AppConstants.COOBO_URL)
        } else
            intent.getStringExtra(VIDEO_URL)
        if (intent.hasExtra(DURATION_LENGTH))
            duration = intent.getIntExtra(DURATION_LENGTH, 0)
        if (intent.hasExtra(SLIDE_COUNT))
            slideCount = intent.getIntExtra(SLIDE_COUNT, 0)
        videoTitle = intent.getStringExtra(VIDEO_TITLE)
        videoDescription = intent.getStringExtra(VIDEO_DESCRIPTION)
        videoSubject = intent.getStringExtra(VIDEO_SUBJECT)
        videoChapter = intent.getStringExtra(VIDEO_CHAPTER)
        videoAuthors = intent.getStringExtra(VIDEO_AUTHORS)
        videoType = intent.getStringExtra(VIDEO_SOURCE)
        videoId = intent.getStringExtra(VIDEO_ID)
        videoConceptId = intent.getStringExtra(VIDEO_CONCEPT_ID)
        contentType = intent.getStringExtra(CONTENT_TYPE)
        if (intent.hasExtra(LEARNMAP_CODE))
            learningMap = intent.getStringExtra(LEARNMAP_CODE)
        if (intent.hasExtra(IS_EMBIBE_SYLLABUS))
            isEmbibeSyllabus = intent.getBooleanExtra(IS_EMBIBE_SYLLABUS, false)
        if (intent.hasExtra(BOOK_ID))
            book_id = intent.getStringExtra(BOOK_ID)
        if (intent.hasExtra(VIDEO_TOTAL_DURATION))
            videoTotalDuration = intent.getStringExtra(VIDEO_TOTAL_DURATION)
        if (intent.hasExtra(TOPIC_LEARN_PATH))
            topicLearnPath = intent.getStringExtra(TOPIC_LEARN_PATH)
        if (intent.hasExtra(LEARN_PATH_NAME))
            learnPathName = intent.getStringExtra(LEARN_PATH_NAME)
        if (intent.hasExtra(LEARN_PATH_FORMAT_NAME))
            learnPathFormatName = intent.getStringExtra(LEARN_PATH_FORMAT_NAME)
        if (intent.hasExtra(FORMAT_ID))
            format_id = intent.getStringExtra(FORMAT_ID)
    }
}