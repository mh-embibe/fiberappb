package com.embibe.embibetvapp.ui.binding

import android.animation.ObjectAnimator
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("android:imgSrc")
fun bindImageView(view: ImageView, img: String?) {
    if (!img.isNullOrEmpty()) {
        Glide.with(view.context).load(img).into(view)
    }
}

@BindingAdapter("bindProgress")
fun ProgressBar.bindProgress(progress: Double) {
    if (progress != null && progress > 0) {
        ObjectAnimator.ofInt(this, "progress", progress.toInt())
            .setDuration(2000)
            .start()
    } else {
        ObjectAnimator.ofInt(this, "progress", 0)
            .setDuration(2000)
            .start()
    }
}

