package com.embibe.embibetvapp.ui.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.embibe.app.utils.Resource
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.model.UpdateProfileRequest
import com.embibe.embibetvapp.model.adduser.AddUserRequest
import com.embibe.embibetvapp.model.adduser.AddUserResponse
import com.embibe.embibetvapp.model.auth.SignUpResponse
import com.embibe.embibetvapp.model.auth.SignupVerifyOtpRequest
import com.embibe.embibetvapp.model.auth.SignupVerifyOtpResponse
import com.embibe.embibetvapp.model.auth.login.LoginResponse
import com.embibe.embibetvapp.model.auth.otp.AuthOTP
import com.embibe.embibetvapp.model.auth.signup.SignupSendOtpRequest
import com.embibe.embibetvapp.model.auth.signup.SignupSendOtpResponse
import com.embibe.embibetvapp.model.auth.signup.UserExistResponse
import com.embibe.embibetvapp.model.goals.GoalsExamsRes
import com.embibe.embibetvapp.model.updateProfile.UpdateProfileRes
import com.embibe.embibetvapp.network.repo.SignUpRepository
import com.embibe.embibetvapp.utils.Status

class SignInViewModel : BaseViewModel() {
    private var repo = SignUpRepository()

    //mutable
    val selectAvatarLiveData: MutableLiveData<String> = MutableLiveData()
    val defaultAvatarLiveData: MutableLiveData<Int> = MutableLiveData()
    val setGoalLiveData: MutableLiveData<Int> = MutableLiveData()
    val addUserLiveData: MutableLiveData<Resource<AddUserResponse>> = MutableLiveData()
    val userExistLiveData: MutableLiveData<Resource<UserExistResponse>> = MutableLiveData()
    val examGoalLiveData: MutableLiveData<Resource<Any>> = MutableLiveData()


    @SuppressLint("CheckResult")
    fun getUserExist(id: String) {
        userExistLiveData.value = Resource(Status.LOADING)
        makeApiCallLiveData(repo.getUserExist(id), userExistLiveData)
    }

    fun getUserExist(id: String, callbacks: APICallBacks<UserExistResponse>) {
        safeApi(repo.getUserExist(id), callbacks)
    }

    fun updateProfile(model: UpdateProfileRequest, callbacks: APICallBacks<UpdateProfileRes>) {
        safeApi(repo.updateProfile(model), callbacks)
    }

    fun getGoalsExams(callbacks: APICallBacks<GoalsExamsRes>) {
        safeApi(repo.getGoalsExams(), callbacks)
    }

    fun signUp(signUpRequest: AuthOTP, callbacks: APICallBacks<SignUpResponse>) {
        safeApi(repo.getSignUpData(signUpRequest), callbacks)
    }

    fun sendOtp(
        sendOtpRequest: SignupSendOtpRequest,
        callbacks: APICallBacks<SignupSendOtpResponse>
    ) {
        safeApi(repo.getSignUpSendOtp(sendOtpRequest), callbacks)
    }

    fun authOTPSignIn(authOtp: AuthOTP, callbacks: APICallBacks<LoginResponse>) {
        isTokenRequired = true
        safeApi(repo.authOTPSignIn(authOtp), callbacks)
    }

    fun getLinkedProfilesApi(callbacks: APICallBacks<LoginResponse>) {
        safeApi(repo.getLinkedProfiles(), callbacks)
    }

    fun verifyOtp(
        verifyOtpRequest: SignupVerifyOtpRequest,
        callbacks: APICallBacks<SignupVerifyOtpResponse>
    ) {
        safeApi(repo.getVerifyOTP(verifyOtpRequest), callbacks)
    }

    fun addUser(userId: String, addUserRequest: AddUserRequest) {
        addUserLiveData.value = Resource(Status.LOADING)
        makeApiCallLiveData(repo.getAddUser(userId, addUserRequest), addUserLiveData)
    }

    fun setAvatar(position: String) {
        selectAvatarLiveData.value = position
    }

    fun setDefaultAvatar(position: Int) {
        defaultAvatarLiveData.value = position
    }

    fun setGoal(position: Int) {
        setGoalLiveData.value = position
    }

}