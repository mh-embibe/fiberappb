package com.embibe.embibetvapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ActivitySplashBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.goals.GoalsExamsRes
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper


class SplashActivity : BaseFragmentActivity() {

    private lateinit var binding: ActivitySplashBinding
    lateinit var signInViewModel: SignInViewModel
    private var isChildIdNotFound: Boolean = false
    var pref = PreferenceHelper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

        // binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        getGoalsExamsAsync()
        initCode()
//        removeRowsAndItemsPrefs()
    }

    private fun removeRowsAndItemsPrefs() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        val editor = sharedPref.edit()
        editor.remove(getString(R.string.last_selected_practice_row_item))
        editor.remove(getString(R.string.last_selected_practice_row))
        editor.remove(getString(R.string.last_selected_learn_row_item))
        editor.remove(getString(R.string.last_selected_learn_row))
        editor.remove(getString(R.string.last_selected_test_row))
        editor.remove(getString(R.string.last_selected_test_row_item))
        editor.remove(getString(R.string.last_selected_dashboard_row_item))
        editor.remove(getString(R.string.last_selected_dashboard_row))
        editor.remove(getString(R.string.last_selected_achieve_row))
        editor.remove(getString(R.string.last_selected_achieve_row_item))
        editor.apply()
    }

    private fun initCode() {
        if (pref[AppConstants.IS_LOGIN, false]) {
            if (UserData.getChildForGoalSet(UserData.getLinkedProfileList()!!).isEmpty()) {
                isChildIdNotFound = true
            } else {
                isChildIdNotFound = false
                startNextActivity()
            }
        } else {
            startActivity(Intent(this@SplashActivity, SignInActivity::class.java))
            this.finish()
        }
        SegmentUtils.trackSplashLoadStart()
    }

    fun getGoalsExamsAsync() {
        signInViewModel.getGoalsExams(object : BaseViewModel.APICallBacks<GoalsExamsRes> {
            override fun onSuccess(model: GoalsExamsRes?) {
                if (model != null) {
                    DataManager.instance.setGoalsExamsList(
                        (model.data ?: arrayListOf())
                    )

                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {

            }
        })
    }


    private fun startNextActivity() {
        SegmentUtils.trackSplashLoadEnd()
        if (isChildIdNotFound) {
            startActivity(Intent(this, AddGoalsActivity::class.java))
            this.finish()
        } else {
            if (pref[AppConstants.IS_LOGIN, false]) {
                val mainIntent = Intent(this@SplashActivity, UserSwitchActivity::class.java)
                startActivity(mainIntent)
                this.finish()
            } else {
                startActivity(Intent(this@SplashActivity, SignInActivity::class.java))
                this.finish()

            }
        }
    }

}
