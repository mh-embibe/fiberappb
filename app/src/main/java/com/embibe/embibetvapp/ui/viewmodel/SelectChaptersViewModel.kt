package com.embibe.embibetvapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import com.embibe.app.utils.Resource
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.model.createtest.examconfig.ChapterDetails
import com.embibe.embibetvapp.model.createtest.examconfig.SubjectDetails
import com.embibe.embibetvapp.network.repo.CreateTestRepository
import com.embibe.embibetvapp.utils.Status
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class SelectChaptersViewModel
    : BaseViewModel() {
    var chapterResponse: MutableLiveData<Resource<Any>> = MutableLiveData()
    var repo = CreateTestRepository()
    var chapterDetailsLiveData: MutableLiveData<MutableList<ChapterDetails>> = MutableLiveData()
    var subjectResponse: MutableLiveData<ArrayList<SubjectDetails>> = MutableLiveData()
    var selectedChapterDetailsLiveData: MutableLiveData<MutableList<ChapterDetails>> =
        MutableLiveData()

    fun getChapterDetailsBasedOnSubjectCode(id: String) {
        repo.getChapterDetailsBasedOnSubjectCode(id, chapterDetailsLiveData.value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                chapterResponse.value =
                    Resource(Status.SUCCESS, it)
            }, {
                chapterResponse.value =
                    Resource(Status.ERROR, null, it.message)
            })

    }

    fun getChapterDetailsFromExamConfig(examConfigDetails: ArrayList<ChapterDetails>?) {
        chapterDetailsLiveData.postValue(examConfigDetails!!.toMutableList())

    }

    fun getChapterResponseDetails(): MutableLiveData<Resource<Any>> {
        return chapterResponse
    }

    fun updateSelectedChaptersCount(
        subjectCode: String?,
        selectedSubjectDetails: ArrayList<SubjectDetails>?
    ) {
        repo.getSelectedChapterDetails(subjectCode!!, chapterDetailsLiveData.value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                var subjectResponseDetail =
                    selectedSubjectDetails?.filter { it.subjectCode == subjectCode }!![0]
                var indexOf = selectedSubjectDetails.indexOf(subjectResponseDetail)
                var updatedSubjectDetails = repo.updateSubjectDetails(subjectResponseDetail, it)
                selectedSubjectDetails.set(indexOf, updatedSubjectDetails)
                subjectResponse.value = selectedSubjectDetails
            }, {

            })
    }

    fun getSubjectDetails(): MutableLiveData<ArrayList<SubjectDetails>> {
        return subjectResponse
    }

    fun getSelectedChapterDetails() {
        repo.getSelectedChapterDetails(chapterDetailsLiveData.value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                selectedChapterDetailsLiveData.value = it.toMutableList()
            }, {

            })


    }

    fun selectedChapterDetails(): MutableLiveData<MutableList<ChapterDetails>> {
        return selectedChapterDetailsLiveData
    }


}