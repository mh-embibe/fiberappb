package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemExamsBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.adduser.Exam
import com.embibe.embibetvapp.utils.SegmentUtils

class SwitchExamAdapter(var context: Context) :
    RecyclerView.Adapter<SwitchExamAdapter.ExamViewHolder>() {

    private var examsList = ArrayList<Exam>()
    var onItemClick: ((Exam) -> Unit)? = null
    var onFocusChange: ((Exam, Boolean) -> Unit)? = null
    var selectedItem = -1
    private var currentPosition = -1
    private var currentExamCode = UserData.getExamCode()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExamViewHolder {

        val binding: ItemExamsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_exams, parent, false
        )
        return ExamViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return examsList.size
    }

    override fun onBindViewHolder(holder: ExamViewHolder, position: Int) {
        currentPosition = position
        holder.bind(examsList[position])

        when (selectedItem) {
            position -> {
                holder.binding.textGoalExam.background =
                    context.getDrawable(R.drawable.shape_rec_white_border_with_radius)
                holder.binding.textGoalExam.setTextColor(context.getColor(R.color.email_font_color))
            }
        }
    }

    fun setData(data: ArrayList<Exam>) {
        selectedItem = -1
        examsList = data
        //for default selection
        examsList.forEachIndexed { index, exam ->
            when (currentExamCode) {
                exam.code -> selectedItem = index
            }
        }
        notifyDataSetChanged()
    }


    inner class ExamViewHolder(var binding: ItemExamsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Exam) {
            binding.exam = item
            binding.executePendingBindings()

            binding.textGoalExam.background = context.getDrawable(R.drawable.bg_button_goals)
            binding.textGoalExam.setTextColor(context.getColor(R.color.white))


            itemView.setOnClickListener {
                onItemClick?.invoke(examsList[adapterPosition])
                currentPosition = adapterPosition
                SegmentUtils.trackProfileExamPrimaryGoalValueCLick(
                    examsList[adapterPosition].name,
                    adapterPosition,
                    ""
                )
                when {
                    selectedItem != adapterPosition -> {
                        val previousItem = selectedItem
                        selectedItem = adapterPosition
                        notifyItemChanged(previousItem)
                    }
                    else -> selectedItem = -1
                }
                notifyItemChanged(adapterPosition)
            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                when {
                    adapterPosition != -1 -> {
                        onFocusChange?.invoke(examsList[adapterPosition], hasFocus)
                        SegmentUtils.trackProfileExamPrimaryGoalValueFocus(
                            examsList[adapterPosition].name,
                            adapterPosition
                        )
                    }
                }
            }

        }
    }
}
