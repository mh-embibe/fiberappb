package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemSelectChaptersBinding
import com.embibe.embibetvapp.model.createtest.examconfig.ChapterDetails
import java.util.regex.Matcher
import java.util.regex.Pattern

class RvChapterAdapter(var context: Context, var clickListner: OnClickListner) :
    RecyclerView.Adapter<RvChapterAdapter.ViewHolder>() {

    private var adapterList: MutableList<ChapterDetails>? = null

    val TAG = this.javaClass.name

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemSelectChaptersBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.item_select_chapters,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(adapterList!![position], holder)
    }

    override fun getItemCount(): Int = adapterList!!.size

    fun setData(list: List<ChapterDetails>) {
        adapterList = list.toMutableList()
        notifyDataSetChanged()
    }

    fun clearList() {
        adapterList!!.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(var binding: ItemSelectChaptersBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            item: ChapterDetails,
            holder: ViewHolder
        ) {

            binding.tvChapterDetails.text = capitalize(item.chapterName!!)


            if (!item.selectedState!!) {
                binding.flMain.setBackgroundResource(R.drawable.bg_select_chapter_unselect)
                binding.tvChapterDetails.isChecked = false
                binding.tvChapterDetails.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
            } else {
                binding.flMain.setBackgroundResource(R.drawable.bg_select_chapter_select)
                binding.tvChapterDetails.isChecked = true
                binding.tvChapterDetails.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.chapter_black_color
                    )
                )
            }

            binding.rlMain.setOnClickListener {
                val value: Boolean = binding.tvChapterDetails.isChecked
                if (!value) {
                    binding.flMain.setBackgroundResource(R.drawable.bg_select_chapter_select)
                    binding.tvChapterDetails.isChecked = true
                    binding.tvChapterDetails.setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.chapter_black_color
                        )
                    )
                    clickListner.selectedChapter(adapterPosition, item.subjectName, item)
                } else {
                    binding.flMain.setBackgroundResource(R.drawable.bg_select_chapter_unselect)
                    binding.tvChapterDetails.isChecked = false
                    binding.tvChapterDetails.setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.white
                        )
                    )
                    clickListner.unselectedChapter(adapterPosition, item.subjectName)

                }
            }

        }

    }

    interface OnClickListner {
        fun unselectedChapter(adapterPosition: Int, subjectCode: String?)
        fun selectedChapter(
            adapterPosition: Int,
            subjectCode: String?,
            item: ChapterDetails
        )
    }

    private fun capitalize(capString: String): String? {
        val capBuffer = StringBuffer()
        val capMatcher: Matcher =
            Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString)
        while (capMatcher.find()) {
            capMatcher.appendReplacement(
                capBuffer,
                capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase()
            )
        }
        return capMatcher.appendTail(capBuffer).toString()
    }


}