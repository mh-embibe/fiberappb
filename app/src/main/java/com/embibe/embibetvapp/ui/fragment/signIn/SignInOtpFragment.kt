package com.embibe.embibetvapp.ui.fragment.signIn

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentSigninOtpBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.auth.login.LoginResponse
import com.embibe.embibetvapp.model.auth.otp.AuthOTP
import com.embibe.embibetvapp.model.auth.signup.SignupSendOtpRequest
import com.embibe.embibetvapp.model.auth.signup.SignupSendOtpResponse
import com.embibe.embibetvapp.ui.activity.AddGoalsActivity
import com.embibe.embibetvapp.ui.activity.UserSwitchActivity
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibejio.coreapp.preference.PreferenceHelper
import com.google.gson.internal.LinkedTreeMap
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class SignInOtpFragment : BaseAppFragment() {

    private lateinit var binding: FragmentSigninOtpBinding
    private lateinit var signInViewModel: SignInViewModel
    private var mobleNumber: String? = null
    private var mobileOrEmail: String? = null
    private var email: String? = null
    private var password: String? = null
    private var userType: String? = null
    private var profileName: String? = null
    private var userName: String? = null
    private var isFirst: Boolean = false
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_signin_otp, container, false)
        mobileOrEmail = requireArguments().getString("mobileOrEmail")
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        clearAllOTP()
        setTextWatcher()
        clickListeners(view)
        clearOTPOnBtnClick()

        when (Utils.getInputType(mobileOrEmail!!)) {
            1 -> mobleNumber = mobileOrEmail
            2 -> email = mobileOrEmail
        }
        setResisteredMobileEmailOnView()
        callSendOtpApi(view)

    }

    private fun setResisteredMobileEmailOnView() {
        if (mobleNumber != null) {
            binding.tv2.text =
                "Enter the OTP sent on your \nregistered mobile number\n" + mobleNumber
        } else if (email != null) {
            binding.tv2.text = "Enter the OTP sent on your \nregistered email ID\n" + email
        }
    }

    private fun clearAllOTP() {
        binding.etOtp1.text.clear()
        binding.etOtp2.text.clear()
        binding.etOtp3.text.clear()
        binding.etOtp4.text.clear()
        binding.etOtp5.text.clear()
        binding.etOtp6.text.clear()
        binding.etOtp1.requestFocus()
        Handler().postDelayed({
            openSoftKeyboard(context, binding.etOtp1)
        }, 500)

    }

    private fun clearOTPOnBtnClick() {

    }

    private fun callSendOtpApi(view: View) {
        val model = SignupSendOtpRequest(
            mobleNumber, email, "embibe_fiber_app", "600", "2"
        )

        showProgress()
        signInViewModel.sendOtp(model, object : BaseViewModel.APICallBacks<SignupSendOtpResponse> {
            override fun onSuccess(model: SignupSendOtpResponse?) {
                hideProgress()
                if (model?.result.equals("OTP sent.")) {
                    updateResult(view)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                retrySendOTPApi(code, error, view)
            }
        })
    }

    private fun retrySendOTPApi(code: Int, error: String, view: View) {
        if (Utils.isApiFailed(code)) {
            Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                override fun onRetry(msg: String) {
                    callSendOtpApi(view)
                }

                override fun onDismiss() {
                }
            })
        } else {
            /*update error msg in ui*/
        }
    }

    private fun updateResult(view: View) {
        /*OTP sent successfully!*/
        Toast.makeText(
            view.context,
            view.context.resources.getString(R.string.otp_sent_successfully),
            Toast.LENGTH_SHORT
        ).show()

    }

    private fun clickListeners(view: View) {
        binding.btnResendOtp.setOnClickListener {
            SegmentUtils.trackResendOtpBtnClick()
            clearAllOTP()
            callSendOtpApi(view)
        }

        binding.btnVerify.setOnClickListener {
            SegmentUtils.trackLoginVerifyOtpBtnClick()
            if (getEnterOtpText().length < 6) {
                Utils.showToast(
                    requireContext(),
                    requireActivity().getString(R.string.enter_corrent_otp)
                )
            } else {
                isFirst = true
                val model = AuthOTP()
                model.fiber_user = true
                model.otp = getEnterOtpText()
                model.login = mobileOrEmail
                binding.tvWrongMessage.visibility = View.GONE
                /*call the api*/
                authenticateOTP(model)
            }
        }

        binding.btnClearOtp.setOnClickListener {
            SegmentUtils.tracClearOtpClick()
            binding.etOtp1.text.clear()
            binding.etOtp2.text.clear()
            binding.etOtp3.text.clear()
            binding.etOtp4.text.clear()
            binding.etOtp5.text.clear()
            binding.etOtp6.text.clear()
            binding.etOtp1.requestFocus()
        }
    }

    private fun authenticateOTP(authOTPModel: AuthOTP) {
        makeLog("Authentication called")
        showProgress()
        signInViewModel.authOTPSignIn(authOTPModel,
            object : BaseViewModel.APICallBacks<LoginResponse> {
                override fun onSuccess(model: LoginResponse?) {

                    if (model != null && model.success) {
                        makeLog("Authentication Success")
                        Toast.makeText(
                            view!!.context,
                            view!!.context.resources.getString(R.string.otp_verified_successfully),
                            Toast.LENGTH_SHORT
                        ).show()
                        updateSignInResponse(model)
                    } else {
                        hideProgress()
                        val error = model!!.error!! as LinkedTreeMap<*, *>
                        if (error.keys.contains("login")) {
                            Toast.makeText(
                                view!!.context,
                                view!!.context.resources.getString(R.string.user_not_registered),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            Toast.makeText(
                                view!!.context,
                                view!!.context.resources.getString(R.string.otp_verification_failed),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        makeLog("login api failed")
                        updateEvent("failed", "", "")
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    hideProgress()
                    makeLog("login api error $error")
                    updateEvent("failed", "", "")
                    retryAuthenticateOTP(code, error, authOTPModel, msg)
                }
            })
    }

    private fun retryAuthenticateOTP(
        code: Int,
        error: String,
        authOTPModel: AuthOTP,
        otpError: String
    ) {
        when {
            code == 401 -> {
                makeLog("authentication api error $error")
                binding.tvWrongMessage.visibility = View.VISIBLE
                if (otpError.equals("is invalid", true)) {
                    binding.tvWrongMessage.text = "You have entered the wrong OTP."
                } else {
                    binding.tvWrongMessage.text = "The OTP is expired. Please Try again/Resend OTP"
                }
                setBackgroundHighlight()
            }
            Utils.isApiFailed(code) -> {
                Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                    override fun onRetry(msg: String) {
                        authenticateOTP(authOTPModel)
                    }

                    override fun onDismiss() {}
                })
            }
            else -> {
                showToast(error)
            }
        }
    }

    private fun updateSignInResponse(response: LoginResponse) {
        if (response.success) {
            doAsync {
                updateLoginPreferences(response)
                updateEvent("success", response.userId.toString(), response.userType!!)
                uiThread {
                    hideProgress()
                    if (UserData.getChildForGoalSet(UserData.getLinkedProfileList()!!).isEmpty()) {
                        makeLog("trackBug MoveToAddGoalExams")
                        startActivity(Intent(activity, AddGoalsActivity::class.java))
                        requireActivity().finish()
                    } else {
                        val mainIntent = Intent(activity, UserSwitchActivity::class.java)
                        startActivity(mainIntent)
                        requireActivity().finish()
                    }
                }
            }
        } else {
            hideProgress()
            makeLog("login api failed")
            updateEvent("failed", "", "")
        }
    }

    private fun updateLoginPreferences(response: LoginResponse) {
        UserData.updateLinkedUser(response)
        PreferenceHelper().put(AppConstants.USER_TYPE, response.userType!!)
    }

    private fun updateEvent(status: String, userId: String, userType: String) {

        if (status == "success") {
            SegmentUtils.loginSuccessEvents(mobileOrEmail!!, "", userId, userType, status)
        } else {
            SegmentUtils.loginFailureEvents(mobileOrEmail!!, "", status)
        }
    }

    private fun setTextWatcher() {
        binding.etOtp1.addTextChangedListener(GenericTextWatcher(binding.etOtp1))
        binding.etOtp2.addTextChangedListener(GenericTextWatcher(binding.etOtp2))
        binding.etOtp3.addTextChangedListener(GenericTextWatcher(binding.etOtp3))
        binding.etOtp4.addTextChangedListener(GenericTextWatcher(binding.etOtp4))
        binding.etOtp5.addTextChangedListener(GenericTextWatcher(binding.etOtp5))
        binding.etOtp6.addTextChangedListener(GenericTextWatcher(binding.etOtp6))
    }

    private fun getEnterOtpText(): String {
        return binding.etOtp1.text.toString() + binding.etOtp2.text.toString() + binding.etOtp3.text.toString() + binding.etOtp4.text.toString() + binding.etOtp5.text.toString() + binding.etOtp6.text.toString()
    }


    private fun setBackgroundHighlight() {
        binding.etOtp1.background =
            requireContext().getDrawable(R.drawable.shape_rec_white_border_maroon)
        binding.etOtp2.background =
            requireContext().getDrawable(R.drawable.shape_rec_white_border_maroon)
        binding.etOtp3.background =
            requireContext().getDrawable(R.drawable.shape_rec_white_border_maroon)
        binding.etOtp4.background =
            requireContext().getDrawable(R.drawable.shape_rec_white_border_maroon)
        binding.etOtp5.background =
            requireContext().getDrawable(R.drawable.shape_rec_white_border_maroon)
        binding.etOtp6.background =
            requireContext().getDrawable(R.drawable.shape_rec_white_border_maroon)
    }

    private fun setBackgroundNormal() {
        binding.etOtp1.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp2.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp3.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp4.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp5.background = requireContext().getDrawable(R.drawable.shape_rec_white)
        binding.etOtp6.background = requireContext().getDrawable(R.drawable.shape_rec_white)
    }


    inner class GenericTextWatcher(var view: View) : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            if (binding.tvWrongMessage.visibility == View.VISIBLE) binding.tvWrongMessage.visibility =
                View.GONE
            binding.tvWrongMessage.text = ""
            setBackgroundNormal()
            val text = editable.toString()
            when (view.id) {
                R.id.et_otp_1 -> if (text.length == 1) binding.etOtp2.requestFocus()
                R.id.et_otp_2 -> if (text.length == 1) binding.etOtp3.requestFocus() else if (text.isEmpty()) binding.etOtp1.requestFocus()
                R.id.et_otp_3 -> if (text.length == 1) binding.etOtp4.requestFocus() else if (text.isEmpty()) binding.etOtp2.requestFocus()
                R.id.et_otp_4 -> if (text.length == 1) binding.etOtp5.requestFocus() else if (text.isEmpty()) binding.etOtp3.requestFocus()
                R.id.et_otp_5 -> if (text.length == 1) binding.etOtp6.requestFocus() else if (text.isEmpty()) binding.etOtp4.requestFocus()
                R.id.et_otp_6 -> {
                    if (text.isEmpty()) binding.etOtp5.requestFocus()
                    if (text.length == 1) {
                        binding.btnVerify.requestFocus()
                        Utils.hideKeyboardFrom(context, binding.etOtp3.rootView)
                    }
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }

    fun openSoftKeyboard(context: Context?, view: View) {
        view.requestFocus()
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)

    }

}