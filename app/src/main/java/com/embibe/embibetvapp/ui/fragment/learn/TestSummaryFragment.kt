package com.embibe.embibetvapp.ui.fragment.learn

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.VolleyError
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.MORE_TESTS
import com.embibe.embibetvapp.constant.AppConstants.RECOMMENDED_LEARNING
import com.embibe.embibetvapp.constant.AppConstants.RECOMMENDED_PRACTICE
import com.embibe.embibetvapp.databinding.FragmentTestSummaryBinding
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.activity.*
import com.embibe.embibetvapp.ui.adapter.TestSummaryAdapter
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.VideoUtils
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class TestSummaryFragment : BaseAppFragment() {

    private lateinit var binding: FragmentTestSummaryBinding
    private lateinit var testSummaryAdapter: TestSummaryAdapter
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var type: String

    private var results: List<Content>? = null
    private var resultsLearning: List<Content>? = null
    private var searchQuery: String = ""
    private var searchResultSize: Int = 0
    private var speechRecognizer: SpeechRecognizer? = null
    private var speechIntent: Intent? = null
    private var countDownTimer: CountDownTimer? = null
    private val speechTimerDuration = 10000L // 10 secs
    private var elapsedSpeechTimer = 0L

    private var TAG = this.javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(activity)
        setupSpeechRecognitionListener()
        speechIntent = getRecognizerIntent()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_test_summary, container, false)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        type = this.requireArguments().getString("type").toString()
        SegmentUtils.trackTestSummaryScreenAvailableOptionContentLoadStart(type)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getData(view)
        micSearchButtonListeners()
        setKeyboardFocusListener()
        setSearchTextChangeListener(view)
    }

    private fun setKeyboardFocusListener() {
        binding.textSearch.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestSummaryScreenKeyboardFocus()
        }
    }

    override fun onResume() {
        super.onResume()
        SegmentUtils.trackPracticeSummaryScreenAvailableOptionContentLoadEnd(type)
    }

    private fun setSearchTextChangeListener(view: View) {
        binding.textSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (!s.isNullOrEmpty()) {
                    SegmentUtils.trackEventSearchResultLoadStart(
                        searchQuery,
                        searchQuery.length.toString()
                    )
                    SegmentUtils.trackTestSummaryScreenSearchLoadStart("Keyboard")
                    SegmentUtils.trackTestSummaryScreenKeyboardClick(
                        s.toString(),
                        s.length.toString(),
                        "D-KeyPad"
                    )
                } else {
                    getData(view)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })


    }

    private fun getData(view: View) {
        when (type) {
            getString(R.string.recommended_learning) -> {
                binding.textSearch.hint = getString(R.string.search)
                binding.tvDetailName.text = type
                results = requireArguments().getParcelableArrayList(RECOMMENDED_LEARNING)
                    ?: arrayListOf()
            }
            getString(R.string.recommended_practice) -> {
                binding.textSearch.hint = getString(R.string.search)
                binding.tvDetailName.text = type
                results = requireArguments().getParcelableArrayList(RECOMMENDED_PRACTICE)
                    ?: arrayListOf()
            }
            getString(R.string.more_tests) -> {
                binding.textSearch.hint = getString(R.string.search)
                binding.tvDetailName.text = type
                results = requireArguments().getParcelableArrayList(MORE_TESTS)
                    ?: arrayListOf()
            }

        }
        setDetailRecycler(view, results as ArrayList<Content>)
    }

    private fun createData(
        results: List<Content>?,
        resultsLearning: List<ResultsEntity>?
    ): ArrayList<Content> {
        val dataList: ArrayList<Content> = ArrayList()

        if (type == getString(R.string.more_tests)) {
            makeLog("results size - ${results?.size}")
            results?.forEach { content ->
                dataList.add(content)
            }
        } else {
            makeLog("results size - ${resultsLearning?.size}")
            resultsLearning?.forEach { resultEntity ->
                resultEntity.content?.forEach { content ->
                    dataList.add(content)
                }
            }
        }
        return dataList
    }

    private fun setDetailRecycler(view: View, data: ArrayList<Content>) {
        SegmentUtils.trackTestSummaryScreenSearchLoadEnd()
        testSummaryAdapter = TestSummaryAdapter(view.context)
        val layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        binding.detailRecycler.adapter = testSummaryAdapter
        binding.detailRecycler.layoutManager = layoutManager
        testSummaryAdapter.setData(data, type)

        testSummaryAdapter.onItemClick = { content ->
            startActivityByType(content)
        }
    }

    private fun startActivityByType(content: Content) {

        when ((content.type).toLowerCase()) {
            AppConstants.TYPE_VIDEO -> {
                startVideoActivity(content)
            }
            AppConstants.COOBO -> {

            }
            AppConstants.TYPE_BOOK -> {
                startBookDetailActivity(content)
            }
            AppConstants.TYPE_PRACTICE, AppConstants.TYPE_CHAPTER -> {
                startPracticeDetailActivity(content)
            }
            AppConstants.NAV_TYPE_SYLLABUS -> {
                startLearnSummaryActivity(content)
            }
            AppConstants.TEST -> {
                startTestScreen(content)
                // startTopicSummaryActivity(content)
            }
        }
    }

    private fun startTestScreen(data: Content) {
        val testIntent = Intent(requireContext(), TestActivity::class.java)
        testIntent.putExtra(AppConstants.TEST_XPATH, data.xpath)
        testIntent.putExtra(AppConstants.TEST_CODE, data.bundle_id)
        testIntent.putExtra(AppConstants.CONCEPT_ID, data.learning_map.conceptId)
        testIntent.putExtra(AppConstants.TEST_NAME, data.title)
        startActivity(testIntent)
    }

    private fun startLearnSummaryActivity(item: Content) {
        Utils.loadLearnSummaryAsync(item, context)
        val intent = Intent(context, LearnSummaryActivity::class.java)
        Utils.insertBundle(item, intent)
        startActivity(intent)
    }

    private fun startVideoActivity(content: Content) {
        doAsync {
            val url = content.url
            val type = Utils.getVideoTypeUsingUrl(content.url)
            val title = content.title
            val id = content.id
            var conceptId = ""
            var subject = ""
            var chapter = ""
            var totalDuration = ""
            var topicLearnPath = ""
            conceptId = content.learning_map.conceptId
            chapter = content.learning_map.chapter
            subject = content.learning_map.subject
            topicLearnPath = content.learning_map.topicLearnPathName
            totalDuration = content.length.toString()
            uiThread {
                hideProgress()
                callVideo(
                    id,
                    AppConstants.VIDEO,
                    conceptId,
                    url,
                    title,
                    "",
                    type.toLowerCase(),
                    subject,
                    chapter,
                    "",
                    totalDuration,
                    topicLearnPath,
                    content
                )
            }
        }
    }


    private fun callVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        videoTotalDuration: String,
        topicLearnpath: String,
        content: Content
    ) {
        if (source == AppConstants.PLAYER_TYPE_YOUTUBE) {
            if (Utils.isYouTubeAppAvailable(requireContext())) {
                Utils.startYoutubeApp(requireContext(), url)
            } else {
                startVideo(
                    id,
                    type,
                    conceptId,
                    url,
                    title,
                    description,
                    source,
                    subject,
                    chapter,
                    authors,
                    videoTotalDuration,
                    topicLearnpath,
                    content
                )
            }
        } else {
            val callback = object : BaseViewModel.APICallBackVolley {
                override fun <T> onSuccessCallBack(response: T) {
                    hideProgress()
                    response as JSONObject
                    val playableUrl = Utils.getVimeoHD((response["files"] as JSONArray))
                    startVideo(
                        id,
                        type,
                        conceptId,
                        playableUrl,
                        title,
                        description,
                        AppConstants.PLAYER_TYPE_EXOPLAYER,
                        subject,
                        chapter,
                        authors,
                        videoTotalDuration,
                        topicLearnpath,
                        content
                    )
                    /*hide ProgressBar here */

                }

                override fun onErrorCallBack(e: VolleyError) {
                    try {
                        makeLog("Error:  $e")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    /*hide ProgressBar here */
                    hideProgress()
                }
            }
            showProgress()
            val videoId = VideoUtils.getVimeoVideoId(url).toString()
            Utils.getVideoURL(requireActivity(), videoId, content.owner_info.key, callback)
        }

    }

    private fun startBookDetailActivity(content: Content) {
        val intent = Intent(App.context, BookDetailActivity::class.java)
        intent.putExtra(AppConstants.NAV_NAME_TYPE, AppConstants.NAV_TYPE_BOOK)
        Utils.insertBundle(content, intent)
        startActivity(intent)
    }

    private fun startPracticeDetailActivity(data: Content) {
        val intent = Intent(requireView().context, PracticeActivity::class.java)
        intent.putExtra("id", data.id)
        intent.putExtra("title", data.title)
        intent.putExtra("subject", data.subject)
        intent.putExtra(AppConstants.TOPIC_LEARN_PATH, data.learning_map.topicLearnPathName)
        intent.putExtra(AppConstants.FORMAT_ID, data.learning_map.format_id)
        intent.putExtra(AppConstants.LM_NAME, data.learnmap_id)
        intent.putExtra(AppConstants.PRACTICE_MODE, AppConstants.PracticeModes.Normal)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, data.learnpath_name)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, data.learnpath_format_name)
        try {
            val parts = data.learning_map.lpcode.split("--")
            val lmCode = parts[parts.size - 1]
            val lmLevel = Utils.getLevelUsingCode(lmCode)
            val examCode = Utils.getExamLevelCode(parts)
            intent.putExtra("lm_level", lmLevel)
            intent.putExtra("lm_code", lmCode)
            intent.putExtra("exam_code", examCode)
            intent.putExtra(AppConstants.PRACTICE_MODE, AppConstants.PracticeModes.Normal)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        startActivity(intent)
    }

    private fun micSearchButtonListeners() {

        binding.micSearch.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.micSearch.setImageResource(R.mipmap.ic_mic_selected)
            } else {
                binding.micSearch.setImageResource(R.mipmap.ic_mic_unselected)
            }
        }

        binding.micSearch.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (elapsedSpeechTimer != 0L) {
                            stopRecognition()
                            countDownTimer?.cancel()
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_DPAD_UP -> {
                        binding.micSearch.postDelayed({
                            binding.micSearch.requestFocus()
                        }, 0)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        startRecognition()
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        toggleFocusForTheView(binding.detailRecycler, true)
                    }
                    KeyEvent.KEYCODE_VOICE_ASSIST -> {
//                        Toast.makeText(context, "Voice assist pressed", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            false
        }
    }

    private fun startRecognition() {
        if (Utils.hasPermission(requireContext(), Manifest.permission.RECORD_AUDIO)) {
            speechRecognizer?.startListening(speechIntent)
            if (speechRecognizer != null) setSpeechRecognitionTimer()
        } else {
            Toast.makeText(
                context,
                requireContext().resources.getString(R.string.audio_permission_not),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun stopRecognition() {
        finishMicSearchAnimation()
        speechRecognizer?.cancel()
    }

    private fun toggleFocusForTheView(view: View, askFocus: Boolean) {

        view.postDelayed({
            when (askFocus) {
                true -> {
                    view.requestFocus()
                }
                false -> {
                    view.clearFocus()
                }
            }
        }, 10)
    }

    private fun setupSpeechRecognitionListener() {

        speechRecognizer?.setRecognitionListener(object : RecognitionListener {
            override fun onReadyForSpeech(params: Bundle?) {
                Log.v(TAG, "onReadyForSpeech")
                startMicSearchAnimation()
            }

            override fun onRmsChanged(rmsdB: Float) {
//                Log.v(TAG, "onRmsChanged $rmsdB")
            }

            override fun onBufferReceived(buffer: ByteArray?) {
//                Log.v(TAG, "onBufferReceived " + buffer?.size)
            }

            override fun onPartialResults(partialResults: Bundle?) {
                val results =
                    partialResults?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                Log.v(
                    TAG, "onPartialResults " + partialResults + " results "
                            + (results?.size ?: results)
                )
            }

            override fun onEvent(eventType: Int, params: Bundle?) {
            }

            override fun onBeginningOfSpeech() {
                Log.v(TAG, "onBeginningOfSpeech")
            }

            override fun onEndOfSpeech() {
                Log.v(TAG, "onEndOfSpeech")
                countDownTimer?.cancel()
            }

            override fun onError(error: Int) {
                when (error) {
                    SpeechRecognizer.ERROR_NETWORK_TIMEOUT ->
                        Log.w(TAG, "recognizer network timeout")
                    SpeechRecognizer.ERROR_NETWORK ->
                        Log.w(TAG, "recognizer network error")
                    SpeechRecognizer.ERROR_AUDIO ->
                        Log.w(TAG, "recognizer audio error")
                    SpeechRecognizer.ERROR_SERVER ->
                        Log.w(TAG, "recognizer server error")
                    SpeechRecognizer.ERROR_CLIENT ->
                        Log.w(TAG, "recognizer client error")
                    SpeechRecognizer.ERROR_SPEECH_TIMEOUT ->
                        Log.w(TAG, "recognizer speech timeout")
                    SpeechRecognizer.ERROR_NO_MATCH ->
                        Log.w(TAG, "recognizer no match")
                    SpeechRecognizer.ERROR_RECOGNIZER_BUSY ->
                        Log.w(TAG, "recognizer busy")
                    SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS ->
                        Log.w(TAG, "recognizer insufficient permissions")
                    else -> Log.d(TAG, "recognizer other error")
                }

                countDownTimer?.cancel()
                stopRecognition()
            }

            override fun onResults(results: Bundle?) {
                val resultsList = results?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                Log.v(
                    TAG, "onResults " + resultsList + " results "
                            + (resultsList?.size ?: resultsList)
                )
                if (resultsList!!.isNotEmpty()) {
                    binding.textSearch.text.clear()
                    val searchQuery = resultsList[0].toLowerCase(Locale.ENGLISH)
                    performSearch(searchQuery)

                    finishMicSearchAnimation()
                    countDownTimer?.cancel()
                }
            }

        })
    }

    fun startMicSearchAnimation() {
        binding.micSearchLav.visibility = View.VISIBLE
        binding.micSearch.setImageResource(0)
        binding.micSearchLav.playAnimation()
    }

    private fun finishMicSearchAnimation() {
        binding.micSearchLav.cancelAnimation()
        binding.micSearchLav.visibility = View.INVISIBLE
        binding.micSearch.setImageResource(R.mipmap.ic_mic_selected)
    }

    private fun performSearch(searchQuery: String) {
        binding.textSearch.setText(searchQuery)
        SegmentUtils.trackTestSummaryScreenVoiceSearchClick(
            searchQuery,
            searchQuery.length.toString()
        )
        SegmentUtils.trackTestSummaryScreenSearchLoadStart("Voice")

        /*api call for load suggestions only*/
//        getSearchSuggestions(searchQuery, "10")
        /*api call for load suggestions with search results*/
//        getSuggestionsAndHitSearch(searchQuery, "10")
    }

    private fun setSpeechRecognitionTimer() {
        elapsedSpeechTimer = 0L
        countDownTimer = object : CountDownTimer(speechTimerDuration, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                elapsedSpeechTimer = (speechTimerDuration - millisUntilFinished) / 1000
                Log.i(TAG, "$elapsedSpeechTimer")
            }

            override fun onFinish() {
                stopRecognition()
            }
        }
        countDownTimer?.start()
    }

    private fun getRecognizerIntent(): Intent? {
        val recognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        recognizerIntent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "en-US")
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, true)

        return recognizerIntent
    }


    fun startVideo(
        id: String,
        type: String,
        conceptId: String,
        url: String,
        title: String,
        description: String,
        source: String,
        subject: String,
        chapter: String,
        authors: String,
        videoTotalDuration: String,
        topicLearnpath: String,
        data: Content
    ) {
        val intent = Intent(App.context, VideoPlayerActivity::class.java)

        intent.putExtra(AppConstants.VIDEO_ID, id)
        intent.putExtra(AppConstants.CONTENT_TYPE, type)
        intent.putExtra(AppConstants.VIDEO_CONCEPT_ID, conceptId)
        intent.putExtra(AppConstants.VIDEO_URL, url)
        intent.putExtra(AppConstants.VIDEO_TITLE, title)
        intent.putExtra(AppConstants.VIDEO_DESCRIPTION, description)
        intent.putExtra(AppConstants.VIDEO_SUBJECT, subject)
        intent.putExtra(AppConstants.VIDEO_CHAPTER, chapter)
        intent.putExtra(AppConstants.VIDEO_AUTHORS, authors)
        intent.putExtra(AppConstants.SOURCE_REFERENCE, AppConstants.PRACTICE)
        intent.putExtra(AppConstants.VIDEO_SOURCE, source)
        intent.putExtra(AppConstants.TOPIC_LEARN_PATH, topicLearnpath)
        intent.putExtra(AppConstants.IS_RECOMMENDATION_ENABLED, true)
        intent.putExtra(AppConstants.VIDEO_TOTAL_DURATION, videoTotalDuration)
        intent.putExtra(AppConstants.FORMAT_ID, data.learning_map.format_id)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, data.learnpath_name)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, data.learnpath_format_name)
        startActivity(intent)
    }
}

