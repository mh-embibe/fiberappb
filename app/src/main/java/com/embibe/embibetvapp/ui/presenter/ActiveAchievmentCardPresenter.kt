package com.embibe.embibetvapp.ui.presenter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.leanback.widget.BaseCardView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.model.achieve.home.Content
import com.embibe.embibetvapp.utils.Utils
import java.util.*

class ActiveAchievmentCardPresenter(context: Context) :
    AbstractCardPresenter<BaseCardView>(context) {
    override fun onCreateView(): BaseCardView {
        val cardView =
            BaseCardView(context, null, R.style.SideInfoCardStyle)
        cardView.isFocusable = true
        cardView.addView(
            LayoutInflater.from(context).inflate(
                R.layout.card_active_achievment,
                null
            )
        )
        return cardView
    }

    @SuppressLint("StringFormatMatches")
    override fun onBindViewHolder(item: Any, cardView: BaseCardView) {
        val card = item as Content

        val tvSubjectName: TextView = cardView.findViewById(R.id.tvSubjectName)
        val imgThumbnailView: ImageView = cardView.findViewById(R.id.iv_thumbnail)
        val tvTitle: TextView = cardView.findViewById(R.id.tvTitle)
        val tvParameters: TextView = cardView.findViewById(R.id.tvParameters)
        val tvPotentialGrade: TextView = cardView.findViewById(R.id.tvPotentialGrade)
        val tvPredictedGrade: TextView = cardView.findViewById(R.id.tvPredictedGrade)
        val tvPredictedGradeLabel: TextView = cardView.findViewById(R.id.tvPredictedGradeLabel)
        val overAllProgress: ProgressBar = cardView.findViewById(R.id.overAllProgress)
        val imglocked: ImageView = cardView.findViewById(R.id.iv_locked)
        try {
            if (card.subject.isNotEmpty()) {
                tvSubjectName.visibility = View.VISIBLE
                tvSubjectName.text = card.subject
                (tvSubjectName.background as GradientDrawable).setColor(
                    context.resources.getColor(
                        Utils.getColorCode(
                            card.subject
                        )
                    )
                )
            } else {
                tvSubjectName.visibility = View.GONE
            }

            tvTitle.text = context.getString(
                R.string.acheive_card_title,
                card.title,
                card.current_step,
                card.total_steps
            )

            tvPotentialGrade.text = card.potential_grade
            tvPredictedGrade.text = card.actual_grade
            overAllProgress.progress = card.overall_progress


            if (card.status.toLowerCase().equals("locked")){
                imglocked.visibility = VISIBLE
            }else{
                imglocked.visibility = GONE
            }
            if (card.overall_progress == 100) {
                tvPotentialGrade.visibility = View.VISIBLE
                tvPredictedGrade.visibility = View.VISIBLE
                tvPredictedGradeLabel.visibility = View.VISIBLE
                overAllProgress.visibility = View.INVISIBLE

                tvParameters.text =
                    context.getString(R.string.achive_card_subtitle) + card.effort_rating + "%"
                if (card.completion_status.toLowerCase() == "gold") {
                    val requestOptions = RequestOptions().transform(RoundedCorners(8))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                    Glide.with(context).applyDefaultRequestOptions(requestOptions)
                        .load(R.drawable.bg_gold_achievement)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .placeholder(R.drawable.bg_gold_achievement)
                        .into(imgThumbnailView)
                } else if (card.completion_status.toLowerCase(Locale.getDefault()) == "silver") {
                    val requestOptions = RequestOptions().transform(RoundedCorners(8))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                    Glide.with(context).applyDefaultRequestOptions(requestOptions)
                        .load(R.drawable.bg_silver_achievement)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(imgThumbnailView)
                }

            } else {
                overAllProgress.visibility = View.VISIBLE
                tvParameters.text = context.getString(
                    R.string.achive_card_subtitle_active,
                    card.questions,
                    card.videos,
                    card.behaviours
                )
                val requestOptions = RequestOptions().transform(RoundedCorners(8))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                Glide.with(context).applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.achieve_bg)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imgThumbnailView)
                tvPotentialGrade.visibility = View.VISIBLE
                tvPredictedGrade.visibility = View.INVISIBLE
                tvPredictedGradeLabel.visibility = View.INVISIBLE
                overAllProgress.visibility = View.INVISIBLE
            }

        } catch (e: Exception) {

        }
    }

}