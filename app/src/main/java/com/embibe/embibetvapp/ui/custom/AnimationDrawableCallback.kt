package com.embibe.embibetvapp.ui.custom

import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.Drawable


/**
 * A custom callback for AnimationDrawable
 */
abstract class AnimationDrawableCallback(
    animationDrawable: AnimationDrawable,
    callback: Drawable.Callback?
) :
    Drawable.Callback {
    private val mLastFrame: Drawable?
    private val mFirstFrame: Drawable?
    private val mWrappedCallback: Drawable.Callback?
    private var mIsCallbackTriggered = false
    override fun invalidateDrawable(who: Drawable) {
        mWrappedCallback?.invalidateDrawable(who)
        if (!mIsCallbackTriggered && mLastFrame != null && mLastFrame == who.current) {
            mIsCallbackTriggered = true
            onAnimationComplete()
        }
        if (mFirstFrame != null && mFirstFrame == who.current) {
            onAnimationStart()
        }
    }

    override fun scheduleDrawable(
        who: Drawable,
        what: Runnable,
        `when`: Long
    ) {
        mWrappedCallback?.scheduleDrawable(who, what, `when`)
    }

    override fun unscheduleDrawable(
        who: Drawable,
        what: Runnable
    ) {
        mWrappedCallback?.unscheduleDrawable(who, what)
    }

    abstract fun onAnimationComplete()
    abstract fun onAnimationStart()

    init {
        mFirstFrame = animationDrawable.getFrame(0)
        mLastFrame = animationDrawable.getFrame(animationDrawable.numberOfFrames - 1)
        mWrappedCallback = callback
    }
}