package com.embibe.embibetvapp.ui.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.databinding.ActivityDiagnosticTestBinding
import com.embibe.embibetvapp.ui.fragment.achieve.DiagnosticTestFragment

class DiagnosticTestActivity : BaseFragmentActivity() {

    private lateinit var binding: ActivityDiagnosticTestBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_diagnostic_test)
        supportFragmentManager.beginTransaction()
            .replace(R.id.diagnostic_FL, DiagnosticTestFragment()).commit()
    }

}