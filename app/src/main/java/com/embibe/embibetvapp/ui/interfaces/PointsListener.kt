package com.embibe.embibetvapp.ui.interfaces

import android.graphics.PointF

interface PointsListener {
    fun getPointOnCurve(point: PointF?)
}