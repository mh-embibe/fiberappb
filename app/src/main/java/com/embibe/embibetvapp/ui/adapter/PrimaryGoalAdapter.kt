package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemGoalsBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.adduser.Goal
import com.embibe.embibetvapp.ui.activity.AddGoalsExamsActivity
import com.embibe.embibetvapp.utils.SegmentUtils


class PrimaryGoalAdapter(var context: Context) :
    RecyclerView.Adapter<PrimaryGoalAdapter.GoalViewHolder>() {

    private var list: List<Goal> = arrayListOf()
    var onItemClick: ((Goal) -> Unit)? = null
    var onFocusChange: ((Goal, Boolean) -> Unit)? = null
    var selectedItem = -1
    private var currentPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GoalViewHolder {
        val binding: ItemGoalsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_goals, parent, false
        )
        return GoalViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: GoalViewHolder, position: Int) {
        currentPosition = holder.adapterPosition
        holder.bind(list[position])

        if (selectedItem == position) {
            holder.binding.textGoalExam.background = context.getDrawable(R.drawable.goal_selected)
            holder.binding.textGoalExam.background =
                context.getDrawable(R.drawable.bg_goals_slected_selector)
            holder.binding.textGoalExam.setTextColor(context.getColor(R.color.email_font_color))
        }
    }

    fun setData(data: List<Goal>) {
        list = data
        setDefaultFocusToSupportedGoal(list)
        notifyDataSetChanged()
    }

    private fun setDefaultFocusToSupportedGoal(primaryGoals: List<Goal>) {
        try {
            val goalCode = UserData.getCurrentProfile()!!.primary_goal_code!!
            for (index in primaryGoals.indices) {
                if (goalCode == primaryGoals[index].code /*primaryGoals[index].default*/) {
                    setDefaultSelectedItem(index)
                    AddGoalsExamsActivity.primaryGoalCode = primaryGoals[index].code
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setDefaultSelectedItem(position: Int) {
        selectedItem = position
        notifyDataSetChanged()
    }

    inner class GoalViewHolder(var binding: ItemGoalsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Goal) {
            binding.goal = item
            binding.executePendingBindings()

            binding.textGoalExam.background = context.getDrawable(R.drawable.bg_button_goals)
            binding.textGoalExam.setTextColor(context.getColor(R.color.white))


            itemView.setOnFocusChangeListener { v, hasFocus ->
                onFocusChange?.invoke(list[adapterPosition], hasFocus)
                SegmentUtils.trackProfilePrimaryGoalValueFocus(
                    list[adapterPosition].name,
                    adapterPosition
                )
            }
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
                if (item.supported) {
                    SegmentUtils.trackProfilePrimaryGoalValueClick(
                        list[adapterPosition].name,
                        adapterPosition
                    )
                    try {
                        this@PrimaryGoalAdapter.currentPosition = adapterPosition
                        if (this@PrimaryGoalAdapter.currentPosition == -1) {
                            return@setOnClickListener
                        }
                        onItemClick?.invoke(list[adapterPosition])

                        if (selectedItem != adapterPosition) {
                            val previousItem = selectedItem
                            selectedItem = adapterPosition
                            notifyItemChanged(previousItem)

                        } else
                            selectedItem = -1
                        notifyItemChanged(adapterPosition)
                    } catch (e: Exception) {

                    }
                }

            }

        }

    }
}
