package com.embibe.embibetvapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemBehaviourImprovementBinding
import com.embibe.embibetvapp.model.test.TestBehaviourImprovement

class BehaviourImprovementAdapter :
    RecyclerView.Adapter<BehaviourImprovementAdapter.TestsViewHolder>() {

    var list = ArrayList<TestBehaviourImprovement>()
    var onItemClick: ((TestBehaviourImprovement) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestsViewHolder {
        val binding: ItemBehaviourImprovementBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_behaviour_improvement, parent, false
        )
        return TestsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (list.size > 0) list.size else 0
    }

    override fun onBindViewHolder(holder: TestsViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun setData(list: ArrayList<TestBehaviourImprovement>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class TestsViewHolder(var binding: ItemBehaviourImprovementBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: TestBehaviourImprovement) {
            binding.valueSincerityScorePrevious.text = item.previousBehaviour
            binding.valueSincerityScoreAfter.text = item.currentBehaviour
            binding.executePendingBindings()

            if (adapterPosition == list.size - 1) {
                binding.verticalViewBottom.visibility = INVISIBLE
            }

            setImage(binding, item, itemView)
        }
    }

    private fun setImage(
        binding: ItemBehaviourImprovementBinding,
        item: TestBehaviourImprovement,
        itemView: View
    ) {
        val requestOptions = RequestOptions().transform(RoundedCorners(16))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        if (item.content_thumb == null || item.content_thumb == "thumb" || item.content_thumb == "") {
            Glide.with(itemView.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(R.drawable.video_placeholder)
                .into(binding.ivImg)
        } else {
            Glide.with(itemView.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(item.content_thumb)
                .placeholder(R.drawable.video_placeholder)
                .into(binding.ivImg)
        }
    }
}