package com.embibe.embibetvapp.ui.fragment.test

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import androidx.leanback.widget.*
import androidx.lifecycle.ViewModelProviders
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseRowsSupportFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.ui.interfaces.RowsToBannerListener
import com.embibe.embibetvapp.ui.presenter.CardPresenterSelector
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.CardListRow
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.ShadowRowPresenterSelector
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class TestRowsFragment : BaseRowsSupportFragment(), DPadKeysListener {

    private lateinit var createOwnTestContent: Content
    private var results: List<ResultsEntity> = arrayListOf()
    private val mRowsAdapter: ArrayObjectAdapter = ArrayObjectAdapter(ShadowRowPresenterSelector())
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var volleyRequest: RequestQueue
    private lateinit var navigationMenuCallback: NavigationMenuCallback
    private lateinit var rowsToBannerListener: RowsToBannerListener
    private lateinit var dPadKeysCallback: DPadKeysListener
    private var isBannerVisible = true

    private val classTag = TestRowsFragment::class.java.toString()

    init {
        initializeListeners()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        volleyRequest = Volley.newRequestQueue(activity)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

        navigationMenuCallback.navMenuToggle(false)
        if (DataManager.instance.getHomeTest().isEmpty()) {
            loadData(false)
            loadTestDataFromApiAsync(true)
        } else {
            createRows(DataManager.instance.getHomeTest())
            loadTestDataFromApiAsync(true)
        }

    }

    private fun loadData(isUpdate: Boolean) {
        if (!isUpdate) {
            //showProgress()
        }
        doAsync {
            results = homeViewModel.fetchSectionByPageName(AppConstants.TEST)
            results = results.sortedBy { it.sectionId }
            uiThread {
                if (results.isEmpty()) {
                    loadTestDataFromApiAsync(false)
                } else {
                    rowsToBannerListener.notifyHost(AppConstants.CONTENT_AVAILABLE)
                    if (isUpdate) {
                        updateRows(results)
                    } else {
                        //hideProgress()
                        createRows(results)
                    }
                }
                rowsToBannerListener.notifyBanner("Synced")
            }
        }
    }


    override fun onResume() {
        super.onResume()
        /*check for content updated status*/
        if (Utils.isContentStatusUpdatedForTest) {
            Utils.isContentStatusUpdatedForTest = !Utils.isContentStatusUpdatedForTest
            /*refreshing home screen*/
            loadTestDataFromApiAsync(true)
        }
        if (Utils.isBookmarkUpdatedForTEST || Utils.isTestGenerated) {
            Utils.isBookmarkUpdatedForTEST = false
            Utils.isTestGenerated = false
            dataRepo.removeListData(results as ArrayList<ResultsEntity>)
            loadTestDataFromApiAsync(false)
        }
        try {
            restoreLastSelection(false)
        } catch (e: Exception) {
            e.printStackTrace()
            makeLog("content-status Exception ${e.printStackTrace()}")
        }
    }

    private fun updateRows(results: List<ResultsEntity>) {
        try {
            for ((index, video) in results.withIndex()) {
                val row = mRowsAdapter.get(index) as CardListRow
                val rowItemsAdapter = row.adapter as ArrayObjectAdapter
                rowItemsAdapter.setItems(getContentItems(video), null)
                mRowsAdapter.replace(
                    index,
                    CardListRow(HeaderItem(setSectionName(video)), rowItemsAdapter)
                )
            }
            restoreLastSelection(false)
        } catch (e: Exception) {
            makeLog("app ${e.localizedMessage}")
            e.printStackTrace()
        }
    }


    fun loadTestDataFromApiAsync(isAsync: Boolean) {
        if (!isAsync) {
            //showProgress()
        }
        homeViewModel.homeTest(object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
            override fun onSuccess(model: List<ResultsEntity>?) {
                if (model != null) {
                    updateData(isAsync, model as java.util.ArrayList<ResultsEntity>)
                }

            }

            override fun onFailed(code: Int, error: String, msg: String) {
                if (!isAsync) {
                    if (code == 204) //no content success
                        rowsToBannerListener.notifyHost(AppConstants.NO_CONTENT)

                    //hideProgress()
                    if (Utils.isApiFailed(code)) {
                        Utils.showError(
                            context,
                            code,
                            object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: String) {
                                    loadTestDataFromApiAsync(isAsync)
                                }

                                override fun onDismiss() {

                                }
                            })

                    } else {
                        showToast(error)
                    }
                }

            }
        })
    }

    private fun getCreateTestTile(): Content {
        if (!::createOwnTestContent.isInitialized) {
            val json =
                Utils.inputStreamToString(App.context.resources.openRawResource(R.raw.create_your_own_test_section))
            val createOwnTestSection = Gson().fromJson(json, ResultsEntity::class.java)
            val content = createOwnTestSection.content?.get(0) ?: Content()
            content.type = AppConstants.CREATE_TEST
            createOwnTestContent = content
        }
        return createOwnTestContent
    }

    private fun updateData(isAsync: Boolean, model: java.util.ArrayList<ResultsEntity>) {
        if (model.size - 1 != mRowsAdapter.size()) {
            try {
                dataRepo.removeListData(results as ArrayList<ResultsEntity>)
                results = arrayListOf()
            } catch (e: Exception) {
                try {
                    dataRepo.removeListData(model)
                    e.printStackTrace()
                } catch (e: Exception) {

                }
            }
        }
        val callback = object : BaseViewModel.DataCallback<String> {
            override fun onSuccess(model: String?) {
                DataManager.instance.setHomeTest(arrayListOf(), null)
                if (!isAsync) {
                    //hideProgress()
                    loadData(false)
                } else {
                    loadData(true)
                }
            }

        }
        homeViewModel.saveResultToDb(model, AppConstants.TEST, "all", callback)
    }



    private fun createRows(results: List<ResultsEntity>) {
        if (mRowsAdapter.size() > 0) {
            mRowsAdapter.clear()
        }
        makeLog("creating..rows")
        for (video in results) {
            mRowsAdapter.add(creatingNewRow(video))
            makeLog("creating..rows ${video.section_name}")
        }
    }

    private fun creatingNewRow(videos: ResultsEntity): Row {
        val presenterSelector = activity?.baseContext?.let { CardPresenterSelector(it) }
        presenterSelector?.setDPadKeysListener(this)
        val adapter = ArrayObjectAdapter(presenterSelector)
        for (video in getContentItems(videos)) {
                adapter.add(video)
        }
        val headerItem = HeaderItem(setSectionName(videos))

        return CardListRow(headerItem, adapter)
    }

    private fun setSectionName(videos: ResultsEntity): String {
        var emptyString = ""
        if (videos.section_name.equals("Static Practise Card", true))
            return emptyString
        else return videos.section_name
    }

    inline fun <reified T : Any> Any.cast(): T {
        return this as T
    }

    private fun initializeListeners() {
        adapter = mRowsAdapter
        onItemViewClickedListener =
            OnItemViewClickedListener { itemViewHolder, item, rowViewHolder, row ->
                Utils.subjectFilterBy = AppConstants.TEST
                onClickItem(item as Content, row, mRowsAdapter, itemViewHolder)

                SegmentUtils.trackTestScreenTileClick(
                    mRowsAdapter.indexOf(row),
                    mRowsAdapter.indexOf(item),
                    item.content_id,
                    item.subject,
                    item.section_name, item
                )
            }

        onItemViewSelectedListener =
            OnItemViewSelectedListener { itemViewHolder, item, rowViewHolder, row ->

                if (item is Content) {
                    val indexOfRow = mRowsAdapter.indexOf(row)
                    val indexOfItem =
                        ((row as CardListRow).adapter as ArrayObjectAdapter).indexOf(item)

                    SegmentUtils.trackEventHomeTileFocus(
                        item,
                        row.headerItem.name,
                        indexOfRow,
                        indexOfItem
                    )
                    SegmentUtils.trackTestScreenTileFocus(
                        indexOfRow,
                        indexOfItem,
                        item.content_id,
                        item.subject,
                        item.section_name
                    )

                    val itemView = itemViewHolder.view

                    if (itemView.isFocusable && !itemView.isFocused) itemView.requestFocus()

                    saveLastSelected(indexOfItem, indexOfRow)

                    itemView.setOnKeyListener { v, keyCode, event ->
                        //checking only for down presses
                        if (event.action == KeyEvent.ACTION_DOWN) {
                            when (keyCode) {
                                KeyEvent.KEYCODE_DPAD_LEFT -> {
                                    if (indexOfItem == 0) {
                                        navigationMenuCallback.navMenuToggle(true)
                                    }
                                }
                                KeyEvent.KEYCODE_DPAD_RIGHT -> {

                                }
                                KeyEvent.KEYCODE_DPAD_UP -> {
                                    if (indexOfRow == 0 && !Utils.isKeyPressedTooFast(500))
                                        rowsToBannerListener.rowsToBanner()
                                }
                                KeyEvent.KEYCODE_DPAD_DOWN -> {
                                    if (isBannerVisible)
                                        dPadKeysCallback.isKeyPadDown(true, "Rows")
                                    //for last row
                                    if (indexOfRow == mRowsAdapter.size() - 1) {
                                        itemView.postDelayed({
                                            itemView.requestFocus()
                                        }, 0)
                                    }
                                }
                            }
                        }
                        false
                    }
                }
            }
    }

    fun isBannerVisible(visible: Boolean) {
        isBannerVisible = visible
    }


    private fun saveLastSelected(indexOfItem: Int, indexOfRow: Int) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        with(sharedPref?.edit()) {
            this?.putInt(getString(R.string.last_selected_test_row), indexOfItem)
            this?.putInt(getString(R.string.last_selected_test_row_item), indexOfRow)
            this?.apply()
        }
    }

    fun restoreLastSelection(fromNavigation: Boolean) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val itemIndex = sharedPref.getInt(getString(R.string.last_selected_test_row), 0)
        val rowIndex = sharedPref.getInt(getString(R.string.last_selected_test_row_item), 0)

        if (fromNavigation) {//for 0th item and Navigation interaction case
            val rvh = getRowViewHolder(rowIndex)
            rvh?.selectedItemViewHolder?.view?.focusSearch(View.FOCUS_LEFT)?.requestFocus()
        } else {
            val rowsSize = mRowsAdapter.size()
            if (rowIndex < rowsSize) {
                val rowItemsSize = (mRowsAdapter.get(rowIndex) as CardListRow).adapter.size()
                if (itemIndex < rowItemsSize) {
                    setSelectedPosition(
                        rowIndex,
                        true,
                        object : ListRowPresenter.SelectItemViewHolderTask(itemIndex) {
                            override fun run(holder: Presenter.ViewHolder?) {
                                super.run(holder)//imp line
                                holder?.view?.postDelayed({
                                    holder.view.requestFocus()
                                    //hideProgress()
                                }, 10)
                            }
                        })
                }
            }
        }
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    fun setRowsToBannerListener(callback: RowsToBannerListener) {
        this.rowsToBannerListener = callback
    }

    fun setFirstRowFocus() {
        Handler().postDelayed({
            mainFragmentRowsAdapter.setSelectedPosition(0, true)
        }, 100)
        mainFragmentRowsAdapter.setSelectedPosition(1, false)
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {

    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {

    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
        navigationMenuCallback.navMenuToggle(true)
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {

    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {
        dPadKeysCallback.isKeyPadEnter(true, "Rows", cardType)
    }

}