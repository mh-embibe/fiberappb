package com.embibe.embibetvapp.ui.activity

import android.view.View
import androidx.constraintlayout.motion.widget.MotionLayout
import com.embibe.embibetvapp.base.TestFeedbackBaseFragmentActivity
import com.embibe.embibetvapp.databinding.ActivityTestFeedbackBinding
import com.embibe.embibetvapp.model.test.*
import com.embibe.embibetvapp.ui.adapter.DiagnosticTestFeedbackAdapter
import com.embibe.embibetvapp.ui.adapter.TestBarAdapter
import com.embibe.embibetvapp.ui.adapter.TestJarAdapter
import com.embibe.embibetvapp.ui.adapter.TopSkillAdapter
import com.embibe.embibetvapp.ui.viewmodel.TestViewModel
import kotlinx.coroutines.CoroutineScope
import java.util.*
import kotlin.collections.ArrayList

class TestFeedbackActivity2 : TestFeedbackBaseFragmentActivity() {

    private lateinit var testViewModel: TestViewModel
    private lateinit var binding: ActivityTestFeedbackBinding
    private lateinit var barAdapter: TestBarAdapter
    private lateinit var topSkillAdapter: TopSkillAdapter
    private lateinit var menuAdapter: DiagnosticTestFeedbackAdapter
    private lateinit var jarAdapter: TestJarAdapter
    private lateinit var coroutineScope: CoroutineScope

    private var testAttemptsRes: TestAttemptsRes? = null
    private var testSkillsRes: TestSkillsRes? = null
    private var testQuestionRes: TestQuestionResponse? = null
    private var testAchieveRes: TestAchieveRes? = null

    private var mySectionSummaryList = ArrayList<SectionSummary>()
    private var listTopSkill = ArrayList<SkillSet>()
    private var list = ArrayList<String>()

    private var correctlyAnsweredMotionLayout: MotionLayout? = null
    private var isFirstRowExpanded: Boolean = false
    private var layoutAQVisibility: Boolean = false
    private var isJarEnabled: Boolean = false
    private var jarType: Int = -1


    private var lastKeyPress = 0L
    private var rowsHM = TreeMap<Int, View>()
    private var viewFocusList = ArrayList<View>()
    private var viewFocusCounter = 0
    private var progressLayoutWidth: Int = 0
    private var currentQWASubject = ""
    private var currentAttemptType = ""
    var testName: String = ""
    var bundleId: String = ""
    var height: Int = 0
    val TAG = this.javaClass.name

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//        binding = DataBindingUtil.setContentView(this, R.layout.activity_test_feedback)
//        coroutineScope = CoroutineScope(Dispatchers.Main)
//        correctlyAnsweredMotionLayout = binding.layoutChart.layoutCorrectlyAnswered.constraintLayout
//        testViewModel = ViewModelProviders.of(this).get(TestViewModel::class.java)
//
//        addViewsToHashMap()
//        addFocusViewsToList()
//        viewFocusList[viewFocusCounter].requestFocus()
//
//        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
//
//        getAllData()
//        setBackgrounds(requestOptions)
//
//        setFirstRowListeners()
//        setClickToWatchListener()
//        setQWAKeyListener()
//        setQWAButtonListener()
//        setAndAnimateProgress()
//        setQWAFocusListener()
//        setWebP(R.drawable.achieve_sphere_animation)
//        //getAchieveForFeedback(bundleId)
//
//    }
//
//    private fun setDropDownQWA() {
//        currentQWASubject = getSubjectList()[0]
//        binding.layoutQWA.tvDropDownSubject.text = currentQWASubject
//        if (getSubjectList().size > 1) {
//            binding.layoutQWA.tvDropDownSubject.setOnClickListener {
//                val builder = AlertDialog.Builder(this)
//                builder.setTitle("Choose Subject")
//                val array = getSubjectList().toTypedArray()
//                builder.setItems(
//                    array
//                ) { dialog, which ->
//                    if (getSubjectList()[which].toLowerCase().contains(ALL_SUBJECTS)) {
//                        currentQWASubject = ALL_SUBJECTS
//                        binding.layoutQWA.tvDropDownSubject.text = currentQWASubject
//                        //setQuestionList(getQuestionListData(currentQWASubject))
//                        setJarRecycler()
//                    } else {
//                        currentQWASubject = getSubjectList()[which]
//                        binding.layoutQWA.tvDropDownSubject.text = currentQWASubject
//                        //setQuestionList(getQuestionListData(currentQWASubject))
//                        setJarRecycler()
//                    }
//                }
//
//                val dialog = builder.create()
//                dialog.show()
//            }
//        }
//    }
//
//    private fun calculateAttemptPercent(
//        testAttemptsRes: TestAttemptsRes,
//        attemptType: String,
//        subject: String,
//        questionList: java.util.ArrayList<Question>
//    ): String {
//        var percentage: Int = 0
//        if (testAttemptsRes != null && testAttemptsRes.attempts != null && testAttemptsRes.attempts!!.size > 0) {
//            var attemptSize =
//                testAttemptsRes.attempts!!.filter {
//                    it.attemptTypeBadge == attemptType && (subject == AppConstants.ALL_SUBJECTS || isQuestionAvailable(
//                        it.questionCode!!,
//                        questionList
//                    ))
//                }.size
//            var totalSize =
//                if (subject == ALL_SUBJECTS) testAttemptsRes.attempts!!.size else testAttemptsRes.attempts!!.filter {
//                    isQuestionAvailable(
//                        it.questionCode!!,
//                        questionList
//                    )
//                }.size
//            percentage =
//                if (attemptSize == 0) 0 else ((attemptSize.toDouble() / totalSize.toDouble()) * 100).toInt()
//        }
//        return "$percentage%"
//    }
//
//    private fun isQuestionAvailable(
//        qcode: String,
//        questionList: java.util.ArrayList<Question>
//    ): Boolean {
//        var pos = questionList.filter { it.code == qcode }.size
//        return pos > 0
//    }
//
//
//    private fun setQWAFocusListener() {
//        binding.layoutQWA.btnUnAttempted.setOnKeyListener { v, keyCode, event ->
//            if (event.action == KeyEvent.ACTION_DOWN) {
//                when (keyCode) {
//                    KeyEvent.KEYCODE_DPAD_DOWN -> {
//                        if (layoutAQVisibility) {
//                            currentFocus?.clearFocus()
//                            binding.layoutQWA.wvQWA.requestFocus()
//                        } else {
//                            currentFocus?.clearFocus()
//                            binding.layoutQWA.layoutJar.rvJar.requestFocus()
//                        }
//                        return@setOnKeyListener true
//                    }
//                }
//            }
//            false
//        }
//        binding.layoutQWA.btnTooFastCorrectAttempts.setOnKeyListener { v, keyCode, event ->
//            if (event.action == KeyEvent.ACTION_DOWN) {
//                when (keyCode) {
//                    KeyEvent.KEYCODE_DPAD_DOWN -> {
//                        if (layoutAQVisibility) {
//                            currentFocus?.clearFocus()
//                            binding.layoutQWA.wvQWA.requestFocus()
//                        } else {
//                            currentFocus?.clearFocus()
//                            binding.layoutQWA.layoutJar.rvJar.requestFocus()
//                        }
//                        return@setOnKeyListener true
//                    }
//
//                }
//            }
//            false
//        }
//
//    }
//
//    private fun getAllData() {
//
//        with(DataManager.instance) {
//            testAttemptsRes = Gson().fromJson(getTestAttempts(), TestAttemptsRes::class.java)
//            testSkillsRes = Gson().fromJson(getTestSkills(), TestSkillsRes::class.java)
//            testQuestionRes = Gson().fromJson(getTestQuestion(), TestQuestionResponse::class.java)
//            //testQuestionRes.paper.sections.entries.
//            testAchieveRes = Gson().fromJson(getTestAchieve(), TestAchieveRes::class.java)
//        }
//
//        parseAttemptInToQuestion()
//
//        if (intent.hasExtra("testName"))
//            testName = intent.getStringExtra("testName")!!
//        if (intent.hasExtra("testCode"))
//            bundleId = intent.getStringExtra("testCode")!!
//        readFromLocal()
//
//        setTitle()
//        setChart()
//        setTopSkillRecycler()
//        setAchieveRecycler()
//        setAchieveMetrics()
//        setAchieveGif()
//        setDropDownQWA()
//        setQWAWebView()
//        setJarRecycler()
//    }
//
//    private fun setAchieveGif() {
//        Glide.with(this)
//            .load(R.drawable.bg_particles_new)
//            .into(binding.layoutAchieve.ivParticles)
//    }
//
//    private fun parseAttemptInToQuestion() {
//        if (testQuestionRes != null) {
//            for (key in testQuestionRes!!.paper.sections.keys) {
//                val section = testQuestionRes!!.paper.sections[key]
//
//                for (question in section!!.questions) {
//                    val attempt = getAttemptByCode(question.code)
//                    if (attempt != null) {
//                        question.answerSelectedOption = attempt.answerSelectedOption
//                        question.attemptTypeBadge = attempt.attemptTypeBadge
//                        question.tags = attempt.tags!!
//                        question.explanation = attempt.explanation
//                        question.correctAnswerCsv = attempt.correctAnswerCsv
//                        question.marks_obtained = attempt.marks!!
//                        question.first_looked_at = attempt.tFirstLook!!
//                        question.attempted_at = attempt.tFirstSave!!
//                        question.test_started_at = testAttemptsRes!!.startedAtInMilliSec!!
//                    }
//                }
//            }
//        }
//    }
//
//
//    private fun getQuestionListData(
//        subject: String,
//        type: String
//    ): ArrayList<Question> {
//        var qlist = ArrayList<Question>()
//        currentAttemptType = type
//        if (testQuestionRes != null) {
//            for (key in testQuestionRes!!.paper.sections.keys) {
//                val section = testQuestionRes!!.paper.sections[key]
//                if (section!!.name == subject || subject == ALL_SUBJECTS) {
//                    for (question in section.questions) {
//                        if (question.attemptTypeBadge == type) {
//                            qlist.add(question)
//                        }
//                    }
//                }
//            }
//        }
//        binding.layoutQWA.layoutAQ.AQ.text =
//            calculateAttemptPercent(testAttemptsRes!!, type, subject, qlist) + " " + type
//        return qlist
//    }
//
//    private fun moveToTestQWA(question: Question, pos: Int) {
//        with(DataManager.instance) {
//            setTestQuestion(Gson().toJson(testQuestionRes!!))
//            setCurrentQuestion(Gson().toJson(question))
//        }
//        startActivity(
//            TestQWAActivity.getQWAIntent(
//                this, pos, currentQWASubject, currentAttemptType
//            )
//        )
//    }
//
//    private fun getSubjectList(): List<String> {
//        var slist = ArrayList<String>()
//        if (testQuestionRes != null) {
//            for (key in testQuestionRes!!.paper.sections.keys) {
//                val section = testQuestionRes!!.paper.sections[key]
//                slist.add(section!!.name)
//            }
//            if (slist.size > 1) {
//                slist.add(0, ALL_SUBJECTS)
//            }
//        }
//        return slist
//    }
//
//
//    private fun getAttemptByCode(code: String): Attempt? {
//        if (testAttemptsRes != null) {
//            for (attempt in testAttemptsRes!!.attempts!!) {
//                if (attempt.questionCode == code)
//                    return attempt
//            }
//        }
//        return null
//    }
//
//
//    private fun readFromLocal() {
//
//        for (i in testAttemptsRes!!.scoreListInfo!!.sectionSummaryList!!.indices) {
////            if(testAttemptsRes!!.scoreListInfo!!.sectionSummaryList!![i].marksScored >= 0)
//            mySectionSummaryList.add(testAttemptsRes!!.scoreListInfo!!.sectionSummaryList!![i])
//        }
//        for (i in testSkillsRes!!.skillset!!.indices) {
//            listTopSkill.add(testSkillsRes!!.skillset?.get(i)!!)
//        }
//        Log.e(TAG, "size: ${mySectionSummaryList.size}")
//        Log.e(TAG, "Top Skill size: ${listTopSkill.size}")
//    }
//
//    private fun setTitle() {
//        binding.tvTitle.text = getString(R.string.marks_and_overall_performance_on_and)
//            .replace("*test_name*", testName)
//            .replace(
//                "*exam_name*", DataManager.instance.getExamNameByCode(
//                    UserData.getGoalCode(), UserData.getExamCode()
//                )
//            )
//    }
//
//    private fun setBackgrounds(requestOptions: RequestOptions) {
//        Glide.with(this).setDefaultRequestOptions(requestOptions)
//            .load(R.drawable.video_banner)
//            .transition(DrawableTransitionOptions.withCrossFade())
//            .transform(RoundedCorners(20))
//            .into(binding.layoutClickToWatchAndTopSkill.layoutClickToWatch.ivThumbnail)
//
//        Glide.with(this).setDefaultRequestOptions(requestOptions)
//            .load(R.drawable.bg_first_row_feedback)
//            .transition(DrawableTransitionOptions.withCrossFade())
//            .transform(RoundedCorners(20))
//            .into(binding.layoutChart.ivBg)
//    }
//
//    private fun setChart() {
////        list.add("30 marks")
////        list.add("45 marks")
////        list.add("-39 marks")
////        list.add("20 marks")
//        barAdapter = TestBarAdapter()
////        for(data in mySectionSummaryList) {
////            if(data.marksScored < 0)
////                increaseSizeOfFirstRow()
////        }
//        barAdapter.setItems(mySectionSummaryList)
//        binding.layoutChart.layoutCorrectlyAnswered.rvScoreBar.adapter = barAdapter
//
//        setDataForChart()
//    }
//
//    private fun increaseSizeOfFirstRow() {
//        val params = binding.layoutChart.constraintCharts.layoutParams as LinearLayout.LayoutParams
//        params.height = 400
//        binding.layoutChart.constraintCharts.layoutParams = params
//    }
//
//    private fun setDataForChart() {
//
//        var scoreText = binding.layoutChart.layoutCorrectlyAnswered.tvScore
//        var questionStr = getString(R.string.test_feedback_questions)
//        var marksObtained = testSkillsRes?.correct_answer
//        var totalMarks = testSkillsRes?.questions
////        val correctAnswer = testSkillsRes?.correct_answer
////        val totalQuestions = testSkillsRes?.questions
//        var bothValueAsStr = "$marksObtained/$totalMarks"
//        scoreText.text =
//            Utils.getSpannable(
//                this, "$bothValueAsStr $questionStr",
//                R.color.colorGreen, 0, marksObtained.toString().length
//            )
////        binding.layoutChart.layoutCorrectlyAnswered.tvScore.text = getScoreText("chart")
//        binding.layoutChart.layoutTestScore.tvScoreNominator.text =
//            if (testAttemptsRes?.scoreListInfo!!.myScore != 0.0)
//                testAttemptsRes?.scoreListInfo!!.myScore.roundToInt().toString()
//            else
//                testAttemptsRes?.scoreListInfo!!.myScore.roundToInt().toString()
//
//        binding.layoutChart.layoutTestScore.tvScoreDenominator.text =
//            if (testAttemptsRes?.scoreListInfo!!.maxScore != 0.0)
//                testAttemptsRes?.scoreListInfo!!.maxScore.roundToInt().toString()
//            else
//                testAttemptsRes?.scoreListInfo!!.maxScore.roundToInt().toString()
//
//        binding.layoutChart.layoutTestScore.tvScoreNominator.setTextColor(getColor(R.color.colorReddishPink))
//    }
//
//    private fun setTopSkillRecycler() {
//
//        topSkillAdapter = TopSkillAdapter()
//        topSkillAdapter.setItems(listTopSkill)
//        binding.layoutClickToWatchAndTopSkill.layoutTopSkill.layoutCorrectlyAnswered.rvScoreBar.adapter =
//            topSkillAdapter
//
//        var scoreText = binding.layoutClickToWatchAndTopSkill.layoutTopSkill.layoutTestScore.tvScore
//        var questionStr = getString(R.string.test_feedback_questions)
//        var marksObtained = testSkillsRes?.correct_answer
//        var totalMarks = testSkillsRes?.questions
////        val correctAnswer = testSkillsRes?.correct_answer
////        val totalQuestions = testSkillsRes?.questions
//        var bothValueAsStr = "$marksObtained/$totalMarks"
//        scoreText.text =
//            Utils.getSpannable(
//                this, "$bothValueAsStr $questionStr",
//                R.color.colorGreen, 0, marksObtained.toString().length
//            )
//
////        binding.layoutClickToWatchAndTopSkill.layoutTopSkill.layoutTestScore.tvCorrectlyAnswered.text =
////            getScoreText("top_skill")
//
//        binding.layoutClickToWatchAndTopSkill.layoutTopSkill.layoutTestScore.tvTopSkillMemory.text =
//            testSkillsRes!!.top_skill
//    }
//
//    private fun getScoreText(type: String): String {
//
//        val questionStr = getString(R.string.test_feedback_questions)
//        val correctAnswer = testSkillsRes?.correct_answer
//        val totalQuestions = testSkillsRes?.questions
//        val bothValueAsStr = "$correctAnswer/$totalQuestions"
//        val text = "$bothValueAsStr $questionStr"
//
//        return when (type) {
//            "chart" -> {
//                Utils.getSpannable(
//                    this, "$bothValueAsStr $questionStr",
//                    R.color.colorGreen, 0, correctAnswer.toString().length
//                ).toString()
//            }
//            "top_skill" -> {
//
//                val styleSpan = StyleSpan(Typeface.BOLD)
//                val wordtoSpan: Spannable = SpannableStringBuilder(text)
//                wordtoSpan.setSpan(
//                    ForegroundColorSpan(Color.GREEN),
//                    text.indexOf(bothValueAsStr),
//                    text.indexOf("/"),
//                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
//                )
//                wordtoSpan.setSpan(styleSpan, 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//                "Correctly Answered $wordtoSpan"
//            }
//
//            else -> ""
//        }
//    }
//
//    private fun setAchieveRecycler() {
//        menuAdapter = DiagnosticTestFeedbackAdapter()
//        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
//        binding.layoutAchieve.recycler.adapter = menuAdapter
//        binding.layoutAchieve.recycler.layoutManager = layoutManager
//        menuAdapter.setData(getAchieveFeedback().steps as ArrayList<Step>)
//        menuAdapter.onItemLeftClick = {
//            coroutineScope.launch {
//                delay(10)
//                binding.layoutQWA.clQuestionWise.btnWastedAttempts.requestFocus()
//            }
//        }
//    }
//
//    /*load the AchieveFeedbackRes model data from the json response file*/
//    private fun getAchieveFeedback(): AchieveFeedbackRes {
//        return fromJson(achieveFeedbackResFromAsset() ?: "")
//    }
//
//    private fun achieveFeedbackResFromAsset(): String? {
//        var json: String? = null
//        try {
//            val inputStream: InputStream = assets.open("achieve_feedback_response_latest.json")
//            json = inputStream.bufferedReader().use { it.readText() }
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//            return null
//        }
//        return json
//    }
//
//    private fun setJarRecycler() {
//        jarAdapter = TestJarAdapter(this)
//        binding.layoutQWA.layoutJar.rvJar.adapter = jarAdapter
//        jarAdapter.setData(createJarData())
//
//        jarAdapter.onItemClick = { jarItem ->
//            performOnClickBasedOnJar(jarItem)
//            animateJar(jarItem.jar)
//        }
//    }
//
//    private fun performOnClickBasedOnJar(jarItem: TestFeedbackJarModel) {
//        when (jarItem.jar) {
//            R.drawable.jar_perfect_attempt -> perfectAttemptOnClick()
//            R.drawable.jar_too_fast_correct -> tooFastCorrectOnClick()
//            R.drawable.jar_overtime_correct -> overtimeCorrectOnClick()
//            R.drawable.jar_overtime_incorrect -> overtimeIncorrectOnClick()
//            R.drawable.jar_incorrect_answer -> incorrectOnClick()
//            R.drawable.jar_wasted_attempt -> wastedOnClick()
//            R.drawable.jar_unattempted -> unattemptedOnClick()
//        }
//    }
//
//    private fun setAchieveMetrics() {
//        if (!testAchieveRes?.achieve?.metric.isNullOrBlank()) {
//            var type = testAchieveRes?.achieve?.metric!!.capitalize()
//            var metricType = "Your\nCurrent\n$type"
//
//            var metricTypePotential = "Your\nPotential\n$type"
//
//            binding.layoutAchieve.tvSphere1Left.text = metricType
//            binding.layoutAchieve.tvSphere1Right.text = metricTypePotential
//        }
//        binding.layoutAchieve.tvSphere1.text =
//            if (testAchieveRes?.achieve?.current.isNullOrEmpty()
//                || testAchieveRes?.achieve?.current!!.toLowerCase().contains("null")
//            ) "NA" else testAchieveRes?.achieve?.current
//
//        binding.layoutAchieve.tvSphere2.text =
//            if (testAchieveRes?.achieve?.potential.isNullOrEmpty()
//                || testAchieveRes?.achieve?.potential!!.toLowerCase().contains("null")
//            ) "NA"
//            else testAchieveRes?.achieve?.potential
//    }
//
//    private fun setQWAWebView() {
//        binding.layoutQWA.wvQWA.webChromeClient = WebChromeClient()
//        binding.layoutQWA.wvQWA.webViewClient = object : WebViewClient() {
//
//            override fun onPageFinished(view: WebView, url: String) {
//                super.onPageFinished(view, url)
//                Log.d("WEBVIEW", "Web view ready")
//            }
//        }
//        binding.layoutQWA.wvQWA.addJavascriptInterface(
//            CommonAndroidAPI(this),
//            CommonAndroidAPI.NAME
//        )
//        binding.layoutQWA.wvQWA.settings.allowFileAccess = true
//        binding.layoutQWA.wvQWA.isLongClickable = false
//        binding.layoutQWA.wvQWA.settings.javaScriptEnabled = true
//        binding.layoutQWA.wvQWA.settings.cacheMode = WebSettings.LOAD_NO_CACHE
//        binding.layoutQWA.wvQWA.settings.domStorageEnabled = true
//        binding.layoutQWA.wvQWA.setBackgroundColor(Color.argb(0, 0, 0, 0))
//        binding.layoutQWA.wvQWA.setLayerType(LAYER_TYPE_SOFTWARE, null)
//        binding.layoutQWA.wvQWA.addJavascriptInterface(
//            CommonAndroidAPI(this),
//            CommonAndroidAPI.NAME
//        )
//        binding.layoutQWA.wvQWA.setLayerType(View.LAYER_TYPE_HARDWARE, null)
//        binding.layoutQWA.wvQWA.loadUrl("http://52.172.137.100:8000/?&invokeModule=question_list")
//
//    }
//
//    private fun setQuestionList(qlist: ArrayList<Question>) {
//        binding.layoutQWA.wvQWA.visibility = View.VISIBLE
//        var str = Gson().newBuilder().serializeNulls().create()
//            .toJson(qlist)
//        Log.i("json data", str)
//        binding.layoutQWA.wvQWA.evaluateJavascript("commonWebviewAPI.setQuestionList($str)", null)
//    }
//
//    private fun addViewsToHashMap() {
//        rowsHM[binding.layoutChart.constraintCharts.id] = binding.layoutChart.constraintCharts
//        rowsHM[binding.layoutClickToWatchAndTopSkill.clClickTopWatchAndTopSkill.id] =
//            binding.layoutClickToWatchAndTopSkill.clClickTopWatchAndTopSkill
//        rowsHM[binding.clNestedQwaAndAchieve.id] = binding.clNestedQwaAndAchieve
////        rowsHM[binding.tvQwaTitle.id] = binding.tvQwaTitle
////        rowsHM[binding.textAchieve.id] = binding.textAchieve
////        rowsHM[binding.layoutAchieve.layoutAchieve.id] = binding.layoutAchieve.layoutAchieve
////        rowsHM[binding.layoutQWA.clQuestionWiseParent.id] = binding.layoutQWA.clQuestionWiseParent
////        rowsHM[binding.layoutQWA.wvQWA.id] = binding.layoutQWA.wvQWA
//    }
//
//    private fun addFocusViewsToList() {
//        viewFocusList.add(binding.layoutChart.constraintCharts)
//        viewFocusList.add(binding.layoutClickToWatchAndTopSkill.clClickTopWatchAndTopSkill)
//        viewFocusList.add(binding.layoutQWA.tvDropDownSubject)
//    }
//
//    private fun setFirstRowListeners() {
////        binding.layoutChart.constraintCharts.clickWithDebounce(1000) {
////
////        }
//
//
//        binding.layoutChart.constraintCharts.setOnKeyListener { v, keyCode, event ->
//            when (event.action) {
//                KeyEvent.ACTION_DOWN -> {
//                    when (keyCode) {
//                        KeyEvent.KEYCODE_DPAD_DOWN -> {
//                            return@setOnKeyListener keyPressLimiter {
//                                smoothScrollRowsUp(binding.layoutChart.constraintCharts.id)
//                            }
//                        }
//
//                        KeyEvent.KEYCODE_NUMPAD_ENTER, KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
//                            return@setOnKeyListener keyPressLimiter {
////                                if (!isFirstRowExpanded)
//////                                    expandFirstRow()
////                                else
//////                                    shrinkFirstRow()
//                            }
//                        }
//                        KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_DPAD_LEFT -> {
//                            coroutineScope.launch {
//                                delay(1)
//                                binding.layoutChart.constraintCharts.requestFocus()
//                            }
//
//                        }
//                    }
//                }
//            }
//            false
//        }
//    }
//
//
//    private fun setClickToWatchListener() {
//
////        binding.layoutClickToWatchAndTopSkill.layoutClickToWatch.ivThumbnail.setOnKeyListener { v, keyCode, event ->
////            if (event.action == KeyEvent.ACTION_DOWN) {
////                when (keyCode) {
////                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
////                        /*val intent = Intent(this,
////                            VideoPOCActivity::class.java)
////                        intent.putExtra("data","achiever")
////                        startActivity(intent)*/
////                    }
////                }
////            }
////
////            false
////        }
//
//        binding.layoutClickToWatchAndTopSkill.layoutClickToWatch.constraintClickToWatch.setOnKeyListener { v, keyCode, event ->
//            when (event?.action) {
//                KeyEvent.ACTION_DOWN -> {
//                    when (keyCode) {
//                        KeyEvent.KEYCODE_DPAD_DOWN -> {
//                            return@setOnKeyListener keyPressLimiter {
//                                smoothScrollRowsUp(binding.layoutClickToWatchAndTopSkill.clClickTopWatchAndTopSkill.id)
//                            }
//                        }
//                        KeyEvent.KEYCODE_DPAD_UP -> {
//                            return@setOnKeyListener keyPressLimiter {
//                                smoothScrollRowsDown(binding.layoutChart.constraintCharts.id)
//                            }
//                        }
//                    }
//                }
//            }
//            false
//        }
//    }
//
//    private fun setQWAKeyListener() {
//        binding.layoutQWA.tvDropDownSubject.setOnKeyListener { v, keyCode, event ->
//            when (event?.action) {
//                KeyEvent.ACTION_DOWN -> {
//                    when (keyCode) {
//                        KeyEvent.KEYCODE_DPAD_UP ->
//                            return@setOnKeyListener keyPressLimiter {
//                                smoothScrollRowsDown(binding.layoutClickToWatchAndTopSkill.clClickTopWatchAndTopSkill.id)
//                            }
//                    }
//                }
//            }
//            false
//        }
//    }
//
//    private fun setQWAButtonListener() {
//
//        binding.layoutQWA.btnPerfectAttempts.setOnClickListener {
//            perfectAttemptOnClick()
//        }
//        binding.layoutQWA.btnOverTimeCorrectAttempts.setOnClickListener {
//            overtimeCorrectOnClick()
//        }
//        binding.layoutQWA.btnTooFastCorrectAttempts.setOnClickListener {
//            tooFastCorrectOnClick()
//        }
//        binding.layoutQWA.btnInCorrectAttempts.setOnClickListener {
//            incorrectOnClick()
//        }
//        binding.layoutQWA.btnOverTimeInCorrectAttempts.setOnClickListener {
//            overtimeIncorrectOnClick()
//        }
//        binding.layoutQWA.btnWastedAttempts.setOnClickListener {
//            wastedOnClick()
//        }
//        binding.layoutQWA.btnUnAttempted.setOnClickListener {
//            unattemptedOnClick()
//        }
//    }
//
//    private fun overtimeCorrectOnClick() {
//        val questionList = getQuestionListData(currentQWASubject, OvertimeCorrect.name)
//        if (questionList.size > 0)
//            setQuestionList(questionList)
//        else {
//            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
//        }
//        if (isJarEnabled) {
//            setAttemptQualityView(R.drawable.jar_overtime_correct)
//        }
//    }
//
//    private fun tooFastCorrectOnClick() {
//        val questionList = getQuestionListData(currentQWASubject, TooFastCorrect.name)
//        if (questionList.size > 0)
//            setQuestionList(questionList)
//        else {
//            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
//        }
//        if (isJarEnabled) {
//            setAttemptQualityView(R.drawable.jar_too_fast_correct)
//        }
//    }
//
//    private fun incorrectOnClick() {
//        val questionList = getQuestionListData(currentQWASubject, Incorrect.name)
//        if (questionList.size > 0)
//            setQuestionList(questionList)
//        else {
//            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
//        }
//        if (isJarEnabled) {
//            setAttemptQualityView(R.drawable.jar_incorrect_answer)
//        }
//    }
//
//    private fun overtimeIncorrectOnClick() {
//        val questionList = getQuestionListData(currentQWASubject, OvertimeIncorrect.name)
//        if (questionList.size > 0)
//            setQuestionList(questionList)
//        else {
//            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
//        }
//        if (isJarEnabled) {
//            setAttemptQualityView(R.drawable.jar_overtime_incorrect)
//        }
//    }
//
//    private fun unattemptedOnClick() {
//        val questionList = getQuestionListData(currentQWASubject, NonAttempt.name)
//        if (questionList.size > 0)
//            setQuestionList(questionList)
//        else {
//            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
//        }
//        if (isJarEnabled) {
//            setAttemptQualityView(R.drawable.jar_unattempted)
//        }
//    }
//
//    private fun wastedOnClick() {
//        val questionList = getQuestionListData(currentQWASubject, Wasted.name)
//        if (questionList.size > 0)
//            setQuestionList(questionList)
//        else {
//            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
//        }
//        if (isJarEnabled) {
//            setAttemptQualityView(R.drawable.jar_wasted_attempt)
//        }
//    }
//
//    private fun perfectAttemptOnClick() {
//        val questionList = getQuestionListData(currentQWASubject, Perfect.name)
//        if (questionList.size > 0)
//            setQuestionList(questionList)
//        else {
//            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
//        }
//        if (isJarEnabled) {
//            setAttemptQualityView(R.drawable.jar_perfect_attempt)
//        }
//    }
//
//    private fun setAndAnimateProgress() {
//        val goodScore = testAttemptsRes?.scoreListInfo?.goodScore!!
//        val yourScore = testAttemptsRes?.scoreListInfo?.myScore!!
//        val cutOffScore = testAttemptsRes?.scoreListInfo?.cutOffScore!!
//        var totalScore = 100
//        //to avoid values going of screen
//        var extraBiasSpace = getHorizontalBiasOfBaseLine(totalScore)
//        totalScore += extraBiasSpace
//        totalScore += 20
//        if (mySectionSummaryList.size >= 5)
//            changeMotionLayoutConstraintSetToFit()
//
//        animate(
//            goodScore.toInt(),
//            totalScore,
//            binding.layoutChart.layoutFeedbackProgress.progressBarGoodScore,
//            binding.layoutChart.layoutFeedbackProgress.tvGoodScore
//        )
////        removeMarginTopFromGoodScoreProgressBar(goodScore)
//        moveVerticalBaseLineBiasBasedOnScore(yourScore, totalScore)
//        moveCutOffLineBiasBasedOnScore(cutOffScore, totalScore)
//        moveHorizontalCutOffMarkerBiasBasedOnScore(cutOffScore, totalScore)
//        if (yourScore < 0) {
//            animate(
//                yourScore.toInt(),
//                totalScore,
//                binding.layoutChart.layoutFeedbackProgress.progressBarNegative,
//                binding.layoutChart.layoutFeedbackProgress.tvNegativeScore
//            )
//        } else {
//            animate(
//                yourScore.toInt(),
//                totalScore,
//                binding.layoutChart.layoutFeedbackProgress.progressBarYourScore,
//                binding.layoutChart.layoutFeedbackProgress.tvYourScore
//            )
//        }
//    }
//
//    private fun removeMarginTopFromGoodScoreProgressBar(goodScore: Double) {
//        var params =
//            binding.layoutChart.layoutFeedbackProgress.tvGoodScore.layoutParams as ViewGroup.MarginLayoutParams
//        if (goodScore > 0.0) {
//            params.topMargin = 0
//            binding.layoutChart.layoutFeedbackProgress.tvGoodScore.layoutParams = params
//        } else if (goodScore == 0.0) {
//            params.topMargin = 20
//            binding.layoutChart.layoutFeedbackProgress.tvGoodScore.layoutParams = params
//        }
//    }
//
//    private fun getHorizontalBiasOfBaseLine(totalScore: Int): Int {
//        var params =
//            binding.layoutChart.layoutCorrectlyAnswered.viewLine.layoutParams as ConstraintLayout.LayoutParams
//        return params.horizontalBias.times(totalScore).toInt()
//    }
//
//    private fun moveVerticalBaseLineBiasBasedOnScore(yourScore: Double, totalScore: Int) {
//        if (yourScore < 0) {
//            val baseLineVertical = binding.layoutChart.layoutFeedbackProgress.viewLine
//            val baseLineParams = baseLineVertical.layoutParams as ConstraintLayout.LayoutParams
//            baseLineParams.horizontalBias =
//                baseLineParams.horizontalBias + (abs(yourScore).toFloat() / totalScore)
//            baseLineVertical.layoutParams = baseLineParams
//            baseLineVertical.requestLayout()
//        }
//    }
//
//    private fun moveCutOffLineBiasBasedOnScore(cutOffScore: Int, totalScore: Int) {
//        // AnyScore/totalScore to get bias value
//        //totalMarks 130, Negative Marks -12
//
//        val calScore = (cutOffScore.toDouble().div(totalScore))
//        Log.e(TAG, "cal Score $calScore")
//        val cutOffLine = binding.layoutChart.layoutFeedbackProgress.ivCutOffMarker
//        val cutOffLabel = binding.layoutChart.layoutFeedbackProgress.tvCutOffMarkerLabel
//        val cutOffLineParams = cutOffLine.layoutParams as ConstraintLayout.LayoutParams
//        val cutOffLineAnimator = ValueAnimator.ofFloat(0f, calScore.toFloat())
//
//        cutOffLineAnimator.addUpdateListener { value ->
//            cutOffLineParams.horizontalBias = (value.animatedValue as Float)
//            cutOffLine.layoutParams = cutOffLineParams
//        }
//
//        val cutOffLabelAnimator = ValueAnimator.ofInt(0, cutOffScore)
//        cutOffLabelAnimator.addUpdateListener { value ->
//            cutOffLabel.text = (value.animatedValue as Int).toString()
//        }
//
//        val animatorSet = AnimatorSet()
//        animatorSet.playTogether(cutOffLabelAnimator, cutOffLineAnimator)
//        animatorSet.duration = 1500
//        animatorSet.start()
//    }
//
//    private fun moveHorizontalCutOffMarkerBiasBasedOnScore(
//        cutOffScore: Int,
//        totalScore: Int
//    ) {
//        var endSet: ConstraintSet = correctlyAnsweredMotionLayout?.getConstraintSet(R.id.end)!!
//        endSet.setVerticalBias(R.id.iv_cut_off_marker, calculateCutOffBias(cutOffScore))
//        endSet.applyTo(correctlyAnsweredMotionLayout)
////        animateBar(binding.layoutChart.layoutCorrectlyAnswered.ivCutOffHolder, cutOffScore)
//
//        val cutOffHorizontalTextView = binding.layoutChart.layoutCorrectlyAnswered.tvCutOffLabel
//        val cutOffStr = getString(R.string.cut_off)
//        val marksStr = getString(R.string.marks)
//        cutOffHorizontalTextView.text = "$cutOffStr $cutOffScore $marksStr"
//    }
//
//    private fun calculateCutOffBias(cutOffScore: Int): Float {
//        if (cutOffScore >= 10 && cutOffScore <= 20) {
//            return 0.28f
//        } else if (cutOffScore >= 20 && cutOffScore <= 40) {
//            return 0.22f
//        } else if (cutOffScore >= 40 && cutOffScore <= 60) {
//            return 0.17f
//        } else if (cutOffScore >= 60 && cutOffScore <= 80) {
//            return 0.08f
//        } else
//            return 0.01f
//    }
//
//    private fun animateBar(bar: ImageView, progressValue: Int) {
//        var animator = ValueAnimator.ofInt(0, progressValue)
//        var listener: ValueAnimator.AnimatorUpdateListener? = null
//        listener = ValueAnimator.AnimatorUpdateListener { valueAnimator ->
//            var value = valueAnimator.animatedValue
//            var params = bar.layoutParams as ConstraintLayout.LayoutParams
//            if (value as Int > 0) {
//                params.height = value
//                bar.visibility = View.VISIBLE
//            } else
//                bar.visibility = View.GONE
//            params.height = value
//            bar.layoutParams = params
//
//            if (value == progressValue)
//                animator.removeUpdateListener(listener!!)
//        }
//        animator.addUpdateListener(listener)
//
//        animator.duration = 1500
//        animator.start()
//    }
//
//    private fun changeMotionLayoutConstraintSetToFit() {
//        val startSet: ConstraintSet = correctlyAnsweredMotionLayout?.getConstraintSet(R.id.start)!!
//        startSet.connect(R.id.tv_currently_answered, TOP, R.id.iv_text_score, TOP, 10)
//        startSet.connect(R.id.tv_currently_answered, START, R.id.iv_text_score, START)
//        startSet.connect(R.id.tv_currently_answered, END, R.id.iv_text_score, END)
//        startSet.connect(R.id.tv_currently_answered, BOTTOM, R.id.iv_text_score, BOTTOM)
//        startSet.connect(R.id.tv_score, START, R.id.tv_currently_answered, END, 5)
//        startSet.connect(R.id.tv_score, TOP, R.id.tv_currently_answered, TOP)
//        startSet.connect(R.id.tv_score, BOTTOM, R.id.tv_currently_answered, BOTTOM)
//        startSet.connect(R.id.tv_score, END, R.id.iv_text_score, END)
//        startSet.connect(R.id.rv_score_bar, START, constraintLayout.id, START)
//        startSet.connect(R.id.rv_score_bar, END, constraintLayout.id, END)
//        startSet.connect(R.id.rv_score_bar, TOP, constraintLayout.id, TOP)
//        startSet.connect(R.id.rv_score_bar, BOTTOM, constraintLayout.id, BOTTOM)
//        startSet.setVerticalBias(R.id.rv_score_bar, 0.7f)
//        startSet.setHorizontalBias(R.id.rv_score_bar, 0.3f)
//        startSet.setHorizontalBias(R.id.tv_currently_answered, 0.3f)
//        startSet.setVerticalBias(R.id.tv_currently_answered, 0.0f)
//        startSet.setHorizontalBias(R.id.tv_score, 0.0f)
//        startSet.applyTo(correctlyAnsweredMotionLayout)
//
//        val endSet: ConstraintSet = correctlyAnsweredMotionLayout?.getConstraintSet(R.id.end)!!
//        endSet.connect(R.id.tv_currently_answered, TOP, R.id.iv_text_score, TOP, 20)
//        endSet.connect(R.id.tv_currently_answered, START, R.id.iv_text_score, START)
//        endSet.connect(R.id.tv_currently_answered, END, R.id.iv_text_score, END)
//        endSet.connect(R.id.tv_currently_answered, BOTTOM, R.id.iv_text_score, BOTTOM)
//        endSet.connect(R.id.tv_score, START, R.id.tv_currently_answered, END, 5)
//        endSet.connect(R.id.tv_score, TOP, R.id.tv_currently_answered, TOP)
//        endSet.connect(R.id.tv_score, BOTTOM, R.id.tv_currently_answered, BOTTOM)
//        endSet.connect(R.id.tv_score, END, R.id.iv_text_score, END)
//        endSet.connect(R.id.rv_score_bar, START, constraintLayout.id, START)
//        endSet.connect(R.id.rv_score_bar, END, constraintLayout.id, END)
//        endSet.connect(R.id.rv_score_bar, TOP, constraintLayout.id, TOP)
//        endSet.connect(R.id.rv_score_bar, BOTTOM, constraintLayout.id, BOTTOM)
//        endSet.setVerticalBias(R.id.rv_score_bar, 0.7f)
//        endSet.setHorizontalBias(R.id.rv_score_bar, 0.3f)
//        endSet.setHorizontalBias(R.id.tv_currently_answered, 0.3f)
//        endSet.setVerticalBias(R.id.tv_currently_answered, 0.0f)
//        endSet.setHorizontalBias(R.id.tv_score, 0.0f)
//        endSet.applyTo(correctlyAnsweredMotionLayout)
//    }
//
//    private fun animate(progressValue: Int, totalScore: Int, progressBar: ImageView, tv: TextView) {
//        animateBar(progressBar, progressValue, totalScore)
//        tv.animateWithProgressBar(progressValue)
//    }
//
//    private fun TextView.animateWithProgressBar(progressValue: Int) {
//
//        val animator = ObjectAnimator.ofInt(0, progressValue)
//        animator.addUpdateListener { animationValue ->
//            this.text = animationValue.animatedValue.toString()
//        }
//        val animatorSet = AnimatorSet()
//        animatorSet.playTogether(animator)
//        animatorSet.duration = 2000
//        animatorSet.start()
//    }
//
//    private fun animateBar(bar: ImageView, progressValue: Int, totalScore: Int) {
//        val progressLayout = binding.layoutChart.layoutFeedbackProgress
//        val vto = progressLayout.root.viewTreeObserver
//        var newProgressValue = 0
//
//        newProgressValue = kotlin.math.abs(progressValue)
//        var listener: ViewTreeObserver.OnGlobalLayoutListener? = null
//
//        listener = ViewTreeObserver.OnGlobalLayoutListener {
//            progressLayoutWidth = progressLayout.root.width
//            Log.e(TAG, "width $progressLayoutWidth")
//            height = progressLayout.root.height
//
//            val calValue = newProgressValue.toDouble() / totalScore * progressLayoutWidth
//            val animator = ValueAnimator.ofInt(0, calValue.toInt())
//            animator.addUpdateListener { valueAnimator ->
//                val value = valueAnimator.animatedValue
//                val params = bar.layoutParams as ConstraintLayout.LayoutParams
//                if (value as Int > 0) {
//                    bar.visibility = View.VISIBLE
//                } else
//                    bar.visibility = View.GONE
//                params.width = value
//                bar.layoutParams = params
//            }
//
//            animator.duration = 1500
//            animator.start()
//
//            binding.layoutChart.layoutFeedbackProgress.root.viewTreeObserver?.removeOnGlobalLayoutListener(
//                listener!!
//            )
//        }
//        vto?.addOnGlobalLayoutListener(listener)
//    }
//
//    private fun expandFirstRow() {
//        animateCorrectlyAnsweredText()
//        scaleRvBar()
//        val params = binding.layoutChart.constraintCharts.layoutParams as LinearLayout.LayoutParams
//        Log.e(TAG, "height: ${params.height}")
//        val animator = ValueAnimator.ofInt(0, 130)
//        animator.addUpdateListener { animatorValue ->
//            val value = animatorValue.animatedValue
//            val calVal = 240 + value as Int
//            params.height = calVal
//            Log.e(TAG, "height: ${params.height}")
//            binding.layoutChart.constraintCharts.layoutParams = params
//
//        }
//        animator.duration = 1000
//        animator.start()
//        isFirstRowExpanded = true
//        binding.layoutChart.layoutTestScore.tvGradeLabel.visibility = View.VISIBLE
//        binding.layoutChart.layoutTestScore.tvGrade.visibility = View.VISIBLE
//    }
//
//    private fun shrinkFirstRow() {
//        animateCorrectlyAnsweredText()
//        binding.layoutChart.layoutCorrectlyAnswered.rvScoreBar.scale()
//        binding.layoutChart.layoutTestScore.tvGrade.visibility = View.GONE
//        binding.layoutChart.layoutTestScore.tvGradeLabel.visibility = View.GONE
//
//        val params =
//            binding.layoutChart.constraintCharts.layoutParams as LinearLayout.LayoutParams
//        Log.e(TAG, "height: ${params.height}")
//        val animator = ValueAnimator.ofInt(0, 190)
//        animator.addUpdateListener { animatorValue ->
//            val value = animatorValue.animatedValue
//            val calVal = 400 - value as Int
//            params.height = calVal
//            Log.e(TAG, "height: ${params.height}")
//            binding.layoutChart.constraintCharts.layoutParams = params
//        }
//        animator.duration = 1000
//        animator.start()
//        isFirstRowExpanded = false
//    }
//
//    private fun scaleRvBar() {
//        val rvScoreBar = binding.layoutChart.layoutCorrectlyAnswered.rvScoreBar
//        if (mySectionSummaryList.size >= 5) {
//            rvScoreBar.scale(1f, 1.2f)
//        } else if (mySectionSummaryList.size >= 2 || mySectionSummaryList.size <= 4) {
//            rvScoreBar.scale(1.2f, 1.2f)
//        }
//    }
//
//    private fun RecyclerView.scale(x: Float = 1f, y: Float = 1f) {
//        this.animate()?.scaleX(x)?.duration = 1000
//        this.animate()?.scaleY(y)?.duration = 1000
//    }
//
//    private fun animateCorrectlyAnsweredText() {
//        val motionLayout = binding.layoutChart.layoutCorrectlyAnswered.constraintLayout
//        motionLayout.transitionToStart()
//        motionLayout.transitionToEnd()
//    }
//
//    private fun keyPressLimiter(threshHold: Long = 1500L, action: () -> Unit): Boolean {
//        if (SystemClock.elapsedRealtime() - lastKeyPress < threshHold) {
//            return true
//        } else {
//            action.invoke()
//        }
//        lastKeyPress = SystemClock.elapsedRealtime()
//        return false
//    }
//
//    private fun smoothScrollRowsUp(viewTranslatingIndex: Int) {
//        viewFocusCounter++
//        rowsHM.forEach { (otherViewsIndex, _) ->
//            if (otherViewsIndex != viewTranslatingIndex) {
//                slideRow(
//                    rowsHM[otherViewsIndex]!!,
//                    -rowsHM[viewTranslatingIndex]?.translationY!!.toFloat()
//                        .minus(rowsHM[viewTranslatingIndex]?.height!!.toFloat()),
//                    false,
//                    rowsHM[viewTranslatingIndex]?.translationY!!.toFloat()
//                )
//            } else
//                slideRow(
//                    rowsHM[viewTranslatingIndex]!!,
//                    -rowsHM[viewTranslatingIndex]?.translationY!!.toFloat()
//                        .minus(rowsHM[viewTranslatingIndex]?.height!!.toFloat()),
//                    true,
//                    rowsHM[viewTranslatingIndex]?.translationY!!.toFloat(),
//                    1f, 0f
//                )
//        }
//        coroutineScope.launch {
//            delay(10)
//            if (viewFocusCounter < viewFocusList.size) {
//                viewFocusList[viewFocusCounter].requestFocus()
//            }
//        }
//    }
//
//    private fun smoothScrollRowsDown(viewTranslatingIndex: Int) {
//        rowsHM.forEach { (otherViewsIndex, _) ->
//            if (otherViewsIndex != viewTranslatingIndex) {
//                slideRow(
//                    rowsHM[otherViewsIndex]!!,
//                    -rowsHM[viewTranslatingIndex]?.height!!.toFloat()
//                        .plus(rowsHM[viewTranslatingIndex]!!.translationY),
//                    false,
//                    rowsHM[viewTranslatingIndex]!!.translationY
//                )
//            } else {
//                slideRow(
//                    rowsHM[viewTranslatingIndex]!!,
//                    -rowsHM[viewTranslatingIndex]?.height!!.toFloat()
//                        .plus(rowsHM[viewTranslatingIndex]!!.translationY),
//                    true,
//                    rowsHM[viewTranslatingIndex]!!.translationY,
//                    0f,
//                    1f
//                )
//            }
//        }
//
//        coroutineScope.launch {
//            delay(10)
//            if (viewFocusCounter > 0 && viewFocusCounter < viewFocusList.size) {
//                viewFocusList[--viewFocusCounter].requestFocus()
//            }
//        }
//    }
//
//    private fun slideRow(
//        view: View,
//        viewHeight: Float = 0f,
//        canFade: Boolean = false,
//        updatedTransYValue: Float = 0f,
//        alphaFrom: Float = 0f,
//        alphaTo: Float = 1f
//    ) {
//        val transYObjectAnimator =
//            ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, updatedTransYValue, -viewHeight)
//        var alphaObjectAnimator: ObjectAnimator? = null
//        if (canFade) {
//            alphaObjectAnimator =
//                ObjectAnimator.ofFloat(view, View.ALPHA, alphaFrom, alphaTo)
//        }
//
//        val animatorSet = AnimatorSet()
//        if (alphaObjectAnimator != null)
//            animatorSet.playTogether(transYObjectAnimator, alphaObjectAnimator)
//        else
//            animatorSet.playTogether(transYObjectAnimator)
//
//        animatorSet.duration = RowAnimDelayValues.ANIM_DELAY
//        animatorSet.start()
//    }
//
//    private fun View.clickWithDebounce(debounceTime: Long = 600L, action: () -> Unit) {
//        this.setOnClickListener(object : View.OnClickListener {
//            private var lastClickTime: Long = 0
//
//            override fun onClick(v: View) {
//                if (SystemClock.elapsedRealtime() - lastClickTime < debounceTime) return
//                else action()
//
//                lastClickTime = SystemClock.elapsedRealtime()
//            }
//        })
//    }
//
//    private fun createAchieveData(): ArrayList<TestFeedbackMenuModel> {
//        return arrayListOf(
//            TestFeedbackMenuModel(
//                "Achieve Amazing Results with your \npersonalised achievement journey",
//                arrayListOf(), R.drawable.achieve_banner
//
//            ),
//            TestFeedbackMenuModel(
//                "Get Your Revision Lists", getRevisionList(), R.drawable.revision_list
//            ),
//            TestFeedbackMenuModel(
//                "Improve Your \nTest Taking Strategy",
//                getImprovingStrategyList(), R.drawable.test_taking_banner
//            )
//        )
//    }
//
//    private fun getRevisionList(): ArrayList<SubFeedbackMenuModel> {
//        val revisions = testAchieveRes?.revisions
//        var revisionList = ArrayList<SubFeedbackMenuModel>()
//        for (index in 0 until revisions?.size!!) {
//            revisionList.add(SubFeedbackMenuModel(revisions[index].display_name!!, arrayListOf()))
//        }
//
//        return revisionList
//    }
//
//    private fun getImprovingStrategyList(): ArrayList<SubFeedbackMenuModel> {
//        val improvingStrategy = testAchieveRes?.improveStrategy
//        val improveStrategyList = ArrayList<SubFeedbackMenuModel>()
//
//        if (improvingStrategy?.positiveBehaviour?.size != 0) {
//            val behaviourList = ArrayList<TestBehaviour>()
//            for (index in 0 until improvingStrategy?.positiveBehaviour?.size!!) {
//                behaviourList.add(improvingStrategy.positiveBehaviour!![index])
//            }
//            improveStrategyList.add(
//                SubFeedbackMenuModel(
//                    "Understand My Positive Behaviour",
//                    behaviourList
//                )
//            )
//        }
//
//        if (improvingStrategy.negativeBehaviour?.size != 0) {
//            val behaviourList = ArrayList<TestBehaviour>()
//            for (index in 0 until improvingStrategy.negativeBehaviour?.size!!) {
//                behaviourList.add(improvingStrategy.negativeBehaviour!![index])
//            }
//            improveStrategyList.add(
//                SubFeedbackMenuModel(
//                    "Understand My Negative Behaviour",
//                    behaviourList
//                )
//            )
//        }
//
//        return improveStrategyList
//    }
//
//    private fun createJarData(): ArrayList<TestFeedbackJarModel> {
//        return arrayListOf(
//            TestFeedbackJarModel(
//                getQuestionListData(currentQWASubject, TooFastCorrect.name).size,
//                R.drawable.jar_too_fast_correct
//            ),
//            TestFeedbackJarModel(
//                getQuestionListData(currentQWASubject, Perfect.name).size,
//                R.drawable.jar_perfect_attempt
//            ),
//            TestFeedbackJarModel(
//                getQuestionListData(currentQWASubject, OvertimeCorrect.name).size,
//                R.drawable.jar_overtime_correct
//            ),
//            TestFeedbackJarModel(
//                getQuestionListData(currentQWASubject, Wasted.name).size,
//                R.drawable.jar_wasted_attempt
//            ),
//            TestFeedbackJarModel(
//                getQuestionListData(currentQWASubject, Incorrect.name).size,
//                R.drawable.jar_incorrect_answer
//            ),
//            TestFeedbackJarModel(
//                getQuestionListData(
//                    currentQWASubject,
//                    OvertimeIncorrect.name
//                ).size, R.drawable.jar_overtime_incorrect
//            ),
//            TestFeedbackJarModel(
//                getQuestionListData(currentQWASubject, NonAttempt.name).size,
//                R.drawable.jar_unattempted
//            )
//        )
//    }
//
////    private fun animateJar() {
////        binding.layoutQWA.layoutJar.rvJar.visibility = View.GONE
////        binding.layoutQWA.layoutAQ.qaMain.visibility = View.VISIBLE
////    }
//
//    override fun onJarSelected(item: TestFeedbackJarModel) {
//        animateJar(item.jar)
//        isJarEnabled = true
//    }
//
//    private fun animateJar(jarType: Int) {
//        binding.layoutQWA.layoutAQ.qaMain.visibility = View.GONE
//        when (jarType) {
//            R.drawable.jar_too_fast_correct -> ObjectAnimator.ofFloat(
//                binding.layoutQWA.imgAttemptJarIcon,
//                "translationX",
//                60f
//            ).apply {
//                duration = 500
//                addListener(object : Animator.AnimatorListener {
//                    override fun onAnimationStart(animation: Animator) {
//                        // 3
//                        viewAnimationImage(1)
//                        //getAttemptQualityData()
//                        tooFastCorrectOnClick()
//
//                    }
//
//                    override fun onAnimationEnd(animation: Animator) {
//                        // 4
//                        //getAttemptQualityData()
//                        setAttemptQualityView(jarType)
//                        binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
//                        binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
//                            .translationY(0f).duration = 500
//
//                    }
//
//                    override fun onAnimationCancel(animation: Animator) {}
//
//                    override fun onAnimationRepeat(animation: Animator) {}
//                })
//                start()
//            }
//            R.drawable.jar_perfect_attempt -> ObjectAnimator.ofFloat(
//                binding.layoutQWA.imgAttemptJarIcon,
//                "translationX",
//                20f
//            ).apply {
//                duration = 500
//                addListener(object : Animator.AnimatorListener {
//                    override fun onAnimationStart(animation: Animator) {
//                        // 3
//                        viewAnimationImage(2)
//                        perfectAttemptOnClick()
//
//                    }
//
//                    override fun onAnimationEnd(animation: Animator) {
//                        // 4
//                        // getAttemptQualityData()
//                        setAttemptQualityView(jarType)
//                        binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
//                        binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
//                            .translationY(0f).duration = 500
//                    }
//
//                    override fun onAnimationCancel(animation: Animator) {}
//                    override fun onAnimationRepeat(animation: Animator) {}
//                })
//                start()
//            }
//            R.drawable.jar_overtime_correct -> ObjectAnimator.ofFloat(
//                binding.layoutQWA.imgAttemptJarIcon,
//                "translationX",
//                -60f
//            ).apply {
//                duration = 500
//                addListener(object : Animator.AnimatorListener {
//                    override fun onAnimationStart(animation: Animator) {
//                        // 3
//                        viewAnimationImage(3)
//                        overtimeCorrectOnClick()
//                    }
//
//                    override fun onAnimationEnd(animation: Animator) {
//                        // 4
//                        //getAttemptQualityData()
//                        setAttemptQualityView(jarType)
//                        binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
//                        binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
//                            .translationY(0f).duration = 500
//
//                    }
//
//                    override fun onAnimationCancel(animation: Animator) {}
//
//                    override fun onAnimationRepeat(animation: Animator) {}
//                })
//                start()
//            }
//            R.drawable.jar_wasted_attempt -> ObjectAnimator.ofFloat(
//                binding.layoutQWA.imgAttemptJarIcon,
//                "translationX",
//                -120f
//            ).apply {
//                duration = 500
//                addListener(object : Animator.AnimatorListener {
//                    override fun onAnimationStart(animation: Animator) {
//                        // 3
//                        viewAnimationImage(4)
//                        wastedOnClick()
//                    }
//
//                    override fun onAnimationEnd(animation: Animator) {
//                        // 4
//                        //getAttemptQualityData()
//                        setAttemptQualityView(jarType)
//                        binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
//                        binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
//                            .translationY(0f).duration = 500
//
//
//                    }
//
//                    override fun onAnimationCancel(animation: Animator) {}
//
//                    override fun onAnimationRepeat(animation: Animator) {}
//                })
//                start()
//            }
//            R.drawable.jar_incorrect_answer -> ObjectAnimator.ofFloat(
//                binding.layoutQWA.imgAttemptJarIcon,
//                "translationX",
//                -180f
//            ).apply {
//                duration = 500
//                addListener(object : Animator.AnimatorListener {
//                    override fun onAnimationStart(animation: Animator) {
//                        // 3
//                        viewAnimationImage(5)
//                        incorrectOnClick()
//                    }
//
//                    override fun onAnimationEnd(animation: Animator) {
//                        // 4
//                        //getAttemptQualityData()
//                        setAttemptQualityView(jarType)
//                        binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
//                        binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
//                            .translationY(0f).duration = 500
//
//
//                    }
//
//                    override fun onAnimationCancel(animation: Animator) {}
//
//                    override fun onAnimationRepeat(animation: Animator) {}
//                })
//                start()
//            }
//            R.drawable.jar_overtime_incorrect -> ObjectAnimator.ofFloat(
//                binding.layoutQWA.imgAttemptJarIcon,
//                "translationX",
//                -240f
//            ).apply {
//                duration = 500
//                addListener(object : Animator.AnimatorListener {
//                    override fun onAnimationStart(animation: Animator) {
//                        // 3
//                        viewAnimationImage(6)
//                        overtimeIncorrectOnClick()
//
//                    }
//
//                    override fun onAnimationEnd(animation: Animator) {
//                        // 4
//                        //getAttemptQualityData()
//                        setAttemptQualityView(jarType)
//                        binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
//                        binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
//                            .translationY(0f).duration = 500
//                    }
//
//                    override fun onAnimationCancel(animation: Animator) {}
//                    override fun onAnimationRepeat(animation: Animator) {}
//                })
//                start()
//            }
//            R.drawable.jar_unattempted -> ObjectAnimator.ofFloat(
//                binding.layoutQWA.imgAttemptJarIcon,
//                "translationX",
//                -260f
//            ).apply {
//                duration = 500
//                addListener(object : Animator.AnimatorListener {
//                    override fun onAnimationStart(animation: Animator) {
//                        // 3
//                        viewAnimationImage(7)
//                        unattemptedOnClick()
//
//                    }
//
//                    override fun onAnimationEnd(animation: Animator) {
//                        // 4
//                        //getAttemptQualityData()
//                        setAttemptQualityView(jarType)
//                        binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
//                        binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
//                            .translationY(0f).duration = 500
//                    }
//
//                    override fun onAnimationCancel(animation: Animator) {}
//                    override fun onAnimationRepeat(animation: Animator) {}
//                })
//                start()
//            }
//        }
//    }
//
//    private fun viewAnimationImage(position: Int) {
//
//        binding.layoutQWA.focusedBtn.requestFocus()
//
//        binding.layoutQWA.layoutJar.rvJar.visibility = View.GONE
//        isJarEnabled = true
//        binding.layoutQWA.imgAttemptJarIcon.alpha = 1.0F
//        binding.layoutQWA.imgAttemptJarIcon.visibility = View.VISIBLE
//        when (position) {
//            1 -> {
//                binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_too_fast_correct)
//            }
//            2 -> {
//                binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_perfect_attempt)
//
//            }
//            3 -> {
//                binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_correct)
//
//            }
//            4 -> {
//                binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_wasted_attempt)
//
//            }
//            5 -> {
//                binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_incorrect_answer)
//
//            }
//            6 -> {
//                binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_incorrect)
//
//            }
//            7 -> {
//                binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_unattempted)
//
//            }
//        }
//        val lp = LinearLayout.LayoutParams(
//            LinearLayout.LayoutParams.WRAP_CONTENT,
//            LinearLayout.LayoutParams.WRAP_CONTENT
//        )
//        if (position < 5)
//            lp.setMargins((position - 1) * 60, 20, 0, 0)
//        if (position == 5)
//            lp.setMargins((position - 1) * 70, 20, 0, 0)
//        if (position == 6)
//            lp.setMargins((position - 1) * 80, 20, 0, 0)
//
//        binding.layoutQWA.imgAttemptJarIcon.layoutParams = lp
//    }
//
//    private fun setAttemptQualityView(jarType: Int) {
//        this.jarType = jarType
////        binding.layoutQWA.focusedBtn.requestFocus()
//        binding.layoutQWA.layoutJar.rvJar.visibility = View.GONE
//        binding.layoutQWA.layoutAQ.qaMain.visibility = View.VISIBLE
//        binding.layoutQWA.layoutAQ.qaMain.requestFocus()
//        when (jarType) {
//            R.drawable.jar_too_fast_correct -> binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(
//                R.raw.new_jar_correct
//            )
//            R.drawable.jar_perfect_attempt -> binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(R.raw.new_jar_perfect_attempts)
//            R.drawable.jar_wasted_attempt -> binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(R.raw.new_jar_wasted_attempt)
//            R.drawable.jar_incorrect_answer -> binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(R.raw.new_jar_wrong)
//            R.drawable.jar_overtime_incorrect -> binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(
//                R.raw.new_jar_overtime_incorrect
//            )
//            R.drawable.jar_unattempted -> binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(R.raw.new_jar_overtime_incorrect)
//            R.drawable.jar_overtime_correct -> binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(R.raw.new_jar_overtime_correct)
//        }
//        binding.layoutQWA.layoutAQ.aqImageJar.loop(true)
//        binding.layoutQWA.layoutAQ.aqImageJar.playAnimation()
//    }
//
//
//    override fun focusUp() {
//        super.focusUp()
//        runOnUiThread {
//            currentFocus?.clearFocus()
//            binding.layoutQWA.btnUnAttempted.postDelayed({
//                binding.layoutQWA.btnUnAttempted.requestFocus()
//            }, 10)
//        }
//    }
//
//    override fun getSelectedQuestion(qListJson: String) {
//        try {
//            Log.i("selected Position", qListJson)
//            var json = JSONObject(qListJson)
//            var pos = json.getInt("pos")
//            var qData = json.getJSONObject("data")
//            moveToTestQWA(Gson().fromJson(qData.toString(), Question::class.java), pos)
//        } catch (exception: JSONException) {
//        }
//    }
//
//    override fun setQuestion(singQuestionJson: String) {
//    }
//
//    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
//        if (keyCode == KeyEvent.KEYCODE_DPAD_UP
//            && viewFocusList[viewFocusCounter].isFocused
//        )
//            return true
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            if (isJarEnabled) {
//                binding.layoutQWA.layoutJar.rvJar.visibility = View.VISIBLE
//                binding.layoutQWA.layoutAQ.qaMain.visibility = View.GONE
//                isJarEnabled = false
//                restoreJarFocus()
//                this.jarType = -1
//            } else {
//                finish()
//            }
//        }
//        if (viewFocusCounter == viewFocusList.size - 1) {
//            return false
//        }
//        return true
//    }
//
//    object RowAnimDelayValues {
//        const val ANIM_DELAY = 1000L
//    }
//
//    override fun onBackPressed() {
//        if (isJarEnabled) {
//            binding.layoutQWA.layoutJar.rvJar.visibility = View.VISIBLE
//            binding.layoutQWA.layoutAQ.qaMain.visibility = View.GONE
//            isJarEnabled = false
//        } else {
//            super.onBackPressed()
//        }
//    }
//
//    private fun restoreJarFocus() {
//        binding.layoutQWA.layoutJar.rvJar.requestFocus(jarType)
//    }
//
//    private fun setWebP(id: Int) {
//        val controller = Fresco.newDraweeControllerBuilder()
//        controller.autoPlayAnimations = true
//        val uri: Uri = Uri.Builder()
//            .scheme(UriUtil.LOCAL_RESOURCE_SCHEME) // "res"
//            .path(java.lang.String.valueOf(id))
//            .build()
//        controller.setUri(uri)
//        controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
//            override fun onFinalImageSet(
//                id: String?,
//                imageInfo: ImageInfo?,
//                animatable: Animatable?
//            ) {
//                val anim = animatable as AnimatedDrawable2
//                anim.setAnimationListener(object : AnimationListener {
//                    override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
//                    override fun onAnimationStart(drawable: AnimatedDrawable2?) {
////                        hideImageView()
//                    }
//
//                    override fun onAnimationFrame(
//                        drawable: AnimatedDrawable2?,
//                        frameNumber: Int
//                    ) {
////                        if (frameNumber == 0)
////                            tag = AppConstants.ANIMATE
//
//                    }
//
//                    override fun onAnimationStop(drawable: AnimatedDrawable2?) {
////                        hideVideoView()
//                    }
//
//                    override fun onAnimationReset(drawable: AnimatedDrawable2?) {}
//
//                })
//            }
//        }
//        binding.layoutAchieve.ivParticles.controller = controller.build()
//    }
//    /*private fun getAchieveForFeedback(testCode: String) {
//        testViewModel.getAchieveData(testCode, object : BaseViewModel.APICallBacks<TestAchieveRes> {
//            override fun onSuccess(model: TestAchieveRes?) {
//                if (model != null) {
//
//                }
//            }
//
//            override fun onFailed(code: Int, error: String, msg: String) {
//                makeLogI("$code - $error - $msg")
//            }
//        })
//    }*/
}
