package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ItemSubDiagnosticFeedbackBinding
import com.embibe.embibetvapp.model.achieve.achieveFeedback.Activity
import com.embibe.embibetvapp.model.achieve.achieveFeedback.Data
import com.embibe.embibetvapp.model.achieve.achieveFeedback.PracticeData
import com.embibe.embibetvapp.model.achieve.achieveFeedback.Step
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.activity.PracticeActivity
import com.embibe.embibetvapp.ui.activity.VideoPlayerActivity
import com.embibe.embibetvapp.utils.BlurTransformation
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.VideoUtils
import com.embibe.embibetvapp.utils.data.DataManager
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject

class DiagnosticTestFeedbackSubAdapter :
    RecyclerView.Adapter<DiagnosticTestFeedbackSubAdapter.TestsViewHolder>() {

    var list = ArrayList<Activity>()
    var onItemClick: ((Activity) -> Unit)? = null
    var selectedPosition = -1
    var parentPos = 0
    var parentItem: Step? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestsViewHolder {
        val binding: ItemSubDiagnosticFeedbackBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_sub_diagnostic_feedback,
            parent,
            false
        )
        return TestsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (list.size > 0) list.size else 0
    }

    override fun onBindViewHolder(holder: TestsViewHolder, position: Int) {
        holder.bind(list[position])

        when (list[position].status) {
            AppConstants.DIAGNOSTIC_TEST_STATUS_OPEN -> {
                updateUIForOpenTopic(holder.binding, list[position], holder.itemView)
            }
            AppConstants.DIAGNOSTIC_TEST_STATUS_LOCKED -> {
                updateUIForLockedTopic(holder.binding, list[position])
            }
        }
    }

    private fun updateUIForOpenTopic(
        binding: ItemSubDiagnosticFeedbackBinding,
        activity: Activity,
        itemView: View
    ) {
        setBgTitleDescription(binding, activity, itemView)
        hideOverlay(binding)
        updateResumeProgress(binding, activity)
    }

    private fun updateUIForLockedTopic(
        binding: ItemSubDiagnosticFeedbackBinding,
        activity: Activity
    ) {
        showOverlay(binding)
        hideOtherViews(binding)
    }

    private fun setBgTitleDescription(
        binding: ItemSubDiagnosticFeedbackBinding,
        activity: Activity,
        itemView: View
    ) {
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        Glide.with(itemView.context).setDefaultRequestOptions(requestOptions)
            .load(getDrawableByTitle(activity.title!!))
            .transition(DrawableTransitionOptions.withCrossFade())
            .transform(RoundedCorners(20))
            .into(binding.ivBg)

        binding.title.text = activity.title

        if (activity.progress != 100) {
            if (!activity.duration.isNullOrEmpty()) {
                binding.textTime.text = activity.duration + " hours"
            }
        } else {
            if (!activity.duration.isNullOrEmpty()) {
                binding.textTime.text = activity.duration + " hours | Completed"
            }
        }

        binding.btnLearn.text = getBtnTextByType(activity.type!!)
    }

    private fun getDrawableByTitle(title: String): Int {
        return when {
            title.contains("Improve your Behaviour") -> R.drawable.ic_improve_your_behaviour
            title.contains("Improve your Knowledge") -> R.drawable.ic_improve_your_knowledge
            else -> 0
        }
    }

    private fun hideOverlay(binding: ItemSubDiagnosticFeedbackBinding) {
        binding.lockedOverlay.visibility = View.GONE
        binding.ivLock.visibility = View.GONE
    }

    private fun updateResumeProgress(
        binding: ItemSubDiagnosticFeedbackBinding,
        activity: Activity
    ) {
        if (activity.progress!! > 0) {
            if (activity.progress == 100) {
                binding.seekBar.visibility = View.GONE
                binding.btnLearn.visibility = View.GONE
            } else {
                binding.seekBar.visibility = View.VISIBLE
                binding.seekBar.progress = activity.progress
            }
        } else {
            binding.seekBar.visibility = View.GONE
        }
    }

    private fun showOverlay(binding: ItemSubDiagnosticFeedbackBinding) {
        binding.lockedOverlay.visibility = View.VISIBLE
        binding.ivLock.visibility = View.VISIBLE

        Glide.with(App.context)
            .load(R.drawable.strong_chapters_not_attempted)
            .transform(BlurTransformation(App.context))
            .transform(RoundedCorners(16))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(binding.lockedOverlay)
    }

    private fun hideOtherViews(binding: ItemSubDiagnosticFeedbackBinding) {
        binding.title.visibility = View.INVISIBLE
        binding.textTime.visibility = View.INVISIBLE
        binding.seekBar.visibility = View.GONE
        binding.btnLearn.visibility = View.GONE
    }

    fun setData(parentItem: Step, list: ArrayList<Activity>, parentPos: Int) {
        this.parentItem = parentItem
        this.list = list
        this.parentPos = parentPos
        notifyDataSetChanged()
    }

    inner class TestsViewHolder(var binding: ItemSubDiagnosticFeedbackBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Activity) {
            if (adapterPosition == list.size - 1) {
                binding.verticalViewBottom.visibility = View.INVISIBLE
            } else {
                binding.verticalViewBottom.visibility = View.VISIBLE
            }

            setFocusListeners(binding, itemView)
        }

        init {
            binding.subImgLayout.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {

                            val activity = list[adapterPosition]
                            DataManager.instance.setFeedbackStep(parentItem!!)
                            DataManager.instance.setSteposPos(parentPos)
                            DataManager.instance.setActivity(activity)
                            DataManager.instance.findActivityPosition(activity)
                            var data = DataManager.instance.getNextFeedbackData()
                            if (data != null)
                                moveToNext(binding.subImgLayout.context, data)
                            when (list[adapterPosition].type) {

                                //binding.subImgLayout.context
                                AppConstants.DIAGNOSTIC_TEST_ACTIVITY_STATUS_COACH -> {

                                }
                                AppConstants.DIAGNOSTIC_TEST_ACTIVITY_STATUS_LEARN -> {

                                }
                            }
                        }
                    }
                }

                false
            }
        }
    }

    private fun moveToNext(context: Context?, data: Data?) {
        if (data!!.type == "practice" || data!!.type == "question")
            callPracticeActivity(context, data.practiceMeta!!)
        else
            callVideoActivity(context, data.contentMeta!!)
    }


    fun callVideoActivity(context: Context?, content: Content) {
        startVideoActivity(context, content)
    }

    private fun startVideoActivity(context: Context?, content: Content) {
        doAsync {
            val url = content.url
            val type = Utils.getVideoTypeUsingUrl(content.url)
            uiThread {
                callVideo(
                    context,
                    url,
                    type.toLowerCase(),
                    content
                )
            }
        }
    }


    private fun callVideo(
        context: Context?,
        url: String,
        source: String,
        content: Content
    ) {
        if (source == AppConstants.PLAYER_TYPE_YOUTUBE) {

        } else {
            val callback = object : BaseViewModel.APICallBackVolley {
                override fun <T> onSuccessCallBack(response: T) {
                    response as JSONObject
                    val playableUrl = Utils.getVimeoHD((response["files"] as JSONArray))
                    startVideo(context, playableUrl, content)
                }

                override fun onErrorCallBack(e: VolleyError) {
                    Log.e("Error"," ${e.localizedMessage}")
                }
            }
            val videoId = VideoUtils.getVimeoVideoId(url).toString()
            Utils.getVideoURL(context!!, videoId, content.owner_info.key, callback)
        }

    }


    fun startVideo(
        context: Context?,
        url: String, content: Content
    ) {
        Log.i("content", Gson().toJson(content))
        val intent = Intent(context, VideoPlayerActivity::class.java)
        intent.putExtra(AppConstants.VIDEO_TITLE, content.title)
        intent.putExtra(AppConstants.IS_ACHIEVE, true)
        val source = Utils.getVideoTypeUsingUrl(content.url)
        intent.putExtra(AppConstants.VIDEO_ID, content.id)
        intent.putExtra(AppConstants.CONTENT_TYPE, content.type)
        intent.putExtra(AppConstants.VIDEO_CONCEPT_ID, content.learning_map.conceptId)
        intent.putExtra(AppConstants.VIDEO_URL, url)
        intent.putExtra(AppConstants.VIDEO_DESCRIPTION, content.description)
        intent.putExtra(AppConstants.VIDEO_SUBJECT, content.subject)
        intent.putExtra(AppConstants.VIDEO_CHAPTER, content.learning_map.chapter)
        intent.putExtra(AppConstants.VIDEO_AUTHORS, content.authors)
        intent.putExtra(AppConstants.SOURCE_REFERENCE, AppConstants.PRACTICE)
        intent.putExtra(AppConstants.VIDEO_SOURCE, source)
        intent.putExtra(AppConstants.TOPIC_LEARN_PATH, content.learning_map.topicLearnPathName)
        intent.putExtra(AppConstants.VIDEO_TOTAL_DURATION, 216.toString())
        intent.putExtra(AppConstants.FORMAT_ID, content.learning_map.format_id)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, content.learnpath_name)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, content.learnpath_format_name)
        context?.startActivity(intent)
    }

    private fun callPracticeActivity(context: Context?, data: PracticeData) {
        val intent = Intent(context, PracticeActivity::class.java)
        intent.putExtra(AppConstants.TITLE, data.title)
        intent.putExtra(AppConstants.PRACTICE_MODE, AppConstants.PracticeModes.PAJPractice)
        intent.putExtra(AppConstants.PAJ_ID, data.paj_id)
        intent.putExtra(AppConstants.PAJ_STEP_INDEX, data.paj_step_index)
        intent.putExtra(AppConstants.PAJ_STEP_PATH, data.paj_step_path)
        intent.putExtra(AppConstants.PAJ_STEP_PATH_INDEX, data.paj_step_path_index)
        intent.putExtra(AppConstants.CALL_ID, data.call_id)
        intent.putExtra(AppConstants.PROGRESS_PERCENT, data.progress_percent)
        intent.putExtra(AppConstants.QUESTION_CODE, data.questionCode)
        context?.startActivity(intent)
    }

    private fun getBtnTextByType(type: String): String {
        return when (type) {
            AppConstants.DIAGNOSTIC_TEST_ACTIVITY_STATUS_COACH -> "Coach me"
            AppConstants.DIAGNOSTIC_TEST_ACTIVITY_STATUS_LEARN -> "Learn"
            else -> ""
        }
    }

    private fun setFocusListeners(
        binding: ItemSubDiagnosticFeedbackBinding,
        itemView: View
    ) {
        binding.subImgLayout.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.btnLearn.background =
                    itemView.context.getDrawable(R.drawable.bg_card_selected_yellow)
                binding.btnLearn.setTextColor(itemView.resources.getColor(R.color.black))
            } else {
                binding.btnLearn.background =
                    itemView.context.getDrawable(R.drawable.bg_card_unselected_grey)
                binding.btnLearn.setTextColor(itemView.resources.getColor(R.color.white))
            }
        }
    }

    private fun startActivityByType(content: Content) {

        when ((content.type).toLowerCase()) {
//            AppConstants.TYPE_VIDEO, AppConstants.TYPE_COOBO -> {
//                startDetailActivity(content)
//            }
//            AppConstants.TYPE_BOOK -> {
//                startBookDetailActivity(content)
//            }
//            AppConstants.TYPE_PRACTICE, AppConstants.TYPE_CHAPTER -> {
//                startPracticeDetailActivity(content)
//            }
//            AppConstants.NAV_TYPE_SYLLABUS -> {
//                startLearnSummaryActivity(content)
//            }
//            AppConstants.TEST -> {
//                startTestScreen(content)
//                // startTopicSummaryActivity(content)
//            }
        }
    }

}