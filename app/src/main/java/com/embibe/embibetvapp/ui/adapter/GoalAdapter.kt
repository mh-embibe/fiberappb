package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemGoalsBinding
import com.embibe.embibetvapp.model.adduser.Goal
import com.embibe.embibetvapp.ui.activity.AddGoalsExamsActivity

class GoalAdapter(var context: Context) :
    RecyclerView.Adapter<GoalAdapter.GoalViewHolder>() {

    private var list: List<Goal> = arrayListOf()
    var onItemClick: ((Goal) -> Unit)? = null
    var onFocusChange: ((Goal, Boolean) -> Unit)? = null
    var selectedItem = -1
    private var focusedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GoalViewHolder {
        val binding: ItemGoalsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_goals, parent, false
        )
        return GoalViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: GoalViewHolder, position: Int) {

        holder.bind(list[position])

        if (selectedItem == position) {
            holder.binding.textGoalExam.background = context.getDrawable(R.drawable.goal_selected)
            holder.binding.textGoalExam.setBackgroundResource(R.drawable.bg_goals_slected_selector)
            holder.binding.textGoalExam.setTextColor(context.getColor(R.color.email_font_color))
        }

    }

    fun setData(data: List<Goal>) {
        list = data
        setDefaultFocusToSupportedGoal(list)
        notifyDataSetChanged()
    }

    private fun setDefaultFocusToSupportedGoal(primaryGoals: List<Goal>) {
        for (index in primaryGoals.indices) {
            if (primaryGoals[index].default) {
                setDefaultSelectedItem(index)
                AddGoalsExamsActivity.primaryGoalCode = primaryGoals[index].code
            }
        }
    }

    fun setDefaultSelectedItem(position: Int) {
        selectedItem = position
        notifyDataSetChanged()
    }

    fun setDefaultFocusedPosition(position: Int) {
        focusedPosition = position
        notifyDataSetChanged()
    }

    inner class GoalViewHolder(var binding: ItemGoalsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Goal) {
            binding.goal = item
            binding.executePendingBindings()

            binding.textGoalExam.background = context.getDrawable(R.drawable.bg_button_goals)
            binding.textGoalExam.setTextColor(context.getColor(R.color.white))

            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
                if (item.supported) {
                    onItemClick?.invoke(list[adapterPosition])

                    if (selectedItem != adapterPosition) {
                        val previousItem = selectedItem
                        selectedItem = adapterPosition
                        notifyItemChanged(previousItem)
                    } else
                        selectedItem = -1
                    notifyItemChanged(adapterPosition)
                }

            }

            itemView.setOnFocusChangeListener { v, hasFocus ->
                onFocusChange?.invoke(list[adapterPosition], hasFocus)
            }
        }

    }
}
