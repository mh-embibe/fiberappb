package com.embibe.embibetvapp.ui.fragment.learn


import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentBannerImgLearnFlipperBinding
import com.embibe.embibetvapp.model.BannerData
import com.embibe.embibetvapp.ui.fragment.bannervideo.BannerVideoPlayer
import com.embibe.embibetvapp.ui.interfaces.BannerImageDataToHostListener
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.VideoUtils
import com.embibe.embibetvapp.utils.data.DataManager
import com.google.android.exoplayer2.Player
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import kotlin.properties.Delegates

class LearnBannerImgFlipper : BaseAppFragment(), BannerVideoPlayer.OnPlayerStateChangeListener {
    val TAG = LearnBannerImgFlipper::class.java.simpleName
    var images = ArrayList<String>()
    var imageViewList = ArrayList<ImageView>()
    lateinit var binding: FragmentBannerImgLearnFlipperBinding
    private lateinit var bannerImageDataToHostListener: BannerImageDataToHostListener
    private lateinit var homeViewModel: HomeViewModel
    private var bannerDataSize by Delegates.notNull<Int>()
    val bannerVideoPlayer = BannerVideoPlayer()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_banner_img_learn_flipper, container, false
        )
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        //showProgress()
        doAsync {
            val learnBannerData = if (DataManager.instance.getHeroBannerData().data.isNotEmpty()) {
                DataManager.instance.getHeroBannerData().data
            } else {
                homeViewModel.fetchBannerSectionByPageName(AppConstants.LEARN) ?: arrayListOf()
            }
            uiThread {
                bannerDataSize = learnBannerData.size
                initViewFlipper(learnBannerData as ArrayList<BannerData>)
            }
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bannerVideoPlayer.setPlayerStateChangeListener(this)
        childFragmentManager.beginTransaction().apply {
            replace(R.id.ivVideo, bannerVideoPlayer)
            commit()
        }
    }

    private fun initViewFlipper(data: ArrayList<BannerData>) {

        for (i in 0 until 1) {
            context?.let {
                val imageView = ImageView(it)
                imageView.apply {
                    scaleType = ImageView.ScaleType.CENTER_CROP
                }
                imageViewList.add(imageView)
            }
            try {
                addImageView(data[i].imgUrl, imageViewList[0])
                CoroutineScope(Dispatchers.Main).launch {
                    delay(2000L)
                    try {
                        // temporary video
                        val videoId = VideoUtils.getVimeoVideoId(data[i].teaser_url).toString()
                        Utils.getVideoURL(
                            requireContext(),
                            videoId,
                            data[i].owner_info.key,
                            object :
                                BaseViewModel.APICallBackVolley {
                                override fun <T> onSuccessCallBack(it: T) {
                                    val response = it as JSONObject
                                    try {
                                        val playableUrl =
                                            Utils.getVimeoHD((response["files"] as JSONArray))
                                        Log.d("Success", "$it")
                                        showVideoView(playableUrl)
                                    } catch (e: Exception) {

                                    }

                                }

                                override fun onErrorCallBack(error: VolleyError) {
                                    error.printStackTrace()
                                }
                            })
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                bannerImageDataToHostListener.setBannerImageData(
                    data[0],
                    0 + 1
                )
            } catch (e: Exception) {
            }
        }
    }

    private fun addImageView(imgUrl: String, imageView: ImageView) {
        val uri = Uri.parse(imgUrl)
        setImage(binding.vfBanner, uri)
    }

    private fun setImage(imageView: ImageView, uri: Uri) {
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        context?.let {
            Glide.with(it).setDefaultRequestOptions(requestOptions).load(uri)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView)
        }
    }

    fun bannerImageDataListener(callback: BannerImageDataToHostListener) {
        this.bannerImageDataToHostListener = callback
    }

    object FlipperPanel {
        const val FLIP_INTERVAL = 10000
    }

    override fun onResume() {
        super.onResume()
        doAsync {
            val learnBannerData = if (DataManager.instance.getHeroBannerData().data.isNotEmpty()) {
                DataManager.instance.getHeroBannerData().data
            } else {
                homeViewModel.fetchBannerSectionByPageName(AppConstants.LEARN) ?: arrayListOf()
            }
            uiThread {
                initViewFlipper(learnBannerData as ArrayList<BannerData>)
            }
        }
    }

    fun showVideoView(previewURl: String) {
        hideImageView()
        bannerVideoPlayer.setUrl(previewURl)
    }

    private fun hideImageView() {
        binding.ivVideo.visibility = View.VISIBLE
        binding.cardView.visibility = View.VISIBLE
        binding.vfBanner.visibility = View.INVISIBLE
    }

    fun hideVideoView() {
        binding.ivVideo.visibility = View.INVISIBLE
        binding.cardView.visibility = View.INVISIBLE
        binding.vfBanner.visibility = View.VISIBLE
    }

    override fun onPlayerStateChange(state: Int) {
        when (state) {
            Player.STATE_ENDED -> {
                hideVideoView()
            }
        }
    }
}

