package com.embibe.embibetvapp.ui.fragment.editUser

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentSecondaryGoalsBinding
import com.embibe.embibetvapp.model.UpdateProfileRequest
import com.embibe.embibetvapp.model.updateProfile.Errors
import com.embibe.embibetvapp.model.updateProfile.UpdateProfileRes
import com.embibe.embibetvapp.ui.activity.AddGoalsExamsActivity
import com.embibe.embibetvapp.ui.adapter.SecondaryGoalAdapter
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent


class SecondaryGoalsFragment : BaseAppFragment() {

    private lateinit var binding: FragmentSecondaryGoalsBinding
    private val fragment = SecondaryGoalsExamFragment()
    private lateinit var signInViewModel: SignInViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_secondary_goals, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        AddGoalsExamsActivity.secondaryGoalCode = ""

        setGoalRecyclerView(view)
        setDoneClickListener()
        setBackClickListener()
        setSkipClickListener()
    }

    private fun setGoalRecyclerView(view: View) {

        val goalAdapter = SecondaryGoalAdapter(view.context)
        makeGridCenter()

        val goalsList =
            DataManager.instance.getPrimaryGoalsExcept(AddGoalsExamsActivity.primaryGoalCode)

        goalAdapter.setData(goalsList)
        binding.rvSecondaryGoal.adapter = goalAdapter

        goalAdapter.onItemClick = { goal ->
            AddGoalsExamsActivity.secondaryGoal = goal.name //"NTSE" // change with goal
            AddGoalsExamsActivity.secondaryGoalCode = goal.code

        }

        /*goalAdapter.onFocusChange = { goal ->
            if (!goal.supported) {
                binding.textGoalNotSupported.visibility = View.VISIBLE
                binding.textGoalNotSupported.text =
                    "${resources.getString(R.string.text_goal_not_supported)} ${goal.name}"
            } else {
            }
        }*/

        binding.rvSecondaryGoal.requestFocus()
    }

    fun makeGridCenter() {
        val layoutManager = FlexboxLayoutManager(context)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.CENTER
        layoutManager.alignItems = AlignItems.CENTER
        binding.rvSecondaryGoal.layoutManager = layoutManager

    }


    private fun setDoneClickListener() {
        binding.btnContinue.setOnClickListener {
            SegmentUtils.trackProfileSecondaryGoalContinueClick(AddGoalsExamsActivity.secondaryGoal)

            if (isValidGoal()) {

                (activity as AddGoalsExamsActivity).setSecondaryGoalExam(
                    fragment, "secondary_goals_exam_fragment"
                )
            } else {
                showToast(resources.getString(R.string.please_select_goal))
            }

        }
        binding.btnContinue.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    SegmentUtils.trackProfileSecondaryGoalContinueFocus(AddGoalsExamsActivity.secondaryGoal)
                }
            }
        }
    }

    private fun setBackClickListener() {
        binding.btnBack.setOnClickListener {
            SegmentUtils.trackProfileSecondaryGoalBackClick(AddGoalsExamsActivity.secondaryGoal)
            requireActivity().onBackPressed()
        }
        binding.btnBack.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    SegmentUtils.trackProfileSecondaryGoalBackFocus(AddGoalsExamsActivity.secondaryGoal)
                }
            }
        }
    }

    private fun setSkipClickListener() {
        binding.btnSkip.setOnClickListener {
            // update data to api
            SegmentUtils.trackProfileSecondaryGoalSkipClick(AddGoalsExamsActivity.secondaryGoal)
            (activity as AddGoalsExamsActivity).finish()
        }
        binding.btnSkip.setOnFocusChangeListener { v, hasFocus ->
            when (hasFocus) {
                true -> {
                    SegmentUtils.trackProfileSecondaryGoalSkipFocus(AddGoalsExamsActivity.secondaryGoal)

                }
            }
        }
    }

    private fun isValidGoal(): Boolean {
        return AddGoalsExamsActivity.secondaryGoalCode.isNotEmpty()
    }

    fun updateGoalExams() {
        val model = UpdateProfileRequest()

        model.primaryGoal = AddGoalsExamsActivity.primaryGoalCode
        model.primaryExamCode = AddGoalsExamsActivity.primaryGoalExamCode
        /* model.secondaryGoal = AddGoalsExamsActivity.secondaryGoalCode
         model.secondaryExamsPreparingFor = AddGoalsExamsActivity.secondaryGoalExams.joinToString()*/
        updateProfile(model)
    }

    fun updateProfile(model: UpdateProfileRequest) {
        showProgress()
        signInViewModel.updateProfile(model, object : BaseViewModel.APICallBacks<UpdateProfileRes> {
            override fun onSuccess(model: UpdateProfileRes?) {
                hideProgress()
                if (model != null) {
                    if (model.success) {
                        profileUpdated(model)
                    } else {
                        failedToUpdateProfile(model.errors)
                    }
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(activity, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            updateProfile(model)
                        }

                        override fun onDismiss() {

                        }
                    })

                } else {
                    Utils.showToast(activity!!, error)
                }

            }
        })
    }

    private fun profileUpdated(model: UpdateProfileRes) {
        AppConstants.ISPROFILEUPDATED = true
        (activity as AddGoalsExamsActivity).finish()
    }

    private fun failedToUpdateProfile(errors: Errors?) {
        if (errors != null) {
            val email = errors.email
            val profile = errors.profile
            val mobile = errors.mobile

            if (profile.equals("Nothing to change")) {
                (activity as AddGoalsExamsActivity).finish()
            } else if (mobile?.contains("has already been taken")!!) {
                showToast(getString(R.string.edit_profile_mobile_exist))
                (activity as AddGoalsExamsActivity).finish()
            }
        }
    }
}