package com.embibe.embibetvapp.ui.activity

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.text.SpannableString
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.View.LAYER_TYPE_SOFTWARE
import android.view.ViewTreeObserver
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.base.TestFeedbackBaseFragmentActivity
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.ALL_ATTEMPTS
import com.embibe.embibetvapp.constant.AppConstants.ALL_SUBJECTS
import com.embibe.embibetvapp.constant.AppConstants.QWA_ATTEMPT_TYPE.*
import com.embibe.embibetvapp.databinding.ActivityTestFeedbackBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.achieve.achieveFeedback.AchieveFeedbackRes
import com.embibe.embibetvapp.model.achieve.achieveFeedback.DiagnosticTest
import com.embibe.embibetvapp.model.achieve.achieveFeedback.Step
import com.embibe.embibetvapp.model.test.*
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.adapter.*
import com.embibe.embibetvapp.ui.interfaces.CommonAndroidAPI
import com.embibe.embibetvapp.ui.viewmodel.AchieveViewModel
import com.embibe.embibetvapp.ui.viewmodel.TestViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.fromJson
import com.embibe.embibetvapp.utils.data.DataManager
import com.facebook.common.util.UriUtil
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.gson.Gson
import kotlinx.android.synthetic.main.layout_correctly_answered_info.view.*
import kotlinx.android.synthetic.main.layout_grade_info.view.*
import kotlinx.android.synthetic.main.layout_start_test.view.*
import kotlinx.android.synthetic.main.qwa_test_component.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

class TestFeedbackActivity : TestFeedbackBaseFragmentActivity(), TestJarAdapter.Listener,
    CommonAndroidAPI.AndroidAPIOnCallListener {

    private lateinit var binding: ActivityTestFeedbackBinding
    private lateinit var testViewModel: TestViewModel
    private lateinit var coroutineScope: CoroutineScope

    private lateinit var barAdapter: TestBarAdapter
    private lateinit var topSkillAdapter: TopSkillAdapter
    private lateinit var menuAdapter: TestFeedbackMenuAdapter
    private lateinit var diagnosticTestFeedbackAdapter: DiagnosticTestFeedbackAdapter
    private lateinit var jarAdapter: TestJarAdapter

    private var testQuestionSummaryRes: TestQuestionSummaryRes? = null
    private var testAttemptsRes: TestAttemptsRes? = null
    private var testSkillsRes: TestSkillsRes? = null
    private var testQuestionRes: TestQuestionResponse? = null
    private var testAchieveRes: TestAchieveRes? = null
    private var testPAJData: AchieveFeedbackRes? = null

    private var mySectionSummaryList = ArrayList<SectionSummary>()
    private var listTopSkill = ArrayList<SkillSet>()

    private var correctlyAnsweredMotionLayout: MotionLayout? = null
    private var isFirstRowExpanded: Boolean = false
    private var layoutAQVisibility: Boolean = false
    private var isJarEnabled: Boolean = false
    private var jarType: Int = -1

    private var rowsHM = TreeMap<Int, View>()
    private var viewFocusList = java.util.ArrayList<View>()
    private var viewFocusCounter = 0
    private var lastKeyPress = 0L
    private var progressLayoutWidth: Int = 0
    private var height: Int = 0

    private var currentQWASubject = ""
    private var currentAttemptType = ""

    var testName: String = ""
    var testCode: String = ""
    val TAG = this.javaClass.name
    lateinit var content: Content
    var isDiagnostic: Boolean = false
    private lateinit var achieveViewModel: AchieveViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SegmentUtils.trackTestFeedbackScreenLoadStart()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_test_feedback)
        testViewModel = ViewModelProviders.of(this).get(TestViewModel::class.java)
        achieveViewModel = ViewModelProviders.of(this).get(AchieveViewModel::class.java)
        coroutineScope = CoroutineScope(Dispatchers.Main)
//        correctlyAnsweredMotionLayout = binding.layoutChart.layoutCorrectlyAnswered.constraintLayout
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)

        addViewsToHashMap()
        addFocusViewsToList()
        viewFocusList[viewFocusCounter].requestFocus()

        getAllData()
        setBackgrounds(requestOptions)

        setFirstRowListeners()
        setClickToWatchListener()
        setQWAKeyListener()

        setTitle()
        updateUIForChart()
        setChart()
        setAndAnimateProgress()
        setTopSkillRecycler()

        setDropDownQWA()
        updateUIForWebview()
        setQWAButtonListener()
        setQWAFocusListener()
        setJarRecycler()

        updateUIForAchieve()
    }

    override fun onResume() {
        super.onResume()
        SegmentUtils.trackTestFeedbackScreenLoadEnd()

        if (UserData.getAchieveOfCurrentUser()) {
            if (DataManager.instance.getTestPAJ() != null) {
                testPAJData = Gson().fromJson(
                    DataManager.instance.getTestPAJ(),
                    AchieveFeedbackRes::class.java
                )
                if (testPAJData != null) {
                    /*load getPAJ from api*/
                    getPAJ(testPAJData?.bundleCode!!, testPAJData?.pajId!!)
                }
            }
        }
    }

    /*load getPAJ from api*/
    fun getPAJ(bundleId: String, id: String) {
        achieveViewModel.getAchieveFeedbackRes(bundleId, id, object :
            BaseViewModel.APICallBacks<AchieveFeedbackRes> {

            override fun onSuccess(model: AchieveFeedbackRes?) {
                if (model != null) {
                    DataManager.instance.setTestPAJ(Gson().toJson(model))
                    testPAJData = model
                    updateUIForAchieve()
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {

            }
        })
    }


    private fun addViewsToHashMap() {
        rowsHM[binding.layoutTitleCharts.constraintTitleCharts.id] =
            binding.layoutTitleCharts.constraintTitleCharts
        rowsHM[binding.layoutClickToWatchTopSkill.clClickTopWatchAndTopSkill.id] =
            binding.layoutClickToWatchTopSkill.clClickTopWatchAndTopSkill
        rowsHM[binding.clNestedQwaAndAchieve.id] = binding.clNestedQwaAndAchieve
    }

    private fun addFocusViewsToList() {
        viewFocusList.add(binding.layoutTitleCharts.layoutChart.constraintCharts)
        viewFocusList.add(binding.layoutClickToWatchTopSkill.clClickTopWatchAndTopSkill)
        viewFocusList.add(binding.layoutQWA.tvDropDownSubject)
    }

    private fun getAllData() {

        with(DataManager.instance) {
            testAttemptsRes = Gson().fromJson(getTestAttempts(), TestAttemptsRes::class.java)
            testSkillsRes = Gson().fromJson(getTestSkills(), TestSkillsRes::class.java)
            testQuestionRes = Gson().fromJson(getTestQuestion(), TestQuestionResponse::class.java)
            testAchieveRes = Gson().fromJson(getTestAchieve(), TestAchieveRes::class.java)
            testQuestionSummaryRes =
                Gson().fromJson(getTestQuestionSummary(), TestQuestionSummaryRes::class.java)
            if (getTestPAJ() != null) {
                testPAJData = Gson().fromJson(getTestPAJ(), AchieveFeedbackRes::class.java)
                if (testPAJData != null)
                    DataManager.instance.setPAJ_Id(testPAJData!!.pajId!!)
            }
        }

        parseAttemptInToQuestion()

        if (intent.hasExtra("testName"))
            testName = intent.getStringExtra("testName")!!
        if (intent.hasExtra("testCode"))
            testCode = intent.getStringExtra("testCode")!!
        if (intent.hasExtra(AppConstants.CONTENT)) {
            content = getContent(intent)
        }
        if (intent.hasExtra(AppConstants.IS_DIAGNOSTIC)) {
            isDiagnostic = intent.getBooleanExtra(AppConstants.IS_DIAGNOSTIC, false)
        }
        readFromLocal()
    }

    private fun parseAttemptInToQuestion() {
        if (testQuestionRes != null) {
            for (key in testQuestionRes!!.paper.sections.keys) {
                val section = testQuestionRes!!.paper.sections[key]

                for (question in section!!.questions) {
                    val attempt = getAttemptByCode(question.code)
                    val summary = getSummaryByCode(question.code)
                    if (attempt != null) {
                        question.answerSelectedOption = attempt.answerSelectedOption
                        question.attemptTypeBadge = attempt.attemptTypeBadge
                        question.tags = attempt.tags!!
                        question.explanation = attempt.explanation
                        question.correctAnswerCsv = attempt.correctAnswerCsv
                        question.marks_obtained = attempt.marks!!
                        question.first_looked_at = attempt.tFirstLook!!
                        question.attempted_at = attempt.tFirstSave!!
                        question.test_started_at = testAttemptsRes!!.startedAtInMilliSec!!
                        if (summary != null) {
                            question.practiced_before = summary.practiced_before
                            question.practice_badge = summary.practice_badge
                            question.primary_concept = summary.primary_concept
                            question.secondary_concept = summary.secondary_concept
                        }
                    }
                }
            }
        }
    }

    private fun readFromLocal() {
        try {
            for (i in testAttemptsRes!!.scoreListInfo!!.sectionSummaryList!!.indices) {
                mySectionSummaryList.add(testAttemptsRes!!.scoreListInfo!!.sectionSummaryList!![i])
            }
            for (i in testSkillsRes!!.skillset!!.indices) {
                listTopSkill.add(testSkillsRes!!.skillset?.get(i)!!)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setBackgrounds(requestOptions: RequestOptions) {

        Glide.with(this).setDefaultRequestOptions(requestOptions)
            .load(R.drawable.bg_first_row_feedback)
            .transition(DrawableTransitionOptions.withCrossFade())
            .transform(RoundedCorners(20))
            .into(binding.layoutTitleCharts.layoutChart.ivBg)

        Glide.with(this).setDefaultRequestOptions(requestOptions)
            .load(R.drawable.video_banner)
            .transition(DrawableTransitionOptions.withCrossFade())
            .transform(RoundedCorners(20))
            .into(binding.layoutClickToWatchTopSkill.layoutClickToWatch.ivThumbnail)
    }

    private fun setFirstRowListeners() {
        binding.layoutTitleCharts.layoutChart.constraintCharts.setOnKeyListener { v, keyCode, event ->
            when (event.action) {
                KeyEvent.ACTION_DOWN -> {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            return@setOnKeyListener keyPressLimiter {
                                smoothScrollRowsUp(binding.layoutTitleCharts.constraintTitleCharts.id)
                            }
                        }

                        KeyEvent.KEYCODE_NUMPAD_ENTER, KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                            return@setOnKeyListener keyPressLimiter {
//                                if (!isFirstRowExpanded)
////                                    expandFirstRow()
//                                else
////                                    shrinkFirstRow()
                            }
                        }
                        KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_DPAD_LEFT -> {
                            coroutineScope.launch {
                                delay(1)
                                binding.layoutTitleCharts.layoutChart.constraintCharts.requestFocus()
                            }

                        }
                    }
                }
            }
            false
        }
    }

    private fun setClickToWatchListener() {
        binding.layoutClickToWatchTopSkill.layoutClickToWatch.constraintClickToWatch.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenClickToWatchFocus()
        }

        binding.layoutClickToWatchTopSkill.layoutClickToWatch.constraintClickToWatch.setOnKeyListener { v, keyCode, event ->
            when (event?.action) {
                KeyEvent.ACTION_DOWN -> {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_DOWN -> {
                            return@setOnKeyListener keyPressLimiter {
                                smoothScrollRowsUp(binding.layoutClickToWatchTopSkill.clClickTopWatchAndTopSkill.id)
                            }
                        }
                        KeyEvent.KEYCODE_DPAD_UP -> {
                            return@setOnKeyListener keyPressLimiter {
                                smoothScrollRowsDown(binding.layoutTitleCharts.constraintTitleCharts.id)
                            }
                        }
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                            SegmentUtils.trackTestFeedbackScreenClickToWatchClick()
                        }
                    }
                }
            }
            false
        }
    }

    private fun setQWAKeyListener() {
        binding.layoutQWA.tvDropDownSubject.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenSubjectWiseAnalyseDropdownFocus()
        }

        binding.layoutQWA.tvDropDownSubject.setOnKeyListener { v, keyCode, event ->
            when (event?.action) {
                KeyEvent.ACTION_DOWN -> {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_UP ->
                            return@setOnKeyListener keyPressLimiter {
                                smoothScrollRowsDown(binding.layoutClickToWatchTopSkill.clClickTopWatchAndTopSkill.id)
                            }
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                            SegmentUtils.trackTestFeedbackScreenSubjectWiseAnalyseDropdownClick()
                        }
                    }
                }
            }
            false
        }
    }

    private fun smoothScrollRowsUp(viewTranslatingIndex: Int) {
        viewFocusCounter++
        rowsHM.forEach { (otherViewsIndex, _) ->
            if (otherViewsIndex != viewTranslatingIndex) {
                slideRow(
                    rowsHM[otherViewsIndex]!!,
                    -rowsHM[viewTranslatingIndex]?.translationY!!.toFloat()
                        .minus(rowsHM[viewTranslatingIndex]?.height!!.toFloat()),
                    false,
                    rowsHM[viewTranslatingIndex]?.translationY!!.toFloat()
                )
            } else
                slideRow(
                    rowsHM[viewTranslatingIndex]!!,
                    -rowsHM[viewTranslatingIndex]?.translationY!!.toFloat()
                        .minus(rowsHM[viewTranslatingIndex]?.height!!.toFloat()),
                    true,
                    rowsHM[viewTranslatingIndex]?.translationY!!.toFloat(),
                    1f, 0f
                )
        }
        coroutineScope.launch {
            delay(10)
            if (viewFocusCounter < viewFocusList.size) {
                viewFocusList[viewFocusCounter].requestFocus()
            }
        }
    }

    private fun smoothScrollRowsDown(viewTranslatingIndex: Int) {
        rowsHM.forEach { (otherViewsIndex, _) ->
            if (otherViewsIndex != viewTranslatingIndex) {
                slideRow(
                    rowsHM[otherViewsIndex]!!,
                    -rowsHM[viewTranslatingIndex]?.height!!.toFloat()
                        .plus(rowsHM[viewTranslatingIndex]!!.translationY),
                    false,
                    rowsHM[viewTranslatingIndex]!!.translationY
                )
            } else {
                slideRow(
                    rowsHM[viewTranslatingIndex]!!,
                    -rowsHM[viewTranslatingIndex]?.height!!.toFloat()
                        .plus(rowsHM[viewTranslatingIndex]!!.translationY),
                    true,
                    rowsHM[viewTranslatingIndex]!!.translationY,
                    0f,
                    1f
                )
            }
        }

        coroutineScope.launch {
            delay(10)
            if (viewFocusCounter > 0 && viewFocusCounter < viewFocusList.size) {
                viewFocusList[--viewFocusCounter].requestFocus()
            }
        }
    }

    private fun slideRow(
        view: View,
        viewHeight: Float = 0f,
        canFade: Boolean = false,
        updatedTransYValue: Float = 0f,
        alphaFrom: Float = 0f,
        alphaTo: Float = 1f
    ) {
        val transYObjectAnimator =
            ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, updatedTransYValue, -viewHeight)
        var alphaObjectAnimator: ObjectAnimator? = null
        if (canFade) {
            alphaObjectAnimator =
                ObjectAnimator.ofFloat(view, View.ALPHA, alphaFrom, alphaTo)
        }

        val animatorSet = AnimatorSet()
        if (alphaObjectAnimator != null)
            animatorSet.playTogether(transYObjectAnimator, alphaObjectAnimator)
        else
            animatorSet.playTogether(transYObjectAnimator)

        animatorSet.duration = RowAnimDelayValues.ANIM_DELAY
        animatorSet.start()
    }

    private fun keyPressLimiter(threshHold: Long = 1500L, action: () -> Unit): Boolean {
        if (SystemClock.elapsedRealtime() - lastKeyPress < threshHold) {
            return true
        } else {
            action.invoke()
        }
        lastKeyPress = SystemClock.elapsedRealtime()
        return false
    }

    private fun setTitle() {
        binding.layoutTitleCharts.tvTitle.text =
            getString(R.string.marks_and_overall_performance_on_and)
                .replace("*test_name*", testName)
                .replace(
                    "*exam_name*",
                    DataManager.instance.getExamNameByCode(
                        UserData.getGoalCode(), UserData.getExamCode()
                    )
                )
    }

    private fun setChart() {
        barAdapter = TestBarAdapter()
        barAdapter.setItems(mySectionSummaryList)
        binding.layoutTitleCharts.layoutChart.layoutCorrectlyAnswered.rvMarksBar.adapter =
            barAdapter
    }

    private fun updateUIForChart() {
        if (UserData.getAchieveOfCurrentUser()) {
            binding.layoutTitleCharts.layoutChart.layoutGradeInfo.visibility = View.VISIBLE
            binding.layoutTitleCharts.layoutChart.layoutTestScore.constraintScore.visibility =
                View.INVISIBLE
            setDataForGrade()
        } else {
            binding.layoutTitleCharts.layoutChart.layoutTestScore.constraintScore.visibility =
                View.VISIBLE
            binding.layoutTitleCharts.layoutChart.layoutGradeInfo.visibility = View.GONE
            setDataForScore()
        }
    }

    private fun setDataForScore() {

        binding.layoutTitleCharts.layoutChart.layoutCorrectlyAnswered.tv_score.text =
            setCorrectlyAnsweredQuestions()

        binding.layoutTitleCharts.layoutChart.layoutTestScore.tvScoreNominator.text =
            testAttemptsRes?.scoreListInfo!!.myScore.roundToInt().toString()

        binding.layoutTitleCharts.layoutChart.layoutTestScore.tvScoreDenominator.text =
            testAttemptsRes?.scoreListInfo!!.maxScore.roundToInt().toString()

        binding.layoutTitleCharts.layoutChart.layoutTestScore.tvScoreNominator.setTextColor(
            getColor(
                R.color.colorReddishPink
            )
        )
    }

    private fun setDataForGrade() {
        binding.layoutTitleCharts.layoutChart.layoutGradeInfo.tvTest.visibility = View.GONE
        binding.layoutTitleCharts.layoutChart.layoutGradeInfo.valueGrade.text =
            testAttemptsRes?.scoreListInfo?.grade
        binding.layoutTitleCharts.layoutChart.layoutCorrectlyAnswered.tv_score.text =
            setCorrectlyAnsweredQuestions()
    }

    private fun setTopSkillRecycler() {
        topSkillAdapter = TopSkillAdapter()
        topSkillAdapter.setItems(listTopSkill)
        binding.layoutClickToWatchTopSkill.layoutTopSkill.layoutCorrectlyAnswered.rvScoreBar.adapter =
            topSkillAdapter

        setDataForTopSkill()
    }

    private fun setDataForTopSkill() {
        binding.layoutClickToWatchTopSkill.layoutTopSkill.layoutTestScore.tvTopSkill.text =
            getString(R.string.your_top_skill)

        binding.layoutClickToWatchTopSkill.layoutTopSkill.layoutTestScore.valueTopSkill.text =
            testSkillsRes!!.top_skill

        binding.layoutClickToWatchTopSkill.layoutTopSkill.layoutTestScore.tvScore.text =
            setCorrectlyAnsweredQuestions()

        binding.layoutClickToWatchTopSkill.layoutTopSkill.layoutCorrectlyAnswered.tvSkillWiseAccuracy.text =
            getString(R.string.skill_wise_accuracy)
    }

    private fun setCorrectlyAnsweredQuestions(): SpannableString {
        val questionStr = getString(R.string.test_feedback_questions)
        val marksObtained = testSkillsRes?.correct_answer
        val totalMarks = testSkillsRes?.questions
        val bothValueAsStr = "$marksObtained/$totalMarks"
        return Utils.getSpannable(
            this, "$bothValueAsStr $questionStr",
            R.color.colorGreen, 0, marksObtained.toString().length
        )
    }

    private fun setAndAnimateProgress() {
        val goodScore = testAttemptsRes?.scoreListInfo?.goodScore!!
        val yourScore = testAttemptsRes?.scoreListInfo?.myScore!!
        val cutOffScore = testAttemptsRes?.scoreListInfo?.cutOffScore!!
        var totalScore = 100
        //to avoid values going of screen
        var extraBiasSpace = getHorizontalBiasOfBaseLine(totalScore)
        totalScore += extraBiasSpace
        totalScore += 20
//        if (mySectionSummaryList.size >= 5)
//            changeMotionLayoutConstraintSetToFit()

        animate(
            goodScore.toInt(),
            totalScore,
            binding.layoutTitleCharts.layoutChart.layoutFeedbackProgress.progressBarGoodScore,
            binding.layoutTitleCharts.layoutChart.layoutFeedbackProgress.tvGoodScore
        )
//        removeMarginTopFromGoodScoreProgressBar(goodScore)
        moveVerticalBaseLineBiasBasedOnScore(yourScore, totalScore)
        moveCutOffLineBiasBasedOnScore(cutOffScore, totalScore)
//        moveHorizontalCutOffMarkerBiasBasedOnScore(cutOffScore, totalScore)
        if (yourScore < 0) {
            animate(
                yourScore.toInt(),
                totalScore,
                binding.layoutTitleCharts.layoutChart.layoutFeedbackProgress.progressBarNegative,
                binding.layoutTitleCharts.layoutChart.layoutFeedbackProgress.tvNegativeScore
            )
        } else {
            animate(
                yourScore.toInt(),
                totalScore,
                binding.layoutTitleCharts.layoutChart.layoutFeedbackProgress.progressBarYourScore,
                binding.layoutTitleCharts.layoutChart.layoutFeedbackProgress.tvYourScore
            )
        }
    }

    private fun getHorizontalBiasOfBaseLine(totalScore: Int): Int {
        var params =
            binding.layoutTitleCharts.layoutChart.layoutFeedbackProgress.viewLine.layoutParams as ConstraintLayout.LayoutParams
        return params.horizontalBias.times(totalScore).toInt()
    }

    private fun moveVerticalBaseLineBiasBasedOnScore(yourScore: Double, totalScore: Int) {
        if (yourScore < 0) {
            val baseLineVertical =
                binding.layoutTitleCharts.layoutChart.layoutFeedbackProgress.viewLine
            val baseLineParams = baseLineVertical.layoutParams as ConstraintLayout.LayoutParams
            baseLineParams.horizontalBias =
                baseLineParams.horizontalBias + (Math.abs(yourScore).toFloat() / totalScore)
            baseLineVertical.layoutParams = baseLineParams
            baseLineVertical.requestLayout()
        }
    }

    private fun moveCutOffLineBiasBasedOnScore(cutOffScore: Int, totalScore: Int) {
        // AnyScore/totalScore to get bias value
        //totalMarks 130, Negative Marks -12

        val calScore = (cutOffScore.toDouble().div(totalScore))
        val cutOffLine = binding.layoutTitleCharts.layoutChart.layoutFeedbackProgress.ivCutOffMarker
        val cutOffLabel =
            binding.layoutTitleCharts.layoutChart.layoutFeedbackProgress.tvCutOffMarkerLabel
        val cutOffLineParams = cutOffLine.layoutParams as ConstraintLayout.LayoutParams
        val cutOffLineAnimator = ValueAnimator.ofFloat(0f, calScore.toFloat())

        cutOffLineAnimator.addUpdateListener { value ->
            cutOffLineParams.horizontalBias = (value.animatedValue as Float)
            cutOffLine.layoutParams = cutOffLineParams
        }

        val cutOffLabelAnimator = ValueAnimator.ofInt(0, cutOffScore)
        cutOffLabelAnimator.addUpdateListener { value ->
            cutOffLabel.text = (value.animatedValue as Int).toString()
        }

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(cutOffLabelAnimator, cutOffLineAnimator)
        animatorSet.duration = 1500
        animatorSet.start()
    }

    private fun animate(progressValue: Int, totalScore: Int, progressBar: ImageView, tv: TextView) {
        animateBar(progressBar, progressValue, totalScore)
        tv.animateWithProgressBar(progressValue)
    }

    private fun TextView.animateWithProgressBar(progressValue: Int) {

        val animator = ObjectAnimator.ofInt(0, progressValue)
        animator.addUpdateListener { animationValue ->
            this.text = animationValue.animatedValue.toString()
        }
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(animator)
        animatorSet.duration = 2000
        animatorSet.start()
    }

    private fun animateBar(bar: ImageView, progressValue: Int, totalScore: Int) {
        val progressLayout = binding.layoutTitleCharts.layoutChart.layoutFeedbackProgress
        val vto = progressLayout.root.viewTreeObserver
        var newProgressValue = 0

        newProgressValue = kotlin.math.abs(progressValue)
        var listener: ViewTreeObserver.OnGlobalLayoutListener? = null

        listener = ViewTreeObserver.OnGlobalLayoutListener {
            progressLayoutWidth = progressLayout.root.width
            height = progressLayout.root.height

            val calValue = newProgressValue.toDouble() / totalScore * progressLayoutWidth
            val animator = ValueAnimator.ofInt(0, calValue.toInt())
            animator.addUpdateListener { valueAnimator ->
                val value = valueAnimator.animatedValue
                val params = bar.layoutParams as ConstraintLayout.LayoutParams
                if (value as Int > 0) {
                    bar.visibility = View.VISIBLE
                } else
                    bar.visibility = View.GONE
                params.width = value
                bar.layoutParams = params
            }

            animator.duration = 1500
            animator.start()

            binding.layoutTitleCharts.layoutChart.layoutFeedbackProgress.root.viewTreeObserver?.removeOnGlobalLayoutListener(
                listener!!
            )
        }
        vto?.addOnGlobalLayoutListener(listener)
    }

    private fun setDropDownQWA() {
        currentQWASubject = getSubjectList()[0]
        binding.layoutQWA.tvDropDownSubject.text = currentQWASubject
        if (getSubjectList().size > 1) {
            binding.layoutQWA.tvDropDownSubject.setOnClickListener {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Choose Subject")
                val array = getSubjectList().toTypedArray()
                builder.setItems(
                    array
                ) { dialog, which ->
                    if (getSubjectList()[which].toLowerCase().contains(ALL_SUBJECTS)) {
                        currentQWASubject = ALL_SUBJECTS
                        binding.layoutQWA.tvDropDownSubject.text = currentQWASubject
                        //setQuestionList(getQuestionListData(currentQWASubject))
                        setJarRecycler()
                    } else {
                        currentQWASubject = getSubjectList()[which]
                        binding.layoutQWA.tvDropDownSubject.text = currentQWASubject
                        //setQuestionList(getQuestionListData(currentQWASubject))
                        setJarRecycler()
                    }
                }

                val dialog = builder.create()
                dialog.show()
            }
        }
    }

    private fun getSubjectList(): List<String> {
        var slist = ArrayList<String>()
        if (testQuestionRes != null) {
            for (key in testQuestionRes!!.paper.sections.keys) {
                val section = testQuestionRes!!.paper.sections[key]
                slist.add(section!!.name)
            }
            if (slist.size > 1) {
                slist.add(0, ALL_SUBJECTS)
            }
        }
        return slist
    }

    private fun getSummaryByCode(code: String): TestQuestionSummaryResData? {
        if (testQuestionSummaryRes != null) {
            for (summary in testQuestionSummaryRes!!) {
                if (summary.question_code == code)
                    return summary
            }
        }
        return null
    }

    private fun getAttemptByCode(code: String): Attempt? {
        if (testAttemptsRes != null) {
            for (attempt in testAttemptsRes!!.attempts!!) {
                if (attempt.questionCode == code)
                    return attempt
            }
        }
        return null
    }

    private fun updateUIForWebview() {
//        if (UserData.getAchieveOfCurrentUser()) {
//            binding.layoutQWA.wvQWA.visibility = View.GONE
//        } else {
//            setQWAWebView()
//        }

        setQWAWebView()
    }

    private fun setQWAWebView() {
        binding.layoutQWA.wvQWA.webChromeClient = WebChromeClient()
        binding.layoutQWA.wvQWA.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                Log.d("WEBVIEW", "Web view ready")
            }
        }
        binding.layoutQWA.wvQWA.addJavascriptInterface(
            CommonAndroidAPI(this), CommonAndroidAPI.NAME
        )
        binding.layoutQWA.wvQWA.settings.allowFileAccess = true
        binding.layoutQWA.wvQWA.isLongClickable = false
        binding.layoutQWA.wvQWA.settings.javaScriptEnabled = true
        binding.layoutQWA.wvQWA.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        binding.layoutQWA.wvQWA.settings.domStorageEnabled = true
        binding.layoutQWA.wvQWA.setBackgroundColor(Color.argb(0, 0, 0, 0))
        binding.layoutQWA.wvQWA.setLayerType(LAYER_TYPE_SOFTWARE, null)
        binding.layoutQWA.wvQWA.addJavascriptInterface(
            CommonAndroidAPI(this), CommonAndroidAPI.NAME
        )
        binding.layoutQWA.wvQWA.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        binding.layoutQWA.wvQWA.loadUrl("http://52.172.137.100:8000/?&invokeModule=question_list")
    }

    private fun setQWAButtonListener() {
        binding.layoutQWA.btnPerfectAttempts.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.PERFECT_ATTEMPTS)
        }
        binding.layoutQWA.btnOverTimeCorrectAttempts.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.OVERTIME_CORRECT)
        }
        binding.layoutQWA.btnTooFastCorrectAttempts.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.TOO_FAST_CORRECT_ATTEMPTS)
        }
        binding.layoutQWA.btnInCorrectAttempts.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.INCORRECT_ATTEMPTS)
        }
        binding.layoutQWA.btnOverTimeInCorrectAttempts.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.OVERTIME_INCORRECT_ATTEMPTS)
        }
        binding.layoutQWA.btnWastedAttempts.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.WASTED_ATTEMPTS)
        }
        binding.layoutQWA.btnUnAttempted.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackTestFeedbackScreenAttemptFocus(AppConstants.UNATTEMPTED_ATTEMPTS)
        }

        //****Click Listener******
        binding.layoutQWA.btnPerfectAttempts.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.PERFECT_ATTEMPTS)
            perfectAttemptOnClick()
        }
        binding.layoutQWA.btnOverTimeCorrectAttempts.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.OVERTIME_CORRECT_ATTEMPTS)
            overtimeCorrectOnClick()
        }
        binding.layoutQWA.btnTooFastCorrectAttempts.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.TOO_FAST_CORRECT_ATTEMPTS)
            tooFastCorrectOnClick()
        }
        binding.layoutQWA.btnInCorrectAttempts.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.INCORRECT_ATTEMPTS)
            incorrectOnClick()
        }
        binding.layoutQWA.btnOverTimeInCorrectAttempts.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.OVERTIME_INCORRECT_ATTEMPTS)
            overtimeIncorrectOnClick()
        }
        binding.layoutQWA.btnWastedAttempts.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.WASTED_ATTEMPTS)
            wastedOnClick()
        }
        binding.layoutQWA.btnUnAttempted.setOnClickListener {
            SegmentUtils.trackTestFeedbackScreenAttemptClick(AppConstants.UNATTEMPTED_ATTEMPTS)
            unattemptedOnClick()
        }
    }

    private fun perfectAttemptOnClick() {
        val questionList = getQuestionListData(currentQWASubject, Perfect.name)
        if (questionList.size > 0)
            setQuestionList(questionList)
        else {
            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
        }
        if (isJarEnabled) {
            setAttemptQualityView(R.drawable.jar_perfect_attempt)
        }
    }

    private fun overtimeCorrectOnClick() {
        val questionList = getQuestionListData(currentQWASubject, OvertimeCorrect.name)
        if (questionList.size > 0)
            setQuestionList(questionList)
        else {
            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
        }
        if (isJarEnabled) {
            setAttemptQualityView(R.drawable.jar_overtime_correct)
        }
    }

    private fun tooFastCorrectOnClick() {
        val questionList = getQuestionListData(currentQWASubject, TooFastCorrect.name)
        if (questionList.size > 0)
            setQuestionList(questionList)
        else {
            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
        }
        if (isJarEnabled) {
            setAttemptQualityView(R.drawable.jar_too_fast_correct)
        }
    }

    private fun incorrectOnClick() {
        val questionList = getQuestionListData(currentQWASubject, Incorrect.name)
        if (questionList.size > 0)
            setQuestionList(questionList)
        else {
            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
        }
        if (isJarEnabled) {
            setAttemptQualityView(R.drawable.jar_incorrect_answer)
        }
    }

    private fun overtimeIncorrectOnClick() {
        val questionList = getQuestionListData(currentQWASubject, OvertimeIncorrect.name)
        if (questionList.size > 0)
            setQuestionList(questionList)
        else {
            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
        }
        if (isJarEnabled) {
            setAttemptQualityView(R.drawable.jar_overtime_incorrect)
        }
    }

    private fun wastedOnClick() {
        val questionList = getQuestionListData(currentQWASubject, Wasted.name)
        if (questionList.size > 0)
            setQuestionList(questionList)
        else {
            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
        }
        if (isJarEnabled) {
            setAttemptQualityView(R.drawable.jar_wasted_attempt)
        }
    }

    private fun unattemptedOnClick() {
        val questionList = getQuestionListData(currentQWASubject, NonAttempt.name)
        if (questionList.size > 0)
            setQuestionList(questionList)
        else {
            binding.layoutQWA.wvQWA.visibility = View.INVISIBLE
        }
        if (isJarEnabled) {
            setAttemptQualityView(R.drawable.jar_unattempted)
        }
    }

    private fun getQuestionListData(subject: String, type: String): ArrayList<Question> {
        val questionList = ArrayList<Question>()
        currentAttemptType = type
        if (testQuestionRes != null) {
            for (key in testQuestionRes!!.paper.sections.keys) {
                val section = testQuestionRes!!.paper.sections[key]
                if (section!!.name == subject || subject == ALL_SUBJECTS) {
                    for (question in section.questions) {
                        if (question.attemptTypeBadge == type) {
                            questionList.add(question)
                        }
                    }
                }
            }
        }
        binding.layoutQWA.layoutAQ.AQ.text =
            calculateAttemptPercent(testAttemptsRes!!, type, subject, questionList) + " " + type
        return questionList
    }

    private fun setQuestionList(questionList: ArrayList<Question>) {
        binding.layoutQWA.wvQWA.visibility = View.VISIBLE
        val str = Gson().newBuilder().serializeNulls().create()
            .toJson(questionList)
        Log.i("json data", str)
        binding.layoutQWA.wvQWA.evaluateJavascript("commonWebviewAPI.setQuestionList($str)", null)
    }

    private fun setQWAFocusListener() {
        binding.layoutQWA.btnUnAttempted.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (layoutAQVisibility) {
                            currentFocus?.clearFocus()
                            binding.layoutQWA.wvQWA.requestFocus()
                        } else {
                            currentFocus?.clearFocus()
                            binding.layoutQWA.layoutJar.rvJar.requestFocus()
                        }
                        return@setOnKeyListener true
                    }
                }
            }
            false
        }
        binding.layoutQWA.btnTooFastCorrectAttempts.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (layoutAQVisibility) {
                            currentFocus?.clearFocus()
                            binding.layoutQWA.wvQWA.requestFocus()
                        } else {
                            currentFocus?.clearFocus()
                            binding.layoutQWA.layoutJar.rvJar.requestFocus()
                        }
                        return@setOnKeyListener true
                    }

                }
            }
            false
        }
    }

    private fun setJarRecycler() {
        jarAdapter = TestJarAdapter(this)
        binding.layoutQWA.layoutJar.rvJar.adapter = jarAdapter
        jarAdapter.setData(createJarData())

        jarAdapter.onItemClick = { jarItem ->
            performOnClickBasedOnJar(jarItem)
            animateJar(jarItem.jar)
        }
    }

    private fun createJarData(): ArrayList<TestFeedbackJarModel> {
        return arrayListOf(
            TestFeedbackJarModel(
                getQuestionListData(currentQWASubject, TooFastCorrect.name).size,
                R.drawable.jar_too_fast_correct
            ),
            TestFeedbackJarModel(
                getQuestionListData(currentQWASubject, Perfect.name).size,
                R.drawable.jar_perfect_attempt
            ),
            TestFeedbackJarModel(
                getQuestionListData(currentQWASubject, OvertimeCorrect.name).size,
                R.drawable.jar_overtime_correct
            ),
            TestFeedbackJarModel(
                getQuestionListData(currentQWASubject, Wasted.name).size,
                R.drawable.jar_wasted_attempt
            ),
            TestFeedbackJarModel(
                getQuestionListData(currentQWASubject, Incorrect.name).size,
                R.drawable.jar_incorrect_answer
            ),
            TestFeedbackJarModel(
                getQuestionListData(currentQWASubject, OvertimeIncorrect.name).size,
                R.drawable.jar_overtime_incorrect
            ),
            TestFeedbackJarModel(
                getQuestionListData(currentQWASubject, NonAttempt.name).size,
                R.drawable.jar_unattempted
            )
        )
    }

    private fun performOnClickBasedOnJar(jarItem: TestFeedbackJarModel) {
        when (jarItem.jar) {
            R.drawable.jar_perfect_attempt -> perfectAttemptOnClick()
            R.drawable.jar_too_fast_correct -> tooFastCorrectOnClick()
            R.drawable.jar_overtime_correct -> overtimeCorrectOnClick()
            R.drawable.jar_overtime_incorrect -> overtimeIncorrectOnClick()
            R.drawable.jar_incorrect_answer -> incorrectOnClick()
            R.drawable.jar_wasted_attempt -> wastedOnClick()
            R.drawable.jar_unattempted -> unattemptedOnClick()
        }
    }

    override fun onJarSelected(item: TestFeedbackJarModel) {
        animateJar(item.jar)
        isJarEnabled = true
    }

    private fun animateJar(jarType: Int) {
        binding.layoutQWA.layoutAQ.qaMain.visibility = View.GONE
        when (jarType) {
            R.drawable.jar_perfect_attempt -> animatePerfectAttemptJar()
            R.drawable.jar_overtime_correct -> animateOvertimeCorrectJar()
            R.drawable.jar_too_fast_correct -> animateTooFastCorrectJar()
            R.drawable.jar_incorrect_answer -> animateIncorrectJar()
            R.drawable.jar_overtime_incorrect -> animateOvertimeIncorrectJar()
            R.drawable.jar_wasted_attempt -> animateWastedJar()
            R.drawable.jar_unattempted -> animateUnattemptedJar()
        }
    }

    private fun animatePerfectAttemptJar() {
        ObjectAnimator.ofFloat(
            binding.layoutQWA.imgAttemptJarIcon, "translationX", 20f
        ).apply {
            duration = 500
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    viewAnimationImage(2)
                    perfectAttemptOnClick()
                }

                override fun onAnimationEnd(animation: Animator) {
                    setAttemptQualityView(jarType)
                    binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
                    binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
                        .translationY(0f).duration = 500
                }

                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
            })
            start()
        }
    }

    private fun animateOvertimeCorrectJar() {
        ObjectAnimator.ofFloat(
            binding.layoutQWA.imgAttemptJarIcon, "translationX", -60f
        ).apply {
            duration = 500
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    viewAnimationImage(3)
                    overtimeCorrectOnClick()
                }

                override fun onAnimationEnd(animation: Animator) {
                    setAttemptQualityView(jarType)
                    binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
                    binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
                        .translationY(0f).duration = 500
                }

                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
            })
            start()
        }
    }

    private fun animateTooFastCorrectJar() {
        ObjectAnimator.ofFloat(
            binding.layoutQWA.imgAttemptJarIcon, "translationX", 60f
        ).apply {
            duration = 500
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    viewAnimationImage(1)
                    tooFastCorrectOnClick()
                }

                override fun onAnimationEnd(animation: Animator) {
                    setAttemptQualityView(jarType)
                    binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
                    binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
                        .translationY(0f).duration = 500

                }

                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
            })
            start()
        }
    }

    private fun animateIncorrectJar() {
        ObjectAnimator.ofFloat(
            binding.layoutQWA.imgAttemptJarIcon, "translationX", -180f
        ).apply {
            duration = 500
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    viewAnimationImage(5)
                    incorrectOnClick()
                }

                override fun onAnimationEnd(animation: Animator) {
                    setAttemptQualityView(jarType)
                    binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
                    binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
                        .translationY(0f).duration = 500
                }

                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
            })
            start()
        }
    }

    private fun animateOvertimeIncorrectJar() {
        ObjectAnimator.ofFloat(
            binding.layoutQWA.imgAttemptJarIcon, "translationX", -240f
        ).apply {
            duration = 500
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    viewAnimationImage(6)
                    overtimeIncorrectOnClick()
                }

                override fun onAnimationEnd(animation: Animator) {
                    setAttemptQualityView(jarType)
                    binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
                    binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
                        .translationY(0f).duration = 500
                }

                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
            })
            start()
        }
    }

    private fun animateWastedJar() {
        ObjectAnimator.ofFloat(
            binding.layoutQWA.imgAttemptJarIcon, "translationX", -120f
        ).apply {
            duration = 500
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    viewAnimationImage(4)
                    wastedOnClick()
                }

                override fun onAnimationEnd(animation: Animator) {
                    setAttemptQualityView(jarType)
                    binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
                    binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
                        .translationY(0f).duration = 500
                }

                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
            })
            start()
        }
    }

    private fun animateUnattemptedJar() {
        ObjectAnimator.ofFloat(
            binding.layoutQWA.imgAttemptJarIcon, "translationX", -260f
        ).apply {
            duration = 500
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    viewAnimationImage(7)
                    unattemptedOnClick()
                }

                override fun onAnimationEnd(animation: Animator) {
                    setAttemptQualityView(jarType)
                    binding.layoutQWA.imgAttemptJarIcon.visibility = View.GONE
                    binding.layoutQWA.imgAttemptJarIcon.animate().translationX(0f)
                        .translationY(0f).duration = 500
                }

                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
            })
            start()
        }
    }

    private fun viewAnimationImage(position: Int) {
        binding.layoutQWA.focusedBtn.requestFocus()
        binding.layoutQWA.layoutJar.rvJar.visibility = View.GONE
        isJarEnabled = true
        binding.layoutQWA.imgAttemptJarIcon.alpha = 1.0F
        binding.layoutQWA.imgAttemptJarIcon.visibility = View.VISIBLE
        when (position) {
            1 -> binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_too_fast_correct)
            2 -> binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_perfect_attempt)
            3 -> binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_correct)
            4 -> binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_wasted_attempt)
            5 -> binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_incorrect_answer)
            6 -> binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_incorrect)
            7 -> binding.layoutQWA.imgAttemptJarIcon.setImageResource(R.drawable.jar_unattempted)
        }

        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        if (position < 5)
            lp.setMargins((position - 1) * 60, 20, 0, 0)
        if (position == 5)
            lp.setMargins((position - 1) * 70, 20, 0, 0)
        if (position == 6)
            lp.setMargins((position - 1) * 80, 20, 0, 0)

        binding.layoutQWA.imgAttemptJarIcon.layoutParams = lp
    }

    private fun setAttemptQualityView(jarType: Int) {
        this.jarType = jarType
        binding.layoutQWA.layoutJar.rvJar.visibility = View.GONE
        binding.layoutQWA.layoutAQ.qaMain.visibility = View.VISIBLE
        binding.layoutQWA.layoutAQ.qaMain.requestFocus()
        when (jarType) {
            R.drawable.jar_too_fast_correct ->
                binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(R.raw.new_jar_correct)
            R.drawable.jar_perfect_attempt ->
                binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(R.raw.new_jar_perfect_attempts)
            R.drawable.jar_wasted_attempt ->
                binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(R.raw.new_jar_wasted_attempt)
            R.drawable.jar_incorrect_answer ->
                binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(R.raw.new_jar_wrong)
            R.drawable.jar_overtime_incorrect ->
                binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(R.raw.new_jar_overtime_incorrect)
            R.drawable.jar_unattempted ->
                binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(R.raw.new_jar_overtime_incorrect)
            R.drawable.jar_overtime_correct ->
                binding.layoutQWA.layoutAQ.aqImageJar.setAnimation(R.raw.new_jar_overtime_correct)
        }
        binding.layoutQWA.layoutAQ.aqImageJar.loop(true)
        binding.layoutQWA.layoutAQ.aqImageJar.playAnimation()
    }

    private fun calculateAttemptPercent(
        testAttemptsRes: TestAttemptsRes,
        attemptType: String,
        subject: String,
        questionList: java.util.ArrayList<Question>
    ): String {
        var percentage: Int = 0
        if (testAttemptsRes != null && testAttemptsRes.attempts != null && testAttemptsRes.attempts!!.size > 0) {
            var attemptSize =
                testAttemptsRes.attempts!!.filter {
                    it.attemptTypeBadge == attemptType && (subject == AppConstants.ALL_SUBJECTS || isQuestionAvailable(
                        it.questionCode!!,
                        questionList
                    ))
                }.size
            var totalSize =
                if (subject == ALL_SUBJECTS) testAttemptsRes.attempts!!.size else testAttemptsRes.attempts!!.filter {
                    isQuestionAvailable(
                        it.questionCode!!,
                        getQListData(subject, ALL_ATTEMPTS)
                    )
                }.size
            percentage =
                if (attemptSize == 0) 0 else ((attemptSize.toDouble() / totalSize.toDouble()) * 100).toInt()
        }
        return "$percentage%"
    }

    private fun isQuestionAvailable(
        qcode: String,
        questionList: java.util.ArrayList<Question>
    ): Boolean {
        var pos = questionList.filter { it.code == qcode }.size
        return pos > 0
    }

    private fun getQListData(
        subject: String,
        type: String
    ): ArrayList<Question> {
        var qlist = ArrayList<Question>()
        if (testQuestionRes != null) {
            for (key in testQuestionRes!!.paper.sections.keys) {
                val section = testQuestionRes!!.paper.sections[key]
                if (section!!.name == subject || subject == ALL_SUBJECTS) {
                    for (question in section.questions) {
                        if (question.attemptTypeBadge == type || type == ALL_ATTEMPTS) {
                            qlist.add(question)
                        }
                    }
                }
            }
        }
        return qlist
    }

    private fun moveToTestQWA(question: Question, pos: Int) {
        with(DataManager.instance) {
            setTestQuestion(Gson().toJson(testQuestionRes!!))
            setCurrentQuestion(Gson().toJson(question))
        }
        startActivity(
            TestQWAActivity.getQWAIntent(this, pos, currentQWASubject, currentAttemptType)
        )
    }

    private fun updateUIForAchieve() {
        if (UserData.getAchieveOfCurrentUser()) {
            binding.textAchieve.text = getString(R.string.your_personalised_achievement_journey)
            binding.layoutAchieve.root.visibility = View.GONE
            binding.layoutDiagnosticAchieve.root.visibility = View.VISIBLE

            animateDiagnosticParticles()
            setDiagnosticAchieveMetrics()
            setDiagnosticAchieveRecycler()
            updateStartTestUI()
        } else {
            binding.textAchieve.text = getString(R.string.how_do_i_achieve_a_high_score)
            binding.layoutDiagnosticAchieve.root.visibility = View.GONE
            binding.layoutAchieve.root.visibility = View.VISIBLE

            animateParticles()
            setAchieveMetrics()
            setAchieveRecycler()
        }
    }

    private fun animateDiagnosticParticles() {
        val controller = Fresco.newDraweeControllerBuilder()
        controller.autoPlayAnimations = true
        val uri: Uri = UriUtil.getUriForResourceId(R.drawable.achieve_sphere_animation)
        controller.setUri(uri)
        binding.layoutDiagnosticAchieve.ivParticles.controller = controller.build()
    }

    private fun animateParticles() {
        val controller = Fresco.newDraweeControllerBuilder()
        controller.autoPlayAnimations = true
        val uri: Uri = UriUtil.getUriForResourceId(R.drawable.achieve_sphere_animation)
        controller.setUri(uri)
        binding.layoutAchieve.ivParticles.controller = controller.build()
    }

    private fun setDiagnosticAchieveMetrics() {

        binding.layoutDiagnosticAchieve.tvSphere1.text =
            if (testPAJData!!.currentGrade.isNullOrEmpty()
                || testPAJData!!.currentGrade!!.toLowerCase().contains("null")
            ) "NA" else testPAJData!!.currentGrade!!

        binding.layoutDiagnosticAchieve.tvSphere2.text =
            if (testPAJData!!.potentialGrade!!.isNullOrEmpty()
                || testPAJData!!.potentialGrade!!.toLowerCase().contains("null")
            ) "NA" else testPAJData!!.potentialGrade!!
    }

    private fun setAchieveMetrics() {
        if (!testAchieveRes?.achieve?.metric.isNullOrBlank()) {
            val type = testAchieveRes?.achieve?.metric!!.capitalize()
            val metricType = "Your\nCurrent\n$type"

            val metricTypePotential = "Your\nPotential\n$type"

            binding.layoutAchieve.tvSphere1Left.text = metricType
            binding.layoutAchieve.tvSphere1Right.text = metricTypePotential
        }
        binding.layoutAchieve.tvSphere1.text =
            if (testAchieveRes?.achieve?.current.isNullOrEmpty()
                || testAchieveRes?.achieve?.current!!.toLowerCase().contains("null")
            ) "NA" else testAchieveRes?.achieve?.current

        binding.layoutAchieve.tvSphere2.text =
            if (testAchieveRes?.achieve?.potential.isNullOrEmpty()
                || testAchieveRes?.achieve?.potential!!.toLowerCase().contains("null")
            ) "NA"
            else testAchieveRes?.achieve?.potential
    }

    private fun setAchieveRecycler() {
        DataManager.instance.testCode = testCode
        if (::content.isInitialized) {
            DataManager.instance.content = content
        }
        menuAdapter = TestFeedbackMenuAdapter()
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.layoutAchieve.recycler.adapter = menuAdapter
        binding.layoutAchieve.recycler.layoutManager = layoutManager
        menuAdapter.setData(createAchieveData())
        menuAdapter.onItemLeftClick = {
            coroutineScope.launch {
                delay(10)
                binding.layoutQWA.clQuestionWise.btnWastedAttempts.requestFocus()
            }
        }
    }

    private fun setDiagnosticAchieveRecycler() {
        diagnosticTestFeedbackAdapter = DiagnosticTestFeedbackAdapter()
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        binding.layoutDiagnosticAchieve.recycler.adapter = diagnosticTestFeedbackAdapter
        binding.layoutDiagnosticAchieve.recycler.layoutManager = layoutManager
        diagnosticTestFeedbackAdapter.setData(testPAJData!!.steps as ArrayList<Step>)
    }

    private fun updateStartTestUI() {
        if (testPAJData?.diagnosticTest != null) {
            val diagnosticTest = testPAJData!!.diagnosticTest
            if (diagnosticTest?.status == AppConstants.DIAGNOSTIC_TEST_STATUS_LOCKED) {
                updateStartTestUIForLockedState()
            } else {
                updateStartTestUIForOpenState(diagnosticTest!!)
            }
        } else {
            updateStartTestUIForLockedState()
        }
    }

    private fun updateStartTestUIForLockedState() {
        showOverlay()
        hideStartTestUI()
    }

    private fun updateStartTestUIForOpenState(diagnosticTest: DiagnosticTest) {
        hideOverlay()
        showStartTestUI(diagnosticTest)
    }

    private fun showOverlay() {
        binding.layoutDiagnosticAchieve.layoutStartTest.lockedOverlay.visibility = View.VISIBLE
        binding.layoutDiagnosticAchieve.layoutStartTest.ivLock.visibility = View.VISIBLE
        binding.layoutDiagnosticAchieve.layoutStartTest.lockedOverlay.setImageResource(R.drawable.start_test_overlay)
    }

    private fun hideStartTestUI() {
        binding.layoutDiagnosticAchieve.layoutStartTest.textCertifyAchievement.visibility =
            View.GONE
        binding.layoutDiagnosticAchieve.layoutStartTest.btnStartTest.visibility = View.GONE
    }

    private fun hideOverlay() {
        binding.layoutDiagnosticAchieve.layoutStartTest.lockedOverlay.visibility = View.GONE
        binding.layoutDiagnosticAchieve.layoutStartTest.ivLock.visibility = View.GONE
        binding.layoutDiagnosticAchieve.layoutStartTest.iv_bg.setImageResource(R.drawable.start_test_bg_for_achieve)
    }

    private fun showStartTestUI(diagnosticTest: DiagnosticTest) {
        binding.layoutDiagnosticAchieve.layoutStartTest.textCertifyAchievement.visibility =
            View.VISIBLE
        binding.layoutDiagnosticAchieve.layoutStartTest.btnStartTest.visibility = View.VISIBLE

        binding.layoutDiagnosticAchieve.layoutStartTest.btnStartTest.setOnClickListener {

            val testIntent = Intent(this, TestActivity::class.java)
            testIntent.putExtra(AppConstants.TEST_XPATH, diagnosticTest.xpath)
            testIntent.putExtra(AppConstants.TEST_CODE, diagnosticTest.bundleId)
            testIntent.putExtra(AppConstants.IS_DIAGNOSTIC_TEST_COMPLETED, true)
            testIntent.putExtra(AppConstants.PAJ_ID, testPAJData?.pajId)
            startActivity(testIntent)
            finish()
        }
    }


    private fun createAchieveData(): ArrayList<TestFeedbackMenuModel> {
        return arrayListOf(
            TestFeedbackMenuModel(
                "Achieve Amazing Results with your \npersonalised achievement journey",
                arrayListOf(), R.drawable.achieve_bg
            ),
            TestFeedbackMenuModel(
                "Get Your Revision Lists", getRevisionList(), R.drawable.revision_list
            ),
            TestFeedbackMenuModel(
                "Improve Your \nTest Taking Strategy",
                getImprovingStrategyList(), R.drawable.test_taking_banner
            )
        )
    }

    private fun getRevisionList(): ArrayList<SubFeedbackMenuModel> {
        val revisions = testAchieveRes?.revisions
        val revisionList = ArrayList<SubFeedbackMenuModel>()
        for (index in 0 until revisions?.size!!) {
            revisionList.add(SubFeedbackMenuModel(revisions[index].display_name!!, arrayListOf()))
        }

        return revisionList
    }

    private fun getImprovingStrategyList(): ArrayList<SubFeedbackMenuModel> {
        val improvingStrategy = testAchieveRes?.improveStrategy
        val improveStrategyList = ArrayList<SubFeedbackMenuModel>()

        if (improvingStrategy?.positiveBehaviour?.size != 0) {
            val behaviourList = ArrayList<TestBehaviour>()
            for (index in 0 until improvingStrategy?.positiveBehaviour?.size!!) {
                behaviourList.add(improvingStrategy.positiveBehaviour!![index])
            }
            improveStrategyList.add(
                SubFeedbackMenuModel("Understand My Positive Behaviour", behaviourList)
            )
        }

        if (improvingStrategy.negativeBehaviour?.size != 0) {
            val behaviourList = ArrayList<TestBehaviour>()
            for (index in 0 until improvingStrategy.negativeBehaviour?.size!!) {
                behaviourList.add(improvingStrategy.negativeBehaviour!![index])
            }
            improveStrategyList.add(
                SubFeedbackMenuModel("Understand My Negative Behaviour", behaviourList)
            )
        }

        return improveStrategyList
    }

    private fun getAchieveFeedback(): AchieveFeedbackRes {
        return fromJson(
            Utils.readJSONFromAsset("achieve_feedback_response_latest", this) ?: ""
        )
    }

    override fun focusUp() {
        super.focusUp()
        runOnUiThread {
            currentFocus?.clearFocus()
            binding.layoutQWA.btnUnAttempted.postDelayed({
                binding.layoutQWA.btnUnAttempted.requestFocus()
            }, 10)
        }
    }

    override fun getSelectedQuestion(qListJson: String) {
        try {
            Log.i("selected Position", qListJson)
            val json = JSONObject(qListJson)
            val pos = json.getInt("pos")
            val qData = json.getJSONObject("data")
            moveToTestQWA(Gson().fromJson(qData.toString(), Question::class.java), pos)
        } catch (exception: JSONException) {
        }
    }

    override fun setQuestion(singQuestionJson: String) {
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_DPAD_UP
            && viewFocusList[viewFocusCounter].isFocused
        )
            return true
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isJarEnabled) {
                binding.layoutQWA.layoutJar.rvJar.visibility = View.VISIBLE
                binding.layoutQWA.layoutAQ.qaMain.visibility = View.GONE
                isJarEnabled = false
                restoreJarFocus()
                this.jarType = -1
            } else {
                finish()
            }
        }
        if (viewFocusCounter == viewFocusList.size - 1) {
            return false
        }
        return true
    }

    object RowAnimDelayValues {
        const val ANIM_DELAY = 1000L
    }

    override fun onBackPressed() {
        if (isJarEnabled) {
            binding.layoutQWA.layoutJar.rvJar.visibility = View.VISIBLE
            binding.layoutQWA.layoutAQ.qaMain.visibility = View.GONE
            isJarEnabled = false
        } else {
            super.onBackPressed()
        }
    }

    private fun restoreJarFocus() {
        binding.layoutQWA.layoutJar.rvJar.requestFocus(jarType)
    }


    // expand shrink row logic
//    private fun changeMotionLayoutConstraintSetToFit() {
//        val startSet: ConstraintSet = correctlyAnsweredMotionLayout?.getConstraintSet(R.id.start)!!
//        startSet.connect(R.id.tv_currently_answered, TOP, R.id.iv_text_score, TOP, 10)
//        startSet.connect(R.id.tv_currently_answered, START, R.id.iv_text_score, START)
//        startSet.connect(R.id.tv_currently_answered, END, R.id.iv_text_score, END)
//        startSet.connect(R.id.tv_currently_answered, BOTTOM, R.id.iv_text_score, BOTTOM)
//        startSet.connect(R.id.tv_score, START, R.id.tv_currently_answered, END, 5)
//        startSet.connect(R.id.tv_score, TOP, R.id.tv_currently_answered, TOP)
//        startSet.connect(R.id.tv_score, BOTTOM, R.id.tv_currently_answered, BOTTOM)
//        startSet.connect(R.id.tv_score, END, R.id.iv_text_score, END)
//        startSet.connect(R.id.rv_score_bar, START, constraintLayout.id, START)
//        startSet.connect(R.id.rv_score_bar, END, constraintLayout.id, END)
//        startSet.connect(R.id.rv_score_bar, TOP, constraintLayout.id, TOP)
//        startSet.connect(R.id.rv_score_bar, BOTTOM, constraintLayout.id, BOTTOM)
//        startSet.setVerticalBias(R.id.rv_score_bar, 0.7f)
//        startSet.setHorizontalBias(R.id.rv_score_bar, 0.3f)
//        startSet.setHorizontalBias(R.id.tv_currently_answered, 0.3f)
//        startSet.setVerticalBias(R.id.tv_currently_answered, 0.0f)
//        startSet.setHorizontalBias(R.id.tv_score, 0.0f)
//        startSet.applyTo(correctlyAnsweredMotionLayout)
//
//        val endSet: ConstraintSet = correctlyAnsweredMotionLayout?.getConstraintSet(R.id.end)!!
//        endSet.connect(R.id.tv_currently_answered, TOP, R.id.iv_text_score, TOP, 20)
//        endSet.connect(R.id.tv_currently_answered, START, R.id.iv_text_score, START)
//        endSet.connect(R.id.tv_currently_answered, END, R.id.iv_text_score, END)
//        endSet.connect(R.id.tv_currently_answered, BOTTOM, R.id.iv_text_score, BOTTOM)
//        endSet.connect(R.id.tv_score, START, R.id.tv_currently_answered, END, 5)
//        endSet.connect(R.id.tv_score, TOP, R.id.tv_currently_answered, TOP)
//        endSet.connect(R.id.tv_score, BOTTOM, R.id.tv_currently_answered, BOTTOM)
//        endSet.connect(R.id.tv_score, END, R.id.iv_text_score, END)
//        endSet.connect(R.id.rv_score_bar, START, constraintLayout.id, START)
//        endSet.connect(R.id.rv_score_bar, END, constraintLayout.id, END)
//        endSet.connect(R.id.rv_score_bar, TOP, constraintLayout.id, TOP)
//        endSet.connect(R.id.rv_score_bar, BOTTOM, constraintLayout.id, BOTTOM)
//        endSet.setVerticalBias(R.id.rv_score_bar, 0.7f)
//        endSet.setHorizontalBias(R.id.rv_score_bar, 0.3f)
//        endSet.setHorizontalBias(R.id.tv_currently_answered, 0.3f)
//        endSet.setVerticalBias(R.id.tv_currently_answered, 0.0f)
//        endSet.setHorizontalBias(R.id.tv_score, 0.0f)
//        endSet.applyTo(correctlyAnsweredMotionLayout)
//    }
//
//    private fun expandFirstRow() {
//        animateCorrectlyAnsweredText()
//        scaleRvBar()
//        val params = binding.layoutChart.constraintCharts.layoutParams as LinearLayout.LayoutParams
//        Log.e(TAG, "height: ${params.height}")
//        val animator = ValueAnimator.ofInt(0, 130)
//        animator.addUpdateListener { animatorValue ->
//            val value = animatorValue.animatedValue
//            val calVal = 240 + value as Int
//            params.height = calVal
//            Log.e(TAG, "height: ${params.height}")
//            binding.layoutChart.constraintCharts.layoutParams = params
//
//        }
//        animator.duration = 1000
//        animator.start()
//        isFirstRowExpanded = true
//        binding.layoutChart.layoutTestScore.tvGradeLabel.visibility = View.VISIBLE
//        binding.layoutChart.layoutTestScore.tvGrade.visibility = View.VISIBLE
//    }
//
//    private fun shrinkFirstRow() {
//        animateCorrectlyAnsweredText()
//        binding.layoutChart.layoutCorrectlyAnswered.rvScoreBar.scale()
//        binding.layoutChart.layoutTestScore.tvGrade.visibility = View.GONE
//        binding.layoutChart.layoutTestScore.tvGradeLabel.visibility = View.GONE
//
//        val params =
//            binding.layoutChart.constraintCharts.layoutParams as LinearLayout.LayoutParams
//        Log.e(TAG, "height: ${params.height}")
//        val animator = ValueAnimator.ofInt(0, 190)
//        animator.addUpdateListener { animatorValue ->
//            val value = animatorValue.animatedValue
//            val calVal = 400 - value as Int
//            params.height = calVal
//            Log.e(TAG, "height: ${params.height}")
//            binding.layoutChart.constraintCharts.layoutParams = params
//        }
//        animator.duration = 1000
//        animator.start()
//        isFirstRowExpanded = false
//    }
//
//    private fun scaleRvBar() {
//        val rvScoreBar = binding.layoutChart.layoutCorrectlyAnswered.rvScoreBar
//        if (mySectionSummaryList.size >= 5) {
//            rvScoreBar.scale(1f, 1.2f)
//        } else if (mySectionSummaryList.size >= 2 || mySectionSummaryList.size <= 4) {
//            rvScoreBar.scale(1.2f, 1.2f)
//        }
//    }
//
//    private fun RecyclerView.scale(x: Float = 1f, y: Float = 1f) {
//        this.animate()?.scaleX(x)?.duration = 1000
//        this.animate()?.scaleY(y)?.duration = 1000
//    }
//
//    private fun animateCorrectlyAnsweredText() {
//        val motionLayout = binding.layoutChart.layoutCorrectlyAnswered.constraintLayout
//        motionLayout.transitionToStart()
//        motionLayout.transitionToEnd()
//    }
//
//    private fun increaseSizeOfFirstRow() {
//        val params = binding.layoutChart.constraintCharts.layoutParams as LinearLayout.LayoutParams
//        params.height = 400
//        binding.layoutChart.constraintCharts.layoutParams = params
//    }
}
