package com.embibe.embibetvapp.ui.adapter

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.animation.doOnEnd
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants.TYPE_FUTURE_SUCCESS
import com.embibe.embibetvapp.constant.AppConstants.TYPE_PROGRESS
import com.embibe.embibetvapp.databinding.ItemFutureSuccessBinding
import com.embibe.embibetvapp.model.SubjectReadiness
import com.embibe.embibetvapp.model.achieve.futureSuccess.FutureSuccessRes

class SubjectReadinessAdapter(var context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var totalDuration = 0L
    private var typeOfItem: String? = null
    private var list = ArrayList<SubjectReadiness>()
    private var list2: List<FutureSuccessRes> = ArrayList()
    private var itemCount: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_PROGRESS -> {
                var binding: ItemFutureSuccessBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_future_success,
                    parent,
                    false
                )
                ProgressViewHolder(binding)
            }
            TYPE_FUTURE_SUCCESS -> {
                var binding: ItemFutureSuccessBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_future_success,
                    parent,
                    false
                )
                FutureSuccessViewHolder(binding)
            }
            else -> throw IllegalArgumentException()

        }
    }

    override fun getItemCount(): Int = itemCount!!

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ProgressViewHolder -> holder.bind(list[position])
            is FutureSuccessViewHolder -> holder.bind(list2[position])
        }
    }


    fun setData(list: ArrayList<SubjectReadiness>) {
        this.list = list
        notifyDataSetChanged()
    }

    fun setItemCount(count: Int) {
        itemCount = count
    }

    fun setFutureSuccessData(list: List<FutureSuccessRes>) {
        this.list2 = list
        notifyDataSetChanged()
    }

    fun setType(type: String) {
        typeOfItem = type
    }

    override fun getItemViewType(position: Int): Int {
        return if (typeOfItem == context.getString(R.string.progress))
            TYPE_PROGRESS else TYPE_FUTURE_SUCCESS
    }

    abstract class ItemViewHolder<T>(binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        abstract fun bind(item: T)
    }

    inner class FutureSuccessViewHolder(var binding: ItemFutureSuccessBinding) :
        ItemViewHolder<FutureSuccessRes>(binding) {
        override fun bind(item: FutureSuccessRes) {
            if (!item.predictedGrade?.statusUrl.isNullOrEmpty()) {
                setActualData(item)
            } else {
                setNoData(item)
            }
        }

        private fun setNoData(item: FutureSuccessRes) {
            binding.tvSubjectPercentage.text = "?"
            binding.tvSubjectPercentage.visibility = View.VISIBLE
            binding.ivLikeDislike.visibility = View.GONE
        }

        private fun setActualData(item: FutureSuccessRes) {
            Glide.with(context).load(item.predictedGrade!!.statusUrl)
                .into(binding.ivLikeDislike)
            binding.ivLikeDislike.visibility = View.VISIBLE
            binding.tvSubjectPercentage.visibility = View.GONE
            binding.tvSubject.text = item.exam
        }
    }

    inner class ProgressViewHolder(var binding: ItemFutureSuccessBinding) :
        ItemViewHolder<SubjectReadiness>(binding) {
        private var isFinished: Boolean = false
        override fun bind(item: SubjectReadiness) {
            totalDuration += Value.DURATION
            Handler().postDelayed({
                binding.tvSubject.text = item.subjectName
                if (item.percentage != null) {
                    setPercentageData(item)
                } else {
                    setNoData(item)
                }
            }, totalDuration)

        }

        private fun setNoData(item: SubjectReadiness?) {
            binding.tvSubjectPercentage.text = "?"
            binding.tvSubjectPercentage.visibility = View.VISIBLE
            binding.progressBarPurple.progress = 0
            binding.progressBarPurple.progressDrawable = item?.progressBar?.let {
                ResourcesCompat.getDrawable(
                    context.resources,
                    it, null
                )
            }
        }

        private fun setPercentageData(item: SubjectReadiness) {
            binding.tvSubjectPercentage.visibility = View.VISIBLE
            binding.ivLikeDislike.visibility = View.GONE
            binding.tvSubjectPercentage.text = """${item.percentage}%"""
            animateProgress(binding.progressBarPurple, item.percentage)
            animatePercentage(binding.tvSubjectPercentage, item.percentage)
            binding.progressBarPurple.progressDrawable = item.progressBar?.let {
                ResourcesCompat.getDrawable(
                    context.resources,
                    it, null
                )
            }
        }

        private fun animatePercentage(tvSubjectPercentage: TextView, percentage: Int?) {
            val animator = percentage?.let { ValueAnimator.ofInt(0, it) }
            animator?.duration = Value.DURATION
            animator?.doOnEnd {
                isFinished = true
            }
            animator?.addUpdateListener { animation ->
                tvSubjectPercentage.text = animation.animatedValue.toString() + "%"
            }
            animator?.start()
        }

        private fun animateProgress(progressBar: ProgressBar?, percentage: Int?) {
            if (percentage != null) {
                ObjectAnimator.ofInt(progressBar, "progress", percentage)
                    .setDuration(Value.DURATION)
                    .start()
            }
        }
    }

    object Value {
        const val DURATION = 1000L
    }
}