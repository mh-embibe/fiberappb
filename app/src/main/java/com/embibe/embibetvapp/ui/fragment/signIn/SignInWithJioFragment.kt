package com.embibe.embibetvapp.ui.fragment.signIn

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.embibe.embibetvapp.BuildConfig
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.constant.AppConstants.SUBSCRIBER_ID
import com.embibe.embibetvapp.databinding.FragmentSignInWithOtpBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.UserData.getChildForGoalSet
import com.embibe.embibetvapp.model.auth.SignUpResponse
import com.embibe.embibetvapp.model.auth.login.LoginResponse
import com.embibe.embibetvapp.model.auth.otp.AuthOTP
import com.embibe.embibetvapp.model.auth.signup.UserExistResponse
import com.embibe.embibetvapp.model.jiostb.JioUser
import com.embibe.embibetvapp.ui.activity.AddGoalsActivity
import com.embibe.embibetvapp.ui.activity.SignInActivity
import com.embibe.embibetvapp.ui.activity.UserSwitchActivity
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.ConnectionManager
import com.embibe.embibetvapp.utils.ContantUtils
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.getEmail
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.model.LinkedProfile
import com.embibejio.coreapp.preference.PreferenceHelper
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class SignInWithJioFragment : BaseAppFragment() {

    private lateinit var binding: FragmentSignInWithOtpBinding
    private lateinit var signInViewModel: SignInViewModel
    private lateinit var homeViewModel: HomeViewModel

    private lateinit var subscriberId: String
    private var isAddChild = false
    private var isDataLoaded = false
    private var isTncSelected: Boolean = false
    var pref = PreferenceHelper()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in_with_otp, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showBannerVideoView()
        loopVideo()

        subscriberId = pref[SUBSCRIBER_ID]

        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

        binding.btnSignInJio.requestFocus()
        setUnderLinedTnC()
        setClickListeners()
//        setFocusListener()

        if (isTncSelected) {
//            binding.textTnCBold.requestFocus()
        }
    }

    private fun loopVideo() {
        binding.vvSignIn.setOnPreparedListener { mp -> mp.isLooping = true }


    }

    private fun setUnderLinedTnC() {
        val content = SpannableString(getString(R.string.term_condition))
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
//        binding.textTnCBold.text = content
    }

//    private fun setFocusListener() {
//        binding.etEmailMobile.setOnFocusChangeListener { v, hasFocus ->
//            when (hasFocus) {
//                true -> {
//                    openSoftKeyboard(context, binding.etEmailMobile)
//                }
//                false -> {
//                    binding.etEmailMobile.clearFocus()
//                    Utils.hideKeyboardFrom(
//                        context,
//                        binding.etEmailMobile.rootView
//                    )
//
//                }
//
//            }
//        }
//        binding.btnGetOtp.setOnFocusChangeListener { v, hasFocus ->
//            when (hasFocus) {
//                true -> {
//                    Utils.hideKeyboardFrom(
//                        context,
//                        binding.etEmailMobile.rootView
//                    )
//                }
//            }
//        }
//    }

    fun openSoftKeyboard(context: Context?, view: View) {
        view.requestFocus()
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)

    }

    private fun setClickListeners() {

        binding.btnSignInJio.setOnClickListener {

            callSignInJio()

        }

//        binding.txtTnC.setOnClickListener {
//            SegmentUtils.tracTermsConditionsClick()
//            (activity as SignInActivity).setFragment(
//                TermsAndConditionFragment(),
//                "terms of service"
//            )
//            isTncSelected = true
//        }
//
//        binding.textTnCBold.setOnClickListener {
//            SegmentUtils.tracTermsConditionsClick()
//            (activity as SignInActivity).setFragment(
//                TermsAndConditionFragment(),
//                "terms of service"
//            )
//            isTncSelected = true
//        }

//        binding.btnGetOtp.setOnClickListener {
//            var mobile = ""
//            var email = ""
//            var invalidInput = ""
//
//            val input = binding.etEmailMobile.text.toString().trim()
//            when (Utils.getInputType(input)) {
//                1 -> mobile = input
//                2 -> email = input
//                0 -> invalidInput = input
//            }
//
//            if (invalidInput.isNotEmpty() || input.isEmpty()) {
//                binding.tvInvalidMessage.visibility = View.VISIBLE
//                binding.tvInvalidMessage.text =
//                    activity?.getText(R.string.please_enter_a_valid_email_address_or_mobile_number)
//                binding.etEmailMobile.background =
//                    requireContext().getDrawable(R.drawable.ic_input_field_error)
//                return@setOnClickListener
//            }
//            if (mobile.isNotEmpty() && mobile.length < 10) {
//                binding.tvInvalidMessage.visibility = View.VISIBLE
//                binding.tvInvalidMessage.text =
//                    activity?.getText(R.string.please_enter_a_valid_mobile_number)
//                binding.etEmailMobile.background =
//                    requireContext().getDrawable(R.drawable.ic_input_field_error)
//                return@setOnClickListener
//            }
//            if (email.isNotEmpty() && !Utils.isEmailValid(email)) {
//                binding.tvInvalidMessage.visibility = View.VISIBLE
//                binding.tvInvalidMessage.text =
//                    activity?.getText(R.string.please_enter_a_valid_email_address)
//                binding.etEmailMobile.background =
//                    requireContext().getDrawable(R.drawable.ic_input_field_error)
//                return@setOnClickListener
//            }
//
//            binding.tvInvalidMessage.visibility = View.GONE
//
//            checkOTPID(binding.etEmailMobile.text.toString())
//        }

//        binding.etEmailMobile.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(text: Editable?) {
//                if (text!!.matches("[0-9]+".toRegex()) && text.length == 10) {
//                    binding.btnGetOtp.requestFocus()
//                    Utils.hideKeyboardFrom(
//                        context,
//                        binding.etEmailMobile.rootView
//                    )
//                }
//            }
//
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
//        })

        //Sign in with OTP
//        binding.btnSignInWithOtp.setOnClickListener {
//            //showToast("Under Development")
//            moveSignWithOTPScreen()
//        }
    }

    private fun callSignInJio() {
        if (ConnectionManager.instance.hasNetworkAvailable()) {
            getSTBJioUser()
        } else {
            hideProgress()
            Utils.showError(activity, ApiConstants.API_CODE_NO_NETWORK,
                object : BaseViewModel.ErrorCallBacks {
                    override fun onRetry(msg: String) {
                        callSignInJio()
                    }

                    override fun onDismiss() {

                    }
                }, object : BaseViewModel.BacKPressListener {
                    override fun onCustomBackPressed() {
                        /* back press event */
                    }

                })
        }
    }

    private fun updateSignInResponse(response: LoginResponse) {
        doAsync {
            updateEvent("success", response.userId.toString(), response.userType!!)
            if (response.linkedProfiles != null && response.linkedProfiles.isNotEmpty()) {
                updateLoginPreferences(response)
                setCurrentProfile(getChildForGoalSet(UserData.getLinkedProfileList()!!)[0])
                uiThread {
                    gotoNextActivity()
                }
            } else {
                updateLoginPreferences(response)
                uiThread {
                    hideProgress()
                    isAddChild = true
                    gotoNextActivity()
                }
            }
        }
    }

    private fun authenticatePassword() {
        val authOTPModel = AuthOTP()
        authOTPModel.fiber_user = true
        authOTPModel.password = subscriberId
        authOTPModel.login = getEmail(subscriberId)
        signInViewModel.authOTPSignIn(
            authOTPModel,
            object : BaseViewModel.APICallBacks<LoginResponse> {
                override fun onSuccess(model: LoginResponse?) {
                    if (model != null && model.success) {
                        updateSignInResponse(model)
                    } else {
                        hideProgress()
                        binding.clContentScreen.visibility = View.VISIBLE
                        makeLog("${getEmail(subscriberId)} login api failed")
                        if (model != null) {
                            if (model.error?.login != null) {
                                showToast("User ${model.error.login[0]}")
                            } else {
                                showToast("signUp api failed")
                            }
                        }
                        updateEvent("failed", "", "")
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    hideProgress()
                    binding.clContentScreen.visibility = View.VISIBLE
                    makeLog("login api error $error")
                    updateEvent("failed", "", "")
                    when {
                        code == 401 -> {
                            makeLog("authentication api error $error")
                        }
                        Utils.isApiFailed(code) -> {
                            Utils.showError(
                                context,
                                code,
                                object : BaseViewModel.ErrorCallBacks {
                                    override fun onRetry(msg: String) {
                                        authenticatePassword()
                                    }

                                    override fun onDismiss() {}
                                })
                        }
                        else -> {
                            showToast(error)
                        }
                    }
                }

            })

    }


    private fun updateLoginPreferences(response: LoginResponse) {
        UserData.updateLinkedUser(response)
        UserData.setCurrentLoginUserType(response.userType!!)
    }

    private fun updateEvent(status: String, userId: String, userType: String) {

        if (status == "success") {
            SegmentUtils.loginSuccessEvents(
                getEmail(subscriberId),
                subscriberId,
                userId,
                userType,
                status
            )
        } else {
            SegmentUtils.loginFailureEvents(getEmail(subscriberId), subscriberId, status)
        }
    }

    private fun setCurrentProfile(profile: LinkedProfile) {
        DataManager.instance.saveChildEmbibeToken(profile.embibe_token)
        UserData.setCurrentProfile(profile)
    }

    override fun onResume() {
        super.onResume()
        SegmentUtils.trackSignInLoadEnd()
    }

    private fun doSignUp() {
        val authSignUpModel = AuthOTP()
        authSignUpModel.fiber_user = true
        authSignUpModel.password = subscriberId
        authSignUpModel.login = getEmail(subscriberId)
        authSignUpModel.flag = "sp"
        authSignUpModel.user_type = "parent"
        authSignUpModel.first_name = "Guest"
        authSignUpModel.profile_pic_file_name = ContantUtils.mImgIds[listOf(0, 1, 2, 3, 4).random()]
        signInViewModel.signUp(
            authSignUpModel,
            object : BaseViewModel.APICallBacks<SignUpResponse> {
                override fun onSuccess(model: SignUpResponse?) {
                    if (model != null && model.success) {
                        authenticatePassword()
                    } else {
                        hideProgress()
                        makeLog("${getEmail(subscriberId)} signUp api failed")
                        if (model != null) {
                            if (model.error?.login != null) {
                                showToast("User ${model.error.login[0]}")
                            } else {
                                showToast("signUp api failed")
                            }
                        }
                        updateEvent("failed", "", "")
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    hideProgress()
                    binding.clContentScreen.visibility = View.VISIBLE
                    makeLog("signUp api error $error")
                    showToast("signUp api failed $error")
                    updateEvent("failed", "", "")
                    when {
                        code == 401 -> {
                            makeLog("authentication api error $error")
                        }
                        Utils.isApiFailed(code) -> {
                            Utils.showError(
                                context,
                                code,
                                object : BaseViewModel.ErrorCallBacks {
                                    override fun onRetry(msg: String) {
                                        doSignUp()
                                    }

                                    override fun onDismiss() {
                                        binding.clContentScreen.visibility = View.VISIBLE
                                    }
                                })
                        }
                        else -> {
                            showToast(error)
                        }
                    }
                }

            })
    }

    private fun gotoNextActivity() {
        if (isAddChild) {
            startActivity(Intent(activity, AddGoalsActivity::class.java))
            requireActivity().finish()
        } else {
            gotoHomeActivity()
        }
    }

    private fun gotoHomeActivity() {
        val intent = Intent(context, UserSwitchActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        Handler().postDelayed({
            activity?.finish()
        }, 1)

    }

    private fun checkSubscriptionID(subscriberId: String) {
        SegmentUtils.tracLoginWithJioClick()
        signInViewModel.getUserExist(getEmail(subscriberId),
            object : BaseViewModel.APICallBacks<UserExistResponse> {
                override fun onSuccess(model: UserExistResponse?) {
                    if (model?.success!!) {
                        if (model.userExists) {
                            /*call the api*/
                            authenticatePassword()
                        } else {
                            doSignUp()
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    hideProgress()
                    binding.clContentScreen.visibility = View.VISIBLE
                }

            })
    }

    //MobileNo or EmailID
    private fun checkOTPID(userId: String) {
//        showProgress()
//        signInViewModel.getUserExist(userId,
//            object : BaseViewModel.APICallBacks<UserExistResponse> {
//                override fun onSuccess(model: UserExistResponse?) {
//                    hideProgress()
//                    if (model?.success!!) {
//                        if (model.userExists) {
//                            moveToOtpLoginScreen()
//                        } else {
//                            if (Utils.isEmailValid(userId))
//                                binding.tvInvalidMessage.text =
//                                    context!!.resources.getString(R.string.user_not_registered_email)
//                            else
//                                binding.tvInvalidMessage.text =
//                                    context!!.resources.getString(R.string.user_not_registered_mobile)
//                            binding.tvInvalidMessage.visibility = View.VISIBLE
//                        }
//                    }
//                }

//                override fun onFailed(code: Int, error: String, msg: String) {
//                    hideProgress()
//                    binding.clContentScreen.visibility = View.VISIBLE
//                }
//
//            })
    }


    private fun moveToOtpLoginScreen() {
//        val bundle = Bundle()
//        bundle.putString("mobileOrEmail", binding.etEmailMobile.text.toString())
//        val fragment = SignInOtpFragment()
//        fragment.arguments = bundle
//        (activity as SignInActivity).setFragment(
//            fragment,
//            "sign_in_otp_fragment"
//        )
    }

    private fun moveSignWithOTPScreen() {
        val fragment = SignInFragment()
        (activity as SignInActivity).setFragment(
            fragment,
            "sign_in_fragment"
        )
    }

    private fun showBannerVideoView() {
        val metrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(metrics)
        val uriPath =
            "android.resource://" + BuildConfig.APPLICATION_ID + "/" + R.raw.signin_bg
        binding.vvSignIn.setVideoPath(uriPath)
        binding.vvSignIn.start()

    }

    private fun getSTBJioUser() {
        binding.clContentScreen.visibility = View.GONE
//        showProgress()
        homeViewModel.getJioSTBUser(object : BaseViewModel.APICallBacks<JioUser> {
            override fun onSuccess(model: JioUser?) {
                if (model != null) {
                    DataManager.instance.setJioSTBUser(model)
                    subscriberId = model.sessionAttributes?.user?.subscriberId ?: Utils.getCustomId(
                        Utils.getAndroidId(), 10
                    )
                    PreferenceHelper().put(SUBSCRIBER_ID, subscriberId)
                    checkSubscriptionID(subscriberId)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                subscriberId = Utils.getCustomId(Utils.getAndroidId(), 10)
                PreferenceHelper().put(SUBSCRIBER_ID, subscriberId)
                checkSubscriptionID(subscriberId)
            }
        })
    }


}