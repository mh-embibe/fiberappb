package com.embibe.embibetvapp.ui.adapter

import android.content.Context
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ItemDiscoverYourselfBinding
import com.embibe.embibetvapp.databinding.ItemPotentialPerformanceBinding
import com.embibe.embibetvapp.databinding.ItemReadinessBinding
import com.embibe.embibetvapp.databinding.ItemTrophyBinding
import com.embibe.embibetvapp.model.ModifiedResponse
import com.embibe.embibetvapp.utils.Utils

class MultiTypeAdapter(var context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var list = ArrayList<ModifiedResponse>()
    private var parent: ViewGroup? = null
    var onItemClick: ((Int, String) -> Unit)? = null
    var onDpadLeftClick: ((Int) -> Unit)? = null
    var onItemFocused: ((Int) -> Unit)? = null
    var clickCounter = 0

    val TAG = this.javaClass.name

    companion object TYPES {
        private const val TYPE_READINESS = 0
        private const val TYPE_DISCOVER = 1
        private const val TYPE_LIKE_DISLIKE = 2
        private const val TYPE_TROPHY = 3
    }

    private fun <T : ViewDataBinding> binder(layout: Int): T {
        return DataBindingUtil.inflate<T>(
            LayoutInflater.from(parent?.context)!!,
            layout, parent, false
        )
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ItemViewHolder<ModifiedResponse> {
        this.parent = parent
        return when (viewType) {
            TYPE_READINESS -> {
                val binding: ItemReadinessBinding = binder(R.layout.item_readiness)
                ReadinessViewHolder(binding)
            }
            TYPE_TROPHY -> {
                val binding: ItemTrophyBinding = binder(R.layout.item_trophy)
                TrophyViewHolder(binding)
            }
            TYPE_DISCOVER -> {
                val binding: ItemDiscoverYourselfBinding = binder(R.layout.item_discover_yourself)
                DiscoverYourselfViewHolder(binding)
            }
            TYPE_LIKE_DISLIKE -> {
                val binding: ItemPotentialPerformanceBinding = binder(
                    R.layout
                        .item_potential_performance
                )
                PotentialViewHolder(binding)
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount() = if (list.size > 0) list.size else 0

    override fun getItemViewType(position: Int): Int {
        var value: Int
        var item = list[position]
        value = when (item.type) {
            AppConstants.TYPE_TROPHY -> TYPE_TROPHY
            AppConstants.TYPE_DISCOVER -> TYPE_DISCOVER
            AppConstants.TYPE_LIKE_DISLIKE -> TYPE_LIKE_DISLIKE
            AppConstants.TYPE_READINESS -> TYPE_READINESS
            else -> TYPE_READINESS
        }
        return value
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = list[position]
        when (holder) {
            is ReadinessViewHolder -> holder.bind(item)
            is DiscoverYourselfViewHolder -> holder.bind(item)
            is PotentialViewHolder -> holder.bind(item)
            is TrophyViewHolder -> holder.bind(item)
        }
    }

    fun setData(list: ArrayList<ModifiedResponse>) {
        this.list = list
        notifyDataSetChanged()
    }

    abstract class ItemViewHolder<T>(binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        abstract fun bind(item: T)
    }

    inner class PotentialViewHolder(var binding: ItemPotentialPerformanceBinding) :
        ItemViewHolder<ModifiedResponse>(binding), View.OnFocusChangeListener {
        override fun bind(item: ModifiedResponse) {
            itemView.clearFocus()
            Glide.with(context).load(item.likeDislikeImg).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.ivPotential)
            binding.tvPotential.text = item.gradeMessage
            Utils.loadLottieAnimation(binding.lvCircularSphere, R.raw.circle_rays_two)
        }

        init {
            itemView.onFocusChangeListener = this
        }

        override fun onFocusChange(v: View?, hasFocus: Boolean) {
            Utils.showToast(context, "$hasFocus")
        }
    }

    inner class TrophyViewHolder(var binding: ItemTrophyBinding) :
        ItemViewHolder<ModifiedResponse>(binding) {
        override fun bind(item: ModifiedResponse) {
            Utils.loadLottieAnimation(binding.lvTrophy, R.raw.trophy_2)
            Utils.loadLottieAnimation(binding.lvTrophyGlow, R.raw.trophy_glow_2)
        }

        init {
            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (event.keyCode) {
                        KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                            if (clickCounter < 2) {
//                                onItemClick?.invoke(adapterPosition, AppConstants.TYPE_TROPHY)
                                clickCounter++
                                Log.e(TAG, "counter $clickCounter")
                            }
                        }
                        KeyEvent.KEYCODE_DPAD_LEFT -> {
                            onDpadLeftClick?.invoke(adapterPosition)
                        }
                    }
                }
                false
            }

            itemView.setOnClickListener {
//                if(clickCounter < 2)
//                onItemClick?.invoke(adapterPosition, AppConstants.TYPE_TROPHY)
            }
        }
    }

    inner class ReadinessViewHolder(var binding: ItemReadinessBinding) :
        ItemViewHolder<ModifiedResponse>(binding) {
        override fun bind(item: ModifiedResponse) {
            Utils.loadLottieAnimation(binding.lvCircularSphere, R.raw.circle_rays_two)
            binding.item = item
        }
    }

    inner class DiscoverYourselfViewHolder(var binding: ItemDiscoverYourselfBinding) :
        ItemViewHolder<ModifiedResponse>(binding) {
        override fun bind(item: ModifiedResponse) {
            Utils.loadLottieAnimation(binding.lvTrophyGlow, R.raw.trophy_glow_2)
        }

        init {
            itemView.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (event.keyCode) {
                        KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                            if (clickCounter < 1) {
                                onItemClick?.invoke(adapterPosition, AppConstants.TYPE_DISCOVER)
                                clickCounter++
                                Log.e(TAG, "counter $clickCounter")
                            }
                        }
                        KeyEvent.KEYCODE_DPAD_LEFT -> {
                            onDpadLeftClick?.invoke(adapterPosition)
                        }
                    }
                }
                false
            }

            itemView.setOnClickListener {
                if (clickCounter < 1)
                    onItemClick?.invoke(adapterPosition, AppConstants.TYPE_DISCOVER)
            }
        }
    }
}