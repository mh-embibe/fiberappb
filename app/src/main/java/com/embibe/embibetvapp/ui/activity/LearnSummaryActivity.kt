package com.embibe.embibetvapp.ui.activity

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.graphics.Color
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.TypedValue
import android.view.KeyEvent
import android.view.View
import android.view.View.*
import android.webkit.*
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.airbnb.lottie.LottieDrawable
import com.android.volley.RequestQueue
import com.android.volley.VolleyError
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.application.App.Companion.context
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.ApiConstants
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.constant.AppConstants.ALL_VIDEOS_FOR_THIS_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.ANNOTATION_DETAILS
import com.embibe.embibetvapp.constant.AppConstants.ANNOTATION_MODEL
import com.embibe.embibetvapp.constant.AppConstants.CONTENT_TYPE
import com.embibe.embibetvapp.constant.AppConstants.CONTINUE_LEARNING
import com.embibe.embibetvapp.constant.AppConstants.IS_RECOMMENDATION_ENABLED
import com.embibe.embibetvapp.constant.AppConstants.PRACTICE_ON_THIS_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.PRE_REQUISITE_TOPICS
import com.embibe.embibetvapp.constant.AppConstants.RESUME
import com.embibe.embibetvapp.constant.AppConstants.START
import com.embibe.embibetvapp.constant.AppConstants.TEST_ON_THIS_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.TEST_ON_THIS_TOPIC
import com.embibe.embibetvapp.constant.AppConstants.TOPICS_IN_THIS_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.TOPIC_LEARN_PATH
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_AUTHORS
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CHAPTER
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_CONCEPT_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_DESCRIPTION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_ID
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SOURCE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_SUBJECT
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TITLE
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_TOTAL_DURATION
import com.embibe.embibetvapp.constant.AppConstants.VIDEO_URL
import com.embibe.embibetvapp.databinding.ActivityLearnSummaryBinding
import com.embibe.embibetvapp.model.chapter.chapterLearningObjects.ChapterLearningObject
import com.embibe.embibetvapp.model.learnSummary.SincerietyRes
import com.embibe.embibetvapp.model.likeAndBookmark.LikeBookmarkResponse
import com.embibe.embibetvapp.model.practise.conceptSequence.ConceptSeqRes
import com.embibe.embibetvapp.model.response.CommonApiResponse
import com.embibe.embibetvapp.model.response.attemptquality.AttemptQualityResponse
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.ui.fragment.learn.LearnSummaryFragment
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener
import com.embibe.embibetvapp.ui.viewmodel.DetailsViewModel
import com.embibe.embibetvapp.unity.UnityPlayerActivity
import com.embibe.embibetvapp.utils.ConnectionManager
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.VideoUtils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo
import com.github.penfeizhou.animation.apng.APNGDrawable
import com.github.penfeizhou.animation.loader.ResourceStreamLoader
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import kotlinx.android.synthetic.main.activity_detail.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class LearnSummaryActivity : BaseFragmentActivity(), ErrorScreensBtnClickListener {

    private var isPrimaryWebViewLoaded: Boolean = false
    private var isFullWebViewLoaded: Boolean = false
    private var mKGdata: String? = null
    private var isJarEnabled: Boolean = false
    private lateinit var binding: ActivityLearnSummaryBinding
    private lateinit var detailsViewModel: DetailsViewModel
    private var mLastKeyDownTime: Long = 0
    private var isSubSectionActive = false
    private lateinit var sincerityCoobo: String
    private lateinit var listener: OnSharedPreferenceChangeListener
    private lateinit var volleyRequest: RequestQueue
    private lateinit var builder: AlertDialog.Builder
    private lateinit var dialog: AlertDialog
    private lateinit var data: Content
    private lateinit var learnSummaryFragment: LearnSummaryFragment
    private var lastWatchedData: Content? = null
    private var isBookMarked: Boolean = false
    private var isLiked: Boolean = false
    private var bookmarkRetryCount: Int = 0
    private var likeRetryCount: Int = 0
    private var pref = PreferenceHelper()
    private var isVideoResumed: Boolean = false
    private var topicsForChapter: ArrayList<Content> = arrayListOf()
    private var testsForChapter: ArrayList<Content> = arrayListOf()
    private var practicesForChapter: ArrayList<Content> = arrayListOf()
    private var videosForChapter: ArrayList<ChapterLearningObject> = arrayListOf()
    private var prerequisitesForChapter: ArrayList<Content> = arrayListOf()
    private val REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124
    private var preview_url =
        "https://visualintelligence.blob.core.windows.net/video-summary/prod/4397012/summary.webp"
    private var jar_url = "https://assets7.lottiefiles.com/packages/lf20_G9rzGD.json"
    private var thumb =
        "https://content-grail-production.s3.amazonaws.com/practice-temp-tiles/1p45wyBVwdAeFP29gv57nU28ymT793kC1.webp"
    private var thumb1 =
        "https://cg-production.s3.amazonaws.com/images/95af3bf6-4d80-4781-924c-591fbd7b7077.webp"
    private var gif_preview = "https://media.giphy.com/media/xT5LMvV7tQfpptiuzK/giphy.gif"
    private var isFisrt = true
    private var cardAttemptScoreVisibility: Boolean = false
    var wvFullViewProgressVisibility: Boolean = false
    private var jarType: String = ""
    private lateinit var attemptQualityRes: AttemptQualityResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SegmentUtils.trackLearnSummaryScreenLoadStart()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_learn_summary)
        detailsViewModel = ViewModelProviders.of(this).get(DetailsViewModel::class.java)

        volleyRequest = Volley.newRequestQueue(this)
        listenForDataPrefsUpdate()
        getDetail()
        setInitialFocusToLearn()
        setOptionsListeners()
        setFocusListeners()
        permissionsCheck()
        bgTransformation()
        initViewSincerity()
        initViewAttemptQuality("")
        setClickListener()
        setJarFocusListener()

        setKeyListener()
        setPrimaryWebView(binding.wvPrimaryProgress)
//        setFullProgressWebView(binding.wvFullViewProgress)
        getAttemptQualityData()
        getSincerietyScore()
        getConceptSequenceCoverageAsync()
    }

    private fun getSincerietyScore() {
        detailsViewModel.getSincerietyScore(
            data.learning_map.format_id,
            data.learnpath_format_name,
            data.learnpath_name,
            "learn",
            object : BaseViewModel.APICallBacks<SincerietyRes> {
                override fun onSuccess(model: SincerietyRes?) {
                    if (model != null) {
                        setSincerietyUIUpdate(model)
                    }
                    makeLog("")
                }

                override fun onFailed(code: Int, error: String, msg: String) {

                }
            })
    }

    private fun setSincerietyUIUpdate(model: SincerietyRes) {
        var data = model.data as LinkedTreeMap<String, LinkedTreeMap<String, String>>
        var keyName: String = ""
        for (key in model.data.keys) {
            keyName = key as String
        }
        binding.cardSincerityScore.tvDescription.text = data.get(keyName)?.get("description")
        binding.cardSincerityScore.tvTopic.text = data.get(keyName)?.get("text")
        gif_preview = data[keyName]?.get("gif")!!
        if (data[keyName]!!.containsKey("coobo")) {
            sincerityCoobo = data[keyName]?.get("coobo")!!
            binding.cardSincerityScore.layout.setOnClickListener {
                val i = Intent(this, UnityPlayerActivity::class.java)
                i.putExtra(AppConstants.COOBO_URL, sincerityCoobo)
                startActivity(i)
            }
        }
    }


    private fun setPrimaryWebView(webView: WebView) {
        val base = "file:///android_asset/"
        val indexPath = "KnowledgeGraph/index.html"
        val template = "file:///android_asset/$indexPath"
        webView.setBackgroundColor(Color.argb(0, 0, 0, 0))
        webView.webChromeClient = WebChromeClient()
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                isPrimaryWebViewLoaded = true
                Log.w("WEBVIEW", "Web view ready")
                setDataToKGwidget(webView)
            }

            override fun onReceivedError(
                view: WebView,
                request: WebResourceRequest,
                error: WebResourceError
            ) {
                //isLoaded = false
                Log.w("WEBVIEW", "Web view failed to load")
                val errorMessage = "$error"
                makeLog(errorMessage)
                //setProgressDialogVisibility(false)
                super.onReceivedError(view, request, error)
            }


        }

        webView.settings.allowFileAccess = true
        webView.settings.allowFileAccessFromFileURLs = true
        webView.isLongClickable = false
        webView.settings.javaScriptEnabled = true
        webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        webView.settings.domStorageEnabled = true
        webView.setBackgroundColor(Color.argb(0, 0, 0, 0))
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        webView.loadUrl(template)
    }

    private fun setDataToKGwidget(webView: WebView) {
        if (isPrimaryWebViewLoaded && mKGdata != null) {
            webView.evaluateJavascript("setDataFromApi($mKGdata)", null)
            isPrimaryWebViewLoaded = false
        }
        if (isFullWebViewLoaded && mKGdata != null) {
            webView.evaluateJavascript("setDataFromApi($mKGdata)", null)
            isFullWebViewLoaded = false
        }
    }

    private fun setFullProgressWebView(webView: WebView) {
        val base = filesDir.path.toString() + "/assets/"
        val path = "KnowledgeGraph/index.html"
        val template = "file:///android_asset/$path"
        webView.setBackgroundColor(Color.argb(0, 0, 0, 0))
        webView.webChromeClient = WebChromeClient()
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                Log.d("WEBVIEW", "Web view ready")
                isFullWebViewLoaded = true
                setDataToKGwidget(webView)
            }
        }

        webView.settings.allowFileAccess = true
        webView.settings.allowFileAccessFromFileURLs = true
        webView.isLongClickable = false
        webView.settings.javaScriptEnabled = true
        webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        webView.settings.domStorageEnabled = true
        webView.setBackgroundColor(Color.argb(0, 0, 0, 0))
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
        /*webView.addJavascriptInterface(
            CommonAndroidAPI(this),
            CommonAndroidAPI.NAME
        )*/
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        webView.loadUrl(template)
    }

    override fun onResume() {
        super.onResume()
        SegmentUtils.trackLearnSummaryScreenLoadEnd()
        if (::data.isInitialized) {
            getBookmarkStatus()
            getLikeStatus()
            SegmentUtils.trackEventScreenLoadEnd(
                isLiked,
                isBookMarked,
                content = data
            )
        }

    }

    private fun listenForDataPrefsUpdate() {

        listener = OnSharedPreferenceChangeListener { sharedPreferences, key ->
            when (key) {
                AppConstants.LAST_WATCHED_CONTENT + "_" + data.content_id -> {
                    getLastWatchedContentData()
                }
                AppConstants.ALL_VIDEOS_FOR_THIS_CHAPTER + "_" + data.id -> {
                    toggleOptionsVisibility(binding.videosForThisChapter.tag.toString())
                }
                AppConstants.TOPICS_IN_THIS_CHAPTER + "_" + data.id -> {
                    toggleOptionsVisibility(binding.topicsInThisChapter.tag.toString())
                }
                AppConstants.PRE_REQUISITE_TOPICS + "_" + data.id -> {
                    toggleOptionsVisibility(binding.preRequisiteTopics.tag.toString())
                }
                AppConstants.TEST_ON_THIS_CHAPTER + "_" + data.id -> {
                    toggleOptionsVisibility(binding.testOnThisChapter.tag.toString())
                }
                AppConstants.PRACTICE_ON_THIS_CHAPTER + "_" + data.id -> {
                    toggleOptionsVisibility(binding.practiceOnThisChapter.tag.toString())
                }
            }
        }
        pref.preferences?.registerOnSharedPreferenceChangeListener(listener)
    }

    private fun getLastWatchedContentData() {
        hideProgress()
        val lastWatchedContent = DataManager.instance.getLastWatchedContent()
        val lastWatchedContents = lastWatchedContent?.content
        if (lastWatchedContents?.size != 0) {
            lastWatchedData = lastWatchedContents?.get(0)
            //open video activity only if url is not empty
            if (lastWatchedData?.url!!.isNotEmpty()) {
                if (lastWatchedData?.watched_duration != 0)
                    showContinueDialog()
                else
                    moveToPlayer()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::listener.isInitialized) {
            pref.preferences?.unregisterOnSharedPreferenceChangeListener(listener)
        }
    }

    private fun showHideLine() {
        //if none of the options are showing, hide the line
        if (!binding.videosForThisChapter.isVisible
            && !binding.topicsInThisChapter.isVisible
            && !binding.preRequisiteTopics.isVisible
            && !binding.testOnThisChapter.isVisible
            && !binding.practiceOnThisChapter.isVisible
        ) {
            binding.view.visibility = GONE
        } else {
            binding.view.visibility = VISIBLE
        }
    }

    private fun getDetail() {
        data = getContent(intent)
        binding.tvKnowledgeGraphTitle.text =
            getString(R.string.here_is_where_you_stand_on)
        val captureDescText =
            getString(R.string.capture_based_on_your_learning_practicing_and_testing_on)
        binding.cardSincerityScore.tvDescription.text = captureDescText
        binding.cardAttemptScore.tvDescription.text = captureDescText
        SegmentUtils.trackEventScreenLoadStart(isLiked, isBookMarked, content = data)
        binding.content = data
        setDetails()
        if (data.bg_thumb.isEmpty())
            Utils.setBackgroundImage(
                Utils.getBackgroundUrl(data),
                binding.imgDescBackground,
                binding.imgDescBackgroundBlur
            )
        else
            Utils.setBackgroundImage(
                data.bg_thumb,
                binding.imgDescBackground,
                binding.imgDescBackgroundBlur
            )
    }

    private fun toggleOptionsVisibility(tag: String) {
        when (tag) {
            getString(R.string.all_videos_for_this_chapter) -> {
                videosForChapter = DataManager.instance.getVideosForChapter()
                if (videosForChapter.isNotEmpty()) {
                    setVisibility(binding.videosForThisChapter, VISIBLE)
                } else {
                    setVisibility(binding.videosForThisChapter, GONE)
                }
            }
            getString(R.string.topics_in_this_chapter) -> {
                topicsForChapter = DataManager.instance.getTopicsForChapter()
                if (topicsForChapter.isNotEmpty()) {
                    setVisibility(binding.topicsInThisChapter, VISIBLE)
                } else {
                    setVisibility(binding.topicsInThisChapter, GONE)
                }
            }
            getString(R.string.pre_requisite_topics) -> {
                prerequisitesForChapter = DataManager.instance.getPrerequisitesForChapter()
                if (prerequisitesForChapter.isNotEmpty()) {
                    setVisibility(binding.preRequisiteTopics, VISIBLE)
                } else {
                    setVisibility(binding.preRequisiteTopics, GONE)
                }
            }
            getString(R.string.test_on_this_chapter) -> {
                testsForChapter = DataManager.instance.getTestsForChapter()
                if (testsForChapter.isNotEmpty()) {
                    setVisibility(binding.testOnThisChapter, VISIBLE)
                } else {
                    setVisibility(binding.testOnThisChapter, GONE)
                }
            }
            getString(R.string.practice_on_this_chapter) -> {
                practicesForChapter = DataManager.instance.getPracticesForChapter()
                if (practicesForChapter.isNotEmpty()) {
                    setVisibility(binding.practiceOnThisChapter, VISIBLE)
                } else {
                    setVisibility(binding.practiceOnThisChapter, GONE)
                }
            }
        }
        showHideLine()
    }

    private fun setVisibility(view: Button, visibility: Int) {
        view.visibility = visibility
    }

    private fun setDetails() {

        if (data.title != "") {
            Log.d(TAG, data.title)
            //Total allowed character count is 25
//            var updateStr = Utils.getTruncatedString(data.title, 25)
            binding.title.text = data.title

        } else binding.title.visibility = GONE

        if (data.subject != "") {
            binding.tvSubjectName.text = data.subject
        } else binding.tvSubjectName.visibility = GONE

        if (data.type != "learn_chapter")
            binding.textTime.text = Utils.convertIntoMins(data.length)
        else
            binding.textTime.text = Utils.convertIntoConcepts(data.concept_count)

        binding.embiumsCount.text = getString(R.string.earn) + " ${data.currency}"

        if (data.description != "") {
            binding.textDescription.text = data.description
        } else binding.textDescription.visibility = GONE

        if (data.watched_duration != 0) {
            btnLearn.text = getString(R.string.continue_learning)
        }
    }

    private fun getBookmarkStatus() {
        detailsViewModel.getBookmarkStatus(data.id,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                val data = dataList.filter {
                                    it.contentId.equals(data.id)
                                }
                                updateBookMarkUI(data[0].status)
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg ")

                }
            })
    }

    private fun getLikeStatus() {
        detailsViewModel.getLikeStatus(data.id,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!! && model.data != null) {
                        val dataList = model.data
                        if (dataList?.isNotEmpty()!!) {
                            try {
                                if (dataList.isNotEmpty()) {
                                    val data = dataList.filter {
                                        it.contentId.equals(data.id)
                                    }
                                    updateLikUI(data[0].status)
                                }
                            } catch (e: Exception) {
                                Log.e("Error :", e.printStackTrace().toString())
                            }
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("$code $error $msg")
                }
            })
    }

    private fun setInitialFocusToLearn() {
        btnLearn.requestFocus()
        btnLearn.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_videos_for_chapter_black, 0, 0, 0
        )
    }

    private fun setOptionsListeners() {
        binding.btnLearn.setOnClickListener {
            SegmentUtils.trackLearnSummaryScreenMainButtonClick("Learn")
            showProgress()
            Utils.getLastWatchedContent(this, AppConstants.EMBIBE_SYLLABUS, data.content_id)
        }

        binding.videosForThisChapter.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
//                        learnSummaryFragment.requestFocus()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        isSubSectionActive = true
                        SegmentUtils.trackLearnSummaryScreenAvailableOptionClick(
                            AppConstants.ALL_VIDEOS_FOR_THIS_CHAPTER,
                            1,
                            ""
                        )
                        val bundle = Bundle()
                        bundle.putString("type", getString(R.string.all_videos_for_this_chapter))
                        bundle.putParcelableArrayList(
                            AppConstants.ALL_VIDEOS_FOR_THIS_CHAPTER,
                            videosForChapter
                        )
                        bundle.putParcelable(AppConstants.CONTENT, data)
                        showDetails(bundle)
                        setStrokOnSelectedMenuOption(ALL_VIDEOS_FOR_THIS_CHAPTER)
                    }
                }
            }

            false
        }

        binding.topicsInThisChapter.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
//                        learnSummaryFragment.requestFocus()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        isSubSectionActive = true
                        SegmentUtils.trackLearnSummaryScreenAvailableOptionClick(
                            AppConstants.TOPICS_IN_THIS_CHAPTER,
                            2,
                            ""
                        )
                        val bundle = Bundle()
                        bundle.putString("type", getString(R.string.topics_in_this_chapter))
                        bundle.putParcelableArrayList(
                            AppConstants.TOPICS_IN_THIS_CHAPTER,
                            topicsForChapter
                        )
                        bundle.putParcelable(AppConstants.CONTENT, data)
                        showDetails(bundle)
                        setStrokOnSelectedMenuOption(TOPICS_IN_THIS_CHAPTER)
                    }
                }
            }

            false
        }

        binding.preRequisiteTopics.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
//                        learnSummaryFragment.requestFocus()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        isSubSectionActive = true
                        SegmentUtils.trackLearnSummaryScreenAvailableOptionClick(
                            AppConstants.PRE_REQUISITE_TOPICS,
                            3,
                            ""
                        )
                        val bundle = Bundle()
                        bundle.putString("type", getString(R.string.pre_requisite_topics))
                        bundle.putParcelableArrayList(
                            AppConstants.PRE_REQUISITE_TOPICS,
                            prerequisitesForChapter
                        )
                        bundle.putParcelable(AppConstants.CONTENT, data)
                        showDetails(bundle)
                        setStrokOnSelectedMenuOption(PRE_REQUISITE_TOPICS)
                    }
                }
            }

            false
        }

        binding.testOnThisChapter.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
//                        learnSummaryFragment.requestFocus()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        isSubSectionActive = true
                        SegmentUtils.trackLearnSummaryScreenAvailableOptionClick(
                            AppConstants.TEST_ON_THIS_CHAPTER,
                            4,
                            ""
                        )
                        val bundle = Bundle()
                        bundle.putString("type", getString(R.string.test_on_this_chapter))
                        bundle.putParcelableArrayList(
                            AppConstants.TEST_ON_THIS_CHAPTER,
                            testsForChapter
                        )
                        bundle.putParcelable(AppConstants.CONTENT, data)
                        showDetails(bundle)
                        setStrokOnSelectedMenuOption(TEST_ON_THIS_CHAPTER)
                    }
                }
            }

            false
        }

        binding.practiceOnThisChapter.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
//                        learnSummaryFragment.requestFocus()
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        isSubSectionActive = true
                        SegmentUtils.trackLearnSummaryScreenAvailableOptionClick(
                            AppConstants.PRACTICE_ON_THIS_CHAPTER,
                            5,
                            ""
                        )
                        val bundle = Bundle()
                        bundle.putString("type", getString(R.string.practice_on_this_chapter))
                        bundle.putParcelableArrayList(
                            AppConstants.PRACTICE_ON_THIS_CHAPTER,
                            practicesForChapter
                        )
                        bundle.putParcelable(AppConstants.CONTENT, data)
                        showDetails(bundle)
                        setStrokOnSelectedMenuOption(PRACTICE_ON_THIS_CHAPTER)
                    }
                }
            }

            false
        }

        binding.btnBookmark.setOnClickListener {
            Utils.isLikeBookmarkUpdatedForQuickLinks = true
            isBookMarked = !isBookMarked
            updateBookMarkUI(isBookMarked)
            SegmentUtils.trackEventMoreInfoBookmarkClick(isBookMarked, content = data)
            SegmentUtils.trackLearnSummaryScreenBookmarkButtonClick(isBookMarked)
            timerBookmark.cancel()
            timerBookmark = Timer()
            timerBookmark.schedule(object : TimerTask() {
                override fun run() {
                    updateBookmark()
                }
            }, interval)
        }

        binding.btnLike.setOnClickListener {
            isLiked = !isLiked
            updateLikUI(isLiked)
            SegmentUtils.trackEventMoreInfoLikeClick(isLiked, content = data)
            SegmentUtils.trackLearnSummaryScreenLikeButtonClick(isLiked)
            /*like api call*/
            timerLike.cancel()
            timerLike = Timer()
            timerLike.schedule(object : TimerTask() {
                override fun run() {
                    updateLike()
                }
            }, interval)
        }
    }

    private fun showContinueDialog() {
        dialog.show()
        //continueVideoDialog?.show(supportFragmentManager, "continue_video")
    }

    private fun hideDialog() {
        if (::dialog.isInitialized && dialog.isShowing)
            dialog.dismiss()
    }

    override fun onBackPressed() {
        if (isJarEnabled || wvFullViewProgressVisibility) {
            if (isJarEnabled) {
                restoreJarFocus()
                setAttemptQualityLayoutVisibility(true)
                binding.cardAttemptQuality.layout.visibility = VISIBLE
                binding.cardAttemptScore.layout.visibility = GONE
                this.jarType = ""
                isJarEnabled = false
            }
            if (wvFullViewProgressVisibility) {
                onwvFullViewProgressBackPress()
                setAttemptQualityLayoutVisibility(true)
                wvFullViewProgressVisibility = false
            }
        } else {
            hideDialog()
            super.onBackPressed()
        }
    }

    private fun moveToPlayer() {
        if (!ConnectionManager.instance.hasNetworkAvailable()) {
            Utils.showError(this@LearnSummaryActivity, ApiConstants.API_CODE_NO_NETWORK,
                object : BaseViewModel.ErrorCallBacks {
                    override fun onRetry(msg: String) {
                        moveToPlayer()
                    }

                    override fun onDismiss() {

                    }
                })
            return
        }
        if (data.type.toLowerCase(Locale.getDefault()) == "Coobo".toLowerCase(Locale.getDefault())) {
            val i = Intent(context, UnityPlayerActivity::class.java)
            i.putExtra(VIDEO_CONCEPT_ID, lastWatchedData?.learning_map?.conceptId)
            i.putExtra(AppConstants.DURATION_LENGTH, lastWatchedData?.length)
            i.putExtra(AppConstants.SLIDE_COUNT, lastWatchedData?.watched_duration)
            i.putExtra(AppConstants.COOBO_URL, lastWatchedData?.url)
            i.putExtra(AppConstants.DETAILS_ID, lastWatchedData?.id)
            Utils.insertBundle(data, i)
            lastWatchedData?.learning_map.let()
            {
                i.putExtra(AppConstants.LEARNMAP_CODE, lastWatchedData?.learning_map?.lpcode)
            }
            i.putExtra(IS_RECOMMENDATION_ENABLED, true)
            startActivity(i)

        } else {
            val callback = object : BaseViewModel.APICallBackVolley {
                override fun <T> onSuccessCallBack(response: T) {
                    hideProgress()
                    response as JSONObject
                    startVideoPlayer(
                        ((response["files"] as JSONArray)[0] as JSONObject)["link"] as String,
                        AppConstants.PLAYER_TYPE_EXOPLAYER
                    )
                    /*hide ProgressBar here */
                }

                override fun onErrorCallBack(e: VolleyError) {
                    try {
                        makeLog("Error:  $e")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    /*hide ProgressBar here */
                    hideProgress()
                }
            }

            val type = Utils.getVideoTypeUsingUrl(lastWatchedData?.url!!)
            if (type == AppConstants.PLAYER_TYPE_EXOPLAYER) {
                showProgress()
                val videoId = VideoUtils.getVimeoVideoId(lastWatchedData?.url!!).toString()
                Utils.getVideoURL(this, videoId, lastWatchedData?.owner_info?.key, callback)
            } else {
                if (Utils.isYouTubeAppAvailable(this)) {
                    Utils.startYoutubeApp(this, lastWatchedData?.url ?: "")
                } else {
                    startVideoPlayer(lastWatchedData?.url!!, AppConstants.PLAYER_TYPE_YOUTUBE)
                }
            }
        }

    }

    private fun showDetails(bundle: Bundle) {
        learnSummaryFragment = LearnSummaryFragment()
        learnSummaryFragment.arguments = bundle
        setContainerVisible()
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainerView, learnSummaryFragment)
            .commit()
    }

    private fun setStrokOnSelectedMenuOption(optionType: String) {
        when (optionType) {
            ALL_VIDEOS_FOR_THIS_CHAPTER -> {
                binding.videosForThisChapter.setBackgroundResource(R.drawable.summary_screen_menu_selected)
                binding.topicsInThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.preRequisiteTopics.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.testOnThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.practiceOnThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
            }
            TOPICS_IN_THIS_CHAPTER -> {
                binding.topicsInThisChapter.setBackgroundResource(R.drawable.summary_screen_menu_selected)
                binding.videosForThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.preRequisiteTopics.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.testOnThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.practiceOnThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
            }
            PRE_REQUISITE_TOPICS -> {
                binding.preRequisiteTopics.setBackgroundResource(R.drawable.summary_screen_menu_selected)
                binding.topicsInThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.videosForThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.testOnThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.practiceOnThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
            }
            TEST_ON_THIS_TOPIC -> {
                binding.testOnThisChapter.setBackgroundResource(R.drawable.summary_screen_menu_selected)
                binding.topicsInThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.preRequisiteTopics.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.videosForThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.practiceOnThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
            }
            PRACTICE_ON_THIS_CHAPTER -> {
                binding.practiceOnThisChapter.setBackgroundResource(R.drawable.summary_screen_menu_selected)
                binding.topicsInThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.preRequisiteTopics.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.videosForThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
                binding.testOnThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
            }
        }
    }

    private fun setAllOptionUnselected() {
        binding.practiceOnThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
        binding.topicsInThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
        binding.preRequisiteTopics.setBackgroundResource(R.drawable.summary_menu_selector)
        binding.videosForThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
        binding.testOnThisChapter.setBackgroundResource(R.drawable.summary_menu_selector)
    }

    private fun setContainerVisible() {
        binding.fragmentContainerView.visibility = VISIBLE
        binding.landingCL.visibility = GONE
    }

    private fun switchViewsVisibility(landingCLVisibility: Int, fragmentCVVisibility: Int) {
        if (binding.landingCL.visibility != landingCLVisibility)
            binding.landingCL.visibility = landingCLVisibility
        if (binding.fragmentContainerView.visibility != fragmentCVVisibility)
            binding.fragmentContainerView.visibility = fragmentCVVisibility
    }

    private fun bgTransformation() {
        Utils.setBlurBackgroundImage(
            binding.imgDescBackground
        )
    }

    private fun setKnowledgeGraphContainer() {
        binding.fragmentContainerView.visibility = GONE
        binding.landingCL.visibility = VISIBLE
    }

    private fun setFocusListeners() {
        binding.btnLearn.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                isSubSectionActive = false
                SegmentUtils.trackLearnSummaryScreenMainButtonFocus("Learn")
                setKnowledgeGraphContainer()
                setAllOptionUnselected()
//                switchViewsVisibility(VISIBLE, INVISIBLE)
                btnLearn.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_videos_for_chapter_black, 0, 0, 0
                )
            } else {
                btnLearn.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_videos_for_chapter, 0, 0, 0
                )
            }
        }

        binding.btnBookmark.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                isSubSectionActive = false
                SegmentUtils.trackLearnSummaryScreenBookmarkButtonFocus()
                setKnowledgeGraphContainer()
                SegmentUtils.trackEventMoreInfoBookmarkFocus(isBookMarked, content = data)
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_black, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                    )
                }
            } else {
                if (isBookMarked) {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_filled_white, 0, 0, 0
                    )
                } else {
                    binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                    )
                }
            }
        }

        binding.btnLike.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                isSubSectionActive = false
                SegmentUtils.trackLearnSummaryScreenLikekButtonFocus()
                setKnowledgeGraphContainer()
                SegmentUtils.trackEventMoreInfoLikeFocus(isLiked, content = data)
                if (isLiked) {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_filled_black, 0, 0, 0
                    )
                } else {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_stroke_black, 0, 0, 0
                    )
                }
            } else {
                if (isLiked) {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_filled_white, 0, 0, 0
                    )
                } else {
                    binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_like_stroke_white, 0, 0, 0
                    )
                }
            }
        }

        binding.videosForThisChapter.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackLearnSummaryScreenAvailableOptionFocus(
                AppConstants.ALL_VIDEOS_FOR_THIS_CHAPTER,
                1,
                ""
            )
        }
        binding.topicsInThisChapter.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackLearnSummaryScreenAvailableOptionFocus(
                AppConstants.TOPICS_IN_THIS_CHAPTER,
                2,
                ""
            )
        }
        binding.preRequisiteTopics.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackLearnSummaryScreenAvailableOptionFocus(
                AppConstants.PRE_REQUISITE_TOPICS,
                3,
                ""
            )
        }
        binding.testOnThisChapter.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackLearnSummaryScreenAvailableOptionFocus(
                AppConstants.TEST_ON_THIS_CHAPTER,
                4,
                ""
            )
        }
        binding.practiceOnThisChapter.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackLearnSummaryScreenAvailableOptionFocus(
                AppConstants.PRACTICE_ON_THIS_CHAPTER,
                5,
                ""
            )
        }

        binding.btnCheckProgress.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) SegmentUtils.trackLearnSummaryScreenKnowledgeGraphFocus(AppConstants.KnowledgeGraph)
        }
    }

    private fun updateBookmark() {
        detailsViewModel.bookmark(data.id, data.type, isBookMarked,
            object : BaseViewModel.APICallBacks<CommonApiResponse> {
                override fun onSuccess(model: CommonApiResponse?) {
                    if (model != null && model.success) {
                        Utils.isLikeBookmarkUpdatedForQuickLinks = true
                        updateBookMarkUI(isBookMarked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (bookmarkRetryCount < retryAttempt) {
                        bookmarkRetryCount++
                        updateBookmark()
                        return
                    } else {
                        /*reset retry count*/
                        bookmarkRetryCount = 0
                        /*revert back to previous state of ui */
                        updateBookMarkUI(!isBookMarked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_BOOKMARK)
                    }
                }
            })
    }

    private fun updateLike() {
        detailsViewModel.like(data.id, data.type, isLiked,
            object : BaseViewModel.APICallBacks<LikeBookmarkResponse> {
                override fun onSuccess(model: LikeBookmarkResponse?) {
                    if (model?.success!!) {
                        Utils.isLikeBookmarkUpdatedForQuickLinks = true
                        updateLikUI(isLiked)
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (likeRetryCount < retryAttempt) {
                        likeRetryCount++
                        updateLike()
                        return
                    } else {
                        /*reset retry count*/
                        likeRetryCount = 0
                        /*revert back to previous state of ui */
                        updateLikUI(!isLiked)
                        showErrorToast(error, AppConstants.ALERT_MSG_FOR_LIKE)
                    }
                }
            })
    }

    private fun updateBookMarkUI(bookmarked: Boolean) {
        isBookMarked = bookmarked
        data.isBookmarked = bookmarked

        if (binding.btnBookmark.hasFocus()) {
            if (isBookMarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_black, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        } else {
            if (isBookMarked) {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_filled_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmarked)
            } else {
                binding.btnBookmark.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bookmark_stroke_white, 0, 0, 0
                )
                binding.btnBookmark.text = getString(R.string.bookmark)
            }
        }
    }

    private fun updateLikUI(liked: Boolean) {
        isLiked = liked
        data.isLiked = liked

        if (binding.btnLike.hasFocus()) {
            if (isLiked) {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_filled_black, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.liked)
            } else {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_stroke_black, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.like)
            }
        } else {
            if (isLiked) {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_filled_white, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.liked)
            } else {
                binding.btnLike.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_like_stroke_white, 0, 0, 0
                )
                binding.btnLike.text = getString(R.string.like)
            }
        }
    }

    override fun screenUpdate(type: String) {
        when (type) {
            RESUME -> {

            }
            START -> {

            }
        }
    }

    private fun startVideoPlayer(url: String, source: String) {
        val intent = Intent(context, VideoPlayerActivity::class.java)
        var resumedDuration: Long? = null
        if (isVideoResumed) {
            if (DataManager.instance.getLastSeek(lastWatchedData?.id!!) != null)
                resumedDuration = DataManager.instance.getLastSeek(lastWatchedData?.id!!)
            if (resumedDuration != null)
                intent.putExtra(CONTINUE_LEARNING, resumedDuration.toString())
            else
                intent.putExtra(CONTINUE_LEARNING, lastWatchedData?.watched_duration.toString())
        } else
            intent.putExtra(CONTINUE_LEARNING, "0")
        if (lastWatchedData?.annotation_attributes != null && lastWatchedData?.annotation_attributes!!.size > 0) {
            intent.putExtra(ANNOTATION_DETAILS, true)
            intent.putParcelableArrayListExtra(
                ANNOTATION_MODEL,
                lastWatchedData?.annotation_attributes
            )
        }
        intent.putExtra(VIDEO_URL, url)
        intent.putExtra(VIDEO_ID, lastWatchedData?.id)
        intent.putExtra(VIDEO_CONCEPT_ID, lastWatchedData?.learning_map?.conceptId)
        intent.putExtra(CONTENT_TYPE, lastWatchedData?.type)
        intent.putExtra(VIDEO_TOTAL_DURATION, lastWatchedData?.length.toString())
        intent.putExtra(VIDEO_TITLE, lastWatchedData?.title)
        intent.putExtra(VIDEO_DESCRIPTION, lastWatchedData?.description)
        intent.putExtra(VIDEO_SUBJECT, lastWatchedData?.subject)
        intent.putExtra(VIDEO_CHAPTER, lastWatchedData?.learning_map?.chapter)
        intent.putExtra(VIDEO_AUTHORS, "")
        intent.putExtra(VIDEO_SOURCE, source)
        intent.putExtra(TOPIC_LEARN_PATH, lastWatchedData?.learning_map?.topicLearnPathName)
        intent.putExtra(IS_RECOMMENDATION_ENABLED, true)
        intent.putExtra(AppConstants.LEARN_PATH_NAME, data.learnpath_name)
        intent.putExtra(AppConstants.LEARN_PATH_FORMAT_NAME, data.learnpath_format_name)
        intent.putExtra(AppConstants.FORMAT_ID, data.learning_map.format_id)
        startActivity(intent)
    }

    private fun initViewSincerity() {
        val requestOptions = RequestOptions().transform(RoundedCorners(5))
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        Glide.with(context)
            .applyDefaultRequestOptions(requestOptions)
            .load(thumb1)
            .placeholder(R.drawable.video_placeholder)
            .into(binding.cardSincerityScore.ivImg)

        binding.cardSincerityScore.layout.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        currentFocus?.clearFocus()
                        binding.btnCheckProgress.requestFocus()
                        return@setOnKeyListener true
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        SegmentUtils.trackLearnSummaryScreenSincerityScoreClick("IDK")
                    }

                }
            }

            false
        }

        binding.cardSincerityScore.layout.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackLearnSummaryScreenSincerityScoreFocus("IDK")
                if (isFisrt) {
                    showSincerityScoreFlipAnimation()
                } else {
                    binding.cardSincerityScore.cardView.visibility = VISIBLE
                    binding.cardSincerityScore.ivGifView.visibility = VISIBLE
                    val controller = Fresco.newDraweeControllerBuilder()
                    controller.autoPlayAnimations = true
                    controller.setUri(preview_url)
                    controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                        override fun onFinalImageSet(
                            id: String?,
                            imageInfo: ImageInfo?,
                            animatable: Animatable?
                        ) {
                            val anim = animatable as AnimatedDrawable2
                            anim.setAnimationListener(object : AnimationListener {
                                override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                                override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                                    binding.cardSincerityScore.ivImg.visibility = INVISIBLE
                                }

                                override fun onAnimationFrame(
                                    drawable: AnimatedDrawable2?,
                                    frameNumber: Int
                                ) {

                                }

                                override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                                    binding.cardSincerityScore.ivImg.visibility = VISIBLE
                                    binding.cardSincerityScore.ivGifView.visibility = INVISIBLE
                                    binding.cardSincerityScore.cardView.visibility = View.INVISIBLE
                                    binding.cardSincerityScore.ivPlay.visibility = VISIBLE
                                }

                                override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                            })
                        }
                    }
                    binding.cardSincerityScore.ivGifView.controller = controller.build()
                }
            } else {
                binding.cardSincerityScore.ivImg.visibility = VISIBLE
                binding.cardSincerityScore.ivGifView.visibility = INVISIBLE
                binding.cardSincerityScore.cardView.visibility = INVISIBLE
                binding.cardSincerityScore.ivPlay.visibility = VISIBLE
            }
        }
    }

    private fun initViewAttemptQuality(jarType: String) {
        val requestOptions = RequestOptions().transform(RoundedCorners(10))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        //binding.cardAttemptScore.tvTopic.text = "0% " + jarType
        when (jarType) {
            AppConstants.CORRECT -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_too_fast_correct)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.PERFECT -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_perfect_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.OVERTIME_CORRECT -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_overtime_correct)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.WASTED -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_wasted_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.WRONG -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_incorrect_answer)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.OVERTIME_WRONG -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_overtime_incorrect)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.WASTED_IN_FOUR -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_wasted_attempt)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
            AppConstants.CONCEPTUAL_THINKING -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_conceptual_thinking)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }

            AppConstants.RECALL -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_recall)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }

            AppConstants.ANALYTICAL_THINKING -> {
                Glide.with(App.context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(R.drawable.jar_analytical_thinking)
                    .placeholder(R.drawable.video_placeholder)
                    .into(binding.cardAttemptScore.ivImg)
            }
        }

        binding.cardAttemptScore.layout.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.cardAttemptScore.lvJar.visibility = View.VISIBLE
                binding.cardAttemptScore.ivPlay.visibility = View.INVISIBLE
                binding.cardAttemptScore.ivImg.visibility = View.INVISIBLE
                when (jarType) {
                    AppConstants.CORRECT -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_correct)
                        SegmentUtils.trackLearnSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.PERFECT -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_perfect_attempts)
                        SegmentUtils.trackLearnSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.OVERTIME_CORRECT -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_overtime_correct)
                        SegmentUtils.trackLearnSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.WASTED -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_wasted_attempt)
                        SegmentUtils.trackLearnSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.WRONG -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_wrong)
                        SegmentUtils.trackLearnSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.WASTED_IN_FOUR -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_wasted_attempt)
                        SegmentUtils.trackLearnSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.OVERTIME_WRONG -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.new_jar_overtime_incorrect)
                        SegmentUtils.trackLearnSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.RECALL -> {
                        binding.cardAttemptScore.lvJar.setAnimation(R.raw.jar_recall)
                        SegmentUtils.trackLearnSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.ANALYTICAL_THINKING -> {
                        binding.cardAttemptScore.lvJar.setAnimation(
                            R.raw.jar_analytical_tinking
                        )
                        SegmentUtils.trackLearnSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                    AppConstants.CONCEPTUAL_THINKING -> {
                        binding.cardAttemptScore.lvJar.setAnimation(
                            R.raw.jar_conceptual_thinking
                        )
                        SegmentUtils.trackLearnSummaryScreenAttemptBucketDescriptionFocus(jarType)
                        setCardAttemptScoreClickListener(jarType)
                    }
                }
                binding.cardAttemptScore.lvJar.repeatCount = LottieDrawable.INFINITE
                binding.cardAttemptScore.lvJar.playAnimation()
            } else {
                binding.cardAttemptScore.lvJar.visibility = View.INVISIBLE
                binding.cardAttemptScore.ivPlay.visibility = View.VISIBLE
                binding.cardAttemptScore.ivImg.visibility = View.VISIBLE
            }
        }
    }

    private fun setClickListener() {
        binding.cardAttemptQuality.layout.setOnClickListener {
            binding.cardAttemptScore.layout.visibility = VISIBLE
            cardAttemptScoreVisibility = true
            binding.cardAttemptScore.layout.postDelayed({
                binding.cardAttemptScore.layout.requestFocus()
                binding.cardAttemptQuality.layout.visibility = GONE
            }, 10)
        }

        /*binding.cardAttemptScore.layout.setOnKeyListener { v, keyCode, event ->
            if (event.action==KeyEvent.ACTION_DOWN){
                when(keyCode){
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        binding.cardThree.layout.visibility = View.INVISIBLE
                        binding.cardAttemptScore.layout.visibility = View.VISIBLE
                        binding.cardAttemptScore.layout.requestFocus()
                    }
                }
            }
            false
        }*/

    }

    private fun setCardAttemptScoreClickListener(jarType: String) {
        binding.cardAttemptScore.layout.setOnClickListener {
            SegmentUtils.trackLearnSummaryScreenAttemptBucketDescriptionClick(jarType)
        }
    }

    private fun setJarFocusListener() {
        binding.cardAttemptQuality.jarCorrectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackLearnSummaryScreenAttemptQualityFocus(AppConstants.CORRECT, 1)
                setJarClickListener(AppConstants.CORRECT)
            }
        }
        binding.cardAttemptQuality.jarPerfectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackLearnSummaryScreenAttemptQualityFocus(AppConstants.PERFECT, 2)
                setJarClickListener(AppConstants.PERFECT)
            }
        }
        binding.cardAttemptQuality.jarOvertimeCorrectAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackLearnSummaryScreenAttemptQualityFocus(
                    AppConstants.OVERTIME_CORRECT,
                    3
                )
                setJarClickListener(AppConstants.OVERTIME_CORRECT)
            }
        }
        binding.cardAttemptQuality.jarWastedAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackLearnSummaryScreenAttemptQualityFocus(AppConstants.WASTED, 4)
                setJarClickListener(AppConstants.WASTED)
            }
        }
        binding.cardAttemptQuality.jarWrongAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackLearnSummaryScreenAttemptQualityFocus(AppConstants.WRONG, 5)
                setJarClickListener(AppConstants.WRONG)
            }
        }
        binding.cardAttemptQuality.jarOvertimeWrongAttempt.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackLearnSummaryScreenAttemptQualityFocus(
                    AppConstants.OVERTIME_WRONG,
                    6
                )
                setJarClickListener(AppConstants.OVERTIME_WRONG)
            }
        }
        binding.cardAttemptQuality.jarRecall.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackLearnSummaryScreenAttemptQualityFocus(AppConstants.RECALL, 7)
                setJarClickListener(AppConstants.RECALL)
            }
        }
        binding.cardAttemptQuality.jarAnalyticalThinking.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackLearnSummaryScreenAttemptQualityFocus(
                    AppConstants.ANALYTICAL_THINKING,
                    8
                )
                setJarClickListener(AppConstants.ANALYTICAL_THINKING)
            }
        }
        binding.cardAttemptQuality.jarConceptualThinking.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackLearnSummaryScreenAttemptQualityFocus(
                    AppConstants.CONCEPTUAL_THINKING,
                    9
                )
                setJarClickListener(AppConstants.CONCEPTUAL_THINKING)
            }
        }
        binding.cardAttemptQuality.jarWastedAttemptNew.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                SegmentUtils.trackLearnSummaryScreenAttemptQualityFocus(
                    AppConstants.WASTED_IN_FOUR,
                    10
                )
                setJarClickListener(AppConstants.WASTED_IN_FOUR)
            }
        }
    }

    private fun setJarClickListener(jarType: String) {
        when (jarType) {
            AppConstants.CORRECT -> binding.cardAttemptQuality.jarCorrectAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackLearnSummaryScreenAttemptQualityClick(jarType, 1)
            }
            AppConstants.PERFECT -> binding.cardAttemptQuality.jarPerfectAttempt.setOnClickListener {
                animateJar(jarType)
                SegmentUtils.trackLearnSummaryScreenAttemptQualityClick(jarType, 2)
                // setAttemptQualityView(jarType)
            }
            AppConstants.OVERTIME_CORRECT -> binding.cardAttemptQuality.jarOvertimeCorrectAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackLearnSummaryScreenAttemptQualityClick(jarType, 3)
            }
            AppConstants.WASTED -> binding.cardAttemptQuality.jarWastedAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackLearnSummaryScreenAttemptQualityClick(jarType, 4)
            }
            AppConstants.WRONG -> binding.cardAttemptQuality.jarWrongAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackLearnSummaryScreenAttemptQualityClick(jarType, 5)
            }
            AppConstants.OVERTIME_WRONG -> binding.cardAttemptQuality.jarOvertimeWrongAttempt.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackLearnSummaryScreenAttemptQualityClick(jarType, 6)
            }
            AppConstants.RECALL -> binding.cardAttemptQuality.jarRecall.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackLearnSummaryScreenAttemptQualityClick(jarType, 7)
            }
            AppConstants.ANALYTICAL_THINKING -> binding.cardAttemptQuality.jarAnalyticalThinking.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackLearnSummaryScreenAttemptQualityClick(jarType, 8)
            }
            AppConstants.CONCEPTUAL_THINKING -> binding.cardAttemptQuality.jarConceptualThinking.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackLearnSummaryScreenAttemptQualityClick(jarType, 9)
            }
            AppConstants.WASTED_IN_FOUR -> binding.cardAttemptQuality.jarWastedAttemptNew.setOnClickListener {
                // setAttemptQualityView(jarType)
                animateJar(jarType)
                SegmentUtils.trackLearnSummaryScreenAttemptQualityClick(jarType, 10)
            }
        }

    }

    private fun animateJar(jarType: String) {
        updateAttemptQualityData(jarType)
        when (jarType) {
            AppConstants.CORRECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                10f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(1)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.CONCEPTUAL_THINKING -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -30f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(7)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.PERFECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -40f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(2)

                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500
                    }

                    override fun onAnimationCancel(animation: Animator) {}
                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.RECALL -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -80f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(8)

                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500
                    }

                    override fun onAnimationCancel(animation: Animator) {}
                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.OVERTIME_CORRECT -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -120f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(3)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.ANALYTICAL_THINKING -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -160f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(9)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500

                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.WASTED_IN_FOUR -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -220f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(10)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }

            AppConstants.WASTED -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -180f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(4)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.WRONG -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -260f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(5)
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500


                    }

                    override fun onAnimationCancel(animation: Animator) {}

                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
            AppConstants.OVERTIME_WRONG -> ObjectAnimator.ofFloat(
                binding.imgAttemptJarIcon,
                "translationX",
                -320f
            ).apply {
                duration = 500
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        // 3
                        viewAnimationImage(6)

                    }

                    override fun onAnimationEnd(animation: Animator) {
                        // 4
                        setAttemptQualityView(jarType)
                        binding.imgAttemptJarIcon.visibility = View.GONE
                        binding.imgAttemptJarIcon.animate().translationX(0f)
                            .translationY(0f).duration = 500
                    }

                    override fun onAnimationCancel(animation: Animator) {}
                    override fun onAnimationRepeat(animation: Animator) {}
                })
                start()
            }
        }
    }

    private fun setAttemptQualityView(jarType: String) {
        this.jarType = jarType
        initViewAttemptQuality(jarType)
        binding.cardAttemptScore.layout.visibility = VISIBLE
        Handler().postDelayed({
            binding.cardAttemptScore.layout.requestFocus()
            binding.cardAttemptQuality.layout.visibility = GONE
        }, 10)

    }

    private fun viewAnimationImage(position: Int) {
        setAttemptQualityLayoutVisibility(false)
        isJarEnabled = true
        binding.imgAttemptJarIcon.alpha = 1.0F
        binding.imgAttemptJarIcon.visibility = VISIBLE
        when (position) {
            1 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_too_fast_correct)
            }
            2 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_perfect_attempt)

            }
            3 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_correct)

            }
            4 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_wasted_attempt)

            }
            5 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_incorrect_answer)

            }
            6 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_overtime_incorrect)

            }
            8 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_recall)
            }
            7 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_conceptual_thinking)
            }
            9 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_analytical_thinking)
            }
            10 -> {
                binding.imgAttemptJarIcon.setImageResource(R.drawable.jar_wasted_attempt)
            }
        }
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        if (position < 5)
            lp.setMargins((position - 1) * 60, 20, 0, 0)
        if (position == 5)
            lp.setMargins((position - 1) * 70, 20, 0, 0)
        if (position == 6)
            lp.setMargins((position - 1) * 80, 20, 0, 0)

        binding.imgAttemptJarIcon.layoutParams = lp
    }


    private fun setKeyListener() {
        binding.wvPrimaryProgress.settings.layoutAlgorithm =
            WebSettings.LayoutAlgorithm.SINGLE_COLUMN

        binding.btnCheckProgress.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        return@setOnKeyListener true
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (!wvFullViewProgressVisibility) {
                            binding.btnLike.postDelayed({
                                binding.btnLike.requestFocus()
                            }, 10)
                        }
                        return@setOnKeyListener true
                    }
                    KeyEvent.KEYCODE_BACK -> {
                        TriggerBackCallBack()
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        binding.wvPrimaryProgress.evaluateJavascript(
                            "javascript:moveForward()",
                            null
                        )
                        return@setOnKeyListener true
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        SegmentUtils.trackLearnSummaryScreenKnowledgeGraphClick(AppConstants.KnowledgeGraph)
                        if (!wvFullViewProgressVisibility) {
                            expandWebView()
                        } else {
                            TriggerBackCallBack()
                            onBackPressed()
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!wvFullViewProgressVisibility) {
                            binding.cardSincerityScore.layout.postDelayed({
                                binding.cardSincerityScore.layout.requestFocus()
                            }, 10)
                        } else {
                            binding.wvPrimaryProgress.evaluateJavascript(
                                "javascript:moveBackward()",
                                null
                            )
                        }
                        return@setOnKeyListener true
                    }
                }
            }

            false
        }
    }

    private fun TriggerBackCallBack() {
        binding.wvPrimaryProgress.evaluateJavascript(
            "javascript:handleOnBack()",
            null
        )
    }

    private fun expandWebView() {
        val lp = binding.primaryWebViewCL.layoutParams as ConstraintLayout.LayoutParams
        lp.height = 450.toDp(this)
        binding.primaryWebViewCL.apply {
            layoutParams = lp
        }
        binding.btnCheckProgress.apply {
            text = "Back"
        }
        setVisiblewvFullViewProgress()
        currentFocus?.clearFocus()
        binding.wvPrimaryProgress.isEnabled = true
        binding.btnCheckProgress.requestFocus()
        Handler().postDelayed({
            binding.wvPrimaryProgress.evaluateJavascript("javascript:handleOnViewProgress()", null)
        }, 1000)
        SegmentUtils.trackEventKnowledgeGraphExpandedBackBtnFocus()
    }

    private fun setVisiblewvFullViewProgress() {
        wvFullViewProgressVisibility = true
        binding.cardAttemptQuality.layout.visibility = GONE
        binding.cardSincerityScore.layout.visibility = GONE
        binding.cardAttemptScore.layout.visibility = GONE
        cardAttemptScoreVisibility = false
    }


    private fun onwvFullViewProgressBackPress() {
//        binding.wvPrimaryProgress.visibility = VISIBLE
//        binding.btnCheckProgress.visibility = VISIBLE
        binding.cardSincerityScore.layout.visibility = VISIBLE
        if (cardAttemptScoreVisibility) {
            binding.cardAttemptScore.layout.visibility = VISIBLE
        } else
            binding.cardAttemptQuality.layout.visibility = VISIBLE
//        currentFocus?.clearFocus()
//        binding.btnCheckProgress.requestFocus()
        val lp = ConstraintLayout.LayoutParams(820, 400)
        lp.startToStart = binding.learnSummaryCL.id
        lp.topToBottom = binding.tvKnowledgeGraphTitle.id
        lp.topMargin = 6
        binding.primaryWebViewCL.apply {
            layoutParams = lp
        }
        binding.btnCheckProgress.apply {
            text = "Check Progress + Achievement"
        }
        binding.btnCheckProgress.postDelayed({
            binding.btnCheckProgress.requestFocus()
        }, 10)

        SegmentUtils.trackEventKnowledgeGraphExpandedBackBtnClick()
    }


    /*get Concept sequence for knowledge graph*/
    private fun getConceptSequenceCoverageAsync() {
        detailsViewModel.loadConceptsSequenceCoverage(data,
            object : BaseViewModel.APICallBacks<ConceptSeqRes> {
                override fun onSuccess(model: ConceptSeqRes?) {
                    if (model != null) {
                        mKGdata = Gson().toJson(model)
                        makeLog("ConceptSequenceCoverage res : $mKGdata")
                        setDataToKGwidget(binding.wvPrimaryProgress)
                    } else {

                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    makeLog("Error in getConceptSequenceCoverage : $code $msg")
                }

            })
    }

    private fun showSincerityScoreFlipAnimation() {
        isFisrt = false
        var resourceLoader = ResourceStreamLoader(context, R.drawable.flip_anim_apng)
        var apngDrawable = APNGDrawable(resourceLoader)
        binding.cardSincerityScore.ivImg.setImageDrawable(apngDrawable)

        val requestOptions = RequestOptions().transform(RoundedCorners(5))
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        Handler().postDelayed({
            Glide.with(App.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(gif_preview)
                .placeholder(R.drawable.video_placeholder)
                .into(binding.cardSincerityScore.ivImg)

        }, 2000)
    }

    private fun setAttemptQualityLayoutVisibility(visibility: Boolean) {
        if (visibility) {
            binding.cardAttemptQuality.title.visibility = VISIBLE
            binding.cardAttemptQuality.jarCorrectAttempt.visibility = VISIBLE
            binding.cardAttemptQuality.jarOvertimeCorrectAttempt.visibility = VISIBLE
            binding.cardAttemptQuality.jarPerfectAttempt.visibility = VISIBLE
            binding.cardAttemptQuality.jarWrongAttempt.visibility = VISIBLE
            binding.cardAttemptQuality.jarWastedAttempt.visibility = VISIBLE
            binding.cardAttemptQuality.jarOvertimeWrongAttempt.visibility = VISIBLE
        } else {
            binding.cardAttemptQuality.title.visibility = GONE
            binding.cardAttemptQuality.jarCorrectAttempt.visibility = GONE
            binding.cardAttemptQuality.jarOvertimeCorrectAttempt.visibility = GONE
            binding.cardAttemptQuality.jarPerfectAttempt.visibility = GONE
            binding.cardAttemptQuality.jarWrongAttempt.visibility = GONE
            binding.cardAttemptQuality.jarWastedAttempt.visibility = GONE
            binding.cardAttemptQuality.jarOvertimeWrongAttempt.visibility = GONE
        }
    }

    private fun restoreJarFocus() {
        when (jarType) {
            AppConstants.CORRECT -> binding.cardAttemptQuality.jarCorrectAttempt.requestFocus()
            AppConstants.PERFECT -> binding.cardAttemptQuality.jarPerfectAttempt.requestFocus()
            AppConstants.OVERTIME_CORRECT -> binding.cardAttemptQuality.jarOvertimeCorrectAttempt.requestFocus()
            AppConstants.WASTED -> binding.cardAttemptQuality.jarWastedAttempt.requestFocus()
            AppConstants.WRONG -> binding.cardAttemptQuality.jarWrongAttempt.requestFocus()
            AppConstants.OVERTIME_WRONG -> binding.cardAttemptQuality.jarOvertimeWrongAttempt.requestFocus()
            AppConstants.RECALL -> binding.cardAttemptQuality.jarRecall.requestFocus()
            AppConstants.ANALYTICAL_THINKING -> binding.cardAttemptQuality.jarAnalyticalThinking.requestFocus()
            AppConstants.CONCEPTUAL_THINKING -> binding.cardAttemptQuality.jarConceptualThinking.requestFocus()
            AppConstants.WASTED_IN_FOUR -> binding.cardAttemptQuality.jarWastedAttemptNew.requestFocus()
        }
    }

    // Extension method to convert pixels to dp
    fun Int.toDp(context: Context): Int = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), context.resources.displayMetrics
    ).toInt()


    private fun getAttemptQualityData() {
        detailsViewModel.getUserAttemptQualityData(
            data.learning_map.format_id,
            data.learnpath_format_name,
            data.learnpath_name,
            object : BaseViewModel.APICallBacks<AttemptQualityResponse> {
                override fun onSuccess(model: AttemptQualityResponse?) {
                    if (model != null) {
                        attemptQualityRes = model
                        setJarVisibility()
                    }
                    makeLog("")
                }

                override fun onFailed(code: Int, error: String, msg: String) {

                }
            })
    }

    private fun setJarVisibility() {
        if (::attemptQualityRes.isInitialized) {
            if (attemptQualityRes.data.conceptual_thinking != null) {
                binding.cardAttemptQuality.layoutFourJars.visibility = View.VISIBLE
                binding.cardAttemptQuality.layoutSixJars.visibility = View.GONE
            } else {
                binding.cardAttemptQuality.layoutFourJars.visibility = View.GONE
                binding.cardAttemptQuality.layoutSixJars.visibility = View.VISIBLE
            }
        }
    }

    private fun getAttemptPercentage(value: String): Int {
        Log.i("attempt data", value)
        if (value.isNotEmpty() || value.isNotBlank())
            return value.toDouble().toInt()
        return 0
    }

    private fun updateAttemptQualityData(jarType: String) {
        var jarName = jarType
        when (jarName) {
            AppConstants.RECALL -> {
                jarName = "Recall"
            }
            AppConstants.ANALYTICAL_THINKING -> {
                jarName = "Analytical Thinking"
            }
            AppConstants.CONCEPTUAL_THINKING -> {
                jarName = "Conceptual Thinking"
            }
            AppConstants.WRONG -> {
                jarName = "Wrong"
            }
            AppConstants.WASTED_IN_FOUR -> {
                jarName = "Wasted"
            }
        }
        if (::attemptQualityRes.isInitialized) {
            val percentage: Int = when (jarType) {
                AppConstants.CORRECT -> getAttemptPercentage(attemptQualityRes.data.overtime_correct!!.value) + getAttemptPercentage(
                    attemptQualityRes.data.perfect_attempt!!.value
                )
                AppConstants.PERFECT -> getAttemptPercentage(attemptQualityRes.data.perfect_attempt!!.value)
                AppConstants.OVERTIME_CORRECT -> getAttemptPercentage(attemptQualityRes.data.overtime_correct!!.value)
                AppConstants.WASTED -> getAttemptPercentage(attemptQualityRes.data.wasted_attempt!!.value)
                AppConstants.WRONG -> getAttemptPercentage(attemptQualityRes.data.wasted_attempt!!.value) + getAttemptPercentage(
                    attemptQualityRes.data.wasted_attempt!!.value
                )
                AppConstants.OVERTIME_WRONG -> getAttemptPercentage(attemptQualityRes.data.overtime_incorrect!!.value)
                AppConstants.RECALL -> getAttemptPercentage(attemptQualityRes.data.recall!!.value)
                AppConstants.ANALYTICAL_THINKING -> getAttemptPercentage(attemptQualityRes.data.analytical_thinking!!.value)
                AppConstants.CONCEPTUAL_THINKING -> getAttemptPercentage(attemptQualityRes.data.conceptual_thinking!!.value)
                AppConstants.WASTED_IN_FOUR -> getAttemptPercentage(attemptQualityRes.data.wasted_attempt!!.value)
                else -> 0
            }
            binding.cardAttemptScore.tvTopic.text = "$percentage% ${jarName}"
        } else
            binding.cardAttemptScore.tvTopic.text = "0% " + jarName
    }

    companion object {
        private const val TAG = "LearnSummaryActivity"
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {

        if (event.keyCode == KeyEvent.KEYCODE_SEARCH) {
            if (!isSubSectionActive) {
                startActivity(Intent(this, SearchActivity::class.java))
            } else {
                learnSummaryFragment.startRecognition()
            }
            return true
        }
        val current = System.currentTimeMillis()
        var res = false
        if (current - mLastKeyDownTime < 300) {
            res = true
        } else {
            res = super.onKeyDown(keyCode, event)
            mLastKeyDownTime = current
        }
        return res
    }
}