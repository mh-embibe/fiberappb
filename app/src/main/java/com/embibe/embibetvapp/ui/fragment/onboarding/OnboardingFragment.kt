package com.embibe.embibetvapp.ui.fragment.onboarding

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.leanback.app.OnboardingSupportFragment
import androidx.lifecycle.ViewModelProviders
import com.airbnb.lottie.LottieAnimationView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.model.jiostb.JioUser
import com.embibe.embibetvapp.ui.activity.SignInActivity
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.preference.PreferenceHelper
import java.util.*

class OnboardingFragment : OnboardingSupportFragment() {

    private lateinit var subscriberId: String
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var signInViewModel: SignInViewModel
    private lateinit var mContentView: LottieAnimationView

    //UsedLayout
    private var layoutId = R.layout.lb_onboarding_fragment
    private var mContentAnimator: Animator? = null
    private var pos = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("usedLayout", "lb_onboarding_fragment$layoutId")
        Log.d("usedId", "" + R.dimen.lb_onboarding_content_width)
        Log.d("usedId", "" + R.dimen.lb_onboarding_header_margin_top)
        Log.d("usedId", "" + R.dimen.lb_onboarding_header_height)
    }

    override fun onFinishFragment() {
        super.onFinishFragment()
        gotoSignInActivity()
    }

    override fun getPageCount(): Int {
        return pageTitles.size
    }

    override fun getPageTitle(pageIndex: Int): String {
        return getString(pageTitles[pageIndex])
    }

    override fun getPageDescription(pageIndex: Int): String {
        return getString(pageDescriptions[pageIndex])
    }

    override fun onCreateBackgroundView(inflater: LayoutInflater, container: ViewGroup): View? {
        val bgView = View(activity)
        bgView.background = resources.getDrawable(R.drawable.shape_rec_gradient_blue)
        return bgView
    }

    override fun onCreateForegroundView(inflater: LayoutInflater, container: ViewGroup): View? {
        return null
    }

    override fun setDotBackgroundColor(color: Int) {
        super.setDotBackgroundColor(resources.getColor(R.color.dot_background))
    }

    override fun setArrowColor(color: Int) {
        super.setArrowColor(resources.getColor(R.color.alpha_black))
    }

    override fun moveToNextPage() {
        super.moveToNextPage()
        SegmentUtils.trackOnboardingRightButton(pos)
    }

    override fun moveToPreviousPage() {
        super.moveToPreviousPage()
        SegmentUtils.trackOnboardingLeftButton(pos)
    }

    override fun onPageChanged(newPage: Int, previousPage: Int) {
        pos = newPage
        if (mContentAnimator != null) {
            mContentAnimator!!.end()
        }
        val animators = ArrayList<Animator>()
        val fadeOut = createFadeOutAnimator(mContentView)
        fadeOut.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                mContentView.setAnimation(pageAnimation[newPage])
                mContentView.playAnimation()
            }
        })
        animators.add(fadeOut)
        animators.add(createFadeInAnimator(mContentView))
        val set = AnimatorSet()
        set.playSequentially(animators)
        set.start()
        mContentAnimator = set
    }

    override fun onCreateEnterAnimation(): Animator? {
        mContentView.loop(true)
        mContentAnimator = createFadeInAnimator(mContentView)
        return mContentAnimator
    }

    override fun onCreateContentView(inflater: LayoutInflater, container: ViewGroup): View? {

        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        SegmentUtils.trackOnboardingLoadStart(pos)

        getSTBJioUser()
        mContentView = LottieAnimationView(activity)
        mContentView.setPadding(0, 32, 0, 32)
        mContentView.setAnimation(pageAnimation[0])
        mContentView.playAnimation()
        SegmentUtils.trackOnboardingLoadEnd()
        return mContentView
    }

    private fun getSTBJioUser() {

        homeViewModel.getJioSTBUser(object : BaseViewModel.APICallBacks<JioUser> {
            override fun onSuccess(model: JioUser?) {
                if (model != null) {
                    DataManager.instance.setJioSTBUser(model)
                    subscriberId = model.sessionAttributes?.user?.subscriberId ?: Utils.getCustomId(
                        Utils.getAndroidId(), 10
                    )
                    PreferenceHelper().put(AppConstants.SUBSCRIBER_ID, subscriberId)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                subscriberId = Utils.getCustomId(Utils.getAndroidId(), 10)
                PreferenceHelper().put(AppConstants.SUBSCRIBER_ID, subscriberId)
            }
        })
    }

    private fun gotoSignInActivity() {
        startActivity(Intent(activity, SignInActivity::class.java))
        requireActivity().finish()
    }

    private fun createFadeInAnimator(view: View?): Animator {
        return ObjectAnimator.ofFloat(view, View.ALPHA, 0.0f, 1.0f).setDuration(ANIMATION_DURATION)
    }

    private fun createFadeOutAnimator(view: View?): Animator {
        return ObjectAnimator.ofFloat(view, View.ALPHA, 1.0f, 0.0f).setDuration(ANIMATION_DURATION)
    }

    companion object {
        private val pageTitles = intArrayOf(
            R.string.onboarding_title_welcome, R.string.onboarding_title_design,
            R.string.onboarding_title_simple, R.string.onboarding_title_project
        )
        private val pageDescriptions = intArrayOf(
            R.string.onboarding_description_welcome, R.string.onboarding_description_design,
            R.string.onboarding_description_simple, R.string.onboarding_description_project
        )

        private val pageAnimation = intArrayOf(
            R.raw.slide_01, R.raw.slide_02, R.raw.slide_03, R.raw.slide_04
        )
        private const val ANIMATION_DURATION: Long = 500
    }
}