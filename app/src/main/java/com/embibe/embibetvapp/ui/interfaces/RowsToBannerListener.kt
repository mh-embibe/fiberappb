package com.embibe.embibetvapp.ui.interfaces

interface RowsToBannerListener {

    /**
     * this function is to notify focus shift from rows to banner
     */
    fun rowsToBanner()

    /**
     * @param notifyMessage refers to any message that needs to be passed between rows and banner
     * on the basis of message we can execute things in host to banner
     */
    fun notifyBanner(notifyMessage: String)

    /**
     * @param notifyMessage refers to any message that needs to be passed between rows and host
     * on the basis of message we can execute things in host
     */
    fun notifyHost(notifyMessage: String)
}