package com.embibe.embibetvapp.ui.adapter

import android.content.Intent
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemTestMenuBinding
import com.embibe.embibetvapp.model.test.TestFeedbackMenuModel
import com.embibe.embibetvapp.ui.activity.AchievePotentialActivity
import com.embibe.embibetvapp.utils.SegmentUtils
import kotlinx.android.synthetic.main.item_test_menu.view.*

class TestFeedbackMenuAdapter : RecyclerView.Adapter<TestFeedbackMenuAdapter.TestsViewHolder>() {

    var list = ArrayList<TestFeedbackMenuModel>()
    var onItemClick: ((TestFeedbackMenuModel) -> Unit)? = null
    private lateinit var subMenuAdapter: TestFeedbackSubMenuAdapter
    var selectedPosition = -1
    var symbolAdd = true
    var onItemLeftClick: ((Boolean) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestsViewHolder {
        val binding: ItemTestMenuBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_test_menu, parent, false
        )
        return TestsViewHolder(binding)
    }

    override fun getItemCount() = if (list.size > 0) list.size else 0

    override fun onBindViewHolder(holder: TestsViewHolder, position: Int) {
        holder.bind(list[position])

        if (position == selectedPosition) {
            if (list[position].subItems.size > 0) {
                holder.binding.imgAdd.setImageDrawable(holder.itemView.resources.getDrawable(R.drawable.minus_bold))
                holder.binding.recyclerSubItem.visibility = View.VISIBLE
                symbolAdd = false
            }
        }
    }

    fun setData(list: ArrayList<TestFeedbackMenuModel>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class TestsViewHolder(var binding: ItemTestMenuBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: TestFeedbackMenuModel) {
            binding.title.text = item.title
            binding.ivBg.setImageResource(item.background)

            var type: String = ""
            when (adapterPosition) {
                1 -> type = "revision_lists"
                2 -> type = "test_taking_strategy"
            }
//            if (item.title == "Get Your Revision Lists") {
//            } else if (item.title == "Improve Your \nTest Taking Strategy") {
//            }

            binding.recyclerSubItem.visibility = View.GONE

            if (list[adapterPosition].subItems.size > 0) {
                binding.imgAdd.visibility = View.VISIBLE
                binding.imgAdd.setImageDrawable(itemView.resources.getDrawable(R.drawable.plus_bold))
                setRecycler(binding, itemView, item, type)
                symbolAdd = true
            } else {
                binding.imgAdd.visibility = View.GONE
                symbolAdd = false

            }
        }

        init {

            binding.imgLayout.setOnFocusChangeListener { v, hasFocus ->
                when(hasFocus) {
                    true -> {
                        SegmentUtils.trackTestFeedbackScreenAchieveHighScoreWidgetFocus(v.title.toString()
                            ,adapterPosition+1,symbolAdd)
                    }
                }
            }

            binding.imgLayout.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {

                            SegmentUtils.trackTestFeedbackScreenCurrentAndPotentialWidgetClick(v.title.toString()
                                ,adapterPosition+1,symbolAdd)
                            if (selectedPosition != adapterPosition) {
                                val previousPosition = selectedPosition
                                notifyItemChanged(previousPosition)

                                selectedPosition = adapterPosition
                                notifyItemChanged(adapterPosition)
                            } else {
                                // make recycler visible and previous item back to original position
                                toggleRecyclerVisibility(binding, itemView, adapterPosition)
                            }

                            if (adapterPosition == 0) {
                                /*move to AchievePotentialActivity*/
                                itemView.context.startActivity(
                                    Intent(itemView.context, AchievePotentialActivity::class.java)
                                )
                            }
                        }

                        KeyEvent.KEYCODE_DPAD_LEFT -> {
                            onItemLeftClick?.invoke(true)
                        }
                    }
                }

                false
            }
        }
    }

    private fun toggleRecyclerVisibility(
        binding: ItemTestMenuBinding,
        itemView: View,
        adapterPosition: Int
    ) {
        if (list[adapterPosition].subItems.size > 0) {
            if (binding.recyclerSubItem.visibility == View.GONE) {

                binding.recyclerSubItem.visibility = View.VISIBLE
                binding.imgAdd.setImageDrawable(itemView.resources.getDrawable(R.drawable.minus_bold))
                symbolAdd = false
            } else if (binding.recyclerSubItem.visibility == View.VISIBLE) {

                binding.recyclerSubItem.visibility = View.GONE
                binding.imgAdd.setImageDrawable(itemView.resources.getDrawable(R.drawable.plus_bold))
                symbolAdd = true

            }
        }
    }

    private fun setRecycler(
        binding: ItemTestMenuBinding,
        itemView: View,
        item: TestFeedbackMenuModel,
        type: String
    ) {
        subMenuAdapter = TestFeedbackSubMenuAdapter(type)
        val layoutManager =
            LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerSubItem.adapter = subMenuAdapter
        binding.recyclerSubItem.layoutManager = layoutManager
        subMenuAdapter.setData(item.subItems)
    }
}