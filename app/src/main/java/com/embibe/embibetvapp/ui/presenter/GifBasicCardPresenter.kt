/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.embibe.embibetvapp.ui.presenter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.utils.GifCardView
import com.embibe.embibetvapp.utils.Utils

/**
 * This Presenter will display a card consisting of an image on the left side of the card followed
 * by text on the right side. The image and text have equal width. The text will work like a info
 * box, thus it will be hidden if the parent row is inactive. This behavior is unique to this card
 * and requires a special focus handler.
 */
class GifBasicCardPresenter(context: Context, var type: String) :
    AbstractCardPresenter<GifCardView>(context) {
    lateinit var cardView: GifCardView
    lateinit var itemCurrent: Content

    @SuppressLint("InflateParams")
    override fun onCreateView(): GifCardView {
        val cardView = GifCardView(context, R.style.SideInfoCardStyle)
        val viewAlpha = cardView.findViewById<View>(R.id.view_focus)
        cardView.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_focused_alpha_value
                )
            } else {
                viewAlpha.alpha = ResourcesCompat.getFloat(
                    context.resources,
                    R.dimen.all_cards_unfocused_alpha_value
                )
            }
        }
        cardView.isFocusable = true
        cardView.isFocusableInTouchMode = true
        //cardView.addView(LayoutInflater.from(context).inflate(R.layout.card_video, null))
        return cardView
    }

    override fun onBindViewHolder(item: Any, cardView: GifCardView) {
        val card = item as Content
        itemCurrent = item
        with(cardView) {
            Glide.with(context).load(itemCurrent.owner_info.copy_logo).into(ivEmbibeLogo)
            if (card.subject.isNotEmpty() && !type.equals(AppConstants.LEARN_CHAPTER)) {
                tvSubjectName.visibility = View.VISIBLE
                tvSubjectName.text = card.subject
                (tvSubjectName.background as GradientDrawable).setColor(
                    context.resources.getColor(
                        Utils.getColorCode(
                            card.subject
                        )
                    )
                )
            } else {
                tvSubjectName.visibility = View.GONE
            }
            if (card.watched_duration > 0) {
                progress_bar.visibility = View.VISIBLE
                progress_bar.max = card.length
                progress_bar.progress = card.watched_duration
            }

            if (card.thumb != "") {
                val requestOptions = RequestOptions().transform(RoundedCorners(8))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                Glide.with(context).applyDefaultRequestOptions(requestOptions)
                    .load(card.thumb)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .placeholder(R.drawable.video_placeholder)
                    .into(iv_thumbnail)
            } else {
                Glide.with(context)
                    .load(R.drawable.video_placeholder)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(iv_thumbnail)
            }
            when (type) {
                AppConstants.COOBO -> {
                    Glide.with(context)
                        .load(R.drawable.ic_coobo_category)
                        .into(ivCategory)
                }
                AppConstants.LEARN_CHAPTER -> {
                    Glide.with(context)
                        .load(R.drawable.ic_multi_video)
                        .into(ivCategory)
                }
                else -> {
                    Glide.with(context)
                        .load(card.category_thumb)
                        .into(ivCategory)
                }
            }
        }
    }


}