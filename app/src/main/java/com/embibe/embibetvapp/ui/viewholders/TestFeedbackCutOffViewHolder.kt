package com.embibe.embibetvapp.ui.viewholders

import com.embibe.embibetvapp.databinding.ItemCutOffScoreBinding

class TestFeedbackCutOffViewHolder(val binding: ItemCutOffScoreBinding) :
    BaseViewHolder<String>(binding) {
    override fun bind(item: String) {
        binding.tvLabel.text = item
    }
}