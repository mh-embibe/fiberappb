package com.embibe.embibetvapp.ui.viewholders

import android.animation.ValueAnimator
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.databinding.ItemTopSkillBarBinding
import java.math.RoundingMode

class DiagnosticDummySkillBarViewHolder(
    val binding: ItemTopSkillBarBinding,
    var listItems: MutableList<String>
) : BaseViewHolder<String>(binding) {

    override fun bind(item: String) {
        val accuracy = getRoundedAccuracy(item.toDouble())

        binding.tvMarksUp.visibility = View.VISIBLE
        binding.ivBarUp.visibility = View.VISIBLE
//        binding.textSkills.text = item.skill

        binding.ivBarUp.setImageResource(getDrawableByAccuracy(accuracy))

        if (accuracy < 30.0) {
            binding.tvMarksUp2.visibility = View.VISIBLE
            binding.tvMarksUp.visibility = View.GONE
            binding.tvMarksUp2.text = getAccuracy(accuracy)
        } else {
            binding.tvMarksUp2.visibility = View.GONE
            binding.tvMarksUp.visibility = View.VISIBLE
            binding.tvMarksUp.text = getAccuracy(accuracy)
        }

        if (accuracy.toInt() != 100) {
            animateBar(binding.ivBarUp, accuracy.toInt())
        }
    }

    private fun getRoundedAccuracy(accuracy: Double): Double {
        return accuracy.toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
    }

    private fun getDrawableByAccuracy(accuracy: Double): Int {

        return when {
            accuracy >= 0 && accuracy < 10 -> R.drawable.test_top_skill_feedback_bar
            accuracy >= 10 && accuracy < 20 -> R.drawable.test_top_skill_feedback_20
            accuracy >= 20 && accuracy < 30 -> R.drawable.test_top_skill_feedback_30
            accuracy >= 30 && accuracy < 40 -> R.drawable.test_top_skill_feedback_40
            accuracy >= 40 && accuracy < 50 -> R.drawable.test_top_skill_feedback_50
            accuracy >= 50 && accuracy < 60 -> R.drawable.test_top_skill_feedback_60
            accuracy >= 60 && accuracy < 70 -> R.drawable.test_top_skill_feedback_70
            accuracy >= 70 && accuracy < 80 -> R.drawable.test_top_skill_feedback_80
            accuracy >= 80 && accuracy < 90 -> R.drawable.test_top_skill_feedback_bar_90
            accuracy >= 90 && accuracy < 100 -> R.drawable.test_top_skill_feedback_100
            else -> 0
        }
    }

    private fun getAccuracy(accuracy: Double): String {
        return if (accuracy != 0.0) "$accuracy%"
        else 0.toString() + "%"
    }

    private fun animateBar(bar: ImageView, progressValue: Int) {
        val animator = ValueAnimator.ofInt(0, progressValue)
        animator.addUpdateListener { valueAnimator ->
            val value = valueAnimator.animatedValue
            val params = bar.layoutParams as ConstraintLayout.LayoutParams
            if (value as Int > 0) {
                params.height = value
                bar.visibility = View.VISIBLE
            } else
                bar.visibility = View.GONE
            params.height = value
            bar.layoutParams = params
        }
        animator.duration = 1500
        animator.start()
    }
}

