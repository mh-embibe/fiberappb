package com.embibe.embibetvapp.ui.viewmodel

import android.annotation.SuppressLint
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.model.NextVideoRecommendationsRes
import com.embibe.embibetvapp.model.PAJ.AchieveSection
import com.embibe.embibetvapp.model.UserData.getBoard
import com.embibe.embibetvapp.model.UserData.getEaxmSlugName
import com.embibe.embibetvapp.model.UserData.getExamName
import com.embibe.embibetvapp.model.UserData.getGoal
import com.embibe.embibetvapp.model.UserData.getGoalSlugName
import com.embibe.embibetvapp.model.UserData.getGrade
import com.embibe.embibetvapp.model.achieve.home.AchieveHome
import com.embibe.embibetvapp.model.chapter.chapterLearningObjects.ChapterLearningObject
import com.embibe.embibetvapp.model.diagnostic.TestDiagnosticRes
import com.embibe.embibetvapp.model.home.*
import com.embibe.embibetvapp.model.jiostb.JioUser
import com.embibe.embibetvapp.model.relatedConcepts.ConceptsResponse
import com.embibe.embibetvapp.model.test.TestDetailRequest
import com.embibe.embibetvapp.network.repo.DiagnosticTestRepo
import com.embibe.embibetvapp.network.repo.HomeRepository
import com.embibe.embibetvapp.network.retrofit.RetrofitClient
import com.embibe.embibetvapp.newmodel.Content
import com.embibe.embibetvapp.newmodel.Results
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.fragment.learn.LearnSummaryFragment
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.Utils.getChildId
import com.embibe.embibetvapp.utils.exceptions.CustomException
import com.embibejio.coreapp.preference.PreferenceHelper
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function3
import io.reactivex.functions.Function5
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

open class HomeViewModel : BaseViewModel() {

    private val diagnosticTestApiPreprod =
        DiagnosticTestRepo(RetrofitClient.diagnosticTestApiPreprod!!)
    private val repoHome = HomeRepository(RetrofitClient.homeApi!!)


    private var bookDetails = ArrayList<ResultsEntity>()

    fun getPercentageConnectionsApi(callback: APICallBacks<TestDiagnosticRes>) {
        safeApi(diagnosticTestApiPreprod.getDiagnosticTestList(), callback)
    }

    fun getJioSTBUser(callback: APICallBacks<JioUser>) {
        val map = HashMap<String, String>()
//        if (BuildConfig.DEBUG) {
//            map[AppConstants.JIO_STB_HEADER_KEY_X_MAC] = AppConstants.JIO_STB_HEADER_VALUE_X_MAC
//            map[AppConstants.JIO_STB_HEADER_KEY_IP] = AppConstants.JIO_STB_HEADER_VALUE_IP
//        }

        map[AppConstants.JIO_STB_HEADER_KEY_APP_NAME] = AppConstants.JIO_STB_HEADER_VALUE_APP_NAME
        map[AppConstants.JIO_STB_HEADER_KEY_X_API_KEY] = AppConstants.JIO_STB_HEADER_VALUE_X_API_KEY
        safeApi(repoHome.fetchJioSTBUser(map), callback)
    }

    fun homeApi(callback: APICallBacks<List<ResultsEntity>>) {
        val req = HomeRequest()
        req.child_id = getChildId()
        req.grade = getGrade()
        req.board = getBoard()
        req.goal = getGoal()
        req.exam = getEaxmSlugName().replace("exam--", "", true)
        req.exam_name = getExamName()

        safeApi(repoHome.fetchHome(req), callback)
    }

    fun homePracticeApi(callback: APICallBacks<List<ResultsEntity>>) {
        val req = HomeRequest()
        req.child_id = getChildId()
        req.grade = getGrade()
        req.board = getBoard()
        req.goal = getGoal()
        req.exam = getEaxmSlugName().replace("exam--", "", true)
        req.exam_name = getExamName()
        safeApi(repoHome.fetchHomePractice(req), callback)
    }

    fun homeSubjectFilter(
        subject: String,
        isPractice: Boolean,
        callback: APICallBacks<List<ResultsEntity>>
    ) {
        val req = HomeRequest()
        req.child_id = getChildId()
        req.grade = getGrade()
        req.board = getBoard()
        req.goal = getGoal()
        req.exam_name = getExamName()
        req.exam = getEaxmSlugName().replace("exam--", "", true)
        req.onlyPractise = isPractice
        val call = repoHome.homeSubjectFilter(req, subject)

        safeApi(call, callback)
    }

    fun homeQuickLinksApi(callback: APICallBacks<List<ResultsEntity>>) {
        val req = HomeRequest()
        req.child_id = getChildId()
        req.grade = getGrade()
        req.board = getBoard()
        req.goal = getGoal()
        req.exam = getEaxmSlugName().replace("exam--", "", true)

        safeApi(repoHome.homeQuickLinks(req), callback)
    }

    fun homeTest(callback: APICallBacks<List<ResultsEntity>>) {
        val req = HomeRequest()
        req.child_id = getChildId()
        req.grade = getGrade()
        req.board = getBoard()
        req.goal = getGoalSlugName()
        req.exam = getEaxmSlugName().replace("exam--", "", true)

        safeApi(repoHome.homeTest(req), callback)
    }

    fun homeTestSubjectFilter(
        subject: String,
        callback: APICallBacks<List<ResultsEntity>>
    ) {
        val req = HomeRequest()
        req.child_id = getChildId()
        req.grade = getGrade()
        req.board = getBoard()
        req.goal = getGoalSlugName()
        req.subject = subject
        req.exam = getEaxmSlugName().replace("exam--", "", true)

        val call = repoHome.homeTestSubjectFilter(req)

        safeApi(call, callback)
    }

    fun homeSectionApi(
        sectionType: String,
        offset: Int,
        size: Int,
        callback: APICallBacks<List<ResultsEntity>>
    ) {
        val req = HomeRequest()
        req.child_id = getChildId()
        req.grade = getGrade()
        req.board = getBoard()
        req.goal = getGoal()
        req.contentSectionType = sectionType
        req.offset = offset
        req.size = size
        req.exam_name = getExamName()
        req.exam = getEaxmSlugName().replace("exam--", "", true)

        safeApi(repoHome.fetchHomeSection(req), callback)
    }

    fun homePracticeSectionApi(
        sectionType: String,
        offset: Int,
        size: Int,
        callback: APICallBacks<List<ResultsEntity>>
    ) {
        val req = HomeRequest()
        req.child_id = getChildId()
        req.grade = getGrade()
        req.board = getBoard()
        req.goal = getGoal()
        req.contentSectionType = sectionType
        req.offset = offset
        req.size = size
        req.exam = getEaxmSlugName().replace("exam--", "", true)
        req.exam_name = getExamName()
        safeApi(repoHome.fetchHomePracticeSection(req), callback)
    }

    fun homeSubjectFilterSection(
        subject: String,
        isPractice: Boolean, sectionType: String, offset: Int, size: Int,
        callback: APICallBacks<List<ResultsEntity>>
    ) {
        val req = HomeRequest()
        req.child_id = getChildId()
        req.grade = getGrade()
        req.board = getBoard()
        req.goal = getGoal()
        req.onlyPractise = isPractice
        req.contentSectionType = sectionType
        req.offset = offset
        req.size = size
        req.exam = getEaxmSlugName().replace("exam--", "", true)
        req.exam_name = getExamName()
        val call = repoHome.homeSubjectFilterSection(req, subject)

        safeApi(call, callback)
    }

    fun homeQuickLinksSectionApi(
        sectionType: String,
        offset: Int,
        size: Int,
        callback: APICallBacks<List<ResultsEntity>>
    ) {
        var req = HomeRequest()
        req.child_id = getChildId()
        req.grade = getGrade()
        req.board = getBoard()
        req.goal = getGoal()
        req.contentSectionType = sectionType
        req.offset = offset
        req.size = size
        req.exam = getEaxmSlugName().replace("exam--", "", true)

        safeApi(repoHome.homeQuickLinksSection(req), callback)
    }

    /*combine both api results*/
    private fun filterBoth(
        relatedConceptsRes: Response<ConceptsResponse>,
        moreConceptsRes: Response<ConceptsResponse>
    ): HomeModel {
        val mHomeModel = HomeModel()
        if (!moreConceptsRes.isSuccessful) {
            /*errorCode = relatedConceptsRes.code()
        throwException(moreConceptsRes.code())*/
            mHomeModel.moreConcepts = arrayListOf()
        } else {
            mHomeModel.moreConcepts = (moreConceptsRes.body() as ConceptsResponse).data
        }
        if (!relatedConceptsRes.isSuccessful) {
            /*errorCode = relatedConceptsRes.code()
        throwException(relatedConceptsRes.code())*/
            mHomeModel.relatedConcepts = arrayListOf()
        } else {
            mHomeModel.relatedConcepts = (relatedConceptsRes.body() as ConceptsResponse).data
        }
        var pref = PreferenceHelper()
        return mHomeModel
    }

    fun getNextVideoRecommendations(
        contentId: String?,
        source: String?,
        bookId: String?,
        topicLearnPathName: String?,
        learnPathName: String?,
        learnPathFormatName: String?,
        callBack: APICallBacks<NextVideoRecommendationsRes>
    ) {
        safeApi(
            repoHome.fetchNextVideoRecommendations(
                contentId!!,
                source!!,
                bookId!!,
                topicLearnPathName!!,
                learnPathName ?: "",
                learnPathFormatName ?: ""
            ), callBack
        )
    }

    fun getAutoPlayVideos(
        conceptId: String,
        contentId: String,
        ids: ArrayList<String>?, bookId: String,
        isEmbibeSyllabus: Boolean,
        callBack: APICallBacks<NextVideoRecommendationsRes>
    ) {
        val map = LinkedHashMap<String, String>()


        if (isEmbibeSyllabus) {
            map[AppConstants.SOURCE] = AppConstants.SOURCE_EMBIBE_SYLLABUS
        } else {
            map[AppConstants.SOURCE] = AppConstants.SOURCE_BOOK_TOC
        }
        map[AppConstants.BOOK_ID] = bookId

        if (ids != null && ids.isNotEmpty()) {
            val idStr = android.text.TextUtils.join(",", ids)
            map[AppConstants.CONTENT_IDS] = idStr
        }
        map[AppConstants.CONTENT_IDS] = contentId

        safeApi(repoHome.fetchAutoPlay(conceptId, map), callBack)
    }

    @SuppressLint("CheckResult")
    fun <T> getConceptsAsync(
        content: Content,
        callback: APIMergeCallBacks<T>
    ) {
        Single.zip(
            repoHome.fetchRelatedConcepts(content.learning_map.conceptId, content.id),
            repoHome.fetchMoreTopics(content.learning_map.topicLearnPathName, content.id),
            BiFunction<Response<ConceptsResponse>, Response<ConceptsResponse>, HomeModel> { mRelatedRes, mMoreConceptsRes ->
                // here we get both the results at a time.
                return@BiFunction filterBoth(mRelatedRes, mMoreConceptsRes)
            })
            .subscribeOn(Schedulers.io())
            .onErrorResumeNext {
                throw CustomException("server Connection failed")
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback.onSuccess(it as T)
            }, {
                callback.onFailed(errorCode, getError(it), "")
            })
    }


    @SuppressLint("CheckResult")
    fun <T> getLearnSummaryAsync(
        content: Content,
        callback: APIMergeCallBacks<T>
    ) {
        val topicForChapterRequest = TopicForChapterRequest()
        topicForChapterRequest.learnmap_id = content.learnmap_id

        val practiceForChapterRequest = PracticeForChapterRequest()
        practiceForChapterRequest.learnmap_id = content.learnmap_id
        practiceForChapterRequest.subject = content.subject

        val testForChapterRequest = TestForChapterRequest()
        testForChapterRequest.board = getBoard()
        testForChapterRequest.child_id = getChildId()
        testForChapterRequest.exam = getEaxmSlugName().replace("exam--", "", true)
        testForChapterRequest.goal = getGoal()
        testForChapterRequest.grade = getGrade()
        if (content.learning_map.chapter.isEmpty()) {
            testForChapterRequest.chapter_name = replaceSpaceWithHypen(content.title.toLowerCase())
        } else {
            testForChapterRequest.chapter_name = replaceSpaceWithHypen(content.learning_map.chapter)
        }

        //sending hardcoded chapter for now due to no support from backend
//        testForChapterRequest.chapter_name = "magnetic-effects-of-electric-current-10"

        Single.zip(
            repoHome.fetchPrerequisitesForChapter(topicForChapterRequest)
                .subscribeOn(Schedulers.newThread()),
            repoHome.fetchPracticesForChapter(practiceForChapterRequest)
                .subscribeOn(Schedulers.newThread()),
            repoHome.fetchTopicsForChapter(topicForChapterRequest)
                .subscribeOn(Schedulers.newThread()),
            repoHome.fetchTestsForChapter(testForChapterRequest)
                .subscribeOn(Schedulers.newThread()),
            repoHome.fetchVideosForChapterV1(content.learnmap_id, AppConstants.VIDEO_FOR_CHAPTER)
                .subscribeOn(Schedulers.newThread()),
            Function5<Response<List<Content>>, Response<List<Content>>, Response<List<Content>>,
                    Response<List<Content>>, Response<List<ChapterLearningObject>>, HomeModel> { mPrerequisiteRes,
                                                                                         mPracticeChapterRes,
                                                                                         mTopicChapterRes,
                                                                                         mTestChapterRes,
                                                                                         mVideoChapterRes ->
                // here we combine the results to one.
                return@Function5 mergeLearnSummaryRes(
                    mPrerequisiteRes,
                    mPracticeChapterRes,
                    mTopicChapterRes,
                    mTestChapterRes,
                    mVideoChapterRes
                )
            })
            .onErrorResumeNext {
                it.printStackTrace()
                throw CustomException(it.message.toString())
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback.onSuccess(it as T)
            }, {
                callback.onFailed(errorCode, getError(it), "")
            })
    }

    private fun replaceSpaceWithHypen(chapterName: String): String {
        if (chapterName.contains(" ")) {
            return chapterName.replace(" ", "-")
        } else return chapterName
    }

    /*combine all learn summary api calls results*/
    private fun mergeLearnSummaryRes(
        prerequisitesRes: Response<List<Content>>,
        practicesForChapterRes: Response<List<Content>>,
        topicsForChapterRes: Response<List<Content>>,
        testsForChapterRes: Response<List<Content>>,
        videosForChapterRes: Response<List<ChapterLearningObject>>
    ): HomeModel {
        val mHomeModel = HomeModel()
        if (!prerequisitesRes.isSuccessful || prerequisitesRes.code() == NO_CONTENT_CODE) {
            mHomeModel.prerequisitesForChapter = arrayListOf()
        } else {
            mHomeModel.prerequisitesForChapter =
                prerequisitesRes.body() as ArrayList<Content>
        }
        if (!topicsForChapterRes.isSuccessful || topicsForChapterRes.code() == NO_CONTENT_CODE) {
            mHomeModel.topicsForChapter = arrayListOf()
        } else {
            mHomeModel.topicsForChapter =
                topicsForChapterRes.body() as ArrayList<Content>
        }
        if (!videosForChapterRes.isSuccessful || videosForChapterRes.code() == NO_CONTENT_CODE) {
            mHomeModel.videosForChapterV1 = arrayListOf()
        } else {
            mHomeModel.videosForChapterV1 =
                videosForChapterRes.body() as ArrayList<ChapterLearningObject>
        }
        if (!testsForChapterRes.isSuccessful || testsForChapterRes.code() == NO_CONTENT_CODE) {
            mHomeModel.testsForChapter = arrayListOf()
        } else {
            mHomeModel.testsForChapter =
                testsForChapterRes.body() as ArrayList<Content>
        }
        if (!practicesForChapterRes.isSuccessful || practicesForChapterRes.code() == NO_CONTENT_CODE) {
            mHomeModel.practicesForChapter = arrayListOf()
        } else {
            mHomeModel.practicesForChapter =
                practicesForChapterRes.body() as ArrayList<Content>
        }

        return mHomeModel
    }

    /*combine all topic summary api calls results*/
    private fun mergeTopicSummaryRes(
        prerequisitesRes: Response<List<Content>>,
        practicesForTopicRes: Response<List<Content>>,
        relatedTopicsRes: Response<List<Content>>,
        testsForTopicRes: Response<List<Content>>,
        videosForTopicRes: Response<List<ResultsEntity>>
    ): HomeModel {
        val mHomeModel = HomeModel()
        if (!prerequisitesRes.isSuccessful || prerequisitesRes.code() == NO_CONTENT_CODE) {
            mHomeModel.prerequisitesForTopic = arrayListOf()
        } else {
            mHomeModel.prerequisitesForTopic =
                prerequisitesRes.body() as ArrayList<Content>
        }
        if (!practicesForTopicRes.isSuccessful || practicesForTopicRes.code() == NO_CONTENT_CODE) {
            mHomeModel.practicesForTopic = arrayListOf()
        } else {
            mHomeModel.practicesForTopic =
                practicesForTopicRes.body() as ArrayList<Content>
        }
        if (!videosForTopicRes.isSuccessful || videosForTopicRes.code() == NO_CONTENT_CODE) {
            mHomeModel.videosForTopic = arrayListOf()
        } else {
            mHomeModel.videosForTopic =
                videosForTopicRes.body() as ArrayList<ResultsEntity>
        }
        if (!testsForTopicRes.isSuccessful || testsForTopicRes.code() == NO_CONTENT_CODE) {
            mHomeModel.testsForTopic = arrayListOf()
        } else {
            mHomeModel.testsForTopic =
                testsForTopicRes.body() as ArrayList<Content>
        }
        if (!relatedTopicsRes.isSuccessful || relatedTopicsRes.code() == NO_CONTENT_CODE) {
            mHomeModel.relatedTopics = arrayListOf()
        } else {
            mHomeModel.relatedTopics =
                relatedTopicsRes.body() as ArrayList<Content>
        }

        return mHomeModel
    }

    @SuppressLint("CheckResult")
    fun <T> getTestSummaryAsync(
        content: Content,
        callback: APIMergeCallBacks<T>
    ) {

        val testDetailRequest = TestDetailRequest()
        testDetailRequest.board = getBoard()
        testDetailRequest.exam = getExamName()
        testDetailRequest.goal = getGoal()
        testDetailRequest.grade = getGrade()
        testDetailRequest.test_type = "full_test"
        testDetailRequest.subject = "Science"
        testDetailRequest.unit = "Chemistry"
        testDetailRequest.test_code = content.bundle_id


        val moreTestsRequest = TestForChapterRequest()
        moreTestsRequest.board = getBoard()
        moreTestsRequest.child_id = getChildId()
        moreTestsRequest.exam = getEaxmSlugName().replace("exam--", "", true)
        moreTestsRequest.goal = getGoal()
        moreTestsRequest.grade = getGrade()
        moreTestsRequest.subject = if (content.learning_map.subject.isNotEmpty()) {
            content.learning_map.subject.toLowerCase()
        } else {
            content.subject.toLowerCase()
        }



        Single.zip(
            repoHome.fetchRecommendedLearningForTest(
                testDetailRequest
            ).subscribeOn(Schedulers.newThread()),
            repoHome.fetchRecommendedPracticeForTest(testDetailRequest)
                .subscribeOn(Schedulers.newThread()),
            repoHome.fetchMoreTests(moreTestsRequest).subscribeOn(Schedulers.newThread()),

            Function3<Response<List<Results>>, Response<List<Content>>, Response<List<Content>>, HomeModel> { mRecommendedLearningRes, mRecommendedPracticeRes, mMoreTestsRes ->
                // here we combine the results to one.
                return@Function3 mergeTestSummaryRes(
                    mRecommendedLearningRes, mRecommendedPracticeRes, mMoreTestsRes
                )
            })
            .onErrorResumeNext {
                it.printStackTrace()
                throw CustomException(it.message.toString())
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback.onSuccess(it as T)
            }, {
                callback.onFailed(errorCode, getError(it), "")
            })
    }

    /*combine all test summary api calls results*/
    private fun mergeTestSummaryRes(
        recommendedLearningRes: Response<List<Results>>,
        recommendedPracticeRes: Response<List<Content>>,
        moreTestsRes: Response<List<Content>>
    ): HomeModel {
        val mHomeModel = HomeModel()
        if (!recommendedLearningRes.isSuccessful || recommendedLearningRes.code() == NO_CONTENT_CODE) {
            mHomeModel.recommendedLearningForTest = arrayListOf()
        } else {
            mHomeModel.recommendedLearningForTest =
                recommendedLearningRes.body() as ArrayList<Results>
        }
        if (!recommendedPracticeRes.isSuccessful || recommendedPracticeRes.code() == NO_CONTENT_CODE) {
            mHomeModel.recommendedPracticeForTest = arrayListOf()
        } else {
            mHomeModel.recommendedPracticeForTest =
                recommendedPracticeRes.body() as ArrayList<Content>
        }
        if (!moreTestsRes.isSuccessful || moreTestsRes.code() == NO_CONTENT_CODE) {
            mHomeModel.moreTests = arrayListOf()
        } else {
            mHomeModel.moreTests =
                moreTestsRes.body() as ArrayList<Content>
        }

        return mHomeModel
    }

    @SuppressLint("CheckResult")
    fun <T> getTopicSummaryAsync(
        content: Content,
        callback: APIMergeCallBacks<T>
    ) {

        val topicsRequest = PracticeForChapterRequest()
        topicsRequest.learnmap_id = content.learnmap_id
        topicsRequest.subject = content.subject

        val testForTopicRequest = TestForChapterRequest()
        testForTopicRequest.board = getBoard()
        testForTopicRequest.child_id = getChildId()
        testForTopicRequest.exam = getEaxmSlugName().replace("exam--", "", true)
        testForTopicRequest.goal = getGoal()
        testForTopicRequest.grade = getGrade()
        testForTopicRequest.unit = content.learning_map.unit

        //sending hardcoded chapter for now due to no support from backend
//        testForChapterRequest.chapter_name = "magnetic-effects-of-electric-current-10"

        Single.zip(
            repoHome.fetchPrerequisitesForTopic(topicsRequest).subscribeOn(Schedulers.newThread()),
            repoHome.fetchPracticesForTopic(topicsRequest).subscribeOn(Schedulers.newThread()),
            repoHome.fetchRelatedTopics(topicsRequest).subscribeOn(Schedulers.newThread()),
            repoHome.fetchTestsForTopic(testForTopicRequest).subscribeOn(Schedulers.newThread()),
            repoHome.fetchVideosForTopic(content.learnmap_id, AppConstants.VIDEO_FOR_CHAPTER)
                .subscribeOn(Schedulers.newThread()),
            Function5<Response<List<Content>>, Response<List<Content>>, Response<List<Content>>,
                    Response<List<Content>>, Response<List<ResultsEntity>>, HomeModel> { mPrerequisiteRes,
                                                                                         mPracticeTopicRes,
                                                                                         mRelatedTopicsRes,
                                                                                         mTestTopicRes,
                                                                                         mVideoTopicRes ->
                // here we combine the results to one.
                return@Function5 mergeTopicSummaryRes(
                    mPrerequisiteRes,
                    mPracticeTopicRes,
                    mRelatedTopicsRes,
                    mTestTopicRes,
                    mVideoTopicRes
                )
            })
            .onErrorResumeNext {
                it.printStackTrace()
                throw CustomException(it.message.toString())
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback.onSuccess(it as T)
            }, {
                callback.onFailed(errorCode, getError(it), "")
            })
    }

    fun searchResults(
        type: String,
        query: String,
        data: Content?,
        learnSummaryFragment: LearnSummaryFragment,
        callBack: APICallBacks<List<Content>>
    ) {
        val searchRequest = SearchRequest()
        searchRequest.user_id = Utils.getChildId()
        searchRequest.learn_path = data?.learnpath_name
        searchRequest.source = "Learn Page Summary"
        searchRequest.query = query

        when (type) {
            learnSummaryFragment.resources.getString(R.string.all_videos_for_this_chapter) -> {
                searchRequest.section = "videos"
                return safeApi(repoHome.searchAllVideos(searchRequest), callBack)

            }
            learnSummaryFragment.resources.getString(R.string.test_on_this_chapter) -> {

            }
            learnSummaryFragment.resources.getString(R.string.practice_on_this_chapter) -> {
                searchRequest.section = "topics"
                return safeApi(repoHome.searchChapterPractise(searchRequest), callBack)

            }

            learnSummaryFragment.resources.getString(R.string.topics_in_this_chapter) -> {
                searchRequest.section = "topics"
                return safeApi(repoHome.searchChapterTopics(searchRequest), callBack)


            }
            learnSummaryFragment.resources.getString(R.string.pre_requisite_topics) -> {
                searchRequest.section = "pre-requisite topics"
                return safeApi(repoHome.searchPrerequisite(searchRequest), callBack)
            }
        }
    }

    fun homeAchieveApi(callback: APICallBacks<List<AchieveHome>>) {
        val req = HomeRequest()
        req.child_id = getChildId()
        req.grade = getGrade()
        req.board = getBoard()
        req.goal = getGoal()
        req.exam = getEaxmSlugName().replace("exam--", "", true)

        safeApi(repoHome.fetchAchieveHome(req), callback)
    }

    fun homeAchieveSubjectApi(subject: String,callback: APICallBacks<List<AchieveHome>>) {
        val req = HomeRequest()
        req.child_id = getChildId()
        req.grade = getGrade()
        req.board = getBoard()
        req.goal = getGoal()
        req.exam = getEaxmSlugName().replace("exam--", "", true)

        safeApi(repoHome.fetchAchieveHomeSubject(subject,req), callback)
    }


}

