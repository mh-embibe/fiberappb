package com.embibe.embibetvapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.ActivitySelectTestSettingsBinding
import com.embibe.embibetvapp.model.createtest.configuration.TestConfigurationRes
import com.embibe.embibetvapp.model.createtest.examconfig.ChapterDetails
import com.embibe.embibetvapp.model.createtest.examconfig.SubjectDetails
import com.embibe.embibetvapp.model.createtest.generateowntest.response.GenerateOwnTestResponse
import com.embibe.embibetvapp.ui.adapter.RvDifficultLevelAdapter
import com.embibe.embibetvapp.ui.adapter.RvSubjectsTestAdapter
import com.embibe.embibetvapp.ui.viewmodel.SelectTestSettingsViewModel
import com.embibe.embibetvapp.utils.CenterZoomLayoutManager
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibe.embibetvapp.utils.toast
import kotlinx.android.synthetic.main.item_select_test_activity.view.*
import java.text.SimpleDateFormat
import java.util.*


class SelectTestSettingsActivity :
    BaseFragmentActivity(),
    View.OnClickListener {
    private lateinit var testConfigurationDetails: TestConfigurationRes
    private var chapterDetails: ArrayList<ChapterDetails>? = null
    private lateinit var createNewTestViewModel: SelectTestSettingsViewModel
    private lateinit var linearLayoutManager: CenterZoomLayoutManager
    private lateinit var subjectAdapter: RvSubjectsTestAdapter
    private var selectedSubjectDetails: ArrayList<SubjectDetails>? = null
    private lateinit var difficultLevelAdapter: RvDifficultLevelAdapter
    private var difficultLevels = mutableListOf<String>()
    private var minutes = mutableListOf<String>()
    private var correctMarks = mutableListOf<String>()
    private var inCorrectMarks = mutableListOf<String>()
    private lateinit var snapHelper: LinearSnapHelper
    private lateinit var binding: ActivitySelectTestSettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        selectedSubjectDetails =
            intent.getParcelableArrayListExtra<SubjectDetails>("SubjectDetails")
        chapterDetails = intent.getParcelableArrayListExtra<ChapterDetails>("ChapterDetails")

        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_test_settings)


        if (DataManager.instance.getTestConfigurations() != null) {
            testConfigurationDetails = DataManager.instance.getTestConfigurations()
            for (data in testConfigurationDetails.duration!!) {
                if (data != null) {
                    minutes.add(data.toString())
                }
            }
            for (data in testConfigurationDetails.correctMarks!!) {
                if (data != null) {
                    correctMarks.add(data.toString())
                }
            }
            for (data in testConfigurationDetails.incorrectMarks!!) {
                if (data != null) {
                    inCorrectMarks.add(data.toString())
                }
            }
            for (data in testConfigurationDetails.difficultyLevel!!) {
                if (data != null) {
                    difficultLevels.add(data)
                }
            }



            initView()
        }

    }

    private fun initView() {
        binding.btnBack.setOnClickListener(this)
        binding.btnCreateTest.setOnClickListener(this)
        createNewTestViewModel =
            ViewModelProviders.of(this).get(SelectTestSettingsViewModel::class.java)

        setSubjectAdapter()

        setAdapter(binding.rvDifficultLevel, RvDifficultLevelAdapter(this), difficultLevels)
        setAdapter(binding.rvDuration, RvDifficultLevelAdapter(this), minutes)
        setAdapter(binding.rvCorrectAnswer, RvDifficultLevelAdapter(this), correctMarks)
        setAdapter(binding.rvInCorrectAnswer, RvDifficultLevelAdapter(this), inCorrectMarks)


    }

    private fun createNewTestApi() {

        var difficultLevel = binding.rvDifficultLevel.getChildAt(1)
            .findViewById<TextView>(R.id.title).text.toString()
        var duration =
            binding.rvDuration.getChildAt(1)
                .findViewById<TextView>(R.id.title).text.toString().toInt()
        var correctMark =
            binding.rvCorrectAnswer.getChildAt(1)
                .findViewById<TextView>(R.id.title).text.toString()
        var inCorrectMark =
            binding.rvInCorrectAnswer.getChildAt(1)
                .findViewById<TextView>(R.id.title).text.toString()

        var testName = binding.etTestName.text.toString().trim()
        var createdAt = getCurrentDateAndTime()
        if (testName.isEmpty()) {
            toast(getString(R.string.please_enter_test_name))

        } else {
            showProgress()
            createNewTestViewModel.generateTest(
                testName, createdAt,
                selectedSubjectDetails,
                chapterDetails,
                difficultLevel.toLowerCase(),
                duration, correctMark, inCorrectMark,
                object : BaseViewModel.APICallBacks<GenerateOwnTestResponse> {
                    override fun onSuccess(model: GenerateOwnTestResponse?) {
                        hideProgress()
                        if (model != null) {

                            var intent = Intent(
                                this@SelectTestSettingsActivity,
                                GeneratingCustomTestProgressActivity::class.java
                            )
                            intent.putExtra(AppConstants.CREATE_TEST, model)
                            intent.putExtra(AppConstants.TEST_NAME, testName)
                            intent.putExtra(AppConstants.CREATED_AT, createdAt)
                            intent.putExtra(AppConstants.TEST_DIFFICULTY_LEVEL, difficultLevel)
                            intent.putExtra(AppConstants.TEST_DURATION, duration)
                            intent.putExtra(AppConstants.TEST_CORRECT_MARK, correctMark)
                            intent.putExtra(AppConstants.TEST_INCORRECT_MARK, inCorrectMark)

                            intent.putParcelableArrayListExtra(
                                "ChapterDetails",
                                chapterDetails
                            )
                            startActivity(intent)
                            finish()
                        }
                    }

                    override fun onFailed(code: Int, error: String, msg: String) {
                        hideProgress()
                        if (Utils.isApiFailed(code)) {
                            Utils.showError(
                                this@SelectTestSettingsActivity,
                                code,
                                object : BaseViewModel.ErrorCallBacks {
                                    override fun onRetry(msg: String) {
                                        createNewTestApi()
                                    }

                                    override fun onDismiss() {}
                                })
                        } else {
                            Utils.showToast(this@SelectTestSettingsActivity, error)
                        }
                    }
                })
        }


    }

    private fun setSubjectAdapter() {
        subjectAdapter = RvSubjectsTestAdapter(this)
        binding.rvSubjects.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.rvSubjects.adapter = subjectAdapter
        subjectAdapter.setData(ArrayList(selectedSubjectDetails!!.filter { it.count != 0 }))
    }

    private fun setAdapter(
        rvDifficultLevel: RecyclerView,
        rvDifficultLevelAdapter: RvDifficultLevelAdapter,
        difficultLevels: MutableList<String>
    ) {
        var mLastSnappedView: View? = null
        difficultLevelAdapter = rvDifficultLevelAdapter
        linearLayoutManager = CenterZoomLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvDifficultLevel.layoutManager =
            CenterZoomLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        difficultLevelAdapter.setHasStableIds(true)
        rvDifficultLevel.adapter = difficultLevelAdapter
        difficultLevelAdapter.setData(difficultLevels)

        snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(rvDifficultLevel)

        Handler().postDelayed(Runnable {
            rvDifficultLevel.scrollToPosition(difficultLevels.size + 5)

            setBackgroundForItemViews(
                rvDifficultLevel.findViewHolderForAdapterPosition(0)?.itemView?.title,
                R.drawable.bg_item_select_test_setting_activity_unselect_left_gradient
            )
            setBackgroundForItemViews(
                rvDifficultLevel.findViewHolderForAdapterPosition(1)?.itemView?.title,
                R.drawable.bg_item_select_test_setting_activity_select
            )
            setBackgroundForItemViews(
                rvDifficultLevel.findViewHolderForAdapterPosition(2)?.itemView?.title,
                R.drawable.bg_item_select_test_setting_activity_unselect
            )
            initDifficultySettingViews(rvDifficultLevel)

        }, 100)
        rvDifficultLevel.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val firstItemVisible: Int = linearLayoutManager.findFirstVisibleItemPosition()
                if (firstItemVisible != 0 && firstItemVisible.rem(difficultLevels.size) === 0) {
                    recyclerView.layoutManager?.scrollToPosition(0)
                }
                try {
                    setBackgroundForItemViews(
                        recyclerView.getChildAt(0)
                            .findViewById<TextView>(R.id.title),
                        R.drawable.bg_item_select_test_setting_activity_unselect_left_gradient
                    )
                    setBackgroundForItemViews(
                        recyclerView.getChildAt(1)
                            .findViewById<TextView>(R.id.title),
                        R.drawable.bg_item_select_test_setting_activity_select
                    )
                    setBackgroundForItemViews(
                        recyclerView.getChildAt(2)
                            .findViewById<TextView>(R.id.title),
                        R.drawable.bg_item_select_test_setting_activity_unselect
                    )

                    setFocusAndClickableAction(
                        recyclerView.getChildAt(0)
                            ?.findViewById<RelativeLayout>(R.id.rl_Main), false
                    )
                    setFocusAndClickableAction(
                        recyclerView.getChildAt(1)
                            ?.findViewById<RelativeLayout>(R.id.rl_Main), true
                    )
                    setFocusAndClickableAction(
                        recyclerView.getChildAt(2)
                            ?.findViewById<RelativeLayout>(R.id.rl_Main), false
                    )

                    recyclerView.getChildAt(1)?.findViewById<RelativeLayout>(R.id.rl_Main)
                        ?.setOnClickListener {
                            rvDifficultLevel.layoutManager?.scrollToPosition(1)
                        }
                } catch (e: NullPointerException) {

                }

            }


            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    val view = snapHelper.findSnapView(recyclerView.layoutManager)

                    setBackgroundForItemViews(
                        recyclerView.getChildViewHolder(view!!)
                            .itemView.title, R.drawable.bg_item_select_test_setting_activity_select
                    )

                    setFocusAndClickableAction(
                        recyclerView.getChildViewHolder(view)
                            .itemView.rl_Main, true
                    )

                    recyclerView.getChildViewHolder(view).itemView.rl_Main?.requestFocus()

                    mLastSnappedView = view

                } else if (mLastSnappedView != null) {

                    try {

                        setBackgroundForItemViews(
                            recyclerView.getChildViewHolder(mLastSnappedView!!)
                                .itemView.title,
                            R.drawable.bg_item_select_test_setting_activity_unselect
                        )

                        setFocusAndClickableAction(
                            recyclerView.getChildViewHolder(mLastSnappedView!!)
                                .itemView.rl_Main, false
                        )

                        mLastSnappedView = null
                    } catch (e: KotlinNullPointerException) {

                    }
                }
            }


        })


    }

    private fun setBackgroundForItemViews(
        title: TextView?,
        bgItemSelectTestSettingActivityUnselectLeftGradient: Int
    ) {
        title?.setBackgroundResource(
            bgItemSelectTestSettingActivityUnselectLeftGradient
        )
    }

    private fun setFocusAndClickableAction(
        title: RelativeLayout?,
        status: Boolean
    ) {
        title?.isClickable =
            status
        title?.isFocusable =
            status
    }


    private fun initDifficultySettingViews(rvDifficultLevel: RecyclerView) {
        setFocusAndClickableAction(
            rvDifficultLevel.findViewHolderForAdapterPosition(0)?.itemView?.rl_Main,
            false
        )
        setFocusAndClickableAction(
            rvDifficultLevel.findViewHolderForAdapterPosition(1)?.itemView?.rl_Main,
            true
        )
        setFocusAndClickableAction(
            rvDifficultLevel.findViewHolderForAdapterPosition(2)?.itemView?.rl_Main,
            false
        )

        rvDifficultLevel.findViewHolderForAdapterPosition(1)?.itemView?.rl_Main?.setOnClickListener {
            rvDifficultLevel.layoutManager?.scrollToPosition(1)
            rvDifficultLevel.findViewHolderForAdapterPosition(1)?.itemView?.rl_Main?.isFocusable =
                true
            rvDifficultLevel.findViewHolderForAdapterPosition(1)?.itemView?.rl_Main?.requestFocus()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnCreateTest -> {
                createNewTestApi()

            }
            R.id.btnBack -> {
                finish()
            }
        }
    }

    fun getCurrentDateAndTime(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
        return sdf.format(Date())
    }

}