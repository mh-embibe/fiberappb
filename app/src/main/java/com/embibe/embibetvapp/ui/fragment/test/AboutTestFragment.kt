package com.embibe.embibetvapp.ui.fragment.test

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentAboutTestBinding
import com.embibe.embibetvapp.model.test.TestDetail
import com.embibe.embibetvapp.model.test.TestDetailModel
import com.embibe.embibetvapp.ui.adapter.TestDetailAdapter
import com.embibe.embibetvapp.utils.SegmentUtils
import java.util.*
import kotlin.math.roundToInt

class AboutTestFragment : BaseAppFragment() {

    private lateinit var binding: FragmentAboutTestBinding
    private lateinit var testDetailAdapter: TestDetailAdapter
    private lateinit var data: TestDetail
    private lateinit var type: String
    private var progressLayoutWidth: Int = 0
    var height: Int = 0

    companion object {
        lateinit var title: String
        lateinit var totalDuration: String
        lateinit var totalQuestions: String
        lateinit var totalMarks: String
        lateinit var myScore: String
        lateinit var goodScore: String
        lateinit var cutOffScore: String
        lateinit var TQS: String
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_about_test, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        data = (arguments?.getSerializable(AppConstants.TEST_DETAIL) as TestDetail)
        type = this.requireArguments().getString("type").toString()
        SegmentUtils.trackTestSummaryScreenAvailableOptionContentLoadStart(type)
        setData()
        setTestDetailRecycler(view, createData())
        setTestAnalysisComponent()
    }

    override fun onResume() {
        super.onResume()
        SegmentUtils.trackPracticeSummaryScreenAvailableOptionContentLoadEnd(type)
    }

    private fun setData() {
        title = data.title
        totalDuration = data.totalDuration
        totalQuestions = data.totalQuestions
        totalMarks = data.totalMarks
        myScore = data.myScore
        goodScore = data.goodScore
        cutOffScore = data.cutOffScore
        TQS = data.tqs
    }

    private fun setTestDetailRecycler(view: View, data: ArrayList<TestDetailModel>) {
        testDetailAdapter = TestDetailAdapter()
        val layoutManager = GridLayoutManager(view.context, 3, GridLayoutManager.VERTICAL, false)
        binding.recyclerTestDetail.adapter = testDetailAdapter
        binding.recyclerTestDetail.layoutManager = layoutManager
        testDetailAdapter.setData(data)
        binding.textPercentage.text = TQS
    }

    private fun createData(): ArrayList<TestDetailModel> {
        return arrayListOf(
            TestDetailModel(R.drawable.ic_duration, totalDuration, getString(R.string.duration)),
            TestDetailModel(R.drawable.ic_questions, totalQuestions, getString(R.string.questions)),
            TestDetailModel(R.drawable.ic_marks, totalMarks, getString(R.string.marks))
        )
    }

    private fun setTestAnalysisComponent() {

        if (myScore.isEmpty() || myScore.isBlank()) { // test not taken so no analysis
            binding.cardTestAnalysis.clTestProgress.visibility = View.GONE
        } else {
            binding.cardTestAnalysis.clTestProgress.visibility = View.VISIBLE
            setAndAnimateProgress()
        }
    }

    private fun setAndAnimateProgress() {
        val goodScore = goodScore.toDouble().roundToInt()
        val yourScore = myScore.toDouble().roundToInt()
        val cutOffScore = cutOffScore.toDouble().roundToInt()

        var totalScore = 100
        //to avoid values going of screen
        val extraBiasSpace = getHorizontalBiasOfBaseLine(totalScore)
        totalScore += extraBiasSpace
        //adding still more delta value to avoid going of screen
        totalScore += 20

        animate(
            goodScore, totalScore,
            binding.cardTestAnalysis.progressBarGoodScore,
            binding.cardTestAnalysis.tvGoodScore
        )

        moveVerticalBaseLineBiasBasedOnScore(yourScore.toDouble(), totalScore)
        moveCutOffLineBiasBasedOnScore(cutOffScore, totalScore)

        animate(
            yourScore, totalScore,
            binding.cardTestAnalysis.progressBarYourScore,
            binding.cardTestAnalysis.tvYourScore
        )
    }

    private fun getHorizontalBiasOfBaseLine(totalScore: Int): Int {
        val params =
            binding.cardTestAnalysis.viewLine.layoutParams as ConstraintLayout.LayoutParams
        return params.horizontalBias.times(totalScore).toInt()
    }

    private fun moveVerticalBaseLineBiasBasedOnScore(yourScore: Double, totalScore: Int) {
        if (yourScore < 0) {
            val baseLineVertical = binding.cardTestAnalysis.viewLine
            val baseLineParams = baseLineVertical.layoutParams as ConstraintLayout.LayoutParams
            baseLineParams.horizontalBias =
                baseLineParams.horizontalBias + (Math.abs(yourScore).toFloat() / totalScore)
            baseLineVertical.layoutParams = baseLineParams
            baseLineVertical.requestLayout()
        }
    }

    private fun moveCutOffLineBiasBasedOnScore(cutOffScore: Int, totalScore: Int) {
        // AnyScore/totalScore to get bias value
        //totalMarks 130, Negative Marks -12

        val calScore = (cutOffScore.toDouble().div(totalScore))
        val cutOffLine = binding.cardTestAnalysis.ivCutOffMarker
        val cutOffLabel = binding.cardTestAnalysis.tvCutOffMarkerLabel
        val cutOffLineParams = cutOffLine.layoutParams as ConstraintLayout.LayoutParams
        val cutOffLineAnimator = ValueAnimator.ofFloat(0f, calScore.toFloat())
        cutOffLineAnimator.addUpdateListener { value ->
            cutOffLineParams.horizontalBias = (value.animatedValue as Float)
            cutOffLine.layoutParams = cutOffLineParams
        }

        val cutOffLabelAnimator = ValueAnimator.ofInt(0, cutOffScore)
        cutOffLabelAnimator.addUpdateListener { value ->
            cutOffLabel.text = (value.animatedValue as Int).toString()
        }
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(cutOffLabelAnimator, cutOffLineAnimator)
        animatorSet.duration = 1500
        animatorSet.start()
    }

    private fun animate(progressValue: Int, totalScore: Int, progressBar: ImageView, tv: TextView) {
        animateBar(progressBar, progressValue, totalScore)
        tv.animateWithProgressBar(progressValue)
    }

    private fun TextView.animateWithProgressBar(progressValue: Int) {

        val animator = ObjectAnimator.ofInt(0, progressValue)
        animator.addUpdateListener { animationValue ->
            this.text = animationValue.animatedValue.toString()
        }
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(animator)
        animatorSet.duration = 2000
        animatorSet.start()
    }

    private fun animateBar(bar: ImageView, progressValue: Int, totalScore: Int) {
        val progressLayout = binding.cardTestAnalysis
        val vto = progressLayout.root.viewTreeObserver
        var newProgressValue = 0
        newProgressValue = Math.abs(progressValue)
        var listener: ViewTreeObserver.OnGlobalLayoutListener? = null
        listener = ViewTreeObserver.OnGlobalLayoutListener {
            progressLayoutWidth = progressLayout.root.width
            height = progressLayout.root.height

            val calValue = newProgressValue.toDouble() / totalScore * progressLayoutWidth
            val animator = ValueAnimator.ofInt(0, calValue.toInt())
            animator.addUpdateListener { valueAnimator ->
                val value = valueAnimator.animatedValue
                val params = bar.layoutParams as ConstraintLayout.LayoutParams
                if (value as Int > 0) {
                    bar.visibility = View.VISIBLE
                } else
                    bar.visibility = View.GONE
                params.width = value
                bar.layoutParams = params
            }

            animator.duration = 1500
            animator.start()

            binding.cardTestAnalysis.root.viewTreeObserver.removeOnGlobalLayoutListener(
                listener!!
            )
        }
        vto?.addOnGlobalLayoutListener(listener)
    }
}
