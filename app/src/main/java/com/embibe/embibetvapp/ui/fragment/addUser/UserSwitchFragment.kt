package com.embibe.embibetvapp.ui.fragment.addUser

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.embibe.embibetvapp.BuildConfig
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentUserSwitchBinding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.goals.GoalsExamsRes
import com.embibe.embibetvapp.newmodel.ResultsEntity
import com.embibe.embibetvapp.ui.activity.*
import com.embibe.embibetvapp.ui.adapter.UserSwitchAdapter
import com.embibe.embibetvapp.ui.interfaces.ComponentClickListener
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.ui.viewmodel.SignInViewModel
import com.embibe.embibetvapp.utils.SegmentUtils
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.embibejio.coreapp.model.LinkedProfile
import com.embibejio.coreapp.preference.PreferenceHelper
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*
import kotlin.collections.ArrayList


class UserSwitchFragment : BaseAppFragment(),
    ComponentClickListener {
    lateinit var listener: CallBackListener
    lateinit var homeViewModel: HomeViewModel
    lateinit var signInViewModel: SignInViewModel
    private var currentProfileId: String = ""
    private lateinit var binding: FragmentUserSwitchBinding
    private lateinit var dataList: List<LinkedProfile?>
    var pref = PreferenceHelper()
    private lateinit var userSwitchAdapterAdapter: UserSwitchAdapter
    lateinit var activityContext: UserSwitchActivity
    private var isVideoFinished: Boolean = false
    private var timer: Timer? = null
    private var isAPICompleted: Boolean = false

    companion object {
        const val POOLING_INTERVAL_MS = 1000L
    }

    interface CallBackListener {
        fun onCallBack()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        signInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        SegmentUtils.trackSwitchUserScreenLoadStart()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_switch, container, false)
        setAdapterOnRv()
        SegmentUtils.trackSwitchUserScreenLoadEnd()

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        if (DataManager.instance.getGoalsExamsList().isEmpty()) {
            getGoalsExamsAsync()
        }
        updatedDataToAdapter(userSwitchAdapterAdapter)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener = (activity as CallBackListener)
    }

    private fun setAdapterOnRv() {
        userSwitchAdapterAdapter = UserSwitchAdapter(requireContext(), this)
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        if (DataManager.instance.getUserId().isEmpty())
            userSwitchAdapterAdapter.currentUserId = DataManager.instance.getUserId()
        binding.tvRecyclerView.layoutManager = layoutManager
        binding.tvRecyclerView.adapter = userSwitchAdapterAdapter
        userSwitchAdapterAdapter.onFocusChange = { linkedProfile, hasFocus ->
            if (hasFocus) {
                if (!linkedProfile.userType.equals("addUser")) {
                    if (!(linkedProfile.goalSupported && linkedProfile.examSupported)) {
                        var examName = DataManager.instance.getExamNameByCode(
                            linkedProfile.primary_goal_code!!,
                            linkedProfile.primary_exam_code
                        )
                        if (examName.isEmpty()) {
                            val currentUserGoal =
                                DataManager.instance.getGoalByGoalCode(linkedProfile.primary_goal_code!!)
                            examName = currentUserGoal?.name?:""
                        }
                        val errorMsg =
                            requireActivity().resources.getString(R.string.text_goal_not_supported) + " " + examName

                        binding.textErrorSelectionExam.text = errorMsg
                        binding.textErrorSelectionExam.visibility = View.VISIBLE
                    } else {
                        binding.textErrorSelectionExam.visibility = View.GONE
                    }
                } else {
                    binding.textErrorSelectionExam.visibility = View.GONE
                }
            } else {
                binding.textErrorSelectionExam.visibility = View.GONE

            }
        }

        userSwitchAdapterAdapter.onItemClick = { linkProfile ->
            startActivity(
                EditUserActivity.getEditUserIntent(
                    requireContext(), linkProfile.embibe_token,
                    Gson().toJson(linkProfile), AppConstants.USER_TYPE_CHILD
                )
            )
        }
    }

    private fun updatedDataToAdapter(adapterAdapter: UserSwitchAdapter) {
        try {
            val goalsSupportCheckList = arrayListOf<LinkedProfile>()
            for (linkProfile in getChildForGoalSet(UserData.getLinkedProfileList()!!)) {
                val currentUserGoal =
                    DataManager.instance.getGoalByGoalCode(linkProfile.primary_goal_code!!)
                if (currentUserGoal?.supported!!) {
                    linkProfile.goalSupported = true
                    if (linkProfile.primary_exam_code.isNullOrEmpty()) {
                        linkProfile.examSupported = false
                    } else {
                        val currentUserExam = DataManager.instance.getExamByGoalCodeExamCode(
                            linkProfile.primary_goal_code!!,
                            linkProfile.primary_exam_code!!
                        )
                        if (currentUserExam == null) {
                            linkProfile.examSupported = false
                        } else
                            linkProfile.examSupported = currentUserExam.supported
                    }

                } else {
                    linkProfile.goalSupported = false
                    linkProfile.examSupported = false
                }
                goalsSupportCheckList.add(linkProfile)
            }
            dataList = goalsSupportCheckList
            SegmentUtils.trackViewUsersEvents(dataList)
            val childList = getChildLinkedProfileList(dataList as List<LinkedProfile>)
            userSwitchAdapterAdapter.currentUserId = childList[0].userId
            adapterAdapter.updateChildData(childList)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun getChildForGoalSet(dataList: List<LinkedProfile?>): List<LinkedProfile> {
        val childLinkedProfileList: MutableList<LinkedProfile> = ArrayList()
        for (item in dataList) {
            if ((item!!.userType.equals("child") || item.userType.equals(
                    "student", true
                )) && childLinkedProfileList.size < 4
            )
                childLinkedProfileList.add(item)
        }
        return childLinkedProfileList
    }


    private fun getChildLinkedProfileList(dataList: List<LinkedProfile>): List<LinkedProfile> {
        val childLinkedProfileList: MutableList<LinkedProfile> = ArrayList()
        val dummyProfile = createDummyProfile()
        for (item in dataList) {
            if (checkUser(item) && childLinkedProfileList.size < 4)
                childLinkedProfileList.add(item)
        }
        if (childLinkedProfileList.size < 4)
            childLinkedProfileList.add(dummyProfile)
        return childLinkedProfileList
    }

    private fun checkUser(item: LinkedProfile?): Boolean {
        return (item!!.userType.equals("child") || item.userType.equals(
            "student",
            true
        ))
    }

    private fun createDummyProfile(): LinkedProfile {
        val linkedProfile = LinkedProfile()
        linkedProfile.userType = "addUser"
        linkedProfile.userId = ""
        linkedProfile.city = ""
        linkedProfile.email = ""
        linkedProfile.firstName = ""
        linkedProfile.gender = ""
        linkedProfile.goals = null
        linkedProfile.grade = ""
        linkedProfile.lastName = ""
        linkedProfile.packs = null
        linkedProfile.profilePic = ""
        linkedProfile.school = ""
        linkedProfile.board = ""
        return linkedProfile
    }

    override fun onClicked(type: String, data: Any) {
        val linkedProfile = data as LinkedProfile
        if (linkedProfile.userType.equals("addUser")) {
            SegmentUtils.trackAddUserClick()
            startActivity(Intent(requireContext(), AddUserActivity::class.java))
        } else {
            if (linkedProfile.examSupported && linkedProfile.goalSupported) {
                gotoHomeScreen(linkedProfile)
                SegmentUtils.trackProfileClick(linkedProfile.userId, dataList.indexOf(data))

            }
        }
    }

    private fun gotoHomeScreen(profile: LinkedProfile) {
        doAsync {
            DataManager.instance.saveChildEmbibeToken(profile.embibe_token)
            currentProfileId = UserData.getChildId()
            SegmentUtils.trackSelectUserEvents(profile.userId)
            UserData.setCurrentProfile(profile)
            UserData.setUserPrefExamWithGoal(
                profile.primary_goal_code!!,
                profile.primary_exam_code!!
            )
            uiThread {
/*                if (profile.userId == currentProfileId) {
                    gotoHomeActivity()
                } else {*/
                    getApiCall()
                //}
            }
        }

    }

    private fun getApiCall() {
        showFingerPrintVideo()
        getQuickLinksApiAsync()
        SegmentUtils.trackSWitchUserLoadStart()
        homeViewModel.homeApi(

            object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
                override fun onSuccess(model: List<ResultsEntity>?) {
                    if (model != null) {
                        doAsync {
                            homeViewModel.saveResultToDb(model, AppConstants.LEARN)
                            DataManager.instance.setHome(
                                model as ArrayList<ResultsEntity>,
                                object : BaseViewModel.DataCallback<ArrayList<ResultsEntity>> {
                                    override fun onSuccess(model: ArrayList<ResultsEntity>?) {
                                        makeLog("callback received")
                                        isAPICompleted = true
                                        if (isAPICompleted && isVideoFinished)
                                            gotoHomeActivity()
                                    }
                                })
                        }

                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    if (Utils.isApiFailed(code)) {
                        Utils.showError(
                            context,
                            code,
                            object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: String) {
                                    getApiCall()
                                }

                                override fun onDismiss() {

                                }
                            })
                    } else {
                        showToast(error)
                    }

                }
            })
    }

    private fun showFingerPrintVideo() {
        configureVideoView()
        binding.clVideoPlayer.visibility = View.VISIBLE
    }

    fun getQuickLinksApiAsync() {
        homeViewModel.homeQuickLinksApi(object : BaseViewModel.APICallBacks<List<ResultsEntity>> {
            override fun onSuccess(model: List<ResultsEntity>?) {
                if (model != null) {
                    doAsync {
                        homeViewModel.saveResultToDb(model, AppConstants.QUICK_LINKS)
                    }
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
            }
        })
    }


    private fun gotoHomeActivity() {
        try {
            isAPICompleted = false
            isVideoFinished = false
            val intent = Intent(activityContext, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            Handler().postDelayed({
                activityContext.finish()
            }, 1)
        } catch (e: Exception) {
            makeLog("Exception on gotoHomeActivity ${e.localizedMessage}")
        }
    }

    fun getGoalsExamsAsync() {
        signInViewModel.getGoalsExams(object : BaseViewModel.APICallBacks<GoalsExamsRes> {
            override fun onSuccess(model: GoalsExamsRes?) {
                if (model != null) {
                    DataManager.instance.setGoalsExamsList(
                        (model.data ?: arrayListOf())
                    )
                    updatedDataToAdapter(userSwitchAdapterAdapter)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {

            }
        })
    }


    private fun configureVideoView() {
        val uriPath =
            "android.resource://" + BuildConfig.APPLICATION_ID + "/" + R.raw.embibe_animation_touch
        binding.videoView.setVideoPath(uriPath)
        binding.videoView.setOnInfoListener(object : MediaPlayer.OnInfoListener {
            override fun onInfo(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    // video started; hide the placeholder.
                    binding.placeholder.visibility = View.GONE
                    return true
                }
                return false
            }
        })
        binding.videoView.start()
        initVideoTimer(8000)
    }

    private fun initVideoTimer(stopAtMsec: Int) {
        cancelTimer()
        timer = Timer()
        timer!!.schedule(object : TimerTask() {
            override fun run() {
                binding.videoView.post {
                    if (binding.videoView.currentPosition >= stopAtMsec) {
                        binding.videoView.pause()
                        cancelTimer()
                        isVideoFinished = true
                        //player!!.pause()
                        if (isAPICompleted)
                            gotoHomeActivity()
                    }

                }
            }
        }, 0, POOLING_INTERVAL_MS)
    }

    private fun cancelTimer() {
        timer?.cancel()
        timer = null
    }
}