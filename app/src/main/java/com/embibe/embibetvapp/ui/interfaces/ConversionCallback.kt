package com.embibe.embibetvapp.ui.interfaces

/**
 * Created by Arpit Johri on 07-04-2020.
 */
interface ConversionCallback {

    fun onReady()
    fun onSuccess(result: ArrayList<String>?)
    fun onCompletion()
    fun onErrorOccurred(errorMessage: String)

} 