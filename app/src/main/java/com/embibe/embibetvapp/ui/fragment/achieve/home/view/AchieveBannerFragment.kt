package com.embibe.embibetvapp.ui.fragment.achieve.home.view

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.annotation.SuppressLint
import android.graphics.drawable.Animatable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.ProgressBar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.databinding.FragmentAchieveHomeHeroBannerBinding
import com.embibe.embibetvapp.model.SubjectReadiness
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.achieve.home.*
import com.embibe.embibetvapp.ui.fragment.achieve.ReadinessFragment
import com.embibe.embibetvapp.ui.fragment.achieve.home.viewmodel.AchieveHomeHeroBannerViewModel
import com.embibe.embibetvapp.ui.fragment.achieve.home.viewmodel.AchieveHomeViewModel
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.utils.Utils
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo
import kotlinx.android.synthetic.main.item_discover_yourself.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Suppress("DUPLICATE_LABEL_IN_WHEN")
class AchieveBannerFragment : BaseAppFragment(), DPadKeysListener {

    private lateinit var viewModel: AchieveHomeHeroBannerViewModel
    private lateinit var homeviewModel: AchieveHomeViewModel
    private var dPadKeysCallback: DPadKeysListener? = null
    private lateinit var navigationMenuCallback: NavigationMenuCallback
    private var listSubject: ArrayList<SubjectReadiness> = ArrayList()
    private val circularProgressDrawable = ArrayList<Int>()
    private val predictedProgressBars = ArrayList<ProgressBar>()
    private val projectedProgressBars = ArrayList<ProgressBar>()
    private var gradeReadiness: GradeReadiness? = null
    private var successReadiness: SuccessReadiness? = null
    private var holisticReadiness: HolisticReadiness? = null
    private lateinit var binding: FragmentAchieveHomeHeroBannerBinding
    private lateinit var coroutineScope: CoroutineScope
    private val userGrade = UserData.getGrade()
    private val userGoal = UserData.getGoal()
    private var isFutureSuccessActive = false
    private var TAG = this.javaClass.name
    private var currentState: String = "grade_readiness"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coroutineScope = CoroutineScope(Dispatchers.Main)
        homeviewModel = ViewModelProvider(this).get(AchieveHomeViewModel::class.java)
        viewModel = ViewModelProvider(this).get(AchieveHomeHeroBannerViewModel::class.java)
        loadData()
    }

    fun loadData() {
        homeviewModel.achieveHomeResponse.forEach {
            when (it.section_id) {
                1 -> {
                    for (content in it.content) {
                        if (content.type == "readiness") {
                            successReadiness =
                                SuccessReadiness(content.exam, content.potential, content.predicted)
                        } else if (content.type == "future_success") {
                            gradeReadiness =
                                GradeReadiness(content.exam, content.potential, content.predicted)
                        }
                        /*  holisticReadiness = it.data?.holisticReadiness*/
                    }

                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_achieve_home_hero_banner,
            container,
            false
        )

        loadDataToView()
        initView()
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        navigationMenuCallback.navMenuToggle(false)
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    fun setDPadKeysListener(callback: DPadKeysListener) {
        this.dPadKeysCallback = callback
    }

    private fun fillDrawables() {
        circularProgressDrawable.add(R.drawable.determinate_ciricular_blue_thicker)
        circularProgressDrawable.add(R.drawable.determinate_ciricular_red_thicker)
        circularProgressDrawable.add(R.drawable.determinate_ciricular_purple_thicker)
        circularProgressDrawable.add(R.drawable.determinate_ciricular_orange_thicker)
    }

    private fun setPredictedProjectedProgressBars() {
        predictedProgressBars.add(binding.predictedGrade.layoutReadiness.progressBarBlue)
        predictedProgressBars.add(binding.predictedGrade.layoutReadiness.progressBarRed)
        predictedProgressBars.add(binding.predictedGrade.layoutReadiness.progressBarPurple)
        predictedProgressBars.add(binding.predictedGrade.layoutReadiness.progressBarOrange)

        projectedProgressBars.add(binding.projectedGrade.layoutReadiness.progressBarBlue)
        projectedProgressBars.add(binding.projectedGrade.layoutReadiness.progressBarRed)
        projectedProgressBars.add(binding.projectedGrade.layoutReadiness.progressBarPurple)
        projectedProgressBars.add(binding.projectedGrade.layoutReadiness.progressBarOrange)
    }

    private fun setDataToBottomRv() {
        listSubject.clear()
        Log.i(TAG, gradeReadiness?.predicted.toString())
    }

    private fun setNullDataToBottomRv() {
        listSubject.clear()
        listSubject.add(
            SubjectReadiness(
                null,
                R.drawable.determinate_ciricular_orange,
                "",
                null
            )
        )
    }

    private fun loadDataToView() {
        setReadinessLabels()
        if (gradeReadiness?.predicted?.grade != null) {
            fillDrawables()
            setPredictedProjectedProgressBars()
            setDataToBottomRv()
        } else {
            setNullDataToBottomRv()
        }
    }

    private fun setReadinessLabels() {
        val yourReadinessTranslation =
            getString(R.string.learning_journey_for, getString(R.string.this_class), userGrade)
        val classTranslation = getString(R.string.this_class)
//        binding.tvHeroBannerHeading.text = yourReadinessTranslation
    }


    private fun initView() {
        playWebPAnimationWithFresco(binding.ivParticles, R.drawable.bg_particle_3, false)
        setListeners()
        setFocusToLastItem()
        setDataToUI()
        coroutineScope.launch {
            delay(500)
            binding.ivParticles.visibility = View.VISIBLE
        }
    }

    private fun setListeners() {
//        binding.layoutRightCta.clDiscoverWhereYouStand.setOnKeyListener { v, keyCode, event ->
//            if (event.action == KeyEvent.ACTION_DOWN) {
//                when (event.keyCode) {
//                    KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
//                        isFutureSuccessActive = true
//                        changeItems()
//                        setFocusToLastItem()
//                    }
//                    KeyEvent.KEYCODE_DPAD_LEFT -> {
//                        dPadKeysCallback?.isKeyPadLeft(true, "ReadinessFragment")
//                    }
//                }
//            }
//            false
//        }


        binding.layoutRightCta.clDiscoverWhereYouStand.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {

            } else {

            }
        }
        binding.layoutLeftCta.clDiscoverWhereYouStand.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {

            } else {

            }
        }

        binding.layoutRightCta.clDiscoverWhereYouStand.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!Utils.isKeyPressedTooFast(500))
                            dPadKeysCallback?.isKeyPadDown(true, "Banner")
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        setFocusToFirstItem()
                        when (currentState) {
                            "grade_readiness" -> {
                                navigationMenuCallback.navMenuToggle(true)
                            }
                            "success_readiness" -> {
                            }
                            "holistic_readiness" -> {
                            }
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        setFocusToLastItem()
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        if (!Utils.isKeyPressedTooFast(1000))
                            dPadKeysCallback?.isKeyPadUp(true, "Banner")
                    }
                    KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                        setFocusToFirstItem()
                        when (currentState) {
                            "grade_readiness" -> {
                                currentState = "success_readiness"
                                setReadinessData()
                            }
                            "success_readiness" -> {
                                currentState = "holistic_readiness"
                                setReadinessData()
                            }
                            "holistic_readiness" -> {
                            }
                        }
                    }
                }
            }
            false
        }
        binding.layoutLeftCta.clDiscoverWhereYouStand.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!Utils.isKeyPressedTooFast(500))
                            dPadKeysCallback?.isKeyPadDown(true, "Banner")
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        setFocusToFirstItem()
                        when (currentState) {
                            "grade_readiness" -> {
                                navigationMenuCallback.navMenuToggle(true)
                            }
                            "success_readiness" -> {
                            }
                            "holistic_readiness" -> {
                            }
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        when (currentState) {
                            "grade_readiness" -> {
                                setFocusToLastItem()
                            }
                            "success_readiness" -> {
                                setFocusToFirstItem()
                                currentState = "grade_readiness"
                                setReadinessData()
                            }
                            "holistic_readiness" -> {
                                setFocusToFirstItem()
                                currentState = "success_readiness"
                                setReadinessData()
                            }
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {

                        setFocusToLastItem()
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        if (!Utils.isKeyPressedTooFast(1000))
                            dPadKeysCallback?.isKeyPadUp(true, "Banner")
                    }
                }
            }
            false
        }
    }


    private fun View.goneVisible(boolean: Boolean) {
        if (boolean)
            this.visibility = View.GONE
        else
            this.visibility = View.VISIBLE
    }

    @SuppressLint("SetTextI18n")
    private fun setDataToUI() {

        Utils.loadLottieAnimation(binding.layoutRightCta.lvTrophyGlow, R.raw.trophy_glow_2)
        Utils.loadLottieAnimation(binding.layoutLeftCta.lvTrophyGlow, R.raw.trophy_glow_2)
        Utils.loadLottieAnimation(binding.predictedGrade.lvCircularSphere, R.raw.circle_rays_two)
        Utils.loadLottieAnimation(binding.projectedGrade.lvCircularSphere, R.raw.circle_rays_two)
        binding.projectedGrade.clItemReadiness.scale()
        binding.predictedGrade.clItemReadiness.scale()
        setFocusToLastItem()
        setReadinessData()

    }

    private fun setReadinessData() {
        when (currentState) {
            "grade_readiness" -> {
                binding.layoutLeftCta.clDiscoverWhereYouStand.visibility = View.INVISIBLE
                binding.layoutRightCta.clDiscoverWhereYouStand.visibility = View.VISIBLE
                binding.layoutRightCta.clDiscoverWhereYouStand.tv_discover_text.text =
                    getString(R.string.life_success_readiness)
                binding.tvYourLifeSuccess.text = getString(R.string.learning_journey_for_grade_10)
                setPredictedGrade(gradeReadiness?.predicted, gradeReadiness?.exam)
                setPotentialGrade(gradeReadiness?.potential, gradeReadiness?.exam)
            }
            "success_readiness" -> {
                binding.layoutLeftCta.clDiscoverWhereYouStand.visibility = View.VISIBLE
                binding.layoutRightCta.clDiscoverWhereYouStand.visibility = View.VISIBLE
                binding.tvYourLifeSuccess.text = getString(R.string.life_success_readiness)
                binding.layoutLeftCta.clDiscoverWhereYouStand.tv_discover_text.text =
                    getString(R.string.grade_10_readiness)
                binding.layoutRightCta.clDiscoverWhereYouStand.tv_discover_text.text =
                    getString(R.string.holistic_readiness)
                setPredictedGrade(successReadiness?.predicted, successReadiness?.exam)
                setPotentialGrade(successReadiness?.potential, successReadiness?.exam)
            }
            "holistic_readiness" -> {
                binding.layoutLeftCta.clDiscoverWhereYouStand.visibility = View.VISIBLE
                binding.layoutRightCta.clDiscoverWhereYouStand.visibility = View.INVISIBLE
                binding.tvYourLifeSuccess.text = getString(R.string.holistic_readiness)
                binding.layoutLeftCta.clDiscoverWhereYouStand.tv_discover_text.text =
                    getString(R.string.life_success_readiness)
                setPredictedGrade(holisticReadiness?.predicted, successReadiness?.exam)
                setPotentialGrade(holisticReadiness?.potential, successReadiness?.exam)
            }
        }

        if (!binding.layoutLeftCta.clDiscoverWhereYouStand.isFocused || !binding.layoutRightCta.clDiscoverWhereYouStand.isFocused) {
            if (binding.layoutLeftCta.clDiscoverWhereYouStand.visibility == View.VISIBLE) {
                binding.layoutLeftCta.clDiscoverWhereYouStand.requestFocus()
            } else {
                binding.layoutRightCta.clDiscoverWhereYouStand.requestFocus()
            }
        }
    }

    private fun setPotentialGrade(potential: Potential?, exam: String?) {
        if (!potential?.grade.isNullOrEmpty()) {
            binding.projectedGrade.layoutReadiness.tvGrade.text = checkGrade(potential?.grade)
            binding.projectedGrade.tvLayoutA.text =
                getString(R.string.your_potential_grade_for) + " " + exam.toString()
            if (potential?.subjects != null) {
                for (i in potential.subjects) {
                    if (i.progress != null) {
                        when (i.subject.toLowerCase()) {
                            "maths", "mathematics" -> {
                                binding.projectedGrade.layoutReadiness.progressBarPurple.progress =
                                    i.progress
                            }
                            "chemistry" -> {
                                binding.projectedGrade.layoutReadiness.progressBarOrange.progress =
                                    i.progress
                            }
                            "physics" -> {
                                binding.projectedGrade.layoutReadiness.progressBarBlue.progress =
                                    i.progress
                            }
                            "biology" -> {
                                binding.projectedGrade.layoutReadiness.progressBarRed.progress =
                                    i.progress
                            }
                        }
                    }
                }
            }
        } else {
            binding.projectedGrade.layoutReadiness.tvGrade.text = "?"
            if (Utils.getCurrentLocale() == "en") {
                binding.projectedGrade.tvLayoutA.text =
                    getString(R.string.your_potential_grade_for) + " " + userGoal + " ${getString(R.string.this_class)} " + userGrade
            } else if (Utils.getCurrentLocale() == "hi") {
                binding.projectedGrade.tvLayoutA.text =
                    getString(R.string.your_potential_grade_for) + " " + userGoal + " " + userGrade + getString(
                        R.string.this_class
                    )
            }
        }
    }

    private fun setPredictedGrade(predicted: Predicted?, exam: String?) {
        if (!predicted?.grade.isNullOrEmpty()) {
            binding.predictedGrade.layoutReadiness.tvGrade.text = checkGrade(predicted?.grade)
            binding.predictedGrade.tvLayoutA.text =
                getString(R.string.your_predicted_grade_for) + " " + exam
            if (predicted?.subjects != null) {
                for (i in predicted.subjects) {
                    when (i.subject.toLowerCase()) {
                        "maths", "mathematics" -> {
                            binding.predictedGrade.layoutReadiness.progressBarPurple.progress =
                                i.progress
                        }
                        "chemistry" -> {
                            binding.predictedGrade.layoutReadiness.progressBarOrange.progress =
                                i.progress
                        }
                        "physics" -> {
                            binding.predictedGrade.layoutReadiness.progressBarBlue.progress =
                                i.progress
                        }
                        "biology" -> {
                            binding.predictedGrade.layoutReadiness.progressBarRed.progress =
                                i.progress
                        }
                    }
                }
            }

        } else {
            binding.predictedGrade.layoutReadiness.tvGrade.text = "?"
            if (Utils.getCurrentLocale() == "en") {
                binding.predictedGrade.tvLayoutA.text =
                    getString(R.string.your_predicted_grade_for) + " " + userGoal + " ${getString(R.string.this_class)} " + userGrade
            } else if (Utils.getCurrentLocale() == "hi") {
                binding.predictedGrade.tvLayoutA.text =
                    getString(R.string.your_predicted_grade_for) + " " + userGoal + " " + userGrade + getString(
                        R.string.this_class
                    )
            }
        }
    }

    private fun View.scale() {
        val scaleX = PropertyValuesHolder.ofFloat(View.SCALE_X, 1f)
        val scaleY = PropertyValuesHolder.ofFloat(View.SCALE_Y, 1f)
        val animator = ObjectAnimator.ofPropertyValuesHolder(this, scaleX, scaleY)
        animator.interpolator = DecelerateInterpolator()
        animator.duration =
            ReadinessFragment.AnimTranslate.DURATION
        animator.start()
    }

    fun setFocusToLastItem() {
        coroutineScope.launch {
            delay(1000)
            binding.layoutRightCta.clDiscoverWhereYouStand.requestFocus()
            binding.layoutRightCta.clDiscoverWhereYouStand.scale()
        }
    }

    fun setFocusToFirstItem() {
        coroutineScope.launch {
            delay(1000)
            binding.layoutLeftCta.clDiscoverWhereYouStand.requestFocus()
            binding.layoutLeftCta.clDiscoverWhereYouStand.scale()
        }
    }

    private fun checkGrade(grade: String?): String? {
        return if (grade!!.isEmpty())
            ""
        else
            grade
    }

    private fun playWebPAnimationWithFresco(
        view: SimpleDraweeView,
        resource: Int,
        setController: Boolean
    ) {
        val controller = Fresco.newDraweeControllerBuilder()
        if (setController) {
            controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                override fun onFinalImageSet(
                    id: String?,
                    imageInfo: ImageInfo?,
                    animatable: Animatable?
                ) {
                    val anim = animatable as AnimatedDrawable2
                    anim.setAnimationListener(object : AnimationListener {
                        override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                        override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                        }

                        override fun onAnimationFrame(
                            drawable: AnimatedDrawable2?,
                            frameNumber: Int
                        ) {
                        }

                        override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                        }

                        override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                    })
                }
            }
        }
        controller.autoPlayAnimations = true
        val res = "res:/" + resource
        controller.setUri(Uri.parse(res))
        view.controller = controller.build()
    }

    override fun isKeyPadDown(isDown: Boolean, from: String) {
    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {
    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {
    }
}