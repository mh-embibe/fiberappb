package com.embibe.embibetvapp.ui.fragment.videoPlayer

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.databinding.FragmentExoPlayerBinding
import com.embibe.embibetvapp.ui.interfaces.PlayerToControlListener
import com.embibe.embibetvapp.utils.Utils
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.audio.AudioListener
import com.google.android.exoplayer2.audio.AuxEffectInfo
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.extractor.ExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.EventLogger

class ExoPlayerFragment : Fragment(), Player.AudioComponent {

//        var videoUrl = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"

    private lateinit var binding: FragmentExoPlayerBinding
    private lateinit var playerToControlListener: PlayerToControlListener
    private lateinit var player: SimpleExoPlayer
    private lateinit var videoUrl: String
    private val TAG = this.javaClass.simpleName
    private val mappingTrackSelector: DefaultTrackSelector? = null
    private var paused: Boolean = false
    var continueWatchingDuration: Long = 0

    var totalDuration: Long = 0
    var currentDuration: Long = 0
    var playing: Boolean = false
    var buffering: Boolean = false
    var isEnd: Boolean = false
    var isPreparing: Boolean = false
    var isVideoReady: Boolean = false
    var currentPlayState = ""
    var currentNetworkState = ""
    var mInitialized = false
    var isBackGround = false

    companion object {
        const val REWIND_FORWARD_TIME: Long = 10000
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_exo_player, container, false)

        val bundle = this.arguments
        if (bundle != null) {
            videoUrl = bundle.getString(AppConstants.VIDEO_URL, "")
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPlayer()
        addListener()
    }

    private fun initPlayer() {
        playerToControlListener.videoLoadingStart(0, "")
        val bandwidthMeter: BandwidthMeter = DefaultBandwidthMeter()
        val trackSelector: TrackSelector =
            DefaultTrackSelector(AdaptiveTrackSelection.Factory(bandwidthMeter))
        player = ExoPlayerFactory.newSimpleInstance(requireContext(), trackSelector)
        player.addAnalyticsListener(EventLogger(mappingTrackSelector))
        val videoUri = Uri.parse(videoUrl)
        val dataSourceFactory = DefaultHttpDataSourceFactory("exoplayer-codelab")
        val extractorsFactory: ExtractorsFactory = DefaultExtractorsFactory()
        val mediaSource: MediaSource =
            ExtractorMediaSource(videoUri, dataSourceFactory, extractorsFactory, null, null)

        binding.exoPlayer.player = player
        player.prepare(mediaSource)
        player.volume = 100f
        isPreparing = true
        player.prepare(mediaSource, true, false)

        if (continueWatchingDuration > 0) {
            player.seekTo(continueWatchingDuration * 1000)
        } else {
            player.seekTo(0)
        }
        if (!Utils.practiceDialogFragment.isVisible)
            player.playWhenReady = true

    }

    private fun isPlaying() = playing
    private fun isPaused() = paused
    private fun isBuffering() = buffering
    private fun videoEnd() = isEnd
    private fun seekTo(second: Long) = player.seekTo(second)
    fun getCurrentPosition() = currentPostion()

    private fun currentPostion(): Long {
        return if (::player.isInitialized) (player.currentPosition / 1000) else 0
    }

    fun getDuration() = player.duration / 1000
    fun pause() {
        player.playWhenReady = false
    }

    fun play() {
        player.playWhenReady = true
    }

    fun playPause() {
        if (isPlaying()) {
            pause()
            playerToControlListener.isPlaying(false)
            playerToControlListener.videoPlayPauseClick(
                player.currentPosition / 1000,
                "full screen",
                "pause",
                "",
                ""
            )

        } else {
            play()
            playerToControlListener.isPlaying(true)
            playerToControlListener.videoPlayPauseClick(
                player.currentPosition / 1000,
                "full screen",
                "play",
                "",
                ""
            )

        }
    }

    fun seekForward10Second() {
        if (mInitialized) {
            if (player.currentPosition + REWIND_FORWARD_TIME < totalDuration) {
                playerToControlListener.videoSeek(
                    player.currentPosition / 1000,
                    player.currentPosition / 1000,
                    player.currentPosition + REWIND_FORWARD_TIME,
                    "button"
                )

                seekTo(player.currentPosition + REWIND_FORWARD_TIME)

            } else {
                seekTo(player.duration)
            }

            play()
            playerToControlListener.isForwarded(true)
            playerToControlListener.isPlaying(true)
        }
    }

    fun seekRewind10Second() {
        if (mInitialized) {
            if (binding.exoPlayer.player!!.currentPosition - REWIND_FORWARD_TIME > 0) {
                playerToControlListener.videoSeek(
                    player.currentPosition / 1000,
                    player.currentPosition / 1000,
                    player.currentPosition - REWIND_FORWARD_TIME,
                    "button"
                )

                seekTo(player.currentPosition - REWIND_FORWARD_TIME)
            } else {
                seekTo(0)
            }

            play()
            playerToControlListener.isRewinded(true)
            playerToControlListener.isPlaying(true)
        }
    }


    fun seekRewindByPercentage(timeInSeconds: Int) {
        var timeInMilliSeconds = (timeInSeconds * 1000).toLong()
        if (mInitialized) {
            if (timeInMilliSeconds > 0) {
                playerToControlListener.videoSeek(
                    player.currentPosition / 1000,
                    player.currentPosition / 1000,
                    timeInMilliSeconds / 1000,
                    "button"
                )

                seekTo(timeInMilliSeconds)
            } else {
                seekTo(0)
            }

            play()
            playerToControlListener.isRewinded(true)
            playerToControlListener.isPlaying(true)
        }
    }

    fun seekForwardByPercentage(timeInSeconds: Int) {
        var timeInMilliSeconds: Long = (timeInSeconds * 1000).toLong()
        if (mInitialized) {
            if (timeInMilliSeconds < totalDuration) {
                playerToControlListener.videoSeek(
                    player.currentPosition / 1000,
                    player.currentPosition / 1000,
                    (timeInMilliSeconds / 1000),
                    "button"
                )

                seekTo(timeInMilliSeconds)

            } else {
                seekTo(player.duration)
            }

            play()
            playerToControlListener.isForwarded(true)
            playerToControlListener.isPlaying(true)
        }
    }

    fun replayVideo() {
        seekTo(0)
        play()
        isPreparing = true
    }

    private fun addListener() {
        player.addListener(object : Player.EventListener {
            override fun onTimelineChanged(timeline: Timeline, reason: Int) {}
            override fun onTimelineChanged(
                timeline: Timeline,
                manifest: Any?,
                reason: Int
            ) {
            }

            override fun onTracksChanged(
                trackGroups: TrackGroupArray,
                trackSelections: TrackSelectionArray
            ) {
            }

            override fun onLoadingChanged(isLoading: Boolean) {

            }

            override fun onPlayerStateChanged(
                playWhenReady: Boolean,
                playbackState: Int
            ) {
                when (playbackState) {
                    Player.STATE_BUFFERING -> {
                        buffering = true
                        currentPlayState = "buffer"
                        playerToControlListener.getPlayerState(currentPlayState)
                        playerToControlListener.videoBufferStart(
                            player.currentPosition / 1000,
                            "", "", ""
                        )
                    }
                    Player.STATE_IDLE -> {

                    }
                    Player.STATE_READY -> {
                        playerToControlListener.videoLoadingEnd(
                            ""
                        )
                        playerToControlListener.videoBufferEnd(
                            player.currentPosition / 1000,
                            "", ""
                        )
                        totalDuration = player.duration
                        isVideoReady = true
                        mInitialized = true
                        buffering = false
                        currentPlayState = if (playWhenReady) "play" else "pause"
                        playerToControlListener.getPlayerState(currentPlayState)
                    }
                    Player.STATE_ENDED -> {
                        isEnd = true
                        currentPlayState = "end"
                        playerToControlListener.onVideoEnd(isEnd)
                        playerToControlListener.getPlayerState(currentPlayState)
                    }
                }

                if (isPreparing && playbackState == Player.STATE_READY) {
                    isPreparing = false
                    playerToControlListener.onVideoReady(true)
                }
            }

            override fun onPlaybackSuppressionReasonChanged(playbackSuppressionReason: Int) {}
            override fun onIsPlayingChanged(isPlaying: Boolean) {
                playing = isPlaying
            }

            override fun onRepeatModeChanged(repeatMode: Int) {}
            override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}
            override fun onPlayerError(error: ExoPlaybackException) {
                currentPlayState = "error"
                playerToControlListener.videoPlaybackError(
                    player.currentPosition / 1000,
                    error.message!!
                )
                Log.d(TAG, "onPlayerError: " + error.cause!!.message)
            }

            override fun onPositionDiscontinuity(reason: Int) {}
            override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {}
            override fun onSeekProcessed() {}
        })
    }

    override fun onStop() {
        super.onStop()
        playerToControlListener.videoExit(
            player.currentPosition / 1000,
            currentPlayState,
            "connected"
        )
        binding.exoPlayer.player = null
        player.release()

    }

    fun setPlayerToControllerListener(callback: PlayerToControlListener) {
        this.playerToControlListener = callback
    }

    override fun addAudioListener(listener: AudioListener) {
    }

    override fun clearAuxEffectInfo() {
    }

    override fun getAudioAttributes(): AudioAttributes {
        return player.audioAttributes
    }

    override fun setVolume(audioVolume: Float) {
    }

    override fun getAudioSessionId(): Int {
        return 0
    }

    override fun setAuxEffectInfo(auxEffectInfo: AuxEffectInfo) {
    }

    override fun setAudioAttributes(audioAttributes: AudioAttributes) {
    }

    override fun setAudioAttributes(audioAttributes: AudioAttributes, handleAudioFocus: Boolean) {
    }

    override fun removeAudioListener(listener: AudioListener) {
    }

    override fun getVolume(): Float {
        playerToControlListener.videoVolume(
            player.currentPosition / 1000,
            player.volume.toInt(),
            currentPlayState,
            ""
        )
        return player.volume
    }

    override fun onPause() {
        super.onPause()
        currentDuration = player.currentPosition / 1000
        isBackGround = true
//        PreferenceHelper().put(AppConstants.RESUME_DURATION, player.currentPosition/1000)
//        PreferenceHelper().put(AppConstants.HOME_BUTTON, true)
        binding.exoPlayer.player = null
        player.release()
    }

    override fun onResume() {
        super.onResume()
        if (isBackGround && !isEnd) {
            continueWatchingDuration = currentDuration
            initPlayer()
            addListener()
            isBackGround = false
        }
    }
}


