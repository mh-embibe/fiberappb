package com.embibe.embibetvapp.ui.activity

import android.os.Bundle
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.base.BaseFragmentActivity
import com.embibe.embibetvapp.constant.AppConstants
import com.embibe.embibetvapp.ui.interfaces.ErrorScreensBtnClickListener
import com.embibe.embibetvapp.utils.Utils

class AddGoalsActivity : BaseFragmentActivity(), ErrorScreensBtnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_goals)
    }

    override fun onBackPressed() {
        Utils.showAlertDialog(AppConstants.APP_CLOSE_DIALOG, supportFragmentManager, this)
    }

    override fun screenUpdate(type: String) {
        when (type) {
            AppConstants.LEAVE_APP -> {
                finish()
            }
        }
    }


}