package com.embibe.embibetvapp.ui.fragment.achieve

import android.animation.*
import android.graphics.PointF
import android.graphics.Typeface
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.get
import androidx.core.view.updateLayoutParams
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import com.airbnb.lottie.LottieAnimationView
import com.airbnb.lottie.LottieDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.integration.webp.decoder.WebpDrawable
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.embibe.embibetvapp.R
import com.embibe.embibetvapp.application.App
import com.embibe.embibetvapp.base.BaseAppFragment
import com.embibe.embibetvapp.base.BaseViewModel
import com.embibe.embibetvapp.databinding.FragmentDiscoverKnowledge2Binding
import com.embibe.embibetvapp.model.UserData
import com.embibe.embibetvapp.model.achieve.*
import com.embibe.embibetvapp.model.achieve.futureSuccess.FutureSuccessRes
import com.embibe.embibetvapp.model.achieve.home.AchieveHome
import com.embibe.embibetvapp.model.achieve.keyconcepts.KeyConceptModel
import com.embibe.embibetvapp.model.achieve.readiness.Readiness
import com.embibe.embibetvapp.ui.custom.ArcView
import com.embibe.embibetvapp.ui.custom.LineView
import com.embibe.embibetvapp.ui.interfaces.DPadKeysListener
import com.embibe.embibetvapp.ui.interfaces.NavigationMenuCallback
import com.embibe.embibetvapp.ui.interfaces.PointsListener
import com.embibe.embibetvapp.ui.viewmodel.AchieveViewModel
import com.embibe.embibetvapp.ui.viewmodel.HomeViewModel
import com.embibe.embibetvapp.utils.Utils
import com.embibe.embibetvapp.utils.data.DataManager
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.fresco.animation.drawable.AnimatedDrawable2
import com.facebook.fresco.animation.drawable.AnimationListener
import com.facebook.imagepipeline.image.ImageInfo
import com.github.penfeizhou.animation.apng.APNGDrawable
import com.github.penfeizhou.animation.loader.ResourceStreamLoader
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.collections.set
import kotlin.math.abs
import kotlin.math.roundToInt

class DiscoverFragment2 : BaseAppFragment(), DPadKeysListener, PointsListener {

    private val MAX_ARCS_ANIM_DURATION = 800L
    private val MAX_ARCS_COUNT = 15
    private var isAchieverShown: Boolean = false
    private lateinit var achieveViewModel: AchieveViewModel
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: FragmentDiscoverKnowledge2Binding
    private val classTag = DiscoverFragment2::class.java.simpleName
    private lateinit var requestOptions: RequestOptions
    private var isEnterClicked = false
    private var isReadinessDataAvailable = false
    private var isFutureSuccessDataAvailable = false
    private val trianglesValuesMap = HashMap<ImageView, TriangleValue>()
    private val circleValuesMap = HashMap<ConstraintLayout, PointF>()
    private val linesMap = HashMap<LineView, LinePoint>()
    private val sphereSelectionLinePointsMap = HashMap<String, Pair<PointF, PointF>>()
    private val spheresPositionsMap = HashMap<ConstraintLayout, PointF>()
    private val sphereCircleMap = HashMap<ConstraintLayout, ConstraintLayout>()
    private val sphereTriangleMap = HashMap<ConstraintLayout, ImageView>()
    private var keyConceptPointsList = ArrayList<Pair<ImageButton, TextView>>()
    private var keyConceptsList = ArrayList<KeyConceptModel>()
    private var arcsList = ArrayList<ArcView>()
    private var percentageConnectionsList = ArrayList<PercentageData>()
    private val currentGrade = UserData.getGrade()
    private val currentGoal = UserData.getGoal()
    private lateinit var navigationMenuCallback: NavigationMenuCallback
    private lateinit var currentSelectedAnimation: Animation
    private var isReadinessActive = false
    private var isSphereSelected = false
    private var whichSphereClicked = ""
    private lateinit var selectedLine: ArcView
    private var currentFocusedConcept: String = ""
    private val sphereClickDelayInterval = 2500
    private val pointFadeInOutDuration = 700
    private val achieversFragment: AchieversFragment by lazy {
        AchieversFragment()
    }

    companion object {
        var readinessFragment: ReadinessFragment = ReadinessFragment()
    }

    private val minSize = 100
    private val maxSize = 200

    private val SPHERE_SUBTLE_GLOW_INDEX = 0
    private val SPHERE_GLOW_INDEX = 1
    private val SPHERE_TEXT_INDEX = 2

    lateinit var centerSphere: ConstraintLayout
    lateinit var sphere9: ConstraintLayout
    private lateinit var sphereRightSelected: ConstraintLayout
    private lateinit var sphereLeftSelected: ConstraintLayout
    lateinit var sphere8: ConstraintLayout
    lateinit var sphere7: ConstraintLayout
    lateinit var sphere6: ConstraintLayout
    lateinit var sphere11: ConstraintLayout
    lateinit var sphere12: ConstraintLayout
    lateinit var sphereNEET: ConstraintLayout
    lateinit var sphereJEE: ConstraintLayout

    lateinit var circle6: ConstraintLayout
    lateinit var circle7: ConstraintLayout
    lateinit var circle8: ConstraintLayout
    lateinit var circle9: ConstraintLayout
    lateinit var circle11: ConstraintLayout
    lateinit var circle12: ConstraintLayout
    lateinit var circleNEET: ConstraintLayout
    lateinit var circleJEE: ConstraintLayout

    lateinit var triangle6: ImageView
    lateinit var triangle7: ImageView
    lateinit var triangle8: ImageView
    lateinit var triangle9: ImageView
    lateinit var triangle11: ImageView
    lateinit var triangle12: ImageView
    lateinit var triangleNEET: ImageView
    lateinit var triangleJEE: ImageView

    private var coroutineScope: CoroutineScope? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coroutineScope = CoroutineScope(Dispatchers.Main)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_discover_knowledge_2, container,
            false
        )
        achieveViewModel = ViewModelProviders.of(this).get(AchieveViewModel::class.java)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        navigationMenuCallback.navMenuToggle(false)
        initSpheres()
        initCircles()
        initTriangles()
//        loadAchieveHome()
        //getAchieveApis()
        return binding.root
    }

    private fun initCircles() {
        circle6 = generateCircle("Circle6")
        circle7 = generateCircle("Circle7")
        circle8 = generateCircle("Circle8")
        circle9 = generateCircle("Circle9")
        circle11 = generateCircle("Circle11")
        circle12 = generateCircle("Circle12")
        circleNEET = generateCircle("CircleNEET")
        circleJEE = generateCircle("CircleJEE")
    }

    private fun initTriangles() {
        triangle6 = generateTriangle("Triangle6")
        triangle7 = generateTriangle("Triangle7")
        triangle8 = generateTriangle("Triangle8")
        triangle9 = generateTriangle("Triangle9")
        triangle11 = generateTriangle("Triangle11")
        triangle12 = generateTriangle("Triangle12")
        triangleNEET = generateTriangle("TriangleNEET")
        triangleJEE = generateTriangle("TriangleJEE")
    }

    private fun initSpheres() {
        centerSphere = binding.layoutSphereCenter.clCenterSphere
        sphere9 = binding.layoutSphere9.clSphere9
        sphereRightSelected = binding.layoutSphereSelected9.clSphere9
        sphereLeftSelected = binding.layoutSphereSelectedNEET.clSphereNEET
        sphereRightSelected.visibility = GONE
        sphereLeftSelected.visibility = GONE
        sphere8 = binding.layoutSphere8.clSphere8
        sphere7 = binding.layoutSphere7.clSphere7
        sphere6 = binding.layoutSphere6.clSphere6
        sphere11 = binding.layoutSphere11.clSphere11
        sphere12 = binding.layoutSphere12.clSphere12
        sphereNEET = binding.layoutSphereNEET.clSphereNEET
        sphereJEE = binding.layoutSphereJEE.clSphereJEE
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentReplacer(binding.flAchievers.id, achieversFragment)
        setAchieversKeyListener()
        discoverWhereYouStandListeners()
        enterButtonKeyListeners()
        goalSpheresListeners()
        setInitialView3()
        getPercentageConnectionsApi()
        callReadinessApiAsync()
        callFutureSuccessApi()
    }

    private fun generateCircle(circleName: String): ConstraintLayout {
        val triangleCL = ConstraintLayout(context)
        val clLP = ConstraintLayout.LayoutParams(65, 65)
        clLP.topToTop = binding.classesCL.id
        clLP.bottomToBottom = binding.classesCL.id
        clLP.endToEnd = binding.classesCL.id
        clLP.startToStart = binding.classesCL.id
        triangleCL.apply {
            id = generateViewId()
            tag = circleName
            layoutParams = clLP
            background = ContextCompat.getDrawable(context, R.drawable.bg_circle_percentage)
        }

        val triangleValueTV = TextView(context)
        val tvLP = ConstraintLayout.LayoutParams(0, 0)
//        tvLP.topMargin = 10
        tvLP.topToTop = triangleCL.id
        tvLP.bottomToBottom = triangleCL.id
        tvLP.endToEnd = triangleCL.id
        tvLP.startToStart = triangleCL.id
        triangleValueTV.setTypeface(triangleValueTV.typeface, Typeface.BOLD)
        triangleValueTV.apply {
            id = generateViewId()
            isFocusable = false
            gravity = Gravity.CENTER
            setTextColor(ContextCompat.getColor(context, R.color.white))
            textSize = 12f
            layoutParams = tvLP
        }
        triangleCL.addView(triangleValueTV)
        binding.classesCL.addView(triangleCL, binding.classesCL.childCount - 2)

        return triangleCL
    }

    private fun generateTriangle(triangleName: String): ImageView {
        val triangleIV = ImageView(context)
        val tvLP = ConstraintLayout.LayoutParams(30, 30)
        tvLP.topToTop = binding.classesCL.id
        tvLP.bottomToBottom = binding.classesCL.id
        tvLP.endToEnd = binding.classesCL.id
        tvLP.startToStart = binding.classesCL.id
        triangleIV.apply {
            id = generateViewId()
            tag = triangleName
            setImageResource(R.drawable.ic_small_triangle_green)
            isFocusable = false
            layoutParams = tvLP
        }
        binding.classesCL.addView(triangleIV, binding.classesCL.childCount - 2)

        return triangleIV
    }

    private fun toggleShadowVisibilityForDiscoverArrow(visibility: Int) {
        if (visibility == VISIBLE)
            ObjectAnimator.ofFloat(binding.lvDiscoverWhereYouStand, ALPHA, 0.7f, 1f).start()
        else
            ObjectAnimator.ofFloat(binding.lvDiscoverWhereYouStand, ALPHA, 1f, 0.7f).start()

        binding.ivDiscoverShadow.visibility = visibility
    }

    private fun goalSpheresListeners() {

        centerSphere.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                (centerSphere[0] as ImageView).changeFocusBg()
            } else {
                (centerSphere[0] as ImageView)
                    .setBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            android.R.color.transparent,
                            null
                        )
                    )
            }
        }
        centerSphere.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        val view = centerSphere.focusSearch(FOCUS_LEFT)
                        if (view == null)
                            Log.i(classTag, "Focus Search left null")
                        sphere9.postDelayed({
                            sphere9.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        showAchievers(600)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphereNEET.postDelayed({
                            sphereNEET.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere12.postDelayed({
                            sphere12.requestFocus()
                        }, 10)
                    }
                }
            }

            false
        }

        sphere11.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphere11[SPHERE_GLOW_INDEX] as ImageView)

            } else {
                defaultSphereBg(sphere11[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphere11.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        sphere12.postDelayed({
                            sphere12.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphere11.postDelayed({
                            sphere11.clearFocus()
                        }, 10)
                        binding.lvDiscoverWhereYouStand.postDelayed({
                            binding.lvDiscoverWhereYouStand.requestFocus()
                            loadLottieAnimation(
                                binding.lvDiscoverWhereYouStand,
                                R.raw.discover_where_you_stand_2,
                                false
                            )
                        }, 10)
                        toggleShadowVisibilityForDiscoverArrow(VISIBLE)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere11.postDelayed({
                            sphere11.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        sphereNEET.postDelayed({
                            sphereNEET.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphereLeftSelected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(
                            GONE
                        )
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphere11,
                                sphereLeftSelected,
                                circle11,
                                triangle11,
                                binding.connection1011,
                                CoordinatePlaneValues.CENTER_LEFT
                            )
                            whichSphereClicked = "Sphere11"
                        }
                    }
                }
            }
            false
        }

        sphere12.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphere12[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere12[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphere12.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        sphere6.postDelayed({
                            sphere6.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphere11.postDelayed({
                            sphere11.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere12.postDelayed({
                            sphere12.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!centerSphere.isFocusable)
                            centerSphere.isFocusable = true
                        centerSphere.postDelayed({
                            centerSphere.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphereLeftSelected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(
                            GONE
                        )
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphere12,
                                sphereLeftSelected,
                                circle12,
                                triangle12,
                                binding.connection1012,
                                CoordinatePlaneValues.CENTER_LEFT
                            )
                            whichSphereClicked = "Sphere12"
                        }
                    }
                }
            }
            false
        }

        sphereLeftSelected.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                changeSphereBg(sphereLeftSelected[SPHERE_GLOW_INDEX] as ImageView)
                updateCurvesConceptsVisibility(0.3f, 1f, 0.3f, 1f, "")
            } else {
                defaultSphereBg(sphereLeftSelected[SPHERE_GLOW_INDEX] as ImageView)
                // initializing currentFocusedConcept
                if (keyConceptsList.isNotEmpty())
                    currentFocusedConcept = keyConceptsList[0].conceptName!!
                updateCurvesConceptsVisibility(1f, 0.3f, 1f, 0.3f, currentFocusedConcept)
            }
        }

        sphereLeftSelected.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP, KeyEvent.KEYCODE_DPAD_RIGHT,
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        updateFocusForThisView(sphereLeftSelected, true)
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (keyConceptsList.isNotEmpty()) {
                            shiftFocusToKeyConceptPoints(
                                sphereLeftSelected,
                                CoordinatePlaneValues.CENTER_LEFT
                            )
                        } else {
                            updateFocusForThisView(sphereLeftSelected, true)
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        resetSpheres(whichSphereClicked)
                    }
                }
            }
            false
        }

        sphereNEET.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphereNEET[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphereNEET[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphereNEET.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (!centerSphere.isFocusable)
                            centerSphere.isFocusable = true
                        centerSphere.postDelayed({
                            centerSphere.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphereNEET.postDelayed({
                            sphereNEET.clearFocus()
                        }, 10)
                        binding.lvDiscoverWhereYouStand.postDelayed({
                            binding.lvDiscoverWhereYouStand.requestFocus()
                            loadLottieAnimation(
                                binding.lvDiscoverWhereYouStand,
                                R.raw.discover_where_you_stand_2,
                                false
                            )
                        }, 10)
                        toggleShadowVisibilityForDiscoverArrow(VISIBLE)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere11.postDelayed({
                            sphere11.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        sphereJEE.postDelayed({
                            sphereJEE.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphereLeftSelected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(
                            GONE
                        )
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphereNEET,
                                sphereLeftSelected,
                                circleNEET,
                                triangleNEET,
                                binding.connection10NEET,
                                CoordinatePlaneValues.CENTER_LEFT
                            )
                            whichSphereClicked = "SphereNEET"
                        }
                    }
                }
            }
            false
        }

        sphereJEE.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphereJEE[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphereJEE[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphereJEE.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        if (!centerSphere.isFocusable)
                            centerSphere.isFocusable = true
                        centerSphere.postDelayed({
                            centerSphere.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphereJEE.postDelayed({
                            sphereJEE.clearFocus()
                        }, 10)
                        binding.lvDiscoverWhereYouStand.postDelayed({
                            binding.lvDiscoverWhereYouStand.requestFocus()
                            loadLottieAnimation(
                                binding.lvDiscoverWhereYouStand,
                                R.raw.discover_where_you_stand_2,
                                false
                            )
                        }, 10)
                        toggleShadowVisibilityForDiscoverArrow(VISIBLE)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphereNEET.postDelayed({
                            sphereNEET.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        showAchievers(600)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphereLeftSelected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(
                            GONE
                        )
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphereJEE,
                                sphereLeftSelected,
                                circleJEE,
                                triangleJEE,
                                binding.connection10JEE,
                                CoordinatePlaneValues.CENTER_LEFT
                            )
                            whichSphereClicked = "SphereJEE"
                        }
                    }
                }
            }
            false
        }

        sphere6.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphere6[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere6[SPHERE_GLOW_INDEX] as ImageView)

            }
        }
        sphere6.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        sphere7.postDelayed({
                            sphere7.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphere12.postDelayed({
                            sphere12.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere6.postDelayed({
                            sphere6.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        if (!centerSphere.isFocusable)
                            centerSphere.isFocusable = true
                        centerSphere.postDelayed({
                            centerSphere.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphereRightSelected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(
                            GONE
                        )
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphere6,
                                sphereRightSelected,
                                circle6,
                                triangle6,
                                binding.connection106,
                                CoordinatePlaneValues.CENTER_RIGHT
                            )
                            whichSphereClicked = "Sphere6"
                        }
                    }
                }
            }
            false
        }

        sphere7.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphere7[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere7[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphere7.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navigationMenuCallback.navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        sphere9.postDelayed({
                            sphere9.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        sphere6.postDelayed({
                            sphere6.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere7.postDelayed({
                            sphere7.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphereRightSelected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(
                            GONE
                        )
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphere7,
                                sphereRightSelected,
                                circle7,
                                triangle7,
                                binding.connection107,
                                CoordinatePlaneValues.CENTER_RIGHT
                            )
                            whichSphereClicked = "Sphere7"
                        }
                    }
                }
            }
            false
        }

        sphere8.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphere8[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere8[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphere8.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navigationMenuCallback.navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        showAchievers(600)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (!centerSphere.isFocusable)
                            centerSphere.isFocusable = true
                        centerSphere.postDelayed({
                            centerSphere.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere9.postDelayed({
                            sphere9.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphereRightSelected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(
                            GONE
                        )
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphere8,
                                sphereRightSelected,
                                circle8,
                                triangle8,
                                binding.connection108,
                                CoordinatePlaneValues.CENTER_RIGHT
                            )
                            whichSphereClicked = "Sphere8"
                        }
                    }
                }
            }
            false
        }

        sphereRightSelected.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                changeSphereBg(sphereRightSelected[SPHERE_GLOW_INDEX] as ImageView)
                updateCurvesConceptsVisibility(0.3f, 1f, 0.3f, 1f, "")
            } else {
                defaultSphereBg(sphereRightSelected[SPHERE_GLOW_INDEX] as ImageView)
                // initializing currentFocusedConcept
                if (keyConceptsList.isNotEmpty())
                    currentFocusedConcept = keyConceptsList[0].conceptName!!
                updateCurvesConceptsVisibility(1f, 0.3f, 1f, 0.3f, currentFocusedConcept)
            }
        }

        sphereRightSelected.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP,
                    KeyEvent.KEYCODE_DPAD_DOWN, KeyEvent.KEYCODE_DPAD_LEFT -> {
                        updateFocusForThisView(sphereRightSelected, true)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (keyConceptsList.isNotEmpty()) {
                            shiftFocusToKeyConceptPoints(
                                sphereRightSelected,
                                CoordinatePlaneValues.CENTER_RIGHT
                            )
                        } else {
                            updateFocusForThisView(sphereRightSelected, true)
                        }
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        resetSpheres(whichSphereClicked)
                    }
                }
            }
            false
        }

        sphere9.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                changeSphereBg(sphere9[SPHERE_GLOW_INDEX] as ImageView)
            } else {
                defaultSphereBg(sphere9[SPHERE_GLOW_INDEX] as ImageView)
            }
        }
        sphere9.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navigationMenuCallback.navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        sphere8.postDelayed({
                            sphere8.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        if (!centerSphere.isFocusable)
                            centerSphere.isFocusable = true
                        centerSphere.postDelayed({
                            centerSphere.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        sphere7.postDelayed({
                            sphere7.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        (sphereRightSelected[SPHERE_SUBTLE_GLOW_INDEX] as ImageView).hideSubtleGlow(
                            GONE
                        )
                        if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                            makeKeyConceptsAPICall(
                                sphere9,
                                sphereRightSelected,
                                circle9,
                                triangle9,
                                binding.connection109,
                                CoordinatePlaneValues.CENTER_RIGHT
                            )
                            whichSphereClicked = "Sphere9"
                        }
                    }
                }
            }
            false
        }

    }

    private fun ImageView.hideSubtleGlow(visibility: Int) {
        this.visibility = visibility
    }

    private fun shiftFocusToKeyConceptPoints(
        selectedSphere: ConstraintLayout,
        direction: Pair<Float, Float>
    ) {
        updateFocusForThisView(selectedSphere, false)
        selectedSphere.isFocusable = false
        focusToKeyConceptPoints(selectedSphere, direction, 0)
    }

    private fun focusToKeyConceptPoints(
        selectedSphere: ConstraintLayout,
        direction: Pair<Float, Float>,
        conceptPointFocusPosition: Int
    ) {
        if (keyConceptPointsList.isNotEmpty()) {
            when {
                conceptPointFocusPosition >= keyConceptPointsList.size -> {
                    updateFocusForThisView(keyConceptPointsList.last().first, true)
                }
                conceptPointFocusPosition < 0 -> {
                    updateFocusForThisView(keyConceptPointsList.first().first, false)
                    selectedSphere.isFocusable = true
                    updateFocusForThisView(selectedSphere, true)
                }
                else -> {
                    val keyConceptPoint = keyConceptPointsList[conceptPointFocusPosition]
                    updateFocusForThisView(keyConceptPoint.first, true)
                    keyConceptPointListeners(
                        selectedSphere,
                        keyConceptPoint,
                        direction,
                        conceptPointFocusPosition
                    )
                }
            }
        }
    }

    private fun keyConceptPointListeners(
        selectedSphere: ConstraintLayout,
        keyConceptPoint: Pair<ImageButton, TextView>,
        direction: Pair<Float, Float>,
        conceptPointFocusPosition: Int
    ) {
        keyConceptPoint.first.setOnFocusChangeListener { v, hasFocus ->
            val conceptText = keyConceptPoint.second
            val conceptImageButton = keyConceptPoint.first
            if (hasFocus) {
                conceptText.apply {
                    animate().apply {
                        alpha(1f)
                        duration = 500
                    }
                    currentFocusedConcept = conceptText.text.toString()
                }
                conceptImageButton.apply {
                    background =
                        ContextCompat.getDrawable(context, R.drawable.summary_curve_point_green)
                    updateLayoutParams {
                        width = 25
                        height = 25
                    }
                    alpha = 1f
                }
            } else {
                conceptText.apply {
                    animate().apply {
                        alpha(0f)
                        duration = 500
                    }
                }
                conceptImageButton.apply {
                    background =
                        ContextCompat.getDrawable(context, R.drawable.summary_curve_point_green)
                    updateLayoutParams {
                        width = 20
                        height = 20
                    }
                    alpha = 0.3f
                }
            }
        }

        keyConceptPoint.first.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (direction) {
                    CoordinatePlaneValues.CENTER_LEFT -> {
                        if (keyCode == KeyEvent.KEYCODE_DPAD_UP
                            && conceptPointFocusPosition < keyConceptPointsList.size
                        ) {
                            focusToKeyConceptPoints(
                                selectedSphere,
                                direction,
                                conceptPointFocusPosition + 1
                            )
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN
                            && conceptPointFocusPosition < keyConceptPointsList.size
                        ) {
                            focusToKeyConceptPoints(
                                selectedSphere,
                                direction,
                                conceptPointFocusPosition - 1
                            )
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
                            updateFocusForThisView(keyConceptPoint.first, true)//blocking it
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                            //move focus to related key concepts points
                            updateFocusForThisView(selectedSphere, true)
                        }
                    }
                    CoordinatePlaneValues.CENTER_RIGHT -> {
                        if (keyCode == KeyEvent.KEYCODE_DPAD_UP
                            && conceptPointFocusPosition < keyConceptPointsList.size
                        ) {
                            focusToKeyConceptPoints(
                                selectedSphere,
                                direction,
                                conceptPointFocusPosition + 1
                            )
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN
                            && conceptPointFocusPosition < keyConceptPointsList.size
                        ) {
                            focusToKeyConceptPoints(
                                selectedSphere,
                                direction,
                                conceptPointFocusPosition - 1
                            )
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                            updateFocusForThisView(keyConceptPoint.first, true)//blocking it
                        } else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
                            updateFocusForThisView(selectedSphere, true)
                        }
                    }
                }
            }
            false
        }
    }

    private fun updateCurvesConceptsVisibility(
        fromA: Float,
        ToA: Float,
        fromK: Float,
        ToK: Float,
        currentConceptText: String
    ) {
        val arcObjectAnimators = ArrayList<ObjectAnimator>()
//        val duration = MAX_ARCS_ANIM_DURATION / keyConceptsList.size
        val duration = 100L
        arcsList.forEach { arc ->
            arcObjectAnimators.add(
                ObjectAnimator.ofFloat(arc, ALPHA, fromA, ToA).setDuration(duration)
            )
        }
        val conceptObjectAnimators = ArrayList<ObjectAnimator>()
        keyConceptPointsList.forEach { keyConcept ->
            if (keyConcept.second.text.toString() != currentConceptText) {
                conceptObjectAnimators.add(
                    ObjectAnimator.ofFloat(keyConcept.first, ALPHA, fromK, ToK)
                        .setDuration(duration)
                )
            }
        }
        val animSetArcs = AnimatorSet()
        val animSetConcepts = AnimatorSet()
        animSetArcs.playSequentially(arcObjectAnimators as List<Animator>)
        animSetArcs.start()
        animSetConcepts.playSequentially(conceptObjectAnimators as List<Animator>)
        animSetConcepts.start()
    }

    private fun updateFocusForThisView(view: View, requestFocus: Boolean) {
        coroutineScope?.launch {
            delay(10)
            view.isFocusable = requestFocus
            if (requestFocus)
                view.requestFocus()
            else
                view.clearFocus()
        }
    }

    private fun defaultSphereBg(imageView: ImageView) {
        imageView.defaultBg()
    }

    private fun changeSphereBg(imageView: ImageView) {
        imageView.changeFocusBg()
    }

    @JvmOverloads
    fun ConstraintLayout.scale(x: Float = 1.5f, y: Float = 1.5f) {
        this.scaleX = x
        this.scaleY = y
    }

    private fun makeKeyConceptsAPICall(
        sphere: ConstraintLayout,
        selectedSphere: ConstraintLayout,
        circle: ConstraintLayout,
        triangle: ImageView,
        connection: LineView,
        direction: Pair<Float, Float>
    ) {
        val sphereTag = sphere.tag.toString()
        val selectedGrade = (sphere[SPHERE_TEXT_INDEX] as TextView).text.toString()
        Log.i(classTag, "Selected Grade = $selectedGrade")
        var fromGoal = ""
        var toGoal = ""
        var fromGrade = ""
        var toGrade = ""
        percentageConnectionsList.forEach { percentageData ->
            val fromExam = Utils.getClassFromExam(percentageData.fromExam)
            val toExam = Utils.getClassFromExam(percentageData.toExam)
            if (currentGrade == toExam && selectedGrade == fromExam) {
                toGrade = percentageData.toExam!!
                fromGrade = percentageData.fromExam!!
                toGoal = percentageData.toGoal.toString()
                fromGoal = percentageData.fromGoal.toString()
            } else if (currentGrade == fromExam && selectedGrade == toExam) {
                toGrade = percentageData.toExam!!
                fromGrade = percentageData.fromExam!!
                toGoal = percentageData.toGoal.toString()
                fromGoal = percentageData.fromGoal.toString()
            }
        }

        bindKeyConceptsData(
            fromGoal,
            toGoal,
            fromGrade,
            toGrade,
            "concept",
            "7",
            sphere,
            selectedSphere,
            circle,
            triangle,
            connection,
            direction
        )
    }

    private fun setSphereSelection(
        sphere: ConstraintLayout,
        selectedSphere: ConstraintLayout,
        circle: ConstraintLayout,
        triangle: ImageView,
        connection: LineView,
        direction: Pair<Float, Float>,
        keyConcepts: ArrayList<KeyConceptModel>?
    ) {
        binding.tvYourLifeSuccess.visibility = INVISIBLE
        //SWAP SPHERES
        val sphereText = (sphere[SPHERE_TEXT_INDEX] as TextView).text.toString()
        val headerText = formSphereSelectionHeaderText(sphere)
        setHeader(headerText, VISIBLE)
        swapSpheres(sphere, selectedSphere)
        updateFocusForThisView(selectedSphere, true)
        currentSelectedAnimation = selectedSphere.animation
        toggleFocusOnSpheres(false)
        binding.lvDiscoverWhereYouStand.isFocusable = false
        selectedSphere.visibility = VISIBLE

        drawCurves(sphere, direction, keyConcepts)
        playArcs()
        playOtherUIItems(sphere, selectedSphere, connection, direction, sphereText)
    }

    private fun formSphereSelectionHeaderText(
        sphere: ConstraintLayout
    ): String {
        val sphereText = (sphere[SPHERE_TEXT_INDEX] as TextView).text.toString()
        return when (currentGrade) {
            //current support is for NEET AND JEE apart from classes from 1 to 12
            "NEET", "JEE" -> {
                getString(R.string.contribution_of) + " " + getString(R.string.this_class) + " " + sphereText + " " + getString(
                    R.string.syllabus_towards
                ) + " " + currentGrade
            }
            else -> {
                when {
                    sphereText == "NEET" || sphereText == "JEE" -> {
                        getString(R.string.contribution_of) + " " + getString(R.string.this_class) + " " + currentGrade + " " + getString(
                            R.string.syllabus_towards
                        ) + " " + sphereText
                    }
                    sphereText.toInt() < currentGrade.toInt() -> {
                        getString(R.string.contribution_of) + " " + getString(R.string.this_class) + " " + sphereText + " " + getString(
                            R.string.syllabus_towards
                        ) + " " + getString(R.string.this_class) + " " + currentGrade
                    }
                    else -> {
                        getString(R.string.contribution_of) + " " + getString(R.string.this_class) + " " + currentGrade + " " + getString(
                            R.string.syllabus_towards
                        ) + " " + getString(R.string.this_class) + " " + sphereText
                    }
                }
            }
        }
    }

    private fun playArcs() {
        val arcsObjectAnimators = ArrayList<ObjectAnimator>()
//        val arcsDuration = MAX_ARCS_ANIM_DURATION / keyConceptsList.size
        val arcsDuration = 100L
        arcsList.forEach { arcView ->
            arcsObjectAnimators.add(
                ObjectAnimator.ofFloat(arcView, ALPHA, 0f, 1f).setDuration(arcsDuration)
            )
        }
        val arcsAnimatorSet = AnimatorSet()
        arcsAnimatorSet.playSequentially(arcsObjectAnimators as List<Animator>?)
        arcsAnimatorSet.start()
    }

    private fun playOtherUIItems(
        sphere: ConstraintLayout,
        selectedSphere: ConstraintLayout,
        connection: LineView,
        direction: Pair<Float, Float>,
        sphereText: String
    ) {
        val objectAnimatorsList = ArrayList<ObjectAnimator>()

        trianglesValuesMap.forEach { (triangle, _) ->
            objectAnimatorsList.add(
                ObjectAnimator.ofFloat(triangle, ALPHA, 1f, 0f).setDuration(500)
            )
        }
        circleValuesMap.forEach { (circle, _) ->
            objectAnimatorsList.add(ObjectAnimator.ofFloat(circle, ALPHA, 1f, 0f).setDuration(500))
        }
        val sphereAlphaAnimator: ObjectAnimator
        val lineAlphaAnimator: ObjectAnimator
        //alpha animators of views
        if (sphereText == "9" || sphereText == "NEET") {
            sphereAlphaAnimator = ObjectAnimator.ofFloat(sphere, ALPHA, 1f, 0f).setDuration(10)
            lineAlphaAnimator = ObjectAnimator.ofFloat(connection, ALPHA, 1f, 0f).setDuration(10)
        } else {
            when (direction) {
                CoordinatePlaneValues.CENTER_RIGHT -> {
                    sphereAlphaAnimator =
                        ObjectAnimator.ofFloat(sphere9, ALPHA, 1f, 0f).setDuration(10)
                    lineAlphaAnimator = ObjectAnimator.ofFloat(binding.connection109, ALPHA, 1f, 0f).setDuration(10)
                }
                else -> {
                    sphereAlphaAnimator = ObjectAnimator.ofFloat(sphereNEET, ALPHA, 1f, 0f).setDuration(10)
                    lineAlphaAnimator = ObjectAnimator.ofFloat(binding.connection10NEET, ALPHA, 1f, 0f).setDuration(10)
                }
            }
        }
        objectAnimatorsList.add(sphereAlphaAnimator)
        objectAnimatorsList.add(lineAlphaAnimator)

        val discoverWhereYouStandAlphaAnimator =
            ObjectAnimator.ofFloat(binding.lvDiscoverWhereYouStand, ALPHA, 0.7f, 0f)
                .setDuration(500)
        val discoverWhereYouStandTextAlphaAnimator =
            ObjectAnimator.ofFloat(binding.tvDiscoverWhereYouStand, ALPHA, 1f, 0f).setDuration(500)
        val selectedSphereAlphaAnimator =
            ObjectAnimator.ofFloat(selectedSphere, ALPHA, 0f, 1f).setDuration(10)
        objectAnimatorsList.add(discoverWhereYouStandAlphaAnimator)
        objectAnimatorsList.add(discoverWhereYouStandTextAlphaAnimator)
        objectAnimatorsList.add(selectedSphereAlphaAnimator)

        //sphere BG particles animators
        val scaleBgParticlesXPVH = PropertyValuesHolder.ofFloat(SCALE_X, 1f, 1.1f)
        val scaleBgParticlesYPVH = PropertyValuesHolder.ofFloat(SCALE_Y, 1f, 1.1f)
        val particlesScaleAnimator = ObjectAnimator.ofPropertyValuesHolder(
            binding.sphereBgParticles,
            scaleBgParticlesXPVH,
            scaleBgParticlesYPVH
        ).setDuration(500)
        val particlesAlphaAnimator =
            ObjectAnimator.ofFloat(binding.sphereBgParticles, ALPHA, 0.7f, 0.2f).setDuration(500)
        objectAnimatorsList.add(particlesScaleAnimator)
        objectAnimatorsList.add(particlesAlphaAnimator)
        //scale-up
        val scaleUpXPVH = PropertyValuesHolder.ofFloat(SCALE_X, 1f, 2f)
        val scaleUpYPVH = PropertyValuesHolder.ofFloat(SCALE_Y, 1f, 2f)
        val sphereScaleUpAnimator =
            ObjectAnimator.ofPropertyValuesHolder(selectedSphere, scaleUpXPVH, scaleUpYPVH)
                .setDuration(1000)
        //translate and scale down
        val scaleDownXPVH = PropertyValuesHolder.ofFloat(SCALE_X, 1f, 0.6f)
        val scaleDownYPVH = PropertyValuesHolder.ofFloat(SCALE_Y, 1f, 0.6f)
        val scaleUpXPVHCenter = PropertyValuesHolder.ofFloat(SCALE_X, 1f, 0.8f)
        val scaleUpYPVHCenter = PropertyValuesHolder.ofFloat(SCALE_Y, 1f, 0.8f)
        val translateXPVH = PropertyValuesHolder.ofFloat(TRANSLATION_X, direction.first)
        val translateYPVH = PropertyValuesHolder.ofFloat(TRANSLATION_Y, direction.second)
        val spheresViewScaleTransAnimator = ObjectAnimator.ofPropertyValuesHolder(
            binding.classesCL,
            scaleDownXPVH,
            scaleDownYPVH,
            translateXPVH,
            translateYPVH
        ).setDuration(700)
        val centerSphereTransAnimator = ObjectAnimator.ofPropertyValuesHolder(
            centerSphere,
            scaleUpXPVHCenter,
            scaleUpYPVHCenter,
            translateXPVH,
            translateYPVH
        ).setDuration(700)
        objectAnimatorsList.add(centerSphereTransAnimator)
        objectAnimatorsList.add(sphereScaleUpAnimator)
        objectAnimatorsList.add(spheresViewScaleTransAnimator)
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(objectAnimatorsList as Collection<Animator>?)
        animatorSet.start()
    }

    private fun swapSpheres(sphere: ConstraintLayout, selectedSphere: ConstraintLayout) {
        val sphereText = (sphere[SPHERE_TEXT_INDEX] as TextView).text.toString()
        val sphereDrawable = (sphere[SPHERE_TEXT_INDEX] as TextView).background
        val selectedSphereText = (selectedSphere[SPHERE_TEXT_INDEX] as TextView).text.toString()
        val selectedSphereDrawable = (selectedSphere[SPHERE_TEXT_INDEX] as TextView).background

        (sphere[SPHERE_TEXT_INDEX] as TextView).apply {
            text = selectedSphereText
            background = selectedSphereDrawable
        }
        (selectedSphere[SPHERE_TEXT_INDEX] as TextView).apply {
            text = sphereText
            background = sphereDrawable
        }
    }

    private fun toggleFocusOnSpheres(focus: Boolean) {
        val spheresList = spheresPositionsMap.keys.toList()
        spheresList.forEach { sphere ->
            sphere.isFocusable = focus
        }
    }

    private fun resetSphereSelection(
        sphere: ConstraintLayout,
        selectedSphere: ConstraintLayout,
        circle: ConstraintLayout,
        triangle: ImageView,
        connection: LineView,
        direction: Pair<Float, Float>
    ) {
        binding.tvYourLifeSuccess.visibility = VISIBLE
        isSphereSelected = false
        setHeader(getString(R.string.select_any_grade_to_explore), VISIBLE)
        val sphereText = (selectedSphere[SPHERE_TEXT_INDEX] as TextView).text.toString()
        swapSpheres(selectedSphere, sphere)

        //scale-down
        val scaleDownXPVH = PropertyValuesHolder.ofFloat(SCALE_X, 2f, 1.1f)
        val scaleDownYPVH = PropertyValuesHolder.ofFloat(SCALE_Y, 2f, 1.1f)
        val selectedSphereAnimator =
            ObjectAnimator.ofPropertyValuesHolder(selectedSphere, scaleDownXPVH, scaleDownYPVH)
                .apply {
                    duration = 700
                }
        //translate and scale Down
        val scaleUpXPVH = PropertyValuesHolder.ofFloat(SCALE_X, 0.6f, 1f)
        val scaleUpYPVH = PropertyValuesHolder.ofFloat(SCALE_Y, 0.6f, 1f)
        val scaleUpXPVHCenter = PropertyValuesHolder.ofFloat(SCALE_X, 0.8f, 1f)
        val scaleUpYPVHCenter = PropertyValuesHolder.ofFloat(SCALE_Y, 0.8f, 1f)
        val translateXPVH = PropertyValuesHolder.ofFloat(TRANSLATION_X, direction.first, 0f)
        val translateYPVH = PropertyValuesHolder.ofFloat(TRANSLATION_Y, direction.second, 0f)
        val centerSphereTransAnimator = ObjectAnimator.ofPropertyValuesHolder(
            centerSphere,
            scaleUpXPVHCenter,
            scaleUpYPVHCenter,
            translateXPVH,
            translateYPVH
        ).setDuration(800)
        val spheresViewScaleTransAnimator = ObjectAnimator.ofPropertyValuesHolder(
            binding.classesCL,
            scaleUpXPVH,
            scaleUpYPVH,
            translateXPVH,
            translateYPVH
        ).apply {
            duration = 800
            doOnStart {
                resetOtherViews()
            }
            doOnEnd {
                toggleFocusOnSpheres(true)
                updateFocusForThisView(sphere, true)
                val sphereAlphaAnimator: ObjectAnimator
                val connectionAlphaAnimator: ObjectAnimator
                if (sphereText == "9" || sphereText == "NEET") {
                    sphereAlphaAnimator =
                        ObjectAnimator.ofFloat(sphere, ALPHA, 0f, 1f).setDuration(200)
                    connectionAlphaAnimator =
                        ObjectAnimator.ofFloat(connection, ALPHA, 0f, 1f).setDuration(500)
                } else {
                    when (direction) {
                        CoordinatePlaneValues.CENTER_RIGHT -> {
                            sphereAlphaAnimator =
                                ObjectAnimator.ofFloat(sphere9, ALPHA, 0f, 1f).setDuration(200)
                            connectionAlphaAnimator =
                                ObjectAnimator.ofFloat(binding.connection109, ALPHA, 0f, 1f)
                                    .setDuration(500)
                        }
                        else -> {
                            sphereAlphaAnimator =
                                ObjectAnimator.ofFloat(sphereNEET, ALPHA, 0f, 1f).setDuration(200)
                            connectionAlphaAnimator =
                                ObjectAnimator.ofFloat(binding.connection10NEET, ALPHA, 0f, 1f)
                                    .setDuration(500)
                        }
                    }
                }

                val objectAnimatorsList = ArrayList<ObjectAnimator>()
                val selectedSphereAlphaAnimator =
                    ObjectAnimator.ofFloat(selectedSphere, ALPHA, 1f, 0f).setDuration(200)
                objectAnimatorsList.add(sphereAlphaAnimator)
                objectAnimatorsList.add(connectionAlphaAnimator)
                objectAnimatorsList.add(selectedSphereAlphaAnimator)
                val endAnimatorSet = AnimatorSet()
                endAnimatorSet.playTogether(objectAnimatorsList as Collection<Animator>?)
                endAnimatorSet.start()

            }
        }
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(
            centerSphereTransAnimator,
            selectedSphereAnimator,
            spheresViewScaleTransAnimator
        )
        animatorSet.start()

    }

    private fun playRemovingArcs() {
        val arcsObjectAnimators = ArrayList<ObjectAnimator>()
//        val duration = MAX_ARCS_ANIM_DURATION / keyConceptsList.size
        val duration = 100L
        arcsList.forEach { arcView ->
            arcsObjectAnimators.add(
                ObjectAnimator.ofFloat(arcView, ALPHA, 1f, 0f).setDuration(duration)
            )
        }
        val arcsAnimatorSet = AnimatorSet()
        arcsAnimatorSet.playSequentially(arcsObjectAnimators as List<Animator>?)
        arcsAnimatorSet.start()
    }

    private fun resetOtherViews() {
        binding.lvDiscoverWhereYouStand.isFocusable = true
        val discoverWhereYouStandAnimator =
            ObjectAnimator.ofFloat(binding.lvDiscoverWhereYouStand, ALPHA, 0f, 0.7f)
                .setDuration(500)
        val discoverWhereYouStandTextAnimator =
            ObjectAnimator.ofFloat(binding.tvDiscoverWhereYouStand, ALPHA, 0f, 1f).setDuration(500)

        val particlesAlphaAnimator =
            ObjectAnimator.ofFloat(binding.sphereBgParticles, ALPHA, 0.2f, 0.7f).setDuration(500)
        val scaleDownParticlesXPVH = PropertyValuesHolder.ofFloat(SCALE_X, 1.1f, 1f)
        val scaleDownParticlesYPVH = PropertyValuesHolder.ofFloat(SCALE_Y, 1.1f, 1f)
        val scaleParticlesAnimator = ObjectAnimator.ofPropertyValuesHolder(
            binding.sphereBgParticles,
            scaleDownParticlesXPVH,
            scaleDownParticlesYPVH
        ).setDuration(500)

        trianglesValuesMap.forEach { (triangle, _) ->
            ObjectAnimator.ofFloat(triangle, ALPHA, 0f, 1f).setDuration(500).start()
        }
        circleValuesMap.forEach { (circle, _) ->
            ObjectAnimator.ofFloat(circle, ALPHA, 0f, 1f).setDuration(500).start()
        }

        removeArcs()
        removePointsFromLine()

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(
            particlesAlphaAnimator,
            scaleParticlesAnimator,
            discoverWhereYouStandAnimator,
            discoverWhereYouStandTextAnimator
        )
        animatorSet.start()
    }

    private fun removeArcs() {
        playRemovingArcs()
        arcsList.forEach { arcView ->
            arcView.clearAnimation()
            removeView(arcView)
        }
        arcsList.clear()
    }

    private fun removePointsFromLine() {

        fadeInOutKeyConcepts(0.6f, 0f, true)

        //Remove point views after key concepts have faded out[Animation]
        view?.postDelayed({
            keyConceptPointsList.forEach { point ->
                point.first.clearAnimation()
                point.second.clearAnimation()
                removeView(point.first)
                removeView(point.second)
            }
            keyConceptPointsList.clear()
        }, pointFadeInOutDuration.toLong())
    }

    private fun fadeInOutKeyConcepts(fromAlpha: Float, toAlpha: Float, removePoint: Boolean) {
        val alphaAnimatorsList = ArrayList<ObjectAnimator>()
        keyConceptPointsList.forEach { point ->
            alphaAnimatorsList.add(ObjectAnimator.ofFloat(point.first, ALPHA, fromAlpha, toAlpha))
            if (removePoint)//in case point is being removed, on d-pad Back need to fade out label as well
                alphaAnimatorsList.add(ObjectAnimator.ofFloat(point.second, ALPHA, toAlpha))
        }
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(alphaAnimatorsList as Collection<Animator>?)
        animatorSet.duration = pointFadeInOutDuration.toLong()
        animatorSet.start()
    }

    private fun removeView(view: View) {
        updateFocusForThisView(view, false)
        var parent = view.parent
        if (parent != null) {
            parent = parent as ConstraintLayout
            parent.removeView(view)
        }
    }

    private fun drawCurves(
        sphere: ConstraintLayout,
        direction: Pair<Float, Float>,
        keyConcepts: ArrayList<KeyConceptModel>?
    ) {

        val point = sphereSelectionLinePointsMap[sphere.tag]
        val first = point?.first
        val second = point?.second
        val conceptsCount = keyConcepts!!.size
//        val conceptsCount = 10
        val arcPointsList = calculateArcPoints(first, second, conceptsCount, direction)
        var curveRadius = when (conceptsCount) {
            1 -> 40
            else -> 250
        }
        val angleGap = (curveRadius / arcPointsList.size) * 2
        arcPointsList.forEachIndexed { index, arcPoint ->

            val arcView = ArcView(context)
            arcView.setPointsListener(this)
            arcView.apply {
                id = generateViewId()
                alpha = 0f
            }
            arcView.setPointA(arcPoint.first)
            arcView.setPointB(arcPoint.second)
            arcView.setCurveRadius(curveRadius)
            when (direction) {
                CoordinatePlaneValues.CENTER_RIGHT -> arcView.reverseArrow(false)
                else -> arcView.reverseArrow(true)
            }
            arcView.draw()
            binding.discoverCL.addView(arcView, 0)
            arcView.animation = currentSelectedAnimation
            arcsList.add(arcView)

            curveRadius -= angleGap
        }
        fadeInOutKeyConcepts(0f, 0.6f, false)
    }

    //TODO: Need to calculate for all cases 1..10
    private fun calculateArcPoints(
        first: PointF?,
        second: PointF?,
        conceptsCount: Int,
        direction: Pair<Float, Float>
    )
            : ArrayList<Pair<PointF, PointF>> {
        val arcPointsList = ArrayList<Pair<PointF, PointF>>()
        val yGap = getYGap(conceptsCount)

        var y1 = when (direction) {
            CoordinatePlaneValues.CENTER_RIGHT -> {
                when {
                    conceptsCount > 3 -> 520f
                    else -> 550f
                }
            }
            else -> {
                when {
                    conceptsCount > 3 -> 450f
                    else -> 520f
                }
            }
        }
        var y2 = when (direction) {
            CoordinatePlaneValues.CENTER_RIGHT -> 500f
            else -> 520f
        }
        for (i in 0 until conceptsCount) {
            when (direction) {
                CoordinatePlaneValues.CENTER_RIGHT -> {
                    arcPointsList.add(Pair(PointF(605f, y1), PointF(1195f, y2)))
                }
                else -> {
                    arcPointsList.add(Pair(PointF(705f, y2), PointF(1295f, y1)))
                }
            }
            y1 += yGap
            y2 += 2
        }

        return arcPointsList
    }

    private fun getYGap(conceptsCount: Int): Int {
        /**
         * 1,2->100
         * 4 -> 60
         * 3,5,6 -> 40
         * 7 -> 30
         * 8,9,10,11 -> 20
         * 12,13,14 -> 15
         * 15 -> 10
         */
        return when (conceptsCount) {
            1,2->100
            4 -> 60
            3, 5, 6 -> 20
            7 -> 30
            8, 9, 10, 11 -> 20
            12, 13, 14 -> 15
            15 -> 10
            else -> 10
        }
    }

    private fun generateKeyConceptPoint(
        linePointX: Float,
        linePointY: Float,
        keyConcept: KeyConceptModel
    ) {

        Log.i(classTag, "key concept name = ${keyConcept.conceptName}")
        val conceptImageButton = ImageButton(context)
        val conceptIVLP = ConstraintLayout.LayoutParams(20, 20)
        conceptImageButton.apply {
            id = generateViewId()
            background = ContextCompat.getDrawable(context, R.drawable.summary_curve_point_green)
            layoutParams = conceptIVLP
            alpha = 1f
        }
        val conceptTextView = TextView(context)
        val conceptTVLP = ConstraintLayout.LayoutParams(150, ConstraintSet.WRAP_CONTENT)
        conceptTVLP.topMargin = 10
        conceptTVLP.topToBottom = conceptImageButton.id
        conceptTVLP.startToStart = conceptImageButton.id
        conceptTVLP.endToEnd = conceptImageButton.id

        conceptTextView.apply {
            id = generateViewId()
            text = keyConcept.conceptName
            alpha = 0f
            setTextColor(ContextCompat.getColor(context, R.color.line_grad_1))
            textSize = 12f
            typeface = Typeface.createFromAsset(activity?.assets, "fonts/gilroy_bold.ttf")
            layoutParams = conceptTVLP
        }

        binding.discoverCL.addView(conceptImageButton, binding.discoverCL.childCount - 1)
        binding.discoverCL.addView(conceptTextView, binding.discoverCL.childCount - 1)
        keyConceptPointsList.add(Pair(conceptImageButton, conceptTextView))

        val conceptLayoutPVHX = PropertyValuesHolder.ofFloat(TRANSLATION_X, linePointX)
        val conceptLayoutPVHY = PropertyValuesHolder.ofFloat(TRANSLATION_Y, linePointY)
        val conceptIBTransAnimator = ObjectAnimator.ofPropertyValuesHolder(
            conceptImageButton,
            conceptLayoutPVHX,
            conceptLayoutPVHY
        )
        val conceptTVTransAnimator = ObjectAnimator.ofPropertyValuesHolder(
            conceptTextView,
            conceptLayoutPVHX,
            conceptLayoutPVHY
        )

        conceptImageButton.animation = currentSelectedAnimation
        conceptTextView.animation = currentSelectedAnimation
        val animatorSet = AnimatorSet()
        animatorSet.duration = 0
        animatorSet.playTogether(conceptIBTransAnimator, conceptTVTransAnimator)
        animatorSet.start()
    }

//ToRefactor: remove setInitialView1,2,3 once library is decided
    /**
     * with aPng4Android
     */
    private fun setInitialView3() {
        val headerText = getString(R.string.everthing_you_will_ever_learn_is_connected_across_classes)
        setHeader(headerText, VISIBLE)
        updateGlobeVisibility(VISIBLE)
        val assetLoader = ResourceStreamLoader(context, R.drawable.k_graph_p1)
        val aPngDrawable = APNGDrawable(assetLoader)
        aPngDrawable.setLoopLimit(1)
        binding.ivGlobeExpanding.setImageDrawable(aPngDrawable)
        aPngDrawable.registerAnimationCallback(object : Animatable2Compat.AnimationCallback() {
            override fun onAnimationStart(drawable: Drawable?) {
                fadeInEnterButton()
            }

            override fun onAnimationEnd(drawable: Drawable?) {
                if (!isEnterClicked) {
                    fadeInExpandedGlobe()
                    fadeOutExpandingGlobe()
                }

            }
        })

        binding.lvDownPointer.apply {
            setAnimation(R.raw.chevron_animation_two)
            playAnimation()
        }
    }

    private fun formHeaderText(): String {
        val allLearningTranslationFirst =
            getString(R.string.all_learning_is_connected_including_all_subjects_first)
        val allLearningTranslationSecond =
            getString(R.string.all_learning_is_connected_including_all_subjects_second)
        val classTranslation = getString(R.string.this_class)

        return when (Utils.getCurrentLocale()) {
            "en" -> {
                "$allLearningTranslationFirst $currentGoal $classTranslation $currentGrade $allLearningTranslationSecond"
            }
            "hi" -> {
                "$allLearningTranslationFirst $currentGoal $currentGrade $classTranslation $allLearningTranslationSecond"
            }
            else -> {
                //for other translations
                ""
            }
        }
    }

    /**
     * with glide
     */
    private fun setInitialView2() {
        updateGlobeVisibility(VISIBLE)
        requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(requireContext())
            .load(R.drawable.k_graph_part_1_new)
            .apply(requestOptions).listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    fadeInEnterButton()
                    (resource as WebpDrawable).loopCount = 1
                    resource.registerAnimationCallback(object :
                        Animatable2Compat.AnimationCallback() {
                        override fun onAnimationEnd(drawable: Drawable) {
                            fadeInExpandedGlobe()
                            fadeOutExpandingGlobe()
                        }
                    })
                    return false
                }
            })
            .into(binding.ivGlobeExpanding)

        binding.lvDownPointer.apply {
            setAnimation(R.raw.chevron_animation_two)
            playAnimation()
        }
    }

    /**
     * with fresco
     */
    private fun setInitialView() {
        setHeader("", GONE)
        updateGlobeVisibility(VISIBLE)
        requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
//        playWebPAnimationWithFresco(binding.ivGlobeExpanding, R.drawable.k_graph_part_1_new, true)

        binding.lvDownPointer.apply {
            setAnimation(R.raw.chevron_animation_two)
            playAnimation()
        }
    }

    private fun setHeader(textToShow: String, visibility: Int) {
        binding.tvAllLearning.visibility = visibility
        binding.tvAllLearning.text = textToShow
    }

    private fun playWebPAnimationWithFresco(
        view: SimpleDraweeView,
        resource: Int,
        setController: Boolean
    ) {
        val controller = Fresco.newDraweeControllerBuilder()
        if (setController) {
            controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
                override fun onFinalImageSet(
                    id: String?,
                    imageInfo: ImageInfo?,
                    animatable: Animatable?
                ) {
                    val anim = animatable as AnimatedDrawable2
                    anim.setAnimationListener(object : AnimationListener {
                        override fun onAnimationRepeat(drawable: AnimatedDrawable2?) {}
                        override fun onAnimationStart(drawable: AnimatedDrawable2?) {
                            fadeInEnterButton()
                        }

                        override fun onAnimationFrame(
                            drawable: AnimatedDrawable2?,
                            frameNumber: Int
                        ) {
                        }

                        override fun onAnimationStop(drawable: AnimatedDrawable2?) {
                            fadeInExpandedGlobe()
                            fadeOutExpandingGlobe()
                            Log.i(classTag, "K-graph 1 animation ended")
                        }

                        override fun onAnimationReset(drawable: AnimatedDrawable2?) {}

                    })
                }
            }
        }
        controller.autoPlayAnimations = true
        val res = "res:/" + resource
        controller.setUri(Uri.parse(res))
        view.controller = controller.build()
    }

    override fun onDestroy() {
//        binding.ivGlobeExpanding.visibility = GONE
//        if (!activity?.isFinishing!!) {
//            Glide.with(requireContext()).clear(binding.ivGlobeExpanding)
//        }
        Log.i(classTag, "onDestroy")
        super.onDestroy()
    }

    private fun fadeInExpandedGlobe() {
        binding.ivGlobeExpanded.apply {
            alpha = 0f
            visibility = VISIBLE
//            Glide.with(requireContext()).load(R.drawable.k_graph_part_2_loop_new).into(this)
//            playWebPAnimationWithFresco(
//                binding.ivGlobeExpanded,
//                R.drawable.k_graph_part_2_loop_new,
//                false
//            )
            val resourceStreamLoader = ResourceStreamLoader(context, R.drawable.k_graph_p2)
            val aPngDrawable = APNGDrawable(resourceStreamLoader)
            setImageDrawable(aPngDrawable)
            animate().apply {
                alpha(1f)
                duration = 300
            }
        }
    }

    private fun fadeInEnterButton() {
        fadeInEnterButtonShadow()
        binding.lvEnterKey.apply {
            alpha = 0f
            visibility = VISIBLE
            animate().apply {
                alpha(1f)
                duration = 5000
                setUpdateListener { animation ->
                    animation?.doOnEnd {
                        ObjectAnimator.ofFloat(binding.lvEnterKey, TRANSLATION_Y, 5f).apply {
                            duration = 1000
                            repeatMode = ValueAnimator.REVERSE
                            repeatCount = ValueAnimator.INFINITE
                            start()
                        }
                    }
                }
            }
            postDelayed({
                this.requestFocus()
            }, 10)
        }
        loadLottieAnimation(binding.lvEnterKey, R.raw.golden_button_new, true)
        loadLottieAnimation(binding.lvEnterKey, R.raw.golden_button_new, false)
    }

    private fun fadeInEnterButtonShadow() {
        binding.enterShadow.apply {
            alpha = 0f
            visibility = VISIBLE
            animate().apply {
                alpha(1f)
                duration = 5000
            }
            postDelayed({
                this.requestFocus()
            }, 10)
        }
    }

    private fun fadeOutExpandingGlobe() {
        binding.ivGlobeExpanding.apply {
            alpha = 1f
            animate().apply {
                alpha(0f)
                duration = 500
                setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator?) {
                        binding.ivGlobeExpanding.visibility = GONE
                    }
                })
            }
        }
    }

    private fun discoverWhereYouStandListeners() {

        binding.lvDiscoverWhereYouStand.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        binding.discoverCL.visibility = GONE
                        binding.flAchievers.visibility = GONE
                        readinessFragment = ReadinessFragment()
                        fragmentReplacer(binding.flReadiness.id, readinessFragment)
                        removeFocusFromRightArrow()
                        toggleShadowVisibilityForDiscoverArrow(GONE)
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT, KeyEvent.KEYCODE_DPAD_DOWN,
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        centerSphere.isFocusable = false
                        sphereNEET.postDelayed({
                            sphereNEET.requestFocus()
                        }, 10)
                        stopLottieAnimation(binding.lvDiscoverWhereYouStand)
                        toggleShadowVisibilityForDiscoverArrow(GONE)
                    }
                }
            }
            false
        }
    }

    private fun removeFocusFromRightArrow() {
        binding.lvDiscoverWhereYouStand.postDelayed({
            binding.lvDiscoverWhereYouStand.clearFocus()
        }, 10)
        stopLottieAnimation(binding.lvDiscoverWhereYouStand)
    }

    private fun enterButtonKeyListeners() {

        binding.lvEnterKey.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_ENTER, KeyEvent.KEYCODE_DPAD_CENTER -> {
                        isEnterClicked = true
                        setGoalsSpheresView()
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navigationMenuCallback.navMenuToggle(true)
                    }
                    KeyEvent.KEYCODE_DPAD_UP, KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        binding.lvEnterKey.postDelayed({
                            binding.lvEnterKey.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN -> {
                        showAchievers(600)
                    }
                }
            }
            false
        }
    }

    private fun setGoalsSpheresView() {
        binding.lvEnterKey.visibility = GONE
        binding.enterShadow.visibility = GONE
        var marginParams = binding.tvAllLearning.layoutParams as ViewGroup.MarginLayoutParams
        marginParams.topMargin = 10
        binding.tvAllLearning.layoutParams = marginParams
        binding.tvYourLifeSuccess.apply {
            text = "See how Class $currentGrade concepts connect to your past and future"
            textSize = 18f
        }
        binding.tvAllLearning.apply {
            text = "Select any grade to explore"
            visibility = VISIBLE
        }

        hideGlobe()
//        updateGlobeVisibility(GONE)
        if (isReadinessDataAvailable && isFutureSuccessDataAvailable) {
            loadLottieAnimation(
                binding.lvDiscoverWhereYouStand,
                R.raw.discover_where_you_stand_no_text, true
            )
            binding.tvDiscoverWhereYouStand.visibility = VISIBLE
        }
        val sphereBgParticlesAnimator =
            ObjectAnimator.ofFloat(binding.sphereBgParticles, ALPHA, 0f, 0.7f).apply {
                duration = 500
            }
        val sphereAnimator = ObjectAnimator.ofFloat(binding.classesCL, ALPHA, 0f, 1f).apply {
            duration = 500
        }
        val centerSphereAnimator = ObjectAnimator.ofFloat(centerSphere, ALPHA, 0f, 1f).apply {
            duration = 500
        }

        val animatorSet = AnimatorSet()
        animatorSet.apply {
            playTogether(sphereBgParticlesAnimator, sphereAnimator, centerSphereAnimator)

            doOnStart {
                loadLottieAnimation(binding.sphereBgParticles, R.raw.circle_dots_2, true)
                loadLottieAnimation(binding.sphereBgParticles, R.raw.circle_dots_2, false)
                setSpheres()
            }

            start()
        }

    }

    private fun hideGlobe() {
        binding.ivGlobeExpanded.visibility = GONE
        binding.ivGlobeExpanding.visibility = GONE
    }

    private fun updateGlobeVisibility(visibility: Int) {
        binding.ivGlobeExpanding.visibility = visibility
        binding.tvAllLearning.visibility = visibility
    }

    private fun setSpheres() {
        binding.ivGlobeExpanded.visibility = GONE
        binding.classesCL.visibility = VISIBLE
        centerSphere.visibility = VISIBLE
        //getting default focus on center sphere
        (centerSphere[1] as TextView).text = currentGrade
        centerSphere.postDelayed({
            centerSphere.requestFocus()
        }, 10)

        val float1Anim = AnimationUtils.loadAnimation(context, R.anim.anim_float_random_1)
        val float2Anim = AnimationUtils.loadAnimation(context, R.anim.anim_float_random_2)
        val float3Anim = AnimationUtils.loadAnimation(context, R.anim.anim_float_random_3)

        spheresPositionsMap.forEach { (sphere, spherePosition) ->
            if (sphere.tag != "CenterSphere" && (sphere[SPHERE_TEXT_INDEX] as TextView).text.toString() != currentGrade) {
                val triangle = sphereTriangleMap[sphere]
                val circle = sphereCircleMap[sphere]
                val triangleValues = trianglesValuesMap[triangle]!!
                val circlePosition = circleValuesMap[circle]!!
                val trianglePosition = triangleValues.trianglePosition
                triangle?.animate()?.apply {
                    translationX(trianglePosition.x)
                    translationY(trianglePosition.y)
                    duration = 1200
                }
                circle?.animate()?.apply {
                    translationX(circlePosition.x)
                    translationY(circlePosition.y)
                    duration = 1200
                }
                val connection = showLine(sphere)
                sphere.animate()?.apply {
                    translationX(spherePosition.x)
                    translationY(spherePosition.y)
                    duration = 1000
                    withEndAction {
                        val randomFloatAnim = listOf(float3Anim, float1Anim, float2Anim).random()
                        sphere.startAnimation(randomFloatAnim)
                        triangle?.startAnimation(randomFloatAnim)
                        circle?.startAnimation(randomFloatAnim)
                        connection?.startAnimation(randomFloatAnim)
                    }
                }
                setSelectedSphereTranslation(sphere, spherePosition)
            }
        }
    }

    private fun setSelectedSphereTranslation(
        originalSphere: ConstraintLayout,
        spherePosition: PointF
    ) {
        when (originalSphere.tag) {
            "Sphere9" -> {
                setSelectedSphereAnimation(sphereRightSelected, originalSphere, spherePosition)
            }
            "SphereNEET" -> {
                setSelectedSphereAnimation(sphereLeftSelected, originalSphere, spherePosition)
            }
        }
    }

    private fun setSelectedSphereAnimation(
        selectedSphere: ConstraintLayout,
        originalSphere: ConstraintLayout,
        spherePosition: PointF
    ) {
        selectedSphere.animate()?.apply {
            translationX(spherePosition.x)
            translationY(spherePosition.y)
            duration = 1000
            withEndAction {
                selectedSphere.startAnimation(originalSphere.animation)
            }
        }
    }

    private fun showLine(
        sphere: ConstraintLayout
    ): LineView? {
        var lineView: LineView? = null
        when (sphere.tag) {
            "Sphere9" -> {
                setSphereConnection(binding.connection109)
                lineView = binding.connection109
            }
            "Sphere11" -> {
                setSphereConnection(binding.connection1011)
                lineView = binding.connection1011
            }
            "Sphere12" -> {
                setSphereConnection(binding.connection1012)
                lineView = binding.connection1012
            }
            "Sphere6" -> {
                setSphereConnection(binding.connection106)
                lineView = binding.connection106
            }
            "Sphere7" -> {
                setSphereConnection(binding.connection107)
                lineView = binding.connection107
            }
            "Sphere8" -> {
                setSphereConnection(binding.connection108)
                lineView = binding.connection108
            }
            "SphereJEE" -> {
                setSphereConnection(binding.connection10JEE)
                lineView = binding.connection10JEE
            }
            "SphereNEET" -> {
                setSphereConnection(binding.connection10NEET)
                lineView = binding.connection10NEET
            }
        }

        return lineView
    }

    private fun setSphereConnection(
        connection: LineView
    ) {
//        getRelativeCoordinates(binding.centerSphere)
        val linePoints = linesMap[connection]
        connection.setPointA(linePoints?.startPoint)
        connection.setPointB(linePoints?.endPoint)
        connection.draw()
        connection.animate().apply {
            scaleX(1f)
            scaleY(1f)
            duration = 1200
        }
    }

    private fun loadSpheres(
        currentGrade: String,
        percentageData: ArrayList<PercentageData>?
    ) {
        //this arrangement is fo 10
        spheresPositionsMap[centerSphere] = PointF(0f, 0f)
        spheresPositionsMap[sphere8] = PointF(-240f, 240f)
        spheresPositionsMap[sphere11] = PointF(240f, -220f)
        spheresPositionsMap[sphere7] = PointF(-350f, -180f)
        spheresPositionsMap[sphere9] = PointF(-390f, 20f)
        spheresPositionsMap[sphere12] = PointF(30f, -320f)
        spheresPositionsMap[sphere6] = PointF(-215f, -295f)
        spheresPositionsMap[sphereJEE] = PointF(300f, 210f)
        spheresPositionsMap[sphereNEET] = PointF(400f, -50f)

        percentageData?.forEach {
            val toExam = Utils.getClassFromExam(it.toExam)
            val fromExam = Utils.getClassFromExam(it.fromExam)
            if (toExam != currentGrade) {
                setSphereValue(it, false)
            }
            if (fromExam != currentGrade) {
                setSphereValue(it, true)
            }
        }
    }

    private fun setSphereValue(it: PercentageData, from: Boolean) {
        val grade = if (from)
            Utils.getClassFromExam(it.fromExam)
        else
            Utils.getClassFromExam(it.toExam)
        val resId = resources.getIdentifier("layout_sphere_$grade", "id", context?.packageName)
        var sphere = view?.findViewById<ConstraintLayout>(resId)

        //As 10 & 6 share same position for grade 1-5, if grade is already set(i.e for 10) then return for 6
        if (grade == "6" && (sphere?.get(SPHERE_TEXT_INDEX) as TextView).text.isNotEmpty()) {
            return
        }

        //this solution is temporary as design & requirement of showing all grades is not clear
        if (sphere == null && grade == "10") {
            //if sphere is null and we have to set position for 10, we will put 10 into current grade's old position
            sphere = view?.findViewById(
                resources.getIdentifier("layout_sphere_$currentGrade", "id", context?.packageName)
            )

            //if sphere is still null that means current grade is in bw 1-5, in that case taking sphere 6's position
            if (sphere == null) {
                sphere = view?.findViewById(
                    resources.getIdentifier("layout_sphere_6", "id", context?.packageName)
                )
            }
        }
        sphere?.apply {
            try {
                val textView = this[SPHERE_TEXT_INDEX] as TextView
                textView.text = grade
                val sphereScaleSize =
                    minSize + it.percentageConnection!!.div(100) * abs(maxSize - minSize)
                val params = sphere.layoutParams
                val sizeDelta = 70
                params.width = sphereScaleSize.toInt() + sizeDelta
                params.height = sphereScaleSize.toInt() + sizeDelta
                Log.i("sphere", "${sphere.tag} ${sphereScaleSize.toInt()}")
                sphere.requestLayout()
            } catch (e: Exception) {

            }
        }
    }

    private fun loadCircles(percentageData: ArrayList<PercentageData>?) {
        //same order as spheres
        trianglesValuesMap[triangle8] = TriangleValue(PointF(-124f, 93f), 40f)//
        circleValuesMap[circle8] = PointF(-162f, 140f)
        sphereTriangleMap[sphere8] = triangle8
        sphereCircleMap[sphere8] = circle8

        trianglesValuesMap[triangle11] = TriangleValue(PointF(97f, -120f), 50f)
        circleValuesMap[circle11] = PointF(142f, -145f)
        sphereTriangleMap[sphere11] = triangle11
        sphereCircleMap[sphere11] = circle11

        trianglesValuesMap[triangle7] = TriangleValue(PointF(-165f, -115f), 110f)//
        circleValuesMap[circle7] = PointF(-215f, -130f)
        sphereTriangleMap[sphere7] = triangle7
        sphereCircleMap[sphere7] = circle7

        trianglesValuesMap[triangle9] = TriangleValue(PointF(-170f, -25f), 85f)//
        circleValuesMap[circle9] = PointF(-270f, -20f)
        sphereTriangleMap[sphere9] = triangle9
        sphereCircleMap[sphere9] = circle9

        trianglesValuesMap[triangle12] = TriangleValue(PointF(10f, -155f), 10f)//
        circleValuesMap[circle12] = PointF(10f, -210f)
        sphereTriangleMap[sphere12] = triangle12
        sphereCircleMap[sphere12] = circle12

        trianglesValuesMap[triangle6] = TriangleValue(PointF(-80f, -150f), 135f)//
        circleValuesMap[circle6] = PointF(-122f, -195f)
        sphereTriangleMap[sphere6] = triangle6
        sphereCircleMap[sphere6] = circle6

        trianglesValuesMap[triangleJEE] = TriangleValue(PointF(145f, 65f), 118f)
        circleValuesMap[circleJEE] = PointF(210f, 115f)
        sphereTriangleMap[sphereJEE] = triangleJEE
        sphereCircleMap[sphereJEE] = circleJEE

        trianglesValuesMap[triangleNEET] = TriangleValue(PointF(180f, -57f), 90f)
        circleValuesMap[circleNEET] = PointF(250f, -55f)
        sphereTriangleMap[sphereNEET] = triangleNEET
        sphereCircleMap[sphereNEET] = circleNEET

        percentageData?.forEach {
            val toExam = Utils.getClassFromExam(it.toExam)
            val fromExam = Utils.getClassFromExam(it.fromExam)
            if (toExam != currentGrade) {
                setCircleValue(it, false)
            }
            if (fromExam != currentGrade) {
                setCircleValue(it, true)
            }
        }

    }

    private fun setCircleValue(it: PercentageData, from: Boolean) {
        var invertTriangle = false
        val grade = if (from)
            Utils.getClassFromExam(it.fromExam)
        else
            Utils.getClassFromExam(it.toExam)
        val triangle = this.view?.findViewWithTag<ImageView>("Triangle${grade}")
        var circle = this.view?.findViewWithTag<ConstraintLayout>("Circle${grade}")

        //this solution is temporary as design & requirement of showing all grades is not clear

        //As 10 & 6 share same position for grade 1-5, if grade is already set(i.e for 10) then return for 6
        if (grade == "6" && (circle?.get(0) as TextView).text.isNotEmpty()) {
            return
        }

        if (circle == null && grade == "10") {
            //if triangle is null and we have to set position for 10, we will put 10 into current grade's old triangle position
            invertTriangle = true
            circle = this.view?.findViewWithTag("Circle${currentGrade}")

            //if triangle is still null that means current grade is in bw 1-5, in that case taking triangle 6's position
            if (circle == null) {
                circle = this.view?.findViewWithTag("Circle6")
            }
        } else {
            if (grade == "7" || grade == "8" || grade == "9" || grade == "6") {
                //this handles from 1-6
                if (currentGrade.toInt() <= 6)
                    invertTriangle = true
                else if (currentGrade.toInt() < grade.toInt())
                    invertTriangle = true

            }
        }

        circle?.apply {
            (this[0] as TextView).apply {
                text = "${it.percentageConnection?.roundToInt().toString()}%"
            }
        }

        triangle?.apply {

            rotation = if (!invertTriangle)
                trianglesValuesMap[triangle]?.triangleAngle!!
            else {
                if (currentGrade == "12")
                    trianglesValuesMap[triangle]?.triangleAngle!! + 173 //rotation adjustment for 12
                else
                    trianglesValuesMap[triangle]?.triangleAngle!! - 180 //180 bcz triangle needs to be inverted
            }
        }
    }

    private fun loadLines() {
        linesMap[binding.connection109] = LinePoint(PointF(940f, 350f), PointF(600f, 390f))
        linesMap[binding.connection108] = LinePoint(PointF(940f, 350f), PointF(740f, 570f))
        linesMap[binding.connection107] = LinePoint(PointF(940f, 350f), PointF(570f, 190f))
        linesMap[binding.connection106] = LinePoint(PointF(940f, 350f), PointF(700f, 40f))
        linesMap[binding.connection1011] = LinePoint(PointF(940f, 350f), PointF(1160f, 180f))
        linesMap[binding.connection1012] = LinePoint(PointF(940f, 350f), PointF(960f, 70f))
        linesMap[binding.connection10JEE] = LinePoint(PointF(940f, 350f), PointF(1200f, 540f))
        linesMap[binding.connection10NEET] = LinePoint(PointF(940f, 350f), PointF(1300f, 330f))
    }

    private fun loadSphereSelectionLinePoints() {
        val sphere9SelectionPair = Pair(PointF(610f, 570f), PointF(1350f, 500f))
        sphereSelectionLinePointsMap["Sphere9"] = sphere9SelectionPair

        val sphere8SelectionPair = Pair(PointF(730f, 770f), PointF(1080f, 350f))
        sphereSelectionLinePointsMap["Sphere8"] = sphere8SelectionPair

        val sphere7SelectionPair = Pair(PointF(650f, 410f), PointF(1200f, 625f))
        sphereSelectionLinePointsMap["Sphere7"] = sphere7SelectionPair

        val sphere6SelectionPair = Pair(PointF(770f, 330f), PointF(1125f, 790f))
        sphereSelectionLinePointsMap["Sphere6"] = sphere6SelectionPair

        val sphere11SelectionPair = Pair(PointF(1180f, 350f), PointF(650f, 720f))
        sphereSelectionLinePointsMap["Sphere11"] = sphere11SelectionPair

        val sphere12SelectionPair = Pair(PointF(950f, 350f), PointF(910f, 800f))
        sphereSelectionLinePointsMap["Sphere12"] = sphere12SelectionPair

        val sphereJEESelectionPair = Pair(PointF(1200f, 730f), PointF(550f, 280f))
        sphereSelectionLinePointsMap["SphereJEE"] = sphereJEESelectionPair

        val sphereNEETSelectionPair = Pair(PointF(1300f, 520f), PointF(575f, 525f))
        sphereSelectionLinePointsMap["SphereNEET"] = sphereNEETSelectionPair

    }

    private fun loadLottieAnimation(
        lv: LottieAnimationView,
        animJson: Int,
        loadAnimation: Boolean
    ) {
        if (loadAnimation) {
            if (lv.visibility != VISIBLE)
                lv.visibility = VISIBLE
            lv.setAnimation(animJson)
        } else {
            //just play as its already loaded
            lv.repeatMode = LottieDrawable.RESTART
            lv.repeatCount = LottieDrawable.INFINITE
            lv.playAnimation()

        }
    }

    private fun stopLottieAnimation(lv: LottieAnimationView) {
        lv.pauseAnimation()
    }

    fun restoreSelection() {
        if (isEnterClicked) {
            centerSphere.postDelayed({
                centerSphere.requestFocus()
            }, 10)
        } else {
            binding.lvEnterKey.postDelayed({
                binding.lvEnterKey.requestFocus()
            }, 10)
        }

    }


    /*load Concepts from api*/
    fun bindKeyConceptsData(
        from_goal: String,
        to_goal: String,
        from_exam: String,
        to_exam: String,
        level: String,
        count: String,
        sphere: ConstraintLayout,
        selectedSphere: ConstraintLayout,
        circle: ConstraintLayout,
        triangle: ImageView,
        connection: LineView,
        direction: Pair<Float, Float>
    ) {
//        showProgress()
        achieveViewModel.getKeyRelationsApi(
            from_goal,
            to_goal,
            from_exam,
            to_exam,
            level,
            count,
            object : BaseViewModel.APICallBacks<ArrayList<KeyConceptModel>> {

                override fun onSuccess(model: ArrayList<KeyConceptModel>?) {
//                    hideProgress()
                    if (model != null) {
                        val keyConcepts = model
                        setSphereSelection(
                            sphere,
                            selectedSphere,
                            circle,
                            triangle,
                            connection,
                            direction,
                            keyConcepts
                        )
                        isSphereSelected = true
                        if (keyConcepts != null) {
                            keyConceptsList.clear()
                            //currently supporting showing max 15 concepts to show
                            if (keyConcepts.size > MAX_ARCS_COUNT)
                                keyConceptsList.addAll(keyConcepts.subList(0, MAX_ARCS_COUNT))
                            else
                                keyConceptsList.addAll(keyConcepts)
                        }
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
//                    hideProgress()
                    if (Utils.isApiFailed(code)) {
                        Utils.showError(
                            context,
                            code,
                            object : BaseViewModel.ErrorCallBacks {
                                override fun onRetry(msg: String) {
                                    bindKeyConceptsData(
                                        from_goal,
                                        to_goal,
                                        from_exam,
                                        to_exam,
                                        level,
                                        count,
                                        sphere,
                                        selectedSphere,
                                        circle,
                                        triangle,
                                        connection,
                                        direction
                                    )
                                }

                                override fun onDismiss() {}
                            })
                    } else {
                        showToast(error)
                    }
                }
            })
    }

    /*load connections from api*/
    fun getPercentageConnectionsApi() {
        // showProgress()
        achieveViewModel.getPercentageConnectionsApi(object :
            BaseViewModel.APICallBacks<PercentageConnectionsRes> {

            override fun onSuccess(model: PercentageConnectionsRes?) {
                // hideProgress()
                if (model != null) {
                    val percentageData = model.data
                    setConnections(percentageData)
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                // hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getPercentageConnectionsApi()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }
        })
    }

    private fun setConnections(percentageData: ArrayList<PercentageData>?) {
        percentageConnectionsList.clear()
        if (percentageData != null) {
            percentageConnectionsList.addAll(percentageData)
            loadSpheres(currentGrade, percentageData)
            loadCircles(percentageData)
            loadLines()
            loadSphereSelectionLinePoints()
        }
    }

    private fun callFutureSuccessApi() {
        achieveViewModel.getFutureSuccessApi(object :
            BaseViewModel.APICallBacks<List<FutureSuccessRes>> {
            override fun onSuccess(model: List<FutureSuccessRes>?) {
                DataManager.instance.setFutureSuccessData(model)
                isFutureSuccessDataAvailable = true
            }

            override fun onFailed(code: Int, error: String, msg: String) {
            }

        })
    }


    /*load Achieve Home api*/
    private fun loadAchieveHome() {
        showProgress()
        homeViewModel.homeAchieveApi(object :
            BaseViewModel.APICallBacks<List<AchieveHome>> {
            override fun onSuccess(model: List<AchieveHome>?) {
                hideProgress()
               if (model!=null){
                   updateHomeUI(model)
               }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(
                        App.context,
                        code,
                        object : BaseViewModel.ErrorCallBacks {
                            override fun onRetry(msg: String) {
                                loadAchieveHome()
                            }

                            override fun onDismiss() {

                            }
                        })

                } else {
                    //showToast(error)
                }
            }

        })
    }

    private fun updateHomeUI(model: List<AchieveHome>) {
        if (model.isNotEmpty()){

        }
    }


    private fun callReadinessApiAsync() {
        achieveViewModel.getReadinessApi(DataManager.instance.getExamCodeFromContent(null),
            object : BaseViewModel.APICallBacks<Readiness> {
                override fun onSuccess(model: Readiness?) {
                    if (model != null) {
                        updateReadinessResult(model)
                        isReadinessDataAvailable = true
                    }
                }

                override fun onFailed(code: Int, error: String, msg: String) {
                    Log.e(classTag, msg)
                }
            })
    }

    private fun updateReadinessResult(model: Readiness) {
        DataManager.instance.setReadiness(model)
    }

    private fun setAchieversKeyListener() {
        binding.flAchievers.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_UP -> {
                        showDiscover(600)
                    }
                    KeyEvent.KEYCODE_DPAD_DOWN, KeyEvent.KEYCODE_DPAD_RIGHT -> {
                        binding.flAchievers.postDelayed({
                            binding.flAchievers.requestFocus()
                        }, 10)
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT -> {
                        navigationMenuCallback.navMenuToggle(true)
                    }
                }
            }
            false
        }
    }

    private fun fragmentReplacer(flId: Int, fragment: Fragment) {
        when (fragment) {
            is ReadinessFragment -> {
                isReadinessActive = true
                binding.flReadiness.visibility = VISIBLE
                childFragmentManager.beginTransaction().add(flId, fragment)
                    .addToBackStack(null)
                    .commit()
            }
            is AchieversFragment -> {
                isReadinessActive = false
                childFragmentManager.beginTransaction().replace(flId, fragment)
                    .commit()
            }
        }
    }

    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        when (childFragment) {
            is AchieversFragment -> {
                childFragment.setDPadKeysListener(this)
            }
            is ReadinessFragment -> {
                childFragment.setDPadKeysListener(this)
            }
        }
    }

    private fun hideShowFrag(fragOneVisibility: Int, fragTwoVisibility: Int) {
        binding.discoverCL.visibility = fragOneVisibility
        binding.flAchievers.visibility = fragTwoVisibility
        if (fragTwoVisibility == VISIBLE) {
            binding.flAchievers.postDelayed({
                binding.flAchievers.requestFocus()
            }, 10)
        }
    }

    fun setNavigationMenuListener(callback: NavigationMenuCallback) {
        this.navigationMenuCallback = callback
    }

    private fun showAchievers(delay: Long) {
        isAchieverShown = true
        binding.discoverCL.animate().translationY(-1100f).duration = 800
        binding.flAchievers.animate().translationY(0f)
        CoroutineScope(Dispatchers.Main).launch {
            delay(delay)
            hideShowFrag(GONE, VISIBLE)
            achieversFragment.startCardFlip()
        }
    }

    private fun showDiscover(viewDelay: Long) {
        isAchieverShown = false
        binding.flAchievers.animate().translationY(1100f).duration = 800
        binding.discoverCL.animate().translationY(0f)
        CoroutineScope(Dispatchers.Main).launch {
            delay(viewDelay)
            if (viewDelay == 600L) { // this means showDiscover call is coming from achievers
                achieversFragment.stopCardFlip()
            }
            hideShowFrag(VISIBLE, GONE)
        }
        restoreSelection()
    }

    fun handleBackPress() {
        if (isReadinessActive) {
            if (readinessFragment.isFutureSuccessActive()) {
                //show 'readiness'
                readinessFragment.showReadiness()
            } else {
                //show 'discover the knowledge space'
                showDiscover(100)
                childFragmentManager.popBackStack()
                binding.flReadiness.visibility = GONE
                isReadinessActive = false
            }
        } else if (isSphereSelected) {
            //handle back press for spheres
            resetSpheres(whichSphereClicked)
        }
    }

    private fun resetSpheres(whichSphereClicked: String) {
        when (whichSphereClicked) {
            "Sphere9" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphere9,
                        sphereRightSelected,
                        circle9,
                        triangle9,
                        binding.connection109,
                        CoordinatePlaneValues.CENTER_RIGHT
                    )
                }
            }
            "Sphere8" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphere8,
                        sphereRightSelected,
                        circle8,
                        triangle8,
                        binding.connection108,
                        CoordinatePlaneValues.CENTER_RIGHT
                    )
                }
            }
            "Sphere7" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphere7,
                        sphereRightSelected,
                        circle7,
                        triangle7,
                        binding.connection107,
                        CoordinatePlaneValues.CENTER_RIGHT
                    )
                }
            }
            "Sphere6" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphere6,
                        sphereRightSelected,
                        circle6,
                        triangle6,
                        binding.connection106,
                        CoordinatePlaneValues.CENTER_RIGHT
                    )
                }
            }
            "Sphere11" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphere11,
                        sphereLeftSelected,
                        circle11,
                        triangle11,
                        binding.connection1011,
                        CoordinatePlaneValues.CENTER_LEFT
                    )
                }
            }
            "Sphere12" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphere12,
                        sphereLeftSelected,
                        circle12,
                        triangle12,
                        binding.connection1012,
                        CoordinatePlaneValues.CENTER_LEFT
                    )
                }
            }
            "SphereJEE" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphereJEE,
                        sphereLeftSelected,
                        circleJEE,
                        triangleJEE,
                        binding.connection10JEE,
                        CoordinatePlaneValues.CENTER_LEFT
                    )
                }
            }
            "SphereNEET" -> {
                if (!Utils.isKeyPressedTooFast(sphereClickDelayInterval)) {
                    resetSphereSelection(
                        sphereNEET,
                        sphereLeftSelected,
                        circleNEET,
                        triangleNEET,
                        binding.connection10NEET,
                        CoordinatePlaneValues.CENTER_LEFT
                    )
                }
            }
        }
    }

    fun isReadinessViewActive(): Boolean = isReadinessActive

    fun isAchieverViewActive(): Boolean = isAchieverShown

    override fun isKeyPadDown(isDown: Boolean, from: String) {

    }

    override fun isKeyPadUp(isUp: Boolean, from: String) {
    }

    override fun isKeyPadLeft(isLeft: Boolean, from: String) {
        if (isLeft) {
            navigationMenuCallback.navMenuToggle(true)
        }
    }

    override fun isKeyPadRight(isRight: Boolean, from: String) {
    }

    override fun isKeyPadEnter(isEnter: Boolean, from: String, cardType: String) {
    }

    private fun ImageView.defaultBg() {
        this.setBackgroundResource(R.drawable.layer_circle_transparent)
    }

    private fun ImageView.changeFocusBg() {
        this.setBackgroundResource(R.drawable.layer_circle_glow_cyan)
    }

    fun isSphereSelected(): Boolean = isSphereSelected

    fun setFocusToAchiever() {
        coroutineScope?.launch {
            delay(100)
            binding.flAchievers.requestFocus()
        }
    }

    object CoordinatePlaneValues {
        val CENTER_RIGHT = Pair(300f, -60f)
        val CENTER_LEFT = Pair(-270f, -24f)
    }

    override fun getPointOnCurve(point: PointF?) {
//        val concept = keyConceptsList.removeAt(keyConceptsList.size - 1)
//        keyConceptsList.add(0, concept)
        val concept = keyConceptsList.first()
        keyConceptsList.remove(concept)
        keyConceptsList.add(concept)
        generateKeyConceptPoint(point!!.x, point.y - 10f, concept)
    }

    private fun getAchieveApis() {
        getEducationQualification()
        getJobOptions()
        getColleges()
        getStates()
    }







    /*load getEducationQualification from api*/
    fun getEducationQualification() {
        // showProgress()
        achieveViewModel.getEducationQualification(object :
            BaseViewModel.APICallBacks<AchieveListBase> {

            override fun onSuccess(model: AchieveListBase?) {
                // hideProgress()
                if (model != null) {
                    makeLog("getEducationQualification ${model.data?.get(0)?.name}")
                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                // hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getEducationQualification()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }
        })
    }

    /*load getJobOptions from api*/
    fun getJobOptions() {
        // showProgress()
        achieveViewModel.getJobOptions(object :
            BaseViewModel.APICallBacks<AchieveListBase> {

            override fun onSuccess(model: AchieveListBase?) {
                // hideProgress()
                if (model != null) {

                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                // hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getJobOptions()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }
        })
    }

    /*load getColleges from api*/
    fun getColleges() {
        // showProgress()
        achieveViewModel.getColleges(object :
            BaseViewModel.APICallBacks<AchieveListBase> {

            override fun onSuccess(model: AchieveListBase?) {
                // hideProgress()
                if (model != null) {

                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                // hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getColleges()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }
        })
    }

    /*load getStates from api*/
    fun getStates() {
        // showProgress()
        achieveViewModel.getStates(object :
            BaseViewModel.APICallBacks<AchieveListBase> {

            override fun onSuccess(model: AchieveListBase?) {
                // hideProgress()
                if (model != null) {

                }
            }

            override fun onFailed(code: Int, error: String, msg: String) {
                // hideProgress()
                if (Utils.isApiFailed(code)) {
                    Utils.showError(context, code, object : BaseViewModel.ErrorCallBacks {
                        override fun onRetry(msg: String) {
                            getStates()
                        }

                        override fun onDismiss() {}
                    })
                } else {
                    showToast(error)
                }
            }
        })
    }



}


